package com.katadigital.owtel.daoDb;

import org.parceler.Parcel;

/**
 * Created by Omar Matthew Reyes on 4/27/16.
 * Object for IDD Price List
 */
@Parcel
public class IddList {
    String IddId;
    String IddReloadName;
    double IddReloadValue;
    String IddReloadPrice;

    public String getIddId() {
        return IddId;
    }

    public void setIddId(String iddId) {
        IddId = iddId;
    }

    public String getIddReloadName() {
        return IddReloadName;
    }

    public void setIddReloadName(String iddReloadName) {
        IddReloadName = iddReloadName;
    }

    public double getIddReloadValue() {
        return IddReloadValue;
    }

    public void setIddReloadValue(double iddReloadValue) {
        IddReloadValue = iddReloadValue;
    }

    public String getIddReloadPrice() {
        return IddReloadPrice;
    }

    public void setIddReloadPrice(String iddReloadPrice) {
        IddReloadPrice = iddReloadPrice;
    }
}
