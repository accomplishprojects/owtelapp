package com.katadigital.owtel.utils;

/**
 * Created by Omar Matthew Reyes on 4/5/16.
 * <p>
 * Class containing Constants for Web API
 */
public class API {
    public static final String URL = "";
    public static final String URL_TEST = "http://52.10.198.14/owtel-api/";
//    public static final String URL_TEST = "http://apius01.owtel.com/owtel-api/";
//public static final String URL_TEST = "http://10.201.6.9/owtel-api/";
//public static final String URL_TEST = "http://52.36.230.252/owtel-api/";

    public static final String ZIP_CODE_ADDRESS = "http://apius01.owtel.com/owtel-api/get_zip_code_address.php";

    public static final String REGISTER = "register_user.php";
    public static final String ASSIGN_NUMBER = "assign_number.php";
    public static final String AREA_CODES = "get_area_code_list.php";
    public static final String CITY_STATE = "get_zip_code.php";
    public static final String VALIDATE_CALL = "validate_call.php";
    public static final String CHANGE_EMAIL_ADDRESS = "change_email_address.php";
    public static final String CHANGE_OWTEL_NUMBER = "change_owtel_number.php";
    public static final String SUBSCRIPTION_LIST = "get_subscription_list.php";
    public static final String SUBMIT_SUBSCRIPTION = "update_subscription.php";
    public static final String IDD_LIST = "get_idd_list.php";
    public static final String ADDRESS_LIST = "get_address_list.php";
    public static final String PROCESS_USER = "process_user.php";
    public static final String ACTIVATE_USER = "activate_user.php";
    public static final String FORGOT_PASS_EMAIL = "forgot_password.php";
    public static final String FORGOT_PASS_KEY = "verify_code.php";
    public static final String FORGOT_PASS_CHANGE = "change_password.php";
    public static final String PROMO_CODES = "get_user_promo_code.php";
    public static final String LOGIN = "login.php";
    public static final String LOGOUT = "logout.php";
    public static final String SESSION_SECURITY = "check_session.php";
    public static final String IDD_CREDITS = "get_idd_credits.php";
    public static final String DELETE_THREAD = "delete_conversation_thread.php";
    public static final String DELETE_SMS_ITEM = "delete_conversation_message.php";
    /**
     * API Parameters
     **/
    public static final String REGISTER_EMAIL = "email_address";
    public static final String REGISTER_PASSWORD = "password";
    public static final String REGISTER_PROMO_CODE = "promo_code";
    public static final String REGISTER_NUMBER = "owtel_number";
    public static final String REGISTER_AREA_CODE = "area_code";
    public static final String REGISTER_COUNTRY_ID = "country_id";
    public static final String REGISTER_STATE_ID = "state_id";
    public static final String REGISTER_LOCK_ID = "lock_id";
    public static final String PAYMENT_SUBSCRIPTION_ID = "subscription_id";
    public static final String PAYMENT_SUBSCRIPTION_PRICE = "subscription_price";
    public static final String PAYMENT_IDD_ID = "idd_top_up_id";
    public static final String PAYMENT_IDD_PRICE = "idd_top_up_price";
    public static final String PAYMENT_TOTAL_PRICE = "total_price";
    public static final String PAYMENT_CARD_NUMBER = "card_number";
    public static final String PAYMENT_CARD_HOLDER = "card_holder";
    public static final String PAYMENT_CARD_CVV = "cc_cvv";
    public static final String PAYMENT_CARD_EXP_MONTH = "cc_month";
    public static final String PAYMENT_CARD_EXP_YEAR = "cc_year";
    public static final String PAYMENT_BILL_ADD_STREET = "street";
    public static final String PAYMENT_BILL_ADD_CITY_ID = "city_id";
    public static final String PAYMENT_BILL_ADD_STATE_ID = "state_id";
    public static final String PAYMENT_BILL_ADD_COUNTRY_ID = "country_id";
    public static final String PAYMENT_BILL_ADD_ZIP_CODE = "zip_code";
    public static final String PAYMENT_TYPE = "payment_type";
    public static final String ENCRYPTED_OBJECT = "json_encrypt";
    public static final String FORGOT_PASSWORD_EMAIL = "email_address";
    public static final String FORGOT_PASSWORD_CODE = "key";
    public static final String FORGOT_PASSWORD_CHANGE = "password";
    public static final String PROMO_CODE_USER_ID = "promo_code_user_id";
    public static final String LOGOUT_RAD_ID = "rad_session_id";
    public static final String OWTEL_NUMBER = "owtel_number";
    public static final String SMS_ID = "sms_id";
    public static final String THREAD_ID = "thread_id";

    public static final String WEB_SERVICE_URL = "http://52.36.230.252/owtel-api/";

    public static final String GET_CONVERSATION_LIST = "get_conversation_list.php";
    public static final String GET_CONVERSATION_THREAD = "get_conversation_thread.php";


    public static int volleyRequestConnectionTimeOut = 8000;

}
