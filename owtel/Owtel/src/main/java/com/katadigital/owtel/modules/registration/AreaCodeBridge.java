package com.katadigital.owtel.modules.registration;

import android.os.Bundle;

import java.util.List;

/**
 * Created by Omar Matthew Reyes on 7/13/16.
 */
public interface AreaCodeBridge {
    void setAreaCodeList(List<com.katadigital.owtel.models.AreaCode> areaCodeList, Bundle args);
}
