package com.katadigital.owtel.modules.main.contacts;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.adapters.AddressesAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.CallLogAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.EmailsAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.EventAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.ImAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.PhoneNumbersAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.RelationAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.WebsiteAdapter;
import com.katadigital.owtel.modules.main.contacts.callsmsblocker.objects.BlockedContact;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.favorites.FavoritesFragment;
import com.katadigital.owtel.helpers.ListViewHelper;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Util;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.modules.main.contacts.util.CustomActionBar;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.portsip.utilities.ui.image.ImageCustomManager;
import com.katadigital.ui.Style;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class ContactDetailActivity extends AppCompatActivity implements
        OnItemClickListener {

    private static final String TAG = "ContactDetailActivity";
    Boolean isContactBlocked = false;

    public TextView textDisplayname;
    public TextView textPname;
    public TextView textSuffix;
    public TextView textNick;
    public TextView textJob;
    public TextView textComp;

    public TextView textBirthDay;

    public EditText edNotText;
    public TextView textNotes;

    SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yy", Locale.getDefault());
    SimpleDateFormat sdfcom = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());

    CircleImageView mCircularImageViewContact;
    ImageView imgViewSample;
    QuickContactHelper quickContactHelper;
    public static ContactDto contactDto;
    static boolean contactIsDeleted = false;
    String selectedNumber = "";

    CallLogAdapter callLogAdapter;
    EmailsAdapter emailsAdapter;
    WebsiteAdapter websiteAdapter;
    AddressesAdapter addressAdapter;
    EventAdapter eventAdapter;
    RelationAdapter relationAdapter;
    ImAdapter imAdapter;

    ListView phoneNumbersListView, emailsListView, addressesListView,
            websiteListView, eventListView, imListView, callLogListView,
            relaListView;

    ArrayList<HashMap<String, String>> callLogsList;

    ImageView type_check;
    ImageView ic_image;
    static boolean isStarred = false;
    ScrollView scrollView;

    public static String favTitle;

    PhoneNumbersAdapter phoneNumberAdapter;
    static boolean isActiveFav;

    public static int postionlv;
    public static String searchValue = "";

    @Bind(R.id.btn_contact_message)
    Button mButtonContactMessage;
    @Bind(R.id.btn_contact_share)
    Button mButtonContactShare;
    @Bind(R.id.btn_contact_add_favorite)
    Button mButtonContactAddFavorite;
    @Bind(R.id.btn_contact_block)
    Button mButtonContactBlock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        Style.statusBarColor(this, R.color.colorPrimaryToolbar);
        setContentView(R.layout.contacts_detail);
        ButterKnife.bind(this);

        /**Setting up layout views*/
        initListView();
        initData();
        initTextField();
    }

    @Override
    protected void onResume() {
        ImageCustomManager.init();
        isContactBlocked = checkIfContactIsBlocked();
        if (isContactBlocked) {
            mButtonContactBlock.setText(getResources().getString(
                    R.string.unblock_caller_string));
        } else {
            mButtonContactBlock.setText(getResources()
                    .getString(R.string.block_caller_string));
        }

        scrollView.post(() -> scrollView.fullScroll(ScrollView.FOCUS_UP));

        if (!contactIsDeleted) {
            initActionBar();
            if (contactDto.getProfilepic() == null) {
                type_check.setVisibility(View.VISIBLE);
                if (contactDto.getAccountType().equals("SIM Account")) {
                    type_check.setBackgroundDrawable(getResources().getDrawable(R.drawable.sim_card));
                } else if (contactDto.getAccountType().equals("Local Phone Account")) {
                    type_check.setBackgroundDrawable(getResources().getDrawable(R.drawable.kataphonebook_icon));
                } else {
                    type_check.setBackgroundDrawable(NewUtil.getIconForAccount(
                            contactDto.getAccountType(), getApplicationContext()));
                }
            } else {
                type_check.setVisibility(View.GONE);
            }

            Util.checkInformationDetails(contactDto, ContactDetailActivity.this);

            try {
                if (contactDto.getAccountType().equals("SIM Account")) {
                    ic_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.sim_card));
                } else if (contactDto.getAccountType().equals("Local Phone Account")) {
                    ic_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.kataphonebook_icon));
                } else {
                    ic_image.setBackgroundDrawable(NewUtil.getIconForAccount(
                            contactDto.getAccountType(),
                            getApplicationContext()));
                }
            } catch (Exception e) {
                Log.e(TAG, "getAccountType() " + e);
            }

            edNotText.setKeyListener(null);

            ArrayList<String> contactIDListFav = QuickContactHelper
                    .getStarred(ContactDetailActivity.this);

            try {
                if (quickContactHelper.hasnumber(contactDto.getContactID()) > 0) {
                    findViewById(R.id.LL_block_caller).setVisibility(View.VISIBLE);
                    findViewById(R.id.LL_addfav).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.LL_block_caller).setVisibility(View.GONE);
                    findViewById(R.id.LL_addfav).setVisibility(View.GONE);
                }
            } catch (Exception e) {
                onBackPressed();
            }

            if (contactIDListFav.contains(contactDto.getContactID())) {
                isStarred = true;
                findViewById(R.id.LL_addfav).setVisibility(View.GONE);
            } else {
                isStarred = false;
                findViewById(R.id.LL_addfav).setVisibility(View.VISIBLE);
            }

            phoneNumberAdapter = new PhoneNumbersAdapter(ContactDetailActivity.this, contactDto.getPhoneNumbers(),
                    selectedNumber, isStarred);
            phoneNumbersListView.setAdapter(phoneNumberAdapter);

            emailsAdapter = new EmailsAdapter(this, contactDto.getEmails());
            websiteAdapter = new WebsiteAdapter(this, contactDto.getWebsites());
            addressAdapter = new AddressesAdapter(this, contactDto.getAddresses());
            eventAdapter = new EventAdapter(this, contactDto.getDateEvent());
            relationAdapter = new RelationAdapter(this, contactDto.getRelationDto());
            imAdapter = new ImAdapter(this, contactDto.getImField());

            phoneNumbersListView.setAdapter(phoneNumberAdapter);
            emailsListView.setAdapter(emailsAdapter);
            websiteListView.setAdapter(websiteAdapter);
            addressesListView.setAdapter(addressAdapter);
            eventListView.setAdapter(eventAdapter);
            relaListView.setAdapter(relationAdapter);
            imListView.setAdapter(imAdapter);

            autoHideLisview(contactDto.getPhoneNumbers().size(), R.id.LL_send_msg);
            autoHideLisview(contactDto.getPhoneNumbers().size(), R.id.LL_number);
            autoHideLisview(contactDto.getEmails().size(), R.id.LL_email);
            autoHideLisview(contactDto.getWebsites().size(), R.id.LL_url);
            autoHideLisview(contactDto.getAddresses().size(), R.id.LL_address);
            autoHideLisview(contactDto.getDateEvent().size(), R.id.LL_dates);
            autoHideLisview(contactDto.getRelationDto().size(), R.id.LL_related);
            autoHideLisview(contactDto.getImField().size(), R.id.LL_im);

            ListViewHelper.setListViewHeightBasedOnChildren(phoneNumbersListView);
            ListViewHelper.setListViewHeightBasedOnChildren(emailsListView);
            ListViewHelper.setListViewHeightBasedOnChildren(websiteListView);
            ListViewHelper.setListViewHeightBasedOnChildren(addressesListView);
            ListViewHelper.setListViewHeightBasedOnChildren(eventListView);
            ListViewHelper.setListViewHeightBasedOnChildren(relaListView);
            ListViewHelper.setListViewHeightBasedOnChildren(imListView);

            if (contactDto.getProfilepic() != null) {
                findViewById(R.id.ic_avatar_relative).setVisibility(View.VISIBLE);
                ic_image.setVisibility(View.GONE);

                /**Loading Image*/
                ImageCustomManager.getImageCustomManagerInstance()
                        .displayProfilePictureFromBitmap(ContactDetailActivity.this, contactDto.getProfilepic(),
                                mCircularImageViewContact);
            } else {
                ic_image.setVisibility(View.GONE);
                findViewById(R.id.ic_avatar_relative).setVisibility(View.GONE);
                ImageCustomManager.getImageCustomManagerInstance()
                        .displayProfilePictureFromBitmap(ContactDetailActivity.this, null,
                                mCircularImageViewContact);
            }

        } else {
            contactIsDeleted = false;
            finish();
        }
        super.onResume();

    }


    public void onBackPressed() {
        if (isActiveFav) {
            FavoritesFragment.reloadFavorites = true;
            isActiveFav = false;
        }

        finish();
        overridePendingTransition(0, R.anim.slide_out_down);

    }

    private void initListView() {
        phoneNumbersListView = (ListView) findViewById(R.id.contactnumber_lv);
        emailsListView = (ListView) findViewById(R.id.email_lv);
        websiteListView = (ListView) findViewById(R.id.website_lv);
        addressesListView = (ListView) findViewById(R.id.address_lv);
        eventListView = (ListView) findViewById(R.id.event_lv);
        imListView = (ListView) findViewById(R.id.im_lv);
        callLogListView = (ListView) findViewById(R.id.callLog_lv);
        relaListView = (ListView) findViewById(R.id.relation_lv);

        phoneNumbersListView.setDivider(null);
        emailsListView.setDivider(null);
        websiteListView.setDivider(null);
        addressesListView.setDivider(null);
        eventListView.setDivider(null);
        imListView.setDivider(null);
        callLogListView.setDivider(null);
        relaListView.setDivider(null);

        phoneNumbersListView.setOnItemClickListener(this);
        emailsListView.setOnItemClickListener(this);
        websiteListView.setOnItemClickListener(this);
        addressesListView.setOnItemClickListener(this);
        eventListView.setOnItemClickListener(this);
        imListView.setOnItemClickListener(this);
        relaListView.setOnItemClickListener(this);

    }

    @SuppressWarnings("unchecked")
    private void initData() {
        callLogsList = new ArrayList<>();
        contactIsDeleted = false;
        quickContactHelper = new QuickContactHelper(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            selectedNumber = bundle.getString("selectednumber");
            ((View) findViewById(R.id.LL_Logexist)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.logsexisting_calltime)).setText(bundle.getString("transactiondate"));
            ((TextView) findViewById(R.id.logsexisting_calltime)).setTypeface(NewUtil.getFontRoman(this));
            ((TextView) findViewById(R.id.logsexisting_calltime)).setTextSize(NewUtil.gettxtSize());
            selectedNumber = "";
        }

        Date convertedDate = new Date();
        try {
            if (bundle.getString("transactiondate") != null)
                convertedDate = sdf.parse(bundle.getString("transactiondate"));
        } catch (ParseException e) {
            Log.e(TAG, "Converted Date " + e);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.e(TAG, "NullPointer " + e);
        }

        if (bundle != null) {
            selectedNumber = bundle.getString("selectednumber");
            ((TextView) findViewById(R.id.logsexisting_calltime)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.logsexisting_calltime)).setText(YesterdayOrToday(convertedDate));

            callLogsList = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("logs");

            Log.i(TAG, "BUNDLE NOT EMPTY");
            for (int index = 0; index < callLogsList.size(); index++) {
                Log.i(TAG, "CONTACT DETAIL");
                Log.i(TAG, "CONTACT TIME: " + callLogsList.get(index).get("time"));
                Log.i(TAG, "CONTACT TYPE: " + callLogsList.get(index).get("calltype"));
                Log.i(TAG, "CONTACT DURATION: " + callLogsList.get(index).get("duration"));
            }

            callLogAdapter = new CallLogAdapter(this, callLogsList);
            callLogListView.setAdapter(callLogAdapter);
            ListViewHelper.setListViewHeightBasedOnChildren(callLogListView);
        } else {
            Log.e(TAG, "BUNDLE IS EMPTY");
            selectedNumber = "";
        }

    }

    private void initTextField() {
        // scrollview
        scrollView = (ScrollView) findViewById(R.id.details_con_sv);
        type_check = (ImageView) findViewById(R.id.ic_type_check);

        textDisplayname = (TextView) findViewById(R.id.detail_contact_name);
        textPname = (TextView) findViewById(R.id.details_phonetic);
        textSuffix = (TextView) findViewById(R.id.details_suffix);
        textNick = (TextView) findViewById(R.id.details_nickname);
        textJob = (TextView) findViewById(R.id.details_title_dep);
        textComp = (TextView) findViewById(R.id.detail_comp_name);

        // Bday
        textBirthDay = (TextView) findViewById(R.id.detail_bday);
        ((TextView) findViewById(R.id.text_bday)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.text_bday)).setTypeface(NewUtil.getFontRoman(this));
        textBirthDay.setTextSize(NewUtil.gettxtSize());
        textBirthDay.setTextSize(NewUtil.gettxtSize());

        // for notes
        textNotes = (TextView) findViewById(R.id.notes_txtview);
        edNotText = (EditText) findViewById(R.id.notes_ed_details);
        edNotText.clearFocus();
        textNotes.setTextSize(NewUtil.gettxtSize());
        edNotText.setTextSize(NewUtil.gettxtSize());
        textNotes.setTypeface(NewUtil.getFontRoman(this));
        edNotText.setTypeface(NewUtil.getFontRoman(this));

        textDisplayname.setTypeface(NewUtil.getFontBold(this));
        textPname.setTypeface(NewUtil.getFontRoman(this));
        textSuffix.setTypeface(NewUtil.getFontRoman(this));
        textNick.setTypeface(NewUtil.getFontRoman(this));
        textJob.setTypeface(NewUtil.getFontRoman(this));
        textComp.setTypeface(NewUtil.getFontRoman(this));

        textDisplayname.setTextSize(NewUtil.gettxtSize());
        textPname.setTextSize(NewUtil.gettxtSize());
        textSuffix.setTextSize(NewUtil.gettxtSize());
        textNick.setTextSize(NewUtil.gettxtSize());
        textJob.setTextSize(NewUtil.gettxtSize());
        textComp.setTextSize(NewUtil.gettxtSize());

        // image
        mCircularImageViewContact = (CircleImageView) findViewById(R.id.detail_contact_img);
        imgViewSample = (ImageView) findViewById(R.id.sample_image);
        ic_image = (ImageView) findViewById(R.id.ic_image_type);

        mButtonContactMessage.setTypeface(NewUtil.getFontRoman(this));
        mButtonContactShare.setTypeface(NewUtil.getFontRoman(this));
        mButtonContactAddFavorite.setTypeface(NewUtil.getFontRoman(this));
        mButtonContactBlock.setTypeface(NewUtil.getFontRoman(this));

        if (isContactBlocked) {
            mButtonContactBlock.setText(getResources().getString(
                    R.string.unblock_caller_string));
        } else {
            mButtonContactBlock.setText(getResources()
                    .getString(R.string.block_caller_string));
        }

    }


    private void initActionBar() {

        // Custom Action Bar ===============

        Intent intenta = getIntent();
        String resting_string = intenta.getStringExtra("recents");

        if (resting_string != null) {

            String leftItem = resting_string;
            String title = getResources().getString(R.string.info_string);
            String rightItem = getResources().getString(R.string.edit_string);
            CustomActionBar.viewCustomActionBarContact(this, leftItem, title,
                    rightItem, contactDto, new loadContactDetails());

        } else if (favTitle != null) {

            String leftItem = favTitle;
            String title = getResources().getString(R.string.info_string);
            String rigtItem = getResources().getString(R.string.edit_string);
            CustomActionBar.viewCustomActionBarContact(this, leftItem, title,
                    rigtItem, contactDto, new loadContactDetails());

            isActiveFav = true;
            favTitle = null;

        } else {

            String leftItem = getResources().getString(
                    R.string.add_contact_activity);
            String rigtItem = getResources().getString(R.string.edit_string);
            CustomActionBar.viewCustomActionBarContact(this, leftItem, "",
                    rigtItem, contactDto, new loadContactDetails());
        }

        // Custom Action Bar ===============

    }

    public String YesterdayOrToday(Date newDate) {
        String yot = "";

        Calendar newsDate = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        Calendar yesDate = Calendar.getInstance();

        newsDate.setTime(newDate);
        currentDate.setTime(new Date());
        yesDate.setTime(new Date());

        yesDate.add(Calendar.DAY_OF_MONTH, -1);

        String strNDate = newsDate.get(Calendar.MONTH) + 1 + " "
                + newsDate.get(Calendar.DAY_OF_MONTH) + ", "
                + newsDate.get(Calendar.YEAR);
        String strCDate = currentDate.get(Calendar.MONTH) + 1 + " "
                + currentDate.get(Calendar.DAY_OF_MONTH) + ", "
                + currentDate.get(Calendar.YEAR);
        String strYDate = yesDate.get(Calendar.MONTH) + 1 + " "
                + yesDate.get(Calendar.DAY_OF_MONTH) + ", "
                + yesDate.get(Calendar.YEAR);

        if (strNDate.equals(strCDate)) {
            yot = "Today";
        } else if (strNDate.equals(strYDate)) {
            yot = "Yesterday";
        } else {
            yot = sdfcom.format(newDate);
        }
        return yot;
    }

    public void autoHideLisview(int numberlist, int idListLayout) {
        if (numberlist != 0) {
            findViewById(idListLayout).setVisibility(View.VISIBLE);
        } else {
            findViewById(idListLayout).setVisibility(View.GONE);
        }
    }

    public boolean checkIfContactIsBlocked() {
        boolean result = false;
        try {
            for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
                if (!phoneNumberDto.getNumber().isEmpty()) {
                    if (MainActivity.blackList.containsKey(phoneNumberDto.getNumber().replaceAll(" ", ""))) {
                        result = true;
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            Log.e(TAG, "checkIfContactIsBlocked " + e);
        }
        return result;
    }

    public void blockContact() {
        for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
            if (!phoneNumberDto.getNumber().isEmpty()) {
                int numberType = 0;
                for (int x = 0; x < getResources().getStringArray(
                        R.array.numberspinner).length; x++) {
                    if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            getResources()
                                    .getStringArray(R.array.numberspinner)[x])) {
                        numberType = x;
                    }
                }

                BlockedContact cn = new BlockedContact(
                        contactDto.getDisplayName(), phoneNumberDto.getNumber()
                        .replaceAll(" ", ""), numberType, true, true);


                MainActivity.blackList.put(cn.getNumber(), cn);
            }

        }
        if (contactDto.getPhoneNumbers().isEmpty()) {
            // CallBlockerToastNotification
            // .showDefaultShortNotification("No phone numbers to block!");
        } else {
            saveData();
            isContactBlocked = true;
            mButtonContactBlock.setText(getResources().getString(
                    R.string.unblock_caller_string));
            // CallBlockerToastNotification
            // .showDefaultShortNotification("Contact added to blacklist successfully");
        }
    }

    public void saveData() {
        try {
            FileOutputStream fos = openFileOutput("CallBlocker.data",
                    Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(MainActivity.blackList);
            fos.close();
            oos.close();
            MainActivityBridge mMainActivityBridge = (MainActivityBridge) this;
            mMainActivityBridge.initBlocker();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }

    public void unblockContact() {
        for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
            if (!phoneNumberDto.getNumber().isEmpty()) {
                MainActivity.blackList.remove(phoneNumberDto.getNumber().replaceAll(" ", ""));
            }
        }

        try {
            saveData();
            mButtonContactBlock.setText(getResources()
                    .getString(R.string.block_caller_string));
            isContactBlocked = false;
        } catch (Exception e) {
            Log.e(TAG, "unblockContact() " + e);
        }
    }

    /**
     * Send Message to Contact
     */
    @OnClick(R.id.btn_contact_message)
    public void onClickBtnContactMessage() {
        Util.addDialogSendMessage(ContactDetailActivity.this, contactDto);
    }

    /**
     * Share contact details
     */
    @OnClick(R.id.btn_contact_share)
    public void onClickBtnContactShare() {
        String nameWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE + " = ?";
        String[] nameWhereParams = new String[]{
                String.valueOf(contactDto.getContactID()),
                StructuredName.CONTENT_ITEM_TYPE};
        Cursor c = getContentResolver().query(Data.CONTENT_URI, null,
                nameWhere, nameWhereParams, null);
        // Cursor c = getContentResolver()
        c.moveToFirst();

        Uri uri = Uri.withAppendedPath(
                ContactsContract.Contacts.CONTENT_VCARD_URI,
                c.getString(c.getColumnIndex(Data.LOOKUP_KEY)));

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/x-vcard");
        i.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(i, "Send Contact"));
        c.close();
    }

    /**
     * Add contact to favorites
     */
    @OnClick(R.id.btn_contact_add_favorite)
    public void onClickBtnContactAddFavorite() {
        Log.i(TAG, "onClick Button Favorite");
        FavoritesFragment.reloadFavorites = true;
        findViewById(R.id.LL_addfav).setVisibility(View.GONE);
        ContentValues values = new ContentValues();
        String[] fv = new String[]{contactDto.getContactID()};
        values.put(ContactsContract.Contacts.STARRED, 1);
        getContentResolver().update(ContactsContract.Contacts.CONTENT_URI,
                values, ContactsContract.Contacts._ID + "= ?", fv);
    }

    /**
     * Block/unblock contact
     */
    @OnClick(R.id.btn_contact_block)
    public void onClickBtnContactBlock() {
        if (isContactBlocked) {
            unblockContact();
        } else {
            Util.blockdialogContact(ContactDetailActivity.this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        switch (parent.getId()) {
            case R.id.contactnumber_lv:
//                startActivity(QuickContactHelper.initiateDefaultCall(
//                        ContactDetailActivity.this, contactDto.getPhoneNumbers()
//                                .get(position).getNumber().replace("#", "%23")));
                QuickContactHelper.initiateSipCall(this, contactDto.getPhoneNumbers().get(position).getNumber().replaceAll("#", "%23"));

                break;
            case R.id.email_lv:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{contactDto
                        .getEmails().get(position).getEmail()});

                try {
                    startActivity(Intent.createChooser(i, "Send Email"));
                } catch (ActivityNotFoundException e) {
                    Log.e(TAG, "onItemClick email_lv " + e);
                }
                break;

            case R.id.website_lv:

                String webstring = contactDto.getWebsites().get(position)
                        .getWebsite();

                String url = "http://" + webstring;
                Intent iweb = new Intent(Intent.ACTION_VIEW);
                iweb.setData(Uri.parse(url));
                startActivity(iweb);

                break;

            case R.id.address_lv:

                String city = contactDto.getAddresses().get(position).getCityStr();
                String street = contactDto.getAddresses().get(position)
                        .getStreetStr();

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=an+" + street + "+" + city));
                startActivity(intent);

                break;

            case R.id.event_lv:

                break;

            case R.id.im_lv:

                break;

            default:
                break;
        }

    }

    public class loadContactDetails extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog
                    .show(ContactDetailActivity.this,
                            getResources().getString(R.string.string_diag_pleasewait),
                            getResources().getString(R.string.string_diag_loading_details) + "...");
            progressDialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            EditContactsActivity.oldContactDto = contactDto;
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            Intent intent = new Intent(ContactDetailActivity.this, EditContactsActivity.class);
            intent.putExtra("isFromContactDetails", true);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_up_exit);
            progressDialog = null;
            super.onPostExecute(result);
        }
    }
}
