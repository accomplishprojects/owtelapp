package com.katadigital.owtel.modules.main.contacts.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorDescription;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.AddContactActivity;
import com.katadigital.owtel.modules.main.history.LogsDetail;
import com.katadigital.owtel.modules.main.contacts.adapters.FavoritesAdapter;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.tools.Functions;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NewUtil {


    static Dialog dialog_con;

    /**
     * Get Helvetica Neue (Bold) typeface
     *
     * @param activity Activity
     * @return Typeface
     */
    public static Typeface getFontBold(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/HelveticaNeue-Bold.ttf");
    }

    /**
     * Get Helvetica Neue (Roman) typeface
     *
     * @param activity Activity
     * @return Typeface
     */
    public static Typeface getFontRoman(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/HelveticaNeue-Roman.ttf");
    }

    /**
     * Get Bauhaus typeface
     *
     * @param activity Activity
     * @return Typeface
     */
    public static Typeface getFontBauhaus(Activity activity) {
        return Typeface.createFromAsset(activity.getAssets(), "fonts/ITC_Bauhaus_LT_Medium.ttf");
    }

    public static int gettxtSize() {
        return 15;
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    // New Util by Jeff
    public static void IntetCustomTrans(Activity activity, Class<?> cls) {
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_up,
                R.anim.slide_in_up_exit);
    }

    public static void DatePicker(Activity activity, final EditText editTxt) {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth, int selectedday) {

                        String year1 = String.valueOf(selectedyear);
                        String month1 = String.valueOf(selectedmonth + 1);
                        String day1 = String.valueOf(selectedday);

                        editTxt.setText(month1 + "-" + day1 + "-" + year1);

                    }
                }, mYear, mMonth, mDay);
        mDatePicker.setTitle(activity.getResources().getString(
                R.string.string_select_date));
        mDatePicker.show();
    }

    public static void CountryPicker(FragmentActivity activity,
                                     final EditText edCountry) {

        CountryPickerActivity.edCountry = edCountry;
        NewUtil.IntetCustomTrans(activity, CountryPickerActivity.class);

    }

    public static void SpinnerActivity(Activity activity,
                                       ArrayList<Button> btnPhonespinner, Button spinnerbtn,
                                       int numberspinner) {
        AllFieldActivity.number = spinnerbtn.getId();
        AllFieldActivity.spinnerbtn = btnPhonespinner;
        AllFieldActivity.spinnerArrayID = numberspinner;
        Intent i = new Intent(activity, AllFieldActivity.class);
        activity.startActivity(i);
    }

    public static void SpinnerActivityCustomLoader(Activity activity,
                                                   ArrayList<Button> btnPhonespinner, Button spinnerbtn,
                                                   int numberspinner, boolean custom, boolean noCustom) {
        AllFieldActivity.number = spinnerbtn.getId();
        AllFieldActivity.spinnerbtn = btnPhonespinner;
        AllFieldActivity.spinnerArrayID = numberspinner;
        AllFieldActivity.custom = custom;
        AllFieldActivity.noCustom = noCustom;
        Intent i = new Intent(activity, AllFieldActivity.class);
        activity.startActivity(i);
    }

    public static Drawable getIconForAccount(String accounttype, Context context) {

        AccountManager accountManager = AccountManager.get(context);

        AuthenticatorDescription[] descriptions = accountManager
                .getAuthenticatorTypes();
        for (AuthenticatorDescription description : descriptions) {
            if (description.type.equals(accounttype)) {
                PackageManager pm = context.getPackageManager();
                return pm.getDrawable(description.packageName,
                        description.iconId, null);
            }
        }
        return null;
    }

    public static void createContactChooseDiag(final Activity activity) {
        final ArrayList<AccountCreate> gList = new ArrayList<>();
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(activity);

        String oprnmeSIM1 = telephonyInfo.getOprnmeSIM1();
        String oprnmeSIM2 = telephonyInfo.getOprnmeSIM2();

        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
        boolean isDualSIM = telephonyInfo.isDualSIM();

        AccountManager accountManager = AccountManager.get(activity);
        Account[] accounts = accountManager.getAccountsByType("com.google");

        // ---- Change

        // String localPhone = "Local Phone Account";
        // final String sim = "SIM Account";

        Intent intent = new Intent(activity, AddContactActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        // activity.finish();

        ContactDto contactDto = new ContactDto();
        contactDto.setAccountType(Var.localPhone);
        contactDto.setAccountName(Var.namePhone);
        AddContactActivity.contactDto = contactDto;
        Log.e("NewUtil", "createContactChooseDiag: ");
    }

    public static void createContactChooseDiag2(final Activity activity, final String text) {

        Intent intent = new Intent(activity, AddContactActivity.class);
        Bundle extras = new Bundle();

        if (text != "") {
            extras.putString("preloaded_number", String.valueOf(text));
        }

        intent.putExtras(extras);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_up,
                R.anim.slide_in_up_exit);

        ContactDto contactDto = new ContactDto();
        contactDto.setAccountType(Var.localPhone);
        contactDto.setAccountName(Var.namePhone);
        AddContactActivity.contactDto = contactDto;

        LogsDetail.removeinfoActiviy = true;
        AddContactActivity.keypadCheck = true;
    }

    public static void createContactChooseDiag3(final Activity activity,
                                                final String text) {

        // --Change

        Intent intent = new Intent(activity, AddContactActivity.class);
        Bundle extras = new Bundle();

        if (text != "") {
            extras.putString("preloaded_number", String.valueOf(text));
        }

        intent.putExtras(extras);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_up,
                R.anim.slide_in_up_exit);

        ContactDto contactDto = new ContactDto();
        contactDto.setAccountType(Var.localPhone);
        contactDto.setAccountName(Var.namePhone);
        AddContactActivity.contactDto = contactDto;
    }

    public static void insertSIMContact(Activity activity) {

        Uri simUri = Uri.parse("content://com.android.contacts/raw_contacts");
        ContentValues values = new ContentValues();
        String name[] = {"a", "b", "c", "d",};
        int number[] = {1, 2, 3, 4};

        for (int i = 0; i < name.length; i++) {
            values.put("tag", name[i]);
            values.put("number", number[i]);
            activity.getContentResolver().insert(simUri, values);
            activity.getContentResolver().notifyChange(simUri, null);
        }

    }

//    public static void onActivityResultCustom(Activity activity,
//                                              ContactDto contactDto, int requestCode, int resultCode,
//                                              Intent data, CircularImageView avatar) {
//
//        if (requestCode == 1) {
//            if (resultCode == activity.RESULT_OK) {
//
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//
//                Intent intent = new Intent(activity, CropperActivity.class);
//
//                intent.putExtra("imagepass", photo);
//                activity.startActivityForResult(intent, 3);
//                activity.overridePendingTransition(R.anim.slide_in_up,
//                        R.anim.slide_in_up_exit);
//
//            }
//        } else if (requestCode == 3) {
//            if (resultCode == activity.RESULT_OK) {
//                contactDto.setProfilepic(CropperActivity.selectedImage);
//                avatar.setImageBitmap(CropperActivity.selectedImage);
//            }
//        } else if (requestCode == 4) {
//            // Gallery
//            if (resultCode == activity.RESULT_OK) {
//
//                Uri selectedImage = data.getData();
//
//                try {
//                    Bitmap thumbnail = Util.decodeBitmap(selectedImage, activity);
//
//                    // here
//                    contactDto.setProfilepic(thumbnail);
//
//                    Intent intent = new Intent(activity, CropperActivity.class);
//                    intent.putExtra("imagepass", thumbnail);
//                    activity.startActivityForResult(intent, 3);
//                    activity.overridePendingTransition(R.anim.slide_in_up,
//                            R.anim.slide_in_up_exit);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (resultCode == activity.RESULT_CANCELED) {
//
//            }
//        }
//    }

    // /

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Uri uri, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static void setBoldFont(Activity activity, int Idnum) {
        ((TextView) activity.findViewById(Idnum)).setTypeface(NewUtil
                .getFontBold(activity));
        ((TextView) activity.findViewById(Idnum)).setTextSize(NewUtil
                .gettxtSize());
    }

    public static void setRomanFont(Activity activity, int Idnum) {
        ((TextView) activity.findViewById(Idnum)).setTypeface(NewUtil
                .getFontRoman(activity));
        ((TextView) activity.findViewById(Idnum)).setTextSize(NewUtil
                .gettxtSize());
    }

    public static void integrationNumber(Activity activity,
                                         ContactDto contactDto, int position) {

        try {

            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            List<ResolveInfo> resInfos = activity.getPackageManager()
                    .queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("JM Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);
                    if (packageName.contains(activity.getResources().getString(
                            R.string.messages_package))) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName,
                                resInfo.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra("MyKey", ""
                                + contactDto.getPhoneNumbers().get(position)
                                .getNumber());
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    System.out.println("Have Intent");
                    Intent chooserIntent = Intent
                            .createChooser(targetShareIntents.remove(0),
                                    "Choose app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                            targetShareIntents.toArray(new Parcelable[]{}));
                    activity.startActivity(chooserIntent);
                } else {

                    // System.out.println("Do not Have Intent");
                    // Toast.makeText(activity, "No Kata Messages App",
                    // Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(activity, ChatActivity.class);
                    i.putExtra("owtel_number", new Functions().getNumber());
                    i.putExtra("receiver", contactDto.getPhoneNumbers()
                            .get(position).getNumber());
                    activity.startActivity(i);
//                    Intent smsIntent = new Intent(
//                            android.content.Intent.ACTION_VIEW);
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//                    smsIntent.putExtra("address", contactDto.getPhoneNumbers()
//                            .get(position).getNumber());
//                    // smsIntent.putExtra("sms_body","message");
//                    activity.startActivity(smsIntent);
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static void integrationNumberTotheAdapter(Activity activity,
                                                     PhoneNumberDto phoneNumberDto) {

        try {

            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            List<ResolveInfo> resInfos = activity.getPackageManager()
                    .queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("JM Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);
                    if (packageName.contains(activity.getResources().getString(
                            R.string.messages_package))) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName,
                                resInfo.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra("MyKey",
                                "" + phoneNumberDto.getNumber());
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    System.out.println("Have Intent");
                    Intent chooserIntent = Intent
                            .createChooser(targetShareIntents.remove(0),
                                    "Choose app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                            targetShareIntents.toArray(new Parcelable[]{}));
                    activity.startActivity(chooserIntent);
                } else {

                    // System.out.println("Do not Have Intent");
                    // Toast.makeText(activity, "No Kata Messages App",
                    // Toast.LENGTH_SHORT).show();

//                    Intent smsIntent = new Intent(
//                            android.content.Intent.ACTION_VIEW);
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//                    smsIntent.putExtra("address", phoneNumberDto.getNumber());
//                    // smsIntent.putExtra("sms_body","message");
//                    activity.startActivity(smsIntent);


                    Intent i = new Intent(activity, ChatActivity.class);
                    i.putExtra("owtel_number", new Functions().getNumber());
                    i.putExtra("receiver", phoneNumberDto.getNumber());
                    activity.startActivity(i);
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    // number
    public static void integrationNumberInfo(Activity activity, String number) {

        try {
            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            List<ResolveInfo> resInfos = activity.getPackageManager()
                    .queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("JM Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);
                    if (packageName.contains(activity.getResources().getString(
                            R.string.messages_package))) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName,
                                resInfo.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra("MyKey", "" + number);
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    System.out.println("Have Intent");
                    Intent chooserIntent = Intent
                            .createChooser(targetShareIntents.remove(0),
                                    "Choose app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                            targetShareIntents.toArray(new Parcelable[]{}));
                    activity.startActivity(chooserIntent);
                } else {

                    // System.out.println("Do not Have Intent");
                    // Toast.makeText(activity, "No Kata Messages App",
                    // Toast.LENGTH_SHORT).show();

//                    Intent smsIntent = new Intent(
//                            android.content.Intent.ACTION_VIEW);
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//                    smsIntent.putExtra("address", number);
//                    // smsIntent.putExtra("sms_body","message");
//                    activity.startActivity(smsIntent);

                    Intent i = new Intent(activity, ChatActivity.class);
                    i.putExtra("owtel_number", new Functions().getNumber());
                    i.putExtra("receiver", number);
                    activity.startActivity(i);
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static void has_Number(String idString) {

        int idNum = Integer.valueOf(idString);

    }

    public static void integrationblockNumberFromMessagesApp(Activity activity,
                                                             boolean check, int IDnumber) {
        try {

            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            List<ResolveInfo> resInfos = activity.getPackageManager()
                    .queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("JM Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);
                    String packname = activity.getResources().getString(
                            R.string.messages_package);
                    // String packname = "com.example.samplereciverdata";
                    if (packageName.contains(packname)) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName,
                                resInfo.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra("KeyIDFromPhone", IDnumber);
                        intent.putExtra("KeyCheckBlock", check);
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                        activity.finish();
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    System.out.println("Have Intent");
                    Intent chooserIntent = Intent
                            .createChooser(targetShareIntents.remove(0),
                                    "Choose app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                            targetShareIntents.toArray(new Parcelable[]{}));
                    activity.startActivity(chooserIntent);
                } else {

                    System.out.println("Do not Have Intent");
                    // Toast.makeText(activity, "No Kata Messages App",
                    // Toast.LENGTH_SHORT).show();

                }
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static void disableSoftInputFromAppearing(EditText editText) {
        if (Build.VERSION.SDK_INT >= 11) {
            editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            editText.setTextIsSelectable(true);
        } else {
            editText.setRawInputType(InputType.TYPE_NULL);
            editText.setFocusable(true);
        }
    }

    public static void likedDoneListrefresh(boolean isEditing,
                                            Activity activity, ListView listView, Button editBtn,
                                            ImageView addFavoritesBtn, ArrayList<ContactDto> favoriteList,
                                            Fragment currentFragment) {

        isEditing = false;
        FavoritesAdapter adapter = new FavoritesAdapter(activity, favoriteList,
                currentFragment, isEditing);
        listView.setAdapter(adapter);
        editBtn.setText(activity.getResources().getString(R.string.edit_string));
        addFavoritesBtn.setVisibility(View.VISIBLE);
        // RelativeLayout.LayoutParams params = new
        // RelativeLayout.LayoutParams(100, 40);
        // params.setMargins(10,15,0,0);
        // editBtn.setLayoutParams(params);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
