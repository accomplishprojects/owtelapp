package com.katadigital.owtel.utils;

import java.util.Date;

public class CallHistory {

    String name;

    Date date;

    int count;

    boolean duplicate;

    public CallHistory(Date date, String name) {
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isDuplicate() {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }

    public static Integer compareUnixTime(CallHistory callHistory1, CallHistory callHistory2) {
        return Long.valueOf(callHistory2.getDate().getTime()/1000)
                .compareTo(callHistory1.getDate().getTime()/1000);
    }
}