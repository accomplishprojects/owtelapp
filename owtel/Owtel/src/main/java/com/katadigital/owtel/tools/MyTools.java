package com.katadigital.owtel.tools;

import android.media.ToneGenerator;

import java.util.Random;

/**
 * Created by josemari on 8/4/16.
 */
public class MyTools {
    public static boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }

    public static long generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }

    public static String getCategory(int drawCount) {
        if (drawCount == 3) {
            return "3D";
        } else if (drawCount == 4) {
            return "4D";
        } else {
            return "null";
        }
    }
    public static void generateTone(int tone) {
        ToneGenerator dtmfGenerator = new ToneGenerator(0, ToneGenerator.MAX_VOLUME);
        dtmfGenerator.startTone(tone, 1000); // all types of tones are available...
        dtmfGenerator.stopTone();
    }
    public static void stopRunningTone(int tone) {
        ToneGenerator dtmfGenerator = new ToneGenerator(0, ToneGenerator.MAX_VOLUME);
//        dtmfGenerator.startTone(tone, 1000); // all types of tones are available...
        dtmfGenerator.stopTone();
    }

    public static boolean intToBool(int input)
    {
        if (input < 0 || input > 1)
        {
            throw new IllegalArgumentException("input must be 0 or 1");
        }

        // Note we designate 1 as true and 0 as false though some may disagree
        return input == 1;
    }

}

