package com.katadigital.owtel.models;

/**
 * Created by dcnc123 on 11/16/16.
 */



import java.util.ArrayList;
        import java.util.List;

import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class PromoCodes {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("promo_code_list")
    @Expose
    private List<PromoCodeObj> promoCodeList = new ArrayList<PromoCodeObj>();

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The promoCodeList
     */
    public List<PromoCodeObj> getPromoCodeList() {
        return promoCodeList;
    }

    /**
     *
     * @param promoCodeList
     * The promo_code_list
     */
    public void setPromoCodeList(List<PromoCodeObj> promoCodeList) {
        this.promoCodeList = promoCodeList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
