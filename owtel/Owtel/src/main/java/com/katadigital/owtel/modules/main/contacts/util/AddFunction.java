package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Nickname;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.AddContactActivity;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.DateDto;
import com.katadigital.owtel.modules.main.contacts.entities.EmailDto;
import com.katadigital.owtel.modules.main.contacts.entities.IMDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.entities.RelationDto;
import com.katadigital.owtel.modules.main.contacts.entities.WebsiteDto;
import com.katadigital.portsip.utilities.converter.DataConverters;

public class AddFunction {

    AddContactActivity activity;

    public AddFunction(AddContactActivity activity) {
        this.activity = activity;
    }

    public ContactDto addFunction(ArrayList<ContentProviderOperation> ops,
                                  ContactDto contactDto) {

        String prefixNameText = activity.prefixNameText.getText().toString()
                .trim();
        String fNameText = activity.fNameText.getText().toString().trim();
        String phonetic_fNameText = activity.phonetic_fNameText.getText()
                .toString().trim();
        String middleNameText = activity.middleNameText.getText().toString()
                .trim();
        String phonetic_midNameText = activity.phonetic_midNameText.getText()
                .toString().trim();
        String lNameText = activity.lNameText.getText().toString().trim();
        String phonetic_lNameText = activity.phonetic_lNameText.getText()
                .toString().trim();
        String sufNameText = activity.sufNameText.getText().toString().trim();
        String nickNameText = activity.nickNameText.getText().toString();
        String jobTitleNameText = activity.jobTitleNameText.getText()
                .toString().trim();
        String jobDepNameText = activity.jobDepNameText.getText().toString()
                .trim();
        String companyName = activity.companyName.getText().toString().trim();

        String noteTxt = activity.noteTxt.getText().toString().trim();

        // contactDto.setDisplayName(String.valueOf(DisplayName));
        contactDto.setPrefix(String.valueOf(prefixNameText));
        contactDto.setFirstName(String.valueOf(fNameText));
        contactDto.setPhonetic_fName(String.valueOf(phonetic_fNameText));
        contactDto.setMiddleName(String.valueOf(middleNameText));
        contactDto.setPhonetic_middleName(String.valueOf(phonetic_midNameText));
        contactDto.setLastName(String.valueOf(lNameText));
        contactDto.setPhonetic_lName(String.valueOf(phonetic_lNameText));
        contactDto.setSuffix(String.valueOf(sufNameText));
        contactDto.setNickname(String.valueOf(nickNameText));
        contactDto.setJobTitle(String.valueOf(jobTitleNameText));
        contactDto.setJobDep(String.valueOf(jobDepNameText));
        contactDto.setCompany(String.valueOf(companyName));

        contactDto.setNote(noteTxt);

        ops.add(ContentProviderOperation
                .newInsert(RawContacts.CONTENT_URI)
                .withValue(RawContacts.ACCOUNT_TYPE,
                        contactDto.getAccountType())
                .withValue(RawContacts.ACCOUNT_NAME,
                        contactDto.getAccountName()).build());

        // ------------------------------------------------------ PHOTO
        if (contactDto.getProfilepic() != null) {
            ops.add(ContentProviderOperation
                    .newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Photo.CONTENT_ITEM_TYPE)
                    .withValue(Photo.PHOTO, DataConverters.convertBitmapToByteArray(contactDto.getProfilepic())
                            /*Util.getbitMapResize(contactDto.getProfilepic())*/)
                    .build());

        } else {
            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Photo.CONTENT_ITEM_TYPE)
                    .withValue(Photo.PHOTO, contactDto.getProfilepic()).build());
        }

        // Add Information Display Name
        ops.add(ContentProviderOperation
                .newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
                .withValue(StructuredName.PREFIX, contactDto.getPrefix())
                .withValue(StructuredName.GIVEN_NAME, contactDto.getFirstName())
                .withValue(StructuredName.MIDDLE_NAME,
                        contactDto.getMiddleName())
                .withValue(StructuredName.FAMILY_NAME, contactDto.getLastName())
                .withValue(StructuredName.PHONETIC_GIVEN_NAME,
                        contactDto.getPhonetic_fName())
                .withValue(StructuredName.PHONETIC_MIDDLE_NAME,
                        contactDto.getPhonetic_middleName())
                .withValue(StructuredName.PHONETIC_FAMILY_NAME,
                        contactDto.getPhonetic_lName())
                .withValue(StructuredName.SUFFIX, contactDto.getSuffix())
                .build());

        // ------------------------------------------------------ Nick Name
        if (contactDto.getNickname() != null) {
            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Nickname.CONTENT_ITEM_TYPE)
                    .withValue(Nickname.NAME, contactDto.getNickname())
                    .withValue(Nickname.TYPE, Nickname.TYPE_SHORT_NAME).build());
        }

        // ------------------------------------------------------ Organization
        ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, Organization.CONTENT_ITEM_TYPE)
                .withValue(Organization.COMPANY, contactDto.getCompany())
                .withValue(Organization.TYPE, Organization.TYPE_WORK)
                .withValue(Organization.TITLE, contactDto.getJobTitle())
                .withValue(Organization.TYPE, Organization.TYPE_CUSTOM)
                .withValue(Organization.DEPARTMENT, contactDto.getJobDep())
                .withValue(Organization.TYPE, Organization.TYPE_CUSTOM).build());
        // }

        // ------------------------------------------------------ NOTE
        ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, Note.CONTENT_ITEM_TYPE)
                .withValue(Note.NOTE, contactDto.getNote()).build());

        // ------------------------------------------------------ BDAY
        ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, Event.CONTENT_ITEM_TYPE)
                .withValue(Event.START_DATE, contactDto.getBirthday())
                .withValue(Event.TYPE, Event.TYPE_BIRTHDAY).build());

        // ------------------------------------------------------ Mobile
        // MY CUSTOM ITO DITO FIELD
        for (PhoneNumberDto num : contactDto.getPhoneNumbers()) {

            int phoneTypeIndex = 0;
            String customName = " ";
            if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_mobile))) {
                phoneTypeIndex = Phone.TYPE_MOBILE;
            } else if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_work))) {
                phoneTypeIndex = Phone.TYPE_WORK;
            } else if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_home))) {
                phoneTypeIndex = Phone.TYPE_HOME;
            } else if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_main))) {
                phoneTypeIndex = Phone.TYPE_MAIN;
            } else if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_workfax))) {
                phoneTypeIndex = Phone.TYPE_FAX_WORK;
            } else if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_homefax))) {
                phoneTypeIndex = Phone.TYPE_FAX_HOME;
            } else if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_pager))) {
                phoneTypeIndex = Phone.TYPE_PAGER;
            } else if (num.getNumberType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_other))) {
                phoneTypeIndex = Phone.TYPE_OTHER;
            } else {
                phoneTypeIndex = Phone.TYPE_CUSTOM;
                customName = num.getNumberType();
            }

            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
                    .withValue(Phone.NUMBER, num.getNumber())
                    .withValue(Phone.LABEL, customName)
                    .withValue(Phone.TYPE, phoneTypeIndex).build());

        }

        // ------------------------------------------------------ Email // NOTE
        // MY CUSTOM ITO DITO FIELD
        for (EmailDto email : contactDto.getEmails()) {

            int emailTypeIndex = 0;
            String customName = " ";

            if (email.getEmailType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_work))) {
                emailTypeIndex = Email.TYPE_WORK;
            } else if (email.getEmailType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_home))) {
                emailTypeIndex = Email.TYPE_HOME;
            } else if (email.getEmailType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_other))) {
                emailTypeIndex = Email.TYPE_OTHER;
            } else {
                emailTypeIndex = Phone.TYPE_CUSTOM;
                customName = email.getEmailType();
            }

            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE)
                    .withValue(Email.DATA, email.getEmail())
                    .withValue(Email.LABEL, customName)
                    .withValue(Email.TYPE, emailTypeIndex).build());
        }

        // ------------------------------------------------------ Address
        for (AddressDto add : contactDto.getAddresses()) {

            int addTypeIndex = 0;
            if (add.getAddressType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_work))) {
                addTypeIndex = StructuredPostal.TYPE_WORK;
            } else if (add.getAddressType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_home))) {
                addTypeIndex = StructuredPostal.TYPE_HOME;
            } else if (add.getAddressType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_other))) {
                addTypeIndex = StructuredPostal.TYPE_OTHER;
            }

            if (add.getStreetStr().equals("") || add.getStreetStr() == null) {
                add.setStreetStr(" ");
            }

            if (add.getNeigborhoodStr().equals("")
                    || add.getStreetStr() == null) {
                add.setNeigborhoodStr(" ");
            }
            if (add.getCityStr().equals("") || add.getStreetStr() == null) {
                add.setCityStr(" ");
            }

            if (add.getZipCodeStr().equals("") || add.getStreetStr() == null) {
                add.setZipCodeStr(" ");
            }

            ops.add(ContentProviderOperation
                    .newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE,
                            StructuredPostal.CONTENT_ITEM_TYPE)
                    .withValue(StructuredPostal.STREET, add.getStreetStr())
                    .withValue(StructuredPostal.NEIGHBORHOOD,
                            add.getNeigborhoodStr())
                    .withValue(StructuredPostal.CITY, add.getCityStr())
                    .withValue(StructuredPostal.COUNTRY, add.getCountryStr())
                    .withValue(StructuredPostal.POSTCODE, add.getZipCodeStr())
                    .withValue(StructuredPostal.TYPE, addTypeIndex).build());

        }

        // ------------------------------------------------------ Websites

        for (WebsiteDto websites : contactDto.getWebsites()) {

            int websiteTypeIndex = 0;

            if (websites.getWebsiteType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_homepage))) {
                websiteTypeIndex = Website.TYPE_HOMEPAGE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_blog))) {
                websiteTypeIndex = Website.TYPE_BLOG;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_profile))) {
                websiteTypeIndex = Website.TYPE_PROFILE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_home))) {
                websiteTypeIndex = Website.TYPE_HOME;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_work))) {
                websiteTypeIndex = Website.TYPE_HOMEPAGE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_array_ftp))) {
                websiteTypeIndex = Website.TYPE_FTP;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_other))) {
                websiteTypeIndex = Website.TYPE_WORK;
            }

            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE)
                    .withValue(Website.URL, websites.getWebsite())
                    .withValue(Website.TYPE, websiteTypeIndex).build());

        }

        // ------------------------------------------------------ Dates

        for (DateDto dateDto : contactDto.getDateEvent()) {

            int dateTypeIndex = 0;

            if (dateDto.getDatesType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_aniv))) {
                dateTypeIndex = Event.TYPE_ANNIVERSARY;
            } else if (dateDto.getDatesType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_array_other))) {
                dateTypeIndex = Event.TYPE_OTHER;
            }

            // else if (dateDto.getDatesType().equalsIgnoreCase(
            // activity.getResources().getString(
            // R.string.string_array_custom))) {
            // dateTypeIndex = Event.TYPE_CUSTOM;
            // }

            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Event.CONTENT_ITEM_TYPE)
                    .withValue(Event.START_DATE, dateDto.getDates())
                    .withValue(Event.TYPE, dateTypeIndex).build());

        }

        // ------------------------------------------------------ Dates

        for (RelationDto relation : contactDto.getRelationDto()) {

            int relaTypeIndex = 0;

            if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation0))) {
                relaTypeIndex = Relation.TYPE_ASSISTANT;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation1))) {
                relaTypeIndex = Relation.TYPE_BROTHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation2))) {
                relaTypeIndex = Relation.TYPE_CHILD;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation3))) {
                relaTypeIndex = Relation.TYPE_DOMESTIC_PARTNER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation4))) {
                relaTypeIndex = Relation.TYPE_FATHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation5))) {
                relaTypeIndex = Relation.TYPE_FRIEND;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation6))) {
                relaTypeIndex = Relation.TYPE_MANAGER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation7))) {
                relaTypeIndex = Relation.TYPE_MOTHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation8))) {
                relaTypeIndex = Relation.TYPE_PARENT;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources()
                            .getString(R.string.string_relation9))) {
                relaTypeIndex = Relation.TYPE_PARTNER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_relation10))) {
                relaTypeIndex = Relation.TYPE_REFERRED_BY;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_relation11))) {
                relaTypeIndex = Relation.TYPE_RELATIVE;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_relation12))) {
                relaTypeIndex = Relation.TYPE_SISTER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    activity.getResources().getString(
                            R.string.string_relation13))) {
                relaTypeIndex = Relation.TYPE_SPOUSE;
            } else {
                relaTypeIndex = Relation.TYPE_CUSTOM;
            }

            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Relation.CONTENT_ITEM_TYPE)
                    .withValue(Relation.DATA, relation.getRelationType())
                    .withValue(Relation.NAME, relation.getRelationName())
                    .withValue(Relation.TYPE, relaTypeIndex).build());

        }

        // ------------------------------------------------------ IM

        for (IMDto imDto : contactDto.getImField()) {

            System.out.println("JM IM " + imDto.getIm());

            int imTypeIndex = 0;

            if (imDto.getImProtocol().equalsIgnoreCase("AIM")) {
                imTypeIndex = Im.PROTOCOL_AIM;
            } else if (imDto.getImProtocol().equalsIgnoreCase("MSN")) {
                imTypeIndex = Im.PROTOCOL_MSN;
            } else if (imDto.getImProtocol().equalsIgnoreCase("YAHOO")) {
                imTypeIndex = Im.PROTOCOL_YAHOO;
            } else if (imDto.getImProtocol().equalsIgnoreCase("QQ")) {
                imTypeIndex = Im.PROTOCOL_SKYPE;
            } else if (imDto.getImProtocol().equalsIgnoreCase("GOOGLE TALK")) {
                imTypeIndex = Im.PROTOCOL_GOOGLE_TALK;
            } else if (imDto.getImProtocol().equalsIgnoreCase("ICQ")) {
                imTypeIndex = Im.PROTOCOL_ICQ;
            } else if (imDto.getImProtocol().equalsIgnoreCase("JABBER")) {
                imTypeIndex = Im.PROTOCOL_JABBER;
            } else if (imDto.getImProtocol().equalsIgnoreCase("NETMEETING")) {
                imTypeIndex = Im.PROTOCOL_NETMEETING;
            }

            //
            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                    .withValue(Data.MIMETYPE, Im.CONTENT_ITEM_TYPE)
                    .withValue(Im.DATA, imDto.getIm())
                    .withValue(Im.PROTOCOL, imTypeIndex).build());

        }

        return contactDto;

    }
}
