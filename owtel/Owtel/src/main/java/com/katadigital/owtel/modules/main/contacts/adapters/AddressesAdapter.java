package com.katadigital.owtel.modules.main.contacts.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

public class AddressesAdapter extends BaseAdapter {
    private Activity context;
    private ArrayList<AddressDto> addresses;

    public AddressesAdapter(Activity context, ArrayList<AddressDto> addresses) {

        super();
        this.context = context;

        this.addresses = new ArrayList<AddressDto>();
        this.addresses.addAll(addresses);
    }

    @Override
    public int getCount() {
        return addresses.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.address_listitem, viewGroup, false);
        TextView addressType = (TextView) rowView
                .findViewById(R.id.detail_address_type);
        TextView addressDetails = (TextView) rowView
                .findViewById(R.id.detail_address);

        addressType.setTypeface(NewUtil.getFontRoman(context));
        addressDetails.setTypeface(NewUtil.getFontRoman(context));

        addressType.setTextSize(NewUtil.gettxtSize());
        addressDetails.setTextSize(NewUtil.gettxtSize());

        if (addresses.get(i) != null) {
            AddressDto addressDto = addresses.get(i);
            addressType.setText(addressDto.getAddressType());
            addressDetails.setText((addressDto.getStreetStr().isEmpty()
                    || addressDto.getStreetStr().equals(" ") ? "" : addressDto
                    .getStreetStr() + "\n")

                    + (addressDto.getNeigborhoodStr().isEmpty()
                    || addressDto.getNeigborhoodStr().equals(" ") ? ""
                    : addressDto.getNeigborhoodStr() + "\n")

                    + (addressDto.getCityStr().isEmpty()
                    || addressDto.getCityStr().equals(" ") ? ""
                    : addressDto.getCityStr() + "\n")

                    + (addressDto.getCountryStr().isEmpty() ? "" : addressDto
                    .getCountryStr() + "\n")

                    + (addressDto.getZipCodeStr().isEmpty()
                    || addressDto.getZipCodeStr().equals(" ") ? ""
                    : addressDto.getZipCodeStr() + "\n")

            );
        }

        return rowView;
    }
}
