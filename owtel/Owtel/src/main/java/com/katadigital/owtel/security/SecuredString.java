package com.katadigital.owtel.security;

import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

import java.io.UnsupportedEncodingException;

/**
 * Created by Omar Matthew Reyes on 4/18/16.
 *
 * Source: http://stackoverflow.com/questions/7360403/base-64-encode-and-decode-example-code
 */
public class SecuredString {
    private static final String TAG = "SecuredString";

    /**
     * Encode string text to Base64
     * @param text String
     * @return String
     */
    @Nullable
    public static String base64Encode(String text) {
        try {
            byte[] data = text.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            if (GlobalValues.DEBUG) Log.e(TAG, "base64Encode " + e);
            return null;
        }
    }

    /**
     * Decode Base64 byte[] to readable string
     * @param base64 byte[]
     * @return String
     */
    @Nullable
    public static String base64Decode(byte[] base64){
        try{
            byte[] data = Base64.decode(base64, Base64.DEFAULT);
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e){
            if (GlobalValues.DEBUG) Log.e(TAG, "base64Decode " + e);
            return null;
        }
    }

    /**
     * Decode Base64 byte[] to readable string
     * @param base64 byte[]
     * @return String
     */
    @Nullable
    public static String base64Decode(String base64){
        try{
            byte[] data = Base64.decode(base64, Base64.DEFAULT);
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e){
            if (GlobalValues.DEBUG) Log.e(TAG, "base64Decode " + e);
            return null;
        }
    }

    /**
     * Encrypt data using default iv and secret key
     * @param text String
     * @return String
     */
    @Nullable
    public static String encryptData(String text){
        String mCryptedToHex;
        try {
            byte[] encodedMCryptText = new MCrypt().encrypt(text);
            mCryptedToHex = MCrypt.bytesToHex(encodedMCryptText);
            return SecuredString.base64Encode(mCryptedToHex);
        } catch (Exception e) {
            if(GlobalValues.DEBUG) Log.e(TAG, "MCrypt encrypt " + e);
            return null;
        }
    }

    /**
     * Encrypt data using modified iv and secret key
     * @param text String
     * @return String
     */
    @Nullable
    public static String encryptData(String text, String iv, String secretKey){
        String mCryptedToHex;
        try {
            byte[] encodedMCryptText = new MCrypt(iv, secretKey).encrypt(text);
            mCryptedToHex = MCrypt.bytesToHex(encodedMCryptText);
            return SecuredString.base64Encode(mCryptedToHex);
        } catch (Exception e) {
            if(GlobalValues.DEBUG) Log.e(TAG, "MCrypt encrypt " + e);
            return null;
        }
    }

    /**
     * Decrypt text using default iv and secret key
     * @param encryptedText String
     * @return String
     */
    public static String decryptData(String encryptedText){
        if (encryptedText != null) {
            final String base64Decoded = SecuredString.base64Decode(encryptedText);
            try {
                byte[] decodedMCryptText = new MCrypt().decrypt(base64Decoded);
                return new String(decodedMCryptText);
            } catch (Exception e) {
                if(GlobalValues.DEBUG) Log.e(TAG, "MCrypt decrypt " + e);
                return null;
            }
        } else return null;
    }

    /**
     * Decrypt text using modified iv and secret key
     * @param encryptedText String
     * @param iv String
     * @param secretKey String
     * @return String
     */
    public static String decryptData(String encryptedText, String iv, String secretKey){
        if (encryptedText != null) {
            final String base64Decoded = SecuredString.base64Decode(encryptedText);
            try {
                byte[] decodedMCryptText = new MCrypt(iv, secretKey).decrypt(base64Decoded);
                return new String(decodedMCryptText);
            } catch (Exception e) {
                if(GlobalValues.DEBUG) Log.e(TAG, "MCrypt decrypt " + e);
                return null;
            }
        } else return null;
    }
}
