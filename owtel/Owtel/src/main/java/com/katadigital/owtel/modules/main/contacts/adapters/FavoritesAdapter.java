package com.katadigital.owtel.modules.main.contacts.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.favorites.FavoritesFragment;
import com.katadigital.owtel.helpers.AlertDialogHelper;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Util;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.portsip.utilities.ui.image.ImageCustomManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FavoritesAdapter extends BaseAdapter {

    private Activity context;

    public static boolean editIsPressed = false;

    View rowView;

    ArrayList<ContactDto> favoriteList;
    Fragment fragment;
    Dialog selectNumberDialog;
    private ProgressDialog progressDialog;
    QuickContactHelper quickContactHelper;
    AlertDialogHelper alertDialog;

    public FavoritesAdapter(Activity context,
                            ArrayList<ContactDto> favoriteList, Fragment fragment,
                            boolean editpress) {
        super();
        ImageCustomManager.init();

        this.context = context;
        this.favoriteList = favoriteList;
        this.fragment = fragment;
        editIsPressed = editpress;
        alertDialog = new AlertDialogHelper();
    }

    public void updateResults(ArrayList<String> results) {

        // Triggers the list update
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return favoriteList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.favor_custom_list, viewGroup, false);
        TextView name = (TextView) rowView.findViewById(R.id.favor_item_textview);
        TextView numberType = (TextView) rowView.findViewById(R.id.favor_number_type);
        TextView lettericon = (TextView) rowView.findViewById(R.id.letter_icon);
        CircleImageView contactProfile = (CircleImageView) rowView.findViewById(R.id.logs_item_imageview);
        final ContactDto contactDto = favoriteList.get(i);
        final Button deleteBtn = (Button) rowView.findViewById(R.id.fav_item_delete_btn);

        contactProfile.setVisibility(View.VISIBLE);
        lettericon.setVisibility(View.GONE);

        lettericon.setTypeface(NewUtil.getFontRoman(context));
        lettericon.setTextSize(NewUtil.gettxtSize());

        numberType.setTypeface(NewUtil.getFontRoman(context));
        numberType.setTextSize(NewUtil.gettxtSize());
        numberType.setTextColor(context.getResources().getColor(
                R.color.gray_notes));

        name.setTypeface(NewUtil.getFontRoman(context));
        name.setTextSize(NewUtil.gettxtSize());

        if (contactDto.getProfilepic() != null) {

            /**Displaying profile picture*/
            ImageCustomManager.getImageCustomManagerInstance()
                    .displayProfilePictureFromBitmap(context, contactDto.getProfilepic(), contactProfile);

        } else {
            try {

                if (contactDto.getFirstName().equals("")
                        && contactDto.getLastName().equals("")) {
                    // && contactDto.getPhoneNumbers().size() > 0) {
                    contactProfile
                            .setImageResource(R.drawable.profile_pic_holder);
                } else if ((!contactDto.getFirstName().equals("") || !contactDto
                        .getLastName().equals(""))) {
                    // && contactDto.getPhoneNumbers().size() > 0) {
                    String iniFName = "", iniLName = "";

                    contactProfile.setVisibility(View.GONE);
                    lettericon.setVisibility(View.VISIBLE);

                    if (!(contactDto.getFirstName() == null)) {

                        if (!contactDto.getFirstName().equals("")) {
                            iniFName = contactDto.getFirstName()
                                    .substring(0, 1).toUpperCase();
                        } else {
                            iniFName = "";
                        }

                    } else
                        iniFName = "";
                    if (!(contactDto.getLastName() == null)) {
                        if (!contactDto.getLastName().equals("")) {
                            iniLName = contactDto.getLastName().substring(0, 1)
                                    .toUpperCase();
                        } else {
                            iniLName = "";
                        }

                    } else
                        iniLName = "";
                    lettericon.setText(iniFName + iniLName);

                }

            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        if (contactDto != null) {

            System.out.println("BERTAX display name -"
                    + contactDto.getDisplayName());

            if (contactDto.getDisplayName().trim().equals("")
                    || contactDto.getDisplayName().isEmpty()) {

                if (contactDto.getFirstName() != null) {

                    if (contactDto.getFirstName().trim().equals("")
                            && contactDto.getLastName().trim().equals("")) {
                        // nickname
                        if (contactDto.getNickname().trim().equals("")
                                || contactDto.getNickname().isEmpty()) {
                            name.setText(context.getResources().getString(
                                    R.string.string_noname));
                        } else {
                            name.setText(contactDto.getNickname());
                        }

                    } else if ((!contactDto.getFirstName().equals("") || !contactDto
                            .getLastName().equals(""))) {
                        String iniFName = "", iniLName = "";
                        if (!(contactDto.getFirstName() == null)) {

                            if (!contactDto.getFirstName().equals("")) {
                                iniFName = contactDto.getFirstName();
                            } else {
                                iniFName = "";
                            }

                        } else
                            iniFName = "";

                        if (!(contactDto.getLastName() == null)) {
                            if (!contactDto.getLastName().equals("")) {
                                iniLName = contactDto.getLastName();
                            } else {
                                iniLName = "";
                            }

                        } else
                            iniLName = "";

                        name.setText(iniFName + " " + iniLName);

                    }
                }

            } else {
                name.setText(contactDto.getDisplayName());

            }

            if (contactDto.getPhoneNumbers().size() > 0) {

                for (PhoneNumberDto phoneNumberDto : contactDto
                        .getPhoneNumbers()) {
                    if (!phoneNumberDto.getNumber().isEmpty()) {
                        numberType.setText(phoneNumberDto.getNumberType());
                        break;
                    }
                }

            }

            Button faveInfoBtn = (Button) rowView
                    .findViewById(R.id.favor_item_info_btn);
            faveInfoBtn.setBackgroundDrawable(context.getResources()
                    .getDrawable(R.drawable.information));

            faveInfoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new loadContactDetails(contactDto)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            });

            View faveInfo = rowView.findViewById(R.id.favor_item_info);
            faveInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new loadContactDetails(contactDto)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            });

            if (editIsPressed == true) {

                faveInfoBtn.setVisibility(View.GONE);
                LinearLayout myLayout = (LinearLayout) rowView
                        .findViewById(R.id.favor_delete_btn_layout);

                Button myButton = new Button(context);
                myButton.setLayoutParams(new LinearLayout.LayoutParams(
                        (int) context.getResources().getDimension(
                                R.dimen.info_btn_size), (int) context
                        .getResources().getDimension(
                                R.dimen.info_btn_size)));
                myButton.setBackgroundDrawable(context.getResources()
                        .getDrawable(R.drawable.delete_item_img));
                myLayout.addView(myButton);

                myButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteBtn.setVisibility(View.VISIBLE);
                        view.setVisibility(View.GONE);

                    }
                });

                deleteBtn.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ContentValues values = new ContentValues();

                        // String[] fv = new String[] { contactDto
                        // .getDisplayName() };
                        String[] fv = new String[]{contactDto.getContactID()};
                        values.put(ContactsContract.Contacts.STARRED, 0);
                        context.getContentResolver().update(
                                ContactsContract.Contacts.CONTENT_URI, values,
                                ContactsContract.Contacts._ID + "= ?", fv);
                        favoriteList.remove(i);
                        FavoritesAdapter.this.notifyDataSetChanged();
                        FavoritesAdapter.this.notifyDataSetInvalidated();
                        // viewGroup.removeViewInLayout(rowView);

                        if (favoriteList.size() == 0) {
                            ((FavoritesFragment) fragment)
                                    .getFavoriteContacts(false);
                        }
                    }
                });
            }

            View listItem = rowView.findViewById(R.id.favor_list_item);
            listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    quickContactHelper = new QuickContactHelper(context);
                    ContactDto contactDtos = quickContactHelper
                            .getContactDetails(contactDto.getContactID());
                    Util.addDialogSendMessage(context, contactDtos, "call");
                }
            });
        }
        return rowView;
    }

    public void openCallDialog(final ContactDto selectedContactDto) {
        LayoutInflater inflater = (LayoutInflater) ((MainActivity) context)
                .getLayoutInflater();
        PhoneNumbersDialogAdapter numbersAdapter = new PhoneNumbersDialogAdapter(
                ((MainActivity) context), selectedContactDto.getPhoneNumbers());
        View customView = inflater.inflate(R.layout.numberlist_dialog, null);

        ListView numberlist = (ListView) customView
                .findViewById(R.id.numberlist);
        numberlist.setAdapter(numbersAdapter);
        numberlist.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView,
                                    int myItemInt, long mylng) {
                if (!selectedContactDto.getPhoneNumbers().get(myItemInt)
                        .getNumber().isEmpty()) {
                    selectNumberDialog.dismiss();
//					context.startActivity(QuickContactHelper
//							.initiateDefaultCall(context, selectedContactDto
//									.getPhoneNumbers().get(myItemInt)
//									.getNumber()));
                    QuickContactHelper.initiateSipCall(context, selectedContactDto
                            .getPhoneNumbers().get(myItemInt)
                            .getNumber());

                } else {
                    Toast.makeText(context, "Number is empty!", Toast.LENGTH_LONG).show();
                }

            }
        });

        // Build the dialog
        selectNumberDialog = new Dialog(context);
        // selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectNumberDialog.setContentView(customView);
        selectNumberDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        selectNumberDialog.setTitle("CALL CONTACT");

        selectNumberDialog.show();
    }

    class loadContactDetails extends AsyncTask<String, String, String> {
        ContactDto contactDto;

        public loadContactDetails(ContactDto contactDto) {
            this.contactDto = contactDto;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(
                    context,
                    context.getResources().getString(
                            R.string.string_diag_pleasewait),
                    context.getResources().getString(
                            R.string.string_diag_loading_details)
                            + "...");
            progressDialog.setCancelable(false);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            if (contactDto != null) {
                quickContactHelper = new QuickContactHelper(context);
                ContactDetailActivity.contactDto = quickContactHelper
                        .getContactDetails(contactDto.getContactID() + "");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            Intent intent = new Intent(context, ContactDetailActivity.class);
            context.startActivity(intent);
            Bundle bundleObject = new Bundle();
            ContactDetailActivity.favTitle = context.getResources().getString(
                    R.string.tab_liked);
            context.overridePendingTransition(
                    R.anim.slide_in_up, R.anim.slide_in_up_exit);

            super.onPostExecute(result);
        }
    }
}
