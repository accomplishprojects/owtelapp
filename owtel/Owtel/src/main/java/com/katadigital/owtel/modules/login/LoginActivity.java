package com.katadigital.owtel.modules.login;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.DaoSession;
import com.katadigital.owtel.models.LoginObj;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity;
import com.katadigital.owtel.modules.registration.RegisterFragment;
import com.katadigital.owtel.modules.resetaccess.ForgotPasswordFragment;
import com.katadigital.owtel.security.PermissionChecker;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.utilities.permissions.PermissionManager;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.katadigital.portsip.views.PortSipCallScreenActivity;
import com.scottyab.aescrypt.AESCrypt;
import com.securepreferences.SecurePreferences;
import com.squareup.okhttp.OkHttpClient;

import java.security.GeneralSecurityException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class LoginActivity extends AppCompatActivity implements MainActivityBridge {
    public static String TAG = "LoginActivity";

    @Bind(R.id.btn_login)
    ImageButton btnLogin;

    @Bind(R.id.et_email)
    EditText etEmail;

    @Bind(R.id.et_password)
    EditText etPassword;

    @Bind(R.id.btn_register)
    Button btnRegister;

    @Bind(R.id.tv_app_name)
    TextView tvAppName;

    @Bind(R.id.tv_email_required)
    TextView tvEmailNotice;

    @Bind(R.id.tv_pass_required)
    TextView tvPassNotice;

    @Bind(R.id.layout_progress_login)
    View layoutProgress;

    @Inject
    ApiService apiService;

    @Inject
    DaoSession daoSession;

    CheckBox cbRememberUser;

    //    ProgressDialog mProgressDialog;
    SharedPreferences preferencesUser;
    public static boolean isAutoLogOut = false;
    OwtelAppController appController;

    TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView tv, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE && tv == etPassword) {
                if (isLoginCredentialsValid()) {
                    doLoginRequest(etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
                }
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        SharedPreferenceManager.init(this);
        PermissionManager.init();

        appController = (OwtelAppController) getApplicationContext();
        preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();

        if (appController.getChronometerStatus()) {
            startActivity(new Intent(this, PortSipCallScreenActivity.class)
                    .setAction(""));
        }
        sendBroadcast(new Intent().setAction("com.katadigital.owtel.modules.login"));
        ButterKnife.bind(this);
        SharedPreferenceManager.init(this);
        OwtelAppController.getComponent(this).inject(this);
        statusBarTransparent();

        cbRememberUser = (CheckBox) findViewById(R.id.chRememberUser);
        cbRememberUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean status) {
                SharedPreferenceManager.getSharedPreferenceManagerInstance().putUserRememberStatus(status);
            }
        });
        tvAppName.setTypeface(NewUtil.getFontBauhaus(this));
//        new PermissionChecker(this).checkPermissions();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent getReceivedMessage = getIntent();
        if (getReceivedMessage != null) {
            switch (getReceivedMessage.getAction()) {
                case Intent.ACTION_SEND:
                    String sharedMessage = getReceivedMessage.getStringExtra(Intent.EXTRA_TEXT);
//                Toast.makeText(this, sharedMessage, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, ChatActivity.class)
                            .putExtra("SharedMessage", sharedMessage));
                    break;
                default:
                    if (PermissionManager.getPermissionManagerInstance().checkPermissions(this)) {
                        if (preferencesUser == null) {
                            preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
                        } else {
                            if (!preferencesUser.getString(Constants.PREFS_USER_EMAIL, "").trim().isEmpty() &&
                                    !preferencesUser.getString(Constants.PREFS_USER_PASS, "").trim().isEmpty()) {
                                doLoginRequest(preferencesUser.getString(Constants.PREFS_USER_EMAIL, ""),
                                        preferencesUser.getString(Constants.PREFS_USER_PASS, ""));
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean remember = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .getUserRememberStatus();
        cbRememberUser.setChecked(remember);
        if (SharedPreferenceManager.getSharedPreferenceManagerInstance().getUserRememberStatus()) {
            String cachedUsername = SharedPreferenceManager.getSharedPreferenceManagerInstance().getUsername();
            String cachedPassword = SharedPreferenceManager.getSharedPreferenceManagerInstance().getPassword();
            etEmail.setText(cachedUsername);
            etPassword.setText(cachedPassword);
        }

        if (isAutoLogOut) {
            DialogBuilder.showAlertDialog(this,
                    getString(R.string.logout), getString(R.string.logout_session_expired_dialog_notice),
                    getString(R.string.string_ok), false, context1 -> Log.e(TAG, "onResume: "
                            + "Registered to other device"));
        }
        if (tvEmailNotice != null && tvPassNotice != null) {
            tvEmailNotice.setVisibility(View.GONE);
            tvPassNotice.setVisibility(View.GONE);
        }
        isAutoLogOut = false;
    }

    @OnClick(R.id.btn_forgot_pass)
    void onClickForgotPass() {
        Intent forgotPass = new Intent(LoginActivity.this, FragmentHolderActivity.class);
        forgotPass.putExtra(FragmentHolderActivity.TAG, ForgotPasswordFragment.TAG);
        startActivity(forgotPass);
        overridePendingTransition(0, 0);
    }


    @OnClick(R.id.btn_register)
    public void registerBtnClicked() {
        if (etEmail.getText() != null) {
            String email = etEmail.getText().toString();
            Intent register = new Intent(LoginActivity.this, FragmentHolderActivity.class);
            register.putExtra(Constants.USER_CREDENTIALS, email);
            register.putExtra(FragmentHolderActivity.TAG, RegisterFragment.TAG);
            startActivity(register);

        } else {
            Log.e(TAG, "Null email");
        }
    }

    private void inflateRegistration(Bundle bundle) {
        Intent register = new Intent(LoginActivity.this, FragmentHolderActivity.class);
        register.putExtra(FragmentHolderActivity.TAG, RegisterFragment.TAG);
        register.putExtra(Constants.USER_CREDENTIALS, bundle);
        startActivity(register);
//        overridePendingTransition(0, 0);
    }

    @OnClick(R.id.btn_login)
    public void loginBtnClicked() {
        if (PermissionManager.getPermissionManagerInstance().checkPermissions(this)) {
            if (isLoginCredentialsValid()) {
                doLoginRequest(etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
            }
        }

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void statusBarTransparent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * Check if Login (email, password) input is valid
     *
     * @return boolean
     */
    private boolean isLoginCredentialsValid() {
        boolean valid = true;
        if (etEmail.getText().toString().trim().isEmpty()) {
//            tvEmailNotice.setVisibility(View.VISIBLE);
            etEmail.setError("Please enter email");
            valid = false;
        } else tvEmailNotice.setVisibility(View.GONE);

        if (etPassword.getText().toString().trim().isEmpty()) {
//            tvPassNotice.setVisibility(View.VISIBLE);
            etPassword.setError("Please enter password");
            valid = false;
        } else tvPassNotice.setVisibility(View.GONE);

        return valid;
    }

    /**
     * Set email password on text fields
     *
     * @param email    String
     * @param password String
     */
    private void setEmailPassword(String email, String password) {
        etEmail.setText(email);
        etPassword.setText(password);
    }

    /**
     * Initiate Login Request
     * <p>
     * Success:
     * Prompt to MainActivity.class
     * Save credentials using <a href="https://github.com/scottyab/secure-preferences/">SecurePreferences</a>
     * <p>
     * Fail:
     * Show dialog
     *
     * @param email    String
     * @param password String
     */
    private void doLoginRequest(String email, String password) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutProgress.setVisibility(View.VISIBLE);

                Retrofit retrofit = new Retrofit.Builder().baseUrl(API.URL_TEST)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(getRequestHeader())
                        .build();
                ApiService service = retrofit.create(ApiService.class);
                service.doLoginRequest(email, password).enqueue(new Callback<LoginObj>() {
                    @Override
                    public void onResponse(Response<LoginObj> response, Retrofit retrofit) {
                        if (response.body().getStatus().equals("Success")) {
                            // Encrypted Preference storage
                            String key = email + password;
                            SecurePreferences securePreferences = OwtelAppController.getInstance()
                                    .getUserPinBasedSharedPreferences(key);

                            SecurePreferences.Editor secureEditor = securePreferences.edit();
                            secureEditor.putString(Constants.PREFS_USER_NUMBER, response.body().getSipNumber());
                            secureEditor.putString(Constants.PREFS_SIP_PASS, response.body().getSipPassword());
                            secureEditor.putString(Constants.PREFS_SIP_SERVER, response.body().getSipServer());
                            secureEditor.putString(Constants.PREFS_SIP_PORT, response.body().getSipPort());
                            secureEditor.putString(Constants.PREFS_RAD_ID, response.body().getRadSessionId());

                            // FIXME: 2/23/17 MY FIXES TESTER
                            SharedPreferences.Editor editor = getSharedPreferences(Constants.PREFS_RAD_NAME, MODE_PRIVATE).edit();
                            editor.putString(Constants.PREFS_RAD_ID, response.body().getRadSessionId());
                            editor.commit();
                            Log.i(TAG, "RAD_ID doLogin " + response.body().getRadSessionId());
                            secureEditor.apply();

                            SharedPreferences.Editor defaultPreferenceEditor = preferencesUser.edit();
                            defaultPreferenceEditor.putString(Constants.PREFS_USER_EMAIL, email);
                            defaultPreferenceEditor.putString(Constants.PREFS_USER_PASS, password);
                            defaultPreferenceEditor.putString(Constants.PREFS_PROMO_CODE, response.body().getPromoCode());
                            defaultPreferenceEditor.putBoolean(Constants.PREFS_USER_TYPE, response.body().getPromoCode().equals(""));
                            defaultPreferenceEditor.putString(Constants.PREFS_USER_SUBSCRIPTION_EXP, response.body().getExpirationDate());
//                            defaultPreferenceEditor.putFloat(Constants.PREFS_USER_IDD_CREDITS, (float) Double.parseDouble(response.body().getIddCredits()));
//                            SharedPreferenceHelper.putDouble(defaultPreferenceEditor, Constants.PREFS_USER_IDD_CREDITS, Double.parseDouble(response.body().getIddCredits()));
                            defaultPreferenceEditor.apply();

                            if (GlobalValues.DEBUG)
                                if (!key.isEmpty()) {
                                    Log.i(TAG, "Stored Secured Email " + securePreferences.getString(Constants.PREFS_USER_NUMBER, "empty email"));
                                    Log.i(TAG, "Stored Secured Password " + securePreferences.getString(Constants.PREFS_SIP_PASS, "empty pass"));
                                    Log.i(TAG, "Stored Secured SIP Server " + securePreferences.getString(Constants.PREFS_SIP_SERVER, "empty SIP Server"));
                                    Log.i(TAG, "Stored Secured SIP Port " + securePreferences.getString(Constants.PREFS_SIP_PORT, "empty SIP port"));
                                    Log.i(TAG, "Stored Secured Rad ID " + securePreferences.getString(Constants.PREFS_RAD_ID, "empty Rad ID"));
                                } else Log.e(TAG, "SecurePreferences is EMPTY");

                            String strRadId = response.body().getRadSessionId();
                            Log.e(TAG, "onResponse: LoginRadId: " + strRadId);
                            SharedPreferenceManager.getSharedPreferenceManagerInstance().putRadId(strRadId);
                            SharedPreferenceManager.getSharedPreferenceManagerInstance().putOwtelNumber(response.body().getSipNumber());
                            SharedPreferenceManager.getSharedPreferenceManagerInstance().putPortSipStatus(true);

                            SharedPreferenceManager.getSharedPreferenceManagerInstance().putIDDCredits(response.body().getIddCredits());

                            /**Username and Password caching*/
                            if (SharedPreferenceManager.getSharedPreferenceManagerInstance().getUserRememberStatus()) {
                                SharedPreferenceManager.getSharedPreferenceManagerInstance().putUsername(email);
                                SharedPreferenceManager.getSharedPreferenceManagerInstance().putPassword(password);
                            }

                            Log.e(TAG, "onResponse: " + SharedPreferenceManager.getSharedPreferenceManagerInstance().getPassword());
                            Intent mIntent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(mIntent);
                            finish();

                        } else {
                            layoutProgress.setVisibility(View.GONE);
                            String jsonObjectMessage = response.body().getMessage();
                            DialogBuilder.showAlertDialog(LoginActivity.this,
                                    response.body().getStatus(), jsonObjectMessage,
                                    getResources().getString(R.string.string_ok));
                            setEmailPassword(email, "");

                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
                        layoutProgress.setVisibility(View.GONE);
                        DialogBuilder.showAlertDialog(LoginActivity.this,
                                "Login Failed", "Please check your internet connection",
                                getResources().getString(R.string.string_ok));
                        setEmailPassword(email, "");
                    }

                });
            }
        }, GlobalValues.LOGIN_DELAY_TIME);
    }


    @Override
    public void finishActivity() {
        Log.e(TAG, "finishActivity: ");
    }

    @Override
    public void initBlocker() {
        Log.e(TAG, "initBlocker: ");
    }

    @Override
    public void switchTab(int tab) {
        Log.e(TAG, "switchTab: ");
    }

    @Override
    public void setBadgeZero(Context context, boolean isActive) {
        Log.e(TAG, "setBadgeZero: ");
    }


    private OkHttpClient getRequestHeader() {
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        httpClient.setReadTimeout(15, TimeUnit.SECONDS);

        return httpClient;
    }
}
