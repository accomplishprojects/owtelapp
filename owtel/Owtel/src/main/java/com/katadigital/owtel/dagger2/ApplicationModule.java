package com.katadigital.owtel.dagger2;

import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.utils.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final OwtelAppController app;


    public ApplicationModule(OwtelAppController app){
        this.app = app;
    }

    @Singleton
    @Provides
    RxBus provideRxBus(){
        return new RxBus();
    }
}
