package com.katadigital.owtel.modules.main.contacts.util;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.WindowManager;
import android.widget.EditText;

import com.countrypicker.CountryPicker;
import com.countrypicker.CountryPickerListener;
import com.kata.phone.R;

public class CountryPickerActivity extends FragmentActivity {

    public static EditText edCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_pic_layout);

        String letItem = getResources().getString(R.string.cancel_string);
        CustomActionBar.viewCustomActionBar(this, letItem, "", "");

        android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = manager.beginTransaction();

        CountryPicker picker = new CountryPicker();
        picker.setListener(new CountryPickerListener() {

            @Override
            public void onSelectCountry(String name, String code) {
                edCountry.setText(name);
                onBackPressed();
            }
        });
        transaction.replace(R.id.home, picker);
        transaction.commit();

    }

}
