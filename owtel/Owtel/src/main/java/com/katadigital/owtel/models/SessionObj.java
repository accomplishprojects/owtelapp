package com.katadigital.owtel.models;

/**
 * Created by dcnc123 on 11/28/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionObj {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("logged")
    @Expose
    private String logged;
    @SerializedName("message")
    @Expose
    private String message;
    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The logged
     */
    public String getLogged() {
        return logged;
    }

    /**
     *
     * @param logged
     * The logged
     */
    public void setLogged(String logged) {
        this.logged = logged;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

