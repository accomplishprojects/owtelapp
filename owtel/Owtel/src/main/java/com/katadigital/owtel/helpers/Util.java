package com.katadigital.owtel.helpers;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorDescription;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore.Images;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.contacts.adapters.PhoneNumbersDialogAdapter;
import com.katadigital.owtel.modules.main.contacts.camerautil.CameraUtil;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.EmailDto;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.ui.CircularImageView;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.R.attr.maxHeight;
import static android.R.attr.maxWidth;

//Jeffrey Util 

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class Util {

    // bitmap resize Image
    public static byte[] getbitMapResize(Bitmap bitmap) {
        final int maxSize = 1000;
        int outWidth;
        int outHeight;
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        if (inWidth > inHeight) {
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth,
                outHeight, false);
        resizedBitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        byte[] profilepic = stream.toByteArray();
        return profilepic;
    }

    // public static byte[] getbitMapResize(Bitmap b) {
    //
    // final int maxSize = 1000;//calculate how many bytes our image consists
    // of.
    // int bytes = b.getByteCount();
    // //or we can calculate bytes this way. Use a different value than 4 if you
    // don't use 32bit images.
    // //int bytes = b.getWidth()*b.getHeight()*4;
    //
    // ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
    // b.copyPixelsToBuffer(buffer); //Move the byte data to the buffer
    //
    // byte[] profilepic = buffer.array(); //Get the underlying array containing
    // the data.
    // return profilepic;
    // }

    // // bitmap resize Image
    // public static byte[] getbitMapResize(Bitmap bitmap) {
    //
    // // final int maxSize = 1000;
    // // int outWidth;
    // // int outHeight;
    // // int inWidth = bitmap.getWidth();
    // // int inHeight = bitmap.getHeight();
    // // if (inWidth > inHeight) {
    // // outWidth = maxSize;
    // // outHeight = (inHeight * maxSize) / inWidth;
    // // } else {
    // // outHeight = maxSize;
    // // outWidth = (inWidth * maxSize) / inHeight;
    // // }
    // //
    // // ByteArrayOutputStream stream = new ByteArrayOutputStream();
    // // Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth,
    // // outHeight, false);
    // // resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    // // byte[] profilepic = stream.toByteArray();
    // //calculate how many bytes our image consists of.
    // int bytes = decodeSampledBitmapFromResource.getByteCount();
    // //or we can calculate bytes this way. Use a different value than 4 if you
    // don't use 32bit images.
    // //int bytes = b.getWidth()*b.getHeight()*4;
    //
    // ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
    // b.copyPixelsToBuffer(buffer); //Move the byte data to the buffer
    //
    // byte[] profilepic = buffer.array(); //Get the underlying array containing
    // the data.
    //
    // return profilepic;
    // }

    public static boolean checkAddFieldisHidden(String stringValue,
                                                EditText editText, Boolean isActive) {

        if (editText.getText().toString().isEmpty()) {

            if (isActive) {
                editText.setVisibility(View.VISIBLE);
                editText.setVisibility(View.VISIBLE);
                editText.setFocusableInTouchMode(true);
                editText.requestFocus();
                isActive = true;
                return true;
            } else {
                editText.setVisibility(View.GONE);
                isActive = false;
                return false;
            }

        } else {

            editText.setVisibility(View.VISIBLE);
            editText.setVisibility(View.VISIBLE);
            // editText.setFocusableInTouchMode(false);
            editText.requestFocus();
            isActive = true;
            return true;
        }

    }

    public static boolean checkIfAddressPresent(AddressDto addressDto,
                                                ArrayList<AddressDto> addressList) {
        boolean result = false;
        for (AddressDto addressDto2 : addressList) {
            if (addressDto2.getAddressType().equalsIgnoreCase(
                    addressDto.getAddressType())
                    && addressDto2.getCityStr().equalsIgnoreCase(
                    addressDto.getCityStr())
                    && addressDto2.getCountryStr().equalsIgnoreCase(
                    addressDto.getCountryStr())
                    && addressDto2.getNeigborhoodStr().equalsIgnoreCase(
                    addressDto.getNeigborhoodStr())
                    && addressDto2.getStreetStr().equalsIgnoreCase(
                    addressDto.getStreetStr())
                    && addressDto2.getZipCodeStr().equalsIgnoreCase(
                    addressDto.getZipCodeStr())) {
                result = true;
                break;
            }
        }

        return result;
    }

    public static void checkInformationDetails(ContactDto contactDto,
                                               ContactDetailActivity contactDetails) {

        String prefix = (contactDto.getPrefix() == null
                || contactDto.getPrefix().isEmpty() ? "" : contactDto
                .getPrefix() + " ");

        String firstname = (contactDto.getFirstName() == null
                || contactDto.getFirstName().isEmpty() ? "" : contactDto
                .getFirstName() + " ");

        String middle = (contactDto.getMiddleName() == null
                || contactDto.getMiddleName().isEmpty() ? "" : contactDto
                .getMiddleName() + " ");

        String lastname = (contactDto.getLastName() == null
                || contactDto.getLastName().isEmpty() ? "" : contactDto
                .getLastName() + " ");

        contactDetails.textDisplayname.setText(prefix + firstname + middle
                + lastname);

        if (!contactDto.getBirthday().isEmpty()) {
            contactDetails.findViewById(R.id.LL_bday).setVisibility(
                    View.VISIBLE);
            contactDetails.textBirthDay.setText(contactDto.getBirthday());
        } else {
            contactDetails.findViewById(R.id.LL_bday).setVisibility(View.GONE);
        }

        contactDetails.edNotText.setText(contactDto.getNote());

        if (contactDto.getSuffix() != null) {
            contactDetails.textSuffix.setText(contactDto.getSuffix());

            if (contactDto.getSuffix().isEmpty()) {
                contactDetails.textSuffix.setVisibility(View.GONE);
            }

        } else {
            contactDetails.textSuffix.setVisibility(View.GONE);
        }

        if (contactDto.getNickname() != null) {

            if (!contactDto.getNickname().isEmpty()) {
                contactDetails.textNick
                        .setText('"' + contactDto.getNickname() + '"');
            } else {
                contactDetails.textNick.setVisibility(View.GONE);
            }

        } else {
            contactDetails.textNick.setVisibility(View.GONE);
        }

        // Phonetic Names

        String pho_firstname = (contactDto.getPhonetic_fName() == null
                || contactDto.getPhonetic_fName().isEmpty() ? "" : contactDto
                .getPhonetic_fName() + " ");

        String pho_middle = (contactDto.getPhonetic_middleName() == null
                || contactDto.getPhonetic_middleName().isEmpty() ? ""
                : contactDto.getPhonetic_middleName() + " ");

        String pho_lastname = (contactDto.getPhonetic_lName() == null
                || contactDto.getPhonetic_lName().isEmpty() ? "" : contactDto
                .getPhonetic_lName() + " ");

        if (contactDto.getPhonetic_fName() != null
                && !contactDto.getPhonetic_fName().isEmpty()
                || contactDto.getPhonetic_middleName() != null
                && !contactDto.getPhonetic_middleName().isEmpty()
                || contactDto.getPhonetic_lName() != null
                && !contactDto.getPhonetic_lName().isEmpty()) {
            contactDetails.textPname.setText(pho_firstname + pho_middle
                    + pho_lastname);
        } else {
            contactDetails.textPname.setVisibility(View.GONE);
        }

        if (contactDto.getJobTitle() != null) {
            // ito
            if (!contactDto.getJobTitle().isEmpty()
                    || !contactDto.getJobDep().isEmpty()) {

                if (contactDto.getJobTitle().isEmpty()) {
                    contactDetails.textJob.setText(contactDto.getJobDep());
                } else {
                    if (contactDto.getJobDep() != null) {
                        if (contactDto.getJobDep().isEmpty()) {
                            contactDetails.textJob.setText(contactDto.getJobTitle());
                        } else {
                            contactDetails.textJob.setText(contactDto.getJobTitle() + " - " + contactDto.getJobDep());
                        }
                    }
                }

            } else {
                contactDetails.textJob.setVisibility(View.GONE);
            }
        } else {
            contactDetails.textJob.setVisibility(View.GONE);
        }

        if (contactDto.getCompany() != null) {
            contactDetails.textComp.setText(contactDto.getCompany());
        } else {
            contactDetails.textComp.setVisibility(View.GONE);
        }

    }

    public static boolean checkIfEmailIsPresent(EmailDto emailDto,
                                                ArrayList<EmailDto> emailList) {
        boolean result = false;
        for (EmailDto emailDto2 : emailList) {
            if (emailDto2.getEmail().equalsIgnoreCase(emailDto.getEmail())
                    && emailDto2.getEmailType().equalsIgnoreCase(
                    emailDto.getEmailType())) {
                result = true;
                break;
            }
        }
        return result;
    }

    // to add Profile Picture Dialog
    public static void adddialogProfilePic(final Activity activity,
                                           final ContactDto contactDto, final CircleImageView imageView,
                                           final File mFileTemp) {

        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();

        View customView = inflater.inflate(R.layout.dialog_profile_pic, null);

        // Build the dialog
        final Dialog selectModelDialog = new Dialog(activity,
                R.style.DialogSlideAnim);
        selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectModelDialog.setContentView(customView);
        selectModelDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                .getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        selectModelDialog.setTitle("Actions");
        selectModelDialog.show();

        TextView textBtn1, textBtn2, textBtn3, textBtn4, textBtn5;
        textBtn1 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_take_photo);
        textBtn2 = (TextView) customView
                .findViewById(R.id.keypad_dialog_choose_photo);
        textBtn3 = (TextView) customView
                .findViewById(R.id.keypad_dialog_edit_photo);
        textBtn4 = (TextView) customView
                .findViewById(R.id.keypad_dialog_delete_photo);
        textBtn5 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_cancel);

        textBtn1.setText(activity.getResources().getString(
                R.string.string_take_photo));
        textBtn2.setText(activity.getResources().getString(
                R.string.string_chooese_photo));
        textBtn3.setText(activity.getResources().getString(
                R.string.string_edit_photo));
        textBtn4.setText(activity.getResources().getString(
                R.string.string_delete_photo));
        textBtn5.setText(activity.getResources().getString(
                R.string.cancel_string));

        textBtn1.setTypeface(NewUtil.getFontBold(activity));
        textBtn2.setTypeface(NewUtil.getFontBold(activity));
        textBtn3.setTypeface(NewUtil.getFontBold(activity));
        textBtn4.setTypeface(NewUtil.getFontBold(activity));
        textBtn5.setTypeface(NewUtil.getFontBold(activity));

        textBtn1.setTextSize(NewUtil.gettxtSize());
        textBtn2.setTextSize(NewUtil.gettxtSize());
        textBtn3.setTextSize(NewUtil.gettxtSize());
        textBtn4.setTextSize(NewUtil.gettxtSize());
        textBtn5.setTextSize(NewUtil.gettxtSize());

        if (contactDto.getProfilepic() == null) {

            textBtn2.setBackground(activity.getResources().getDrawable(
                    R.drawable.roundedbtn_bottom_selector));
            textBtn3.setVisibility(View.GONE);
            textBtn4.setVisibility(View.GONE);

        } else {

            textBtn2.setBackground(activity.getResources().getDrawable(
                    R.drawable.roundedbtn_nocorner_selector));
            textBtn3.setBackground(activity.getResources().getDrawable(
                    R.drawable.roundedbtn_nocorner_selector));
            textBtn4.setBackground(activity.getResources().getDrawable(
                    R.drawable.roundedbtn_bottom_selector));

            textBtn3.setVisibility(View.VISIBLE);
            textBtn4.setVisibility(View.VISIBLE);
        }

        textBtn1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                CameraUtil.takePicture(mFileTemp, activity);

                selectModelDialog.dismiss();
                activity.overridePendingTransition(R.anim.slide_in_up,
                        R.anim.slide_in_up_exit);

            }
        });

        textBtn2.setOnClickListener(new OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                CameraUtil.openGallery(activity);

                // dismiss dialog
                selectModelDialog.dismiss();
                activity.overridePendingTransition(R.anim.slide_in_up,
                        R.anim.slide_in_up_exit);
                selectModelDialog.dismiss();

            }
        });

        textBtn3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    Bitmap bit = contactDto.getProfilepic();
                    UCrop.of(getImageUri(activity, bit), Uri.fromFile(mFileTemp))
                            .withAspectRatio(16, 9)
                            .withMaxResultSize(maxWidth, maxHeight)
                            .start(activity);

                    // dismiss dialog
                    selectModelDialog.dismiss();
                    activity.overridePendingTransition(R.anim.slide_in_up,
                            R.anim.slide_in_up_exit);
                    selectModelDialog.dismiss();

                } catch (Exception e) {

                    Log.e(CameraUtil.TAG, "Error while creating temp file", e);
                }

            }
        });


        textBtn4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                deletedialogProfilePic(activity, contactDto, imageView);
                selectModelDialog.dismiss();
            }
        });

        textBtn5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //deletedialogProfilePic(activity, contactDto, imageView);
                selectModelDialog.dismiss();
            }
        });

    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public static DatePickerDialog datePickerString(Activity activity,
                                                    final EditText editText) {

        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker,
                                          int selectedyear, int selectedmonth, int selectedday) {

                        String year1 = String.valueOf(selectedyear);
                        String month1 = String.valueOf(selectedmonth + 1);
                        String day1 = String.valueOf(selectedday);
                        editText.setText(month1 + "-" + day1 + "-" + year1);

                    }
                }, mYear, mMonth, mDay);
        mDatePicker.setTitle(activity.getResources().getString(
                R.string.string_select_date));
        mDatePicker.show();
        return mDatePicker;
    }

    // Check icon drawable
    public static Drawable getIconForAccount(ContactDto contactDto,
                                             Context context) {

        AccountManager accountManager = AccountManager.get(context);

        AuthenticatorDescription[] descriptions = accountManager
                .getAuthenticatorTypes();
        for (AuthenticatorDescription description : descriptions) {
            if (description.type.equals(contactDto.getAccountType())) {
                PackageManager pm = context.getPackageManager();
                return pm.getDrawable(description.packageName,
                        description.iconId, null);
            }
        }
        return null;
    }

    public static LayoutParams paramCircle(LinearLayout layout) {
        LayoutParams params3 = layout.getLayoutParams();
        // Changes the height and width to the specified *pixels*
        params3.height = 60;
        params3.width = 15;
        return params3;
    }

    public static Bitmap decodeBitmap(Uri selectedImage, Activity activity) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(selectedImage), null, o2);
    }

    // to add Profile Picture Dialog
    public static void deletedialogProfilePic(final Activity activity,
                                              final ContactDto contactDto, final ImageView imageView) {
        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();

        View customView = inflater.inflate(R.layout.dialog_profile_pic, null);

        // Build the dialog
        final Dialog selectModelDialog = new Dialog(activity, R.style.DialogSlideAnim);
        selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectModelDialog.setContentView(customView);
        selectModelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = selectModelDialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        selectModelDialog.setTitle("Actions");
        selectModelDialog.show();

        TextView textBtn1, textBtn2, textBtn3, textBtn4, textBtn5;
        textBtn1 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_take_photo);
        textBtn2 = (TextView) customView
                .findViewById(R.id.keypad_dialog_choose_photo);
        textBtn3 = (TextView) customView
                .findViewById(R.id.keypad_dialog_edit_photo);
        textBtn4 = (TextView) customView
                .findViewById(R.id.keypad_dialog_delete_photo);
        textBtn5 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_cancel);

        textBtn1.setText(activity.getResources().getString(
                R.string.string_take_photo));
        textBtn2.setText(activity.getResources().getString(
                R.string.string_chooese_photo));
        textBtn3.setText(activity.getResources().getString(
                R.string.string_edit_photo));
        textBtn4.setText(activity.getResources().getString(
                R.string.string_delete_photo));
        textBtn5.setText(activity.getResources().getString(
                R.string.cancel_string));

        textBtn1.setVisibility(View.GONE);
        textBtn2.setVisibility(View.GONE);
        textBtn3.setVisibility(View.GONE);
        textBtn4.setTextColor(Color.RED);

        if (textBtn1.getVisibility() == View.GONE && textBtn2.getVisibility() == View.GONE && textBtn3.getVisibility() == View.GONE) {
            // Its visible
            textBtn4.setBackgroundResource(R.drawable.roundedbtn_allsides_selector);
        } else {
            textBtn4.setBackgroundResource(R.drawable.roundedbtn_top_selector);
        }

        textBtn1.setTypeface(NewUtil.getFontBold(activity));
        textBtn2.setTypeface(NewUtil.getFontBold(activity));
        textBtn3.setTypeface(NewUtil.getFontBold(activity));
        textBtn4.setTypeface(NewUtil.getFontBold(activity));
        textBtn5.setTypeface(NewUtil.getFontBold(activity));

        textBtn1.setTextSize(NewUtil.gettxtSize());
        textBtn2.setTextSize(NewUtil.gettxtSize());
        textBtn3.setTextSize(NewUtil.gettxtSize());
        textBtn4.setTextSize(NewUtil.gettxtSize());
        textBtn5.setTextSize(NewUtil.gettxtSize());

        textBtn4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                imageView.setImageResource(R.drawable.profile_pic_holder);
                contactDto.setProfilepic(null);
                selectModelDialog.dismiss();
            }
        });

        textBtn5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectModelDialog.dismiss();
            }
        });

    }

    public static void addDialogSendMessage(final Activity activity,
                                            final ContactDto contactDto, final String IsmessageOrCall) {
        if (contactDto.getPhoneNumbers().size() > 1) {

            LayoutInflater inflater = activity.getLayoutInflater();

            View customView = inflater
                    .inflate(R.layout.numberlist_dialog, null);

            // Build the dialog
            final Dialog selectModelDialog = new Dialog(activity,
                    R.style.DialogSlideAnim);
            selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            selectModelDialog.setContentView(customView);
            selectModelDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                    .getAttributes();

            wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

            selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT);
            selectModelDialog.setTitle("Actions");
            selectModelDialog.show();

            PhoneNumbersDialogAdapter numbersAdapter = new PhoneNumbersDialogAdapter(
                    activity, contactDto.getPhoneNumbers());

            ListView numberlist = (ListView) customView
                    .findViewById(R.id.numberlist);
            TextView cancel = (TextView) customView
                    .findViewById(R.id.keypad_dialog_btn_cancel);

            cancel.setTypeface(NewUtil.getFontRoman(activity));
            cancel.setTextSize(NewUtil.gettxtSize());

            numberlist.setAdapter(numbersAdapter);

            numberlist.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView,
                                        int myItemInt, long mylng) {
                    if (!contactDto.getPhoneNumbers().get(myItemInt)
                            .getNumber().isEmpty()) {
                        if (IsmessageOrCall.equalsIgnoreCase("message")) {
                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            sendIntent.setData(Uri.parse("sms:"
                                    + contactDto.getPhoneNumbers()
                                    .get(myItemInt).getNumber()));
                            activity.startActivity(sendIntent);
                        } else {
//                            activity.startActivity(QuickContactHelper
//                                    .initiateDefaultCall(activity, contactDto
//                                            .getPhoneNumbers().get(myItemInt)
//                                            .getNumber().replaceAll("#", "%23")));
                            QuickContactHelper.initiateSipCall(activity, contactDto
                                    .getPhoneNumbers().get(myItemInt)
                                    .getNumber().replaceAll("#", "%23"));
                        }

                    }
                    selectModelDialog.dismiss();
                }
            });

            cancel.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectModelDialog.dismiss();
                }
            });

        } else if (contactDto.getPhoneNumbers().size() == 1) {
            if (IsmessageOrCall.equalsIgnoreCase("message")) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"
                        + contactDto.getPhoneNumbers().get(0).getNumber()));
                activity.startActivity(sendIntent);
            } else {
//                activity.startActivity(QuickContactHelper
//                        .initiateDefaultCall(activity, contactDto
//                                .getPhoneNumbers().get(0).getNumber()
//                                .replaceAll("#", "%23")));
                QuickContactHelper.initiateSipCall(activity, contactDto
                        .getPhoneNumbers().get(0).getNumber()
                        .replaceAll("#", "%23"));
            }

        }
    }

    // to add Profile Picture Dialog
    public static void blockdialogProfilePic(final Activity activity,
                                             final ContactDto contactDto, final CircularImageView imageView) {
        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();

        View customView = inflater.inflate(R.layout.dialog_profile_pic, null);

        // Build the dialog
        final Dialog selectModelDialog = new Dialog(activity,
                R.style.DialogSlideAnim);
        selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectModelDialog.setContentView(customView);
        selectModelDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                .getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        selectModelDialog.setTitle("Actions");
        selectModelDialog.show();

        TextView textBtn1, textBtn2, textBtn3, textBtn4, textBtn5;
        textBtn1 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_take_photo);
        textBtn2 = (TextView) customView
                .findViewById(R.id.keypad_dialog_choose_photo);
        textBtn3 = (TextView) customView
                .findViewById(R.id.keypad_dialog_edit_photo);
        textBtn4 = (TextView) customView
                .findViewById(R.id.keypad_dialog_delete_photo);
        textBtn5 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_cancel);

        textBtn1.setText(activity.getResources().getString(
                R.string.string_take_photo));
        textBtn2.setText(activity.getResources().getString(
                R.string.string_chooese_photo));
        textBtn3.setText(activity.getResources().getString(
                R.string.string_edit_photo));
        textBtn4.setText(activity.getResources().getString(
                R.string.string_delete_photo));
        textBtn5.setText(activity.getResources().getString(
                R.string.cancel_string));

        textBtn1.setVisibility(View.GONE);
        textBtn2.setVisibility(View.GONE);
        textBtn3.setVisibility(View.GONE);
        textBtn4.setTextColor(Color.RED);

        textBtn1.setTypeface(NewUtil.getFontBold(activity));
        textBtn2.setTypeface(NewUtil.getFontBold(activity));
        textBtn3.setTypeface(NewUtil.getFontBold(activity));
        textBtn4.setTypeface(NewUtil.getFontBold(activity));
        textBtn5.setTypeface(NewUtil.getFontBold(activity));

        textBtn1.setTextSize(NewUtil.gettxtSize());
        textBtn2.setTextSize(NewUtil.gettxtSize());
        textBtn3.setTextSize(NewUtil.gettxtSize());
        textBtn4.setTextSize(NewUtil.gettxtSize());
        textBtn5.setTextSize(NewUtil.gettxtSize());

        textBtn4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                imageView.setImageResource(R.drawable.profile_pic_holder);
                contactDto.setProfilepic(null);
                selectModelDialog.dismiss();
            }
        });

        textBtn5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                selectModelDialog.dismiss();
            }
        });

    }

    public static void syncContacts(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Account[] accounts = accountManager.getAccounts();
            boolean isMasterSyncOn = ContentResolver.getMasterSyncAutomatically();
            for (Account account : accounts) {
                Log.d("CONTACTS", "account=" + account);

                int isSyncable = ContentResolver.getIsSyncable(account,
                        ContactsContract.AUTHORITY);
                boolean isSyncOn = ContentResolver.getSyncAutomatically(account,
                        ContactsContract.AUTHORITY);
                Log.d("CONTACTS", "Syncable=" + isSyncable + " SyncOn=" + isSyncOn);
                if (isSyncable > 0 /* && isSyncOn */) {
                    Log.d("CONTACTS", "request Sync");
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    ContentResolver.requestSync(account,
                            ContactsContract.AUTHORITY, bundle);
                }
            }
            return;
        }


    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth,
                                                     int reqHeight) { // BEST QUALITY MATCH

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            // if(Math.round((float)width / (float)reqWidth) > inSampleSize) //
            // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static void addDialogSendMessage(final Activity activity,
                                            final ContactDto contactDto) {
        if (contactDto.getPhoneNumbers().size() > 1) {

            LayoutInflater inflater = (LayoutInflater) activity
                    .getLayoutInflater();

            View customView = inflater
                    .inflate(R.layout.numberlist_dialog, null);

            // Build the dialog
            final Dialog selectModelDialog = new Dialog(activity,
                    R.style.DialogSlideAnim);
            selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            selectModelDialog.setContentView(customView);
            selectModelDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                    .getAttributes();

            wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

            selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT);
            selectModelDialog.setTitle("Actions");
            selectModelDialog.show();

            PhoneNumbersDialogAdapter numbersAdapter = new PhoneNumbersDialogAdapter(
                    activity, contactDto.getPhoneNumbers());

            ListView numberlist = (ListView) customView
                    .findViewById(R.id.numberlist);
            TextView cancel = (TextView) customView
                    .findViewById(R.id.keypad_dialog_btn_cancel);

            cancel.setTypeface(NewUtil.getFontBold(activity));
            cancel.setTextSize(NewUtil.gettxtSize());
            numberlist.setAdapter(numbersAdapter);
            numberlist.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> myAdapter, View myView,
                                        int myItemInt, long mylng) {
                    if (!contactDto.getPhoneNumbers().get(myItemInt)
                            .getNumber().isEmpty()) {
                        NewUtil.integrationNumber(activity, contactDto,
                                myItemInt);
                    } else {

                    }

                }
            });

            cancel.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    selectModelDialog.dismiss();
                }
            });

        } else if (contactDto.getPhoneNumbers().size() == 1) {
            NewUtil.integrationNumber(activity, contactDto, 0);

        }
    }

    public static void blockdialogContact(final ContactDetailActivity activity) {
        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();

        View customView = inflater.inflate(R.layout.dialog_profile_pic, null);

        // Build the dialog
        final Dialog selectModelDialog = new Dialog(activity,
                R.style.DialogSlideAnim);
        selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectModelDialog.setContentView(customView);
        selectModelDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                .getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        selectModelDialog.setTitle("Actions");
        selectModelDialog.show();

        TextView textBtn1, textBtn2, textBtn3, textBtn4, textBtn5;
        textBtn1 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_take_photo);
        textBtn2 = (TextView) customView
                .findViewById(R.id.keypad_dialog_choose_photo);
        textBtn3 = (TextView) customView
                .findViewById(R.id.keypad_dialog_edit_photo);
        textBtn4 = (TextView) customView
                .findViewById(R.id.keypad_dialog_delete_photo);
        textBtn5 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_cancel);

        textBtn1.setText(activity.getResources().getString(
                R.string.alert_block));
        textBtn2.setText(activity.getResources().getString(
                R.string.block_contact));
        textBtn3.setText(activity.getResources().getString(
                R.string.string_edit_photo));
        textBtn4.setText(activity.getResources().getString(
                R.string.string_delete_photo));
        textBtn5.setText(activity.getResources().getString(
                R.string.cancel_string));

        textBtn1.setTypeface(NewUtil.getFontBold(activity));
        textBtn2.setTypeface(NewUtil.getFontBold(activity));
        textBtn3.setTypeface(NewUtil.getFontBold(activity));
        textBtn4.setTypeface(NewUtil.getFontBold(activity));
        textBtn5.setTypeface(NewUtil.getFontBold(activity));

        textBtn1.setTextSize(NewUtil.gettxtSize());
        textBtn2.setTextSize(NewUtil.gettxtSize());
        textBtn3.setTextSize(NewUtil.gettxtSize());
        textBtn4.setTextSize(NewUtil.gettxtSize());
        textBtn5.setTextSize(NewUtil.gettxtSize());


        textBtn1.setTextColor(Color.GRAY);
        textBtn2.setBackground(activity.getResources().getDrawable(
                R.drawable.roundedbtn_bottom_selector));
        textBtn2.setTextColor(Color.RED);
        textBtn3.setVisibility(View.GONE);
        textBtn4.setVisibility(View.GONE);

        textBtn1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectModelDialog.dismiss();
                activity.overridePendingTransition(R.anim.slide_in_up,
                        R.anim.slide_in_up_exit);

            }
        });

        textBtn2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                activity.blockContact();

                // dismiss dialog
                selectModelDialog.dismiss();
                activity.overridePendingTransition(R.anim.slide_in_up,
                        R.anim.slide_in_up_exit);
                selectModelDialog.dismiss();

            }
        });

        textBtn3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });

        textBtn4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectModelDialog.dismiss();
            }
        });

        textBtn5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectModelDialog.dismiss();
            }
        });
    }

}
