package com.katadigital.owtel.modules.main.contacts.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.entities.DateDto;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

public class EventAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<DateDto> emails;

    public EventAdapter(Activity context, ArrayList<DateDto> emails) {

        super();
        this.context = context;

        this.emails = new ArrayList<DateDto>();
        this.emails.addAll(emails);
    }

    @Override
    public int getCount() {
        return emails.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.event_listitem, viewGroup,
                false);
        TextView emailType = (TextView) rowView
                .findViewById(R.id.detail_event_type);
        TextView detailEmail = (TextView) rowView
                .findViewById(R.id.detail_event);

        emailType.setTypeface(NewUtil.getFontRoman(context));
        detailEmail.setTypeface(NewUtil.getFontRoman(context));

        emailType.setTextSize(NewUtil.gettxtSize());
        detailEmail.setTextSize(NewUtil.gettxtSize());

        if (emails.get(i) != null) {

            DateDto emailDto = emails.get(i);

            emailType.setText(emailDto.getDatesType());
            detailEmail.setText(emailDto.getDates());

        }

        return rowView;
    }

}
