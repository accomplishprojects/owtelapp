package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.graphics.Typeface;
import android.widget.Button;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.DateDto;
import com.katadigital.owtel.modules.main.contacts.entities.EmailDto;
import com.katadigital.owtel.modules.main.contacts.entities.IMDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.entities.RelationDto;
import com.katadigital.owtel.modules.main.contacts.entities.WebsiteDto;

public class EditHelperField {

    EditContactsActivity activity;

    int fieldPhone_ID = 0;
    int fieldEmail_ID = 0;
    int fieldURL_ID = 0;
    int fieldAddress_ID = 0;
    int fieldDate_ID = 0;
    int fieldRE_ID = 0;
    int fieldIM_ID = 0;

    Typeface font1;
    Typeface font2;

    public ArrayList<Button> btnPhonespinner = new ArrayList<Button>();
    public ArrayList<Button> btnEmailspinner = new ArrayList<Button>();
    public ArrayList<Button> btnURLspinner = new ArrayList<Button>();
    public ArrayList<Button> btnAddresspinner = new ArrayList<Button>();
    public ArrayList<Button> btnDatespinner = new ArrayList<Button>();
    public ArrayList<Button> btnREspinner = new ArrayList<Button>();
    public ArrayList<Button> btnIMspinner = new ArrayList<Button>();

    public static boolean hasBirthday = false;

    // helper
    EditHelperJMPhone phoneHelper;
    EditHelperJMEmail emailHelper;
    EditHelperJMURL urlHelper;
    EditHelperJMAddress addressHelper;
    EditHelperJMBday bdayHelper;
    EditHelperJMDate dateHelper;
    EditHelperJMRelation relationHelper;
    EditHelperJMIM imHelper;

    public EditHelperField(EditContactsActivity activity) {
        this.activity = activity;
        this.font1 = NewUtil.getFontBold(activity);
        this.font2 = NewUtil.getFontRoman(activity);
        this.phoneHelper = new EditHelperJMPhone(activity, this);
        this.emailHelper = new EditHelperJMEmail(activity, this);
        this.urlHelper = new EditHelperJMURL(activity, this);
        this.addressHelper = new EditHelperJMAddress(activity, this);
        this.bdayHelper = new EditHelperJMBday(activity, this);
        this.dateHelper = new EditHelperJMDate(activity, this);
        this.relationHelper = new EditHelperJMRelation(activity, this);
        this.imHelper = new EditHelperJMIM(activity, this);
    }

    public void clearAllButtonList() {
        btnPhonespinner.clear();
        btnEmailspinner.clear();
        btnURLspinner.clear();
        btnAddresspinner.clear();
        hasBirthday = false;
        btnDatespinner.clear();
        btnREspinner.clear();
        btnIMspinner.clear();
    }

    public ContactDto fillInformation(ContactDto old, ContactDto newCon) {
        if (old != null) {
            activity.prefixNameText.setText(old.getPrefix());
            activity.fNameText.setText(old.getFirstName());
            activity.phonetic_fNameText.setText(old.getPhonetic_fName());
            activity.middleNameText.setText(old.getMiddleName());
            activity.phonetic_midNameText.setText(old.getPhonetic_middleName());
            activity.lNameText.setText(old.getLastName());
            activity.phonetic_lNameText.setText(old.getPhonetic_lName());
            activity.sufNameText.setText(old.getSuffix());
            activity.nickNameText.setText(old.getNickname());
            activity.jobTitleNameText.setText(old.getJobTitle());
            activity.jobDepNameText.setText(old.getJobDep());
            activity.companyName.setText(old.getCompany());
            activity.noteTxt.setText(old.getPrefix());

            // Important ID
            newCon.setContactID(old.getContactID());
            newCon.setAccountName(old.getAccountName());
            newCon.setAccountType(old.getAccountType());

            // get Information
            // newCon.setDisplayName(old.getDisplayName());
            newCon.setPrefix(old.getPrefix());
            newCon.setFirstName(old.getFirstName());
            newCon.setPhonetic_fName(old.getPhonetic_fName());
            newCon.setMiddleName(old.getMiddleName());
            newCon.setPhonetic_middleName(old.getPhonetic_middleName());
            newCon.setLastName(old.getLastName());
            newCon.setPhonetic_lName(old.getPhonetic_lName());
            newCon.setSuffix(old.getSuffix());
            newCon.setNickname(old.getNickname());
            newCon.setJobTitle(old.getJobTitle());
            newCon.setJobDep(old.getJobDep());
            newCon.setCompany(old.getCompany());

            // Phone Numbers
            for (PhoneNumberDto phoneNumberDto : old.getPhoneNumbers()) {
                PhoneNumberDto phoneNumberDto2 = new PhoneNumberDto();
                phoneNumberDto2.setNumber(phoneNumberDto.getNumber());
                phoneNumberDto2.setNumberType(phoneNumberDto.getNumberType());
                newCon = phoneHelper.createLoadedNumberField(phoneNumberDto2, newCon);
            }

            if (!activity.isFromContactDetailsPage) {
                PhoneNumberDto phoneNumberDto = new PhoneNumberDto();
                phoneNumberDto.setNumber(activity.unknownNumberToAdd);
                phoneNumberDto.setNumberType(activity.getResources().getStringArray(R.array.numberspinner)[0]);
                newCon = phoneHelper.createLoadedNumberField(phoneNumberDto, newCon);
            }

            for (EmailDto emailDto : old.getEmails()) {
                EmailDto emailDto2 = new EmailDto();
                emailDto2.setEmail(emailDto.getEmail());
                emailDto2.setEmailType(emailDto.getEmailType());
                newCon = emailHelper.createLoadedEmailField(emailDto2, newCon);
            }

            for (WebsiteDto websiteDto : old.getWebsites()) {
                WebsiteDto websiteDto2 = new WebsiteDto();
                websiteDto2.setWebsite(websiteDto.getWebsite());
                websiteDto2.setWebsiteType(websiteDto.getWebsiteType());
                newCon = urlHelper.createLoadedURLField(websiteDto2, newCon);
            }

            for (AddressDto addressDto : old.getAddresses()) {
                AddressDto addressDto2 = new AddressDto();
                addressDto2.setStreetStr(addressDto.getStreetStr());
                addressDto2.setNeigborhoodStr(addressDto.getNeigborhoodStr());
                addressDto2.setCityStr(addressDto.getCityStr());
                addressDto2.setZipCodeStr(addressDto.getZipCodeStr());
                addressDto2.setCountryStr(addressDto.getCountryStr());
                addressDto2.setAddressType(addressDto.getAddressType());
                newCon = addressHelper.createLoadedAddressField(addressDto2,
                        newCon);
            }

            for (DateDto dateDto : old.getDateEvent()) {
                DateDto dateDto2 = new DateDto();
                dateDto2.setDates(dateDto.getDates());
                dateDto2.setDatesType(dateDto.getDatesType());
                newCon = dateHelper.createLoadedDateField(dateDto2, newCon);
            }

            for (RelationDto imDto : old.getRelationDto()) {
                RelationDto reDto = new RelationDto();
                reDto.setRelationName(imDto.getRelationName());
                reDto.setRelationType(imDto.getRelationType());
                newCon = relationHelper.createLoadedRelation(reDto, newCon);
            }

            for (IMDto imDto : old.getImField()) {
                IMDto imDto2 = new IMDto();
                imDto2.setIm(imDto.getIm());
                imDto2.setImProtocol(imDto.getImProtocol());
                newCon = imHelper.createLoadedIMField(imDto2, newCon);
            }

            if (!old.getNote().isEmpty()) {
                newCon.setNote(old.getNote());
                activity.noteTxt.setText(old.getNote());
            }

            if (!old.getBirthday().isEmpty()) {
                newCon.setBirthday(old.getBirthday());
                newCon = bdayHelper.createLoadedBirthdayField(
                        old.getBirthday(), newCon);
            }

        }
        return newCon;
    }

    public void addPhoneFunction(ContactDto contactDto) {
        phoneHelper.addPhoneFunction(contactDto);
    }

    public void addEmailFunction(ContactDto contactDto) {
        emailHelper.addEmailFunction(contactDto);
    }

    public void addURLFunction(ContactDto contactDto) {
        urlHelper.addURLFunction(contactDto);
    }

    public void addAddressFunction(ContactDto contactDto) {
        addressHelper.addAddressFunction(contactDto);
    }

    public void addBdayFunction(ContactDto contactDto) {
        bdayHelper.addBdayFunction(contactDto);
    }

    public void addDateFunction(ContactDto contactDto) {
        dateHelper.addDateFunction(contactDto);
    }

    public void addREFunction(ContactDto contactDto) {
        relationHelper.addREFunction(contactDto);
    }

    public void addIMFunction(ContactDto contactDto) {
        imHelper.addIMFunction(contactDto);

    }

}
