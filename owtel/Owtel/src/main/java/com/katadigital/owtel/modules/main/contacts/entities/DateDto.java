package com.katadigital.owtel.modules.main.contacts.entities;

public class DateDto {

    private int id;
    private String datesType;
    private String dates;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDatesType() {
        return datesType;
    }

    public void setDatesType(String datesType) {
        this.datesType = datesType;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }


}
