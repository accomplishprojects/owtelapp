package com.katadigital.owtel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Omar Matthew Reyes on 7/7/16.
 * Data Object for user login API response
 */
public class UserSession {
    @SerializedName("status")
    @Expose
    private String apiStatus;

    @SerializedName("expiration_date")
    @Expose
    private String userExpDate;

    @SerializedName("idd_credits")
    @Expose
    private double userIddCredits;

    @SerializedName("sip_number")
    @Expose
    private String userSipNumber;

    @SerializedName("sip_password")
    @Expose
    private String userSipPassword;

    @SerializedName("sip_server")
    @Expose
    private String userSipServer;

    @SerializedName("sip_port")
    @Expose
    private String userSipPort;

    @SerializedName("rad_session_id")
    @Expose
    private String userRadId;

    @SerializedName("promo_code")
    @Expose
    private String userPromoCode;

    @SerializedName("message")
    @Expose
    private String apiMessage;

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public String getUserExpDate() {
        return userExpDate;
    }

    public void setUserExpDate(String userExpDate) {
        this.userExpDate = userExpDate;
    }

    public double getUserIddCredits() {
        return userIddCredits;
    }

    public void setUserIddCredits(double userIddCredits) {
        this.userIddCredits = userIddCredits;
    }

    public String getUserSipNumber() {
        return userSipNumber;
    }

    public void setUserSipNumber(String userSipNumber) {
        this.userSipNumber = userSipNumber;
    }

    public String getUserSipPassword() {
        return userSipPassword;
    }

    public void setUserSipPassword(String userSipPassword) {
        this.userSipPassword = userSipPassword;
    }

    public String getUserSipServer() {
        return userSipServer;
    }

    public void setUserSipServer(String userSipServer) {
        this.userSipServer = userSipServer;
    }

    public String getUserSipPort() {
        return userSipPort;
    }

    public void setUserSipPort(String userSipPort) {
        this.userSipPort = userSipPort;
    }

    public String getUserRadId() {
        return userRadId;
    }

    public void setUserRadId(String userRadId) {
        this.userRadId = userRadId;
    }

    public String getUserPromoCode() {
        return userPromoCode;
    }

    public void setUserPromoCode(String userPromoCode) {
        this.userPromoCode = userPromoCode;
    }

    public String getApiMessage() {
        return apiMessage;
    }

    public void setApiMessage(String apiMessage) {
        this.apiMessage = apiMessage;
    }
}
