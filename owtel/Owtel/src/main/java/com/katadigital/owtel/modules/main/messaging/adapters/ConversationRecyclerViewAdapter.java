//package com.katadigital.owtel.modules.main.messaging.adapters;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.toolbox.StringRequest;
//import com.kata.phone.R;
//import com.katadigital.owtel.OwtelAppController;
//import com.katadigital.owtel.utils.API;
//import com.katadigital.owtel.utils.Constants;
//import com.katadigital.owtel.utils.StringFormatter;
//import com.katadigital.owtel.modules.login.LoginActivity;
//import com.katadigital.owtel.modules.main.MainActivityBridge;
//import com.katadigital.owtel.sip.SipSettings;
//import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
//import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
//import com.securepreferences.SecurePreferences;
//
//import org.json.JSONException;
//import org.json.JSONObject;
////import org.sipdroid.sipua.ui.Receiver;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by Omar Matthew Reyes on 2/19/16.
// * <p>
// * RecyclerView adapter for SettingsFragment list
// */
//public class ConversationRecyclerViewAdapter extends RecyclerView.Adapter<ConversationRecyclerViewAdapter.ViewHolder> {
//    private String[] mDataset;
//    private Context context;
//    private MainActivityBridge mMainActivityBridge;
//    private static final int
//            SETTINGS_EMAIL = 0,
//            SETTINGS_CALL_TEXT_BAL = 1,
//            SETTINGS_IDD_BAL = 2,
//            SETTINGS_NUMBER = 3,
//            SETTINGS_ABOUT = 4,
////            SETTINGS_SIP = 5,
//            SETTINGS_LOGOUT = 5;
//    private static final String TAG = "SettingsRecycler";
//    private int devClick = 0;
//
//    private SharedPreferences preferencesUser;
//
//    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        protected TextView mTextViewSettingsItem, mTextViewSettingsItemDetail;
//        protected Context context;
//
//        public ViewHolder(View v, Context context) {
//            super(v);
//            mTextViewSettingsItem = (TextView) v.findViewById(R.id.tv_settings_item);
//            mTextViewSettingsItemDetail = (TextView) v.findViewById(R.id.tv_settings_item_detail);
//            this.context = context;
//            v.setOnClickListener(this);
//        }
//
//        @Override
//        public void onClick(View v) {
//            Log.i(TAG, "RecyclerView clicked at " + getAdapterPosition());
//            int devClickNeed = 9;
//            Intent intent;
//            switch (getAdapterPosition()) {
//                case SETTINGS_NUMBER:
//                    devClick++;
//                    if (devClick == devClickNeed)
//                        context.startActivity(new Intent(context, org.sipdroid.sipua.ui.Settings.class));
//                    break;
////                case SETTINGS_SIP:
////                    intent = new Intent(context, org.sipdroid.sipua.ui.Settings.class);
////                    context.startActivity(intent);
////                    break;
//                case SETTINGS_LOGOUT:
//                    // TODO
//                    // Clear Preferences
//                    // Stop running Services
//                    doLogout();
//                    break;
//            }
//        }
//    }
//
//    public ConversationRecyclerViewAdapter(String[] myDataset, Context context) {
//        this.mDataset = myDataset;
//        this.context = context;
//
//        preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
//        this.mMainActivityBridge = (MainActivityBridge) context;
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
//        // create a new view
//        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.recycler_view_item_settings, viewGroup, false);
//        return new ViewHolder(v, context);
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
//        holder.mTextViewSettingsItem.setText(mDataset[position]);
//        switch (position) {
//            case SETTINGS_EMAIL:
//                holder.mTextViewSettingsItemDetail.setText(preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty"));
//                break;
//            case SETTINGS_CALL_TEXT_BAL:
//                break;
//            case SETTINGS_IDD_BAL:
//                break;
//            case SETTINGS_NUMBER:
//                holder.mTextViewSettingsItemDetail.setText(StringFormatter.formatUsNumber(new SipSettings().getSipUsername()));
//                break;
//            case SETTINGS_ABOUT:
//                try {
//                    holder.mTextViewSettingsItemDetail
//                            .setText(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
//                } catch (PackageManager.NameNotFoundException e) {
//                    Log.e(TAG, "About: Version " + e);
//                }
//                break;
////            case SETTINGS_SIP: // Empty
////                break;
//            case SETTINGS_LOGOUT: // Empty
//                break;
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return mDataset.length;
//    }
//
//    /**
//     * <p>Do logout function
//     * <p>Clears the following:
//     * <ul>
//     * <li>SharedPreference (User data)</li>
//     * <li>SecurePreference (Server configs)</li>
//     * <li>Settings</ul></p>
//     * <p>Stops Services:
//     * <ul>
//     * <li>SIP Engine</li>
//     * <li>Web Socket Receiver</li>
//     * </ul></p>
//     */
//    private void doLogout() {
//        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");
//        String pass = preferencesUser.getString(Constants.PREFS_USER_PASS, "empty_pass");
//        String key = email + pass;
//        SecurePreferences securePreferences = OwtelAppController.getInstance()
//                .getUserPinBasedSharedPreferences(key);
//        String radId = securePreferences.getString(Constants.PREFS_RAD_ID, "empty_rad_id");
//        Log.i(TAG, "RAD ID " + radId);
//
//        ProgressDialog mProgressDialog = new ProgressDialog(context);
//        mProgressDialog.setMessage(context.getString(R.string.logging_out));
//        mProgressDialog.show();
//        StringRequest mStringRequestLogout = new StringRequest(Request.Method.POST, API.URL_TEST + API.LOGOUT,
//                response -> {
//                    try {
//                        JSONObject jsonObjectMain = new JSONObject(response);
//                        String jsonObjectStatus = jsonObjectMain.getString("status");
//                        if (jsonObjectStatus.equals("Success")) {
//                            stopServices();
//                        } else {
//                            mProgressDialog.dismiss();
//                        }
//                    } catch (JSONException e) {
//                        mProgressDialog.dismiss();
//                        if (GlobalValues.DEBUG) Log.e(TAG, "Volley " + e);
//                        Toast.makeText(context, context.getString(R.string.request_failed) + e, Toast.LENGTH_LONG).show();
//                    }
//                }, error ->
//        {
//            if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
//            mProgressDialog.dismiss();
//            Toast.makeText(context, context.getString(R.string.request_failed) + error, Toast.LENGTH_LONG).show();
//
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                final Map<String, String> params = new HashMap<>();
//                params.put(API.LOGOUT_RAD_ID, radId);
//                return params;
//            }
//        };
//        mStringRequestLogout.setRetryPolicy(new DefaultRetryPolicy(
//                GlobalValues.REQUEST_TIMEOUT,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mStringRequestLogout.setTag(Constants.REQUEST_TAG_SAVE_USER);
//        OwtelAppController.getInstance().addToRequestQueue(mStringRequestLogout);
//    }
//    /**
//     * Stop running services
//     */
//    private void stopServices() {
//        preferencesUser.edit().clear().apply(); // Clear default Shared Preference for email & pass
//        OwtelAppController.getInstance().stopWebSocketService();    // Stop Web Socket service
//        OwtelAppController.getInstance().stopZoiperCallbackService();    // Stop Zoiper Callback service
////        Receiver.engine(context).halt();    // Stop SIP Engine
//        mMainActivityBridge.finishActivity(); // end MainActivity
//        context.startActivity(new Intent(context, LoginActivity.class));    // go to LoginActivity
//    }
//}
