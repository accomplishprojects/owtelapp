package com.katadigital.owtel.modules.main.contacts.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

import java.util.ArrayList;

public class PhoneNumbersAdapter extends BaseAdapter {
    private Activity context;
    private ArrayList<PhoneNumberDto> phoneNumbers;
    String selectedNumber = "";
    boolean isfavContact;

    public PhoneNumbersAdapter(Activity context,
                               ArrayList<PhoneNumberDto> phoneNumbers, String selectedNumber,
                               boolean fav) {

        super();
        this.context = context;

        this.phoneNumbers = new ArrayList<PhoneNumberDto>();
        this.phoneNumbers.addAll(phoneNumbers);
        this.selectedNumber = selectedNumber;
        isfavContact = fav;
    }

    @Override
    public int getCount() {
        return phoneNumbers.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.contactnumber_listitem,
                viewGroup, false);
        TextView detailNumberType = (TextView) rowView
                .findViewById(R.id.detail_number_type);
        TextView detailNumber = (TextView) rowView
                .findViewById(R.id.detail_number);

        detailNumberType.setTypeface(NewUtil.getFontRoman(context));
        detailNumber.setTypeface(NewUtil.getFontRoman(context));

        detailNumberType.setTextSize(NewUtil.gettxtSize());
        detailNumber.setTextSize(NewUtil.gettxtSize());

        ImageView fav_indicator = (ImageView) rowView
                .findViewById(R.id.fav_indicator);

        ImageView message = (ImageView) rowView.findViewById(R.id.send_message);

        ImageView video = (ImageView) rowView
                .findViewById(R.id.details_video_call);
        video.setVisibility(View.GONE);
        video.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    Intent callIntent = new Intent(
                            "com.android.phone.videocall");
                    callIntent.putExtra("videocall", true);
                    callIntent.setData(Uri.parse("tel:" + "09153250701"));
                    context.startActivity(callIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        if (phoneNumbers.get(i) != null) {
            final PhoneNumberDto phoneNumberDto = phoneNumbers.get(i);
            if (!selectedNumber.isEmpty()) {
                if (selectedNumber.equalsIgnoreCase(phoneNumberDto.getNumber())) {
                    detailNumberType.setText(phoneNumberDto.getNumberType() + " ");
                    detailNumber.setText(phoneNumberDto.getNumber().replaceAll(" ", ""));
                } else {
                    detailNumberType.setText(phoneNumberDto.getNumberType());
                    detailNumber.setText(phoneNumberDto.getNumber().replaceAll(" ", ""));
                }
            } else {
                detailNumberType.setText(phoneNumberDto.getNumberType());
                detailNumber.setText(phoneNumberDto.getNumber().replaceAll(" ", ""));
            }

            System.out.println("JM string " + detailNumberType.getText().toString());

            if (detailNumberType
                    .getText()
                    .toString()
                    .equalsIgnoreCase(
                            context.getResources().getString(
                                    R.string.string_array_mobile))) {

                message.setVisibility(View.VISIBLE);

                message.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        // sendIntent.setData(Uri.parse("sms:"
                        // + phoneNumberDto.getNumber()));
                        // context.startActivity(sendIntent);

                        NewUtil.integrationNumberTotheAdapter(context,
                                phoneNumberDto);

                    }
                });

            } else {
                message.setVisibility(View.GONE);
            }
        }

        if (phoneNumbers.get(i) != null) {
            final PhoneNumberDto phoneNumberDto = phoneNumbers.get(i);

            if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                    context.getResources().getString(
                            R.string.string_array_mobile))) {
                message.setVisibility(View.VISIBLE);

                message.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        // sendIntent.setData(Uri.parse("sms:"
                        // + phoneNumberDto.getNumber()));
                        // context.startActivity(sendIntent);

                        NewUtil.integrationNumberTotheAdapter(context,
                                phoneNumberDto);

                    }
                });
            } else {
                message.setVisibility(View.GONE);
            }

        }

        if (isfavContact) {
            fav_indicator.setVisibility(View.VISIBLE);
        } else {
            fav_indicator.setVisibility(View.GONE);
        }

        return rowView;
    }
}
