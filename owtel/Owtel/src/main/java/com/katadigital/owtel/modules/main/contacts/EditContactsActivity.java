package com.katadigital.owtel.modules.main.contacts;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.helpers.CustomSheetKata;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Util;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.contacts.camerautil.CameraManager;
import com.katadigital.owtel.modules.main.contacts.camerautil.CameraUtil;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.util.AddHelperField;
import com.katadigital.owtel.modules.main.contacts.util.CustomActionBar;
import com.katadigital.owtel.modules.main.contacts.util.CustomTextField;
import com.katadigital.owtel.modules.main.contacts.util.EditFunction;
import com.katadigital.owtel.modules.main.contacts.util.EditHelperField;
import com.katadigital.owtel.modules.main.contacts.util.EditHelperJMPhone;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.owtel.modules.main.contacts.util.RingtoneActivity;
import com.katadigital.owtel.modules.main.dialer.DialerFragment;
import com.katadigital.owtel.modules.main.history.LogsDetail;
import com.katadigital.portsip.utilities.ui.image.ImageCustomManager;

import java.io.File;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditContactsActivity extends AppCompatActivity implements
        OnClickListener {
    private static final String TAG = "EditContactsActivity";
    ContactDto contactDto = new ContactDto();
    public static ContactDto oldContactDto;
    QuickContactHelper quickContactHelper;

    CircleImageView avatar;
    public EditText prefixNameText;
    public EditText fNameText;
    public EditText phonetic_fNameText;
    public EditText middleNameText;
    public EditText lNameText;
    public EditText phonetic_lNameText;
    public EditText nickNameText;
    public EditText phonetic_midNameText;
    public EditText sufNameText;
    public EditText jobTitleNameText;
    public EditText jobDepNameText;
    public EditText companyName;
    public EditText noteTxt;

    static boolean isEditable;

    static boolean removePref;
    static boolean removePfname;
    static boolean removeMiddle;
    static boolean removePmiddle;
    static boolean removePLastname;
    static boolean removeSuffix;
    static boolean removeNickname;
    static boolean removeJobTitle;
    static boolean removeJobDep;

    EditHelperField helperField;
    EditFunction edithelper;

    private ProgressDialog progressDialog;
    public boolean isFromContactDetailsPage = false;
    public String unknownNumberToAdd = "";

    ScrollView scrollView;

    Typeface font1;
    Typeface font2;

    TextView ringtoneName;

    private File mFileTemp;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CameraManager.onActivityResultCamera(requestCode, resultCode, data, this, avatar, mFileTemp, oldContactDto);

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_contacts);
        ImageCustomManager.init();


        /**Usable Modifications*/
        EditHelperJMPhone.checkerForPhoneNumbers.clear();
        EditHelperJMPhone.checkerForPhoneNumberIndex = -1;

        Log.i(TAG, "onCreate");

        String letItem = getResources().getString(R.string.cancel_string);
        String rightItem = getResources().getString(R.string.done_string);

        View doneBtn = CustomActionBar.viewCustomActionBar(this, letItem, "", rightItem);
        doneBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (progressDialog == null) {
                    Log.e(TAG, "onClick: " + EditHelperJMPhone.checkerForPhoneNumbers.size());
                    /**Usable Modification*/
                    String strFname = fNameText.getText().toString();
                    String strLname = lNameText.getText().toString();
                    if (!strFname.isEmpty() || !strLname.isEmpty()) {
                        if (EditHelperJMPhone.checkerForPhoneNumbers.size() > 0) {
                            if (EditHelperJMPhone.checkerForPhoneNumbers
                                    .get(EditHelperJMPhone.checkerForPhoneNumbers.size() - 1)
                                    .getPhoneNumber() != null) {
                                if (!EditHelperJMPhone.checkerForPhoneNumbers
                                        .get(EditHelperJMPhone.checkerForPhoneNumbers.size() - 1)
                                        .getPhoneNumber().isEmpty()) {
                                    new saveEditedContact()
                                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {
                                    Toast.makeText(EditContactsActivity.this, "Please check numbers", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(EditContactsActivity.this, "Please check numbers", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(EditContactsActivity.this, "Please enter your phone number", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(EditContactsActivity.this, "Please check your firstname and lastname", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        initData();
        initInformation();
        initTextField();
        fillNewContactDto();
        createNoteField();
        initLayoutforFont();

        noteTxt.setFocusableInTouchMode(false);
        mFileTemp = CameraUtil.getFile(this);
    }

    @Override
    public void onBackPressed() {
        finish();
        EditContactsActivity.this.overridePendingTransition(0,
                R.anim.slide_out_down);
        AddFieldActivity.setFalseAllFields();
    }

    private void initData() {
        quickContactHelper = new QuickContactHelper(this);
        helperField = new EditHelperField(this);
        helperField.clearAllButtonList();
        edithelper = new EditFunction(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isFromContactDetailsPage = extras.getBoolean("isFromContactDetails");
            if (!isFromContactDetailsPage) {
                unknownNumberToAdd = extras.getString("unknownNumberToAdd");
            }
        }

        // scrollview
        scrollView = (ScrollView) findViewById(R.id.add_scroll_view);
        Button delete = (Button) findViewById(R.id.delete_btn);
        delete.setTypeface(NewUtil.getFontBold(this));
        delete.setTextSize(NewUtil.gettxtSize());
        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomSheetKata.deleteSheet(EditContactsActivity.this,
                        oldContactDto);
            }
        });

    }

    private void initLayoutforFont() {

        ((TextView) findViewById(R.id.add_phone_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_phone_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_email_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_email_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_ringtone_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_ringtone_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_url_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_url_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_address_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_address_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_bday_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_bday_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_dates_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_dates_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_im_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_im_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_related_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_related_txt)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_notes_ok)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_notes_ok)).setTextSize(NewUtil
                .gettxtSize());
        ((TextView) findViewById(R.id.add_field_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_field_txt)).setTextSize(NewUtil
                .gettxtSize());

    }

    private void initInformation() {
        prefixNameText = CustomTextField.includeInit(R.id.include_prefix, EditContactsActivity.this);
        prefixNameText.requestFocus();
        fNameText = CustomTextField.includeInit(R.id.include_givenname, EditContactsActivity.this);
        fNameText.requestFocus();
        phonetic_fNameText = CustomTextField.includeInit(R.id.include_p_givenname, EditContactsActivity.this);
        middleNameText = CustomTextField.includeInit(R.id.include_middle, EditContactsActivity.this);
        phonetic_midNameText = CustomTextField.includeInit(R.id.include_p_middle, EditContactsActivity.this);
        lNameText = CustomTextField.includeInit(R.id.include_familyname, EditContactsActivity.this);
        phonetic_lNameText = CustomTextField.includeInit(R.id.include_p_familyname, EditContactsActivity.this);
        sufNameText = CustomTextField.includeInit(R.id.include_suffix, EditContactsActivity.this);
        nickNameText = CustomTextField.includeInit(R.id.include_nicname, EditContactsActivity.this);
        jobTitleNameText = CustomTextField.includeInit(R.id.include_job_title, EditContactsActivity.this);
        jobDepNameText = CustomTextField.includeInit(R.id.include_job_dep, EditContactsActivity.this);
        companyName = CustomTextField.includeInit(R.id.include_compny, EditContactsActivity.this);

        prefixNameText.setHint(getResources().getString(
                R.string.add_field_prfix));
        fNameText.setHint(getResources().getString(R.string.first_string));
        phonetic_fNameText.setHint(getResources().getString(
                R.string.add_field_pfname));
        middleNameText.setHint(getResources().getString(
                R.string.add_field_middle));
        phonetic_midNameText.setHint(getResources().getString(
                R.string.add_field_pmiddle));
        lNameText.setHint(getResources().getString(R.string.last_string));
        phonetic_lNameText.setHint(getResources().getString(
                R.string.add_field_plname));
        sufNameText.setHint(getResources().getString(R.string.add_field_suffix));
        nickNameText.setHint(getResources().getString(
                R.string.add_field_nickname));
        jobTitleNameText.setHint(getResources().getString(
                R.string.add_field_jobtitle));
        jobDepNameText.setHint(getResources().getString(
                R.string.add_field_jobdepartment));
        companyName.setHint(getResources().getString(R.string.Company_string));

        noteTxt = (EditText) findViewById(R.id.add_notes_ok);

        avatar = (CircleImageView) this.findViewById(R.id.add_photo_image);
        avatar.setOnClickListener(this);

        noteTxt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                noteTxt.setFocusableInTouchMode(true);
                noteTxt.requestFocus();
            }
        });

    }

    private void initTextField() {
        LinearLayout btnPhone = (LinearLayout) findViewById(R.id.phone_click);
        Button btnPhoneimg = (Button) findViewById(R.id.add_phone_img);
        btnPhone.setOnClickListener(this);
        btnPhoneimg.setOnClickListener(this);

        LinearLayout btnEmail = (LinearLayout) findViewById(R.id.email_click);
        Button btnEmailimg = (Button) findViewById(R.id.add_email_img);
        btnEmail.setOnClickListener(this);
        btnEmailimg.setOnClickListener(this);

        LinearLayout btnRingtone = (LinearLayout) findViewById(R.id.ringtone_click);
        ringtoneName = (TextView) findViewById(R.id.ringtone_name);
        btnRingtone.setOnClickListener(this);

        LinearLayout btnURL = (LinearLayout) findViewById(R.id.url_click);
        Button btnURLimg = (Button) findViewById(R.id.add_url_img);
        btnURL.setOnClickListener(this);
        btnURLimg.setOnClickListener(this);

        LinearLayout btnAddress = (LinearLayout) findViewById(R.id.address_click);
        Button btnAddimg = (Button) findViewById(R.id.add_address_img);
        btnAddress.setOnClickListener(this);
        btnAddimg.setOnClickListener(this);

        LinearLayout btnBday = (LinearLayout) findViewById(R.id.birthday_click);
        Button btnBdayimg = (Button) findViewById(R.id.add_birthday_img);
        btnBday.setOnClickListener(this);
        btnBdayimg.setOnClickListener(this);

        LinearLayout btnDate = (LinearLayout) findViewById(R.id.date_click);
        Button btnDateimg = (Button) findViewById(R.id.add_date_img);
        btnDate.setOnClickListener(this);
        btnDateimg.setOnClickListener(this);

        LinearLayout btnRelated = (LinearLayout) findViewById(R.id.related_click);
        Button btnRelatedimg = (Button) findViewById(R.id.add_related_img);
        btnRelated.setOnClickListener(this);
        btnRelatedimg.setOnClickListener(this);

        LinearLayout btnIM = (LinearLayout) findViewById(R.id.im_click);
        Button btnIMmg = (Button) findViewById(R.id.add_im_img);
        btnIM.setOnClickListener(this);
        btnIMmg.setOnClickListener(this);

        View addField = findViewById(R.id.add_field);
        addField.setOnClickListener(this);
    }

    private void createNoteField() {
        // TODO Auto-generated method stub

    }

    private void fillNewContactDto() {
        // Information
        contactDto = helperField.fillInformation(oldContactDto, contactDto);
    }

    @Override
    protected void onResume() {

        scrollView.post(new Runnable() {
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_UP);

                fNameText.setFocusable(true);
                fNameText.requestFocus();

                if (oldContactDto.getProfilepic() != null) {
                    /**Loading Image*/
                    contactDto.setProfilepic(oldContactDto.getProfilepic());
                    ImageCustomManager.getImageCustomManagerInstance()
                            .displayProfilePictureFromBitmap(EditContactsActivity.this, contactDto.getProfilepic(), avatar);

                }

                removePref = Util.checkAddFieldisHidden(
                        oldContactDto.getPrefix(), prefixNameText,
                        AddFieldActivity.prefisHidden);
                removePfname = Util.checkAddFieldisHidden(
                        oldContactDto.getPhonetic_fName(), phonetic_fNameText,
                        AddFieldActivity.pfirstisHidden);
                removeMiddle = Util.checkAddFieldisHidden(
                        oldContactDto.getMiddleName(), middleNameText,
                        AddFieldActivity.middleisHidden);
                removePmiddle = Util.checkAddFieldisHidden(
                        oldContactDto.getPhonetic_middleName(),
                        phonetic_midNameText, AddFieldActivity.pmiddleisHidden);
                removePLastname = Util.checkAddFieldisHidden(
                        oldContactDto.getPhonetic_lName(), phonetic_lNameText,
                        AddFieldActivity.plastisHidden);
                removeSuffix = Util.checkAddFieldisHidden(
                        oldContactDto.getSuffix(), sufNameText,
                        AddFieldActivity.suffixisHidden);
                removeNickname = Util.checkAddFieldisHidden(
                        oldContactDto.getNickname(), nickNameText,
                        AddFieldActivity.nickisHidden);
                removeJobTitle = Util.checkAddFieldisHidden(
                        oldContactDto.getJobTitle(), jobTitleNameText,
                        AddFieldActivity.jobTitleisHidden);
                removeJobDep = Util.checkAddFieldisHidden(
                        oldContactDto.getJobDep(), jobDepNameText,
                        AddFieldActivity.jobDepisHidden);

                isEditable = true;

                if (oldContactDto.getRingtone() != null) {

                    Uri localuri = Uri.parse(oldContactDto.getRingtone());
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            getApplicationContext(), localuri);

                    String defaultPath = Settings.System.DEFAULT_NOTIFICATION_URI
                            .getPath();
                    String name = ringtone.getTitle(getApplicationContext());

                    RingtoneActivity.ringtoneDto.setRintoneName(name);
                    RingtoneActivity.ringtoneDto.setIndexSeleced(0);

                    String pathRingtone = "/system/" + name;
                    System.out.println("JM ringtone " + "/system/" + name);
                    System.out.println("JM ringtoned " + defaultPath);

                    if (pathRingtone.equals(defaultPath)) {
                        ringtoneName.setText(""
                                + getResources().getString(
                                R.string.string_default));
                    } else {
                        ringtoneName.setText("" + name);
                    }

                } else {
                    ringtoneName
                            .setText(""
                                    + getResources().getString(
                                    R.string.string_default));
                }

            }
        });

        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // Phone
            case R.id.phone_click:
                if (EditHelperJMPhone.checkerForPhoneNumbers.size() > 0) {
                    if (EditHelperJMPhone.checkerForPhoneNumbers
                            .get(EditHelperJMPhone.checkerForPhoneNumbers.size() - 1)
                            .getPhoneNumber() != null) {
                        if (!EditHelperJMPhone.checkerForPhoneNumbers
                                .get(EditHelperJMPhone.checkerForPhoneNumbers.size() - 1)
                                .getPhoneNumber().isEmpty()) {
                            helperField.addPhoneFunction(contactDto);
                        } else {
                            Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    helperField.addPhoneFunction(contactDto);
                }
                break;
            case R.id.add_phone_img:
                if (EditHelperJMPhone.checkerForPhoneNumbers.size() > 0) {
                    if (EditHelperJMPhone.checkerForPhoneNumbers
                            .get(EditHelperJMPhone.checkerForPhoneNumbers.size() - 1)
                            .getPhoneNumber() != null) {
                        if (!EditHelperJMPhone.checkerForPhoneNumbers
                                .get(EditHelperJMPhone.checkerForPhoneNumbers.size() - 1)
                                .getPhoneNumber().isEmpty()) {
                            helperField.addPhoneFunction(contactDto);
                        } else {
                            Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    helperField.addPhoneFunction(contactDto);
                }
                break;

            // Email
            case R.id.email_click:
                helperField.addEmailFunction(contactDto);
                break;
            case R.id.add_email_img:
                helperField.addEmailFunction(contactDto);
                break;

            // Ringtone
            case R.id.ringtone_click:
                Intent intent = new Intent(EditContactsActivity.this,
                        RingtoneActivity.class);
                RingtoneActivity.oldContactDto = oldContactDto;
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up,
                        R.anim.slide_in_up_exit);
                break;

            // URL
            case R.id.url_click:
                helperField.addURLFunction(contactDto);
                break;
            case R.id.add_url_img:
                helperField.addURLFunction(contactDto);
                break;

            // Address
            case R.id.address_click:
                helperField.addAddressFunction(contactDto);
                break;
            case R.id.add_address_img:
                helperField.addAddressFunction(contactDto);
                break;

            // Bday
            case R.id.birthday_click:
                if (!EditHelperField.hasBirthday)
                    helperField.addBdayFunction(contactDto);
                break;
            case R.id.add_birthday_img:
                if (!EditHelperField.hasBirthday)
                    helperField.addBdayFunction(contactDto);
                break;

            // Date
            case R.id.date_click:
                helperField.addDateFunction(contactDto);
                break;
            case R.id.add_date_img:
                helperField.addDateFunction(contactDto);
                break;

            // Related
            case R.id.related_click:
                helperField.addREFunction(contactDto);
                break;
            case R.id.add_related_img:
                helperField.addREFunction(contactDto);
                break;

            // IM
            case R.id.im_click:
                helperField.addIMFunction(contactDto);
                break;
            case R.id.add_im_img:
                helperField.addIMFunction(contactDto);
                break;

            // avatar
            case R.id.add_photo_image:
                Util.adddialogProfilePic(this, contactDto, avatar, mFileTemp);
                break;

            case R.id.add_field:
                NewUtil.IntetCustomTrans(this, AddFieldActivity.class);
                break;

        }
    }

    /***/
    class saveEditedContact extends AsyncTask<String, String, String> {

        boolean response = true;

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        @Override
        protected void onPreExecute() {
            Activity activity = EditContactsActivity.this;

            String messages = getResources().getString(
                    R.string.string_diag_pleasewait);
            String messages1 = getResources().getString(
                    R.string.string_diag_loading_details);

            progressDialog = ProgressDialog.show(activity, messages, messages1
                    + " ... ");
            progressDialog.setCancelable(true);

            contactDto = quickContactHelper.removeNullValues(contactDto);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            contactFieldsSetUpInformation();

            try {
                edithelper.updateInformation(contactDto);
                edithelper.deleteArrayInfo(contactDto, oldContactDto);
                edithelper.insertArrayInfo(contactDto);
                contactDto.setRingtone(oldContactDto.getRingtone());

            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }

            // Asking the Contact provider to create a new contact
            try {

                ContentProviderResult[] results = getContentResolver()
                        .applyBatch(ContactsContract.AUTHORITY, ops);

                int contactId = Integer.parseInt(results[0].uri.getLastPathSegment());

                contactDto.setContactID(contactId + "");

                Uri contactUri = ContactsContract.Contacts.CONTENT_URI;
                ContentValues values = new ContentValues();
                values.put(ContactsContract.Contacts.CUSTOM_RINGTONE,
                        contactDto.getRingtone());
                getContentResolver()
                        .update(contactUri,
                                values,
                                ContactsContract.Contacts._ID + " = ?",
                                new String[]{String.valueOf(contactDto
                                        .getContactID())});

                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                runOnUiThread(new Runnable() { // Run on UI thread
                    @Override
                    public void run() {
                        imm.hideSoftInputFromWindow(fNameText.getWindowToken(), 0);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            if (response == true) {

                Toast.makeText(EditContactsActivity.this, "Changes Saved",
                        Toast.LENGTH_LONG).show();

                if (isFromContactDetailsPage == true) {
                    ContactDetailActivity.contactDto = contactDto;
                }

                if (contactDto.getAccountType().equals("com.google")) {
                    Util.syncContacts(getApplicationContext());
                }

                if (LogsDetail.removeinfoActiviy) {
                    Var.logActivity.finish();
                    DialerFragment.fromKeypad = false;
                }

                onBackPressed();

            } else {
                Toast.makeText(EditContactsActivity.this,
                        "Error Updating contact", Toast.LENGTH_LONG).show();

            }
            progressDialog = null;
            super.onPostExecute(result);
        }
    }

    public void contactFieldsSetUpInformation() {

        contactDto.setAccountType(oldContactDto.getAccountType());
        contactDto.setAccountName(oldContactDto.getAccountName());

        contactDto.setPrefix(prefixNameText.getText().toString().trim());
        contactDto.setFirstName(fNameText.getText().toString().trim());
        contactDto.setMiddleName(middleNameText.getText().toString().trim());
        contactDto.setLastName(lNameText.getText().toString().trim());
        contactDto.setPhonetic_fName(phonetic_fNameText.getText().toString()
                .trim());
        contactDto.setPhonetic_middleName(phonetic_midNameText.getText()
                .toString().trim());
        contactDto.setPhonetic_lName(phonetic_lNameText.getText().toString()
                .trim());
        contactDto.setSuffix(sufNameText.getText().toString().trim());
        contactDto.setNickname(nickNameText.getText().toString().trim());
        contactDto.setJobTitle(jobTitleNameText.getText().toString().trim());
        contactDto.setJobDep(jobDepNameText.getText().toString().trim());
        contactDto.setCompany(companyName.getText().toString().trim());

        contactDto.setNote(noteTxt.getText().toString().trim());
    }

}
