package com.katadigital.owtel.modules.main.contacts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Item;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Row;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Section;
import com.katadigital.owtel.modules.main.contacts.adapters.IndexedListAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.TestSectionedAdapter;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.ui.pinnedheader.PinnedHeaderListView;

public class AddRelatedActivity extends Activity implements
        LoaderCallbacks<Cursor>, OnClickListener {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    SimpleCursorAdapter mAdapter = null;
    private String mCurrentFilter = null;
    ArrayList<String> contactsArray;
    PinnedHeaderListView contact_listview;

    TextView myNumber;

    private static final String[] CONTACTS_SUMMARY_PROJECTION = new String[]{
            Contacts._ID, Contacts.DISPLAY_NAME, Contacts.LOOKUP_KEY};
    private String[] PHOTO_BITMAP_PROJECTION = new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO};
    QuickContactHelper quickContactHelper;
    private ProgressDialog progressDialog;
    String searchedName = "";

    String[] let = {"#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
            "X", "Y", "Z",};

    // ALPHABET LISTVIEW VARIABLES
    private AlphabetListAdapter adapter;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private int sideIndexHeight;
    private static float sideIndexX;
    private static float sideIndexY;

    private int indexListSize;
    ArrayList<Integer> contactDtoIDList = new ArrayList<Integer>();

    // Search
    TextView txt_center_dummy, cancel_search;
    RelativeLayout header;
    InputMethodManager imm;

    public static boolean isSearchClick = false;
    EditText searchContacts;

    public static EditText editTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_related_layout);
        initActionBar();
        initData();

        try {
            getActionBar().hide();
        } catch (Exception e) {
            Log.e(TAG, "onCreate: " + e.getMessage());
        }
        searchContacts = (EditText) findViewById(R.id.search_favorite_contacts);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        header = (RelativeLayout) findViewById(R.id.contacts_actionbar);
        cancel_search = (TextView) findViewById(R.id.cancel);
        txt_center_dummy = (TextView) findViewById(R.id.txt_search_favorite_contacts);

        View layout_search = findViewById(R.id.layout_search);
        layout_search.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                searchContacts.setVisibility(View.VISIBLE);
                txt_center_dummy.setVisibility(View.GONE);
                searchContacts.requestFocus();

                imm.showSoftInput(searchContacts,
                        InputMethodManager.SHOW_IMPLICIT);
            }
        });

        txt_center_dummy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                searchContacts.setVisibility(View.VISIBLE);
                txt_center_dummy.setVisibility(View.GONE);
                searchContacts.requestFocus();

                imm.showSoftInput(searchContacts,
                        InputMethodManager.SHOW_IMPLICIT);
            }
        });

        cancel_search.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(searchContacts.getWindowToken(), 0);
                searchContacts.setText("");
                isSearchClick = false;
                // Keyboard();
            }
        });

        contact_listview.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(searchContacts.getWindowToken(), 0);
                isSearchClick = true;
                return false;
            }
        });

        searchContacts.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
                AddRelatedActivity.this.mAdapter.getFilter().filter(cs);
                searchedName = cs + "";

                mCurrentFilter = (String) (!TextUtils.isEmpty(searchedName) ? searchedName
                        : null);
                getLoaderManager().restartLoader(0, null,
                        AddRelatedActivity.this);

            }

            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        searchContacts.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null
                        && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    in.hideSoftInputFromWindow(
                            searchContacts.getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    // Must return true here to consume event
                    return true;

                }
                return false;
            }
        });

        searchContacts.setOnKeyListener(new OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        searchContacts.setVisibility(View.VISIBLE);
                        txt_center_dummy.setVisibility(View.GONE);
                        searchContacts.requestFocus();
                        isSearchClick = true;

                    }
                }

                return false;
            }
        });

        // contact_listview.setOnItemClickListener(new OnItemClickListener() {
        // public void onItemClick(AdapterView<?> myAdapter, View myView,
        // int myItemInt, long mylng) {
        //
        // if (contactDtoIDList.get(myItemInt) > 0) {
        // Util.syncContacts(AddRelatedActivity.this);
        //
        // if (progressDialog == null) {
        // new loadContactDetails(contactDtoIDList.get(myItemInt))
        // .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        // }
        //
        // }
        // }
        // });

        Keyboard();

    }

    private void initActionBar() {

        // String rigtItem = getResources().getString(R.string.cancel_string);
        // cancelNav = CustomActionBar.viewCustomActionBarNew(this, "", "",
        // rigtItem);
        // cancelNav.setOnClickListener(new OnClickListener() {
        //
        // @Override
        // public void onClick(View v) {
        // onBackPressed();
        // }
        // });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:

                break;

            case R.id.txt_search_favorite_contacts:
                // searchContacts.setVisibility(View.VISIBLE);
                // txt_search.setVisibility(View.GONE);
                // searchContacts.requestFocus();
                //
                // imm.showSoftInput(searchContacts,
                // InputMethodManager.SHOW_IMPLICIT);
                break;

            case R.id.layout_search:

                break;

            default:
                break;
        }

    }

    private void Keyboard() {
        final View activityRootView = findViewById(R.id.rootView);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(
                new OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        Rect r = new Rect();
                        activityRootView.getWindowVisibleDisplayFrame(r);
                        int heightDiff = activityRootView.getRootView()
                                .getHeight() - (r.bottom - r.top);

                        if (heightDiff > 100) {

                            // getActionBar().hide();
                            header.setVisibility(View.GONE);
                            cancel_search.setVisibility(View.VISIBLE);
                            searchContacts.setVisibility(View.VISIBLE);
                            txt_center_dummy.setVisibility(View.GONE);
                            searchContacts.requestFocus();
                        } else {
                            if (isSearchClick) {
                                if (searchContacts.length() == 0) {
                                    searchContacts.setText("");
                                    isSearchClick = false;
                                }
                            } else {
                                if (searchContacts.length() > 0) {
                                    searchContacts.setText("");
                                    isSearchClick = false;
                                } else {
                                    header.setVisibility(View.VISIBLE);
                                    cancel_search.setVisibility(View.GONE);
                                    searchContacts.setVisibility(View.GONE);
                                    txt_center_dummy
                                            .setVisibility(View.VISIBLE);
                                }

                            }
                        }
                    }
                });
    }

    private void initData() {
        quickContactHelper = new QuickContactHelper(this);
        getLoaderManager().initLoader(0, null, AddRelatedActivity.this);

        mAdapter = new IndexedListAdapter(this, R.layout.list_item_contacts,
                null, new String[]{ContactsContract.Contacts.DISPLAY_NAME,
                Contacts._ID}, new int[]{R.id.display_name});
        mAdapter.notifyDataSetChanged();

        contact_listview = (PinnedHeaderListView) findViewById(R.id.contact_list);

    }

    @Override
    protected void onResume() {
        contact_listview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView,
                                    int myItemInt, long mylng) {
                if (contactDtoIDList.get(myItemInt) > 0) {
                    int idNum = contactDtoIDList.get(myItemInt);
                    String Name = QuickContactHelper.getDisplayName(
                            AddRelatedActivity.this, String.valueOf(idNum));
                    editTxt.setText(Name);
                    onBackPressed();
                }
            }
        });
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.slide_out_down);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
        Uri baseUri;
        if (mCurrentFilter != null) {
            baseUri = Uri.withAppendedPath(Contacts.CONTENT_FILTER_URI,
                    Uri.encode(mCurrentFilter));
        } else {
            baseUri = Contacts.CONTENT_URI;
        }

        String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + Contacts.DISPLAY_NAME + " != '' ))";

        String[] projection = new String[]{Contacts._ID,
                Contacts.DISPLAY_NAME, Contacts.CONTACT_STATUS,
                Contacts.CONTACT_PRESENCE, Contacts.PHOTO_ID,
                Contacts.LOOKUP_KEY,};

        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";

        CursorLoader cursorLoader = new CursorLoader(this, baseUri,
                projection, select, null, sortOrder);

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        System.out.println("On loader finished");

        adapter = new AlphabetListAdapter(this);
        alphabet = new ArrayList<Object[]>();
        sections = new HashMap<String, Integer>();

        List<Row> rows = new ArrayList<Row>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Object[] tmpIndexItem = null;
        Pattern numberPattern = Pattern.compile("[0-9]");
        contactDtoIDList = new ArrayList<Integer>();

        TestSectionedAdapter secAdapter;
        ArrayList<String> listDataHeader = new ArrayList<String>();
        HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();
        List<String> items = new ArrayList<String>();

        while (data.moveToNext()) {

            String contactDisplayName = data.getString(data
                    .getColumnIndex(Contacts.DISPLAY_NAME));

            int contactid = data.getInt(data.getColumnIndex(Contacts._ID));

            String firstLetter = contactDisplayName.substring(0, 1)
                    .toUpperCase();

            // Group numbers together in the scroller
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }

            // If we've changed to a new letter, add the previous letter to the
            // alphabet scroller
            if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                end = rows.size() - 1;
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase();
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = end;
                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equals(previousLetter)) {
                rows.add(new Section(firstLetter));
                sections.put(firstLetter, start);
                contactDtoIDList.add(0);
                listDataHeader.add(firstLetter);
                items = new ArrayList<String>();
            }

            // Add the country to the list
            contactDtoIDList.add(contactid);
            items.add(contactDisplayName);
            rows.add(new Item(contactDisplayName));
            if (!firstLetter.equals(previousLetter)) {
                listDataChild.put(firstLetter, items);
            }
            previousLetter = firstLetter;

        }

        if (previousLetter != null) {
            // Save the last letter
            tmpIndexItem = new Object[3];
            tmpIndexItem[0] = previousLetter.toUpperCase();
            tmpIndexItem[1] = start;
            tmpIndexItem[2] = rows.size() - 1;
            alphabet.add(tmpIndexItem);
        }

        adapter.setRows(rows);
        secAdapter = new TestSectionedAdapter(listDataHeader, listDataChild);
        contact_listview.setAdapter(secAdapter);
        updateList();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        System.out.println("onLoader reset");
        mAdapter.swapCursor(null);
    }

    public void initComponents() {
        searchContacts = (EditText) findViewById(R.id.search_contacts);
        contact_listview = (PinnedHeaderListView) findViewById(R.id.contact_list);
    }

    class loadContactDetails extends AsyncTask<String, String, String> {
        long contactID;

        public loadContactDetails(long contactID) {
            this.contactID = contactID;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            progressDialog = ProgressDialog.show(
                    AddRelatedActivity.this,
                    getResources().getString(R.string.string_diag_pleasewait),
                    getResources().getString(
                            R.string.string_diag_loading_details)
                            + "...");
            progressDialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            Intent intent = new Intent(AddRelatedActivity.this,
                    ContactDetailActivity.class);
            startActivity(intent);
            AddRelatedActivity.this.overridePendingTransition(
                    R.anim.slide_in_up, R.anim.slide_in_up_exit);
            progressDialog = null;
            super.onPostExecute(result);
        }
    }

    TextView tmpTV;
    LinearLayout sideIndex;

    public void updateList() {

        sideIndex = (LinearLayout) AddRelatedActivity.this
                .findViewById(R.id.sideIndex);
        sideIndex.removeAllViews();

        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }

        int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

        for (int l = 0; l < let.length; l++) {
            boolean isExist = false;
            for (double i = 1; i <= indexListSize; i = i + delta) {
                Object[] tmpIndexItem = alphabet.get((int) i - 1);
                String tmpLetter = tmpIndexItem[0].toString();

                if (let[l].equalsIgnoreCase(tmpLetter)) {
                    showLetters(let[l]);
                    isExist = true;
                    break;
                }

            }
            if (!isExist) {
                showLetters(let[l]);
            }

        }

        sideIndexHeight = sideIndex.getHeight();

        sideIndex.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // now you know coordinates of touch
                sideIndexX = event.getX();
                sideIndexY = event.getY();

                // and can display a proper item it country list
                displayListItem();

                return false;
            }
        });

    }

    public void showLetters(String let) {
        tmpTV = new TextView(AddRelatedActivity.this);
        tmpTV.setText(let);
        tmpTV.setGravity(Gravity.CENTER);
        tmpTV.setTextSize(9);
        tmpTV.setTypeface(NewUtil.getFontRoman(this));
        tmpTV.setTextColor(AddRelatedActivity.this.getResources().getColor(
                R.color.phone_app_blue));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        tmpTV.setLayoutParams(params);
        sideIndex.addView(tmpTV);
    }

    public void displayListItem() {
        LinearLayout sideIndex = (LinearLayout) AddRelatedActivity.this
                .findViewById(R.id.sideIndex);
        sideIndexHeight = sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition < alphabet.size()) {
            Object[] indexItem = alphabet.get(itemPosition);
            int subitemPosition = sections.get(indexItem[0]);

            // ListView listView = (ListView) findViewById(android.R.id.list);
            contact_listview.setSelection(subitemPosition);
        }
    }

}
