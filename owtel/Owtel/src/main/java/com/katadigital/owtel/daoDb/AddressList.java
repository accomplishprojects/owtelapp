package com.katadigital.owtel.daoDb;

import com.google.gson.Gson;

import org.parceler.Parcel;

/**
 * Created by Omar Matthew Reyes on 4/26/16.
 * DAO for Address Adapter
 */
@Parcel
public class AddressList {
    public String country;
    public String countryId;
    public String state;
    public String stateId;
    public String city;
    public String cityId;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
