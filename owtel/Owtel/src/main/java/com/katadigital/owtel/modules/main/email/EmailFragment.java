package com.katadigital.owtel.modules.main.email;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.models.ValidateCallObject;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.ProgressDialogHelper;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by Jose Mari Lumanlan on 9/07/16.
 * Change Email Fragment
 */
public class EmailFragment extends Fragment implements ISimpleDialogListener {
    public final static String TAG = "PromoCodeFragment";
    private static final int CHANGE_EMAIL_PASSWORD = 897123;
    private MainActivity mainActivity;
    EditText editTextNewEmail;
    EditText editTextPasswordConfirmation;
    TextView tvExistingEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        ButterKnife.bind(getActivity());
        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");
        mainActivity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_email, container, false);
        ImageButton mButtonBack = (ImageButton) view.findViewById(R.id.btn_settings_back);
        mButtonBack.setOnClickListener(v -> mainActivity.popFragment());
        mButtonBack.setVisibility(View.VISIBLE);
        tvExistingEmail = (TextView) view.findViewById(R.id.tv_email_existing);
        editTextNewEmail = (EditText) view.findViewById(R.id.et_new_email);
        editTextPasswordConfirmation = (EditText) view.findViewById(R.id.et_password_change_email_confirmation);
        editTextPasswordConfirmation.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        editTextPasswordConfirmation.setTypeface(Typeface.DEFAULT);
        tvExistingEmail.setText(email);
        Button button = (Button) view.findViewById(R.id.btn_change_email);
        button.setOnClickListener(v -> {
            SimpleDialogFragment.createBuilder(getActivity(), getActivity().getSupportFragmentManager()).setTitle("Change Email").setMessage("Are you sure you want to change email address to " + editTextNewEmail.getText().toString() + "?").setTargetFragment(EmailFragment.this, CHANGE_EMAIL_PASSWORD).setPositiveButtonText(getString(R.string.ok)).setNegativeButtonText(getString(R.string.cancel)).show();
        });
        return view;
    }


    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {

        if (tvExistingEmail.getText().length() == 0 || editTextPasswordConfirmation.getText().length() == 0 || editTextNewEmail.getText().length() == 0) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.validate_change_email_error_message), Toast.LENGTH_SHORT).show();
        } else {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(API.URL_TEST)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            ProgressDialogHelper.showDialog(getContext());
            ApiService service = retrofit.create(ApiService.class);
            service.changeEmail(tvExistingEmail.getText().toString(), editTextNewEmail.getText().toString(), editTextPasswordConfirmation.getText().toString()).enqueue(new Callback<ValidateCallObject>() {
                @Override
                public void onResponse(Response<ValidateCallObject> response, Retrofit retrofit) {
                    if (response.body().getStatus().equals("Success")) {
                        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
                        SharedPreferences.Editor defaultPreferenceEditor = preferencesUser.edit();
                        defaultPreferenceEditor.putString(Constants.PREFS_USER_EMAIL, editTextNewEmail.getText().toString());
                        Toast.makeText(getActivity(), "Email successfully changed!", Toast.LENGTH_SHORT).show();
                        editTextNewEmail.setText("");
//                    mainActivity.popFragment();
                        new ApiHelper(getActivity()).doLogout();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    ProgressDialogHelper.hideDialog();
                }

                @Override
                public void onFailure(Throwable t) {
                    ProgressDialogHelper.hideDialog();
                }
            });
        }
    }
}

