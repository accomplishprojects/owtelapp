package com.katadigital.owtel.modules.registration;

/**
 * Created by Omar Matthew Reyes on 7/18/16.
 */
public interface PaymentInteractor {
    boolean validEtCardNumber(boolean valid);
    boolean validEtCardHolder(boolean valid);
    boolean validSpinnerMonth(boolean valid);
    boolean validSpinnerYear(boolean valid);
    boolean validEtCvv(boolean valid);
    boolean validEtStreet(boolean valid);
    boolean validEtCity(boolean valid);
    boolean validEtState(boolean valid);
    boolean validSpinnerCountry(boolean valid);
    boolean validEtZipCode(boolean valid);
}
