package com.katadigital.owtel.helpers;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Nickname;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.RawContacts;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.DateDto;
import com.katadigital.owtel.modules.main.contacts.entities.EmailDto;
import com.katadigital.owtel.modules.main.contacts.entities.IMDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.entities.RelationDto;
import com.katadigital.owtel.modules.main.contacts.entities.WebsiteDto;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.portsip.utilities.logs.CallLogsManager;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class QuickContactHelper {
    private static final String TAG = "QuickContactHelper";

    private static final String[] PHOTO_ID_PROJECTION = new String[]{ContactsContract.Contacts.PHOTO_ID};

    private static final String[] PHOTO_BITMAP_PROJECTION = new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO};
    ContactDto contactDto;
    private ImageView badge;

    // private String phoneNumber;
    static Context context;
    private static ContentResolver contentResolver;

    public QuickContactHelper(Context context) {
        this.context = context;
        contentResolver = context.getContentResolver();
    }

    public static Bitmap addThumbnail(String id) {
        Bitmap thumbnail = null;
        Integer thumbnailId = null;
        try {
            thumbnailId = fetchThumbnailId(id);
        } catch (Exception e) {
            if (GlobalValues.DEBUG) Log.e(TAG, "addThumbnail " + e);
        }
        if (thumbnailId != null) {
            thumbnail = fetchThumbnail(thumbnailId);
        }
        return thumbnail;

    }

    public ContactDto getFavContactDetails(String contactID) {// favorite
        ContactDto contactDto = new ContactDto();
        contactDto.setContactID(contactID);
        Cursor cursor = null;
        try {

            cursor = contentResolver.query(RawContacts.CONTENT_URI,
                    new String[]{RawContacts.ACCOUNT_NAME,
                            RawContacts.ACCOUNT_TYPE}, RawContacts.CONTACT_ID
                            + "=?", new String[]{String.valueOf(contactID)},
                    null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();

                contactDto.setAccountName(cursor.getString(cursor
                        .getColumnIndex(RawContacts.ACCOUNT_NAME)));
                contactDto.setAccountType(cursor.getString(cursor
                        .getColumnIndex(RawContacts.ACCOUNT_TYPE)));
                cursor.close();
            }
        } catch (Exception e) {
            System.out.println("" + e);
        } finally {
            cursor.close();
        }
        // /////////////////////name
        String nameWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?";
        String[] nameWhereParams = new String[]{String.valueOf(contactID),
                StructuredName.CONTENT_ITEM_TYPE};
        Cursor nameCur = context.getContentResolver().query(Data.CONTENT_URI,
                null, nameWhere, nameWhereParams, null);

        if (nameCur.moveToNext()) {

            contactDto.setPrefix(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PREFIX)));
            contactDto.setFirstName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.GIVEN_NAME)));
            contactDto.setPhonetic_fName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PHONETIC_GIVEN_NAME)));
            contactDto.setMiddleName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.MIDDLE_NAME)));
            contactDto.setPhonetic_middleName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PHONETIC_MIDDLE_NAME)));
            contactDto.setLastName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.FAMILY_NAME)));
            contactDto.setPhonetic_lName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PHONETIC_FAMILY_NAME)));
            contactDto.setSuffix(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.SUFFIX)));

        }
        nameCur.close();
        // /////////Image
        contactDto.setProfilepic(addThumbnail(String.valueOf(contactID)));
        // /////////DisplayName
        String[] projection = new String[]{Phone.DISPLAY_NAME, Phone.NUMBER,
                Phone.TYPE};
        final Cursor phoneCursor = context.getContentResolver().query(
                Phone.CONTENT_URI, projection, Data.CONTACT_ID + "=?",
                new String[]{String.valueOf(contactID)}, null);

        if (phoneCursor.moveToFirst()) {
            do {
                contactDto.setDisplayName(phoneCursor.getString(phoneCursor
                        .getColumnIndex(Phone.DISPLAY_NAME)) == null ? ""
                        : phoneCursor.getString(phoneCursor
                        .getColumnIndex(Phone.DISPLAY_NAME)));
                break;
            } while (phoneCursor.moveToNext());
        }
        phoneCursor.close();
        return contactDto;
    }

    public boolean getContactPNumber(String contactID) {
        boolean res = false;
        ContactDto contactDto = new ContactDto();
        contactDto.setContactID(contactID);
        String[] projection = new String[]{Phone.DISPLAY_NAME, Phone.NUMBER,
                Phone.TYPE};
        final Cursor phoneCursor = context.getContentResolver().query(
                Phone.CONTENT_URI, projection, Data.CONTACT_ID + "=?",
                new String[]{String.valueOf(contactID)}, null);

        if (phoneCursor.moveToFirst()) {

            do {

                int contactNumberColumnIndex = phoneCursor
                        .getColumnIndex(Phone.NUMBER);

                PhoneNumberDto phoneNumberDto = new PhoneNumberDto();

                phoneNumberDto.setNumber(phoneCursor
                        .getString(contactNumberColumnIndex) == null ? ""
                        : phoneCursor.getString(contactNumberColumnIndex));
                if (!phoneNumberDto.getNumber().isEmpty()) {
                    res = true;
                    break;
                }

                // if(phoneCursor.getString(contactNumberColumnIndex) != null)
                // res = true;

            } while (phoneCursor.moveToNext());
        }
        phoneCursor.close();

        return res;
    }

    public int hasnumber(String contactID) {

        int hasnum = 0;
        // try {

        String nameWhere = Contacts._ID + " = ?";
        String[] nameWhereParams = new String[]{String.valueOf(contactID)};
        Cursor c = context.getContentResolver().query(Contacts.CONTENT_URI,
                null, nameWhere, nameWhereParams, null);
        c.moveToFirst();
        System.out
                .println("BERTAX HAS NUMBER "
                        + c.getInt(c
                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
        hasnum = c.getInt(c
                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
        c.close();
        // } catch (Exception e) {
        // hasnum = 1;
        // }

        return hasnum;
    }

    public static Integer fetchThumbnailId(String id) {
        Cursor cursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null, ContactsContract.Data.CONTACT_ID + "=" + id + " AND " +
                        ContactsContract.Data.MIMETYPE + "='" +
                        ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null, null);
        try {
            Integer thumbnailId = null;
            if (cursor.moveToFirst()) {
                thumbnailId = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
            }
            return thumbnailId;
        } catch (NullPointerException e) {
            Log.e(TAG, "fetchThumbnailId " + e);
            return null;
        } finally {
            try {
                cursor.close();
            } catch (NullPointerException e) {
                Log.e(TAG, "fetchThumbnailId " + e);
            }
        }
    }

    public static Bitmap fetchThumbnail(final int thumbnailId) {
        Bitmap thumbnail = null;
        final Uri uri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, thumbnailId);
        final Cursor cursor = contentResolver.query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);
        try {
            if (cursor.moveToFirst()) {
                final byte[] thumbnailBytes = cursor.getBlob(0);
                if (thumbnailBytes != null) {
                    thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes,
                            0, thumbnailBytes.length);
                    Log.e(TAG, "fetchThumbnail: ");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "fetchThumbnail: " + e.getMessage());
        } finally {
            cursor.close();
        }
        return thumbnail;
    }

    // CONTACTS OPERATION
    public boolean updateContact(String name, String number, String email,
                                 String ContactId, Context context) {
        boolean success = true;
        String phnumexp = "^[0-9]*$";

        try {
            name = name.trim();
            email = email.trim();
            number = number.trim();

            if (name.equals("") && number.equals("") && email.equals("")) {
                success = false;
            } else if ((!number.equals("")) && (!match(number, phnumexp))) {
                success = false;
            } else if ((!email.equals("")) && (!isEmailValid(email))) {
                success = false;
            } else {
                ContentResolver contentResolver = context.getContentResolver();

                String where = ContactsContract.Data.CONTACT_ID + " =? AND "
                        + ContactsContract.Data.MIMETYPE + " =?";

                String[] emailParams = new String[]{
                        ContactId,
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE};
                String[] nameParams = new String[]{
                        ContactId,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE};
                String[] numberParams = new String[]{
                        ContactId,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE};

                ArrayList<android.content.ContentProviderOperation> ops = new ArrayList<android.content.ContentProviderOperation>();

                if (!email.equals("")) {
                    ops.add(android.content.ContentProviderOperation
                            .newUpdate(
                                    android.provider.ContactsContract.Data.CONTENT_URI)
                            .withSelection(where, emailParams)
                            .withValue(Email.DATA, email).build());

                }

                if (!name.equals("")) {
                    ops.add(android.content.ContentProviderOperation
                            .newUpdate(
                                    android.provider.ContactsContract.Data.CONTENT_URI)
                            .withSelection(where, nameParams)
                            .withValue(StructuredName.DISPLAY_NAME, name)
                            .build());

                }

                if (!number.equals("")) {

                    ops.add(android.content.ContentProviderOperation
                            .newUpdate(
                                    android.provider.ContactsContract.Data.CONTENT_URI)
                            .withSelection(where, numberParams)
                            .withValue(Phone.NUMBER, number).build());
                }
                contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
            }
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    private boolean isEmailValid(String email) {
        String emailAddress = email.toString().trim();
        if (emailAddress == null)
            return false;
        else if (emailAddress.equals(""))
            return false;
        else if (emailAddress.length() <= 6)
            return false;
        else {
            String expression = "^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\\.([a-z][a-z|0-9]*(\\.[a-z][a-z|0-9]*)?)$";
            CharSequence inputStr = emailAddress;
            Pattern pattern = Pattern.compile(expression,
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches())
                return true;
            else
                return false;
        }
    }

    private boolean match(String stringToCompare, String regularExpression) {
        boolean success = false;
        Pattern pattern = Pattern.compile(regularExpression);
        Matcher matcher = pattern.matcher(stringToCompare);
        if (matcher.matches())
            success = true;
        return success;
    }

    // Edit Jeff
    public ContactDto getContactDetails(String contactID) {

        ContactDto contactDto = new ContactDto();
        contactDto.setContactID(contactID);

        // Jeff
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(RawContacts.CONTENT_URI,
                    new String[]{RawContacts.ACCOUNT_NAME, RawContacts.ACCOUNT_TYPE},
                    RawContacts.CONTACT_ID + "=?",
                    new String[]{String.valueOf(contactID)},
                    null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();

                contactDto.setAccountName(cursor.getString(cursor.getColumnIndex(RawContacts.ACCOUNT_NAME)));
                contactDto.setAccountType(cursor.getString(cursor.getColumnIndex(RawContacts.ACCOUNT_TYPE)));
                cursor.close();
            }
        } catch (Exception e) {
            System.out.println("" + e);
        } finally {
            cursor.close();
        }

        Cursor cur = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                // Projection
                new String[]{ContactsContract.CommonDataKinds.Phone.CUSTOM_RINGTONE, ContactsContract.Contacts._ID},
                // Where
                ContactsContract.Contacts._ID + " = ?",
                // Selection
                new String[]{String.valueOf(contactID)},
                // Order
                null);

        String ring = null;
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                ring = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CUSTOM_RINGTONE));
                contactDto.setRingtone(ring);
                System.out.println("JM Ringtone " + ring);

            }
        }
        cur.close();

        String number = null;
        int numType = 0;

        String numWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
        String[] numWhereParams = new String[]{String.valueOf(contactID),
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE};

        Cursor numCur = context.getContentResolver()
                .query(ContactsContract.Data.CONTENT_URI,
                        null, numWhere, numWhereParams, null);
        while (numCur.moveToNext()) {
            do {
                PhoneNumberDto num = new PhoneNumberDto();
                num.setNumber(numCur.getString(numCur
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA)) == null ? ""
                        : numCur.getString(numCur.getColumnIndex(Phone.DATA)));

                if (!num.getNumber().isEmpty()) {
                    numType = numCur.getInt(numCur.getColumnIndex(Phone.TYPE));

                    switch (numType) {
                        case Phone.TYPE_MOBILE:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[0]);
                            break;

                        case Phone.TYPE_WORK:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[1]);
                            break;

                        case Phone.TYPE_HOME:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[2]);
                            break;

                        case Phone.TYPE_MAIN:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[3]);
                            break;

                        case Phone.TYPE_FAX_WORK:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[4]);
                            break;

                        case Phone.TYPE_FAX_HOME:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[5]);
                            break;

                        case Phone.TYPE_PAGER:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[6]);
                            break;

                        case Phone.TYPE_OTHER:
                            num.setNumberType(context.getResources()
                                    .getStringArray(R.array.numberspinner)[7]);
                            break;

                        default:
                            // In case na may custom
                            num.setNumberType(numCur.getString(numCur
                                    .getColumnIndex(Phone.LABEL)));
                            break;
                    }

                    contactDto.addPhoneNumbers(num);
                }

            } while (numCur.moveToNext());

        }
        numCur.close();

        String nameWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE + " = ?";
        String[] nameWhereParams = new String[]{String.valueOf(contactID), StructuredName.CONTENT_ITEM_TYPE};
        Cursor nameCur = context.getContentResolver().query(Data.CONTENT_URI,
                null, nameWhere, nameWhereParams, null);

        if (nameCur.moveToNext()) {
            contactDto.setPrefix(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PREFIX)));
            contactDto.setFirstName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.GIVEN_NAME)));
            contactDto.setPhonetic_fName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PHONETIC_GIVEN_NAME)));
            contactDto.setMiddleName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.MIDDLE_NAME)));
            contactDto.setPhonetic_middleName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PHONETIC_MIDDLE_NAME)));
            contactDto.setLastName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.FAMILY_NAME)));
            contactDto.setPhonetic_lName(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.PHONETIC_FAMILY_NAME)));
            contactDto.setSuffix(nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.SUFFIX)));
        }
        nameCur.close();

        String noteWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE + " = ?";
        String[] noteWhereParams = new String[]{String.valueOf(contactID), Note.CONTENT_ITEM_TYPE};
        Cursor noteCur = context.getContentResolver().query(Data.CONTENT_URI,
                null, noteWhere, noteWhereParams, null);
        if (noteCur.moveToNext()) {
            contactDto.setNote(noteCur.getString(noteCur
                    .getColumnIndex(Note.NOTE)) == null ? "" : noteCur
                    .getString(noteCur.getColumnIndex(Note.NOTE)));
        }
        noteCur.close();

        // ------------------------------------------------------ IM
        int imProtocol = 0;
        String websiteWhere2 = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE + " = ?";
        String[] websiteWhereParams2 = new String[]{String.valueOf(contactID), Im.CONTENT_ITEM_TYPE};
        Cursor websiteCur2 = context.getContentResolver()
                .query(Data.CONTENT_URI, null, websiteWhere2, websiteWhereParams2, null);
        while (websiteCur2.moveToNext()) {
            do {
                IMDto imDto = new IMDto();
                imDto.setIm(websiteCur2.getString(websiteCur2
                        .getColumnIndex(Im.DATA)) == null ? "" : websiteCur2
                        .getString(websiteCur2.getColumnIndex(Im.DATA)));

                if (!imDto.getIm().isEmpty()) {
                    imProtocol = websiteCur2.getInt(websiteCur2
                            .getColumnIndex(Im.PROTOCOL));

                    switch (imProtocol) {

                        case Im.PROTOCOL_AIM:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[0]);
                            break;
                        case Im.PROTOCOL_MSN:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[1]);
                            break;

                        case Im.PROTOCOL_YAHOO:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[2]);
                            break;

                        case Im.PROTOCOL_SKYPE:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[3]);
                            break;

                        case Im.PROTOCOL_GOOGLE_TALK:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[4]);
                            break;

                        case Im.PROTOCOL_ICQ:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[5]);
                            break;

                        case Im.PROTOCOL_JABBER:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[6]);
                            break;

                        case Im.PROTOCOL_NETMEETING:
                            imDto.setImProtocol(context.getResources()
                                    .getStringArray(R.array.imspinner)[7]);
                            break;
                    }
                    contactDto.addIM(imDto);
                }
            } while (websiteCur2.moveToNext());
        }
        websiteCur2.close();

        // Email

        String email = null;
        int emailType = 0;

        String emailWhere = ContactsContract.Data.CONTACT_ID + " = ? AND "
                + ContactsContract.Data.MIMETYPE + " = ?";
        String[] emailWhereParams = new String[]{String.valueOf(contactID),
                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE};
        Cursor emailCur = context.getContentResolver().query(
                ContactsContract.Data.CONTENT_URI, null, emailWhere,
                emailWhereParams, null);
        while (emailCur.moveToNext()) {
            do {
                EmailDto emailDto = new EmailDto();
                emailDto.setEmail(emailCur.getString(emailCur
                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)) == null ? ""
                        : emailCur.getString(emailCur
                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));

                if (!emailDto.getEmail().isEmpty()) {
                    emailType = emailCur
                            .getInt(emailCur
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

                    System.out.println(email + " <---- Email baby");

                    switch (emailType) {

                        case Email.TYPE_HOME:
                            // do something with the Work number here...

                            emailDto.setEmailType(context.getResources()
                                    .getStringArray(R.array.emailspinner)[0]);
                            break;
                        case Email.TYPE_WORK:
                            // do something with the Mobile number here...
                            emailDto.setEmailType(context.getResources()
                                    .getStringArray(R.array.emailspinner)[1]);
                            break;

                        case Email.TYPE_OTHER:
                            // do something with the Work number here...
                            emailDto.setEmailType(context.getResources()
                                    .getStringArray(R.array.emailspinner)[2]);
                            break;

                        default:

                            // In case na may custom
                            emailDto.setEmailType(emailCur.getString(emailCur
                                    .getColumnIndex(Email.LABEL)));

                            break;

                    }

                    contactDto.addEmails(emailDto);
                }

            } while (emailCur.moveToNext());

        }
        emailCur.close();

        // ------------------------------------------------------ NICKNAME

        String nickWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?";
        String[] nickWhereParams = new String[]{String.valueOf(contactID),
                Nickname.CONTENT_ITEM_TYPE};
        Cursor nickCur = context.getContentResolver().query(Data.CONTENT_URI,
                null, nickWhere, nickWhereParams, null);
        while (nickCur.moveToNext()) {

            contactDto.setNickname(nickCur.getString(nickCur
                    .getColumnIndex(Nickname.NAME)));
        }
        nickCur.close();

        // ------------------------------------------------------ JOB

        String orgWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?";
        String[] orgWhereParams = new String[]{String.valueOf(contactID),
                Organization.CONTENT_ITEM_TYPE};
        Cursor orgCur = context.getContentResolver().query(Data.CONTENT_URI,
                null, orgWhere, orgWhereParams, null);
        while (orgCur.moveToNext()) {

            contactDto.setJobTitle(orgCur.getString(orgCur
                    .getColumnIndex(Organization.DATA)) == null ? "" : orgCur
                    .getString(orgCur.getColumnIndex(Organization.TITLE)));

            contactDto.setJobDep(orgCur.getString(orgCur
                    .getColumnIndex(Organization.DATA)) == null ? "" : orgCur
                    .getString(orgCur.getColumnIndex(Organization.DEPARTMENT)));

            contactDto.setCompany(orgCur.getString(orgCur
                    .getColumnIndex(Organization.DATA)) == null ? "" : orgCur
                    .getString(orgCur.getColumnIndex(Organization.DATA)));

        }
        orgCur.close();

        // ------------------------------------------------------ WebSite
        String website = null;
        int wesiteType = 0;

        String websiteWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?";
        String[] websiteWhereParams = new String[]{String.valueOf(contactID),
                Website.CONTENT_ITEM_TYPE};
        Cursor websiteCur = context.getContentResolver().query(
                Data.CONTENT_URI, null, websiteWhere, websiteWhereParams, null);
        while (websiteCur.moveToNext()) {

            do {
                WebsiteDto websiteDto = new WebsiteDto();

                websiteDto.setWebsite(websiteCur.getString(websiteCur
                        .getColumnIndex(Website.DATA)) == null ? ""
                        : websiteCur.getString(websiteCur
                        .getColumnIndex(Website.DATA)));

                if (!websiteDto.getWebsite().isEmpty()) {

                    wesiteType = websiteCur.getInt(websiteCur
                            .getColumnIndex(Website.TYPE));

                    switch (wesiteType) {

                        case Website.TYPE_HOMEPAGE:

                            websiteDto.setWebsiteType(context.getResources()
                                    .getStringArray(R.array.websitespinner)[0]);
                            break;
                        case Website.TYPE_BLOG:

                            websiteDto.setWebsiteType(context.getResources()
                                    .getStringArray(R.array.websitespinner)[1]);
                            break;

                        case Website.TYPE_PROFILE:

                            websiteDto.setWebsiteType(context.getResources()
                                    .getStringArray(R.array.websitespinner)[2]);
                            break;

                        case Website.TYPE_HOME:

                            websiteDto.setWebsiteType(context.getResources()
                                    .getStringArray(R.array.websitespinner)[3]);
                            break;

                        case Website.TYPE_WORK:

                            websiteDto.setWebsiteType(context.getResources()
                                    .getStringArray(R.array.websitespinner)[4]);
                            break;

                        case Website.TYPE_FTP:
                            // do something with the Work number here...
                            websiteDto.setWebsiteType(context.getResources()
                                    .getStringArray(R.array.websitespinner)[5]);
                            break;

                        case Website.TYPE_OTHER:

                            websiteDto.setWebsiteType(context.getResources()
                                    .getStringArray(R.array.websitespinner)[6]);
                            break;
                    }

                    contactDto.addWebsite(websiteDto);
                }

            } while (websiteCur.moveToNext());

        }
        websiteCur.close();

        // Relation

        // ==== Relation

        String relation = null;
        int relationType = 0;

        String relaWhere = ContactsContract.Data.CONTACT_ID + " = ? AND "
                + ContactsContract.Data.MIMETYPE + " = ?";
        String[] relaWhereParams = new String[]{String.valueOf(contactID),
                ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE};
        Cursor relaCur = context.getContentResolver().query(
                ContactsContract.Data.CONTENT_URI, null, relaWhere,
                relaWhereParams, null);
        while (relaCur.moveToNext()) {
            do {

                RelationDto relationDto = new RelationDto();

                relationDto
                        .setRelationName(relaCur.getString(relaCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Relation.NAME)) == null ? ""
                                : relaCur.getString(relaCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Relation.NAME)));

                // if (!relationDto.getRelationType().isEmpty()) {

                relationType = relaCur
                        .getInt(relaCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Relation.TYPE));

                System.out.println("JM " + relationType);

                switch (relationType) {
                    case Relation.TYPE_ASSISTANT:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[0]);
                        break;

                    case Relation.TYPE_BROTHER:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[1]);
                        break;

                    case Relation.TYPE_CHILD:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[2]);
                        break;

                    case Relation.TYPE_DOMESTIC_PARTNER:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[3]);
                        break;

                    case Relation.TYPE_FATHER:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[4]);
                        break;

                    case Relation.TYPE_FRIEND:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[5]);
                        break;

                    case Relation.TYPE_MANAGER:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[6]);
                        break;

                    case Relation.TYPE_MOTHER:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[7]);
                        break;

                    case Relation.TYPE_PARENT:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[8]);
                        break;

                    case Relation.TYPE_PARTNER:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[9]);
                        break;

                    case Relation.TYPE_REFERRED_BY:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[10]);
                        break;

                    case Relation.TYPE_RELATIVE:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[11]);
                        break;

                    case Relation.TYPE_SISTER:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[12]);
                        break;

                    case Relation.TYPE_SPOUSE:
                        relationDto.setRelationType(context.getResources()
                                .getStringArray(R.array.relationspinner)[13]);
                        break;

                    default:

                        relationDto.setRelationType(numCur.getString(numCur
                                .getColumnIndex(Relation.LABEL)));

                        break;
                }

                contactDto.addRelation(relationDto);
                // }

            } while (relaCur.moveToNext());

        }
        relaCur.close();

        //

        String addressWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?";
        String[] addressWhereParams = new String[]{String.valueOf(contactID),
                StructuredPostal.CONTENT_ITEM_TYPE};
        Cursor addressCur = context.getContentResolver().query(
                Data.CONTENT_URI, null, addressWhere, addressWhereParams, null);
        while (addressCur.moveToNext()) {

            do {
                AddressDto addressDto = new AddressDto();

                addressDto.setStreetStr(addressCur.getString(addressCur
                        .getColumnIndex(StructuredPostal.STREET)) == null ? " "
                        : addressCur.getString(addressCur
                        .getColumnIndex(StructuredPostal.STREET)));

                addressDto
                        .setNeigborhoodStr(addressCur.getString(addressCur
                                .getColumnIndex(StructuredPostal.NEIGHBORHOOD)) == null ? " "
                                : addressCur.getString(addressCur
                                .getColumnIndex(StructuredPostal.NEIGHBORHOOD)));
                addressDto.setCityStr(addressCur.getString(addressCur
                        .getColumnIndex(StructuredPostal.CITY)) == null ? " "
                        : addressCur.getString(addressCur
                        .getColumnIndex(StructuredPostal.CITY)));
                addressDto
                        .setZipCodeStr(addressCur.getString(addressCur
                                .getColumnIndex(StructuredPostal.POSTCODE)) == null ? " "
                                : addressCur.getString(addressCur
                                .getColumnIndex(StructuredPostal.POSTCODE)));
                addressDto
                        .setCountryStr(addressCur.getString(addressCur
                                .getColumnIndex(StructuredPostal.COUNTRY)) == null ? " "
                                : addressCur.getString(addressCur
                                .getColumnIndex(StructuredPostal.COUNTRY)));

                if (!addressDto.getCityStr().isEmpty()
                        && !addressDto.getCountryStr().isEmpty()
                        && !addressDto.getNeigborhoodStr().isEmpty()
                        && !addressDto.getStreetStr().isEmpty()
                        && !addressDto.getZipCodeStr().isEmpty()) {
                    int addressType = addressCur.getInt(addressCur
                            .getColumnIndex(StructuredPostal.TYPE));

                    switch (addressType) {

                        case StructuredPostal.TYPE_HOME:
                            // do something with the Work number here...

                            addressDto.setAddressType(context.getResources()
                                    .getStringArray(R.array.addressspinner)[0]);
                            break;
                        case Email.TYPE_WORK:
                            // do something with the Mobile number here...
                            addressDto.setAddressType(context.getResources()
                                    .getStringArray(R.array.addressspinner)[1]);
                            break;

                        case Email.TYPE_OTHER:
                            // do something with the Work number here...
                            addressDto.setAddressType(context.getResources()
                                    .getStringArray(R.array.addressspinner)[2]);
                            break;

                    }
                    contactDto.addAddresses(addressDto);
                }

            } while (addressCur.moveToNext());

        }
        addressCur.close();

        // Event
        int eventType = 0;

        // String eventWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
        // + " = ?";
        // String[] eventWhereParams = new String[] { String.valueOf(contactID),
        // Event.CONTENT_ITEM_TYPE };

        String eventWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?AND " + Event.TYPE + "<> ?";
        String[] eventWhereParams = new String[]{String.valueOf(contactID),
                Event.CONTENT_ITEM_TYPE, Event.TYPE_BIRTHDAY + ""};

        Cursor eventCur = context.getContentResolver().query(Data.CONTENT_URI,
                null, eventWhere, eventWhereParams, null);
        while (eventCur.moveToNext()) {
            do {

                DateDto dateDto = new DateDto();
                dateDto.setDates(eventCur.getString(eventCur
                        .getColumnIndex(Event.DATA)) == null ? "" : eventCur
                        .getString(eventCur.getColumnIndex(Event.START_DATE)));

                if (!dateDto.getDates().isEmpty()) {
                    eventType = eventCur.getInt(eventCur
                            .getColumnIndex(Event.TYPE));

                    switch (eventType) {

                        case Event.TYPE_ANNIVERSARY:
                            dateDto.setDatesType(context.getResources()
                                    .getStringArray(R.array.eventspinner)[0]);
                            break;
                        case Event.TYPE_OTHER:
                            // do something with the Mobile number here...
                            dateDto.setDatesType(context.getResources()
                                    .getStringArray(R.array.eventspinner)[1]);
                            break;

                        case Event.TYPE_CUSTOM:
                            // do something with the Work number here...
                            dateDto.setDatesType(context.getResources()
                                    .getStringArray(R.array.eventspinner)[2]);
                            break;

                    }

                    contactDto.addDateEvent(dateDto);
                }

            } while (eventCur.moveToNext());

        }
        eventCur.close();

        // GETTING BIRTHDAY
        String birthdayWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?AND " + Event.TYPE + "= ?";
        String[] birthdayWhereParams = new String[]{
                String.valueOf(contactID), Event.CONTENT_ITEM_TYPE,
                Event.TYPE_BIRTHDAY + ""};

        Cursor birthdayCur = context.getContentResolver().query(
                Data.CONTENT_URI, null, birthdayWhere, birthdayWhereParams,
                null);
        while (birthdayCur.moveToNext()) {

            contactDto.setBirthday(birthdayCur.getString(birthdayCur
                    .getColumnIndex(Event.START_DATE)));

        }
        birthdayCur.close();

        // GENERATING THUMBNAIL ID
//        if(addThumbnail(String.valueOf(contactID)) == null) Log.e(TAG, "BITMAP IS NULL");
//        contactDto.setProfilepic(addThumbnail(String.valueOf(contactID)));

        /**Setting up profile picture*/
        CallLogsManager.init();
        try {
            contactDto.setProfilepic(BitmapFactory
                    .decodeStream(CallLogsManager.getCallLogsManagerInstance()
                            .openDisplayPhoto(context, Long.parseLong(contactID))));
        } catch (Exception e) {
            Log.e(TAG, "getContactDetails: " + e.getMessage());
            contactDto.setProfilepic(null);
        }
        return contactDto;
    }

    public String getContactInformation(String number) {

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor cur = context.getContentResolver().query(
                uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME,
                        ContactsContract.PhoneLookup.NUMBER,
                        ContactsContract.PhoneLookup._ID}, null, null, null);
        String id2 = "";
        if (cur.moveToNext()) {
            int id = cur.getColumnIndex(ContactsContract.Contacts._ID);

            id2 = cur.getString(id);

        }
        cur.close();
        return id2;

    }

    public static String getContactName(String number) {
        Uri uri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        Cursor cur = context.getContentResolver().query(
                uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME,
                        ContactsContract.PhoneLookup.NUMBER,
                        ContactsContract.PhoneLookup._ID}, null, null, null);
        String contactName = "";
        if (cur.moveToNext()) {
            int name = cur
                    .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            contactName = cur.getString(name);

        }
        cur.close();
        return contactName;

    }

    public static String getContactNumType(String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor cur = context.getContentResolver().query(uri, new String[]{
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup.NUMBER,
                ContactsContract.PhoneLookup.TYPE,
                ContactsContract.PhoneLookup._ID}, null, null, null);
        String numType = "";
        if (cur.moveToNext()) {
            int type = cur.getColumnIndex(ContactsContract.PhoneLookup.TYPE);
            numType = cur.getString(type);
        }
        cur.close();
        return numType;

    }

    public static String getContactNumType2(String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{
                PhoneLookup.DISPLAY_NAME,
                PhoneLookup.LABEL,
                PhoneLookup.TYPE,}, null, null, null);

        if (cursor == null) {
            return null;
        }
        String customLabel = null;
        if (cursor.moveToFirst()) {

            customLabel = cursor.getString(cursor.getColumnIndex(PhoneLookup.LABEL));
            int type = cursor.getColumnIndex(PhoneLookup.TYPE);

            String phoneType = (String) Phone.getTypeLabel(context.getResources(), type, customLabel);
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return customLabel;
    }

    public boolean updateContact(ContactDto contactDto, ContactDto oldContactDto) {
        boolean success = true;

        // DELETION OF EXISTING NUMBERS
        ArrayList<android.content.ContentProviderOperation> number_oplist = new ArrayList<android.content.ContentProviderOperation>();
        for (PhoneNumberDto phoneNumberDto : oldContactDto.getPhoneNumbers()) {
            if (checkIfPhoneNumberIsPresent(phoneNumberDto,
                    contactDto.getPhoneNumbers()) == false) {
                int phoneTypeIndex = 0;

                if (phoneNumberDto.getNumberType().equalsIgnoreCase("Mobile")) {
                    phoneTypeIndex = Phone.TYPE_MOBILE;
                } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                        "Work")) {
                    phoneTypeIndex = Phone.TYPE_WORK;
                } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                        "Home")) {
                    phoneTypeIndex = Phone.TYPE_HOME;
                } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                        "OwtelAppDaoGenerator")) {
                    phoneTypeIndex = Phone.TYPE_MAIN;
                } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                        "Work Fax")) {
                    phoneTypeIndex = Phone.TYPE_FAX_WORK;
                } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                        "Home Fax")) {
                    phoneTypeIndex = Phone.TYPE_FAX_HOME;
                } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                        "Pager")) {
                    phoneTypeIndex = Phone.TYPE_PAGER;
                } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                        "Other")) {
                    phoneTypeIndex = Phone.TYPE_OTHER;
                }

                number_oplist.add(ContentProviderOperation
                        .newDelete(Data.CONTENT_URI)
                        .withSelection(
                                ContactsContract.Data.CONTACT_ID + "!=?"
                                        + " AND "
                                        + ContactsContract.Data.MIMETYPE + "=?"
                                        + " AND " + Phone.TYPE + "=?" + " AND "
                                        + Phone.NUMBER + "!=?",
                                new String[]{contactDto.getContactID(),
                                        Phone.CONTENT_ITEM_TYPE,
                                        String.valueOf(phoneTypeIndex),
                                        phoneNumberDto.getNumber()})

                        .build());
            }

        }

        try {
            if (!number_oplist.isEmpty()) {
                context.getContentResolver().applyBatch(
                        ContactsContract.AUTHORITY, number_oplist);
            }

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // INSERTING NEW PHONE NUMBERS PHONE NUMBERS
        ContentValues phoneContentValues = new ContentValues();
        for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
            if (checkIfPhoneNumberIsPresent(phoneNumberDto,
                    oldContactDto.getPhoneNumbers()) == false) {
                if (phoneNumberDto != null) {
                    System.out.println(phoneNumberDto.getNumber());
                    phoneContentValues = new ContentValues();
                    phoneContentValues.put(Data.RAW_CONTACT_ID,
                            Integer.parseInt(contactDto.getContactID()));
                    phoneContentValues.put(Data.MIMETYPE,
                            Phone.CONTENT_ITEM_TYPE);
                    phoneContentValues.put(Phone.NUMBER,
                            phoneNumberDto.getNumber());

                    int phoneTypeIndex = 0;

                    if (phoneNumberDto.getNumberType()
                            .equalsIgnoreCase("Mbile")) {
                        phoneTypeIndex = Phone.TYPE_MOBILE;
                    } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            "Work")) {
                        phoneTypeIndex = Phone.TYPE_WORK;
                    } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            "Home")) {
                        phoneTypeIndex = Phone.TYPE_HOME;
                    } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            "OwtelAppDaoGenerator")) {
                        phoneTypeIndex = Phone.TYPE_MAIN;
                    } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            "Work Fax")) {
                        phoneTypeIndex = Phone.TYPE_FAX_WORK;
                    } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            "Home Fax")) {
                        phoneTypeIndex = Phone.TYPE_FAX_HOME;
                    } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            "Pager")) {
                        phoneTypeIndex = Phone.TYPE_PAGER;
                    } else if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            "Other")) {
                        phoneTypeIndex = Phone.TYPE_OTHER;
                    }
                    phoneContentValues.put(Phone.TYPE, phoneTypeIndex);
                    context.getContentResolver().insert(Data.CONTENT_URI,
                            phoneContentValues);
                }
            }

        }

        // DELETE OF OLD EMAILS
        ArrayList<android.content.ContentProviderOperation> emails_oplist = new ArrayList<android.content.ContentProviderOperation>();
        for (EmailDto emailDto : oldContactDto.getEmails()) {
            if (checkIfEmailIsPresent(emailDto, contactDto.getEmails()) == false) {
                int emailTypeIndex = 0;

                if (emailDto.getEmailType().equalsIgnoreCase("Work")) {
                    emailTypeIndex = Email.TYPE_WORK;
                } else if (emailDto.getEmailType().equalsIgnoreCase("Home")) {
                    emailTypeIndex = Email.TYPE_HOME;
                }
                if (emailDto.getEmailType().equalsIgnoreCase("Other")) {
                    emailTypeIndex = Email.TYPE_OTHER;
                }

                emails_oplist.add(ContentProviderOperation
                        .newDelete(Data.CONTENT_URI)
                        .withSelection(
                                ContactsContract.Data.CONTACT_ID + "=?"
                                        + " AND "
                                        + ContactsContract.Data.MIMETYPE + "=?"
                                        + " AND " + Email.TYPE + "=?" + " AND "
                                        + Email.DATA + "=?",
                                new String[]{contactDto.getContactID(),
                                        Email.CONTENT_ITEM_TYPE,
                                        String.valueOf(emailTypeIndex),
                                        emailDto.getEmail()}).build());
            }

        }

        try {
            if (!emails_oplist.isEmpty()) {
                context.getContentResolver().applyBatch(
                        ContactsContract.AUTHORITY, emails_oplist);
            }

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // INSERTING NEW EMAILS
        ContentValues emailContentValues = new ContentValues();
        for (EmailDto emailDto : contactDto.getEmails()) {
            if (checkIfEmailIsPresent(emailDto, oldContactDto.getEmails()) == false) {
                if (emailDto != null) {
                    emailContentValues = new ContentValues();
                    emailContentValues.put(Data.RAW_CONTACT_ID,
                            Integer.parseInt(contactDto.getContactID()));
                    emailContentValues.put(Data.MIMETYPE,
                            Email.CONTENT_ITEM_TYPE);
                    emailContentValues.put(Email.DATA, emailDto.getEmail());

                    int emailTypeIndex = 0;

                    if (emailDto.getEmailType().equalsIgnoreCase("Work")) {
                        emailTypeIndex = Email.TYPE_WORK;
                    } else if (emailDto.getEmailType().equalsIgnoreCase("Home")) {
                        emailTypeIndex = Email.TYPE_HOME;
                    } else if (emailDto.getEmailType()
                            .equalsIgnoreCase("Other")) {
                        emailTypeIndex = Email.TYPE_OTHER;
                    }
                    emailContentValues.put(Email.TYPE, emailTypeIndex);
                    context.getContentResolver().insert(Data.CONTENT_URI,
                            emailContentValues);
                }
            }

        }

        // DELETION OF OLD ADDRESSES
        ArrayList<android.content.ContentProviderOperation> address_oplist = new ArrayList<android.content.ContentProviderOperation>();
        for (AddressDto addressDto : oldContactDto.getAddresses()) {
            if (checkIfAddressPresent(addressDto, contactDto.getAddresses()) == false) {
                int addressTypeIndex = 0;

                if (addressDto.getAddressType().equalsIgnoreCase("Work")) {
                    addressTypeIndex = StructuredPostal.TYPE_WORK;
                } else if (addressDto.getAddressType().equalsIgnoreCase("Home")) {
                    addressTypeIndex = StructuredPostal.TYPE_HOME;
                }
                if (addressDto.getAddressType().equalsIgnoreCase("Other")) {
                    addressTypeIndex = StructuredPostal.TYPE_OTHER;
                }

                address_oplist.add(ContentProviderOperation
                        .newDelete(Data.CONTENT_URI)
                        .withSelection(
                                ContactsContract.Data.CONTACT_ID + "=?"
                                        + " AND "
                                        + ContactsContract.Data.MIMETYPE + "=?"
                                        + " AND " + StructuredPostal.TYPE
                                        + "!=?" + " AND "
                                        + StructuredPostal.STREET + "=?"
                                        + " AND "
                                        + StructuredPostal.NEIGHBORHOOD + "=?"
                                        + " AND " + StructuredPostal.CITY
                                        + "=?" + " AND "
                                        + StructuredPostal.COUNTRY + "=?"
                                        + " AND " + StructuredPostal.POSTCODE
                                        + "!=?",
                                new String[]{contactDto.getContactID(),
                                        StructuredPostal.CONTENT_ITEM_TYPE,
                                        String.valueOf(addressTypeIndex),
                                        addressDto.getStreetStr(),
                                        addressDto.getNeigborhoodStr(),
                                        addressDto.getCityStr(),
                                        addressDto.getCountryStr(),
                                        addressDto.getZipCodeStr()}).build());
            }

        }

        try {
            if (!address_oplist.isEmpty()) {
                context.getContentResolver().applyBatch(
                        ContactsContract.AUTHORITY, address_oplist);
            }

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // INSERTING NEW ADDRESSES
        ContentValues values = new ContentValues();
        for (AddressDto addressDto : contactDto.getAddresses()) {
            if (checkIfAddressPresent(addressDto, oldContactDto.getAddresses()) == false) {
                if (addressDto != null) {
                    values = new ContentValues();
                    values.put(Data.RAW_CONTACT_ID,
                            Integer.parseInt(contactDto.getContactID()));
                    values.put(Data.MIMETYPE,
                            StructuredPostal.CONTENT_ITEM_TYPE);

                    int addressTypeIndex = 0;

                    if (addressDto.getAddressType().equalsIgnoreCase("Work")) {
                        addressTypeIndex = StructuredPostal.TYPE_WORK;
                    } else if (addressDto.getAddressType().equalsIgnoreCase(
                            "Home")) {
                        addressTypeIndex = StructuredPostal.TYPE_HOME;
                    } else if (addressDto.getAddressType().equalsIgnoreCase(
                            "Other")) {
                        addressTypeIndex = StructuredPostal.TYPE_OTHER;
                    }
                    values.put(StructuredPostal.TYPE, addressTypeIndex);
                    values.put(StructuredPostal.STREET,
                            addressDto.getStreetStr());
                    values.put(StructuredPostal.NEIGHBORHOOD,
                            addressDto.getNeigborhoodStr());
                    values.put(StructuredPostal.CITY, addressDto.getCityStr());
                    values.put(StructuredPostal.COUNTRY,
                            addressDto.getCountryStr());
                    values.put(StructuredPostal.POSTCODE,
                            addressDto.getZipCodeStr());
                    context.getContentResolver().insert(Data.CONTENT_URI,
                            values);
                }
            }
        }

        // UPDATING OTHER DETAILS
        ArrayList<android.content.ContentProviderOperation> op_list = new ArrayList<android.content.ContentProviderOperation>();

        if (!contactDto.getDisplayName().isEmpty()) {
            op_list.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(
                            ContactsContract.Data.CONTACT_ID + "!=?" + " AND "
                                    + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{contactDto.getContactID(),
                                    StructuredName.CONTENT_ITEM_TYPE})
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            contactDto.getDisplayName()).build());
        }

        if (!contactDto.getFirstName().isEmpty()) {
            op_list.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(
                            ContactsContract.Data.CONTACT_ID + "=?" + " AND "
                                    + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{contactDto.getContactID(),
                                    StructuredName.CONTENT_ITEM_TYPE})
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                            contactDto.getFirstName()).build());

        }

        // ------------------------------------------------------ Names
        if (!contactDto.getLastName().isEmpty()) {
            op_list.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(
                            ContactsContract.Data.CONTACT_ID + "!=?" + " AND "
                                    + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{contactDto.getContactID(),
                                    StructuredName.CONTENT_ITEM_TYPE})
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                            contactDto.getLastName()).build());

            // .withSelection(
            // ContactsContract.Data.CONTACT_ID + "=?" + " AND "
            // + ContactsContract.Data.MIMETYPE + "=?"
            // + " AND " + StructuredName.FAMILY_NAME
            // + "=?",
            // new String[] { contactDto.getContactID(),
            // StructuredName.CONTENT_ITEM_TYPE,
            // oldContactDto.getLastName() })

        }

        if (contactDto.getProfilepic() != null) {
            // Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable())
            // .getBitmap();

            System.out.println("Pasok sa Photos Creation");
            ByteArrayOutputStream newStream = new ByteArrayOutputStream();
            contactDto.getProfilepic().compress(Bitmap.CompressFormat.PNG, 100, newStream);
            byte[] newImage = newStream.toByteArray();

            // ByteArrayOutputStream oldStream = new ByteArrayOutputStream();
            // oldContactDto.getProfilepic().compress(Bitmap.CompressFormat.PNG,
            // 75, oldStream);
            // byte[] oldImage = oldStream.toByteArray();

            op_list.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(
                            ContactsContract.Data.CONTACT_ID + "=?" + " AND "
                                    + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{contactDto.getContactID(),
                                    Photo.CONTENT_ITEM_TYPE})
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, newImage).build());

        }

        if (!contactDto.getBirthday().isEmpty()) {
            op_list.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(
                            ContactsContract.Data.CONTACT_ID + "=?" + " AND "
                                    + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{contactDto.getContactID(),
                                    Event.CONTENT_ITEM_TYPE})
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.Event.START_DATE,
                            contactDto.getBirthday())
                    .withValue(
                            ContactsContract.CommonDataKinds.Event.TYPE,
                            ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY)
                    .build());
        }

        if (!contactDto.getCompany().isEmpty()) {
            op_list.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(
                            ContactsContract.Data.CONTACT_ID + "=?" + " AND "
                                    + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{contactDto.getContactID(),
                                    Organization.CONTENT_ITEM_TYPE})
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.Organization.COMPANY,
                            contactDto.getCompany())
                    .withValue(
                            ContactsContract.CommonDataKinds.Organization.TYPE,
                            ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
                    .build());
        }

        // ------------------------------------------------------
        // Note
        if (!contactDto.getNote().isEmpty()) {
            op_list.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(
                            ContactsContract.Data.CONTACT_ID + "=?" + " AND "
                                    + ContactsContract.Data.MIMETYPE + "=?",
                            new String[]{contactDto.getContactID(),
                                    Note.CONTENT_ITEM_TYPE})
                    .withValue(
                            ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Note.NOTE,
                            contactDto.getNote())

                    .build());
        }

        try {

            ContentProviderResult[] results = contentResolver.applyBatch(
                    ContactsContract.AUTHORITY, op_list);

        } catch (Exception e) {
            e.printStackTrace();

            success = false;
        }

        return success;
    }

    public boolean isContactFavorite(String contactName) {
        boolean result = false;

        // ContactsContract.Data.CONTACT_ID

        String[] projection = new String[]{ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.STARRED};

        Cursor cursor = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                projection,
                "starred!=? AND " + ContactsContract.Contacts.DISPLAY_NAME
                        + "=?", new String[]{"1", contactName}, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {
                x++;
            } while (cursor.moveToNext());
        }

        if (x > 0) {
            result = true;
        }

        cursor.close();

        return result;
    }

    public ContactDto removeNullValues(ContactDto contactToFilter) {
        ContactDto contactDto = new ContactDto();
        contactDto = contactToFilter;

        // REMOVING NULL VALUES FROM PHONENUMBERS
        ArrayList<PhoneNumberDto> dummyPhoneNumberList = new ArrayList<PhoneNumberDto>();
        for (int x = 0; x < contactDto.getPhoneNumbers().size(); x++) {
            if (contactDto.getPhoneNumbers().get(x) != null) {
                if (!contactDto.getPhoneNumbers().get(x).getNumber().isEmpty()
                        && !contactDto.getPhoneNumbers().get(x).getNumberType()
                        .isEmpty()) {
                    dummyPhoneNumberList.add(contactDto.getPhoneNumbers()
                            .get(x));
                }
            }
        }
        contactDto.phoneNumbers.clear();
        contactDto.phoneNumbers.addAll(dummyPhoneNumberList);

        // REMOVING NULL VALUES FROM EMAILS
        ArrayList<EmailDto> dummyEmailList = new ArrayList<EmailDto>();
        for (int x = 0; x < contactDto.getEmails().size(); x++) {
            if (contactDto.getEmails().get(x) != null) {

                if (!contactDto.getEmails().get(x).getEmail().isEmpty()
                        && !contactDto.getEmails().get(x).getEmailType()
                        .isEmpty()) {
                    dummyEmailList.add(contactDto.getEmails().get(x));
                }
            }
        }
        contactDto.emails.clear();
        contactDto.emails.addAll(dummyEmailList);

        // REMOVING NULL VALUES FROM ADDRESSES
        ArrayList<AddressDto> dummyAddressList = new ArrayList<AddressDto>();
        for (int x = 0; x < contactDto.getAddresses().size(); x++) {
            System.out.println("here on address clear null");
            if (contactDto.getAddresses().get(x) != null) {
                dummyAddressList.add(contactDto.getAddresses().get(x));
            }
        }
        contactDto.addresses.clear();
        contactDto.addresses.addAll(dummyAddressList);

        // REMOVING NULL VALUES FROM WEBSITES
        ArrayList<WebsiteDto> dummyWebSiteList = new ArrayList<WebsiteDto>();
        for (int x = 0; x < contactDto.getWebsites().size(); x++) {
            if (contactDto.getWebsites().get(x) != null) {
                if (!contactDto.getWebsites().get(x).getWebsite().isEmpty()
                        && !contactDto.getWebsites().get(x).getWebsite()
                        .isEmpty()) {
                    dummyWebSiteList.add(contactDto.getWebsites().get(x));
                }
            }
        }
        contactDto.websites.clear();
        contactDto.websites.addAll(dummyWebSiteList);

        // REMOVING NULL VALUES FROM IM
        ArrayList<IMDto> dummyIMList = new ArrayList<IMDto>();
        for (int x = 0; x < contactDto.getImField().size(); x++) {
            if (contactDto.getImField().get(x) != null) {
                if (!contactDto.getImField().get(x).getIm().isEmpty()
                        && !contactDto.getImField().get(x).getImProtocol()
                        .isEmpty()) {
                    dummyIMList.add(contactDto.getImField().get(x));
                }
            }
        }
        contactDto.imField.clear();
        contactDto.imField.addAll(dummyIMList);

        // REMOVING NULL VALUES FROM DATE
        ArrayList<DateDto> dummydateList = new ArrayList<DateDto>();
        for (int x = 0; x < contactDto.getDateEvent().size(); x++) {
            if (contactDto.getDateEvent().get(x) != null) {
                if (!contactDto.getDateEvent().get(x).getDates().isEmpty()
                        && !contactDto.getDateEvent().get(x).getDatesType()
                        .isEmpty()) {
                    dummydateList.add(contactDto.getDateEvent().get(x));
                }
            }
        }
        contactDto.dateEvent.clear();
        contactDto.dateEvent.addAll(dummydateList);

        // REMOVING NULL VALUES FROM RELATIONS
        ArrayList<RelationDto> dummyRelationList = new ArrayList<RelationDto>();
        for (int x = 0; x < contactDto.getRelationDto().size(); x++) {
            if (contactDto.getRelationDto().get(x) != null) {
                if (!contactDto.getRelationDto().get(x).getRelationName()
                        .isEmpty()
                        && !contactDto.getRelationDto().get(x)
                        .getRelationType().isEmpty()) {
                    dummyRelationList.add(contactDto.getRelationDto().get(x));
                }
            }
        }
        contactDto.relation.clear();
        contactDto.relation.addAll(dummyRelationList);

        return contactDto;
    }

    public boolean checkIfPhoneNumberIsPresent(PhoneNumberDto phoneNumberDto,
                                               ArrayList<PhoneNumberDto> phoneNumberList) {
        boolean result = false;
        for (PhoneNumberDto phoneNumberDto2 : phoneNumberList) {
            if (phoneNumberDto2.getNumber().equalsIgnoreCase(
                    phoneNumberDto.getNumber())
                    && phoneNumberDto2.getNumberType().equalsIgnoreCase(
                    phoneNumberDto.getNumberType())) {
                result = true;
                break;
            }
        }

        return result;
    }

    public boolean checkIfEmailIsPresent(EmailDto emailDto,
                                         ArrayList<EmailDto> emailList) {
        boolean result = false;
        for (EmailDto emailDto2 : emailList) {
            if (emailDto2.getEmail().equalsIgnoreCase(emailDto.getEmail())
                    && emailDto2.getEmailType().equalsIgnoreCase(
                    emailDto.getEmailType())) {
                result = true;
                break;
            }
        }
        return result;
    }

    public boolean checkIfAddressPresent(AddressDto addressDto,
                                         ArrayList<AddressDto> addressList) {
        boolean result = false;
        for (AddressDto addressDto2 : addressList) {
            if (addressDto2.getAddressType().equalsIgnoreCase(
                    addressDto.getAddressType())
                    && addressDto2.getCityStr().equalsIgnoreCase(
                    addressDto.getCityStr())
                    && addressDto2.getCountryStr().equalsIgnoreCase(
                    addressDto.getCountryStr())
                    && addressDto2.getNeigborhoodStr().equalsIgnoreCase(
                    addressDto.getNeigborhoodStr())
                    && addressDto2.getStreetStr().equalsIgnoreCase(
                    addressDto.getStreetStr())
                    && addressDto2.getZipCodeStr().equalsIgnoreCase(
                    addressDto.getZipCodeStr())) {
                result = true;
                break;
            }
        }

        return result;
    }

    /**
     * Initiate call using default dialer
     *
     * @param context Context
     * @param number  String
     * @return Intent
     */
    public static Intent initiateDefaultCall(Context context, String number) {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_CALL);
        i.setData(Uri.parse("tel:" + number));
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> list = pm.queryIntentActivities(i, 0);
        for (ResolveInfo info : list) {
            String packageName = info.activityInfo.packageName;
            if (packageName.toLowerCase().equals("com.android.phone")) {
                i.setClassName(packageName, info.activityInfo.name);
                return i;
            }
        }
        return i;
    }

    /**
     * Initiate SIP call.
     *
     * @param context Context
     * @param number  String
     */
    public static void initiateSipCall(Context context, String number) {
        if (new Functions().getNumber().equals(number)) {
            Toast.makeText(context, R.string.call_own_number_error, Toast.LENGTH_SHORT).show();
        } else {
            new ApiHelper(context).doCallCheckingThenCall(context, number);
        }
    }

    public static ArrayList<String> getStarred(Activity activity) {

        String[] projection = new String[]{ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.STARRED};
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";
        Cursor cursor = activity.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI, projection, "starred=?",
                new String[]{"1"}, sortOrder);

        int id = cursor.getColumnIndex(ContactsContract.Contacts._ID);

        ArrayList<String> contactIDList = new ArrayList<String>();
        if (cursor.moveToFirst()) {
            do {
                contactIDList.add(cursor.getString(id));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return contactIDList;

    }

    public static String getDisplayName(Activity activity, String id) {

        String displayName = null;
        String nameWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                + " = ?";
        String[] nameWhereParams = new String[]{String.valueOf(id),
                StructuredName.CONTENT_ITEM_TYPE};
        Cursor nameCur = activity.getContentResolver().query(Data.CONTENT_URI,
                null, nameWhere, nameWhereParams, null);
        if (nameCur.moveToNext()) {
            displayName = nameCur.getString(nameCur
                    .getColumnIndex(StructuredName.DISPLAY_NAME));

        }
        nameCur.close();
        return displayName;

    }

    public static String getAccountType(Activity activity, String contactID) {
        Cursor cursor = null;
        String AccountType = null;
        try {
            cursor = activity.getContentResolver().query(
                    RawContacts.CONTENT_URI,
                    new String[]{RawContacts.ACCOUNT_NAME,
                            RawContacts.ACCOUNT_TYPE},
                    RawContacts.CONTACT_ID + "=?",
                    new String[]{String.valueOf(contactID)}, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                AccountType = cursor.getString(cursor
                        .getColumnIndex(RawContacts.ACCOUNT_TYPE));

                cursor.close();
            }
        } catch (Exception e) {
            System.out.println("" + e);
        } finally {
            cursor.close();
        }
        return AccountType;
    }

    public static void getSearchByNumber() {
        String phoneNo = null;
        ContentResolver cr = Var.activity.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur
                        .getString(cur
                                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer
                        .parseInt(cur.getString(cur
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                    + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {

                        phoneNo = pCur
                                .getString(pCur
                                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        System.out.println("JM number " + phoneNo);

                    }
                    pCur.close();
                }

            }

        }
    }
}