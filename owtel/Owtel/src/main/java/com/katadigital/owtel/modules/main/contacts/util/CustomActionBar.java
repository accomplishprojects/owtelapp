package com.katadigital.owtel.modules.main.contacts.util;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity.loadContactDetails;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;

public class CustomActionBar {
    private static String TAG = "CustomActionBar";

    public static View viewCustomActionBar(final Activity activity,
                                           String leftItem, String title, String rightItem) {
        View include = activity.findViewById(R.id.contct_include);

        TextView a_title = (TextView) include.findViewById(R.id.a_title);
        Button mButtonLeft = (Button) include.findViewById(R.id.btn_custom_action_bar_left);
        Button mButtonRight = (Button) include.findViewById(R.id.btn_custom_action_bar_right);

        mButtonLeft.setOnClickListener(v -> activity.onBackPressed());
        mButtonRight.setText(leftItem);
        a_title.setText(title);
        mButtonRight.setText(rightItem);

        mButtonRight.setVisibility(View.VISIBLE);
        return mButtonRight;
    }

    public static void viewCustomActionBarContact(final Activity activity,
                                                  String leftItem, String title, String rightItem,
                                                  ContactDto contactDto, final loadContactDetails loadContactDetails) {
        // Action bar
        View include = activity.findViewById(R.id.contct_include);
        TextView a_title = (TextView) include.findViewById(R.id.a_title);
        Button mButtonLeft = (Button) include.findViewById(R.id.btn_custom_action_bar_left);
        Button mButtonRight = (Button) include.findViewById(R.id.btn_custom_action_bar_right);
        mButtonLeft.setOnClickListener(v -> activity.onBackPressed());

        a_title.setText(title);
        mButtonLeft.setText(leftItem);
        mButtonRight.setText(rightItem);

        mButtonRight.setVisibility(View.VISIBLE);

        try {
            if (contactDto.getAccountType().equals("SIM Account")
                    || contactDto.getAccountType().equals("Local Phone Account")
                    || contactDto.getAccountType().equals("com.google")
                    || contactDto.getAccountType().equals("USIM Account")) {
                mButtonRight.setVisibility(View.VISIBLE);
                mButtonRight.setOnClickListener(v -> loadContactDetails.execute());
            } else mButtonRight.setVisibility(View.GONE);
        } catch (Exception e) {
            Log.e(TAG, "viewCustomActionBarContact " + e);
        }
    }
}
