package com.katadigital.owtel.api;


import com.katadigital.owtel.models.Address;
import com.katadigital.owtel.models.IddCreditsObj;
import com.katadigital.owtel.models.ListAreaCode;
import com.katadigital.owtel.models.LoginObj;
import com.katadigital.owtel.models.Message;
import com.katadigital.owtel.models.MessageList;
import com.katadigital.owtel.models.PromoCodes;
import com.katadigital.owtel.models.RadiusSession;
import com.katadigital.owtel.models.RegisterUser;
import com.katadigital.owtel.models.SessionObj;
import com.katadigital.owtel.models.UsNumberList;
import com.katadigital.owtel.models.UserSession;
import com.katadigital.owtel.models.ValidateCallObject;
import com.katadigital.owtel.models.ZipCodeCityState;
import com.katadigital.owtel.modules.billing.response.PaymentResponse;
import com.katadigital.owtel.utils.API;

import java.util.Map;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

import static com.katadigital.owtel.utils.API.PROMO_CODES;


public interface ApiService {

    String SERVICE_ENDPOINT = "http://52.36.230.252/owtel-api/";


    @GET("conversation/list")
    Call<Object> userConversations(@Query("userId") String userId, @Query("page") int page);

    @FormUrlEncoded
    @POST(API.URL_TEST + "get_conversation_thread.php")
    Observable<MessageList> getMessagesRxJava(@Field("owtel_number") String owtel_number, @Field("thread_id") String thread_id, @Field("sms_id") String sms_id, @Field("scroll") String scroll);

    @FormUrlEncoded
//    @POST(API.URL_TEST + "send_message.php")
    @POST(API.URL_TEST + "send_message.php")
    Observable<Message> sendMessage(@Field("sender") String sender, @Field("receiver") String receiver, @Field("message") String message, @Field("route") String route);

    @FormUrlEncoded
    @POST(API.URL_TEST + "get_conversation_list.php")
    Observable<MessageList> getMessagesConversationRxJava(@Field("owtel_number") String owtel_number);

    @FormUrlEncoded
    @POST("update_subscription.php")
    Observable<PaymentResponse> submitSubscription(@Field("json_encrypt") String jsonEncrypt, @Field("payment_type") String paymentType);

    /**
     * Check if user has a valid Radius Session ID
     *
     * @param radId String
     * @return Observable<RadiusSession>
     */
    @FormUrlEncoded
    @POST(API.SESSION_SECURITY)
    Observable<RadiusSession> checkUserSession(@Field(API.LOGOUT_RAD_ID) String radId);

    @FormUrlEncoded
    @POST(API.REGISTER)
    Call<RegisterUser> registerUser(@Field(API.REGISTER_EMAIL) String email, @Field(API.REGISTER_PASSWORD) String password, @Field(API.REGISTER_PROMO_CODE) String promoCode);

    /**
     * Area Code List API request
     *
     * @return Call<ListAreaCode>
     */
    @POST(API.AREA_CODES)
    Call<ListAreaCode> getAreaCodes();

    @FormUrlEncoded
    @POST(API.CITY_STATE)
    Call<ZipCodeCityState> getCityState(@Field(API.PAYMENT_BILL_ADD_ZIP_CODE) String zipCode);

    // FIXME: 7/28/16 I need to add validate call API intefacew
    @FormUrlEncoded
    @POST(API.VALIDATE_CALL)
    Call<ValidateCallObject> validateCall(@Field("email_address") String emailAddress, @Field("dialed_number") String dialedNumber);


    @FormUrlEncoded
    @POST(API.LOGIN)
    Call<LoginObj> doLoginRequest(@Field("email_address") String email, @Field("password") String password);


    @FormUrlEncoded
    @POST(API.CHANGE_EMAIL_ADDRESS)
    Call<ValidateCallObject> changeEmail(@Field("email_address") String emailAddress, @Field("email_address_new") String emailAddressNew, @Field("password") String password);


    @FormUrlEncoded
    @POST(API.SESSION_SECURITY)
    Call<SessionObj> checkSession(@Field("rad_session_id") String radSessionID);

    @FormUrlEncoded
    @POST(API.LOGOUT)
    Call<SessionObj> doLogOutRequest(@Field("rad_session_id") String radSessionID);


    @FormUrlEncoded
    @POST(API.ADDRESS_LIST)
    Call<Address> getAddressList(@Field("country_id") String countryID, @Field("state_id") String stateID);

    @FormUrlEncoded
    @POST(API.CHANGE_OWTEL_NUMBER)
    Call<ValidateCallObject> changeNumber(@Field("email_address") String emailAddress, @Field("old_number") String oldNumber, @Field("new_number") String newNumber);

    /**
     * US Number List API request
     *
     * @param areaCode String
     * @param lockId   String
     * @return Call<UsNumberList>
     */
    @FormUrlEncoded
    @POST(API.ASSIGN_NUMBER)
    Call<UsNumberList> getUsNumbers(@Field(API.REGISTER_AREA_CODE) String areaCode,
                                    @Field(API.REGISTER_LOCK_ID) String lockId);


    @FormUrlEncoded
//    @POST(API.URL_TEST + "send_message.php")
    @POST(API.URL_TEST + PROMO_CODES)
    Call<PromoCodes> getPromoCodes(@Field("email_address") String emailAddress);


    @FormUrlEncoded
    @POST(API.IDD_CREDITS)
    Call<IddCreditsObj> getIDDCredits(@Field(API.REGISTER_EMAIL) String emailAddress);
}

