package com.katadigital.owtel.modules.billing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.security.PaymentHelper;
import com.katadigital.owtel.security.SecuredString;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.ui.adapter.AddressAdapter;
import com.katadigital.ui.adapter.IddAdapter;
import com.katadigital.ui.adapter.SubscriptionAdapter;

import org.json.JSONObject;
import org.parceler.Parcels;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Omar Matthew Reyes on 5/17/16.
 * Fragment for Premium User to pay for the Promo Code user
 */
public class PromoUserPaymentFragment extends Fragment {
    public final static String TAG = "PromoUserPaymentFragmen";

    @Bind(R.id.tv_register_payment_subscription_type)
    TextView textViewSubscriptionType;
    @Bind(R.id.tv_register_payment_subscription_amount)
    TextView textViewSubscriptionPrice;
    @Bind(R.id.spinner_register_payment_subscription_amount)
    Spinner spinnerSubscriptionPrices;
    @Bind(R.id.spinner_register_payment_idd_amount)
    Spinner spinnerIDDPrices;
    @Bind(R.id.tv_register_payment_total_amount)
    TextView textViewPaymentTotal;
    @Bind(R.id.et_card_number)
    EditText etCardNumber;
    @Bind(R.id.et_card_holder)
    EditText etCardHolder;
    @Bind(R.id.spinner_cc_month)
    Spinner spinnerCardExpireMonth;
    @Bind(R.id.spinner_cc_year)
    Spinner spinnerCardExpireYear;
    @Bind(R.id.et_cvv)
    EditText etCardCVV;
    @Bind(R.id.et_street)
    EditText etAddressStreet;
    @Bind(R.id.et_city)
    EditText etCity;
    @Bind(R.id.et_state)
    EditText etState;
    @Bind(R.id.et_zip_code)
    EditText etAddressZipCode;
    @Bind(R.id.spinner_country)
    Spinner spinnerAddressCountry;

    private MainActivity mainActivity;
    private Bundle bundlePrev;
    private ArrayList<SubscriptionList> listSubscription = new ArrayList<>();
    private ArrayList<IddList> listIdd = new ArrayList<>();
    private ArrayList<String> listYear = new ArrayList<>();
    private List<AddressList> countryList = new ArrayList<>();
//    , stateList = new ArrayList<>(), cityList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_promo_code, container, false);
        ButterKnife.bind(this, view);

        mainActivity = (MainActivity) getActivity();

        bundlePrev = getArguments();
        listSubscription = Parcels.unwrap(bundlePrev.getParcelable(Constants.PARCEL_LIST_SUBSCRIPTION));
        listIdd = Parcels.unwrap(bundlePrev.getParcelable(Constants.PARCEL_LIST_IDD));
        countryList = Parcels.unwrap(bundlePrev.getParcelable(Constants.PARCEL_LIST_COUNTRY));

        textViewSubscriptionPrice.setVisibility(View.GONE);
        spinnerSubscriptionPrices.setVisibility(View.VISIBLE);
        spinnerSubscriptionPrices.setAdapter(new SubscriptionAdapter(listSubscription));
        spinnerIDDPrices.setAdapter(new IddAdapter(listIdd));

        if (listSubscription.size() > 0 && listIdd.size() > 0)
            textViewPaymentTotal.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                    (listSubscription.get(0).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue())));

        // Fill spinner credit card expiry month
        spinnerCardExpireMonth.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.register_payment_card_months)));

        // Fill spinner credit card expiry year
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        listYear.add(getString(R.string.register_payment_card_year));
        for (int i = 0; i < 10; i++) {
            listYear.add(Integer.toString(currentYear + i));
        }
        ArrayAdapter<String> spinnerCcYear = new ArrayAdapter<>(getActivity(),
                R.layout.simple_list_item, listYear);
        spinnerCardExpireYear.setAdapter(spinnerCcYear);

//        spinnerAddressCity.setEnabled(false);
//        spinnerAddressCity.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.spinner_city_default)));
//        spinnerAddressState.setEnabled(false);
//        spinnerAddressState.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.spinner_state_default)));

        spinnerAddressCountry.setAdapter(new AddressAdapter(countryList, GlobalValues.GET_COUNTRY));
        spinnerAddressCountry.post(new Runnable() {
            @Override
            public void run() {
                spinnerAddressCountry.setSelection(getIndex(spinnerAddressCountry,"USA"));
            }
        });
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinnerAddressCountry);

            // Set popupWindow height to 500px
            popupWindow.setHeight(500);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
        return view;
    }

    @OnClick(R.id.btn_back)
    public void onBack(){
        mainActivity.popFragment();
    }

    @OnClick(R.id.btn_payment_continue)
    public void onContinue(){
        if (validateFieldValues()) {
            doPromoCodeUserPayment();
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_fields), Toast.LENGTH_SHORT).show();
        }
    }

    @OnItemSelected(R.id.spinner_register_payment_subscription_amount)
    public void onItemSelectSubscription(int position) {
        Log.i(TAG, "Spinner Subscription position " + position);
        Log.i(TAG, "Spinner Subscription id: " + listSubscription.get(position).getSubscriptionId() +
                " price " + listSubscription.get(position).getSubscriptionPrice() +
                " value " + listSubscription.get(position).getSubscriptionValue());
        textViewSubscriptionType.setText(listSubscription.get(position).getSubscriptionName());
        if (listSubscription.size() > 0 && listSubscription.size() > 0)
            textViewPaymentTotal.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                    (listSubscription.get(position).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue())));
    }

    @OnItemSelected(R.id.spinner_register_payment_idd_amount)
    public void onItemSelectIdd(int position) {
        Log.i(TAG, "Spinner IDD position " + position);
        Log.i(TAG, "Spinner IDD id: " + listIdd.get(position).getIddId() +
                " price " + listIdd.get(position).getIddReloadPrice() +
                " value " + listIdd.get(position).getIddReloadValue());
        if (listSubscription.size() > 0 && listIdd.size() > 0)
            textViewPaymentTotal.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                    (listSubscription.get(spinnerSubscriptionPrices.getSelectedItemPosition()).getSubscriptionValue() + listIdd.get(position).getIddReloadValue())));
    }

//    @OnItemSelected(R.id.spinner_country)
//    public void onItemSelectCountry(int position) {
//        Log.i(TAG, "Spinner Country position " + position);
//        if (position > 0) {
//            mainActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
//            mainActivity.mProgressDialog.show();
//            ApiHelper.doGetAddressList(mainActivity.mProgressDialog, mainActivity, GlobalValues.GET_STATE, "", countryList.get(position).getCountryId()).subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Subscriber<List<AddressList>>() {
//                        @Override
//                        public void onCompleted() {
//                            spinnerAddressState.setEnabled(true);
//                            mainActivity.mProgressDialog.dismiss();
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//
//                        @Override
//                        public void onNext(List<AddressList> addresses) {
//                            stateList = new ArrayList<>(addresses);
//                            Collections.copy(stateList, addresses);
//                            spinnerAddressState.setAdapter(new AddressAdapter(addresses, GlobalValues.GET_STATE));
//                        }
//                    });
//        } else {
//            spinnerAddressState.setEnabled(false);
//            spinnerAddressState.setSelection(0);
//            spinnerAddressCity.setEnabled(false);
//            spinnerAddressCity.setSelection(0);
//        }
//    }
//
//    @OnItemSelected(R.id.spinner_state)
//    public void onItemSelectState(int position) {
//        if (position > 0) {
//            mainActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
//            mainActivity.mProgressDialog.show();
//            ApiHelper.doGetAddressList(mainActivity.mProgressDialog, mainActivity, GlobalValues.GET_CITY, stateList.get(position).getStateId(), countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId()).subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Subscriber<List<AddressList>>() {
//                        @Override
//                        public void onCompleted() {
//                            spinnerAddressCity.setEnabled(true);
//                            mainActivity.mProgressDialog.dismiss();
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//
//                        @Override
//                        public void onNext(List<AddressList> addresses) {
//                            cityList = new ArrayList<>(addresses);
//                            spinnerAddressCity.setAdapter(new AddressAdapter(addresses, GlobalValues.GET_CITY));
//                        }
//                    });
//        } else {
//            spinnerAddressCity.setEnabled(false);
//            spinnerAddressCity.setSelection(0);
//        }
//    }
//
//    @OnItemSelected(R.id.spinner_city)
//    public void onItemSelectCity(int position) {
//        if (position > 0) Log.i(TAG, "Selected City: " + cityList.get(position).getCity());
//    }

    /**
     * Generate default adapter for Spinner
     *
     * @param stringList String[]
     * @return ArrayAdapter<String>
     */
    private ArrayAdapter<String> generateDefaultAdapter(String[] stringList) {
        return new ArrayAdapter<>(getActivity(), R.layout.simple_list_item, stringList);
    }

    /**
     * Check if all fields are valid
     *
     * @return boolean
     */
    private boolean validateFieldValues() {
        return isCreditCardValid(etCardNumber) &&
                isCreditCardHolderValid(etCardHolder) &&
                isCreditCardExpiryValid(spinnerCardExpireMonth, spinnerCardExpireYear) &&
                isCreditCardCvvValid(etCardCVV) &&
                isCreditCardAddressValid(etAddressStreet, etCity, etState,
                        spinnerAddressCountry, etAddressZipCode);
    }

    private boolean isCreditCardValid(EditText etCardNumber) {
        String cardNum = etCardNumber.getText().toString().trim();
        return !(cardNum.equals("") || cardNum.trim().isEmpty());
    }

    private boolean isCreditCardHolderValid(EditText creditCardHolder) {
        String cardHolder = creditCardHolder.getText().toString().trim();
        return !(cardHolder.equals("") || cardHolder.trim().isEmpty());
    }

    /**
     * Check if credit card expiry date is valid
     *
     * @param creditCardExpiryMonth Spinner
     * @param creditCardExpiryYear  Spinner
     * @return boolean
     */
    private boolean isCreditCardExpiryValid(Spinner creditCardExpiryMonth, Spinner creditCardExpiryYear) {
        return creditCardExpiryMonth.getSelectedItemPosition() > 0 &&
                creditCardExpiryYear.getSelectedItemPosition() > 0;
    }

    /**
     * Check if credit card cvv is valid
     *
     * @param creditCardCvv String
     * @return boolean
     */
    private boolean isCreditCardCvvValid(EditText creditCardCvv) {
        return creditCardCvv.getText().toString().trim().length() >= 3;
    }

    /**
     * Check if credit card billing address is valid
     *
     * @param etAddressStreet       EditText
     * @param etCity                EditText
     * @param etState               EditText
     * @param spinnerAddressCountry Spinner
     * @return boolean
     */
    private boolean isCreditCardAddressValid(EditText etAddressStreet, EditText etCity,
                                             EditText etState, Spinner spinnerAddressCountry,
                                             EditText etAddressZipCode) {
        return etAddressStreet.getText().toString().trim().length() > 0 &&
                etCity.getText().toString().trim().length() > 0 &&
                etState.getText().toString().trim().length() > 0 &&
                spinnerAddressCountry.getSelectedItemPosition() > 0 &&
                etAddressZipCode.getText().toString().trim().length() > 0;
    }

    /**
     * Do payment for Promo Code User
     *
     */
    private void doPromoCodeUserPayment() {
        mainActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
        mainActivity.mProgressDialog.show();
        StringRequest mStringRequestRegisterUser = new StringRequest(Request.Method.POST, API.URL_TEST + API.SUBMIT_SUBSCRIPTION,
                response -> {
                    Log.i(TAG, "Promo Code User Payment response " + response);
                    try {
                        JSONObject jsonObjectMain = new JSONObject(response);
                        String jsonObjectStatus = jsonObjectMain.getString("status");
                        if (jsonObjectStatus.equals("Success")) {
                            mainActivity.mProgressDialog.dismiss();
                            Bundle args = new Bundle();
                            args.putString(Constants.BUNDLE_PAYMENT_MESSAGE, getString(R.string.payment_success));
                            PaymentSuccessFragment paymentSuccessFragment = new PaymentSuccessFragment();
                            paymentSuccessFragment.setArguments(args);
                            mainActivity.changeFragment(paymentSuccessFragment, false);
                        } else {
                            mainActivity.mProgressDialog.dismiss();
                            String jsonObjectMessage = jsonObjectMain.getString("message");
                            DialogBuilder.showAlertDialog(getActivity(),
                                    jsonObjectStatus, jsonObjectMessage,
                                    getResources().getString(R.string.string_ok));
                        }
                    } catch (Exception e) {
                        if (GlobalValues.DEBUG) Log.e(TAG, "Volley " + e);
                        mainActivity.mProgressDialog.dismiss();
                        Toast.makeText(mainActivity, getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                    }
                }, error -> {
            if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
            mainActivity.mProgressDialog.dismiss();
            DialogBuilder.displayVolleyError(getActivity(), error);
        }) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                // Default value for json params
                String month = "0", year = "0", addressCityId = "", addressStateId = "", addressCountryId = "";
                if(spinnerCardExpireMonth.getSelectedItemPosition() > 0 && spinnerCardExpireYear.getSelectedItemPosition() > 0){
                    month = getResources().getStringArray(R.array.register_payment_card_months)[spinnerCardExpireMonth.getSelectedItemPosition()];
                    year = listYear.get(spinnerCardExpireYear.getSelectedItemPosition());
                }
                addressCityId = etCity.getText().toString();
                addressStateId = etState.getText().toString();
//                if(cityList.size() > 1) addressCityId = cityList.get(spinnerAddressCity.getSelectedItemPosition()).getCityId();
//                if(stateList.size() > 1) addressStateId = stateList.get(spinnerAddressState.getSelectedItemPosition()).getStateId();
                if(countryList.size() > 1) addressCountryId = countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId();
                String jsonObject = new PaymentHelper(OwtelAppController.getInstance().getApplicationContext())
                        .getSecuredJsonPromoCodePayment(bundlePrev.getString(Constants.BUNDLE_PROMO_CODE_USER_EMAIL),
                                listSubscription.get(spinnerSubscriptionPrices.getSelectedItemPosition()).getSubscriptionId(),
                                listSubscription.get(spinnerSubscriptionPrices.getSelectedItemPosition()).getSubscriptionValue(),
                                listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddId(),
                                listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue(),
                                (listSubscription.get(spinnerSubscriptionPrices.getSelectedItemPosition()).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue()),
                                etCardNumber.getText().toString().trim(),
                                etCardHolder.getText().toString().trim(),
                                etCardCVV.getText().toString().trim(),
                                month,
                                year,
                                etAddressStreet.getText().toString(),
                                addressCityId,
                                addressStateId,
                                addressCountryId,
                                etAddressZipCode.getText().toString());
                params.put(API.ENCRYPTED_OBJECT, jsonObject);
                if(GlobalValues.DEBUG) {
                    Log.i(TAG, API.ENCRYPTED_OBJECT + " encrypted " + jsonObject);
                    Log.i(TAG, API.ENCRYPTED_OBJECT + " encrypted " + jsonObject);
                    Log.i(TAG, API.ENCRYPTED_OBJECT + " decrypted " + SecuredString.decryptData(jsonObject));
                }
                return params;
            }
        };
        mStringRequestRegisterUser.setTag(Constants.REQUEST_TAG_PROCESS_USER);
        OwtelAppController.getInstance().addToRequestQueue(mStringRequestRegisterUser);
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().toLowerCase().contains(myString.toLowerCase())){
                index = i;
                break;
            }
        }
        return index;
    }
}
