package com.katadigital.owtel.modules.main.contacts.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.kata.phone.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutoCompleteTextAdapter extends SimpleAdapter {

    private Activity mContext;
    public LayoutInflater inflater = null;

    public AutoCompleteTextAdapter(Activity context,
                                   List<? extends Map<String, ?>> data, int resource, String[] from,
                                   int[] to) {
        super(context, data, resource, from, to);
        mContext = context;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null)
            vi = inflater.inflate(R.layout.keypad_autocomplete_contact, null);

        new ThumbnailTask(position, vi).executeOnExecutor(
                AsyncTask.THREAD_POOL_EXECUTOR, null);

        return vi;
    }

    private class ThumbnailTask extends AsyncTask {

        private int mPosition;
        private HashMap<String, Object> data;
        private View mView;

        private String mId;
        private String mName;
        private String mNumber;
        private Bitmap thumb;

        public ThumbnailTask(int position, View view) {
            this.mPosition = position;
            this.mView = view;
            this.data = (HashMap<String, Object>) getItem(mPosition);
        }

        @Override
        protected Object doInBackground(Object... params) {

            mName = (String) data.get("name");

            String phoneNumber = (String) data.get("phoneNumber");
            mNumber = phoneNumber.replaceAll(" ", "");

            //mId = (String) data.get("id");

//			if (data != null) {
//				Bitmap thumb1 = QuickContactHelper.addThumbnail(mId);
//				if (thumb1 != null)
//					thumb = NewUtil.getResizedBitmap(thumb1, 100);
//			}

            return data;
        }

        @Override
        protected void onPostExecute(Object result) {
            initData(mView, mName, mNumber, thumb);
        }
    }

    public void initData(View vi, String mName, String mNumber, Bitmap thumb) {
        TextView name = (TextView) vi.findViewById(R.id.name);
        TextView number = (TextView) vi.findViewById(R.id.number);

//		CircularImageView image = (CircularImageView) vi
//				.findViewById(R.id.thumb);

        //image.setVisibility(View.GONE);

        name.setText(mName);
        number.setText(mNumber);

//		if (thumb != null) {
//			Bitmap newthumb = NewUtil.getResizedBitmap(thumb, 100);
//			if (newthumb != null)
//				image.setImageBitmap(newthumb);
//		} else {
//			thumb = DialerFragment.decodeSampledBitmapFromResource(
//					Var.activity.getResources(), R.drawable.profile_pic_holder,
//					100, 100);
//			Bitmap newthumb = NewUtil.getResizedBitmap(thumb, 100);
//			if (newthumb != null)
//				image.setImageBitmap(newthumb);
//		}

    }
}
