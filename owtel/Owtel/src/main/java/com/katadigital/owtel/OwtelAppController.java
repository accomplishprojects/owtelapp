package com.katadigital.owtel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.kata.phone.BuildConfig;
import com.kata.phone.R;
import com.katadigital.owtel.dagger2.AppComponent;
import com.katadigital.owtel.dagger2.ApplicationModule;
import com.katadigital.owtel.dagger2.DaggerAppComponent;
import com.katadigital.owtel.dagger2.NetworkingModule;
import com.katadigital.owtel.dagger2.StorageModule;
import com.katadigital.owtel.daoDb.DaoMaster;
import com.katadigital.owtel.daoDb.DaoSession;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.messaging.service.WebSocketService;
import com.katadigital.owtel.utils.CallBlockHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.UtilityMethods;
import com.katadigital.portsip.helper.Line;
import com.katadigital.portsip.helper.Network;
import com.katadigital.portsip.helper.Ring;
import com.katadigital.portsip.helper.Session;
import com.katadigital.portsip.helper.SipContact;
import com.katadigital.portsip.utilities.androidcomponent.interfaces.PortSipContract;
import com.katadigital.portsip.utilities.logs.CallLogsManager;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.katadigital.portsip.views.PortSipCallScreenActivity;
import com.portsip.OnPortSIPEvent;
import com.portsip.PortSipErrorcode;
import com.portsip.PortSipSdk;
import com.securepreferences.SecurePreferences;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

import java.security.GeneralSecurityException;
import java.util.ArrayList;

//@ReportsCrashes(
//        formUri = "https://dcnc123.cloudant.com/acra-owtel/_design/acra-storage/_update/report",
//        reportType = HttpSender.Type.JSON,
//        httpMethod = HttpSender.Method.POST,
//        formUriBasicAuthLogin = "allyrodingingconfuldiabl",
//        formUriBasicAuthPassword = "5e42b9ada0fadfdf41f93db9a36fcdae23ea76e9",
//        customReportContent = {
//                ReportField.APP_VERSION_CODE,
//                ReportField.APP_VERSION_NAME,
//                ReportField.ANDROID_VERSION,
//                ReportField.PACKAGE_NAME,
//                ReportField.REPORT_ID,
//                ReportField.BUILD,
//                ReportField.STACK_TRACE
//        },
//        mode = ReportingInteractionMode.TOAST,
//        resToastText = R.string.toast_crash
//)


public class OwtelAppController extends Application implements OnPortSIPEvent {
    public static final String TAG = "OwtelAppController";

    private AppComponent component;
    public DaoSession daoSession;
    private SecurePreferences mSecurePrefs;
    private SecurePreferences mUserPrefs;
    private RequestQueue mRequestQueue;
    private static OwtelAppController sInstance;
    private static AudioManager audioManager;

    /**
     * PortSipSdk Variables
     */
    PortSipSdk mPortSipSdk;
    boolean conference = false;
    private boolean _SIPLogined = false;
    private boolean _SIPOnline = false;
    Activity activity;
    private PowerManager.WakeLock mCpuLock;
    public Line curSession;
    static final private ArrayList<SipContact> contacts = new ArrayList<SipContact>();
    static final private Line[] _CallSessions = new Line[Line.MAX_LINES];
    private Line _CurrentlyLine = _CallSessions[Line.LINE_BASE];// active line
    static final String SIP_ADDRESS_PATTERN = "^(sip:)(\\+)?[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\\.-][a-z0-9]+)*)+\\.[a-z]{2,}(:[0-9]{2,5})?$";
    public static final String SESSION_CHANG = OwtelAppController.class
            .getCanonicalName() + "Session change";
    public static final String CONTACT_CHANG = OwtelAppController.class
            .getCanonicalName() + "Contact change";
    private PortSipContract.PortSipContractListener portSipContractListener;
    private Network netmanager;
    private boolean isCallMissed = true;
    private boolean isChronometerRunning = false;

    /**
     * Variables
     */
    Chronometer mChronometer;

    @Override
    public void onCreate() {
        super.onCreate();
        /**Initiating helper classes*/
        NotificationHelper.init(this);
        CallLogsManager.init();

        sInstance = this;
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mChronometer = new Chronometer(this);

        component = DaggerAppComponent.builder()
                .networkingModule(new NetworkingModule(this))
                .storageModule(new StorageModule(this))
                .applicationModule(new ApplicationModule(this))
                .build();

        new GlobalValues().setDebug(BuildConfig.DEBUG);
//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(true)
//                .build();
//        Fabric.with(fabric);

        // The following line triggers the initialization of ACRA
        ACRA.init(this);

        try {
            setupDatabase();
        } catch (Exception e) {
            Log.e(TAG, "Setup SQLite DB " + e);
        }
        startWebSocketService();

        /**Initializing PortSipSdk*/
        setupPortSipSDK();
    }

    public static AppComponent getComponent(Context context) {
        return ((OwtelAppController) context.getApplicationContext()).component;
    }

    /**
     * Start web socket service
     */
    public void startWebSocketService() {
        if (!UtilityMethods.isMyServiceRunning(this, WebSocketService.class)) {
            if (GlobalValues.DEBUG) {
                Log.e(TAG, "Starting Serving...");
            }
            startService(new Intent(this, WebSocketService.class));
        } else {
            if (GlobalValues.DEBUG) {
                Log.e(TAG, "Service Running...");
            }
        }
    }

    /**
     * Stop web socket service
     */
    public void stopWebSocketService() {
        if (!UtilityMethods.isMyServiceRunning(this, WebSocketService.class)) {
            if (GlobalValues.DEBUG) {
                Log.e(TAG, "Starting Serving...");
            }
            startService(new Intent(this, WebSocketService.class));
        } else {
            if (GlobalValues.DEBUG) {
                Log.e(TAG, "Service Running...");
            }
        }
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized OwtelAppController getInstance() {
        return sInstance;
    }

    /**
     * AudioManager singleton
     *
     * @return
     */
    public static synchronized AudioManager getAudioManager() {
        return audioManager;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req //     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private void setupDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "katacloud-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    public SharedPreferences getDefaultSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    /**
     * SecurePreferences
     * Get secured SharedPreferences with user-based pin
     *
     * @param password
     * @return
     */
    public SecurePreferences getUserPinBasedSharedPreferences(String password) {
        if (mUserPrefs == null) {
            mUserPrefs = new SecurePreferences(this, password, "owtel_prefs.xml");
        }
        return mUserPrefs;
    }

    public boolean changeUserPrefPassword(String newPassword) {
        if (mUserPrefs != null) {
            try {
                mUserPrefs.handlePasswordChange(newPassword, this);
                return true;
            } catch (GeneralSecurityException e) {
                Log.e(TAG, "Error during password change", e);
            }
        }
        return false;
    }

    /**
     * PortSipSdk Methods
     */
    private void setupPortSipSDK() {
        /**Initializing Portsip notification class*/
        portSipContractListener = (PortSipContract.PortSipContractListener) activity;
        mPortSipSdk = new PortSipSdk();
        mPortSipSdk.setOnPortSIPEvent(this);

        for (int i = 0; i < _CallSessions.length; i++) {
            _CallSessions[i] = new Line(i);
        }

    }

    public Line[] getLines() {
        return _CallSessions;
    }

    public PortSipSdk getPortSipSdk() {
        return mPortSipSdk;
    }

    public boolean isOnline() {
        return _SIPOnline;
    }

    void setConferenceMode(boolean state) {
        conference = state;
    }

    public boolean isConference() {
        return conference;
    }

    public void setLoginState(boolean state) {
        _SIPLogined = state;
        if (state == false) {
            setRegistState(false);
        }
    }

    void setRegistState(boolean state) {
        _SIPOnline = false;
    }

    public void showTipMessage(String text) {
        if (activity != null) {
            if (activity instanceof PortSipCallScreenActivity) {
                ((PortSipCallScreenActivity) activity).showTips(text);
            }
        }
    }

    public int answerSessionCall(Line sessionLine, boolean videoCall) {
        long sessionId = sessionLine.getSessionId();
        int rt = PortSipErrorcode.INVALID_SESSION_ID;
        Ring.getInstance(this).stopRingTone();
        if (sessionId != PortSipErrorcode.INVALID_SESSION_ID) {
            rt = mPortSipSdk.answerCall(sessionLine.getSessionId(), videoCall);
        }

        Log.e(TAG, "answerSessionCall: RT: " + rt + " Session ID: " + sessionId);

        if (rt == 0) {
            sessionLine.setSessionState(true);
            setCurrentLine(sessionLine);
            if (videoCall) {
                sessionLine.setVideoState(true);
            } else {
                sessionLine.setVideoState(false);
            }
//            updateSessionVideo();

            if (conference) {
                mPortSipSdk.joinToConference(sessionLine.getSessionId());
                mPortSipSdk.sendVideo(sessionLine.getSessionId(), sessionLine.getVideoState());
            }
            showTipMessage(sessionLine.getLineName() + ": Call established");
        } else {
            sessionLine.reset();
            showTipMessage(sessionLine.getLineName() + ": failed to answer call !");
        }

        return rt;
    }

//    public void updateSessionVideo() {
//        if (activity != null) {
//            if (activity instanceof PortSipCallScreenActivity) {
//                ((PortSipCallScreenActivity) activity).updateVideo();
//            }
//        }
//    }

    String getLocalIP(boolean ipv6) {
        return netmanager.getLocalIP(ipv6);
    }

    public boolean isCallMissed() {
        return isCallMissed;
    }

    public void setCallMissed(boolean callMissed) {
        isCallMissed = callMissed;
    }

    public void setChronometerStatus(boolean status) {
        isChronometerRunning = status;
    }

    public boolean getChronometerStatus() {
        return isChronometerRunning;
    }

    public Chronometer getChronometer() {
        return mChronometer;
    }


    /**
     * Portsip registration listeners
     */
    @Override
    public void onRegisterSuccess(String reason, int code) {
        Log.e(TAG, "onRegisterSuccess: " + reason + " " + code);
        NotificationHelper.getNotificationHelperInstance()
                .notifyPortSipStatus(getApplicationContext(), "Owtel", "Online (" +
                                SharedPreferenceManager.getSharedPreferenceManagerInstance().getOwtelNumber() + ")",
                        R.drawable.ic_owtel_icon_notification);
        _SIPOnline = true;
        keepCpuRun(true);
    }

    @Override
    public void onRegisterFailure(String reason, int code) {
        Log.e(TAG, "onRegisterFailure: ");
        NotificationHelper.getNotificationHelperInstance()
                .notifyPortSipStatus(getApplicationContext(), "Owtel", "Offline (" +
                                SharedPreferenceManager.getSharedPreferenceManagerInstance().getOwtelNumber() + ")"
                        , R.drawable.ic_owtel_icon_notification);

        _SIPOnline = false;
        keepCpuRun(false);
    }

    /**
     * Call event listeners
     */
    @Override
    public void onInviteIncoming(long sessionId, String callerDisplayName, String caller,
                                 String calleeDisplayName, String callee,
                                 String audioCodecs, String videoCodecs, boolean existsAudio,
                                 boolean existsVideo) {
        Log.e(TAG, "onInviteIncoming: " +
                "CallerDisplayName: " + callerDisplayName +
                " Caller: " + caller +
                " Callee: " + callee);


        /**Auto Reject Blocked Caller*/
        boolean isCallerBlocked = CallBlockHelper.getInstance(OwtelAppController.getInstance())
                .checkIfContactIsBlocked(callerDisplayName);

        if (isCallerBlocked) {
            Log.e(TAG, "onInviteIncoming: " + "Caller is blocked");
            Session currentLine = findSessionByIndex(0);

            Ring.getInstance(this).stopRingTone();
            if (currentLine.getRecvCallState()) {
                mPortSipSdk.rejectCall(currentLine.getSessionId(), 486);
                currentLine.reset();
                Log.e(TAG, "onClick: " + "Reject");
                return;
            }

            if (currentLine.getSessionState()) {
                mPortSipSdk.hangUp(currentLine.getSessionId());
                currentLine.reset();
                Log.e(TAG, "onClick: " + "Hang up");
            }
        } else {
            Line tempSession = findIdleLine();

            if (tempSession == null)// all sessions busy
            {
                mPortSipSdk.rejectCall(sessionId, 486);
                return;
            } else {
                tempSession.setRecvCallState(true);
            }

            Ring.getInstance(this).startRingTone();
            if (existsVideo) {
                // If more than one codecs using, then they are separated with "#",
                // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
                // by yourself.
            }
            if (existsAudio) {
                // If more than one codecs using, then they are separated with "#",
                // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
                // by yourself.
            }

            tempSession.setSessionId(sessionId);
            tempSession.setVideoState(existsVideo);
            String comingCallTips = "Call incoming: " + callerDisplayName + "<" + caller + ">";
            tempSession.setDescriptionString(comingCallTips);
//        sendSessionChangeMessage(comingCallTips, SESSION_CHANG);
            setCurrentLine(tempSession);

            if (existsVideo) {
                Log.e(TAG, "onInviteIncoming: " + "This is a video call");
            } else {//Audio call
                curSession = tempSession;
                isCallMissed = true;

                if (callerDisplayName.equals("Unavailable")
                        || callerDisplayName.equals("anonymous")) {
                    callerDisplayName = caller;
                }

                startActivity(new Intent(this, PortSipCallScreenActivity.class)
                        .putExtra("isIncomingCall", true)
                        .putExtra("callerDisplayName", callerDisplayName)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }

            Toast.makeText(this, comingCallTips, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInviteTrying(long sessionId) {
        Log.e(TAG, "onInviteTrying: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Call is trying...");
//        sendSessionChangeMessage("Call is trying...", SESSION_CHANG);
    }

    @Override
    public void onInviteSessionProgress(long sessionId, String audioCodecs,
                                        String videoCodecs, boolean existsEarlyMedia, boolean existsAudio,
                                        boolean existsVideo) {
        Log.e(TAG, "onInviteSessionProgress: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        if (existsVideo) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        if (existsAudio) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }

        tempSession.setSessionState(true);

        tempSession.setDescriptionString("Call session progress.");
//        sendSessionChangeMessage("Call session progress.", SESSION_CHANG);
        tempSession.setEarlyMeida(existsEarlyMedia);

    }

    @Override
    public void onInviteRinging(long sessionId, String statusText, int statusCode) {
        Log.e(TAG, "onInviteRinging: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        if (!tempSession.hasEarlyMeida()) {
            // Hasn't the early media, you must play the local WAVE file for
            // ringing tone
            Ring.getInstance(this).startRingBackTone(false);
        }

        tempSession.setDescriptionString("Ringing...");
//        sendSessionChangeMessage("Ringing...", SESSION_CHANG);
    }

    @Override
    public void onInviteAnswered(long sessionId, String callerDisplayName, String caller,
                                 String calleeDisplayName, String callee,
                                 String audioCodecs, String videoCodecs, boolean existsAudio,
                                 boolean existsVideo) {
        Log.e(TAG, "onInviteAnswered: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        if (existsVideo) {
            mPortSipSdk.sendVideo(tempSession.getSessionId(), true);
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        if (existsAudio) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        tempSession.setVideoState(existsVideo);
        tempSession.setSessionState(true);
        tempSession.setDescriptionString("call established");
//        sendSessionChangeMessage("call established", SESSION_CHANG);

        if (isConference()) {
            mPortSipSdk.joinToConference(tempSession.getSessionId());
            mPortSipSdk.sendVideo(tempSession.getSessionId(), tempSession.getVideoState());
            tempSession.setHoldState(false);

        }

        // If this is the refer call then need set it to normal
        if (tempSession.isReferCall()) {
            tempSession.setReferCall(false, 0);
        }

    }

    @Override
    public void onInviteFailure(long sessionId, String reason, int code) {
        Log.e(TAG, "onInviteFailure: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("call failure" + reason);
//        sendSessionChangeMessage("call failure" + reason, SESSION_CHANG);
        if (tempSession.isReferCall()) {
            // Take off the origin call from HOLD if the refer call is failure
            Line originSession = findLineBySessionId(tempSession
                    .getOriginCallSessionId());
            if (originSession != null) {
                mPortSipSdk.unHold(originSession.getSessionId());
                originSession.setHoldState(false);

                // Switch the currently line to origin call line
                setCurrentLine(originSession);

                tempSession.setDescriptionString("refer failure:" + reason
                        + "resume orignal call");
//                sendSessionChangeMessage("call failure" + reason, SESSION_CHANG);
            }
        }

        tempSession.reset();

    }

    @Override
    public void onInviteUpdated(long sessionId, String audioCodecs,
                                String videoCodecs, boolean existsAudio, boolean existsVideo) {
        Log.e(TAG, "onInviteUpdated: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        if (existsVideo) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }
        if (existsAudio) {
            // If more than one codecs using, then they are separated with "#",
            // for example: "g.729#GSM#AMR", "H264#H263", you have to parse them
            // by yourself.
        }

        tempSession.setDescriptionString("Call is updated");
    }

    @Override
    public void onInviteConnected(long sessionId) {
        Log.e(TAG, "onInviteConnected: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Call is connected");
        ((PortSipCallScreenActivity) activity).onInviteCallConnected();
        Ring.getInstance(getApplicationContext()).stop();
//        sendSessionChangeMessage("Call is connected", SESSION_CHANG);
//        updateSessionVideo();
    }

    @Override
    public void onInviteBeginingForward(String forwardTo) {
        Log.e(TAG, "onInviteBeginingForward: ");
//        sendSessionChangeMessage("An incoming call was forwarded to: " + forwardTo, SESSION_CHANG);
    }

    @Override
    public void onInviteClosed(long sessionId) {
        Log.e(TAG, "onInviteClosed: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        Ring.getInstance(this).stopRingTone();
        tempSession.reset();
//        updateSessionVideo();
        tempSession.setDescriptionString(": Call closed.");
//        sendSessionChangeMessage(": Call closed.", SESSION_CHANG);
        ((PortSipCallScreenActivity) activity).onInviteCallEnded();
        Ring.getInstance(getApplicationContext()).stop();
    }

    @Override
    public void onRemoteHold(long sessionId) {
        Log.e(TAG, "onRemoteHold: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Placed on hold by remote.");
//        sendSessionChangeMessage("Placed on hold by remote.", SESSION_CHANG);
    }

    @Override
    public void onRemoteUnHold(long sessionId, String audioCodecs,
                               String videoCodecs, boolean existsAudio, boolean existsVideo) {
        Log.e(TAG, "onRemoteUnHold: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Take off hold by remote.");
//        sendSessionChangeMessage("Take off hold by remote.", SESSION_CHANG);
    }

    @Override
    public void onRecvDtmfTone(long sessionId, int tone) {
        // TODO Auto-generated method stub
        Log.e(TAG, "onRecvDtmfTone: ");

    }

    @Override
    public void onReceivedRefer(long sessionId, final long referId, String to,
                                String from, final String referSipMessage) {
        Log.e(TAG, "onReceivedRefer: ");
        final Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            mPortSipSdk.rejectRefer(referId);
            return;
        }

        final Line referSession = findIdleLine();

        if (referSession == null)// all sessions busy
        {
            mPortSipSdk.rejectRefer(referId);
            return;
        } else {
            referSession.setSessionState(true);
        }

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE: {

                        mPortSipSdk.rejectRefer(referId);
                        referSession.reset();
                    }
                    break;
                    case DialogInterface.BUTTON_POSITIVE: {

                        mPortSipSdk.hold(tempSession.getSessionId());// hold current session
                        tempSession.setHoldState(true);

                        tempSession
                                .setDescriptionString("Place currently call on hold on line: ");

                        long referSessionId = mPortSipSdk.acceptRefer(referId,
                                referSipMessage);
                        if (referSessionId <= 0) {
                            referSession
                                    .setDescriptionString("Failed to accept REFER on line");

                            referSession.reset();

                            // Take off hold
                            mPortSipSdk.unHold(tempSession.getSessionId());
                            tempSession.setHoldState(false);
                        } else {
                            referSession.setSessionId(referSessionId);
                            referSession.setSessionState(true);
                            referSession.setReferCall(true,
                                    tempSession.getSessionId());

                            referSession
                                    .setDescriptionString("Accepted the refer, new call is trying on line ");

                            _CurrentlyLine = referSession;

                            tempSession
                                    .setDescriptionString("Now current line is set to: "
                                            + _CurrentlyLine.getLineName());
//                            updateSessionVideo();
                        }
                    }
                }

            }
        };
        showGlobalDialog("Received REFER", "accept", listener, "reject",
                listener);

    }

    @Override
    public void onReferAccepted(long sessionId) {
        Log.e(TAG, "onReferAccepted: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("the REFER was accepted.");
//        sendSessionChangeMessage("the REFER was accepted.", SESSION_CHANG);
    }

    @Override
    public void onReferRejected(long sessionId, String reason, int code) {
        Log.e(TAG, "onReferRejected: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("the REFER was rejected.");
//        sendSessionChangeMessage("the REFER was rejected.", SESSION_CHANG);
    }

    @Override
    public void onTransferTrying(long sessionId) {
        Log.e(TAG, "onTransferTrying: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Transfer Trying.");
//        sendSessionChangeMessage("Transfer Trying.", SESSION_CHANG);
    }

    @Override
    public void onTransferRinging(long sessionId) {
        Log.e(TAG, "onTransferRinging: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Transfer Ringing.");
//        sendSessionChangeMessage("Transfer Ringing.", SESSION_CHANG);
    }

    @Override
    public void onACTVTransferSuccess(long sessionId) {
        Log.e(TAG, "onACTVTransferSuccess: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }
        tempSession.setDescriptionString("Transfer succeeded.");
    }

    @Override
    public void onACTVTransferFailure(long sessionId, String reason, int code) {
        Log.e(TAG, "onACTVTransferFailure: ");
        Line tempSession = findLineBySessionId(sessionId);
        if (tempSession == null) {
            return;
        }

        tempSession.setDescriptionString("Transfer failure");

        // reason is error reason
        // code is error code

    }

    public Line findLineBySessionId(long sessionId) {
        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
            if (_CallSessions[i].getSessionId() == sessionId) {
                return _CallSessions[i];
            }
        }

        return null;
    }

    public Line findSessionByIndex(int index) {

        if (Line.LINE_BASE <= index && index < Line.MAX_LINES) {
            return _CallSessions[index];
        }

        return null;
    }

    static Line findIdleLine() {

        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i)// get idle session
        {
            if (!_CallSessions[i].getSessionState()
                    && !_CallSessions[i].getRecvCallState()) {
                return _CallSessions[i];
            }
        }

        return null;
    }

    public void setCurrentLine(Line line) {
        if (line == null) {
            _CurrentlyLine = _CallSessions[Line.LINE_BASE];
        } else {
            _CurrentlyLine = line;
        }

    }

    public Session getCurrentSession() {
        return _CurrentlyLine;
    }

    @Override
    public void onReceivedSignaling(long sessionId, String message) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendingSignaling(long sessionId, String message) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onWaitingVoiceMessage(String messageAccount,
                                      int urgentNewMessageCount, int urgentOldMessageCount,
                                      int newMessageCount, int oldMessageCount) {
        String text = messageAccount;
        text += " has voice message.";

//        showMessage(text);
        // You can use these parameters to check the voice message count

        // urgentNewMessageCount;
        // urgentOldMessageCount;
        // newMessageCount;
        // oldMessageCount;

    }

    @Override
    public void onWaitingFaxMessage(String messageAccount,
                                    int urgentNewMessageCount, int urgentOldMessageCount,
                                    int newMessageCount, int oldMessageCount) {
        String text = messageAccount;
        text += " has FAX message.";

        showMessage(text);
        // You can use these parameters to check the FAX message count

        // urgentNewMessageCount;
        // urgentOldMessageCount;
        // newMessageCount;
        // oldMessageCount;

    }

    @Override
    public void onPresenceRecvSubscribe(long subscribeId,
                                        String fromDisplayName, String from, String subject) {
        Log.e(TAG, "onPresenceRecvSubscribe: ");

        String fromSipUri = "sip:" + from;

        final long tempId = subscribeId;
        DialogInterface.OnClickListener onClick;
        SipContact contactReference = null;
        boolean contactExist = false;

        for (int i = 0; i < contacts.size(); ++i) {
            contactReference = contacts.get(i);
            String SipUri = contactReference.getSipAddr();

            if (SipUri.equals(fromSipUri)) {
                contactExist = true;
                if (contactReference.isAccept()) {
                    long nOldSubscribeID = contactReference.getSubId();
                    mPortSipSdk.presenceAcceptSubscribe(tempId);
                    String status = "Available";
                    mPortSipSdk.presenceOnline(tempId, status);

                    if (contactReference.isSubscribed() && nOldSubscribeID >= 0) {
                        mPortSipSdk.presenceSubscribeContact(fromSipUri, subject);
                    }
                    return;
                } else {
                    break;
                }
            }
        }


        if (!contactExist) {
            contactReference = new SipContact();
            contacts.add(contactReference);
            contactReference.setSipAddr(fromSipUri);
        }
        final SipContact contact = contactReference;
        onClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        mPortSipSdk.presenceAcceptSubscribe(tempId);
                        contact.setSubId(tempId);
                        contact.setAccept(true);
                        String status = "Available";
                        mPortSipSdk.presenceOnline(tempId, status);
                        contact.setSubstatus(true);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        contact.setAccept(false);// reject subscribe
                        contact.setSubId(0);
                        contact.setSubstatus(false);// offline

                        mPortSipSdk.presenceRejectSubscribe(tempId);
                        break;
                    default:
                        break;
                }
                dialog.dismiss();
            }
        };
        showGlobalDialog(from, "Accept", onClick, "Reject", onClick);

    }

    @Override
    public void onPresenceOnline(String fromDisplayName, String from,
                                 String stateText) {
        Log.e(TAG, "onPresenceOnline: ");

        String fromSipUri = "sip:" + from;
        SipContact contactReference;
        for (int i = 0; i < contacts.size(); ++i) {
            contactReference = contacts.get(i);
            String SipUri = contactReference.getSipAddr();
            if (SipUri.endsWith(fromSipUri)) {
                contactReference.setSubDescription(stateText);
                contactReference.setSubstatus(true);// online
            }
        }
//        sendSessionChangeMessage("contact status change.", CONTACT_CHANG);
    }

    @Override
    public void onPresenceOffline(String fromDisplayName, String from) {
        Log.e(TAG, "onPresenceOffline: ");

        String fromSipUri = "sip:" + from;
        SipContact contactReference;
        for (int i = 0; i < contacts.size(); ++i) {
            contactReference = contacts.get(i);
            String SipUri = contactReference.getSipAddr();
            if (SipUri.endsWith(fromSipUri)) {
                contactReference.setSubstatus(false);// "Offline";
                contactReference.setSubId(0);
            }
        }
//        sendSessionChangeMessage("contact status change.", CONTACT_CHANG);
    }

    @Override
    public void onRecvOptions(String optionsMessage) {
        // String text = "Received an OPTIONS message: ";
        // text += optionsMessage.toString();
        // showTips(text);
    }

    @Override
    public void onRecvInfo(String infoMessage) {

        // String text = "Received a INFO message: ";
        // text += infoMessage.toString();
        // showTips(text);
    }

    @Override
    public void onRecvMessage(long sessionId, String mimeType,
                              String subMimeType, byte[] messageData, int messageDataLength) {

    }

    @Override
    public void onRecvOutOfDialogMessage(String fromDisplayName, String from,
                                         String toDisplayName, String to, String mimeType,
                                         String subMimeType, byte[] messageData, int messageDataLength) {
        String text = "Received a " + mimeType + "message(out of dialog) from ";
        text += from;

        if (mimeType.equals("text") && subMimeType.equals("plain")) {
            // String receivedMsg = GetString(messageData);
            showMessage(text);
        } else if (mimeType.equals("application")
                && subMimeType.equals("vnd.3gpp.sms")) {
            // The messageData is binary data

            showMessage(text);
        } else if (mimeType.equals("application")
                && subMimeType.equals("vnd.3gpp2.sms")) {
            // The messageData is binary data
            showMessage(text);

        }
    }

    @Override
    public void onPlayAudioFileFinished(long sessionId, String fileName) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onPlayVideoFileFinished(long sessionId) {
        // TODO Auto-generated method stub
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    @Override
    public void onSendMessageSuccess(long sessionId, long messageId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendMessageFailure(long sessionId, long messageId,
                                     String reason, int code) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendOutOfDialogMessageSuccess(long messageId,
                                                String fromDisplayName, String from, String toDisplayName, String to) {
    }

    @Override
    public void onSendOutOfDialogMessageFailure(long messageId,
                                                String fromDisplayName, String from, String toDisplayName,
                                                String to, String reason, int code) {
    }

    @Override
    public void onReceivedRTPPacket(long sessionId, boolean isAudio,
                                    byte[] RTPPacket, int packetSize) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendingRTPPacket(long sessionId, boolean isAudio,
                                   byte[] RTPPacket, int packetSize) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAudioRawCallback(long sessionId, int enum_audioCallbackMode,
                                   byte[] data, int dataLength, int samplingFreqHz) {
        // TODO Auto-generated method stub

    }

    @Override
    public int onVideoRawCallback(long sessionId, int enum_videoCallbackMode,
                                  int width, int height, byte[] data, int dataLength) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void onVideoDecodedInfoCallback(long sessionId, int width, int height,
                                           int framerate, int bitrate) {
        Line line = findLineBySessionId(sessionId);
        if (width != 0 && height != 0 && (line.getVideoWidth() != width || line.getVideoHeight() != height)) {
            line.setVideoWidth(width);
            line.setVideoHeight(height);
            line.notifyObservers();
        }
    }

    void showMessage(String message) {
        DialogInterface.OnClickListener listener = null;
        showGlobalDialog(message, null, listener, "Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
    }

    void showGlobalDialog(String message, String strPositive,
                          DialogInterface.OnClickListener positiveListener, String strNegative,
                          DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        if (positiveListener != null) {
            builder.setPositiveButton(strPositive, positiveListener);
        }

        if (negativeListener != null) {
            builder.setNegativeButton(strNegative, negativeListener);
        }

        AlertDialog ad = builder.create();
        ad.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        ad.setCanceledOnTouchOutside(false);
        ad.show();
    }

    public void bringToFront() {

//        try {
//            Intent startActivity = new Intent();
//            startActivity.setClass(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pi = PendingIntent.getActivity(this, 0, startActivity, 0);
//
//            pi.send(this, 0, null);
//        } catch (PendingIntent.CanceledException e) {
//            e.printStackTrace();
//        }
    }

    public void keepCpuRun(boolean keepRun) {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (keepRun == true) { //open
            if (mCpuLock == null) {
                if ((mCpuLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SipSampleCpuLock")) == null) {
                    return;
                }
                mCpuLock.setReferenceCounted(false);
            }

            synchronized (mCpuLock) {
                if (!mCpuLock.isHeld()) {
                    mCpuLock.acquire();
                }
            }
        } else {//close
            if (mCpuLock != null) {
                synchronized (mCpuLock) {
                    if (mCpuLock.isHeld()) {
                        //Log.d(TAG,"releasePowerLock()");
                        mCpuLock.release();
                    }
                }
            }
        }
    }
}

