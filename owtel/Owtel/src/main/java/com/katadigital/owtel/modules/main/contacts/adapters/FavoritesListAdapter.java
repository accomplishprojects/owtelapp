package com.katadigital.owtel.modules.main.contacts.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.helpers.AlertDialogHelper;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Util;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.owtel.modules.main.favorites.FavoritesFragment;
import com.katadigital.portsip.utilities.ui.image.ImageCustomManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class FavoritesListAdapter extends RecyclerView.Adapter<FavoritesListAdapter.ViewHolder> {

    ArrayList<ContactDto> favoriteList;
    Fragment fragment;
    Dialog selectNumberDialog;
    private ProgressDialog progressDialog;
    QuickContactHelper quickContactHelper;
    AlertDialogHelper alertDialog;
    Activity context;
    public static boolean editIsPressed = false;
    ContactDto contactDto;

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, numberType, lettericon;
        CircleImageView contactProfile;
        Button deleteBtn, faveInfoBtn;
        View faveInfo;
        ImageView imgRemoveFavorites;
        View listItem;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.favor_item_textview);
            numberType = (TextView) view.findViewById(R.id.favor_number_type);
            lettericon = (TextView) view.findViewById(R.id.letter_icon);
            contactProfile = (CircleImageView) view.findViewById(R.id.logs_item_imageview);
            deleteBtn = (Button) view.findViewById(R.id.fav_item_delete_btn);
            faveInfoBtn = (Button) view.findViewById(R.id.favor_item_info_btn);
            faveInfo = view.findViewById(R.id.favor_item_info);
            imgRemoveFavorites = (ImageView) view.findViewById(R.id.favor_delete_btn_layout);
            listItem = view.findViewById(R.id.favor_list_item);

            /**Setting up views attributes*/
            contactProfile.setVisibility(View.VISIBLE);
            lettericon.setVisibility(View.GONE);

            lettericon.setTypeface(NewUtil.getFontRoman(context));
            lettericon.setTextSize(NewUtil.gettxtSize());

            numberType.setTypeface(NewUtil.getFontRoman(context));
            numberType.setTextSize(NewUtil.gettxtSize());
            numberType.setTextColor(context.getResources().getColor(
                    R.color.gray_notes));

            name.setTypeface(NewUtil.getFontRoman(context));
            name.setTextSize(NewUtil.gettxtSize());
        }
    }

    public FavoritesListAdapter(Activity context, ArrayList<ContactDto> favoriteList,
                                Fragment fragment, boolean editpress) {
        this.context = context;
        this.favoriteList = favoriteList;
        this.fragment = fragment;
        editIsPressed = editpress;
        alertDialog = new AlertDialogHelper();

        ImageCustomManager.init();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View views = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_favorites_list, parent, false);

        return new ViewHolder(views);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        contactDto = favoriteList.get(position);
        if (contactDto.getProfilepic() != null) {
            /**Displaying profile picture*/
            ImageCustomManager.getImageCustomManagerInstance()
                    .displayProfilePictureFromBitmap(context, contactDto.getProfilepic(), holder.contactProfile);

        } else {
            try {
                if (contactDto.getFirstName().equals("")
                        && contactDto.getLastName().equals("")) {
                    // && contactDto.getPhoneNumbers().size() > 0) {
                    holder.contactProfile.setImageResource(R.drawable.profile_pic_holder);
                } else if ((!contactDto.getFirstName().equals("") || !contactDto
                        .getLastName().equals(""))) {
                    // && contactDto.getPhoneNumbers().size() > 0) {
                    String iniFName = "", iniLName = "";
                    holder.contactProfile.setVisibility(View.GONE);
                    holder.lettericon.setVisibility(View.VISIBLE);

                    if (!(contactDto.getFirstName() == null)) {
                        if (!contactDto.getFirstName().equals("")) {
                            iniFName = contactDto.getFirstName()
                                    .substring(0, 1).toUpperCase();
                        } else {
                            iniFName = "";
                        }
                    } else
                        iniFName = "";
                    if (!(contactDto.getLastName() == null)) {
                        if (!contactDto.getLastName().equals("")) {
                            iniLName = contactDto.getLastName().substring(0, 1)
                                    .toUpperCase();
                        } else {
                            iniLName = "";
                        }
                    } else
                        iniLName = "";
                    holder.lettericon.setText(iniFName + iniLName);
                }

            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        if (contactDto != null) {
            System.out.println("BERTAX display name -"
                    + contactDto.getDisplayName());

            if (contactDto.getDisplayName().trim().equals("")
                    || contactDto.getDisplayName().isEmpty()) {

                if (contactDto.getFirstName() != null) {

                    if (contactDto.getFirstName().trim().equals("")
                            && contactDto.getLastName().trim().equals("")) {
                        // nickname
                        if (contactDto.getNickname().trim().equals("")
                                || contactDto.getNickname().isEmpty()) {
                            holder.name.setText(context.getResources().getString(
                                    R.string.string_noname));
                        } else {
                            holder.name.setText(contactDto.getNickname());
                        }

                    } else if ((!contactDto.getFirstName().equals("") || !contactDto
                            .getLastName().equals(""))) {
                        String iniFName = "", iniLName = "";
                        if (!(contactDto.getFirstName() == null)) {

                            if (!contactDto.getFirstName().equals("")) {
                                iniFName = contactDto.getFirstName();
                            } else {
                                iniFName = "";
                            }

                        } else
                            iniFName = "";

                        if (!(contactDto.getLastName() == null)) {
                            if (!contactDto.getLastName().equals("")) {
                                iniLName = contactDto.getLastName();
                            } else {
                                iniLName = "";
                            }

                        } else
                            iniLName = "";

                        holder.name.setText(iniFName + " " + iniLName);

                    }
                }

            } else {
                holder.name.setText(contactDto.getDisplayName());

            }

            if (contactDto.getPhoneNumbers().size() > 0) {

                for (PhoneNumberDto phoneNumberDto : contactDto
                        .getPhoneNumbers()) {
                    if (!phoneNumberDto.getNumber().isEmpty()) {
                        holder.numberType.setText(phoneNumberDto.getNumberType());
                        break;
                    }
                }

            }


            holder.faveInfoBtn.setBackgroundDrawable(context.getResources()
                    .getDrawable(R.drawable.information));

            holder.faveInfoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new FavoritesListAdapter.loadContactDetails(favoriteList.get(position))
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            });

            if (editIsPressed == true) {

                holder.faveInfoBtn.setVisibility(View.GONE);
                holder.imgRemoveFavorites.setVisibility(View.VISIBLE);
                holder.imgRemoveFavorites.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.deleteBtn.setVisibility(View.VISIBLE);
                        holder.imgRemoveFavorites.setVisibility(View.GONE);
                    }
                });

                holder.deleteBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ContentValues values = new ContentValues();

                        String[] fv = new String[]{favoriteList.get(position).getContactID()};
                        values.put(ContactsContract.Contacts.STARRED, 0);
                        context.getContentResolver().update(
                                ContactsContract.Contacts.CONTENT_URI, values,
                                ContactsContract.Contacts._ID + "= ?", fv);
                        favoriteList.remove(position);
                        notifyDataSetChanged();

                        if (favoriteList.size() == 0) {
                            ((FavoritesFragment) fragment)
                                    .getFavoriteContacts(false);
                        }

                        holder.deleteBtn.setVisibility(View.GONE);
                    }
                });
            }

            holder.listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    quickContactHelper = new QuickContactHelper(context);
                    ContactDto contactDtos = quickContactHelper
                            .getContactDetails(favoriteList.get(position).getContactID());
                    Util.addDialogSendMessage(context, contactDtos, "call");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }


    class loadContactDetails extends AsyncTask<String, String, String> {
        ContactDto contactDto;

        public loadContactDetails(ContactDto contactDto) {
            this.contactDto = contactDto;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(
                    context,
                    context.getResources().getString(
                            R.string.string_diag_pleasewait),
                    context.getResources().getString(
                            R.string.string_diag_loading_details)
                            + "...");
            progressDialog.setCancelable(false);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            if (contactDto != null) {
                quickContactHelper = new QuickContactHelper(context);
                ContactDetailActivity.contactDto = quickContactHelper
                        .getContactDetails(contactDto.getContactID() + "");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            Intent intent = new Intent(context, ContactDetailActivity.class);
            context.startActivity(intent);
            Bundle bundleObject = new Bundle();
            ContactDetailActivity.favTitle = context.getResources().getString(
                    R.string.tab_liked);
            context.overridePendingTransition(
                    R.anim.slide_in_up, R.anim.slide_in_up_exit);

            super.onPostExecute(result);
        }
    }

}
