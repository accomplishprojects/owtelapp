package com.katadigital.owtel.modules.main.contacts.util;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Nickname;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.DateDto;
import com.katadigital.owtel.modules.main.contacts.entities.EmailDto;
import com.katadigital.owtel.modules.main.contacts.entities.IMDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.entities.RelationDto;
import com.katadigital.owtel.modules.main.contacts.entities.WebsiteDto;

public class EditFunction {

    EditContactsActivity editActivity;

    public EditFunction(EditContactsActivity editActivity) {
        this.editActivity = editActivity;
    }

    public void updateInformation(ContactDto contactDto)
            throws RemoteException, OperationApplicationException {

        String strID = contactDto.getContactID();
        int intID = Integer.valueOf(contactDto.getContactID());

        ArrayList<ContentProviderOperation> ops = new ArrayList<>();

        // =============================================================
        // AVATAR
        if (contactDto.getProfilepic() != null) {
            String where = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE + " = ?";
            String[] params = new String[]{strID, Photo.CONTENT_ITEM_TYPE};

//            ops.add(ContentProviderOperation
//                    .newUpdate(Data.CONTENT_URI)
//                    .withSelection(where, params)
//                    .withValue(Photo.PHOTO,
//                            Util.getbitMapResize(contactDto.getProfilepic()))
//                    .build());

            Bitmap bmp = contactDto.getProfilepic();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

            ops.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(where, params)
                    .withValue(Photo.PHOTO,
                            stream.toByteArray())
                    .build());

            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, ops);
        } else {
            String where = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                    + " = ?";
            String[] params = new String[]{strID, Photo.CONTENT_ITEM_TYPE};
            ops.add(ContentProviderOperation.newUpdate(Data.CONTENT_URI)
                    .withSelection(where, params).withValue(Photo.PHOTO, null)
                    .build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, ops);
        }

        if (contactDto.getFirstName().isEmpty()
                || contactDto.getMiddleName().isEmpty()
                || contactDto.getLastName().isEmpty()) {
            // =============================================================
            // NAMES
            String whereInfo = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE
                    + " = ?";
            String[] paramsInfo = new String[]{strID,
                    StructuredName.CONTENT_ITEM_TYPE};
            ops.add(ContentProviderOperation
                    .newUpdate(Data.CONTENT_URI)
                    .withSelection(whereInfo, paramsInfo)
                    .withValue(StructuredName.PREFIX, contactDto.getPrefix())
                    .withValue(StructuredName.GIVEN_NAME,
                            contactDto.getFirstName())
                    .withValue(StructuredName.MIDDLE_NAME,
                            contactDto.getMiddleName())
                    .withValue(StructuredName.FAMILY_NAME,
                            contactDto.getLastName())
                    .withValue(StructuredName.PHONETIC_GIVEN_NAME,
                            contactDto.getPhonetic_fName())
                    .withValue(StructuredName.PHONETIC_MIDDLE_NAME,
                            contactDto.getPhonetic_middleName())
                    .withValue(StructuredName.PHONETIC_FAMILY_NAME,
                            contactDto.getPhonetic_lName())
                    .withValue(StructuredName.SUFFIX, contactDto.getSuffix())
                    .build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, ops);
        }

        // =============================================================
        // NICKNAME
        String whereNick = Data.CONTACT_ID + "=?" + " AND " + Data.MIMETYPE
                + "=?";
        String[] paramsNick = new String[]{strID, Nickname.CONTENT_ITEM_TYPE};

        ops.add(ContentProviderOperation.newUpdate(Data.CONTENT_URI)
                .withSelection(whereNick, paramsNick)
                .withValue(Data.MIMETYPE, Nickname.CONTENT_ITEM_TYPE)
                .withValue(Nickname.NAME, contactDto.getNickname())
                .withValue(Nickname.TYPE, Nickname.TYPE_SHORT_NAME).build());
        editActivity.getContentResolver().applyBatch(
                ContactsContract.AUTHORITY, ops);

        // =============================================================
        // JOB
        String whereJob = Data.CONTACT_ID + "=?" + " AND " + Data.MIMETYPE
                + "=?";
        String[] paramsJob = new String[]{strID,
                Organization.CONTENT_ITEM_TYPE};

        ops.add(ContentProviderOperation.newUpdate(Data.CONTENT_URI)
                .withSelection(whereJob, paramsJob)
                .withValue(Data.MIMETYPE, Organization.CONTENT_ITEM_TYPE)
                .withValue(Organization.COMPANY, contactDto.getCompany())
                .withValue(Organization.TYPE, Organization.TYPE_WORK)
                .withValue(Organization.TITLE, contactDto.getJobTitle())
                .withValue(Organization.TYPE, Organization.TYPE_CUSTOM)
                .withValue(Organization.DEPARTMENT, contactDto.getJobDep())
                .withValue(Organization.TYPE, Organization.TYPE_CUSTOM).build());
        editActivity.getContentResolver().applyBatch(
                ContactsContract.AUTHORITY, ops);

        // =============================================================
        // BDAY
        String whereBday = Data.CONTACT_ID + "=?" + " AND " + Data.MIMETYPE
                + "=?";
        String[] paramsBday = new String[]{contactDto.getContactID(),
                Event.CONTENT_ITEM_TYPE};
        ops.add(ContentProviderOperation.newUpdate(Data.CONTENT_URI)
                .withSelection(whereBday, paramsBday)
                .withValue(Data.MIMETYPE, Event.CONTENT_ITEM_TYPE)
                .withValue(Event.START_DATE, contactDto.getBirthday())
                .withValue(Event.TYPE, Event.TYPE_BIRTHDAY).build());
        editActivity.getContentResolver().applyBatch(
                ContactsContract.AUTHORITY, ops);

        // =============================================================
        // NOTES
        String whereNote = Data.CONTACT_ID + "=?" + " AND " + Data.MIMETYPE
                + "=?";
        String[] paramsNote = new String[]{contactDto.getContactID(),
                Note.CONTENT_ITEM_TYPE};

        ops.add(ContentProviderOperation
                .newUpdate(Data.CONTENT_URI)
                .withSelection(whereNote, paramsNote)
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Note.NOTE,
                        contactDto.getNote()).build());
        editActivity.getContentResolver().applyBatch(
                ContactsContract.AUTHORITY, ops);

    }

    public void deleteArrayInfo(ContactDto contactDto,
                                final ContactDto oldContactDto) throws RemoteException,
            OperationApplicationException {

        // String strID = oldContactDto.getContactID();
        // int intID = Integer.valueOf(oldContactDto.getContactID());

        String strID = "";
        ContentResolver cr = editActivity.getContentResolver();
        Cursor cur = cr.query(RawContacts.CONTENT_URI, null,
                RawContacts.CONTACT_ID + "=?",
                new String[]{contactDto.getContactID()}, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(RawContacts._ID));
                System.out.println("BERTAX RAW CONTACT ID " + id);
                strID = id;
            }

        }

        System.out.println("JM DADAAAN KA BA ");

        // =============================================================
        // PHONE_NUMBER
        ArrayList<ContentProviderOperation> num_oplist = new ArrayList<ContentProviderOperation>();

        for (PhoneNumberDto num : oldContactDto.getPhoneNumbers()) {
            int phoneTypeIndex = 0;

            System.out.println("JM SA LOOB " + num.getNumber());

            if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_mobile))) {
                phoneTypeIndex = Phone.TYPE_MOBILE;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                phoneTypeIndex = Phone.TYPE_WORK;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                phoneTypeIndex = Phone.TYPE_HOME;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_main))) {
                phoneTypeIndex = Phone.TYPE_MAIN;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_workfax))) {
                phoneTypeIndex = Phone.TYPE_FAX_WORK;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_homefax))) {
                phoneTypeIndex = Phone.TYPE_FAX_HOME;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_pager))) {
                phoneTypeIndex = Phone.TYPE_PAGER;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                phoneTypeIndex = Phone.TYPE_OTHER;
            }

            String where = Data.RAW_CONTACT_ID + "=?" + " AND " + Data.MIMETYPE
                    + "=?" + " AND " + Phone.TYPE + "=?" + " AND "
                    + Phone.NUMBER + "=?";
            String[] params = new String[]{strID, Phone.CONTENT_ITEM_TYPE,
                    String.valueOf(phoneTypeIndex), num.getNumber()};

            num_oplist.add(ContentProviderOperation.newDelete(Data.CONTENT_URI)
                    .withSelection(where, params).build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, num_oplist);
        }

        // =============================================================
        // EMAIL
        ArrayList<ContentProviderOperation> emails_oplist = new ArrayList<ContentProviderOperation>();

        for (EmailDto emailDto : oldContactDto.getEmails()) {

            int emailTypeIndex = 0;

            if (emailDto.getEmailType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                emailTypeIndex = Email.TYPE_WORK;
            } else if (emailDto.getEmailType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                emailTypeIndex = Email.TYPE_HOME;
            }
            if (emailDto.getEmailType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                emailTypeIndex = Email.TYPE_OTHER;
            }

            String where = Data.RAW_CONTACT_ID + "=?" + " AND " + Data.MIMETYPE
                    + "=?" + " AND " + Email.TYPE + "=?" + " AND " + Email.DATA
                    + "=?";
            String[] params = new String[]{strID, Email.CONTENT_ITEM_TYPE,
                    String.valueOf(emailTypeIndex), emailDto.getEmail()};

            emails_oplist.add(ContentProviderOperation
                    .newDelete(Data.CONTENT_URI).withSelection(where, params)
                    .build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, emails_oplist);

        }

        // =============================================================
        // URL
        ArrayList<ContentProviderOperation> web_oplist = new ArrayList<ContentProviderOperation>();
        for (WebsiteDto websites : oldContactDto.getWebsites()) {

            int websiteTypeIndex = 0;

            if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_homepage))) {
                websiteTypeIndex = Website.TYPE_HOMEPAGE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_blog))) {
                websiteTypeIndex = Website.TYPE_BLOG;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_profile))) {
                websiteTypeIndex = Website.TYPE_PROFILE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                websiteTypeIndex = Website.TYPE_HOME;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                websiteTypeIndex = Website.TYPE_HOMEPAGE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_ftp))) {
                websiteTypeIndex = Website.TYPE_FTP;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                websiteTypeIndex = Website.TYPE_OTHER;
            }

            String where = Data.RAW_CONTACT_ID + "=?" + " AND " + Data.MIMETYPE
                    + "=?" + " AND " + Website.TYPE + "=?" + " AND "
                    + Website.URL + "=?";
            String[] params = new String[]{strID, Website.CONTENT_ITEM_TYPE,
                    String.valueOf(websiteTypeIndex), websites.getWebsite()};

            web_oplist.add(ContentProviderOperation.newDelete(Data.CONTENT_URI)
                    .withSelection(where, params).build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, web_oplist);

        }

        // =============================================================
        // ADDRESS
        ArrayList<ContentProviderOperation> address_oplist = new ArrayList<ContentProviderOperation>();
        for (AddressDto addressDto : oldContactDto.getAddresses()) {
            int addressTypeIndex = 0;

            if (addressDto.getAddressType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                addressTypeIndex = StructuredPostal.TYPE_WORK;
            } else if (addressDto.getAddressType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                addressTypeIndex = StructuredPostal.TYPE_HOME;
            }
            if (addressDto.getAddressType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                addressTypeIndex = StructuredPostal.TYPE_OTHER;
            }

            String where = ContactsContract.Data.RAW_CONTACT_ID + "=?"
                    + " AND " + ContactsContract.Data.MIMETYPE + "=?" + " AND "
                    + StructuredPostal.TYPE + "=?" + " AND "
                    + StructuredPostal.STREET + "=?" + " AND "
                    + StructuredPostal.NEIGHBORHOOD + "=?" + " AND "
                    + StructuredPostal.CITY + "=?" + " AND "
                    + StructuredPostal.COUNTRY + "=?" + " AND "
                    + StructuredPostal.POSTCODE + "=?";

            String[] params = new String[]{strID,
                    StructuredPostal.CONTENT_ITEM_TYPE,
                    String.valueOf(addressTypeIndex),
                    addressDto.getStreetStr(), addressDto.getNeigborhoodStr(),
                    addressDto.getCityStr(), addressDto.getCountryStr(),
                    addressDto.getZipCodeStr()};

            address_oplist.add(ContentProviderOperation
                    .newDelete(Data.CONTENT_URI).withSelection(where, params)
                    .build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, address_oplist);
        }

        // =============================================================
        // Date
        ArrayList<ContentProviderOperation> date_oplist = new ArrayList<ContentProviderOperation>();
        for (DateDto dateDto : contactDto.getDateEvent()) {

            int dateTypeIndex = 0;

            if (dateDto.getDatesType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_aniv))) {
                dateTypeIndex = Event.TYPE_ANNIVERSARY;
            } else if (dateDto.getDatesType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                dateTypeIndex = Event.TYPE_OTHER;
            }

            String whereDate = Data.RAW_CONTACT_ID + "=?" + " AND "
                    + Data.MIMETYPE + "=?" + " AND " + Event.TYPE + "=?"
                    + " AND " + Event.START_DATE + "=?";
            String[] paramsDate = new String[]{strID,
                    Event.CONTENT_ITEM_TYPE, String.valueOf(dateTypeIndex),
                    dateDto.getDates()};

            date_oplist.add(ContentProviderOperation
                    .newDelete(Data.CONTENT_URI)
                    .withSelection(whereDate, paramsDate).build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, date_oplist);
        }

        // =============================================================
        // RELATION
        ArrayList<ContentProviderOperation> rela_oplist = new ArrayList<ContentProviderOperation>();
        for (RelationDto relation : oldContactDto.getRelationDto()) {

            int imTypeIndex = 0;

            Resources res = editActivity.getResources();

            if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation0))) {
                imTypeIndex = Relation.TYPE_ASSISTANT;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation1))) {
                imTypeIndex = Relation.TYPE_BROTHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation2))) {
                imTypeIndex = Relation.TYPE_CHILD;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation3))) {
                imTypeIndex = Relation.TYPE_DOMESTIC_PARTNER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation4))) {
                imTypeIndex = Relation.TYPE_FATHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation5))) {
                imTypeIndex = Relation.TYPE_FRIEND;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation6))) {
                imTypeIndex = Relation.TYPE_MANAGER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation7))) {
                imTypeIndex = Relation.TYPE_MOTHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation8))) {
                imTypeIndex = Relation.TYPE_PARENT;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation9))) {
                imTypeIndex = Relation.TYPE_PARTNER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation10))) {
                imTypeIndex = Relation.TYPE_REFERRED_BY;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation11))) {
                imTypeIndex = Relation.TYPE_RELATIVE;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation12))) {
                imTypeIndex = Relation.TYPE_SISTER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation12))) {
                imTypeIndex = Relation.TYPE_SPOUSE;
            }

            String whereIM = Data.RAW_CONTACT_ID + "=?" + " AND "
                    + Data.MIMETYPE + "=?" + " AND " + Relation.TYPE + "=?"
                    + " AND " + Relation.NAME + "=?";
            String[] paramsIM = new String[]{strID,
                    Relation.CONTENT_ITEM_TYPE, String.valueOf(imTypeIndex),
                    relation.getRelationName()};

            rela_oplist.add(ContentProviderOperation
                    .newDelete(Data.CONTENT_URI)
                    .withSelection(whereIM, paramsIM).build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, rela_oplist);
        }

        // =============================================================
        // IM
        ArrayList<ContentProviderOperation> im_oplist = new ArrayList<ContentProviderOperation>();
        for (IMDto imDto : oldContactDto.getImField()) {

            int imTypeIndex = 0;

            if (imDto.getImProtocol().equalsIgnoreCase("AIM")) {
                imTypeIndex = Im.PROTOCOL_AIM;
            } else if (imDto.getImProtocol().equalsIgnoreCase("MSN")) {
                imTypeIndex = Im.PROTOCOL_MSN;
            } else if (imDto.getImProtocol().equalsIgnoreCase("YAHOO")) {
                imTypeIndex = Im.PROTOCOL_YAHOO;
            } else if (imDto.getImProtocol().equalsIgnoreCase("QQ")) {
                imTypeIndex = Im.PROTOCOL_SKYPE;
            } else if (imDto.getImProtocol().equalsIgnoreCase("GOOGLE TALK")) {
                imTypeIndex = Im.PROTOCOL_GOOGLE_TALK;
            } else if (imDto.getImProtocol().equalsIgnoreCase("ICQ")) {
                imTypeIndex = Im.PROTOCOL_ICQ;
            } else if (imDto.getImProtocol().equalsIgnoreCase("JABBER")) {
                imTypeIndex = Im.PROTOCOL_JABBER;
            } else if (imDto.getImProtocol().equalsIgnoreCase("NETMEETING")) {
                imTypeIndex = Im.PROTOCOL_NETMEETING;
            }

            String whereIM = Data.RAW_CONTACT_ID + "=?" + " AND "
                    + Data.MIMETYPE + "=?" + " AND " + Im.PROTOCOL + "=?"
                    + " AND " + Im.DATA + "=?";
            String[] paramsIM = new String[]{strID, Im.CONTENT_ITEM_TYPE,
                    String.valueOf(imTypeIndex), imDto.getIm()};

            im_oplist.add(ContentProviderOperation.newDelete(Data.CONTENT_URI)
                    .withSelection(whereIM, paramsIM).build());
            editActivity.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, im_oplist);
        }

    }

    public void insertArrayInfo(ContactDto contactDto) throws RemoteException,
            OperationApplicationException {

        // String strID = contactDto.getContactID();
        // int intID = Integer.valueOf(contactDto.getContactID());

        String strID = contactDto.getContactID();
        // int intID = Integer.valueOf(contactDto.getContactID());
        int intID = 0;
        ContentResolver cr = editActivity.getContentResolver();
        Cursor cur = cr.query(RawContacts.CONTENT_URI, null,
                RawContacts.CONTACT_ID + "=?",
                new String[]{contactDto.getContactID()}, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                int id = cur.getInt(cur.getColumnIndex(RawContacts._ID));
                System.out.println("BERTAX RAW CONTACT ID " + id);
                intID = id;
            }

        }

        // ============================================================
        // PHONE_NUMBER
        ContentValues phoneContentValues = new ContentValues();
        for (PhoneNumberDto num : contactDto.getPhoneNumbers()) {

            phoneContentValues.put(Data.RAW_CONTACT_ID, intID);
            phoneContentValues.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
            phoneContentValues.put(Phone.NUMBER, num.getNumber());

            int phoneTypeIndex = 0;
            if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_mobile))) {
                phoneTypeIndex = Phone.TYPE_MOBILE;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                phoneTypeIndex = Phone.TYPE_WORK;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                phoneTypeIndex = Phone.TYPE_HOME;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_main))) {
                phoneTypeIndex = Phone.TYPE_MAIN;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_workfax))) {
                phoneTypeIndex = Phone.TYPE_FAX_WORK;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_homefax))) {
                phoneTypeIndex = Phone.TYPE_FAX_HOME;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_pager))) {
                phoneTypeIndex = Phone.TYPE_PAGER;
            } else if (num.getNumberType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                phoneTypeIndex = Phone.TYPE_OTHER;
            } else {
                phoneTypeIndex = Phone.TYPE_CUSTOM;
                phoneContentValues.put(Phone.LABEL, num.getNumberType());
            }

            phoneContentValues.put(Phone.TYPE, phoneTypeIndex);
            editActivity.getContentResolver().insert(Data.CONTENT_URI,
                    phoneContentValues);

        }

        // ============================================================
        // EMAIL
        ContentValues emailContentValues = new ContentValues();
        for (EmailDto emailDto : contactDto.getEmails()) {

            emailContentValues.put(Data.RAW_CONTACT_ID, intID);
            emailContentValues.put(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
            emailContentValues.put(Email.DATA, emailDto.getEmail());

            int emailTypeIndex = 0;

            if (emailDto.getEmailType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                emailTypeIndex = Email.TYPE_WORK;
            } else if (emailDto.getEmailType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                emailTypeIndex = Email.TYPE_HOME;
            } else if (emailDto.getEmailType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                emailTypeIndex = Email.TYPE_OTHER;
            } else {
                emailTypeIndex = Email.TYPE_CUSTOM;
                emailContentValues.put(Email.LABEL, emailDto.getEmailType());
            }

            emailContentValues.put(Email.TYPE, emailTypeIndex);

            editActivity.getContentResolver().insert(Data.CONTENT_URI,
                    emailContentValues);

        }

        // ============================================================
        // URL
        ContentValues webContentValues = new ContentValues();
        for (WebsiteDto websites : contactDto.getWebsites()) {

            webContentValues.clear();
            webContentValues.put(Data.RAW_CONTACT_ID, intID);
            webContentValues.put(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE);
            webContentValues.put(Website.URL, websites.getWebsite());
            int websiteTypeIndex = 0;

            if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_homepage))) {
                websiteTypeIndex = Website.TYPE_HOMEPAGE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_blog))) {
                websiteTypeIndex = Website.TYPE_BLOG;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_profile))) {
                websiteTypeIndex = Website.TYPE_PROFILE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                websiteTypeIndex = Website.TYPE_HOME;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                websiteTypeIndex = Website.TYPE_HOMEPAGE;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_ftp))) {
                websiteTypeIndex = Website.TYPE_FTP;
            } else if (websites.getWebsiteType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                websiteTypeIndex = Website.TYPE_WORK;
            }

            webContentValues.put(Website.TYPE, websiteTypeIndex);
            editActivity.getContentResolver().insert(Data.CONTENT_URI,
                    webContentValues);
        }

        // ============================================================
        // ADDRESS
        ContentValues addressContentValues = new ContentValues();
        for (AddressDto addressDto : contactDto.getAddresses()) {
            addressContentValues.put(Data.RAW_CONTACT_ID, intID);
            addressContentValues.put(Data.MIMETYPE,
                    StructuredPostal.CONTENT_ITEM_TYPE);

            int addressTypeIndex = 0;

            if (addressDto.getAddressType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_work))) {
                addressTypeIndex = StructuredPostal.TYPE_WORK;
            } else if (addressDto.getAddressType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_home))) {
                addressTypeIndex = StructuredPostal.TYPE_HOME;
            } else if (addressDto.getAddressType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                addressTypeIndex = StructuredPostal.TYPE_OTHER;
            }

            if (addressDto.getStreetStr().equals("")
                    || addressDto.getStreetStr() == null) {
                addressDto.setStreetStr(" ");
            }

            if (addressDto.getNeigborhoodStr().equals("")
                    || addressDto.getStreetStr() == null) {
                addressDto.setNeigborhoodStr(" ");
            }
            if (addressDto.getCityStr().equals("")
                    || addressDto.getStreetStr() == null) {
                addressDto.setCityStr(" ");
            }

            if (addressDto.getZipCodeStr().equals("")
                    || addressDto.getStreetStr() == null) {
                addressDto.setZipCodeStr(" ");
            }

            addressContentValues.put(StructuredPostal.TYPE, addressTypeIndex);
            addressContentValues.put(StructuredPostal.STREET,
                    addressDto.getStreetStr());
            addressContentValues.put(StructuredPostal.NEIGHBORHOOD,
                    addressDto.getNeigborhoodStr());
            addressContentValues.put(StructuredPostal.CITY,
                    addressDto.getCityStr());
            addressContentValues.put(StructuredPostal.COUNTRY,
                    addressDto.getCountryStr());
            addressContentValues.put(StructuredPostal.POSTCODE,
                    addressDto.getZipCodeStr());
            editActivity.getContentResolver().insert(Data.CONTENT_URI,
                    addressContentValues);

        }

        // ============================================================
        // DATES
        ContentValues eventContentValues = new ContentValues();
        for (DateDto dateDto : contactDto.getDateEvent()) {

            eventContentValues.clear();
            eventContentValues.put(Data.RAW_CONTACT_ID, intID);
            eventContentValues.put(Data.MIMETYPE, Event.CONTENT_ITEM_TYPE);
            eventContentValues.put(Event.DATA, dateDto.getDates());

            int dateTypeIndex = 0;

            if (dateDto.getDatesType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_aniv))) {
                dateTypeIndex = Event.TYPE_ANNIVERSARY;
            } else if (dateDto.getDatesType().equalsIgnoreCase(
                    editActivity.getResources().getString(
                            R.string.string_array_other))) {
                dateTypeIndex = Event.TYPE_OTHER;
            }

            // else if (dateDto.getDatesType().equalsIgnoreCase(
            // editActivity.getResources().getString(
            // R.string.string_array_custom))) {
            // dateTypeIndex = Event.TYPE_CUSTOM;
            // }

            eventContentValues.put(Event.TYPE, dateTypeIndex);
            editActivity.getContentResolver().insert(Data.CONTENT_URI,
                    eventContentValues);
        }

        // ============================================================
        // RELATION
        ContentValues relationContentValues = new ContentValues();
        for (RelationDto relation : contactDto.getRelationDto()) {

            // phoneContentValues.clear();
            relationContentValues.put(Data.RAW_CONTACT_ID, intID);
            relationContentValues
                    .put(Data.MIMETYPE, Relation.CONTENT_ITEM_TYPE);
            relationContentValues
                    .put(Relation.NAME, relation.getRelationName());

            int imTypeIndex = 0;

            Resources res = editActivity.getResources();

            if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation0))) {
                imTypeIndex = Relation.TYPE_ASSISTANT;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation1))) {
                imTypeIndex = Relation.TYPE_BROTHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation2))) {
                imTypeIndex = Relation.TYPE_CHILD;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation3))) {
                imTypeIndex = Relation.TYPE_DOMESTIC_PARTNER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation4))) {
                imTypeIndex = Relation.TYPE_FATHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation5))) {
                imTypeIndex = Relation.TYPE_FRIEND;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation6))) {
                imTypeIndex = Relation.TYPE_MANAGER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation7))) {
                imTypeIndex = Relation.TYPE_MOTHER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation8))) {
                imTypeIndex = Relation.TYPE_PARENT;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation9))) {
                imTypeIndex = Relation.TYPE_PARTNER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation10))) {
                imTypeIndex = Relation.TYPE_REFERRED_BY;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation11))) {
                imTypeIndex = Relation.TYPE_RELATIVE;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation12))) {
                imTypeIndex = Relation.TYPE_SISTER;
            } else if (relation.getRelationType().equalsIgnoreCase(
                    res.getString(R.string.string_relation12))) {
                imTypeIndex = Relation.TYPE_SPOUSE;
            } else {
                imTypeIndex = Relation.TYPE_CUSTOM;
                emailContentValues.put(Relation.LABEL,
                        relation.getRelationType());
            }

            relationContentValues.put(Relation.TYPE, imTypeIndex);
            editActivity.getContentResolver().insert(Data.CONTENT_URI,
                    relationContentValues);

        }

        // ============================================================
        // IM
        ContentValues imContentValues = new ContentValues();
        for (IMDto imDto : contactDto.getImField()) {

            // phoneContentValues.clear();
            imContentValues.put(Data.RAW_CONTACT_ID, intID);
            imContentValues.put(Data.MIMETYPE, Im.CONTENT_ITEM_TYPE);
            imContentValues.put(Im.DATA, imDto.getIm());

            int imTypeIndex = 0;

            if (imDto.getImProtocol().equalsIgnoreCase("AIM")) {
                imTypeIndex = Im.PROTOCOL_AIM;
            } else if (imDto.getImProtocol().equalsIgnoreCase("MSN")) {
                imTypeIndex = Im.PROTOCOL_MSN;
            } else if (imDto.getImProtocol().equalsIgnoreCase("YAHOO")) {
                imTypeIndex = Im.PROTOCOL_YAHOO;
            } else if (imDto.getImProtocol().equalsIgnoreCase("QQ")) {
                imTypeIndex = Im.PROTOCOL_SKYPE;
            } else if (imDto.getImProtocol().equalsIgnoreCase("GOOGLE TALK")) {
                imTypeIndex = Im.PROTOCOL_GOOGLE_TALK;
            } else if (imDto.getImProtocol().equalsIgnoreCase("ICQ")) {
                imTypeIndex = Im.PROTOCOL_ICQ;
            } else if (imDto.getImProtocol().equalsIgnoreCase("JABBER")) {
                imTypeIndex = Im.PROTOCOL_JABBER;
            } else if (imDto.getImProtocol().equalsIgnoreCase("NETMEETING")) {
                imTypeIndex = Im.PROTOCOL_NETMEETING;
            }

            imContentValues.put(Im.PROTOCOL, imTypeIndex);
            editActivity.getContentResolver().insert(Data.CONTENT_URI,
                    imContentValues);

        }

    }

}
