package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.entities.WebsiteDto;

public class WebsiteAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<WebsiteDto> website;

    public WebsiteAdapter(Activity context, ArrayList<WebsiteDto> website) {

        super();
        this.context = context;

        this.website = new ArrayList<WebsiteDto>();
        this.website.addAll(website);
    }

    @Override
    public int getCount() {
        return website.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.website_listitem, viewGroup,
                false);
        TextView websiteType = (TextView) rowView
                .findViewById(R.id.detail_website_type);
        TextView websiteDetails = (TextView) rowView
                .findViewById(R.id.detail_website);

        websiteType.setTypeface(NewUtil.getFontRoman(context));
        websiteDetails.setTypeface(NewUtil.getFontRoman(context));

        websiteType.setTextSize(NewUtil.gettxtSize());
        websiteDetails.setTextSize(NewUtil.gettxtSize());


        if (website.get(i) != null) {
            WebsiteDto websiteDto = website.get(i);
            websiteType.setText(websiteDto.getWebsiteType());
            websiteDetails.setText(websiteDto.getWebsite());
        }

        return rowView;
    }

}
