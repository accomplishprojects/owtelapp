package com.katadigital.owtel.modules.main.contacts.entities;

import java.util.Date;

public class CallLogDto {

    String contactName;
    String number;
    Date callDate;
    String callLogID;
    String callType;
    String numberType;
    String duration;
    int count;
    boolean duplicate;

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getCallDate() {
        return callDate;
    }

    public void setCallDate(Date callDate) {
        this.callDate = callDate;
    }

    public String getCallLogID() {
        return callLogID;
    }

    public void setCallLogID(String callLogID) {
        this.callLogID = callLogID;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isDuplicate() {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }

    public static Integer compareUnixTime(CallLogDto callHistory1, CallLogDto callHistory2) {
        return Long.valueOf(callHistory2.getCallDate().getTime() / 1000)
                .compareTo(callHistory1.getCallDate().getTime() / 1000);
    }
}
