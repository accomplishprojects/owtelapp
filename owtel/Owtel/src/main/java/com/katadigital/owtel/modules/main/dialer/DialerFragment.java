package com.katadigital.owtel.modules.main.dialer;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.AddNumberToContact;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.contacts.CustomAutoCompleteTextView;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.adapters.AutoCompleteTextAdapter;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.modules.main.contacts.ContactsFragment;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.portsip.utilities.androidcomponent.service.SipSettings;
import com.katadigital.portsip.utilities.logs.CallLogsManager;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;
import com.katadigital.portsip.views.PortSipCallScreenActivity;
import com.katadigital.ui.recyclerview.DialerRecyclerViewAdapter;

//import org.sipdroid.sipua.ui.Receiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Omar Matthew Reyes on 2/18/16.
 * Fragment for Dialer and initiate SIP calls
 */
public class DialerFragment extends Fragment implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    View rootView;
    private ImageButton mButtonCall;
    // public EditText keyPadTextView;
    int count = 0;
    boolean clearIsPressed = false;
    Dialog selectModelDialog;

    public static CustomAutoCompleteTextView keyPadTextView;
    public static ImageButton mButtonClear, mButtonContactAdd;
    public static TextView mTextViewSIPConnectionStatus;

    FrameLayout cover;
    public static boolean fromKeypad = false;
    List<HashMap<String, Object>> cListkypad = new ArrayList<>();
    RecyclerView recyclerViewDialer;

    private String strSipUsername;

    @Override
    public void onResume() {
        super.onResume();
        /**Hiding portsip notification if shown*/
        NotificationHelper.getNotificationHelperInstance().unnotifyPortSipCallStatus();

        try {
            ContactDetailActivity.searchValue = "";
            ContactsFragment.searchContacts.setText("");

            if (DialerFragment.fromKeypad) {
                ((MainActivityBridge) getActivity()).switchTab(2);
            }
        } catch (Exception e) {
            Log.e(TAG, "onResume " + e);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).currentTab = 3;
        ((MainActivity) getActivity()).currentFragment = this;

        rootView = inflater.inflate(R.layout.fragment_dialer, container, false);

        /**Initialize notification helper class*/
        NotificationHelper.init(getActivity());

        mButtonClear = (ImageButton) rootView.findViewById(R.id.clear_image_view);
        mTextViewSIPConnectionStatus = (TextView) rootView.findViewById(R.id.tv_connection_status);
        // FIXME: 12/16/16 set status for sip
//        mTextViewSIPConnectionStatus.setText(Receiver.stringConnectionStatus);
        cover = (FrameLayout) rootView.findViewById(R.id.cover);

        cover.setOnClickListener(null);

        keyPadTextView = (CustomAutoCompleteTextView) rootView.findViewById(R.id.enteredDigits);
        NewUtil.disableSoftInputFromAppearing(keyPadTextView);

        TextView mTextViewDialerUserNumber = (TextView) rootView.findViewById(R.id.tv_dialer_number_user);
        String dialerUserNumber = "";
        try {
            strSipUsername = StringFormatter.formatUsNumber(new SipSettings().getSipUsername());
            dialerUserNumber = getString(R.string.dialer_number_user) + strSipUsername;
        } catch (NullPointerException e) {
            Log.e(TAG, "User Number " + e);
        }
        mTextViewDialerUserNumber.setText(dialerUserNumber);

//        if (new PermissionChecker(getActivity())
//                .getContactsPermission(PermissionChecker.REQUEST_PERMISSION_CONTACTS_DIALER)) {
//            new AutoComKeyPadAsync().execute();
//        }

        final float size1 = 34, size2 = 25, size3 = 20;


        keyPadTextView.addTextChangedListener(new TextWatcher() {
            private boolean mFormatting; // this is a flag which prevents the  stack overflow.
            private int mAfter;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here..
                if (s.length() > 16) {
                    keyPadTextView.setTextSize(size3);
                } else if (s.length() > 12) {
                    keyPadTextView.setTextSize(size2);
                } else {
                    keyPadTextView.setTextSize(size1);
                }
            }

            //called before the text is changed...
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //nothing to do here...
                mAfter = after; // flag to detect backspace..

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Make sure to ignore calls to afterTextChanged caused by the work done below
//                boolean append=false;
                if (!mFormatting) {
                    mFormatting = true;
                    // using US formatting...
                    if (mAfter != 0) // in case back space ain't clicked...{
                    {
//                        if(s.toString().contains("+")){
//                            append =true;
//                        }
                        PhoneNumberUtils.formatNumber(s, PhoneNumberUtils.getFormatTypeForLocale(Locale.getDefault()));
//                        if(append)
//                        {
//                            s.insert(0,"+");
//                            append =false;
//                        }
                    }
                    mFormatting = false;
                }
            }
        });
        keyPadTextView.setText("");
        keyPadTextView.setThreshold(1);
        keyPadTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    keyPadTextView.setFocusable(true);
                    keyPadTextView.setSelection(keyPadTextView.getText().length());
                    keyPadTextView.setCursorVisible(false);
                    keyPadTextView.requestFocus();

                    keyPadTextView.setVisibility(View.VISIBLE);
                    mButtonClear.setVisibility(View.VISIBLE);
                    mButtonContactAdd.setVisibility(View.VISIBLE);
                }
                // else{
                // keyPadTextView.setFocusable(false);
                // keyPadTextView.setCursorVisible(false);
                // keyPadTextView.setKeyListener(null);
                // }
            }
        });

        keyPadTextView.setOnTouchListener((v, event) -> {
            int inType = keyPadTextView.getInputType(); // backup the input
            // type
            keyPadTextView.setInputType(InputType.TYPE_NULL); // disable
            // soft
            // input
            keyPadTextView.onTouchEvent(event); // call native handler
            keyPadTextView.setInputType(inType); // restore input type

            return true; // consume touch even
        });

        keyPadTextView.setOnClickListener(v -> {
            keyPadTextView.setFocusable(true);
            keyPadTextView.setSelection(keyPadTextView.getText().length());
            keyPadTextView.setCursorVisible(false);
            keyPadTextView.requestFocus();
        });

        mButtonContactAdd = (ImageButton) rootView.findViewById(R.id.keypad_add_contact);
        mButtonContactAdd.setOnClickListener(view -> openDialog());

        mButtonClear.setOnLongClickListener(view -> {
            mAutoDecrement = true;
            repeatUpdateHandler.post(new RptUpdater());
            return false;
        });
        mButtonClear.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                String textInBox = keyPadTextView.getText().toString();
                if (textInBox.length() > 0) {
                    mButtonClear.setPressed(true);
                    String newText = textInBox.substring(0,
                            textInBox.length() - 1);
                    keyPadTextView.setText(newText);
                }
                return false;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                mButtonClear.setPressed(false);
                mAutoDecrement = false;
                if (keyPadTextView.length() <= 0) {
                    keyPadTextView.setVisibility(View.VISIBLE);
                    mButtonClear.setVisibility(View.INVISIBLE);
                    mButtonContactAdd.setVisibility(View.INVISIBLE);

                    // keyPadTextView.setFocusable(false);
                    keyPadTextView.setCursorVisible(false);

                }
                return false;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                // mButtonClear.setPressed(false);
                mAutoDecrement = false;
                return false;
            } else {
                return false;
            }
        });
        mButtonCall = (ImageButton) rootView.findViewById(R.id.btn_dialer_call);
        mButtonCall.setOnClickListener(this);

        keyPadTextView.setVisibility(View.VISIBLE);
        mButtonClear.setVisibility(View.INVISIBLE);
        mButtonContactAdd.setVisibility(View.INVISIBLE);

        recyclerViewDialer = (RecyclerView) rootView.findViewById(R.id.rv_dialer);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerViewDialer.setLayoutManager(gridLayoutManager);
        recyclerViewDialer.setHasFixedSize(true);
        DialerRecyclerViewAdapter mAdapter = new DialerRecyclerViewAdapter(getActivity(), keyPadTextView);
        recyclerViewDialer.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        keyPadTextView.setVisibility(View.VISIBLE);
        mButtonClear.setVisibility(View.VISIBLE);
        mButtonContactAdd.setVisibility(View.VISIBLE);
        switch (view.getId()) {
            case R.id.btn_dialer_call:
                String strNumberToCall = keyPadTextView.getText().toString().replaceAll("-", "");

                if (!strNumberToCall.equals(new SipSettings().getSipUsername())) {
                    if (!strNumberToCall.isEmpty() || strNumberToCall.length() != 0) {
                        new ApiHelper(getActivity()).doCallCheckingThenCall(getActivity(), strNumberToCall);
//                        startActivity(new Intent(getActivity(), PortSipCallScreenActivity.class)
//                                .putExtra("NumberToCall", strNumberToCall)
//                                .putExtra("isIncomingCall", false));
                    } else {
                        Toast.makeText(getActivity(), "Please enter a mobile number", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Invalid mobile number", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        keyPadTextView.setText("");
    }

    private Handler repeatUpdateHandler = new Handler();

    private boolean mAutoDecrement = false;
    // speed of deletion
    int REP_DELAY = 30;
    int mValue = 0;

    class RptUpdater implements Runnable {
        public void run() {
            if (mAutoDecrement) {
                decrement();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            }
            // else if( mAutoDecrement ){
            // decrement();
            // repeatUpdateHandler.postDelayed( new RptUpdater(), REP_DELAY );
            // }
        }
    }

    public void decrement() {
        // mValue--;
        // keyPadTextView.setText( ""+mValue );

        String textInBox = keyPadTextView.getText().toString();
        if (textInBox.length() > 0) {
            String newText = textInBox.substring(0, textInBox.length() - 1);
            keyPadTextView.setText(newText);
        } else {
            mAutoDecrement = false;
        }

        if (keyPadTextView.getText().toString().isEmpty()) {
            keyPadTextView.setVisibility(View.VISIBLE);
            mButtonClear.setVisibility(View.INVISIBLE);
            mButtonContactAdd.setVisibility(View.INVISIBLE);
        }
    }

    public void openDialog() {
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View customView = inflater.inflate(R.layout.keypad_addcontact_dialog, null);

        TextView textBtn1, textBtn2, textBtn3;
        textBtn1 = (TextView) customView.findViewById(R.id.keypad_dialogbtn1);
        textBtn2 = (TextView) customView.findViewById(R.id.keypad_dialogbtn2);
        textBtn3 = (TextView) customView.findViewById(R.id.keypad_dialogbtn3);

        textBtn1.setTypeface(NewUtil.getFontRoman(getActivity()));
        textBtn2.setTypeface(NewUtil.getFontRoman(getActivity()));
        textBtn3.setTypeface(NewUtil.getFontRoman(getActivity()));

        textBtn1.setTextSize(NewUtil.gettxtSize());
        textBtn2.setTextSize(NewUtil.gettxtSize());
        textBtn3.setTextSize(NewUtil.gettxtSize());

        textBtn1.setOnClickListener(v -> {
            String text = String.valueOf(keyPadTextView.getText());
            NewUtil.createContactChooseDiag2(getActivity(), text);
            selectModelDialog.dismiss();
        });

        textBtn2.setOnClickListener(v -> {
            String text = String.valueOf(keyPadTextView.getText());
            Intent intent = new Intent(getActivity(),
                    AddNumberToContact.class);
            selectModelDialog.dismiss();
            intent.putExtra("number_to_add", text);
            startActivity(intent);

            getActivity().overridePendingTransition(R.anim.slide_in_up,
                    R.anim.slide_in_up_exit);

        });

        textBtn3.setOnClickListener(v -> selectModelDialog.dismiss());

        // Build the dialog
        selectModelDialog = new Dialog(getActivity(), R.style.DialogSlideAnim);
        selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectModelDialog.setContentView(customView);
        selectModelDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                .getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        selectModelDialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        selectModelDialog.setTitle("Actions");

        selectModelDialog.show();
    }

    @Override
    public void onStart() {
        System.out.println(keyPadTextView.getText());
        super.onStart();
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public void keypadList() {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

//        Cursor cur = PermissionChecker.getCallLogs(getActivity(), ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cur != null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                    + " = ?", new String[]{id}, null);
//                    Cursor pCur = PermissionChecker.getCallLogs(getActivity(), ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
//                                    + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        HashMap<String, Object> con = new HashMap<>();
                        String phoneNo = pCur
                                .getString(pCur
                                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        // try {
                        // new loadingTask(con, id, name, phoneNo)
                        // .executeOnExecutor(
                        // AsyncTask.THREAD_POOL_EXECUTOR,
                        // null);
                        //
                        // } catch (Exception e) {
                        // }

                        con.put("name", name);
                        con.put("phoneNumber", phoneNo.replaceAll(" ", ""));
                        // con.put("id", mId);
                        cListkypad.add(con);

                        String replaceNum = phoneNo.replaceAll(" ", "");

                        try {
                            if (replaceNum.substring(0, 2).equals("09")) {
                                con = new HashMap<>();
                                con.put("name", name);
                                con.put("phoneNumber", "+639"
                                        + replaceNum.replaceAll(" ", "")
                                        .substring(2));

                                cListkypad.add(con);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "keypadList " + e);
                        }

                    }
                    pCur.close();
                }
            }
            cur.close();
        }
    }

    class AutoComKeyPadAsync extends AsyncTask<Object, Object, Object> {
        AutoCompleteTextAdapter adapter;

        @Override
        protected Object doInBackground(Object... params) {
            cListkypad.clear();
            keypadList();

            String[] from = {"name", "phoneNumber"};
            int[] to = {R.id.name, R.id.number};

            try {
                System.out.println("JM list " + cListkypad);
                adapter = new AutoCompleteTextAdapter(getActivity(),
                        cListkypad, R.layout.keypad_autocomplete_contact, from,
                        to);

            } catch (Exception e) {
                Log.e(TAG, "AutoComKeyPadAsync " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            keyPadTextView.setAdapter(adapter);
            super.onPostExecute(result);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i(TAG, "Permission request code: " + requestCode);
        for (String permission : permissions) Log.i(TAG, "Permission request: " + permission);
        for (int result : grantResults) Log.i(TAG, "Permission request result: " + result);
//        if (requestCode == PermissionChecker.REQUEST_PERMISSION_CONTACTS_DIALER)
//            new AutoComKeyPadAsync().execute();
    }
}