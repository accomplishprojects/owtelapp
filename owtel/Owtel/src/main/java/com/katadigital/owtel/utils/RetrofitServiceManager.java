package com.katadigital.owtel.utils;

import com.katadigital.owtel.api.ApiService;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;


/**
 * Created by Omar Matthew Reyes on 7/18/16.
 */
public class RetrofitServiceManager {

    public static Retrofit retrofit;
    public static RetrofitServiceManager mServiceManager;


    public static void init(){
        if(mServiceManager == null){
            mServiceManager = new RetrofitServiceManager();
        }
    }

    public static RetrofitServiceManager getRetrofitServiceInstance(){
        if(mServiceManager == null){
            throw new IllegalStateException("Call RetrofitServiceManager.init()");
        }
        return mServiceManager;
    }

    public ApiService getApiService(){
        retrofit = new Retrofit.Builder()
                .baseUrl(API.URL_TEST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return retrofit.create(ApiService.class);
    }
}
