package com.katadigital.owtel.modules.main.contacts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AlphabetIndexer;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Item;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Row;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Section;
import com.katadigital.owtel.modules.main.contacts.adapters.TestSectionedAdapter;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.ui.pinnedheader.PinnedHeaderListView;

public class AddNumberToContact extends AppCompatActivity implements
        LoaderCallbacks<Cursor> {

    SimpleCursorAdapter mAdapter = null;
    private String mCurrentFilter = null;
    ArrayList<String> contactsArray;
    PinnedHeaderListView contact_listview;
    EditText searchContacts;

    private static final String[] CONTACTS_SUMMARY_PROJECTION = new String[]{
            Contacts._ID, Contacts.DISPLAY_NAME, Contacts.LOOKUP_KEY};
    private String[] PHOTO_BITMAP_PROJECTION = new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO};
    QuickContactHelper quickContactHelper;
    private ProgressDialog progressDialog;
    String searchedName = "";
    String number = "";
    TextView addToNumberCaption;

    // ALPHABET LISTVIEW VARIABLES
    private AlphabetListAdapter adapter;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private int addNumberSideIndexHeight;
    private static float addnumber_sideIndexX;
    private static float addnumber_sideIndexY;
    private int indexListSize;
    ArrayList<Integer> contactDtoIDList = new ArrayList<Integer>();

    String[] let = {"#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
            "X", "Y", "Z",};

    TextView txt_center_dummy, cancel_search;
    RelativeLayout header, sl;
    InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_number_to_contact);
        contactsArray = new ArrayList<>();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            number = extras.getString("number_to_add");
        }
        invalidateOptionsMenu();

        quickContactHelper = new QuickContactHelper(this);
        getLoaderManager().initLoader(0, null, AddNumberToContact.this);

        mAdapter = new IndexedListAdapter(this, R.layout.list_item_contacts,
                null, new String[]{ContactsContract.Contacts.DISPLAY_NAME,
                Contacts._ID}, new int[]{R.id.display_name});

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        header = (RelativeLayout) findViewById(R.id.contacts_actionbar);
        cancel_search = (TextView) findViewById(R.id.cancel);
        sl = (RelativeLayout) findViewById(R.id.layout_search);
        txt_center_dummy = (TextView) findViewById(R.id.txt_search_contacts);
        Keyboard();

        initComponents();

        View back = findViewById(R.id.contacts_addbtn_wrapper);
        TextView cancel_text = (TextView) findViewById(R.id.txt_cancel_num_contct);
        cancel_text.setText(getResources().getString(R.string.cancel_string));
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        addToNumberCaption = (TextView) findViewById(R.id.add_to_number_caption);
        String addToNumber = getResources().getString(R.string.add_num_to_contacts) + " " + number;
        addToNumberCaption.setText(addToNumber);
        searchContacts.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                AddNumberToContact.this.mAdapter.getFilter().filter(cs);
                searchedName = cs + "";
                System.out.println(cs + " <--- CS");

                mCurrentFilter = (!TextUtils.isEmpty(searchedName) ? searchedName : null);
                getLoaderManager().restartLoader(0, null,
                        AddNumberToContact.this);
            }

            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        contact_listview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView,
                                    int myItemInt, long mylng) {
                if (contactDtoIDList.get(myItemInt) > 0) {
                    Integer Id = contactDtoIDList.get(myItemInt);
                    String accountType = QuickContactHelper.getAccountType(AddNumberToContact.this, String.valueOf(Id));

                    System.out.println("JM ACCOUNTTYPE " + accountType);

                    if (accountType.equals("SIM Account")
                            || accountType.equals("Local Phone Account")
                            || accountType.equals("com.google")) {

                        new loadContactDetails(contactDtoIDList.get(myItemInt))
                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Not editable from this app",
                                Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        txt_center_dummy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                searchContacts.setVisibility(View.VISIBLE);
                txt_center_dummy.setVisibility(View.GONE);
                searchContacts.requestFocus();

                imm.showSoftInput(searchContacts,
                        InputMethodManager.SHOW_IMPLICIT);
            }
        });

        cancel_search.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(searchContacts.getWindowToken(), 0);
                searchContacts.setText("");
                Keyboard();
            }
        });

        sl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                searchContacts.setVisibility(View.VISIBLE);
                txt_center_dummy.setVisibility(View.GONE);
                searchContacts.requestFocus();

                imm.showSoftInput(searchContacts,
                        InputMethodManager.SHOW_IMPLICIT);

            }
        });
    }

    private void Keyboard() {
        final View activityRootView = findViewById(R.id.rootView);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(
                new OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        Rect r = new Rect();
                        activityRootView.getWindowVisibleDisplayFrame(r);
                        int heightDiff = activityRootView.getRootView()
                                .getHeight() - (r.bottom - r.top);

                        if (heightDiff > 100) {
                            // System.out.println("Keyboard is shown);
                            // ab.hide();
                            header.setVisibility(View.GONE);
                            cancel_search.setVisibility(View.VISIBLE);
                            searchContacts.setVisibility(View.VISIBLE);
                            txt_center_dummy.setVisibility(View.GONE);
                            searchContacts.requestFocus();
                        } else {
                            // System.out.println("Keyboard is hidden");
                            // ab.show();
                            header.setVisibility(View.VISIBLE);
                            cancel_search.setVisibility(View.GONE);
                            searchContacts.setVisibility(View.GONE);
                            txt_center_dummy.setVisibility(View.VISIBLE);
                        }
                    }
                });

    }

    class IndexedListAdapter extends SimpleCursorAdapter implements
            SectionIndexer {

        AlphabetIndexer alphaIndexer;

        public IndexedListAdapter(Context context, int layout, Cursor c,
                                  String[] from, int[] to) {
            super(context, layout, c, from, to, 0);
        }

        @Override
        public Cursor swapCursor(Cursor c) {

            return super.swapCursor(c);
        }

        @Override
        public int getPositionForSection(int section) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public Object[] getSections() {
            return null;
        }
    }

    public void initComponents() {
        searchContacts = (EditText) findViewById(R.id.addnumber_searchbar);
        contact_listview = (PinnedHeaderListView) findViewById(R.id.add_numberlist);
    }

    class loadContactDetails extends AsyncTask<String, String, String> {
        long contactID;
        ContactDto contactDto;

        public loadContactDetails(long contactID) {
            this.contactID = contactID;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            progressDialog = ProgressDialog.show(AddNumberToContact.this, ""
                            + getResources().getString(R.string.string_diag_pleasewait)
                            + "...",
                    "" + getResources().getString(R.string.adding_contact_fav)
                            + "...");
            progressDialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            quickContactHelper = new QuickContactHelper(AddNumberToContact.this);
            contactDto = new ContactDto();

            quickContactHelper = new QuickContactHelper(AddNumberToContact.this);
            contactDto = quickContactHelper.getContactDetails(contactID + "");

            EditContactsActivity.oldContactDto = contactDto;

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            Intent intent = new Intent(AddNumberToContact.this,
                    EditContactsActivity.class);
            intent.putExtra("isFromContactDetails", false);
            intent.putExtra("unknownNumberToAdd", number);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_up,
                    R.anim.slide_in_up_exit);
            progressDialog.dismiss();
            finish();

            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_to_favorite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        System.out.println("On loader finished");

        adapter = new AlphabetListAdapter(this);
        alphabet = new ArrayList<>();
        sections = new HashMap<>();

        List<Row> rows = new ArrayList<>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Object[] tmpIndexItem = null;
        Pattern numberPattern = Pattern.compile("[0-9]");
        Pattern specialPattern = Pattern.compile("[^A-Za-z-0-9]");
        contactDtoIDList = new ArrayList<Integer>();

        TestSectionedAdapter secAdapter;
        ArrayList<String> listDataHeader = new ArrayList<>();
        HashMap<String, List<String>> listDataChild = new HashMap<>();
        List<String> items = new ArrayList<>();

        while (data.moveToNext()) {
            String contactDisplayName = data.getString(data.getColumnIndex(Contacts.DISPLAY_NAME));
            String firstLetter = "";

            int contactId = data.getInt(data.getColumnIndex(Contacts._ID));
            if (contactDisplayName != null) {
                firstLetter = contactDisplayName.substring(0, 1).toUpperCase();
            }
            // Group numbers together in the scroller
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }

            if (specialPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }

            // If we've changed to a new letter, add the previous letter to the
            // alphabet scroller
            if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                end = rows.size() - 1;
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase();
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = end;
                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equals(previousLetter)) {
                rows.add(new Section(firstLetter));
                sections.put(firstLetter, start);
                contactDtoIDList.add(0);
                listDataHeader.add(firstLetter);
                items = new ArrayList<String>();
            }

            // Add the country to the list
            contactDtoIDList.add(contactId);
            items.add(contactDisplayName);
            rows.add(new Item(contactDisplayName));
            if (!firstLetter.equals(previousLetter)) {
                listDataChild.put(firstLetter, items);
            }
            previousLetter = firstLetter;

        }

        if (previousLetter != null) {
            // Save the last letter
            tmpIndexItem = new Object[3];
            tmpIndexItem[0] = previousLetter.toUpperCase();
            tmpIndexItem[1] = start;
            tmpIndexItem[2] = rows.size() - 1;
            alphabet.add(tmpIndexItem);
        }

        adapter.setRows(rows);
        secAdapter = new TestSectionedAdapter(listDataHeader, listDataChild);
        contact_listview.setAdapter(secAdapter);

        updateList();

    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public android.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri baseUri;
        if (mCurrentFilter != null) {
            baseUri = Uri.withAppendedPath(Contacts.CONTENT_FILTER_URI,
                    Uri.encode(mCurrentFilter));
        } else {
            baseUri = Contacts.CONTENT_URI;
        }

        String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + Contacts.DISPLAY_NAME + " != '' ))";

        String[] projection = new String[]{Contacts._ID,
                Contacts.DISPLAY_NAME, Contacts.CONTACT_STATUS,
                Contacts.CONTACT_PRESENCE, Contacts.PHOTO_ID,
                Contacts.LOOKUP_KEY,};

        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";

        CursorLoader cursorLoader = new CursorLoader(this, baseUri, projection,
                select, null, sortOrder);

        return cursorLoader;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.slide_out_down);
    }

    TextView tmpTV;
    LinearLayout addNumberSideIndex;

    public void updateList() {
        addNumberSideIndex = (LinearLayout) findViewById(R.id.addnumber_sideIndex);
        addNumberSideIndex.removeAllViews();
        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }

        int indexMaxSize = (int) Math
                .floor(addNumberSideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

        for (int l = 0; l < let.length; l++) {
            boolean isExist = false;
            for (double i = 1; i <= indexListSize; i = i + delta) {
                Object[] tmpIndexItem = alphabet.get((int) i - 1);
                String tmpLetter = tmpIndexItem[0].toString();

                if (let[l].equalsIgnoreCase(tmpLetter)) {
                    showLetters(let[l]);
                    isExist = true;
                    break;
                }

            }
            if (!isExist) {
                showLetters(let[l]);
            }

        }

        addNumberSideIndexHeight = addNumberSideIndex.getHeight();

        addNumberSideIndex.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // now you know coordinates of touch
                addnumber_sideIndexX = event.getX();
                addnumber_sideIndexY = event.getY();

                // and can display a proper item it country list
                displayListItem();

                return false;
            }
        });

    }

    public void showLetters(String let) {
        tmpTV = new TextView(AddNumberToContact.this);
        tmpTV.setText(let);
        tmpTV.setGravity(Gravity.CENTER);
        tmpTV.setTextSize(10);
        tmpTV.setTextColor(getResources().getColor(R.color.phone_app_blue));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        tmpTV.setLayoutParams(params);
        addNumberSideIndex.addView(tmpTV);
    }

    public void displayListItem() {
        LinearLayout addnumber_sideIndex = (LinearLayout) findViewById(R.id.addnumber_sideIndex);
        addNumberSideIndexHeight = addnumber_sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) addNumberSideIndexHeight
                / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (addnumber_sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition < alphabet.size()) {
            Object[] indexItem = alphabet.get(itemPosition);
            int subitemPosition = sections.get(indexItem[0]);

            // ListView listView = (ListView) findViewById(android.R.id.list);
            contact_listview.setSelection(subitemPosition);
        }
    }

}
