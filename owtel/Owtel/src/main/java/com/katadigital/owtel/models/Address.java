package com.katadigital.owtel.models;

/**
 * Created by dcnc123 on 11/28/16.
 */

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Address {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("address_list")
    @Expose
    private List<AddressList> addressList = new ArrayList<AddressList>();

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The addressList
     */
    public List<AddressList> getAddressList() {
        return addressList;
    }

    /**
     *
     * @param addressList
     * The address_list
     */
    public void setAddressList(List<AddressList> addressList) {
        this.addressList = addressList;
    }

}