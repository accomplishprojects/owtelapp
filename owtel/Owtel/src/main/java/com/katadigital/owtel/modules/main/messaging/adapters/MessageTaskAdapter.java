package com.katadigital.owtel.modules.main.messaging.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.modules.main.messaging.MessageTaskObj;

import java.util.List;

/**
 * Created by dcnc123 on 2/24/17.
 */

public class MessageTaskAdapter extends ArrayAdapter<MessageTaskObj> {

    public MessageTaskAdapter(Context context, List<MessageTaskObj> messageTaskObjectList) {
        super(context, 0, messageTaskObjectList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_message_task_layout, parent, false);
            vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        } else vh = (ViewHolder) convertView.getTag();

        MessageTaskObj messageTaskObj = getItem(position);

        vh.title.setText(messageTaskObj.getTitle());
        vh.description.setText(messageTaskObj.getDescription());

        return convertView;
    }

    private static final class ViewHolder {
        TextView title;
        TextView description;

        public ViewHolder(View v) {
            title = (TextView) v.findViewById(R.id.title);
            description = (TextView) v.findViewById(R.id.description);
        }
    }
}