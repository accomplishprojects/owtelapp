package com.katadigital.owtel.modules.resetaccess;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Omar Matthew Reyes on 4/28/16.
 * Forgot Password Fragment
 */
public class ForgotPasswordFragment extends Fragment {
    public static final String TAG = "ForgotPassFragment";

    @Bind(R.id.et_email)
    EditText etEmail;

    @Bind(R.id.tv_email_required)
    TextView tvEmailNotice;

    @Bind(R.id.et_code)
    EditText etCode;

    @Bind(R.id.tv_code_required)
    TextView tvCodeNotice;

    @Bind(R.id.et_password)
    EditText etPassword;

    @Bind(R.id.tv_pass_required)
    TextView tvPassNotice;

    @Bind(R.id.checkbox_password_show)
    CheckBox checkBoxPasswordShow;

    @Bind(R.id.btn_submit)
    Button btnSubmit;

    @Bind(R.id.layout_forgot_password_email)
    View layoutForgotPassEmail;

    @Bind(R.id.layout_forgot_password_code)
    View layoutForgotPassCode;

    @Bind(R.id.layout_forgot_password_change)
    View layoutForgotPassChange;

    private final int FORGOT_PASS_EMAIL = 0, FORGOT_PASS_CODE = 1,
            FORGOT_PASS_CHANGE = 2;

    FragmentHolderActivity fragmentHolderActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_forgot_password, container, false);
        ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        fragmentHolderActivity = (FragmentHolderActivity) getActivity();

        checkBoxPasswordShow.setChecked(false);
        checkBoxPasswordShow.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) etPassword.setTransformationMethod(null);
            else etPassword.setTransformationMethod(new PasswordTransformationMethod());

            etPassword.setSelection(etPassword.getText().length());
        });
        return view;
    }

    @OnClick(R.id.btn_submit)
    void onSubmit() {
        if (GlobalValues.DEBUG) Log.i(TAG, "Email: " + etEmail.getText().toString().trim() +
                " Code: " + etCode.getText().toString().trim() +
                " Password " + etPassword.getText().toString().trim());
//        if (getCurrentPage() == 2)
//            fragmentHolderActivity.changeFragment(new ForgotPasswordSuccessFragment(), false);
//        else setForgotPasswordPage(getCurrentPage() + 1);
        switch (getCurrentPage()) {
            case FORGOT_PASS_EMAIL:
                submitForgotPassEmail();
                break;
            case FORGOT_PASS_CODE:
                submitForgotPassCode();
                break;
            case FORGOT_PASS_CHANGE:
                submitForgotPassChange();
                break;
        }
    }

    /**
     * Check if email format is valid
     *
     * @param etEmail EditText
     * @return boolean
     */
    public boolean isEmailValid(EditText etEmail) {
        boolean valid = true;
        String email = etEmail.getText().toString().trim();

        if (email.equals("") || email.trim().isEmpty()) {
            tvEmailNotice.setVisibility(View.VISIBLE);
            valid = false;
        } else tvEmailNotice.setVisibility(View.GONE);

        Pattern pattern = Pattern.compile(Constants.REGEX_EMAIL);
        Matcher matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            Toast.makeText(getActivity(), R.string.invalid_email, Toast.LENGTH_SHORT).show();
            valid = false;
        }

        return valid;
    }

    /**
     * Check if new password is valid
     *
     * @param etPassword EditText
     * @return boolean
     */
    public boolean isPasswordValid(EditText etPassword) {
        boolean valid = true;
        String password = etPassword.getText().toString();

        if (password.equals("") || password.trim().isEmpty()) {
            tvPassNotice.setVisibility(View.VISIBLE);
            valid = false;
        } else tvPassNotice.setVisibility(View.GONE);

        Pattern pattern = Pattern.compile(Constants.REGEX_PASSWORD);
        Matcher matcher = pattern.matcher(password);

        if (!matcher.matches()) {
            Toast.makeText(getActivity(), getString(R.string.invalid_password), Toast.LENGTH_SHORT).show();
            valid = false;
        }

        return valid;
    }

    /**
     * Check if code is valid
     *
     * @param etCode EditText
     * @return boolean
     */
    public boolean isCodeValid(EditText etCode) {
        boolean valid = true;
        String code = etCode.getText().toString().trim();

        if (code.equals("") || code.trim().isEmpty()) {
            tvCodeNotice.setVisibility(View.VISIBLE);
            valid = false;
        } else tvCodeNotice.setVisibility(View.GONE);
        return valid;
    }

    /**
     * Set layout view visibility
     *
     * @param page int
     */
    private void setForgotPasswordPage(int page) {
        switch (page) {
            case FORGOT_PASS_EMAIL:
                layoutForgotPassEmail.setVisibility(View.VISIBLE);
                layoutForgotPassCode.setVisibility(View.GONE);
                layoutForgotPassChange.setVisibility(View.GONE);
                break;
            case FORGOT_PASS_CODE:
                layoutForgotPassEmail.setVisibility(View.GONE);
                layoutForgotPassCode.setVisibility(View.VISIBLE);
                layoutForgotPassChange.setVisibility(View.GONE);
                break;
            case FORGOT_PASS_CHANGE:
                layoutForgotPassEmail.setVisibility(View.GONE);
                layoutForgotPassCode.setVisibility(View.GONE);
                layoutForgotPassChange.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Get current layout view page
     *
     * @return int
     */
    private int getCurrentPage() {
        if (layoutForgotPassEmail.getVisibility() == View.VISIBLE) return FORGOT_PASS_EMAIL;
        else if (layoutForgotPassCode.getVisibility() == View.VISIBLE) return FORGOT_PASS_CODE;
        else if (layoutForgotPassChange.getVisibility() == View.VISIBLE) return FORGOT_PASS_CHANGE;
        else return -1;
    }

    /**
     * Forgot Password API request for sending Email
     * Next: Send received code
     */
    private void submitForgotPassEmail() {
        if (isEmailValid(etEmail)) {
            if (GlobalValues.DEBUG) Log.i(TAG, "Submit email " + etEmail.getText());
            fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
            fragmentHolderActivity.mProgressDialog.show();
            StringRequest mStringRequestPassword = new StringRequest(Request.Method.POST, API.URL_TEST + API.FORGOT_PASS_EMAIL, response -> {
                try {
                    fragmentHolderActivity.mProgressDialog.dismiss();
                    JSONObject jsonObjectMain = new JSONObject(response);
                    String jsonObjectStatus = jsonObjectMain.getString("status");
                    if (jsonObjectStatus.equals("Success")) {
                        setForgotPasswordPage(getCurrentPage() + 1);
                    } else {
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        String jsonObjectMessage = jsonObjectMain.getString("message");
                        fragmentHolderActivity.showAlertDialog(getActivity(),
                                jsonObjectStatus, jsonObjectMessage,
                                getResources().getString(R.string.string_ok));
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "forgotPassword " + e);
                }
            }, error -> {
                if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
                fragmentHolderActivity.mProgressDialog.dismiss();
                DialogBuilder.displayVolleyError(getActivity(), error);
            }) {
                @Override
                protected Map<String, String> getParams() {
                    final Map<String, String> params = new HashMap<>();
                    params.put(API.FORGOT_PASSWORD_EMAIL, etEmail.getText().toString());
                    return params;
                }
            };
            mStringRequestPassword.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalValues.REQUEST_TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mStringRequestPassword.setTag(Constants.REQUEST_TAG_FORGOT_PASS);
            OwtelAppController.getInstance().addToRequestQueue(mStringRequestPassword);
        }
    }

    /**
     * Forgot Password API request for sending code
     * Next: Send new password
     */
    private void submitForgotPassCode() {
        if (isCodeValid(etCode)) {
            // TODO Change API URL
            fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
            fragmentHolderActivity.mProgressDialog.show();
            StringRequest mStringRequestPassword = new StringRequest(Request.Method.POST, API.URL_TEST + API.FORGOT_PASS_KEY, response -> {
                try {
                    fragmentHolderActivity.mProgressDialog.dismiss();
                    JSONObject jsonObjectMain = new JSONObject(response);
                    String jsonObjectStatus = jsonObjectMain.getString("status");
                    if (jsonObjectStatus.equals("Success")) {
                        setForgotPasswordPage(getCurrentPage() + 1);
                    } else {
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        String jsonObjectMessage = jsonObjectMain.getString("message");
                        fragmentHolderActivity.showAlertDialog(getActivity(),
                                jsonObjectStatus, jsonObjectMessage,
                                getResources().getString(R.string.string_ok));
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "forgotPassword code " + e);
                }
            }, error -> {
                if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
                fragmentHolderActivity.mProgressDialog.dismiss();
                DialogBuilder.displayVolleyError(getActivity(), error);
            }) {
                @Override
                protected Map<String, String> getParams() {
                    final Map<String, String> params = new HashMap<>();
                    params.put(API.FORGOT_PASSWORD_EMAIL, etEmail.getText().toString());
                    params.put(API.FORGOT_PASSWORD_CODE, etCode.getText().toString());
                    return params;
                }
            };
            mStringRequestPassword.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalValues.REQUEST_TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mStringRequestPassword.setTag(Constants.REQUEST_TAG_FORGOT_PASS);
            OwtelAppController.getInstance().addToRequestQueue(mStringRequestPassword);
        }
    }

    /**
     * Forgot Password API request for sending new password
     * Next: Go back to LoginActivity
     */
    private void submitForgotPassChange() {
        if (isPasswordValid(etPassword)) {
            fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
            fragmentHolderActivity.mProgressDialog.show();
            StringRequest mStringRequestPassword = new StringRequest(Request.Method.POST, API.URL_TEST + API.FORGOT_PASS_CHANGE, response -> {
                try {
                    fragmentHolderActivity.mProgressDialog.dismiss();
                    JSONObject jsonObjectMain = new JSONObject(response);
                    String jsonObjectStatus = jsonObjectMain.getString("status");
                    if (jsonObjectStatus.equals("Success")) {
                        // Go to success Fragment
                        fragmentHolderActivity.changeFragment(new ForgotPasswordSuccessFragment(), false);
                    } else {
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        String jsonObjectMessage = jsonObjectMain.getString("message");
                        fragmentHolderActivity.showAlertDialog(getActivity(),
                                jsonObjectStatus, jsonObjectMessage,
                                getResources().getString(R.string.string_ok));
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "forgotPassword change " + e);
                }
            }, error -> {
                if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
                fragmentHolderActivity.mProgressDialog.dismiss();
                DialogBuilder.displayVolleyError(getActivity(), error);
            }) {
                @Override
                protected Map<String, String> getParams() {
                    final Map<String, String> params = new HashMap<>();
                    params.put(API.FORGOT_PASSWORD_EMAIL, etEmail.getText().toString());
                    params.put(API.FORGOT_PASSWORD_CODE, etCode.getText().toString());
                    params.put(API.FORGOT_PASSWORD_CHANGE, etPassword.getText().toString());
                    return params;
                }
            };
            mStringRequestPassword.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalValues.REQUEST_TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mStringRequestPassword.setTag(Constants.REQUEST_TAG_FORGOT_PASS);
            OwtelAppController.getInstance().addToRequestQueue(mStringRequestPassword);
        }
    }
}
