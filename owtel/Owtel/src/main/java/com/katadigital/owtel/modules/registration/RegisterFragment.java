package com.katadigital.owtel.modules.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.models.ListAreaCode;
import com.katadigital.owtel.models.RegisterUser;
import com.katadigital.owtel.models.collectedobjects.RegistrationResult;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.SharedPreferencesManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterFragment extends Fragment{
    public static final String TAG = "RegisterFragment";

    @Bind(R.id.btn_register)
    Button btnRegister;

    @Bind(R.id.input_layout_password)
    TextInputLayout tilPassword;

    @Bind(R.id.input_layout_email)
    TextInputLayout tilEmail;

    @Bind(R.id.et_email)
    EditText etEmail;

    @Bind(R.id.et_password)
    EditText etPassword;

    @Bind(R.id.checkbox_password_show)
    CheckBox checkBoxPasswordShow;

    @Bind(R.id.et_promo_code)
    EditText etPromoCode;

    FragmentHolderActivity fragmentHolderActivity;

    Bundle args = new Bundle();


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_register, container, false);
        ButterKnife.bind(this, view);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        fragmentHolderActivity = (FragmentHolderActivity) getActivity();

        checkBoxPasswordShow.setChecked(false);
        checkBoxPasswordShow.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) etPassword.setTransformationMethod(null);
            else etPassword.setTransformationMethod(new PasswordTransformationMethod());

            etPassword.setSelection(etPassword.getText().length());
        });

        SharedPreferencesManager.sharedManagerInit(container.getContext());
        return view;
    }

    @OnClick(R.id.btn_register)
    public void onContinueClicked() {
        fragmentHolderActivity.hideSoftInput(btnRegister);
        if (validateFieldValues()) {
            fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
            fragmentHolderActivity.mProgressDialog.show();

            fragmentHolderActivity
                    .getRetrofitService()
                    .getApiService()
                    .registerUser(etEmail.getText().toString().trim(),
                    etPassword.getText().toString().trim(),
                    etPromoCode.getText().toString().trim())
                    .enqueue(new Callback<RegisterUser>() {
                        @Override
                        public void onResponse(Response<RegisterUser> response, Retrofit retrofit) {
                                if(response.body().getStatus().equalsIgnoreCase("Success")){
                                    Log.e(TAG, "success!");

                                    RegistrationResult registrationResult = new RegistrationResult();

                                    registrationResult.setEmail(etEmail.getText().toString());
                                    registrationResult.setPassword(etPassword.getText().toString());
                                    registrationResult.setWebCsUser(response.body().isWebCsUser());

                                    args.putString(Constants.BUNDLE_REGISTER_EMAIL, etEmail.getText().toString().trim());
                                    args.putString(Constants.BUNDLE_REGISTER_PASSWORD, etPassword.getText().toString().trim());
                                    args.putBoolean(Constants.BUNDLE_REGISTER_WEBCS_USER, response.body().isWebCsUser());


                                    args.putSerializable
                                            (Constants.REGISTRATION_RESULT,registrationResult.toString());
                                    SharedPreferencesManager.getInstance()
                                            .putRegistrationResultToShared(registrationResult);

                                    if(response.body().getPromoCode() !=null){
                                       SharedPreferencesManager
                                               .getInstance()
                                               .putPromoCodeToShared(response.body().getPromoCode());

                                        args.putSerializable(
                                                Constants.PROMO_CODE
                                                ,response.body().getPromoCode());

                                        args.putString(Constants.BUNDLE_REGISTER_PROMO_CODE, response.body().getPromoCode());
                                    }

                                    getAreaCodes(args);
                                }else {
                                    if(fragmentHolderActivity.mProgressDialog.isShowing()){
                                        fragmentHolderActivity.mProgressDialog.dismiss();
                                    fragmentHolderActivity.showAlertDialog(getActivity(),
                                            response.body().getStatus(), response.body().getMessage(),
                                            getResources().getString(R.string.string_ok));
                                    }
                                }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            if(fragmentHolderActivity.mProgressDialog.isShowing()){
                                fragmentHolderActivity.mProgressDialog.dismiss();
                                Toast.makeText(getActivity(), "Register " + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
        }
    }


    private void getAreaCodes(Bundle args) {
        fragmentHolderActivity.apiService.getAreaCodes().enqueue(new Callback<ListAreaCode>() {
            @Override
            public void onResponse(Response<ListAreaCode> response, Retrofit retrofit) {
                for (com.katadigital.owtel.models.AreaCode areaCode : response.body().getAreaCodeList()) {
                    Log.e(TAG, "getAreaCodes() area code " + areaCode.getAreaCode());
                    Log.e(TAG, "getAreaCodes() city " + areaCode.getCity());
                    Log.e(TAG, "getAreaCodes() state " + areaCode.getState());
                }

                fragmentHolderActivity.mProgressDialog.dismiss();

                ((AreaCodeBridge) getActivity()).setAreaCodeList(response.body().getAreaCodeList(), args);
            }

            @Override
            public void onFailure(Throwable t) {
                fragmentHolderActivity.mProgressDialog.dismiss();
                Toast.makeText(getActivity(), "Get Area Codes " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateFieldValues() {
        boolean valid = true;
        if (!isEmailValid(etEmail)) {
            valid = false;
        }

        if (!isPasswordValid(etPassword)) {
            valid = false;
        }
        return valid;
    }


    public boolean isPasswordValid(EditText etPassword) {
        String password = etPassword.getText().toString();

        if (password == null || password.trim().isEmpty()) {
            tilPassword.setError(getString(R.string.required));
            return false;
        }

        Pattern pattern = Pattern.compile(Constants.REGEX_PASSWORD);
        Matcher matcher = pattern.matcher(password);

        if (!matcher.matches()) {
            tilPassword.setError(getString(R.string.invalid_password));
            return false;
        }
        tilPassword.setError(null);
        return true;
    }

    public boolean isEmailValid(EditText etEmail) {
        String email = etEmail.getText().toString().trim();

        if (email == null || email.trim().isEmpty()) {
            tilEmail.setError(getString(R.string.required));
            return false;
        }

        Pattern pattern = Pattern.compile(Constants.REGEX_EMAIL);
        Matcher matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            tilEmail.setError(getString(R.string.invalid_email));
            return false;
        }

        tilEmail.setError(null);
        return true;
    }

    public void autoPopulateEmail(String email) {
        etEmail.setText(email);
    }
}
