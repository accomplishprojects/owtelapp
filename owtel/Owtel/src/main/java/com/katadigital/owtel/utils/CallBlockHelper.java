package com.katadigital.owtel.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.portsip.helper.Ring;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;

/**
 * Created by dcnc123 on 3/2/17.
 */
public class CallBlockHelper {

    private static final String TAG = "CallBlockHelper";
    private static CallBlockHelper single = null;
    private Context mContext;


    public static CallBlockHelper getInstance(Context context) {
        if (single == null) {
            single = new CallBlockHelper(context);
        }
        return single;
    }

    private CallBlockHelper(Context context) {
        mContext = context;
    }


    public boolean checkIfContactIsBlocked(String phoneNumber) {

        QuickContactHelper quickContactHelper = new QuickContactHelper(mContext);
        ContactDto contactDto= quickContactHelper.getContactDetails(getContactCode(phoneNumber) + "");
        boolean result = false;
        try {
            for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
                if (!phoneNumberDto.getNumber().isEmpty()) {
                    if (MainActivity.blackList.containsKey(phoneNumberDto.getNumber().replaceAll(" ", ""))) {
                        result = true;
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            Log.e(TAG, "checkIfContactIsBlocked " + e);
        }
        return result;
    }


    private String getContactCode(String phoneNo)
    {
        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNo));//phone no
        String[] proj = new String[]
                {///as we need only this colum from a table...
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts._ID,
                };
        String id=null;
        String sortOrder1 = ContactsContract.CommonDataKinds.StructuredPostal.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor crsr = mContext.getContentResolver().query(lookupUri,proj, null, null, sortOrder1);
        while(crsr.moveToNext())
        {
            String name=crsr.getString(crsr.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            id = crsr.getString(crsr.getColumnIndex(ContactsContract.Contacts._ID));
//            Toast.makeText(this,"Name of this cntct is   : " +name +"\n id is : "+id ,Toast.LENGTH_SHORT).show();
        }
        crsr.close();
        return id;
    }
}
