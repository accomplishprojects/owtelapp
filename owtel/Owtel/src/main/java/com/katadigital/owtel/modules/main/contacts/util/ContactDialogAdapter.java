package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kata.phone.R;

public class ContactDialogAdapter extends BaseAdapter {

    Activity activity;
    ArrayList<AccountCreate> glist;

    public ContactDialogAdapter(Activity activity,
                                ArrayList<AccountCreate> gList) {
        this.activity = activity;

        this.glist = new ArrayList<AccountCreate>();
        this.glist.addAll(gList);
    }

    @Override
    public int getCount() {
        return glist.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater
                .inflate(R.layout.diag_account, viewGroup, false);

        TextView txt_account = (TextView) rowView
                .findViewById(R.id.txt_account);
        TextView txt_info = (TextView) rowView.findViewById(R.id.txt_info);
        ImageView ic_account = (ImageView) rowView
                .findViewById(R.id.ic_account);

        txt_account.setTypeface(NewUtil.getFontRoman(activity));
        txt_info.setTypeface(NewUtil.getFontRoman(activity));

        txt_account.setTextSize(NewUtil.gettxtSize());
        txt_info.setTextSize(NewUtil.gettxtSize());

        if (glist.get(position) != null) {
            AccountCreate acreate = glist.get(position);
            txt_account.setText(acreate.getName());
            txt_info.setText(acreate.getName2());
            ic_account.setBackgroundDrawable(acreate.getDraw());
        }

        return rowView;
    }

}
