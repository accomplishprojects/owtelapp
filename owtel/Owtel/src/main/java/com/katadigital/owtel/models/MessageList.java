
package com.katadigital.owtel.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MessageList {

    @SerializedName("message")
    @Expose
    private List<Message> message = new ArrayList<Message>();

    /**
     * 
     * @return
     *     The message
     */
    public List<Message> getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(List<Message> message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this).toString();
    }
}
