package com.katadigital.owtel.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsNumberList {

    @SerializedName("lock_id")
    @Expose
    private String lockId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("owtel_numbers")
    @Expose
    private List<String> owtelNumbers = new ArrayList<String>();

    /**
     *
     * @return
     * The lockId
     */
    public String getLockId() {
        return lockId;
    }

    /**
     *
     * @param lockId
     * The lock_id
     */
    public void setLockId(String lockId) {
        this.lockId = lockId;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The owtelNumbers
     */
    public List<String> getOwtelNumbers() {
        return owtelNumbers;
    }

    /**
     *
     * @param owtelNumbers
     * The owtel_numbers
     */
    public void setOwtelNumbers(List<String> owtelNumbers) {
        this.owtelNumbers = owtelNumbers;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this).toString();
    }
}