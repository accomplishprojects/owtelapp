package com.katadigital.owtel.modules.main;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.billing.CallAndTextSubscriptionPaymentFragment;
import com.katadigital.owtel.modules.billing.TopUpIDDPaymentFragment;
import com.katadigital.owtel.modules.main.contacts.ContactsFragment;
import com.katadigital.owtel.modules.main.contacts.MyCallReceiver;
import com.katadigital.owtel.modules.main.contacts.callsmsblocker.objects.BlockedContact;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.util.BadgeView;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.owtel.modules.main.dialer.DialerFragment;
import com.katadigital.owtel.modules.main.favorites.FavoritesFragment;
import com.katadigital.owtel.modules.main.history.LogsFragment;
import com.katadigital.owtel.modules.main.messaging.ConversationsFragment;
import com.katadigital.owtel.modules.main.settings.SettingsFragment;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.RxBus;
import com.katadigital.portsip.utilities.androidcomponent.interfaces.PortSipContract;
import com.katadigital.portsip.utilities.androidcomponent.service.SipSettings;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.katadigital.portsip.utilities.tone.ToneHelper;
import com.katadigital.ui.recyclerview.DialerRecyclerViewAdapter;
import com.katadigital.ui.slidingtab.ViewPagerAdapter;
import com.securepreferences.SecurePreferences;

import org.parceler.Parcels;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * OwtelAppDaoGenerator Activity
 * <p>
 * User goes here after Login
 * Saves user SIP Settings
 * //TODO Saves user Web Socket Settings
 * Starts SIP and Web Socket service
 * <p>
 * Initializes ViewPager Fragments
 */


//Fix resending message failed and success
//auto top up
//registration


public class MainActivity extends AppCompatActivity implements MainActivityBridge,
        ISimpleDialogListener, PortSipContract.PortSipContractListener,
        PortSipContract.PortSipContractCallback {

    public static final int REQUEST_TOP_UP = 83;
    private final String SUBSCRIPTION = "subscription", TOP_UP = "top_up";
    private final String TAG = this.getClass().getSimpleName();
    private ApiHelper apiHelper;
    public int currentTab = 0;
    ArrayList<Fragment> fr_list = new ArrayList<>();

    @Inject
    public static RxBus rxBus;
    @Inject
    public ApiService apiService;

    /**
     * Variables for logsFragment
     */
    public LogsFragment logsFragment;
    public DialerFragment currentFragment;
    public static HashMap<String, BlockedContact> blackList;

    SharedPreferences preferencesUser;
    public ViewPagerAdapter adapter;
    public ProgressDialog mProgressDialog;
    private TextView txtMissedCallBadge, txtSmsBadge;
    /**
     * Fragments variables
     */
    private FragmentManager fragmentManager;
    public TabLayout tabLayout;
    private ViewPager pager;
    private int[] tabIcons = {
            R.drawable.ic_tab_selector_favorites,
            R.drawable.ic_tab_selector_logs,
            R.drawable.ic_tab_selector_contacts,
            R.drawable.ic_tab_selector_dialer,
            R.drawable.ic_tab_selector_messages
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_main);
        SharedPreferenceManager.init(this);
        OwtelAppController.getComponent(this).inject(this);
        fragmentManager = getSupportFragmentManager();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);


        initBlocker();
        messagesAppBlock();

        /**Setting up ViewPager Adapter fragments*/
        Bundle args = new Bundle();
        args.putBoolean(Constants.BUNDLE_HIDE_BTN_BACK, true);

        SettingsFragment settingsFragment = new SettingsFragment();
        settingsFragment.setArguments(args);
        SettingsFragment settingsFragment2 = new SettingsFragment();
        settingsFragment2.setArguments(args);

        fr_list.add(settingsFragment);
        fr_list.add(new FavoritesFragment());
        fr_list.add(new LogsFragment());
        fr_list.add(new ContactsFragment());
        fr_list.add(new DialerFragment());
        fr_list.add(new ConversationsFragment());
        fr_list.add(settingsFragment2);

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), fr_list);
        String[] tabTitle = getResources().getStringArray(R.array.tab_title);

        /**Assigning ViewPager View and setting the adapter**/
        pager = (ViewPager) findViewById(R.id.pager);
        if (pager != null) {
            pager.setAdapter(adapter);
            pager.setOffscreenPageLimit(8);
        }

        /**Assigning the Sliding Tab Layout View*/
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout != null) {
            for (int index = 0; index < tabIcons.length; index++) {
                try {
                    View tab = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

                    ImageView imgTabIcon = (ImageView) tab.findViewById(R.id.imgTabIcon);
                    imgTabIcon.setImageResource(tabIcons[index]);
                    TextView txtTabLabel = (TextView) tab.findViewById(R.id.txtTabLabel);
                    txtTabLabel.setText(tabTitle[index]);

                    tabLayout.addTab(tabLayout.newTab()
                            .setCustomView(tab)
                            .setIcon(tabIcons[index])
                            .setText(tabTitle[index]));
                } catch (NullPointerException e) {
                    Log.e(TAG, "setupTabIcons " + e);
                }
            }
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabSelected: " + tab.getPosition());
                pager.setCurrentItem(tab.getPosition() + 1);

                if (tab.getPosition() == 1) {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putMissedCallBadgeCount(0);

                } else if (tab.getPosition() == 4) {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSMSBadgeCount(0);
                }

                onUpdateBadgeCount();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (pager.getCurrentItem() == 0 && tab.getPosition() == 0) {
                    pager.setCurrentItem(1);
                } else if (pager.getCurrentItem() == 6 && tab.getPosition() == 4) {
                    pager.setCurrentItem(5);
                }
            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // auto generated method
                DialerRecyclerViewAdapter.dtmfGenerator.stopTone();
            }

            @Override
            public void onPageSelected(int position) {
                Log.e(TAG, "onPageSelected: " + position);
                if (position == 3) {
                    tabLayout.setScrollPosition(3, 0, true);
                }
                if (position > 0) {
                    tabLayout.setScrollPosition(position - 1, 0, true);
                } else {
                    tabLayout.setScrollPosition(0, 0, true);
                }

                new ApiHelper(MainActivity.this).checkRadSession();
                NewUtil.hideKeyboard(MainActivity.this);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // auto generated method
            }
        });

        pager.setCurrentItem(3); //Set default page dialer (Dialer)
        tabLayout.getTabAt(2).select();

        /**Set default notification at first install of app*/
        String notifName = SharedPreferenceManager.getSharedPreferenceManagerInstance().getSelectedNotificationName();
        String notifURI = SharedPreferenceManager.getSharedPreferenceManagerInstance().getSelectedNotificationURI();

        String ringtoneName = SharedPreferenceManager.getSharedPreferenceManagerInstance().getSelectedNotificationName();
        String ringtoneURI = SharedPreferenceManager.getSharedPreferenceManagerInstance().getSelectedNotificationURI();
        if (notifName == null && notifURI == null && ringtoneName == null && ringtoneURI == null) {
            new ToneHelper(this).setDefaultRingAndNotificationTone();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "Lifecycle onStart");
        apiHelper = new ApiHelper(this);

        /**
         * username
         * password
         * server
         * port
         * protocol
         *
         * See org.sipdroid.sipua.ui.Settings for the DEFAULT preferences
         */

        SipSettings sipSettings = new SipSettings();
        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");
        String pass = preferencesUser.getString(Constants.PREFS_USER_PASS, "empty_pass");
        boolean userType = preferencesUser.getBoolean(Constants.PREFS_USER_TYPE, false);
        String key = email + pass;

        SecurePreferences securePreferences = OwtelAppController.getInstance().getUserPinBasedSharedPreferences(key);
        String sipNumber = securePreferences.getString(Constants.PREFS_USER_NUMBER, "empty_number");
//        String sipNumber = SharedPreferenceManager.getSharedPreferenceManagerInstance().getOwtelNumber();
        Log.e(TAG, "onStart: OwtelAccountNumber: " + sipNumber);
        String sipPass = securePreferences.getString(Constants.PREFS_SIP_PASS, "empty_sip_pass");
        String sipServer = securePreferences.getString(Constants.PREFS_SIP_SERVER, "empty_sip_server");
        String sipPort = securePreferences.getString(Constants.PREFS_SIP_PORT, "empty_sip_port");
//        String radId = securePreferences.getString(Constants.PREFS_RAD_ID, "empty_rad_id");
//        SharedPreferences prefs = getSharedPreferences(Constants.PREFS_RAD_NAME, MODE_PRIVATE);
//        radId = prefs.getString(Constants.PREFS_RAD_ID, null);

        sipSettings.setSipUsername(sipNumber);
        sipSettings.setSipPassword(sipPass);
        sipSettings.setSipServer(sipServer);
        sipSettings.setSipPort(sipPort);
        sipSettings.setEnable3g(true);
//        sipSettings.setMicGain(0.1);
//        sipSettings.setEarpieceGain(1.0);
//        sipSettings.setHeadsetGain(1.0);


        if (GlobalValues.DEBUG) {
            Log.i(TAG, "User Premium: " + userType);
            Log.i(TAG, "Updated USERNAME: " + sipSettings.getSipUsername());
            Log.i(TAG, "Updated PASSWORD: " + sipSettings.getSipPassword());
            Log.i(TAG, "Updated SERVER: " + sipSettings.getSipServer());
            Log.i(TAG, "Updated PORT: " + sipSettings.getSipPort());
            Log.i(TAG, "Updated PROTOCOL: " + sipSettings.getSipProtocol());
            Log.i(TAG, "Updated CODECS: " + sipSettings.getSipCodec());
//            Log.i(TAG, "Rad Id: " + radId);
        }

        Log.i(TAG, "Connection Pref WLAN " + sipSettings.getPrefWlan());
        Log.i(TAG, "Connection Pref 3G " + sipSettings.getPref3g());
        Log.i(TAG, "Connection Pref Edge " + sipSettings.getPrefEdge());
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Lifecycle onResume");
        if (!Var.addLike) {
            if (MyCallReceiver.missedCount != 0) {
                pager.setCurrentItem(1, true);
            }
        } else {
            Log.i(TAG, "HIDE ICON");
            MyCallReceiver.missedCount = 0;
            MyCallReceiver.setBadge(this.getBaseContext(), MyCallReceiver.missedCount);
        }

        /**Show missed call and sms badge count*/
        int pagerCurrentItem = pager.getCurrentItem();
        if (pagerCurrentItem == 2) {
            SharedPreferenceManager.getSharedPreferenceManagerInstance()
                    .putMissedCallBadgeCount(0);

        } else if (pagerCurrentItem == 5) {
            SharedPreferenceManager.getSharedPreferenceManagerInstance()
                    .putSMSBadgeCount(0);
        }

        onUpdateBadgeCount();
        onUpdateBadgeCount();

        super.onResume();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void switchTab(int tab) {
        pager.setCurrentItem(tab, true);
        DialerFragment.fromKeypad = false;
    }

    /**
     * Set missed call badge to zero
     *
     * @param context  Context
     * @param isActive boolean
     */
    @Override
    public void setBadgeZero(Context context, boolean isActive) {
        if (isActive) {
            MyCallReceiver.setBadge(context, 0);
        }
    }

    /**
     * Initialize CallBlockerService
     */
    @Override
    public void initBlocker() {
        loadData();
    }

    /**
     * Load call blocker data
     */
    public void loadData() {
        try {
            FileInputStream fis = openFileInput("CallBlocker.data");
            ObjectInputStream objeto = new ObjectInputStream(fis);
            blackList = (HashMap<String, BlockedContact>) objeto.readObject();

            fis.close();
            objeto.close();
        } catch (Exception e) {
            blackList = new HashMap<>();
            Log.e(TAG, "Load Data Exception: " + e);
        }
    }

    public void messagesAppBlock() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                try {
                    // JM contact ID 13520
                    String data = (intent.getExtras().getString("checkBlockID"));

                    QuickContactHelper quickContactHelper = new QuickContactHelper(MainActivity.this);
                    ContactDto contactDto = quickContactHelper.getContactDetails(Integer.valueOf(data) + "");
                    boolean check = checkIfContactIsBlocked(contactDto);
                    NewUtil.integrationblockNumberFromMessagesApp(MainActivity.this, check, Integer.valueOf(data));
                    finish();

                    String checkBlock = (intent.getExtras().getString("contactBlocked"));
                    if (checkBlock != null)
                        if (checkBlock.equals("true")) {
                            blockContact(contactDto);
                            Toast.makeText(getApplicationContext(), "blocked", Toast.LENGTH_SHORT)
                                    .show();
                            finish();
                        } else {
                            unblockContact(contactDto);
                            Toast.makeText(getApplicationContext(), "unblocked",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                } catch (Exception e) {
                    Log.e(TAG, "messagesAppBlock() Exception " + e);
                }
            }
        }
    }

    /**
     * Check if contact number is blocked
     *
     * @param contactDto ContactDto
     * @return boolean
     */
    public boolean checkIfContactIsBlocked(ContactDto contactDto) {
        boolean result = false;
        for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
            if (!phoneNumberDto.getNumber().isEmpty()) {
                String numberChecker = String.valueOf(phoneNumberDto.getNumber().replaceAll(" ", ""));
                if (MainActivity.blackList.containsKey(numberChecker.trim())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Block contact number
     *
     * @param contactDto ContactDto
     */
    public void blockContact(ContactDto contactDto) {
        for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
            if (!phoneNumberDto.getNumber().isEmpty()) {
                int numberType = 0;
                for (int x = 0; x < getResources().getStringArray(
                        R.array.numberspinner).length; x++) {
                    if (phoneNumberDto.getNumberType().equalsIgnoreCase(
                            getResources()
                                    .getStringArray(R.array.numberspinner)[x])) {
                        numberType = x;
                    }
                }

                BlockedContact cn = new BlockedContact(contactDto.getDisplayName(),
                        phoneNumberDto.getNumber().replaceAll(" ", ""), numberType, true, true);
                MainActivity.blackList.put(cn.getNumber(), cn);
            }

        }
        if (contactDto.getPhoneNumbers().isEmpty()) {

        } else {
            saveData();
        }
    }

    /**
     * Save call blocker data
     */
    public void saveData() {
        try {
            FileOutputStream fos = openFileOutput("CallBlocker.data",
                    Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(blackList);
            fos.close();
            oos.close();
            this.initBlocker();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }

    /**
     * Unblock Contact
     *
     * @param contactDto ContactDto
     */
    public void unblockContact(ContactDto contactDto) {
        for (PhoneNumberDto phoneNumberDto : contactDto.getPhoneNumbers()) {
            if (!phoneNumberDto.getNumber().isEmpty()) {
                blackList.remove(phoneNumberDto.getNumber().replaceAll(" ", ""));
            }
        }
        try {
            saveData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set status bar color
     *
     * @param color int
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void statusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(color));
        }
    }

    /**
     * Change Fragment
     *
     * @param fragment       Fragment
     * @param addToBackStack boolean
     */
    public void changeFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (!addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Change Fragment and add to stack
     *
     * @param fragment Fragment
     * @param TAG      String
     */
    public void changeFragment(Fragment fragment, String TAG) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }

    /**
     * Change Fragment with Bundle
     *
     * @param fragment       Fragment
     * @param bundle         Bundle
     * @param addToBackStack boolean
     */
    public void changeFragment(Fragment fragment, Bundle bundle, boolean addToBackStack) {
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (!addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Hide SoftInputKeyboard
     *
     * @param view View
     */
    public void hideSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Pop current fragment. Initiate back stack
     */
    public void popFragment(boolean doRefresh) {
        fragmentManager.popBackStack();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG, "popFragment: " + doRefresh);
                if (doRefresh)
                    try {
                        if (adapter != null && fr_list.get(0) != null && fr_list.get(6) != null) {
                            ((SettingsFragment) adapter.fr_list.get(0)).refresh();
                            ((SettingsFragment) adapter.fr_list.get(6)).refresh();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "popFragment: refresh failed!");
                    }
            }
        }, 5000);
    }

    public void popFragment() {
        fragmentManager.popBackStack();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (GlobalValues.DEBUG) Log.i(TAG, "onAttachFragment " + fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == MainActivity.REQUEST_TOP_UP) {
            getAddressCountryListRequestTopup(TOP_UP);
        }
    }

    /**
     * Finish MainActivity
     */
    @Override
    public void finishActivity() {
        this.finish();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {

    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {

    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        if (requestCode == MainActivity.REQUEST_TOP_UP) {
            getAddressCountryListRequestTopup(TOP_UP);
        }
    }

    public void getAddressCountryListRequestTopup(String paymentType) {
        Bundle bundle = new Bundle();
        mProgressDialog.setMessage(getResources().getString(R.string.processing));
        mProgressDialog.show();
        ApiHelper.doGetAddressList(mProgressDialog, MainActivity.this, GlobalValues.GET_COUNTRY, "", "").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<AddressList>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<AddressList> addresses) {
                        Parcelable listAddressParcel = Parcels.wrap(addresses);
                        bundle.putParcelable("countries", listAddressParcel);
                        if (paymentType.equalsIgnoreCase(SUBSCRIPTION)) {
                            ApiHelper.doGetSubscriptionList(MainActivity.this, mProgressDialog, false).subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Subscriber<List<SubscriptionList>>() {
                                        @Override
                                        public void onCompleted() {
                                            mProgressDialog.dismiss();
                                            CallAndTextSubscriptionPaymentFragment callAndTextSubscriptionPaymentFragment = new CallAndTextSubscriptionPaymentFragment();
                                            callAndTextSubscriptionPaymentFragment.setArguments(bundle);
                                            changeFragment(callAndTextSubscriptionPaymentFragment, false);
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onNext(List<SubscriptionList> subscriptionList) {
                                            Parcelable listSubscriptionParcel = Parcels.wrap(subscriptionList);
                                            bundle.putParcelable(SUBSCRIPTION, listSubscriptionParcel);
                                        }
                                    });
                        } else {
                            ApiHelper.doGetIddList(MainActivity.this, mProgressDialog).subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Subscriber<List<IddList>>() {
                                        @Override
                                        public void onCompleted() {
                                            mProgressDialog.dismiss();
                                            TopUpIDDPaymentFragment topUpIDDPaymentFragment = new TopUpIDDPaymentFragment();
                                            topUpIDDPaymentFragment.setArguments(bundle);
                                            changeFragment(topUpIDDPaymentFragment, false);
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onNext(List<IddList> subscriptionList) {
                                            Parcelable listIddParcel = Parcels.wrap(subscriptionList);
                                            bundle.putParcelable(TOP_UP, listIddParcel);
                                        }
                                    });
                        }

                    }
                });
    }

    @Override
    public void onInviteCallConnected() {

    }

    @Override
    public void onInviteCallEnded() {

    }

    @Override
    public void onBluetoothStatusChange(boolean status) {

    }

    @Override
    public void onUpdateBadgeCount() {

        /**Missed Call Badge*/
        int missedCallBadgeCount = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .getMissedCallBadgeCount();
        View callTab = tabLayout.getTabAt(1).getCustomView();
        txtMissedCallBadge = (TextView) callTab.findViewById(R.id.txtBadgeCount);
        if (missedCallBadgeCount > 0) {
            Log.e(TAG, "onUpdateBadgeCount: " + missedCallBadgeCount);
            txtMissedCallBadge.setVisibility(View.VISIBLE);
            txtMissedCallBadge.setText(String.valueOf(missedCallBadgeCount));
        } else {
            txtMissedCallBadge.setVisibility(View.GONE);
        }

        /**SMS Badge*/
        int smsBadgeCount = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .getSMSBadgeCount();
        View smsTab = tabLayout.getTabAt(4).getCustomView();
        txtSmsBadge = (TextView) smsTab.findViewById(R.id.txtBadgeCount);
        if (smsBadgeCount > 0) {
            txtSmsBadge.setVisibility(View.VISIBLE);
            txtSmsBadge.setText(String.valueOf(smsBadgeCount));
        } else {
            txtSmsBadge.setVisibility(View.GONE);
        }

        txtMissedCallBadge = null;
        txtSmsBadge = null;

    }

    @Override
    public void onPortSipSendDMTFCallback(char number) {

    }
}
