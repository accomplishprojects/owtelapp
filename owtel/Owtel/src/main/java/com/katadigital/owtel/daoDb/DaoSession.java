package com.katadigital.owtel.daoDb;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig messageDaoConfig;
    private final DaoConfig areaCodeDaoConfig;

    private final MessageDao messageDao;
    private final AreaCodeDao areaCodeDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        messageDaoConfig = daoConfigMap.get(MessageDao.class).clone();
        messageDaoConfig.initIdentityScope(type);

        areaCodeDaoConfig = daoConfigMap.get(AreaCodeDao.class).clone();
        areaCodeDaoConfig.initIdentityScope(type);

        messageDao = new MessageDao(messageDaoConfig, this);
        areaCodeDao = new AreaCodeDao(areaCodeDaoConfig, this);

        registerDao(Message.class, messageDao);
        registerDao(AreaCode.class, areaCodeDao);
    }
    
    public void clear() {
        messageDaoConfig.getIdentityScope().clear();
        areaCodeDaoConfig.getIdentityScope().clear();
    }

    public MessageDao getMessageDao() {
        return messageDao;
    }

    public AreaCodeDao getAreaCodeDao() {
        return areaCodeDao;
    }

}
