package com.katadigital.owtel.modules.main.history;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.MyCallReceiver;
import com.katadigital.owtel.modules.main.contacts.adapters.LogsAdapter;
import com.katadigital.owtel.modules.main.contacts.entities.CallLogDto;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.modules.main.contacts.ContactsFragment;
import com.katadigital.owtel.modules.main.favorites.FavoritesFragment;
import com.katadigital.owtel.security.PermissionChecker;
import com.katadigital.owtel.modules.main.contacts.util.DateTimeHelper;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.portsip.utilities.converter.DataConverters;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

public class LogsFragment extends Fragment {
    private static final String TAG = "LogsFragment";

    private final int FILTER_ALL = 101,
            FILTER_INCOMING = 102,
            FILTER_OUTGOING = 103,
            FILTER_MISSED = 104;

    // callLogsList - filtered raw call logs list
    // callLogsListRaw - raw call logs list
    static ArrayList<CallLogDto> callLogList = new ArrayList<>();
    static ArrayList<CallLogDto> callLogListRaw = new ArrayList<>();
    //    static ListView listView;
    RecyclerView recyclerLogsList;
    static Button clearBtn;
    static Button editBtn;

    private View rootView;
    ToggleButton mToggleButtonAll, mToggleButtonMissed;
//    Button mButtonLogsAll;
//    Button mButtonLogsMissed;

    public static boolean allCallsIsActive = true;
    public static boolean isEditing = false;

    //    private EmptyLayout mEmptyLayout;
    QuickContactHelper quickContactHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ButterKnife.bind(getActivity());

        rootView = inflater.inflate(R.layout.fragment_logs, container, false);
        isEditing = false;
        ((MainActivity) getActivity()).currentTab = 1;
//        initActionbar();
        editBtn = (Button) rootView.findViewById(R.id.logs_edit_button);
        clearBtn = (Button) rootView.findViewById(R.id.logs_clear_btn);
        clearBtn.setVisibility(View.INVISIBLE);
        FavoritesFragment.isEditing = false;

        mToggleButtonAll = (ToggleButton) rootView.findViewById(R.id.tb_logs_all);
        mToggleButtonMissed = (ToggleButton) rootView.findViewById(R.id.tb_logs_missed);
        mToggleButtonAll.setChecked(true);
        mToggleButtonAll.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.i(TAG, "ToggleButton All " + isChecked);
            if (isChecked) {
                mToggleButtonAll.setTextColor(getResources().getColor(R.color.colorPrimary));
                if (isEditing) editBtn.performClick();
                Log.i(TAG, "Logs ALL clicked!");
                mToggleButtonMissed.setChecked(false);
                mToggleButtonAll.setClickable(false);
                mToggleButtonMissed.setClickable(false);
                new loadCallDetailsAsync(FILTER_ALL).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                allCallsIsActive = true;
            } else mToggleButtonAll.setTextColor(getResources().getColor(R.color.white));
        });

        mToggleButtonMissed.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.i(TAG, "ToggleButton Missed " + isChecked);
            if (isChecked) {
                mToggleButtonMissed.setTextColor(getResources().getColor(R.color.colorPrimary));
                if (isEditing) editBtn.performClick();
                Log.i(TAG, "Logs MISSED clicked!");
                mToggleButtonAll.setChecked(false);
                mToggleButtonAll.setClickable(false);
                mToggleButtonMissed.setClickable(false);
                new loadCallDetailsAsync(FILTER_MISSED).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                allCallsIsActive = false;
                MyCallReceiver.missedCount = 0;
                ((MainActivityBridge) getActivity()).setBadgeZero(getActivity(), true);
            } else mToggleButtonMissed.setTextColor(getResources().getColor(R.color.white));
        });

        quickContactHelper = new QuickContactHelper(getActivity());

//        mButtonLogsAll = (Button) rootView.findViewById(R.id.btn_logs_all);
//        mButtonLogsMissed = (Button) rootView.findViewById(R.id.btn_logs_missed);

//        mButtonLogsAll.setTypeface(NewUtil.getFontBold(getActivity()));
//        mButtonLogsMissed.setTypeface(NewUtil.getFontBold(getActivity()));
//
//        mButtonLogsAll.setTextSize(NewUtil.gettxtSize());
//        mButtonLogsMissed.setTextSize(NewUtil.gettxtSize());

//        editBtn.setTypeface(NewUtil.getFontRoman(getActivity()));
//        editBtn.setTextSize(NewUtil.gettxtSize());
//
//        clearBtn.setTypeface(NewUtil.getFontRoman(getActivity()));
//        clearBtn.setTextSize(NewUtil.gettxtSize());

        Var.logFragActivity = getActivity();
        Var.logsFragment = LogsFragment.this;

        /**Setting up Recycler view Logs List*/
        recyclerLogsList = (RecyclerView) rootView.findViewById(R.id.logsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerLogsList.setLayoutManager(mLayoutManager);
        recyclerLogsList.setItemAnimator(new DefaultItemAnimator());
        recyclerLogsList.setHasFixedSize(true);
        recyclerLogsList.setItemViewCacheSize(20);
        recyclerLogsList.setDrawingCacheEnabled(true);
        recyclerLogsList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

//        mEmptyLayout = new EmptyLayout(getActivity(), listView);

//        mButtonLogsAll.setOnClickListener(this);
//        mButtonLogsMissed.setOnClickListener(this);

        ((MainActivity) getActivity()).logsFragment = LogsFragment.this;

        clearBtn.setOnClickListener(v -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
            adb.setTitle(getResources().getString(R.string.alert_call_logs));
            adb.setIcon(R.drawable.warning);
            adb.setMessage(getResources().getString(R.string.alert_deleall_call_logs));
            adb.setNegativeButton(getResources().getString(R.string.string_no), null);
            adb.setPositiveButton(getResources().getString(R.string.string_yes),
                    (arg0, arg1) -> {
                        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG);
                        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_CALL_LOG}, PermissionChecker.REQUEST_PERMISSION_CALL_LOG);
                            return;
                        }
//                                new PermissionChecker(getActivity(), getActivity()).checkMultiplePermission();
                        if (allCallsIsActive) {
                            getActivity().getContentResolver().delete(CallLog.Calls.CONTENT_URI, null, null);
                            editBtn.performClick();
                            refreshLogs();
                        } else {
                            int a = CallLog.Calls.MISSED_TYPE;
                            String mis = String.valueOf(a);
                            getActivity().getContentResolver().delete(CallLog.Calls.CONTENT_URI,
                                    CallLog.Calls.TYPE + " = ? ", new String[]{mis});
                            editBtn.performClick();
                            refreshLogs();
                        }
                    });
            adb.show();
        });

        editBtn.setOnClickListener(view -> {
            if (editBtn.getText().equals(getResources().getString(R.string.edit_string))) {
                isEditing = true;
                if (allCallsIsActive) {
                    sortDefault(callLogListRaw);
                } else {
                    sortMissed(callLogListRaw);
                }
                editBtn.setText(getResources().getString(R.string.done_string));
                clearBtn.setVisibility(View.VISIBLE);

            } else {
                isEditing = false;
                refreshLogs();
                editBtn.setText(getResources().getString(R.string.edit_string));
                clearBtn.setVisibility(View.INVISIBLE);
            }
        });
        return rootView;
    }

    // FIXME: 2/14/17 Optimize
    private void getCallDetails() {
        callLogList = new ArrayList<>();
        callLogListRaw = new ArrayList<>();
        String sortOrder = CallLog.Calls.DATE + " COLLATE LOCALIZED DESC";
        ContentResolver cr = getActivity().getContentResolver();

        Cursor managedCursor = cr.query(CallLog.Calls.CONTENT_URI, null, null, null, sortOrder);
//        Cursor managedCursor = PermissionChecker.getCallLogs(getActivity(), CallLog.Calls.CONTENT_URI, null, null, null, sortOrder);

        if (managedCursor == null) {
            return;
        }
//        try {
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int id = managedCursor.getColumnIndex(CallLog.Calls._ID);
        int numberType = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NUMBER_TYPE);
        int contactName = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);

        if (managedCursor.moveToFirst()) {
            do {
                CallLogDto callLogDto = new CallLogDto();
//                    try {
                String phNumber = managedCursor.getString(number);
                String callType = managedCursor.getString(type);
                String callDate = managedCursor.getString(date);
                String dur = managedCursor.getString(duration);
                String number_type = managedCursor.getString(numberType);
                String callID = managedCursor.getString(id);
                String contact_name = "";
                Date callDayTime = new Date(Long.valueOf(callDate));

                String dir = null;
                int dirCode = Integer.parseInt(callType);
                switch (dirCode) {
                    case CallLog.Calls.INCOMING_TYPE:
                        dir = "INCOMING";
                        break;

                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = "OUTGOING";
                        break;

                    case CallLog.Calls.MISSED_TYPE:
                        dir = "MISSED";
                        break;
                }

                if (dir != null && phNumber != null && !phNumber.isEmpty()) {
                    if (!phNumber.equalsIgnoreCase("-1")) {
                        if (contact_name.equals("null")) {
                            callLogDto.setContactName(phNumber);
                        } else {
                            callLogDto.setContactName(contact_name);
                        }
                        callLogDto.setCallLogID(callID);
                        callLogDto.setDuration(dur);
                        callLogDto.setCallType(dir);
                        callLogDto.setCallDate(callDayTime);
                        callLogDto.setNumber(phNumber);
                    } else {
                        callLogDto.setContactName(getResources().getString(R.string.string_unknown));
                        callLogDto.setCallType(dir);
                        callLogDto.setCallLogID(callID);
                        callLogDto.setDuration(dur);
                        callLogDto.setCallDate(callDayTime);
                        callLogDto.setNumber(phNumber);
                    }

                    callLogList.add(callLogDto);
                    callLogListRaw.add(callLogDto);
                }

            } while (managedCursor.moveToNext());
        }
        managedCursor.close();
    }

    class loadCallDetailsAsync extends AsyncTask<String, String, String> {
        private int filter;

        /**
         * Async Task load call logs data to ArrayList and
         * ListView adapter.
         *
         * @param filter int
         */
        public loadCallDetailsAsync(int filter) {
            this.filter = filter;
        }

        @Override
        protected void onPreExecute() {
            callLogList = new ArrayList<>();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            if (new PermissionChecker(getActivity()).getCallLogPermission(PermissionChecker.REQUEST_PERMISSION_CALL_LOG))
                getCallDetails();
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (callLogList.size() <= 0) {
                editBtn.setVisibility(View.GONE);
            } else {
                editBtn.setVisibility(View.VISIBLE);
            }


            if (filter == FILTER_MISSED) {
                sortMissed(callLogListRaw);
                mToggleButtonAll.setClickable(true);
            } else {
                sortDefault(callLogListRaw);
                mToggleButtonMissed.setClickable(true);
            }

            super.onPostExecute(result);
        }
    }

    @Override
    public void onResume() {
        if (!allCallsIsActive)
            ((MainActivityBridge) getActivity()).setBadgeZero(getActivity(), true);
        try {
            ContactDetailActivity.searchValue = "";
            ContactsFragment.searchContacts.setText("");
        } catch (Exception e) {
            if (GlobalValues.DEBUG) Log.e(TAG, "onResume Contacts searchContacts " + e);
        }

        refreshLogs();
        super.onResume();
    }

    public void refreshLogs() {
        if (allCallsIsActive) {
            if (isEditing) {
                editBtn.performClick();
            }
            new loadCallDetailsAsync(FILTER_ALL).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            allCallsIsActive = true;

        } else {
            if (isEditing) editBtn.performClick();
            new loadCallDetailsAsync(FILTER_MISSED).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            allCallsIsActive = false;
        }
    }

    /**
     * Erick Lester sorting algorithm
     *
     * @param callLogList List<CallLogDto>
     */
    public void sortDefault(List<CallLogDto> callLogList) {
        reusableObserver(callLogList).subscribe(new Subscriber<List<CallLogDto>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<CallLogDto> callLogList) {
//                for (int i = 0; i < callLogList.size(); i++) {
//                    CallLogDto callHistory = callLogList.get(i);
//                    String contactName;
//                    if (callHistory.getContactName().isEmpty())
//                        contactName = callHistory.getNumber();
//                    else contactName = callHistory.getContactName();
//                    Log.e(TAG, callHistory.getCallDate() + " " + contactName + " (" + callHistory.getCount() + ")");
//                }

//                LogsAdapter adapter = new LogsAdapter(getActivity(), callLogList, callLogListRaw, LogsFragment.this, isEditing);
                CallLogsHistoryAdapter adapter = new CallLogsHistoryAdapter(Var.logFragActivity, callLogList, callLogListRaw, Var.logsFragment, isEditing);
                recyclerLogsList.setAdapter(adapter);

            }
        });
    }

    public void sortMissed(List<CallLogDto> callLogList) {
        reusableObserver(callLogList)
                .flatMap(callHistories -> Observable.from(callHistories))
                .filter(callHistory -> callHistory.getCallType().equals("MISSED"))
                .toList().subscribe(new Subscriber<List<CallLogDto>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<CallLogDto> callLogList) {
                CallLogsHistoryAdapter adapter = new CallLogsHistoryAdapter(Var.logFragActivity, callLogList, callLogListRaw, Var.logsFragment, isEditing);
                recyclerLogsList.setAdapter(adapter);
            }
        });
    }

    private Observable<List<CallLogDto>> reusableObserver(List<CallLogDto> callLogList) {
        return getObservableHistory(callLogList)
                .toSortedList(CallLogDto::compareUnixTime).map(this::optimizeCallHistory)
                .flatMap(Observable::from)
                .filter(callHistory -> !callHistory.isDuplicate())
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<CallLogDto> getObservableHistory(final List<CallLogDto> callLogList) {
        return Observable.create(new Observable.OnSubscribe<CallLogDto>() {
            @Override
            public void call(Subscriber<? super CallLogDto> subscriber) {
                for (CallLogDto callHistory : callLogList) {
                    subscriber.onNext(callHistory);
                }
                subscriber.onCompleted();
            }
        });
    }

    private List<CallLogDto> optimizeCallHistory(List<CallLogDto> histories) {
        for (int i = 0; i < histories.size(); i++) {
            CallLogDto callHistory = histories.get(i);

//            Log.e(TAG, "optimizeCallHistory: 1-" + callHistory.getNumber() + " " + i);
            if (callHistory.isDuplicate()) {
                continue;
            }


            HashMap<String, Object> map = getCountAndIdentifyDuplicates(histories, i);
            int count = (Integer) map.get("count");
            histories = (List<CallLogDto>) map.get("list");
            callHistory.setCount(count);
            histories.set(i, callHistory);
        }
        return histories;
    }

    private HashMap<String, Object> getCountAndIdentifyDuplicates(List<CallLogDto> histories, int position) {
        HashMap<String, Object> map = new HashMap<>();
        int counter = 1;
        CallLogDto curr = histories.get(position);
        int size = histories.size();
        for (int i = position + 1; i < size; i++) {
            CallLogDto nxt = histories.get(i);
            boolean isSameDay = DateTimeHelper.isSameDay(curr.getCallDate(), nxt.getCallDate());
            if (isSameDay && curr.getNumber().equals(nxt.getNumber())) {
                nxt.setDuplicate(true);
                histories.set(i, nxt);
                if (!allCallsIsActive) {
                    if (nxt.getCallType().equals("MISSED")) {
                        counter++;
                    }
                } else {
                    counter++;
                }
            } else if (!isSameDay) {
                break;
            }
        }
        map.put("list", histories);
        map.put("count", counter);

        return map;
    }
}
