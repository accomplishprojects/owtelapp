package com.katadigital.owtel.modules.main.contacts.callsmsblocker.callListener;

import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.katadigital.owtel.modules.main.contacts.callsmsblocker.callBlockerService.CallBlockerService;
import com.katadigital.owtel.modules.main.contacts.callsmsblocker.objects.BlockedContact;
import com.katadigital.owtel.helpers.Var;

public class DeviceStateListener extends PhoneStateListener {
    private ITelephony telephonyService;
    private Context context;

    public DeviceStateListener(Context context) {
        this.context = context;
        initializeTelephonyService();
    }

    private void initializeTelephonyService() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            Class clase = Class.forName(telephonyManager.getClass().getName());
            Method method = clase.getDeclaredMethod("getITelephony");
            method.setAccessible(true);
            telephonyService = (ITelephony) method.invoke(telephonyManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCallStateChanged(int state, final String incomingNumber) {
        switch (state) {

            case TelephonyManager.CALL_STATE_RINGING:

                final BlockedContact cn = CallBlockerService.blackList
                        .get(incomingNumber);

                // Thread t1 = new Thread(new Runnable() {
                // @Override
                // public void run() {
                //
                // Toast.makeText(context,
                // "Hi Lou BlockList number"+CallBlockerService.blackList
                // .get(incomingNumber), 1000).show();
                //
                // }
                // });
                // t1.start();

                if (cn != null && cn.isBlockedForCalling()) {
                    try {

                        Var.misscaltost = true;

                        telephonyService.endCall();

                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                DateFormat dateFormat = new SimpleDateFormat(
                                        "yyyy/MM/dd - HH:mm:ss");
                                Date date = new Date();
                                String currentDate = dateFormat.format(date);

                                // LOG FORMAT -->
                                // TITLE;;MESSAGE;;NAME;;NUMBER;;HOUR;;BODYMESSAGE(NULL);;SEPARATOR
                                final String message = "Call Blocked;;A call from "
                                        + cn.getName() + " (" + incomingNumber
                                        + ") was blocked at " + currentDate + ";;"
                                        + cn.getName() + ";;" + incomingNumber
                                        + ";;" + currentDate + ";;NULL;;\r\n";
                                writeInLog(message);

                                Var.activity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "Blocked Number " + incomingNumber, 1000)
                                                .show();
                                        Var.misscaltost = false;
                                    }
                                });

                            }
                        });
                        t.start();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public void writeInLog(String message) {
        try {
            OutputStreamWriter fos = new OutputStreamWriter(
                    context.openFileOutput("CallLog.txt", Context.MODE_APPEND));
            fos.append(message);
            fos.close();
            System.out.println("Writed in log succesfully");
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }

    }

}