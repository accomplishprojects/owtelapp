package com.katadigital.owtel.modules.main.contacts.entities;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class ContactDto {

    private String contactID = "";
    private String DisplayName = "";
    private String prefix = "";
    private String firstName = "";
    private String phonetic_fName = "";
    private String middleName = "";
    private String phonetic_middleName = "";
    private String lastName = "";
    private String phonetic_lName = "";
    private String suffix = "";
    private String nickname = "";
    private String jobTitle = "";
    private String jobDep = "";
    private String company = "";
    public ArrayList<AddressDto> addresses = new ArrayList<AddressDto>();
    public ArrayList<EmailDto> emails = new ArrayList<EmailDto>();
    public ArrayList<PhoneNumberDto> phoneNumbers = new ArrayList<PhoneNumberDto>();
    public ArrayList<WebsiteDto> websites = new ArrayList<WebsiteDto>();
    public ArrayList<DateDto> dateEvent = new ArrayList<DateDto>();
    public ArrayList<IMDto> imField = new ArrayList<IMDto>();
    public ArrayList<RelationDto> relation = new ArrayList<RelationDto>();
    private Bitmap profilepic = null;
    private String birthday = "";
    private String note = "";
    private String ringtone;

    private String accountName = "";
    private String accountType = "";

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public ArrayList<AddressDto> getAddresses() {
        return addresses;
    }

    public AddressDto getAddressWithID(int id) {
        AddressDto address = new AddressDto();

        for (AddressDto addressDto : addresses) {
            if (addressDto.getId() == id) {
                address = addressDto;
            }
        }

        return address;
    }

    public void addAddresses(AddressDto address) {
        addresses.add(address);
    }

    public Bitmap getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(Bitmap profilepic) {
        this.profilepic = profilepic;
    }

    public void setAddresses(ArrayList<AddressDto> addresses) {
        this.addresses = addresses;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    // Edit Jeff Phone

    public void setPhoneNumbers(ArrayList<PhoneNumberDto> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public ArrayList<PhoneNumberDto> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void addPhoneNumbers(PhoneNumberDto phonenumber) {
        phoneNumbers.add(phonenumber);
    }

    // Edit Jeff Email

    public void setEmails(ArrayList<EmailDto> emails) {
        this.emails = emails;
    }

    public ArrayList<EmailDto> getEmails() {
        return emails;
    }

    public void addEmails(EmailDto email) {
        emails.add(email);
    }

    public EmailDto getEmailWithID(int id) {
        EmailDto email = new EmailDto();

        for (EmailDto emailDto : emails) {
            if (emailDto.getId() == id) {
                email = emailDto;
            }
        }
        return email;
    }

	/* PREFIX */

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

	/* PHONETIC NAME */

    public String getPhonetic_fName() {
        return phonetic_fName;
    }

    public void setPhonetic_fName(String phonetic_fName) {
        this.phonetic_fName = phonetic_fName;
    }

	/* MIDDLE NAME */

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

	/* PHONETIC MIDDLE NAME */

    public String getPhonetic_middleName() {
        return phonetic_middleName;
    }

    public void setPhonetic_middleName(String phonetic_middleName) {
        this.phonetic_middleName = phonetic_middleName;
    }

	/* PHONETIC LAST NAME */

    public String getPhonetic_lName() {
        return phonetic_lName;
    }

    public void setPhonetic_lName(String phonetic_lName) {
        this.phonetic_lName = phonetic_lName;
    }

	/* SUFFIX */

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

	/* NICKNAME */

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

	/* JOBTITLE */

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

	/* JOB DEPARMENT */

    public String getJobDep() {
        return jobDep;
    }

    public void setJobDep(String jobDep) {
        this.jobDep = jobDep;
    }

	/* WEBSITE */

    public ArrayList<WebsiteDto> getWebsites() {
        return websites;
    }

    public void setWebsites(ArrayList<WebsiteDto> websites) {
        this.websites = websites;
    }

    public void addWebsite(WebsiteDto website) {
        websites.add(website);
    }

    public WebsiteDto getWebsiteWithID(int id) {
        WebsiteDto website = new WebsiteDto();

        for (WebsiteDto websiteDto : websites) {
            if (websiteDto.getId() == id) {
                website = websiteDto;
            }
        }
        return website;
    }

	/* DATES OR EVENTS */

    public ArrayList<DateDto> getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(ArrayList<DateDto> dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void addDateEvent(DateDto dateDto) {
        dateEvent.add(dateDto);
    }

    public DateDto getDateEventWithID(int id) {
        DateDto date = new DateDto();

        for (DateDto dateDto : dateEvent) {
            if (dateDto.getId() == id) {
                date = dateDto;
            }
        }
        return date;
    }

	/* Relation */

    public ArrayList<RelationDto> getRelationDto() {
        return relation;
    }

    public void setRelationDto(ArrayList<RelationDto> relation) {
        this.relation = relation;
    }

    public void addRelation(RelationDto relationDto) {
        relation.add(relationDto);
    }

    public RelationDto getRelationDtoWithID(int id) {
        RelationDto date = new RelationDto();

        for (RelationDto dateDto : relation) {
            if (dateDto.getId() == id) {
                date = dateDto;
            }
        }
        return date;
    }

    public String getRingtone() {
        return ringtone;
    }

    public void setRingtone(String ringtoneString) {
        this.ringtone = ringtoneString;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public ArrayList<IMDto> getImField() {
        return imField;
    }

    public void setImField(ArrayList<IMDto> imField) {
        this.imField = imField;
    }

    public void addIM(IMDto im) {
        imField.add(im);
    }

    public IMDto getIMFieldWithID(int id) {
        IMDto date = new IMDto();

        for (IMDto dateDto : imField) {
            if (dateDto.getId() == id) {
                date = dateDto;
            }
        }
        return date;
    }

}
