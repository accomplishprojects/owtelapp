//package com.katadigital.owtel.modules.main.messaging.service;
//
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.PixelFormat;
//import android.os.IBinder;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.Chronometer;
//import android.widget.ImageView;
//
//import com.kata.phone.R;
//
//import org.sipdroid.sipua.ui.InCallScreen;
//
///**
// * Created by Jose Mari Lumanlan on 5/31/16.
// * Chat Head Service
// */
//public class ChatHeadService extends Service {
//    private static final String TAG = "ChatHeadService";
//
//    private WindowManager windowManager;
//    private static ImageView chatHead;
//    private static Chronometer chronometer;
//    View v;
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        // Not used
//        return null;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
//        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
//
////        chatHead = new ImageView(this);
////        chatHead.setImageResource(R.mipmap.ic_launcher);
//
////        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
////                WindowManager.LayoutParams.WRAP_CONTENT,
////                WindowManager.LayoutParams.WRAP_CONTENT,
////                WindowManager.LayoutParams.TYPE_PHONE,
////                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
////                PixelFormat.TRANSLUCENT);
//
//        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
//                WindowManager.LayoutParams.WRAP_CONTENT,
//                WindowManager.LayoutParams.WRAP_CONTENT,
//                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
//                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
//                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
//                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
//                PixelFormat.TRANSLUCENT);
//
//        params.gravity = Gravity.TOP | Gravity.LEFT;
//        params.x = 0;
//        params.y = 100;
//
//        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        v = inflater.inflate(R.layout.call_head_view, null);
//
////        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
////                //WindowManager.LayoutParams.TYPE_INPUT_METHOD |
////                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,// | WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
////                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
////                PixelFormat.TRANSLUCENT);
//
////        params.gravity = Gravity.RIGHT | Gravity.TOP;
//
//        chatHead = (ImageView) v.findViewById(R.id.imgUserContact);
//
//        chronometer = (Chronometer) v.findViewById(R.id.chronometer_chat_head);
//        if (InCallScreen.mChronometerDuration != null)
//            startChronometer(InCallScreen.mChronometerDuration);
//
////        chatHead.setOnTouchListener(new View.OnTouchListener() {
////            private int initialX;
////            private int initialY;
////            private float initialTouchX;
////            private float initialTouchY;
////
////            @Override public boolean onTouch(View v, MotionEvent event) {
////                switch (event.getAction()) {
////                    case MotionEvent.ACTION_DOWN:
////                        initialX = params.x;
////                        initialY = params.y;
////                        initialTouchX = event.getRawX();
////                        initialTouchY = event.getRawY();
////                        return true;
////                    case MotionEvent.ACTION_UP:
////                        return true;
////                    case MotionEvent.ACTION_MOVE:
////                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
////                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
////                        windowManager.updateViewLayout(chatHead, params);
////                        return true;
////                }
////                return false;
////            }
////        });
//
//        v.setOnClickListener(v -> {
//            stopService(new Intent(getApplicationContext(), ChatHeadService.class));
//            chronometer.stop();
//            Intent resultIntent = new Intent(getApplicationContext(), InCallScreen.class);
//            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            getApplicationContext().startActivity(resultIntent);
//        });
//
//        windowManager.addView(v, params);
//
//    }
//
//    /**
//     * Update Chat Head Contact picture
//     *
//     * @param bitmap Bitmap
//     */
//    public static void updateChatHeadContactImage(Context context, Bitmap bitmap) {
//        try {
//            chatHead.setImageBitmap(bitmap);
//        } catch (Exception e) {
//            Log.e(TAG, "Update Chat Head img " + e);
//        }
//    }
//
//    /**
//     * Public statically accessible update Chat Head Chronometer
//     * Start chronometer if Receiver call state changes from DIALING to ACTIVE
//     */
//    public static void updateChatHeadChronometer() {
//        if (InCallScreen.mChronometerDuration != null)
//            startChronometer(InCallScreen.mChronometerDuration);
//    }
//
//    private static void startChronometer(Chronometer chronometer) {
//        chronometer.setBase(chronometer.getBase());
//        chronometer.start();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (v != null) windowManager.removeView(v);
//    }
//}