package com.katadigital.owtel.modules.registration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.models.LoginObj;
import com.katadigital.owtel.modules.login.LoginActivity;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.SharedPreferenceHelper;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.modules.main.promo.PromoCodeFragment;
import com.securepreferences.SecurePreferences;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by Omar Matthew Reyes on 4/1/16.
 * <p>
 * Final page of User Registration
 * Displays email, password, and promo code
 */
public class RegisterEndFragment extends Fragment {
    private final static String TAG = "RegisterEndFragment";
    @Bind(R.id.tv_register_end_user_email)
    TextView tvUserEmail;
    @Bind(R.id.tv_register_end_user_number)
    TextView tvUserNumber;
    @Bind(R.id.btn_register_end_line_share)
    Button btnLineShare;
    private boolean doShare = false;
    FragmentHolderActivity fragmentHolderActivity;
    private Bundle bundlePrev;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_register_end, container, false);
        ButterKnife.bind(this, view);

        bundlePrev = getArguments();

        fragmentHolderActivity = (FragmentHolderActivity) getActivity();
        fragmentHolderActivity.mProgressDialog.dismiss();

        tvUserEmail.setText(bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL));
        Log.d(TAG, "onCreateView: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_AREA_CODE));
        Log.d(TAG, "onCreateView1: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_NUMBER));
        tvUserNumber.setText(StringFormatter.formatUsNumber(bundlePrev.getString(Constants.BUNDLE_REGISTER_AREA_CODE), bundlePrev.getString(Constants.BUNDLE_REGISTER_NUMBER)));

        btnLineShare.setOnClickListener(v -> {
            doShare = true;
            Bundle args = new Bundle();
            args.putString(Constants.BUNDLE_REGISTER_EMAIL, bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL));
            args.putString(Constants.BUNDLE_REGISTER_PASSWORD, bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD));
            args.putBoolean(Constants.BUNDLE_PROMO_CODE_FROM_REGISTER, true);
            fragmentHolderActivity.changeFragment(new PromoCodeFragment(), args);
        });
//        btnLineShareSkip.setOnClickListener(v -> {
//            Intent intent = new Intent(getActivity(), MainActivity.class);
////            Bundle args = new Bundle();
////            args.putString(Constants.BUNDLE_REGISTER_EMAIL, bundle.getString(Constants.BUNDLE_REGISTER_EMAIL));
////            args.putString(Constants.BUNDLE_REGISTER_PASSWORD, bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD));
////            args.putString(Constants.BUNDLE_REGISTER_AREA_CODE, bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE));
////            args.putString(Constants.BUNDLE_REGISTER_NUMBER, bundle.getString(Constants.BUNDLE_REGISTER_NUMBER));
////            args.putString(Constants.BUNDLE_REGISTER_PROMO_CODE, bundle.getString(Constants.BUNDLE_REGISTER_PROMO_CODE));
////            startActivity(intent, args);
//            new ApiHelper().doLoginRequest(getActivity(),
//                    bundle.getString(Constants.BUNDLE_REGISTER_EMAIL),
//                    bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD),
//                    new ProgressDialog(getActivity()));
//        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!doShare) {
            final String email = bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL);
            final String pass = bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD);
            new ApiHelper(getActivity()).doLoginRequest(fragmentHolderActivity,
                    email, pass);
        }
    }
}
