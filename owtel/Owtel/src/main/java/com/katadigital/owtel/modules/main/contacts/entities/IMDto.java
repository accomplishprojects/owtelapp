package com.katadigital.owtel.modules.main.contacts.entities;

public class IMDto {

    private int id;
    private String imProtocol;
    private String im;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImProtocol() {
        return imProtocol;
    }

    public void setImProtocol(String imProtocol) {
        this.imProtocol = imProtocol;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

}
