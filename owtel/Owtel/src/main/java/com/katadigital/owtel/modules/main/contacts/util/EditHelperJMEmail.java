package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.EmailDto;

public class EditHelperJMEmail {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    int fieldEmail_ID = 0;

    ArrayList<Button> btnEmailspinner = new ArrayList<Button>();
    boolean custom = false;

    public EditHelperJMEmail(EditContactsActivity activity,
                             EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;
        this.fieldEmail_ID = editHelp.fieldEmail_ID;
        this.btnEmailspinner = editHelp.btnEmailspinner;
    }

    public ContactDto createLoadedEmailField(EmailDto emailDto,
                                             final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.email_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setId(fieldEmail_ID);
        spinnerbtn.setId(fieldEmail_ID);
        btnDelete.setId(fieldEmail_ID);
        editTxt.setId(fieldEmail_ID);

        editTxt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnEmailspinner.add(spinnerbtn);
        editTxt.setFocusableInTouchMode(false);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        editTxt.setFocusableInTouchMode(true);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.emailspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        int selecteditem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(emailDto.getEmailType())) {
                custom = false;
                selecteditem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(emailDto.getEmailType())) {
                custom = true;
                btnEmailspinner.get(spinnerbtn.getId()).setText(
                        emailDto.getEmailType());
                btnEmailspinner.get(spinnerbtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnEmailspinner.get(spinnerbtn.getId()).setText(
                    myResArray[selecteditem]);
        } else {
            btnEmailspinner.get(spinnerbtn.getId()).setText(
                    emailDto.getEmailType());
        }

        btnEmailspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivityCustomLoader(activity,
                                btnEmailspinner, spinnerbtn,
                                R.array.emailspinner, custom, true);

                    }
                });

        btnEmailspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnEmailspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.emails.get(spinnerbtn.getId()).setEmailType(
                                txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        editTxt.setFocusableInTouchMode(true);
                        break;
                }
                return false;
            }
        });

        // Get Text
        editTxt.setText(emailDto.getEmail());

        editTxt.setHint(activity.getResources().getString(
                R.string.string_hit_email));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.emails.get(editTxt.getId()).setEmail(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.emails.get(editTxt.getId()).setEmail("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDto.emails.remove(btnDelete.getId());
                contactDto.emails.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnEmailspinner.get(spinnerbtn.getId()).getText()
                .toString();

        emailDto.setEmail(editTxt.getText().toString());
        emailDto.setEmailType(txtbtn);

        contactDto.addEmails(emailDto);
        holderfield.addView(lLayoutPanel);
        fieldEmail_ID++;
        return contactDto;

    }

    public void addEmailFunction(final ContactDto contactDto) {
        EmailDto emailDto = new EmailDto();

        final LinearLayout holderfield = (LinearLayout) activity.findViewById(R.id.email_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel.inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel.findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel.findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel.findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel.findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel.findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setId(fieldEmail_ID);
        spinnerbtn.setId(fieldEmail_ID);
        btnDelete.setId(fieldEmail_ID);
        editTxt.setId(fieldEmail_ID);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnEmailspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.emailspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnEmailspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnEmailspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnEmailspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnEmailspinner.get(spinnerbtn.getId()).setTag(
                    myResArray.length - 1);
        }

        btnEmailspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnEmailspinner,
                                spinnerbtn, R.array.emailspinner);
                    }
                });

        btnEmailspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnEmailspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.emails.get(spinnerbtn.getId()).setEmailType(
                                txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setHint(activity.getResources().getString(
                R.string.string_hit_email));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.emails.get(editTxt.getId()).setEmail(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.emails.get(editTxt.getId()).setEmail("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDto.emails.remove(btnDelete.getId());
                contactDto.emails.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnEmailspinner.get(spinnerbtn.getId()).getText()
                .toString();

        int tag = (Integer) btnEmailspinner.get(spinnerbtn.getId()).getTag();

        emailDto.setId(spinnerbtn.getId());
        emailDto.setEmail(editTxt.getText().toString());
        emailDto.setEmailType(txtbtn);
        contactDto.addEmails(emailDto);

        holderfield.addView(lLayoutPanel);
        fieldEmail_ID++;

    }

}
