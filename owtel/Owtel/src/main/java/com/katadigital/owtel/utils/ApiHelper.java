package com.katadigital.owtel.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.models.IddCreditsObj;
import com.katadigital.owtel.models.SessionObj;
import com.katadigital.owtel.models.UserSession;
import com.katadigital.owtel.models.ValidateCallObject;
import com.katadigital.owtel.models.ZipCodeCityState;
import com.katadigital.owtel.modules.login.LoginActivity;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.portsip.helper.Line;
import com.katadigital.portsip.utilities.network.NetworkHelper;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.katadigital.portsip.views.PortSipCallScreenActivity;
import com.katadigital.ui.recyclerview.SettingsRecyclerViewAdapter;
import com.portsip.PortSipSdk;
import com.securepreferences.SecurePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.Subscriber;

import static android.content.Context.MODE_PRIVATE;

//import org.sipdroid.sipua.ui.Receiver;


/**
 * Created by Omar Matthew Reyes on 4/7/16.
 * API Handlers that can be accessed globally
 * Modified b
 */
public class ApiHelper {
    private static final String TAG = "ApiHelper";
    private Context context;

    /**
     * PortSipSDK Variables
     */
    OwtelAppController app;
    PortSipSdk mPortSipSdk;

    public ApiHelper(Context context) {
        this.context = context;

        /**Initialize notfication helper class*/
        NotificationHelper.init(context);
        NetworkHelper.init();
        SharedPreferenceManager.init(context);

        /**Initializing PortSipSDK*/
        app = ((OwtelAppController) context.getApplicationContext());
        mPortSipSdk = app.getPortSipSdk();
    }

    /**
     * Initiate Login Request
     * <p>
     * Success:
     * Prompt to MainActivity.class
     * Save credentials using <a href="https://github.com/scottyab/secure-preferences/">SecurePreferences</a>
     * <p>
     * Fail:
     * Show dialog
     *
     * @param email    String
     * @param password String
     */
    public void doLoginRequest(Activity activity, String email, String password) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "doLoginRequest email: " + email + " pass: " + password);
                ProgressDialog mProgressDialog = new ProgressDialog(activity);
                mProgressDialog.setMessage(activity.getString(R.string.logging_in));
                mProgressDialog.show();

                StringRequest mStringRequestLogin = new StringRequest(
                        Request.Method.POST, API.URL_TEST + API.LOGIN,
                        response -> {
                            if (GlobalValues.JC_debug) {
                                Toast.makeText(activity, "doLoginRequest" + response, Toast.LENGTH_SHORT).show();
                            }
                            try {
                                Log.e(TAG, "doLoginRequest: " + response);
                                JSONObject jsonObjectMain = new JSONObject(response);
                                String jsonObjectStatus = jsonObjectMain.getString("status");

                                if (jsonObjectStatus.equals("Success")) {
                                    String jsonObjectUserExpiration = jsonObjectMain.getString("expiration_date");
                                    String jsonObjectUserNumber = jsonObjectMain.getString("sip_number");
                                    String jsonObjectUserSipPassword = jsonObjectMain.getString("sip_password");
                                    String jsonObjectUserSipServer = jsonObjectMain.getString("sip_server");
                                    String jsonObjectUserSipPort = jsonObjectMain.getString("sip_port");
//                            String jsonObjectUserSipProtocol = jsonObjectMain.getString("sip_protocol");

                                    String jsonObjectRadId = jsonObjectMain.getString("rad_session_id");
                                    String jsonObjectPromoCode = jsonObjectMain.getString("promo_code");

                                    // Encrypted Preference storage
                                    String key = email + password;
                                    SecurePreferences securePreferences = OwtelAppController.getInstance()
                                            .getUserPinBasedSharedPreferences(key);
                                    SecurePreferences.Editor secureEditor = securePreferences.edit();
                                    secureEditor.putString(Constants.PREFS_USER_NUMBER, jsonObjectUserNumber);
                                    secureEditor.putString(Constants.PREFS_SIP_PASS, jsonObjectUserSipPassword);
                                    secureEditor.putString(Constants.PREFS_SIP_SERVER, jsonObjectUserSipServer);
                                    secureEditor.putString(Constants.PREFS_SIP_PORT, jsonObjectUserSipPort);
                                    secureEditor.putString(Constants.PREFS_RAD_ID, jsonObjectRadId);
                                    secureEditor.apply();

                                    SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_RAD_NAME, MODE_PRIVATE).edit();
                                    editor.putString(Constants.PREFS_RAD_ID, jsonObjectRadId);
                                    editor.commit();

                                    SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
                                    SharedPreferences.Editor defaultPreferenceEditor = preferencesUser.edit();
                                    defaultPreferenceEditor.putString(Constants.PREFS_USER_EMAIL, email);
                                    defaultPreferenceEditor.putString(Constants.PREFS_USER_PASS, password);
                                    defaultPreferenceEditor.putString(Constants.PREFS_PROMO_CODE, jsonObjectPromoCode);
                                    defaultPreferenceEditor.putBoolean(Constants.PREFS_USER_TYPE, jsonObjectPromoCode.equals(""));
                                    defaultPreferenceEditor.putString(Constants.PREFS_USER_SUBSCRIPTION_EXP, jsonObjectUserExpiration);
                                    defaultPreferenceEditor.apply();

                                    if (GlobalValues.DEBUG)
                                        if (!key.isEmpty()) {
                                            Log.i(TAG, "Stored Secured Number " + securePreferences.getString(Constants.PREFS_USER_NUMBER, "empty number"));
                                            Log.i(TAG, "Stored Secured Password " + securePreferences.getString(Constants.PREFS_SIP_PASS, "empty pass"));
                                            Log.i(TAG, "Stored Secured SIP Server " + securePreferences.getString(Constants.PREFS_SIP_SERVER, "empty SIP Server"));
                                            Log.i(TAG, "Stored Secured SIP Port " + securePreferences.getString(Constants.PREFS_SIP_PORT, "empty SIP port"));
                                            Log.i(TAG, "Stored Secured Rad ID " + securePreferences.getString(Constants.PREFS_RAD_ID, "empty Rad ID"));
                                        } else Log.e(TAG, "SecurePreferences is EMPTY");

                                    SharedPreferenceManager.getSharedPreferenceManagerInstance().putRadId(jsonObjectRadId);
                                    SharedPreferenceManager.getSharedPreferenceManagerInstance().putOwtelNumber(jsonObjectUserNumber);
                                    SharedPreferenceManager.getSharedPreferenceManagerInstance().putPortSipStatus(true);
                                    SharedPreferenceManager.getSharedPreferenceManagerInstance().putIDDCredits(jsonObjectMain.getString("idd_credits"));


                                    mProgressDialog.dismiss();
                                    Intent mIntent = new Intent(activity, MainActivity.class);
                                    activity.startActivity(mIntent);
                                    activity.finish();
                                } else {
                                    mProgressDialog.dismiss();
                                    String jsonObjectMessage = jsonObjectMain.getString("message");
                                    DialogBuilder.showAlertDialog(activity,
                                            jsonObjectStatus, jsonObjectMessage,
                                            activity.getString(R.string.string_ok));
                                }
                            } catch (JSONException e) {
                                mProgressDialog.dismiss();
                                if (GlobalValues.DEBUG) Log.e(TAG, "Volley " + e);
                                Toast.makeText(activity, activity.getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                                Log.e(TAG, "doLoginRequest: " + response);
                            }
                        },

                        error -> {

                            if (GlobalValues.DEBUG)
                                Log.e(TAG, "That didn't work! Error: doLoginRequest" + error);
                            mProgressDialog.dismiss();
                            Toast.makeText(activity, activity.getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                            Log.e(TAG, "on Error: " + error);

                        }) {

                    @Override
                    protected Map<String, String> getParams() {
                        final Map<String, String> params = new HashMap<>();
                        params.put(API.REGISTER_EMAIL, email);
                        params.put(API.REGISTER_PASSWORD, password);
                        return params;
                    }
                };

                mStringRequestLogin.setRetryPolicy(new DefaultRetryPolicy(
                        GlobalValues.REQUEST_TIMEOUT,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                mStringRequestLogin.setTag(Constants.REQUEST_TAG_SAVE_USER);
                OwtelAppController.getInstance().addToRequestQueue(mStringRequestLogin);
            }
        }, GlobalValues.LOGIN_DELAY_TIME);


    }

    /**
     * Get Subscription price list
     *
     * @param context         Context
     * @param mProgressDialog ProgressDialog
     * @param isPromoUser     boolean
     * @return List<SubscriptionList>
     */
    public static Observable<List<SubscriptionList>> doGetSubscriptionList(Context context, ProgressDialog mProgressDialog, boolean isPromoUser) {
        List<SubscriptionList> listSubscription = new ArrayList<>();
        return Observable.create(new Observable.OnSubscribe<List<SubscriptionList>>() {
            @Override
            public void call(Subscriber<? super List<SubscriptionList>> subscriber) {
                try {
                    StringRequest mStringRequestSubscriptionPrices = new StringRequest(Request.Method.POST, API.URL_TEST + API.SUBSCRIPTION_LIST,
                            response -> {
                                if (GlobalValues.DEBUG)
                                    Log.i(TAG, "Subscription Prices response " + response);
                                try {
                                    JSONObject jsonObjectMain = new JSONObject(response);
                                    JSONArray jsonObjectSubscriptionList = jsonObjectMain.getJSONArray("subscription_list");
                                    for (int i = 0; i < jsonObjectSubscriptionList.length(); i++) {
                                        SubscriptionList subscription = new SubscriptionList();
                                        String subscriptionId = jsonObjectSubscriptionList.getJSONObject(i).getString("subscription_id");
                                        String description = jsonObjectSubscriptionList.getJSONObject(i).getString("description");
                                        Double price = Double.parseDouble(jsonObjectSubscriptionList.getJSONObject(i).getString("price"));
                                        subscription.setSubscriptionId(subscriptionId);
                                        subscription.setSubscriptionName(description);
                                        // 1 Year Subscription is FREE for Promo Code users
                                        if (isPromoUser) {
                                            if (i == 0) price = 0.00;
                                            subscription.setSubscriptionPrice(StringFormatter.formatCurrency(Constants.CURRENCY, price));
                                            subscription.setSubscriptionValue(price);
                                        } else {
                                            subscription.setSubscriptionValue(price);
                                            subscription.setSubscriptionPrice(StringFormatter.formatCurrency(Constants.CURRENCY, price));
                                        }
                                        listSubscription.add(subscription);
                                    }

                                    subscriber.onNext(listSubscription);
                                    subscriber.onCompleted();
                                } catch (Exception e) {
                                    if (GlobalValues.DEBUG)
                                        Log.e(TAG, "Volley doGetSubscriptionList " + e);
                                    Toast.makeText(context, context.getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                                    Log.e(TAG, "doGetSubscriptionList: " + response);
                                }
                            }, error -> {
                        if (GlobalValues.DEBUG)
                            Log.e(TAG, "That didn't work! Error: doGetSubscriptionList" + error);
                        mProgressDialog.dismiss();
                        DialogBuilder.displayVolleyError(context, error);
                    });
                    mStringRequestSubscriptionPrices.setTag(Constants.REQUEST_TAG_GET_SUBSCRIPTION_LIST);
                    OwtelAppController.getInstance().addToRequestQueue(mStringRequestSubscriptionPrices);
                } catch (Exception e) {
                    Log.e(TAG, "doGetSubscriptionList " + e);
                }
            }
        });
    }

    /**
     * Get IDD top-up price list
     *
     * @param context         Context
     * @param mProgressDialog ProgressDialog
     * @return List<IddList>
     */
    // FIXME: 11/28/16 convert to retrofit
    public static Observable<List<IddList>> doGetIddList(Context context, ProgressDialog mProgressDialog) {
        List<IddList> listIdd = new ArrayList<>();
        return Observable.create(new Observable.OnSubscribe<List<IddList>>() {
            @Override
            public void call(Subscriber<? super List<IddList>> subscriber) {
                StringRequest mStringRequestIddPrices = new StringRequest(Request.Method.POST, API.URL_TEST + API.IDD_LIST,
                        response -> {
                            if (GlobalValues.DEBUG) Log.i(TAG, "IDD Prices response " + response);
                            try {
                                JSONObject jsonObjectMain = new JSONObject(response);
                                JSONArray jsonObjectSubscriptionList = jsonObjectMain.getJSONArray("idd_list");

                                for (int i = 0; i < jsonObjectSubscriptionList.length(); i++) {
                                    IddList idd = new IddList();
                                    String iddId = jsonObjectSubscriptionList.getJSONObject(i).getString("idd_id");
                                    String description = jsonObjectSubscriptionList.getJSONObject(i).getString("description");
                                    Double price = Double.parseDouble(jsonObjectSubscriptionList.getJSONObject(i).getString("price"));
                                    idd.setIddId(iddId);
                                    idd.setIddReloadName(description);
                                    idd.setIddReloadPrice(StringFormatter.formatCurrency(Constants.CURRENCY, price));
                                    idd.setIddReloadValue(price);
                                    listIdd.add(idd);
                                }
                            } catch (Exception e) {
                                if (GlobalValues.DEBUG) Log.e(TAG, "Volley doGetIddList " + e);
                                Toast.makeText(context, context.getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                                Log.e(TAG, "doGetIddList: " + response);
                            }
                            subscriber.onNext(listIdd);
                            subscriber.onCompleted();
                        }, error -> {
                    if (GlobalValues.DEBUG)
                        Log.e(TAG, "That didn't work! Error: doGetIddList" + error);
                    mProgressDialog.dismiss();
                    DialogBuilder.displayVolleyError(context, error);
                });
                mStringRequestIddPrices.setTag(Constants.REQUEST_TAG_GET_IDD_LIST);
                OwtelAppController.getInstance().addToRequestQueue(mStringRequestIddPrices);
            }
        });
    }

    public static Observable<List<AddressList>> doGetAddressList(ProgressDialog mProgressDialog, Activity activity, int type, String stateId, String countryId) {
        Context context = OwtelAppController.getInstance();
        List<AddressList> countryList = new ArrayList<>(), stateList = new ArrayList<>(), cityList = new ArrayList<>();
        return Observable.create(new Observable.OnSubscribe<List<AddressList>>() {
            @Override
            public void call(Subscriber<? super List<AddressList>> subscriber) {
                StringRequest mStringRequestAddressList = new StringRequest(Request.Method.POST, API.URL_TEST + API.ADDRESS_LIST,
                        response -> {
                            Log.i(TAG, "Address List response " + response);
                            try {
                                JSONObject jsonObjectMain = new JSONObject(response);
                                String jsonObjectStatus = jsonObjectMain.getString("status");
                                if (jsonObjectStatus.equals("Success")) {
                                    JSONArray jsonObjectAddressList = jsonObjectMain.getJSONArray("address_list");
                                    AddressList addressDefault = new AddressList();
                                    // Set default value at 0
                                    switch (type) {
                                        case GlobalValues.GET_COUNTRY:
                                            addressDefault.setCountry(context.getString(R.string.country));
                                            addressDefault.setCountryId("");
                                            countryList.add(addressDefault);
                                            break;
                                        case GlobalValues.GET_STATE:
                                            addressDefault.setState(context.getString(R.string.state));
                                            addressDefault.setStateId("");
                                            stateList.add(addressDefault);
                                            break;
                                        case GlobalValues.GET_CITY:
                                            addressDefault.setCity(context.getString(R.string.city));
                                            addressDefault.setCityId("");
                                            cityList.add(addressDefault);
                                            break;
                                    }
                                    for (int i = 0; i < jsonObjectAddressList.length(); i++) {
                                        AddressList address = new AddressList();
                                        String jsonObjectCountryId, jsonObjectStateId, jsonObjectCityId,
                                                jsonObjectCountryName, jsonObjectStateName, jsonObjectCityName;
                                        try {
                                            switch (type) {
                                                case GlobalValues.GET_COUNTRY:
                                                    jsonObjectCountryName = jsonObjectAddressList.getJSONObject(i).getString("country_name");
                                                    jsonObjectCountryId = jsonObjectAddressList.getJSONObject(i).getString("country_id");
                                                    address.setCountry(jsonObjectCountryName);
                                                    address.setCountryId(jsonObjectCountryId);
                                                    countryList.add(address);
                                                    break;
                                                case GlobalValues.GET_STATE:
                                                    jsonObjectStateName = jsonObjectAddressList.getJSONObject(i).getString("state_name");
                                                    jsonObjectStateId = jsonObjectAddressList.getJSONObject(i).getString("state_id");
                                                    address.setState(jsonObjectStateName);
                                                    address.setStateId(jsonObjectStateId);
                                                    stateList.add(address);
                                                    break;
                                                case GlobalValues.GET_CITY:
                                                    jsonObjectCityName = jsonObjectAddressList.getJSONObject(i).getString("city_name");
                                                    jsonObjectCityId = jsonObjectAddressList.getJSONObject(i).getString("city_id");
                                                    address.setCity(jsonObjectCityName);
                                                    address.setCityId(jsonObjectCityId);
                                                    cityList.add(address);
                                                    break;
                                            }
                                        } catch (JSONException e) {
                                            Log.e(TAG, "doGetAddressList " + e);
                                        }
                                    }
                                } else {
                                    mProgressDialog.dismiss();
                                    String jsonObjectMessage = jsonObjectMain.getString("message");
                                    DialogBuilder.showAlertDialog(activity,
                                            jsonObjectStatus, jsonObjectMessage,
                                            context.getResources().getString(R.string.string_ok));
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "doGetAddressList " + e);
                            }

                            switch (type) {
                                case GlobalValues.GET_COUNTRY:
                                    subscriber.onNext(countryList);
                                    break;
                                case GlobalValues.GET_STATE:
                                    subscriber.onNext(stateList);
                                    break;
                                case GlobalValues.GET_CITY:
                                    subscriber.onNext(cityList);
                                    break;
                            }
                            subscriber.onCompleted();
                        }, error -> {
                    if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
                    mProgressDialog.dismiss();
                    DialogBuilder.displayVolleyError(context, error);
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        final Map<String, String> params = new HashMap<>();
                        params.put(API.REGISTER_COUNTRY_ID, countryId);
                        params.put(API.REGISTER_STATE_ID, stateId);
                        return params;
                    }
                };
                mStringRequestAddressList.setTag(Constants.REQUEST_TAG_GET_ADDRESS_LIST);
                OwtelAppController.getInstance().addToRequestQueue(mStringRequestAddressList);
            }
        });
    }


    /**
     * Check User Radius Session using Volley
     */
    public void checkRadSession() {
        String strRadId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getRadId();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API.URL_TEST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        service.checkSession(strRadId).enqueue(new Callback<SessionObj>() {
            @Override
            public void onResponse(Response<SessionObj> response, Retrofit retrofit) {

                if (response.body().getStatus().equals("Success")) {

                    /**Session is false*/
                    if (!response.body().getLogged().equals("true")) {
                        Log.e(TAG, "onResponse: LoggedStatus: False," + "Session is expired");
                        doLogout();
                    } else {
                        /**Session is true*/
                        context.sendBroadcast(new Intent(Constants.PORTSIP_REGISTER_ON_RECEIVER));
                    }
                } else {
                    doLogout();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }


    /**
     * Update user IDD credits in SharedPreferences
     * API param needed is String email - fetched from stored SharedPreference
     */
    public static void updateUserIddCredits(FragmentActivity activity, String[] settingsDataSet, RecyclerView recyclerViewSettings) {
        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API.URL_TEST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        ApiService service = retrofit.create(ApiService.class);
        service.getIDDCredits(email).enqueue(new Callback<IddCreditsObj>() {
            @Override
            public void onResponse(Response<IddCreditsObj> response, Retrofit retrofit) {
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    SharedPreferences.Editor defaultPreferenceEditor = preferencesUser.edit();
                    SharedPreferenceManager.getSharedPreferenceManagerInstance().putIDDCredits(response.body().getIddCredits());
//                    defaultPreferenceEditor.putFloat(Constants.PREFS_USER_IDD_CREDITS,
//                            (float) Double.parseDouble(response.body().getIddCredits().replaceAll("[^\\d.-]", "")));
//                    SharedPreferenceHelper.putDouble(defaultPreferenceEditor, Constants.PREFS_USER_IDD_CREDITS,
//                            Double.parseDouble(response.body().getIddCredits().replaceAll("[^\\d.-]", "")));
                    defaultPreferenceEditor.apply();
                    SettingsRecyclerViewAdapter mAdapter = new SettingsRecyclerViewAdapter(settingsDataSet, activity);
                    recyclerViewSettings.setAdapter(mAdapter);

                } else {
                    DialogBuilder.showAlertDialog(OwtelAppController.getInstance(),
                            response.body().getStatus(), response.body().getMessage(),
                            OwtelAppController.getInstance().getString(R.string.string_ok));
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

//    /**
//     * Update user IDD credits in SharedPreferences
//     * API param needed is String email - fetched from stored SharedPreference
//     */
//    // FIXME: 11/28/16 Convert to RetroFit
//    public static void updateUserIddCredits() {
//        if (GlobalValues.DEBUG) Log.i(TAG, "updateUserIddCredits");
//        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
//        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");
//        StringRequest mStringRequestUserIddCredits = new StringRequest(Request.Method.POST, API.URL_TEST + API.IDD_CREDITS, response -> {
//            try {
//                if (GlobalValues.DEBUG) Log.i(TAG, "updateUserIddCredits response " + response);
//                JSONObject jsonObjectMain = new JSONObject(response);
//                String jsonObjectStatus = jsonObjectMain.getString("status");
//                if (jsonObjectStatus.equals("Success")) {
//                    double jsonObjectUserIddCredits = jsonObjectMain.getDouble("idd_credits");
//                    SharedPreferences.Editor defaultPreferenceEditor = preferencesUser.edit();
//                    defaultPreferenceEditor.putFloat(Constants.PREFS_USER_IDD_CREDITS, (float) jsonObjectUserIddCredits);
//                    SharedPreferenceHelper.putDouble(defaultPreferenceEditor, Constants.PREFS_USER_IDD_CREDITS, jsonObjectUserIddCredits);
//                    defaultPreferenceEditor.apply();
//
//                    if (GlobalValues.DEBUG)
//                        Log.i(TAG, "updateUserIddCredits user credits: " + jsonObjectUserIddCredits);
//                } else {
//                    String jsonObjectMessage = jsonObjectMain.getString("message");
//                    DialogBuilder.showAlertDialog(OwtelAppController.getInstance(),
//                            jsonObjectStatus, jsonObjectMessage,
//                            OwtelAppController.getInstance().getString(R.string.string_ok));
//                }
//            } catch (JSONException e) {
//                Log.e(TAG, "updateUserIddCredits " + e);
//            }
//        }, error -> {
//            if (GlobalValues.DEBUG)
//                Log.e(TAG, "That didn't work! Error: updateUserIddCredits" + error);
//            DialogBuilder.displayVolleyError(OwtelAppController.getInstance(), error);
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                final Map<String, String> params = new HashMap<>();
//                params.put(API.REGISTER_EMAIL, email);
//                return params;
//            }
//        };
//        mStringRequestUserIddCredits.setRetryPolicy(new DefaultRetryPolicy(
//                GlobalValues.REQUEST_TIMEOUT,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mStringRequestUserIddCredits.setTag(Constants.REQUEST_TAG_FORGOT_PASS);
//        OwtelAppController.getInstance().addToRequestQueue(mStringRequestUserIddCredits);
//    }

    /**
     * Activate Web CS created user
     *
     * @param email    String
     * @param number   String
     * @param password String
     * @return Observable
     */
    public static Observable<Boolean> registerUserWebCS(String email, String number, String password) {
        return Observable.create((Observable.OnSubscribe<Boolean>) subscriber -> {
            StringRequest mStringRequestActivateUserWebCS = new StringRequest(Request.Method.POST, API.URL_TEST + API.ACTIVATE_USER, response -> {
                try {
                    if (GlobalValues.DEBUG) Log.i(TAG, "registerUserWebCS response " + response);
                    JSONObject jsonObjectMain = new JSONObject(response);
                    String jsonObjectStatus = jsonObjectMain.getString("status");
                    if (jsonObjectStatus.equals("Success")) {
                        subscriber.onNext(true);
                        subscriber.onCompleted();
                    } else {
                        String jsonObjectMessage = jsonObjectMain.getString("message");
                        DialogBuilder.showAlertDialog(OwtelAppController.getInstance(),
                                jsonObjectStatus, jsonObjectMessage,
                                OwtelAppController.getInstance().getString(R.string.string_ok));
                        subscriber.onNext(false);
                        subscriber.onCompleted();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "registerUserWebCS " + e);
                }
            }, error -> {
                if (GlobalValues.DEBUG)
                    Log.e(TAG, "That didn't work! Error: registerUserWebCS" + error);
                DialogBuilder.displayVolleyError(OwtelAppController.getInstance(), error);
            }) {
                @Override
                protected Map<String, String> getParams() {
                    final Map<String, String> params = new HashMap<>();
                    params.put(API.REGISTER_EMAIL, email);
                    params.put(API.REGISTER_NUMBER, number);
                    params.put(API.REGISTER_PASSWORD, password);
                    return params;
                }
            };
            mStringRequestActivateUserWebCS.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalValues.REQUEST_TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mStringRequestActivateUserWebCS.setTag(Constants.REQUEST_TAG_FORGOT_PASS);
            OwtelAppController.getInstance().addToRequestQueue(mStringRequestActivateUserWebCS);
        });
    }

    /**
     * <p>Do logout function
     * <p>Clears the following:
     * <ul>
     * <li>SharedPreference (User data)</li>
     * <li>SecurePreference (Server configs)</li>
     * <li>Settings</ul></p>
     * <p>Stops Services:
     * <ul>
     * <li>SIP Engine</li>
     * <li>Web Socket Receiver</li>
     * </ul></p>
     */
    public void doLogout() {
        String strRadId = SharedPreferenceManager.getSharedPreferenceManagerInstance().getRadId();
        Log.e(TAG, "RAD_ID doLogout " + strRadId);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(API.URL_TEST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        service.checkSession(strRadId).enqueue(new Callback<SessionObj>() {
            @Override
            public void onResponse(Response<SessionObj> response, Retrofit retrofit) {
                Log.e(TAG, "onResponse: LogoutStatus: " + response.body().getStatus());
                stopServices();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, "onFailure: LogoutStatus: " + t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /**
     * This is the method for Un-registering the user into PortSipSDK
     */
    private void unregisteringFromPortSipSDK() {
        Log.e(TAG, "stopServices: " + "Unregister user from PortSipSdk");
        Line[] mLines = app.getLines();
        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
            if (mLines[i].getRecvCallState()) {
                mPortSipSdk.rejectCall(mLines[i].getSessionId(), 486);
            } else if (mLines[i].getSessionState()) {
                mPortSipSdk.hangUp(mLines[i].getSessionId());
            }
            mLines[i].reset();
        }
        app.setLoginState(false);
        mPortSipSdk.unRegisterServer();
        mPortSipSdk.DeleteCallManager();
        NotificationHelper.getNotificationHelperInstance()
                .unnotifyPortSipStatus();
        SharedPreferenceManager.getSharedPreferenceManagerInstance().putPortSipStatus(false);

    }

    /**
     * Stop running services
     */
    public void stopServices() {
        MainActivityBridge mMainActivityBridge = (MainActivityBridge) context;

        /**Clear SharedPreferences username and password*/
        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        preferencesUser.edit().clear().apply();

        /**Stop web socket for messaging*/
        OwtelAppController.getInstance().stopWebSocketService();

        /**Unregister user from PortSipSdk*/
        unregisteringFromPortSipSDK();

        mMainActivityBridge.finishActivity();
        context.startActivity(new Intent(context, LoginActivity.class).setAction(""));
    }

    /**
     * Update user session
     *
     * @param userSession     UserSession
     * @param preferencesUser SharedPreferences
     * @param securedKey      String
     * @param email           String
     * @param pass            String
     */
    public void updateUserSession(UserSession userSession, SharedPreferences preferencesUser,
                                  String securedKey, String email, String pass) {
        SecurePreferences securePreferences = OwtelAppController.getInstance()
                .getUserPinBasedSharedPreferences(securedKey);
        SecurePreferences.Editor secureEditor = securePreferences.edit();
        secureEditor.putString(Constants.PREFS_USER_NUMBER, userSession.getUserSipNumber());
        secureEditor.putString(Constants.PREFS_SIP_PASS, userSession.getUserSipPassword());
        secureEditor.putString(Constants.PREFS_SIP_SERVER, userSession.getUserSipServer());
        secureEditor.putString(Constants.PREFS_SIP_PORT, userSession.getUserSipPort());
        secureEditor.putString(Constants.PREFS_RAD_ID, userSession.getUserRadId());

        // FIXME: 2/23/17 MY FIXES TESTER
        SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREFS_RAD_NAME, MODE_PRIVATE).edit();
        editor.putString(Constants.PREFS_RAD_ID, userSession.getUserRadId());
        editor.commit();

        Log.i(TAG, "RAD_ID updateUserSession " + userSession.getUserRadId());
        secureEditor.apply();

        SharedPreferences.Editor defaultPreferenceEditor = preferencesUser.edit();
        defaultPreferenceEditor.putString(Constants.PREFS_USER_EMAIL, email);
        defaultPreferenceEditor.putString(Constants.PREFS_USER_PASS, pass);
        defaultPreferenceEditor.putString(Constants.PREFS_PROMO_CODE, userSession.getUserPromoCode());
        defaultPreferenceEditor.putBoolean(Constants.PREFS_USER_TYPE, userSession.getUserPromoCode().equals(""));
        defaultPreferenceEditor.putString(Constants.PREFS_USER_SUBSCRIPTION_EXP, userSession.getUserExpDate());
        defaultPreferenceEditor.putFloat(Constants.PREFS_USER_IDD_CREDITS, (float) userSession.getUserIddCredits());
        SharedPreferenceHelper.putDouble(defaultPreferenceEditor, Constants.PREFS_USER_IDD_CREDITS, userSession.getUserIddCredits());
        defaultPreferenceEditor.apply();
    }

    /**
     * Check if app should allow user to call or send message
     *
     * @param 'number String
     * @return boolean
     */
    public boolean proceedSendNumber() {
//        return !isSubscriptionExpired() && (isUsSipNumber(number) || checkUserSufficientCredits());
        return !isSubscriptionExpired() || checkUserSufficientCredits();
    }

    /**
     * Check if number is US for unlimited call/text
     *
     * @param number String
     * @return boolean
     */
//    public boolean isUsSipNumber(String number) {
//        // TODO API to check if US or IDD number
//        return number.length() > 0;
//    }
    public void doCallCheckingThenCall(Context context, String number) {
        ProgressDialogHelper.showDialog(context);

        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");

        Retrofit retrofit = new Retrofit.Builder().baseUrl(API.URL_TEST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        ApiService service = retrofit.create(ApiService.class);
        service.validateCall(email, number).enqueue(new Callback<ValidateCallObject>() {
            @Override
            public void onResponse(Response<ValidateCallObject> response, Retrofit retrofit) {
//                Log.d(TAG, "doCallCheckingThenCall: " + response.raw().toString());
//                Log.d(TAG, "doCallCheckingThenCall: " + email);
//                Log.d(TAG, "doCallCheckingThenCall: " + number);
////                Log.d(TAG, "doCallCheckingThenCall: "+response.body().getUsNumber());
////                Log.d(TAG, "doCallCheckingThenCall: checkUserSufficientCredits "+checkUserSufficientCredits());
                if (response.body().getStatus().equals("Success")) {
                    if ((response.body().getUsNumber() || checkUserSufficientCredits()) && !isSubscriptionExpired()) {
                        new ApiHelper(context).checkRadSession();
                        String formattedNum = number.replaceAll(" ", "");
                        formattedNum.replace("-", "");
//                        String formattedNum = number.replaceAll("\\s+", "");
                        // Check if app should allow user to call number
//                    if (new ApiHelper(context).proceedSendNumber()) {
                        if (formattedNum.length() == 0)
                            new AlertDialog.Builder(context)
                                    .setMessage(R.string.empty)
                                    .setTitle(R.string.app_name)
                                    .setCancelable(true)
                                    .show();
                        else {

                        }

                        /**Make Call with PortSipCallScreenActivity*/
                        String strOwnNumber = new Functions().getNumber();
                        Log.e(TAG, "onResponse: " + strOwnNumber + "-" + formattedNum.replace("-", ""));
                        if (strOwnNumber != null && !strOwnNumber.equals(formattedNum.replace("-", "").replace(" ", ""))) {
                            context.startActivity(new Intent(context, PortSipCallScreenActivity.class)
                                    .putExtra("NumberToCall", number)
                                    .putExtra("isIncomingCall", false)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
//                        {
//                            try {
//                                String callNumber = formattedNum.replaceAll("-","");
//                                Log.e(TAG, "+: "+callNumber);
////                                MainActivity.tJavaWrapper.callCreate(MainActivity.nUserId, callNumber, MainActivity.callId);
//
////                                Button btnCall = (Button)findViewById(R.id.buttonCall);
////                                btnCall.setEnabled(false);
////                                Button btnHangup = (Button)findViewById(R.id.buttonHangup);
////                                btnHangup.setEnabled(true);
//
//
//                            } catch (WrapperException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        if (!Receiver.engine(context).call(formattedNum, true))
//                            new AlertDialog.Builder(context)
//                                    .setMessage(R.string.notfast)
//                                    .setTitle(R.string.app_name)
//                                    .setCancelable(true)
//                                    .show();
                    } else {
                        try {
                            SimpleDialogFragment
                                    .createBuilder(context, ((AppCompatActivity) context)
                                            .getSupportFragmentManager())
                                    .setTitle(context.getString(R.string.error_credits_insufficient))
                                    .setMessage("Would you like to do the top-up now ?")
                                    .setPositiveButtonText(context.getString(R.string.ok))
                                    .setNegativeButtonText(context.getString(R.string.cancel))
                                    .setRequestCode(MainActivity.REQUEST_TOP_UP)
                                    .show();
                        } catch (NullPointerException e) {
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }
//                        Toast.makeText(context, context.getString(R.string.error_credits_insufficient), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                ProgressDialogHelper.hideDialog();
            }

            @Override
            public void onFailure(Throwable t) {
                ProgressDialogHelper.hideDialog();
            }
        });
    }

    /**
     * Check if user subscription is expired
     *
     * @return boolean
     */
    public boolean isSubscriptionExpired() {
        return new Date().getTime() >= new StringFormatter().getUtcTimeToUnixTime(OwtelAppController
                .getInstance().getDefaultSharedPreferences()
                .getString(Constants.PREFS_USER_SUBSCRIPTION_EXP, "empty"));
    }

    /**
     * Check if user still has sufficient credits
     *
     * @return boolean
     */
    public boolean checkUserSufficientCredits() {
//        return SharedPreferenceHelper.getDouble(OwtelAppController.getInstance().getDefaultSharedPreferences(), Constants.PREFS_USER_IDD_CREDITS, 0.0) > 0.0;
        String strIddCredits = SharedPreferenceManager.getSharedPreferenceManagerInstance().getIDDCredits();
        double total = Double.parseDouble(strIddCredits.replace(",", "").replace(" ", ""));
        Log.e(TAG, "checkUserSufficientCredits: " + total);
        return total > 0;
    }

    /**
     * Generate City and State automatically from Zip Code
     * if selected Country is "United States"
     *
     * @param etAddressZipCode      EditText
     * @param etAddressCity         EditText
     * @param etAddressState        EditText
     * @param spinnerAddressCountry Spinner
     * @param apiService            ApiService
     */
    public void generateCityState(EditText etAddressZipCode, EditText etAddressCity,
                                  EditText etAddressState, Spinner spinnerAddressCountry,
                                  List<AddressList> countryList, ApiService apiService) {

        etAddressZipCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editableZipCode) {

                Log.e(TAG, String.valueOf(editableZipCode.length()));

                if (editableZipCode.length() != 0 &&
                        countryList.get(spinnerAddressCountry
                                .getSelectedItemPosition())
                                .getCountry().equalsIgnoreCase("United States")) {
                    apiService.getCityState(etAddressZipCode.getText().toString()).enqueue(new Callback<ZipCodeCityState>() {
                        @Override
                        public void onResponse(Response<ZipCodeCityState> response, Retrofit retrofit) {
                            if (response.body().getStatus().equalsIgnoreCase("Success")) {
                                etAddressCity.setText(response.body().getCityName());
                                etAddressState.setText(response.body().getStateName());
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        spinnerAddressCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.i(TAG, "genCityState spinnerAddressCountry " + etAddressZipCode.getText() + " " + countryList.get(position).getCountry());
                if (etAddressZipCode.getText().length() != 0 &&
                        countryList.get(position).getCountry().equals("United States")) {
                    apiService.getCityState(etAddressZipCode.getText().toString()).enqueue(new Callback<ZipCodeCityState>() {
                        @Override
                        public void onResponse(Response<ZipCodeCityState> response, Retrofit retrofit) {
                            if (response.body().getStatus().equalsIgnoreCase("Success")) {
                                etAddressCity.setText(response.body().getCityName());
                                etAddressState.setText(response.body().getStateName());
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public static void countUnreadMessages(Context context, int badgeCount) {
        ShortcutBadger.applyCount(context, badgeCount); //for 1.1.4+
    }


    public static void updateBadge(Context context, int badgeCount) {
        ShortcutBadger.applyCount(context, badgeCount); //for 1.1.4+
    }
}
