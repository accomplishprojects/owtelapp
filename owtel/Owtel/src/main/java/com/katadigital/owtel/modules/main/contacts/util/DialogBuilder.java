package com.katadigital.owtel.modules.main.contacts.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.settings.SettingsFragment;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.ui.recyclerview.SettingsRecyclerViewAdapter;

/**
 * Created by Omar Matthew Reyes on 4/6/16.
 * Class containing various Dialog Builders
 */
public class DialogBuilder {
    /**
     * Invokes an AlertDialog popup on view.
     *
     * @param activity            Current activity
     * @param title               Title for the AlertDialog
     * @param msg                 Message content
     * @param buttonNegativeTitle Text on the negative button
     * @see android.app.AlertDialog
     */
    public static void showAlertDialog(Activity activity, String title, String msg, String buttonNegativeTitle) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    /**
     * Invokes an AlertDialog popup on view.
     *
     * @param context             Current activity
     * @param title               Title for the AlertDialog
     * @param msg                 Message content
     * @param buttonNegativeTitle Text on the negative button
     * @see android.app.AlertDialog
     */
    public static void showAlertDialog(Context context, String title, String msg, String buttonNegativeTitle) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(msg);
            alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
            });

            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }catch (Exception e)
        {
            try {
                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(title);
                alertDialogBuilder.setMessage(msg);
                alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
                });

                android.app.AlertDialog alert = alertDialogBuilder.create();
                alert.show();
            }catch (Exception e1)
            {
                Toast.makeText(context, ""+msg, Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }


//    public static void showAlertDialogNext(Context context, String title, String msg, String buttonNegativeTitle) {
//        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
//        alertDialogBuilder.setTitle(title);
//        alertDialogBuilder.setMessage(msg);
//        alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
//        });
//
//        android.app.AlertDialog alert = alertDialogBuilder.create();
//        alert.show();
//    }

    /**
     * Invokes an AlertDialog popup on view.
     *
     * @param context             Current activity
     * @param title               Title for the AlertDialog
     * @param msg                 Message content
     * @param buttonNegativeTitle Text on the negative button
     * @see android.app.AlertDialog
     */
    public static void showAlertDialog(Context context, String title, String msg, String buttonNegativeTitle, boolean cancelable, SettingsRecyclerViewAdapter.ServiceKiller serviceKiller) {

        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(msg);
            alertDialogBuilder.setCancelable(cancelable);
            alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
                serviceKiller.stopServices(context);

            });

            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }catch (Exception e)
        {        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(msg);
            alertDialogBuilder.setCancelable(cancelable);
            alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
                serviceKiller.stopServices(context);
            });

            android.app.AlertDialog alert = alertDialogBuilder.create();
            alert.show();}
    }

//    public static void showAlertDialogNext(Context context, String title, String msg, String buttonNegativeTitle, boolean cancelable, SettingsRecyclerViewAdapter.ServiceKiller serviceKiller) {
//        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
//        alertDialogBuilder.setTitle(title);
//        alertDialogBuilder.setMessage(msg);
//        alertDialogBuilder.setCancelable(cancelable);
//        alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
//            serviceKiller.stopServices(context);
//        });
//
//        android.app.AlertDialog alert = alertDialogBuilder.create();
//        alert.show();
//    }

    /**
     * Show pop-up Dialog from "More" button. Displays Settings and Logout
     *
     * @param context Context
     */
    public static void showDialogMore(Context context) {
        final String TAG = "Dialog More";
        Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);

        MainActivity mainActivity = (MainActivity) context;

        dialog.setTitle(null);
        dialog.setContentView(R.layout.layout_dialog_more);

        dialog.findViewById(R.id.layout_dialog_bg).setOnClickListener(v -> dialog.dismiss());
        dialog.findViewById(R.id.tv_more_settings).setOnClickListener(v -> {
            dialog.dismiss();
            if (GlobalValues.DEBUG) Log.i(TAG, "Settings Clicked");
            Bundle args = new Bundle();
            args.putBoolean(Constants.BUNDLE_HIDE_BTN_BACK, false);
            SettingsFragment settingsFragment = new SettingsFragment();
            settingsFragment.setArguments(args);
            mainActivity.changeFragment(settingsFragment, false);
        });
        dialog.findViewById(R.id.tv_more_logout).setOnClickListener(v -> {
            dialog.dismiss();
            if (GlobalValues.DEBUG) Log.i(TAG, "Logout Clicked");
            showAlertDialog(context,
                    context.getString(R.string.logout), context.getString(R.string.logout_dialog_notice),
                    context.getString(R.string.string_ok), true, context1 -> new ApiHelper(context).doLogout());
        });

        dialog.show();
    }

    /**
     * Display Volley error in a Toast
     *
     * @param context Context
     * @param error   VolleyError
     */
    public static void displayVolleyError(Context context, VolleyError error) {
        Toast.makeText(context, new DialogBuilder().getVolleyErrorMessage(context, error), Toast.LENGTH_SHORT).show();
    }

    /**
     * Get Volley Error message
     *
     * @param context Context
     * @param error   VolleyError
     * @return String
     */
    private String getVolleyErrorMessage(Context context, VolleyError error) {
        try {
            String errorMsg = context.getString(R.string.request_failed);
            if (error instanceof TimeoutError) {
                errorMsg = context.getString(R.string.error_timeout);
            } else if (error instanceof NoConnectionError) {
                errorMsg = context.getString(R.string.error_no_internet);
            } else if (error instanceof AuthFailureError) {
                errorMsg = context.getString(R.string.error_authentication);
            } else if (error instanceof ServerError) {
                errorMsg = context.getString(R.string.error_server_unreachable);
            } else if (error instanceof NetworkError) {
                errorMsg = context.getString(R.string.error_network);
            } else if (error instanceof ParseError) {
                errorMsg = context.getString(R.string.error_parse);
            }
            return errorMsg;
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return "Error Unknown! See logs";
        }
    }


}
