package com.katadigital.owtel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by josemari on 7/28/16.
 */


public class ValidateCallObject {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("us_number")
    @Expose
    private Boolean usNumber;
    @SerializedName("expiration_date")
    @Expose
    private String expirationDate;
    @SerializedName("subscription_status")
    @Expose
    private String subscriptionStatus;
    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The usNumber
     */
    public Boolean getUsNumber() {
        return usNumber;
    }

    /**
     *
     * @param usNumber
     * The us_number
     */
    public void setUsNumber(Boolean usNumber) {
        this.usNumber = usNumber;
    }

    /**
     *
     * @return
     * The expirationDate
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     *
     * @param expirationDate
     * The expiration_date
     */
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     *
     * @return
     * The subscriptionStatus
     */
    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    /**
     *
     * @param subscriptionStatus
     * The subscription_status
     */
    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

}
