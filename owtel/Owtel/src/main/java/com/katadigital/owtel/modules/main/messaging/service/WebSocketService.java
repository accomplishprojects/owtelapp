package com.katadigital.owtel.modules.main.messaging.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.Message;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.Logger;
import com.katadigital.owtel.utils.RxBus;
import com.katadigital.portsip.helper.Ring;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;


public class WebSocketService extends Service {

    public static final String TAG = "WebSocketService";

    private WebSocketConnection mWebSocketConnection;

    @Inject
    RxBus rxBus;

    private String wsUri;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OwtelAppController.getComponent(this).inject(this);
        NotificationHelper.init(getApplicationContext());
        mWebSocketConnection = WebSocket.getWebSocketConnection();
        wsUri = "ws://52.38.105.189:9990";
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.e(TAG, "onStartCommand");

        connectToWebSocket();
        return Service.START_STICKY;
    }

    private void connectToWebSocket() {
        try {
            mWebSocketConnection.connect(wsUri, new WebSocketHandler() {
                @Override
                public void onOpen() {
                    Logger.e(TAG, "Status: Connected to " + wsUri);
                }

                @Override
                public void onTextMessage(String payload) {
                    Logger.e(TAG, "Payload Logger " + payload);
                    try {

                        JSONObject chat = new JSONObject(payload);
                        if (chat.getString("receiver").equals(new Functions().getNumber()) ||
                                (chat.getString("sender").equals(new Functions().getNumber()) && chat.getString("receiver").equals(new Functions().getNumber()))) {
                            Message message = new Message(Long.parseLong(chat.getString("sms_id")));
                            message.setMessageID(chat.getString("sms_id"));
                            message.setRoomID(chat.getString("thread_id"));
                            message.setType("text");
                            message.setValue(chat.getString("sms_content"));
                            message.setCreated(chat.getString("sms_date"));
                            message.setUserID(chat.getString("receiver"));
                            message.setReceiver(chat.getString("receiver"));
                            message.setSender(chat.getString("sender"));
                            message.setIs_message_read("0");
                            message.setIs_deleted("0");

                            if (rxBus.hasObservers()) {
                                rxBus.send(message);
                            } else {
                                NotificationHelper.getNotificationHelperInstance()
                                        .notifyOnMessageReceived(getApplicationContext(), chat);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return;


                }

                @Override
                public void onClose(int code, String reason) {
                    Logger.e(TAG, "Connection lost. " + reason);

                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        // reconnect after 10 sec
                        connectToWebSocket();
                    }, 10000);
                }
            });
        } catch (WebSocketException e) {
            e.printStackTrace();
        }
    }

}
