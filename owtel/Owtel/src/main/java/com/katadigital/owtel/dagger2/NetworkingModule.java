package com.katadigital.owtel.dagger2;

import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.utils.API;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

@Module
public class NetworkingModule {

    private final OwtelAppController app;

    public NetworkingModule(OwtelAppController app){
        this.app = app;
    }


    @Singleton
    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }


    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor){
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.interceptors().add(httpLoggingInterceptor);
        return  httpClient;
    }

    @Singleton
    @Provides
    ApiService provideApiService(OkHttpClient httpClient){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.URL_TEST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient)
                .build();

        return retrofit.create(ApiService.class);
    }
}
