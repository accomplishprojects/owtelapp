package com.katadigital.owtel.security;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Omar Matthew Reyes on 3/9/16.
 * Permission Checker required for Android 5.0 and up
 */
public class PermissionChecker {
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 100;
    public static final int REQUEST_PERMISSION_CALL_LOG = 120;
    public static final int REQUEST_PERMISSION_CALL_LOG_RW = 121;
    public static final int REQUEST_PERMISSION_CONTACTS = 130;
    public static final int REQUEST_PERMISSION_CONTACTS_DIALER = 131;
    public static final int REQUEST_PERMISSION_WRITE_SETTINGS_CALL_SCREEN = 141;
    public static final int REQUEST_PERMISSION_READ_SMS = 142;
    public static final int REQUEST_PERMISSION_BLUETOOTH_ADMIN = 150;
    private final static String TAG = "PermissionChecker";

    private Activity activity;

    public PermissionChecker(Activity activity) {
        this.activity = activity;
    }

    public boolean isAllPermissionsAllowed() {
        return hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) &&
                hasPermission(Manifest.permission.READ_CONTACTS) &&
                hasPermission(Manifest.permission.WRITE_CONTACTS) &&
                hasPermission(Manifest.permission.READ_CALL_LOG) &&
                hasPermission(Manifest.permission.READ_SMS) &&
                hasPermission(Manifest.permission.WRITE_CALL_LOG) &&
                hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                hasPermission(Manifest.permission.CAMERA) &&
                hasPermission(Manifest.permission.WRITE_SETTINGS) &&
                hasPermission(Manifest.permission.RECORD_AUDIO) &&
                hasPermission(Manifest.permission.VIBRATE) &&
                hasPermission(Manifest.permission.WAKE_LOCK);
    }

    public void checkPermissions() {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add("Read Contacts");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CONTACTS))
            permissionsNeeded.add("Write Contacts");
        if (!addPermission(permissionsList, Manifest.permission.READ_CALL_LOG))
            permissionsNeeded.add("Read Call Logs");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CALL_LOG))
            permissionsNeeded.add("Write Call Logs");
        if (!addPermission(permissionsList, Manifest.permission.DISABLE_KEYGUARD))
            permissionsNeeded.add("Disable Keyguard");
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
            permissionsNeeded.add("Read SMS");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");
        if (!addPermission(permissionsList, Manifest.permission.VIBRATE))
            permissionsNeeded.add("Enable Vibration");
        if (!addPermission(permissionsList, Manifest.permission.WAKE_LOCK))
            permissionsNeeded.add("Wake Lock");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        (dialog, which) -> ActivityCompat.requestPermissions(activity,
                                permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS));
                return;
            }
            ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

//        insertDummyContact();
    }

    public void checkPermissions(final Activity activity, Class goToClass) {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add("Read Contacts");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CONTACTS))
            permissionsNeeded.add("Write Contacts");
        if (!addPermission(permissionsList, Manifest.permission.READ_CALL_LOG))
            permissionsNeeded.add("Read Call Logs");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CALL_LOG))
            permissionsNeeded.add("Write Call Logs");
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
            permissionsNeeded.add("Read SMS");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_SETTINGS))
            permissionsNeeded.add("Write Settings");
        if (!addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Record Audio");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        (dialog, which) -> ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS));
//                return;
            }
            ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
//            return;
        }
        if (isAllPermissionsAllowed()) activity.startActivity(new Intent(activity, goToClass));
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    public boolean hasPermission(String permission) {
        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public static Cursor getCallLogs(Activity activity, Uri uri, String[] projection,
                                     String selection, String[] selectionArgs, String sortOrder) {
        ContentResolver cr = activity.getContentResolver();
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CALL_LOG);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CALL_LOG}, PermissionChecker.REQUEST_PERMISSION_CALL_LOG);
            return cr.query(uri, projection, selection, selectionArgs, sortOrder);
        }
        return cr.query(uri, projection, selection, selectionArgs, sortOrder);
    }


    /**
     * Check if ACCESS_FINE_LOCATION and ACCESS_COARSE_LOCATION permissions are granted.
     *
     * @param mContext Context
     * @return boolean
     */
    public static boolean checkAccessLocationPermission(Context mContext) {
        String permissionFineLocation = "android.permission.ACCESS_FINE_LOCATION";
        String permissionCoarseLocation = "android.permission.ACCESS_COARSE_LOCATION";
        int resultFineLocation = mContext.checkCallingOrSelfPermission(permissionFineLocation);
        int resultCoarseLocation = mContext.checkCallingOrSelfPermission(permissionCoarseLocation);
        return (resultFineLocation == PackageManager.PERMISSION_GRANTED ||
                resultCoarseLocation == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Get security permission for Contacts
     */
    public boolean getContactsPermission(int permissionCode) {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, permissionCode);
            return false;
        }
        return true;
    }

    /**
     * Get security permission for CALL LOGS
     */
    public boolean getCallLogPermission(int permissionCode) {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CALL_LOG);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CALL_LOG}, permissionCode);
            return false;
        }
        return true;
    }

    public boolean getCallLogReadWritePermission(int permissionCode) {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.READ_CALL_LOG))
            permissionsNeeded.add("Read Call Logs");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CALL_LOG))
            permissionsNeeded.add("Write Call Logs");

        if (permissionsList.size() > 0 && permissionsNeeded.size() > 0) {
            String message = "You need to grant access to " + permissionsNeeded.get(0);
            for (int i = 1; i < permissionsNeeded.size(); i++)
                message = message + ", " + permissionsNeeded.get(i);
            showMessageOKCancel(message,
                    (dialog, which) -> ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS));
            return false;
        }
        return true;
    }

    /**
     * Get security permission for Write Settings
     */
    public boolean getWriteSettingsPermission(int permissionCode) {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_SETTINGS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_SETTINGS}, permissionCode);
            return false;
        }
        return true;
    }

    /**
     * Get Read SMS
     */
    public boolean getReadSMSPermission(int permissionCode) {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_SMS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_SMS}, permissionCode);
            return false;
        }
        return true;
    }

    /**
     * Get permission for Bluetooth and Bluetooth Admin
     *
     * @param permissionCode int
     * @return boolean
     */
    public boolean getBluetoothPermission(int permissionCode) {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.BLUETOOTH))
            permissionsNeeded.add("Bluetooth");
        if (!addPermission(permissionsList, Manifest.permission.BLUETOOTH_ADMIN))
            permissionsNeeded.add("Bluetooth Admin");

        if (permissionsList.size() > 0 && permissionsNeeded.size() > 0) {
            String message = "You need to grant access to " + permissionsNeeded.get(0);
            for (int i = 1; i < permissionsNeeded.size(); i++)
                message = message + ", " + permissionsNeeded.get(i);
            showMessageOKCancel(message,
                    (dialog, which) -> ActivityCompat.requestPermissions(activity,
                            permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS));
            return false;
        }
        return true;
    }
}
