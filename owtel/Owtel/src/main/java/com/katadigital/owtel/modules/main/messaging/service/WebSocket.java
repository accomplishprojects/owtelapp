package com.katadigital.owtel.modules.main.messaging.service;


import de.tavendo.autobahn.WebSocketConnection;

public class WebSocket {
    private static WebSocketConnection mInstance = null;
    public static WebSocketConnection getWebSocketConnection(){
        if(mInstance == null){
            mInstance = new WebSocketConnection();
        }
        return mInstance;
    }
}
