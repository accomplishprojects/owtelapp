package com.katadigital.owtel.modules.main.contacts;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.edmodo.cropper.CropImageView;
import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

public class CropperActivity extends AppCompatActivity {

    // Static final constants
    private static final int DEFAULT_ASPECT_RATIO_VALUES = 10;
    private static final int ROTATE_NINETY_DEGREES = 90;
    private static final String ASPECT_RATIO_X = "ASPECT_RATIO_X";
    private static final String ASPECT_RATIO_Y = "ASPECT_RATIO_Y";
    private static final int ON_TOUCH = 1;

    // Instance variables
    private int mAspectRatioX = DEFAULT_ASPECT_RATIO_VALUES;
    private int mAspectRatioY = DEFAULT_ASPECT_RATIO_VALUES;

    public static Bitmap selectedImage;
    CropImageView cropImageView;

    // Saves the state upon rotating the screen/restarting the activity
    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(ASPECT_RATIO_X, mAspectRatioX);
        bundle.putInt(ASPECT_RATIO_Y, mAspectRatioY);
    }

    // Restores the state upon rotating the screen/restarting the activity
    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        mAspectRatioX = bundle.getInt(ASPECT_RATIO_X);
        mAspectRatioY = bundle.getInt(ASPECT_RATIO_Y);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_cropper);

        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        // Sets initial aspect ratio to 10/10
        cropImageView.setAspectRatio(DEFAULT_ASPECT_RATIO_VALUES,
                DEFAULT_ASPECT_RATIO_VALUES);

        Intent intent = getIntent();

        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("imagepass");
        Uri tempUri = NewUtil.getImageUri(getApplicationContext(), bitmap);
        File finalFile = new File(NewUtil.getRealPathFromURI(tempUri, this));

        System.out.println("JM PATH " + finalFile.toString());

        FileInputStream in;
        BufferedInputStream buf;

        try {
            in = new FileInputStream("/storage/sdcard0/DCIM/Camera/1415274130768.jpg");
            buf = new BufferedInputStream(in);

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            Bitmap bMap = BitmapFactory.decodeStream(buf, null, bmOptions);

            int targetW = cropImageView.getWidth();
            int targetH = cropImageView.getHeight();


            cropImageView.setImageBitmap(bMap);
            if (in != null) {
                in.close();
            }
            if (buf != null) {
                buf.close();
            }
        } catch (Exception e) {
            Log.e("Error reading file", e.toString());
        }

        //
        // selectedImage = bitmap;
        //
        // if (selectedImage != null) {
        // cropImageView
        // .setImageBitmap(scaleBitmap(selectedImage,
        // selectedImage.getWidth() * 4,
        // selectedImage.getHeight() * 4));
        //
        // }

    }

    private void setPic(CropImageView cropImageView2, String mCurrentPhotoPath) {
        // Get the dimensions of the View
        int targetW = cropImageView2.getWidth();
        int targetH = cropImageView2.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        // cropImageView2.setImageBitmap(bitmap);
    }

    public void rotateImage(View view) {
        cropImageView.rotateImage(ROTATE_NINETY_DEGREES);
    }

    public void cropImage(View view) {
        selectedImage = cropImageView.getCroppedCircleImage();
        if (CropperActivity.selectedImage != null) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("resultimage", selectedImage);
            setResult(RESULT_OK, returnIntent);

            finish();
        } else {
            Toast.makeText(CropperActivity.this, "Please select an image!!",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Scales the provided bitmap to have the height and width provided.
     * (Alternative method for scaling bitmaps since
     * Bitmap.createScaledBitmap(...) produces bad (blocky) quality bitmaps.)
     *
     * @param bitmap    is the bitmap to scale.
     * @param newWidth  is the desired width of the scaled bitmap.
     * @param newHeight is the desired height of the scaled bitmap.
     * @return the scaled bitmap.
     */
    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight,
                Config.ARGB_8888);

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

}
