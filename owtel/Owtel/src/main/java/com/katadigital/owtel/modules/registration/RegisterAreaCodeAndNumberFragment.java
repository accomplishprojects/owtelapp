package com.katadigital.owtel.modules.registration;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.models.AreaCode;
import com.katadigital.owtel.models.UsNumberList;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.utils.StringFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by
 * John Erick Lester Sumugat on 4/1/16.
 * Omar Matthew Reyes on 4/1/16.
 * <p>
 * Second page of User registration
 * Displays Spinner containing Area Codes and RadioGroup containing US Numbers.
 * RadioGroup can only display three(3) US numbers per instance
 */
public class RegisterAreaCodeAndNumberFragment extends Fragment {

    public static final String TAG = "CodeNumberFragment";

    @Bind(R.id.spinner_area_code)
    Spinner spinnerAreaCode;

    @Bind(R.id.rg_number)
    RadioGroup radioGroupNumber;

    @Bind(R.id.ib_refresh_number)
    ImageButton ibRefreshNum;
    @Bind(R.id.tv_refresh_credits)
    TextView tvRefreshCredits;
    @Bind(R.id.btn_continue)
    Button btnContinue;

    FragmentHolderActivity fragmentHolderActivity;

    private String tempLockId = ""; // Used for locking US numbers on the backend
    private String selectedUsNumber;
    List<AreaCode> areaCodeList = new ArrayList<>();
    List<RadioButton> radioButtons;
    int refreshCredits = 3, indexCredits = 0; // credits

    private RequestQueue mRequestQueue;
    Bundle args = new Bundle();

    private boolean isPromoUser = false;

    private List<SubscriptionList> listSubscription = new ArrayList<>();

    @Override
    public void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(Constants.REQUEST_TAG_ASSIGN_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_register_area_code_and_number_chooser, container, false);

        ButterKnife.bind(this, view);
        radioGroupNumber.setOrientation(LinearLayout.VERTICAL);
        radioButtons = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            RadioButton radioButton = new RadioButton(getActivity());
            radioButtons.add(radioButton);
            radioGroupNumber.addView(radioButton);
        }

        Bundle bundlePrev = getArguments();

        // Instantiate the RequestQueue.
        mRequestQueue = Volley.newRequestQueue(getActivity());
        fragmentHolderActivity = (FragmentHolderActivity) getActivity();

        String refresh = getString(R.string.register_number_refresh_credits) + " " + refreshCredits;
        tvRefreshCredits.setText(refresh);

        isPromoUser = getArguments().getString(Constants.BUNDLE_REGISTER_PROMO_CODE, "").trim().length() > 0;

        return view;
    }

    @OnClick(R.id.btn_continue)
    public void onContinue() {
        if (radioGroupNumber.getCheckedRadioButtonId() > 0
                && !selectedUsNumber.equals("null")
                && !selectedUsNumber.equals("")) {
            String selectedUsAreaCode = areaCodeList.get(spinnerAreaCode.getSelectedItemPosition()).getAreaCode();
            Bundle bundlePrev = getArguments();
            args.putString(Constants.BUNDLE_REGISTER_EMAIL, bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL));
            args.putString(Constants.BUNDLE_REGISTER_PASSWORD, bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD));
            args.putString(Constants.BUNDLE_REGISTER_AREA_CODE, selectedUsAreaCode);
            args.putString(Constants.BUNDLE_REGISTER_NUMBER, selectedUsNumber);
            if (bundlePrev.getString(Constants.BUNDLE_REGISTER_PROMO_CODE) != null)
                args.putString(Constants.BUNDLE_REGISTER_PROMO_CODE, bundlePrev.getString(Constants.BUNDLE_REGISTER_PROMO_CODE));
            else args.putString(Constants.BUNDLE_REGISTER_PROMO_CODE, "");
            args.putString(Constants.BUNDLE_REGISTER_LOCK_ID, tempLockId);
            fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
            fragmentHolderActivity.mProgressDialog.show();

            if (bundlePrev.getBoolean(Constants.BUNDLE_REGISTER_WEBCS_USER))
                /*
                 * Register WebCS-created user. Skip RegisterPaymentFragment
                 * User is already paid through WebCS
                 */
                ApiHelper.registerUserWebCS(bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL),
                        selectedUsAreaCode + selectedUsNumber,
                        bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD)).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "registerUserWebCS " + e);
                    }

                    @Override
                    public void onNext(Boolean success) {
                        if (success) {
                            // Proceed to congratulatory page
                            RegisterEndFragment registerEndFragment = new RegisterEndFragment();
                            registerEndFragment.setArguments(args);
                            fragmentHolderActivity.changeFragment(registerEndFragment, false);
                        }
                    }
                });
            else
                ApiHelper.doGetSubscriptionList(getActivity(), fragmentHolderActivity.mProgressDialog, isPromoUser).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<List<SubscriptionList>>() {
                            @Override
                            public void onCompleted() {
                                // Proceed fetching IDD price list
                                ApiHelper.doGetIddList(getActivity(), fragmentHolderActivity.mProgressDialog).subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Subscriber<List<IddList>>() {
                                            @Override
                                            public void onCompleted() {

                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onNext(List<IddList> iddList) {
                                                ((SubscriptionIddBridge) getActivity()).setSubscriptionList(listSubscription, iddList, args);
                                            }
                                        });
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(List<SubscriptionList> subscriptionList) {
                                listSubscription = subscriptionList;
                            }
                        });
        } else {
            Toast.makeText(fragmentHolderActivity, getString(R.string.register_number_notice_select_number), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.ib_refresh_number)
    public void refreshNumberList() {
        if (refreshCredits != 0) {
            String refresh = getString(R.string.register_number_refresh_credits) + " " + --refreshCredits;
            ++indexCredits;
            getUsNumbers(false, refresh);    // false - not from Area Code; refresh is to update refresh credits
        } else
            Toast.makeText(fragmentHolderActivity, getString(R.string.register_number_notice_refresh_credits), Toast.LENGTH_SHORT).show();
    }

    @OnItemSelected(R.id.spinner_area_code)
    public void onAreaCodeSelected(int position) {
        Log.i(TAG, "Spinner item selected " + position);
        getUsNumbers(true, ""); // true - from Area Code; refresh is empty - no need to update refresh credits
    }

    private void setNumberAdapter(List<String> usNumberList) {
        for (int i = 0; i < usNumberList.size(); i++) {
            final int assignedNum = i;
//            radioButtons.get(i).setText(usNumberList.get(i));

            String formattedNumber = PhoneNumberUtils.formatNumber(new StringFormatter().getContactNameViaMainThread(usNumberList.get(i)));
            radioButtons.get(i).setText(formattedNumber);
            radioButtons.get(i).setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    if (GlobalValues.DEBUG)
                        Log.i(TAG, "Radio Group selected " + radioButtons.get(assignedNum).getText());
                    selectedUsNumber = radioButtons.get(assignedNum).getText().toString();
                }
            });
        }
    }

    /**
     * Reset radio buttons
     */
    private void resetRadioButtons() {
        for (RadioButton radioButton : radioButtons) {
            radioButton.setChecked(false);
        }
    }

    public void setAreaCodeAdapter(List<com.katadigital.owtel.models.AreaCode> areaCodeList) {
        this.areaCodeList = areaCodeList;

        AreaCodeAdapter areaCodeAdapter = new AreaCodeAdapter(areaCodeList);
        spinnerAreaCode.setAdapter(areaCodeAdapter);
    }

    /**
     * Get US numbers
     *
     * @param isFromAreaCode boolean
     * @param refresh        String
     */
    private void getUsNumbers(boolean isFromAreaCode, String refresh) {
        fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing_numbers));
        fragmentHolderActivity.mProgressDialog.show();
        fragmentHolderActivity.apiService
                .getUsNumbers(areaCodeList.get(spinnerAreaCode.getSelectedItemPosition()).getAreaCode(),
                        tempLockId).enqueue(new Callback<UsNumberList>() {
            @Override
            public void onResponse(Response<UsNumberList> response, Retrofit retrofit) {
// FIXME: 12/2/16 auto retry
                try {
                    Log.e(TAG, "onResponse: getUsNumbers " + response.body().toString());
                    fragmentHolderActivity.mProgressDialog.dismiss();
                    setNumberAdapter(response.body().getOwtelNumbers());
                    resetRadioButtons();    // reset radio buttons
                    if (!isFromAreaCode && !refresh.trim().isEmpty())
                        tvRefreshCredits.setText(refresh);
                }catch (Exception e)
                {
                    Log.e(TAG, "onResponse: "+e.getLocalizedMessage());
                    Toast.makeText(OwtelAppController.getInstance(), "No numbers are currently available for the selected region. Please try again later!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Throwable t) {
                fragmentHolderActivity.mProgressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class AreaCodeAdapter extends BaseAdapter {
        private List<AreaCode> areaCodeList;

        public AreaCodeAdapter(List<AreaCode> areaCodeList) {
            this.areaCodeList = areaCodeList;
        }

        @Override
        public int getCount() {
            return areaCodeList.size();
        }

        @Override
        public Object getItem(int position) {
            return areaCodeList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.spinner_area_code, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.tvCity = (TextView) convertView.findViewById(R.id.tv_city);
                viewHolder.tvState = (TextView) convertView.findViewById(R.id.tv_state);
                viewHolder.tvAreaCode = (TextView) convertView.findViewById(R.id.tv_area_code);
                convertView.setTag(viewHolder);
            } else viewHolder = (ViewHolder) convertView.getTag();

            viewHolder.tvCity.setText(areaCodeList.get(position).getCity());
            viewHolder.tvState.setText(areaCodeList.get(position).getState());
            viewHolder.tvAreaCode.setText(areaCodeList.get(position).getAreaCode());

            return convertView;
        }

        private class ViewHolder {
            public TextView tvCity;
            public TextView tvState;
            public TextView tvAreaCode;
        }
    }
}
