package com.katadigital.owtel.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kata.phone.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MyProgressBar extends Dialog {

    @Bind(R.id.cv_progress_loading)
    CardView card;

    @Bind(R.id.progress)
    ProgressBar progressBar;

    @Bind(R.id.tv_progress_text)
    TextView tvMessage;

    public MyProgressBar(Context context){
        super(context);
        setContentView(R.layout.loading_layout);

        ButterKnife.bind(this);

        ViewGroup.LayoutParams params = card.getLayoutParams();
        params.width=(int) (context.getResources().getDisplayMetrics().widthPixels * .90);

        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

    }

   public void setMessage(String message){
       tvMessage.setText(message);
   }



}
