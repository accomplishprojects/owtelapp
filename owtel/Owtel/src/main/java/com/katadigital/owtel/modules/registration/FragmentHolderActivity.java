package com.katadigital.owtel.modules.registration;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.DaoSession;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.models.AreaCode;
import com.katadigital.owtel.models.collectedobjects.RegistrationResult;
import com.katadigital.owtel.modules.resetaccess.ForgotPasswordFragment;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.MyProgressBar;
import com.katadigital.owtel.utils.RetrofitServiceManager;
import com.katadigital.owtel.utils.SharedPreferencesManager;

import java.util.List;

import javax.inject.Inject;

public class FragmentHolderActivity extends AppCompatActivity implements AreaCodeBridge, SubscriptionIddBridge {

    public static final String TAG = "FragmentHolderActivity";

    FragmentManager fragmentManager;

    @Inject
    public ApiService apiService;

    @Inject
    public DaoSession daoSession;

    public MyProgressBar myProgressBar;
    public ProgressDialog mProgressDialog;

    private String registrationString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscription_layout);

        OwtelAppController.getComponent(this).inject(this);

        myProgressBar = new MyProgressBar(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        fragmentManager = getSupportFragmentManager();

        statusBarColor();


        RetrofitServiceManager.init();
        SharedPreferencesManager.sharedManagerInit(this);

        registrationString = this.getIntent ().getStringExtra ( Constants.USER_CREDENTIALS );

        if(registrationString != null){
            Log.e ( TAG, "Not null");
            if (getIntent().getStringExtra(TAG).equals(RegisterFragment.TAG)) {
                RegisterFragment registerFragment = new RegisterFragment ();
                changeFragment(registerFragment, true);
                Handler handler = new Handler (  );
                handler.postDelayed ( new Runnable () {
                    @Override
                    public void run() {
                        registerFragment.autoPopulateEmail ( registrationString );
                    }
                } ,0);
            } else if (getIntent().getStringExtra(TAG).equals(ForgotPasswordFragment.TAG)){
                ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment ();
                changeFragment(forgotPasswordFragment, true);
            }

        }else {
            Log.e ( TAG, "Null");
            if (getIntent().getStringExtra(TAG).equals(RegisterFragment.TAG)) {
                changeFragment(new RegisterFragment(), true);
            } else if (getIntent().getStringExtra(TAG).equals(ForgotPasswordFragment.TAG))
                changeFragment(new ForgotPasswordFragment(), true);
        }


    }
    /**
     * Change Fragment
     *
     * @param fragment Fragment
     */
    public void changeFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_fragment_container, fragment);
        if (!addToBackStack)
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume ();
    }




    /**
     * Change Fragment with Bundle
     *
     * @param fragment Fragment
     * @param bundle   Bundle
     */
    public void changeFragment(Fragment fragment, Bundle bundle) {
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Change Fragment add, replace with Bundle
     * @param fragment Fragment
     * @param add boolean
     * @param bundle Bundle
     */
    public void changeFragment(Fragment fragment, boolean add, Bundle bundle) {
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(add)fragmentTransaction.add(R.id.fl_fragment_container, fragment);
        else fragmentTransaction.replace(R.id.fl_fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void statusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
    }

    /**
     * Invokes an AlertDialog popup on view.
     *
     * @param activity            Current activity
     * @param title               Title for the AlertDialog
     * @param msg                 Message content
     * @param buttonNegativeTitle Text on the negative button
     * @see android.app.AlertDialog
     */
    public void showAlertDialog(Activity activity, String title, String msg, String buttonNegativeTitle) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setNegativeButton(buttonNegativeTitle, (dialog, which) -> {
        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    /**
     * Hide SoftInputKeyboard
     * @param view View
     */
    public void hideSoftInput(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Set Area Code
     * @param areaCodeList List<ListAreaCode>
     * @param args Bundle
     */
    @Override
    public void setAreaCodeList(List<AreaCode> areaCodeList, Bundle args) {
        RegisterAreaCodeAndNumberFragment regNumFragment = new RegisterAreaCodeAndNumberFragment();
        changeFragment(regNumFragment, args);
        Handler handler = new Handler();
        handler.postDelayed(() -> regNumFragment.setAreaCodeAdapter(areaCodeList), 0);
    }

    /**
     * Set Subscription IDD in Payment Fragment
     * @param subscriptionList List<SubscriptionList>
     * @param args Bundle
     */
    @Override
    public void setSubscriptionList(List<SubscriptionList> subscriptionList, List<IddList> iddList, Bundle args) {
        RegisterPaymentItemsFragment regPaymentFragment = new RegisterPaymentItemsFragment();
        changeFragment(regPaymentFragment, true, args);
        Handler handler = new Handler();
        handler.postDelayed(() ->{
            regPaymentFragment.setSubscription(subscriptionList);
            regPaymentFragment.setIdd(iddList);
        }, 0);
    }

    @Override
    public void setSubscriptionIdd(SubscriptionList subscription, IddList idd, Bundle args, List<AddressList> countryList) {
        RegisterPaymentCheckoutFragment registerPaymentCheckoutFragment = new RegisterPaymentCheckoutFragment();
        changeFragment(registerPaymentCheckoutFragment, true, args);
        Handler handler = new Handler();
        handler.postDelayed(() -> registerPaymentCheckoutFragment.prepareCheckout(subscription, idd, countryList), 50);
    }

    /**
     * Implements Retrofit Service Manager Instance to parent view*/
    public RetrofitServiceManager getRetrofitService(){
        return RetrofitServiceManager.getRetrofitServiceInstance();
    }




}
