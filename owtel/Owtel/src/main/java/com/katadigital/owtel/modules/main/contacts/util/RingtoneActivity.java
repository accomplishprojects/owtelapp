package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.RingtoneDto;

public class RingtoneActivity extends Activity implements OnItemClickListener,
        OnItemSelectedListener {

    ListAdapter listadapter;
    Cursor mCursor2;
    RingtoneManager mRingtoneManager2;
    Uri FinaLURI;
    public static int localuri1;
    public static ContactDto oldContactDto;
    int position;

    ListView ringtone_lv;

    MediaPlayer mMediaPlayer;
    ImageView iv_checked;
    AdapterRingtone adapterRingtone;

    ArrayList<Integer> arrayIv = new ArrayList<Integer>();
    int getIndexCheck;
    ArrayList<String> mArrayList;
    public static int pos = 0;
    public static RingtoneDto ringtoneDto = new RingtoneDto();

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mCursor2.close();
        this.closeOptionsMenu();
        this.finish();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        mCursor2.close();
        this.closeOptionsMenu();
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ringtone_layout);

        // Custom Action Bar ===============
        String letItem = getResources().getString(R.string.cancel_string);
        String rightItem = getResources().getString(R.string.done_string);

        View doneBtn = CustomActionBar.viewCustomActionBar(this, letItem, "",
                "");

        doneBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Ringtone ringtone = RingtoneManager.getRingtone(
                        getApplicationContext(), FinaLURI);
                oldContactDto.setRingtone(FinaLURI.toString());
            }
        });

        // Custom Action Bar ===============

        mRingtoneManager2 = new RingtoneManager(this); // adds ringtonemanager
        mRingtoneManager2.setType(RingtoneManager.TYPE_RINGTONE); // sets the
        // type to
        // ringtones
        mRingtoneManager2.setIncludeDrm(true); // get list of ringtones to
        // include DRM

        mCursor2 = mRingtoneManager2.getCursor(); // appends my cursor to the
        // ringtonemanager

        startManagingCursor(mCursor2); // starts the cursor query

        ringtone_lv = (ListView) findViewById(R.id.ringtone_lv);

        int count = -1;
        mArrayList = new ArrayList<String>();
        for (mCursor2.moveToFirst(); !mCursor2.isAfterLast(); mCursor2
                .moveToNext()) {
            // The Cursor is now set to the right position

            String title = mCursor2
                    .getString(mCursor2
                            .getColumnIndexOrThrow(RingtoneManager.EXTRA_RINGTONE_TITLE));

            mArrayList.add(title);
            arrayIv.add(0);
            count++;

            if (title.equalsIgnoreCase(ringtoneDto.getRintoneName())) {

                ringtoneDto.setIndexSeleced(count);

                ringtone_lv.clearFocus();
                ringtone_lv.post(new Runnable() {
                    @Override
                    public void run() {

                        ringtone_lv.smoothScrollToPositionFromTop(ringtoneDto.getIndexSeleced(), 10, 1);

                        arrayIv.set(ringtoneDto.getIndexSeleced(), 0);
                        arrayIv.set(ringtoneDto.getIndexSeleced(), 1);
                        adapterRingtone.updateData(arrayIv);
                        adapterRingtone.notifyDataSetChanged();
                        pos = ringtoneDto.getIndexSeleced();
                    }
                });

            }
        }

        // adapterRingtone.notifyDataSetChanged();
        ringtone_lv.requestFocusFromTouch();
        ringtone_lv.setSelection(position);

        adapterRingtone = new AdapterRingtone(this, mArrayList, arrayIv);
        ringtone_lv.setAdapter(adapterRingtone);
        ringtone_lv.setOnItemClickListener(this);
        ringtone_lv.setOnItemSelectedListener(this);
        mMediaPlayer = new MediaPlayer();
        ringtone_lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.slide_out_down);
        mMediaPlayer.reset();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        RingtoneManager rm = new RingtoneManager(getApplicationContext());
        Cursor cursor = rm.getCursor();
        cursor.moveToFirst();

        Uri parcialUri = Uri.parse("content://media/internal/audio/media");
        String ringtoneTitle = mArrayList.get(position);
        Uri finalSuccessfulUri = null;

        while (!cursor.isAfterLast()) {
            if (ringtoneTitle.compareToIgnoreCase(cursor.getString(cursor
                    .getColumnIndex(MediaStore.MediaColumns.TITLE))) == 0) {
                int ringtoneID = cursor.getInt(cursor
                        .getColumnIndex(MediaStore.MediaColumns._ID));
                finalSuccessfulUri = Uri.withAppendedPath(parcialUri, ""
                        + ringtoneID);
                break;
            }
            cursor.moveToNext();
        }

        playSound(finalSuccessfulUri);

        FinaLURI = finalSuccessfulUri;

        arrayIv.set(pos, 0);
        arrayIv.set(position, 1);
        adapterRingtone.updateData(arrayIv);
        adapterRingtone.notifyDataSetChanged();
        pos = position;

        // Custom Action Bar ===============
        String letItem = getResources().getString(R.string.cancel_string);
        String rightItem = getResources().getString(R.string.done_string);

        View doneBtn = CustomActionBar.viewCustomActionBar(this, letItem, "",
                rightItem);

        doneBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Ringtone ringtone = RingtoneManager.getRingtone(
                        getApplicationContext(), FinaLURI);
                oldContactDto.setRingtone(FinaLURI.toString());
                onBackPressed();
            }
        });

        // Custom Action Bar ===============;

    }

    private void playSound(Uri uri) {

        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.reset();
            mMediaPlayer.stop();
            method(uri);
        } else {
            method(uri);
        }

    }

    public void method(Uri uri) {
        try {
            mMediaPlayer.setDataSource(this, uri);
            mMediaPlayer.setAudioStreamType(AudioManager.RINGER_MODE_NORMAL);
            mMediaPlayer.prepare();
            mMediaPlayer.start();
            mMediaPlayer.setVolume(1, 1);
            mMediaPlayer
                    .setOnSeekCompleteListener(new OnSeekCompleteListener() {
                        public void onSeekComplete(MediaPlayer mp) {
                            mMediaPlayer.reset();
                            mMediaPlayer.stop();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            mMediaPlayer.reset();
            mMediaPlayer.stop();
        }
    }

    class AdapterRingtone extends BaseAdapter {

        RingtoneActivity mRingtoneActivity;
        ArrayList<String> mArrayList2;
        ArrayList<Integer> mArrayListbool;

        public AdapterRingtone(RingtoneActivity ringtoneActivity,
                               ArrayList<String> mArrayList, ArrayList<Integer> mArrayListbool) {
            this.mRingtoneActivity = ringtoneActivity;
            this.mArrayList2 = mArrayList;
            this.mArrayListbool = mArrayListbool;
        }

        @Override
        public int getCount() {
            return mArrayList2.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        public void updateData(ArrayList<Integer> mArrayListbool) {
            this.mArrayListbool = mArrayListbool;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) mRingtoneActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.ringtone_item, viewGroup,
                    false);

            TextView textView1 = (TextView) rowView
                    .findViewById(R.id.txt_ringtone);
            textView1.setText(mArrayList2.get(position));

            iv_checked = (ImageView) rowView.findViewById(R.id.checkedIV);
            if (mArrayListbool.get(position) == 1) {
                iv_checked.setVisibility(View.VISIBLE);
            }

            return rowView;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        Toast.makeText(getApplicationContext(), "" + position, 1000).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }

}