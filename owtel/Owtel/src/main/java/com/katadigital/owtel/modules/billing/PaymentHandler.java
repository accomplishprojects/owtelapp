package com.katadigital.owtel.modules.billing;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;

/**
 * Created by Omar Matthew Reyes on 7/18/16.
 */
public class PaymentHandler {
    /**
     * Generate default adapter for Spinner
     *
     * @param stringList String[]
     * @return ArrayAdapter<String>
     */
    private ArrayAdapter<String> generateDefaultAdapter(String[] stringList) {
        return new ArrayAdapter<>(OwtelAppController.getInstance(), R.layout.simple_list_item, stringList);
    }

    /**
     * Check if all fields are valid
     *
     * @return boolean
     */
    private boolean validateFieldValues(EditText etCardNumber, EditText etCardHolder,
                                        Spinner spinnerCardExpireMonth, Spinner spinnerCardExpireYear,
                                        EditText etCardCVV, EditText etAddressStreet, EditText etCity,
                                        EditText etState, Spinner spinnerAddressCountry,
                                        EditText etAddressZipCode) {
        return isCreditCardValid(etCardNumber) &&
                isCreditCardHolderValid(etCardHolder) &&
                isCreditCardExpiryValid(spinnerCardExpireMonth, spinnerCardExpireYear) &&
                isCreditCardCvvValid(etCardCVV) &&
                isCreditCardAddressValid(etAddressStreet, etCity, etState,
                        spinnerAddressCountry, etAddressZipCode);
    }

    private boolean isCreditCardValid(EditText etCardNumber) {
        String cardNum = etCardNumber.getText().toString().trim();
        return !(cardNum.equals("") || cardNum.trim().isEmpty());
    }

    private boolean isCreditCardHolderValid(EditText creditCardHolder) {
        String cardHolder = creditCardHolder.getText().toString().trim();
        return !(cardHolder.equals("") || cardHolder.trim().isEmpty());
    }

    /**
     * Check if credit card expiry date is valid
     *
     * @param creditCardExpiryMonth Spinner
     * @param creditCardExpiryYear  Spinner
     * @return boolean
     */
    private boolean isCreditCardExpiryValid(Spinner creditCardExpiryMonth, Spinner creditCardExpiryYear) {
        return creditCardExpiryMonth.getSelectedItemPosition() > 0 &&
                creditCardExpiryYear.getSelectedItemPosition() > 0;
    }

    /**
     * Check if credit card cvv is valid
     *
     * @param creditCardCvv String
     * @return boolean
     */
    private boolean isCreditCardCvvValid(EditText creditCardCvv) {
        return creditCardCvv.getText().toString().trim().length() >= 3;
    }

    /**
     * Check if credit card billing address is valid
     *
     * @param etAddressStreet       EditText
     * @param etCity                EditText
     * @param etState               EditText
     * @param spinnerAddressCountry Spinner
     * @return boolean
     */
    private boolean isCreditCardAddressValid(EditText etAddressStreet, EditText etCity,
                                             EditText etState, Spinner spinnerAddressCountry,
                                             EditText etAddressZipCode) {
        return etAddressStreet.getText().toString().trim().length() > 0 &&
                etCity.getText().toString().trim().length() > 0 &&
                etState.getText().toString().trim().length() > 0 &&
                spinnerAddressCountry.getSelectedItemPosition() > 0 &&
                etAddressZipCode.getText().toString().trim().length() > 0;
    }
}
