package com.katadigital.owtel.modules.main.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.ui.recyclerview.SettingsRecyclerViewAdapter;

/**
 * Created by Omar Matthew Reyes on 2/18/16.
 * Settings Fragment
 */


public class SettingsFragment extends Fragment {
    public static final String TAG = "SettingFragment";
    private RecyclerView recyclerViewSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        // Instantiate MainActivity
        MainActivity mainActivity = (MainActivity) getActivity();

        String[] settingsDataSet = getResources().getStringArray(R.array.list_item_settings);

        recyclerViewSettings = (RecyclerView) view.findViewById(R.id.rv_settings);
        recyclerViewSettings.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewSettings.setLayoutManager(mLinearLayoutManager);

        // specify an adapter (see also next example)
        SettingsRecyclerViewAdapter mAdapter = new SettingsRecyclerViewAdapter(settingsDataSet, getActivity());
        recyclerViewSettings.setAdapter(mAdapter);

        // Fragment popBackStack()
        ImageButton mButtonBack = (ImageButton) view.findViewById(R.id.btn_settings_back);
        mButtonBack.setOnClickListener(v -> mainActivity.popFragment());

        if (getArguments().getBoolean(Constants.BUNDLE_HIDE_BTN_BACK, false))
            mButtonBack.setVisibility(View.INVISIBLE);
        else mButtonBack.setVisibility(View.VISIBLE);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        // Refresh IDD Credits upon resume
        String[] settingsDataSet = getResources().getStringArray(R.array.list_item_settings);
        ApiHelper.updateUserIddCredits(getActivity(), settingsDataSet, recyclerViewSettings);

    }

    public void refresh() {        // Refresh IDD Credits upon resume
        String[] settingsDataSet = getResources().getStringArray(R.array.list_item_settings);
        ApiHelper.updateUserIddCredits(getActivity(), settingsDataSet, recyclerViewSettings);
    }
}


