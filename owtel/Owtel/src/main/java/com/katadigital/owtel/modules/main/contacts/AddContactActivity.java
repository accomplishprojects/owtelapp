package com.katadigital.owtel.modules.main.contacts;

import android.accounts.AuthenticatorDescription;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Util;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.contacts.camerautil.CameraManager;
import com.katadigital.owtel.modules.main.contacts.camerautil.CameraUtil;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.util.AddFunction;
import com.katadigital.owtel.modules.main.contacts.util.AddHelperField;
import com.katadigital.owtel.modules.main.contacts.util.CustomActionBar;
import com.katadigital.owtel.modules.main.contacts.util.CustomTextField;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.owtel.modules.main.contacts.util.RingtoneActivity;
import com.katadigital.owtel.modules.main.dialer.DialerFragment;
import com.katadigital.owtel.modules.main.history.LogsDetail;
import com.katadigital.portsip.utilities.ui.image.ImageCustomManager;
import com.katadigital.ui.Style;

import java.io.File;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AddContactActivity extends AppCompatActivity implements
        OnClickListener {

    private static final String TAG = "AddContactActivity";

    QuickContactHelper quickContactHelper;
    AddHelperField helperField;
    AddFunction addHelper;

    CircleImageView avatar;
    public EditText prefixNameText;
    public EditText fNameText;
    public EditText phonetic_fNameText;
    public EditText middleNameText;
    public EditText lNameText;
    public EditText phonetic_lNameText;
    public EditText nickNameText;
    public EditText phonetic_midNameText;
    public EditText sufNameText;
    public EditText jobTitleNameText;
    public EditText jobDepNameText;
    public EditText companyName;
    public EditText noteTxt;

    public static TextView ringtoneName;

    private ProgressDialog progressDialog;
    boolean success = false;
    String returnmessage;

    Typeface font1;
    Typeface font2;

    ScrollView scrollView;
    public static ContactDto contactDto = new ContactDto();
    public static boolean keypadCheck;

    private File mFileTemp;
    Uri localUri;
//    private ArrayList<AccountData> mAccounts;
//    private AccountAdapter mAccountAdapter;
//    private Spinner mAccountSpinner;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CameraManager.onActivityResultCamera(requestCode, resultCode, data, this, avatar, mFileTemp, contactDto);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contacts_add);
        Style.statusBarColor(this, R.color.colorPrimaryToolbar);

        /**Usable Modifications*/
        AddHelperField.checkerForPhoneNumbers.clear();
        AddHelperField.checkerForPhoneNumbersIndex = -1;

        if (GlobalValues.DEBUG) Log.i(TAG, "onCreate");

        AddHelperField.clearAllButtonList();
        initData();
        initInformation();
        initTextField();
        initLayoutForFont();

        // scrollview
        scrollView = (ScrollView) findViewById(R.id.add_scroll_view);

        // Custom Action Bar ===============
        String letItem = getResources().getString(R.string.cancel_string);
        String title = getResources().getString(R.string.string_new_mssg);
        String rightItem = getResources().getString(R.string.done_string);

        View doneBtn = CustomActionBar.viewCustomActionBar(this, letItem, title, rightItem);

        doneBtn.setOnClickListener(v -> {
//            showDialogContactAdd();
            if (progressDialog == null) {

                /**Usable Modification*/
                String strFname = fNameText.getText().toString();
                String strLname = lNameText.getText().toString();
                if (!strFname.isEmpty() || !strLname.isEmpty()) {
                    if (AddHelperField.checkerForPhoneNumbers.size() > 0) {
                        if (AddHelperField.checkerForPhoneNumbers
                                .get(AddHelperField.checkerForPhoneNumbers.size() - 1)
                                .getPhoneNumber() != null) {
                            if (!AddHelperField.checkerForPhoneNumbers
                                    .get(AddHelperField.checkerForPhoneNumbers.size() - 1)
                                    .getPhoneNumber().isEmpty()) {
                                new addContactAsync(AddContactActivity.this)
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this, "Please check your firstname and lastname", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Custom Action Bar ===============

        // Intent ===============
        Intent intent;
        if (getIntent().getExtras() != null) {
            intent = getIntent();
            Bundle bundle = intent.getExtras();
            final String numberFromKeypad = bundle.getString("preloaded_number");
            if (numberFromKeypad != null) {
                // mobileNumber.setText(numberFromKeypad);
                PhoneNumberDto phoneNumberDto = new PhoneNumberDto();
                phoneNumberDto.setNumber(numberFromKeypad);
                if (getResources().getStringArray(R.array.numberspinner)[0] != null) {
                    phoneNumberDto.setNumberType(getResources().getStringArray(R.array.numberspinner)[0]);
                }
                contactDto = helperField.createLoadedNumberField(phoneNumberDto, contactDto);
                Log.e(TAG, "onCreate: " + AddHelperField.checkerForPhoneNumbers.size());
            }
        }
        // Intent ===============
        mFileTemp = CameraUtil.getFile(this);

//        mAccountSpinner = (Spinner) findViewById(R.id.spinner_contacts_save);
//        mAccounts = new ArrayList<>();
//        mAccountAdapter = new AccountAdapter(this, mAccounts);
//        mAccountSpinner.setAdapter(mAccountAdapter);
    }

    private void initData() {
        quickContactHelper = new QuickContactHelper(this);
        addHelper = new AddFunction(this);
        helperField = new AddHelperField(this);
        font1 = NewUtil.getFontBold(this);
        font2 = NewUtil.getFontRoman(this);
    }

    private void initLayoutForFont() {
        ((TextView) findViewById(R.id.add_phone_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_phone_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_email_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_email_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_ringtone_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_ringtone_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_url_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_url_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_address_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_address_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_bday_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_bday_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_dates_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_dates_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_related_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_related_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_im_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_im_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_notes_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_notes_txt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_notes_ok)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_notes_ok)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.add_field_txt)).setTypeface(font2);
        ((TextView) findViewById(R.id.add_field_txt)).setTextSize(NewUtil.gettxtSize());
    }

    private void initInformation() {
        prefixNameText = CustomTextField.includeInit(R.id.include_prefix, AddContactActivity.this);
        fNameText = CustomTextField.includeInit(R.id.include_givenname, AddContactActivity.this);
        phonetic_fNameText = CustomTextField.includeInit(R.id.include_p_givenname, AddContactActivity.this);
        middleNameText = CustomTextField.includeInit(R.id.include_middle, AddContactActivity.this);
        phonetic_midNameText = CustomTextField.includeInit(R.id.include_p_middle, AddContactActivity.this);
        lNameText = CustomTextField.includeInit(R.id.include_familyname, AddContactActivity.this);
        phonetic_lNameText = CustomTextField.includeInit(R.id.include_p_familyname, AddContactActivity.this);
        sufNameText = CustomTextField.includeInit(R.id.include_suffix, AddContactActivity.this);
        nickNameText = CustomTextField.includeInit(R.id.include_nicname, AddContactActivity.this);
        jobTitleNameText = CustomTextField.includeInit(R.id.include_job_title, AddContactActivity.this);
        jobDepNameText = CustomTextField.includeInit(R.id.include_job_dep, AddContactActivity.this);
        companyName = CustomTextField.includeInit(R.id.include_compny, AddContactActivity.this);

        prefixNameText.setHint(getResources().getString(
                R.string.add_field_prfix));
        fNameText.setHint(getResources().getString(R.string.first_string));
        phonetic_fNameText.setHint(getResources().getString(
                R.string.add_field_pfname));
        middleNameText.setHint(getResources().getString(
                R.string.add_field_middle));
        phonetic_midNameText.setHint(getResources().getString(
                R.string.add_field_pmiddle));
        lNameText.setHint(getResources().getString(R.string.last_string));
        phonetic_lNameText.setHint(getResources().getString(
                R.string.add_field_plname));
        sufNameText.setHint(getResources().getString(R.string.add_field_suffix));
        nickNameText.setHint(getResources().getString(
                R.string.add_field_nickname));
        jobTitleNameText.setHint(getResources().getString(
                R.string.add_field_jobtitle));
        jobDepNameText.setHint(getResources().getString(
                R.string.add_field_jobdepartment));
        companyName.setHint(getResources().getString(R.string.Company_string));

        noteTxt = (EditText) findViewById(R.id.add_notes_ok);
        noteTxt.clearFocus();

        avatar = (CircleImageView) this.findViewById(R.id.add_photo_image);
        avatar.setOnClickListener(this);

    }

    private void initTextField() {
        LinearLayout btnPhone = (LinearLayout) findViewById(R.id.phone_click);
        Button btnPhoneImg = (Button) findViewById(R.id.add_phone_img);
        btnPhone.setOnClickListener(this);
        btnPhoneImg.setOnClickListener(this);

        LinearLayout btnEmail = (LinearLayout) findViewById(R.id.email_click);
        Button btnEmailImg = (Button) findViewById(R.id.add_email_img);
        btnEmail.setOnClickListener(this);
        btnEmailImg.setOnClickListener(this);

        LinearLayout btnRingtone = (LinearLayout) findViewById(R.id.ringtone_click);
        ringtoneName = (TextView) findViewById(R.id.ringtone_name);
        btnRingtone.setOnClickListener(this);

        LinearLayout btnURL = (LinearLayout) findViewById(R.id.url_click);
        Button btnURLimg = (Button) findViewById(R.id.add_url_img);
        btnURL.setOnClickListener(this);
        btnURLimg.setOnClickListener(this);

        LinearLayout btnAddress = (LinearLayout) findViewById(R.id.address_click);
        Button btnAddimg = (Button) findViewById(R.id.add_address_img);
        btnAddress.setOnClickListener(this);
        btnAddimg.setOnClickListener(this);

        LinearLayout btnBday = (LinearLayout) findViewById(R.id.birthday_click);
        Button btnBdayimg = (Button) findViewById(R.id.add_birthday_img);
        btnBday.setOnClickListener(this);
        btnBdayimg.setOnClickListener(this);

        LinearLayout btnDate = (LinearLayout) findViewById(R.id.date_click);
        Button btnDateimg = (Button) findViewById(R.id.add_date_img);
        btnDate.setOnClickListener(this);
        btnDateimg.setOnClickListener(this);

        LinearLayout btnRelated = (LinearLayout) findViewById(R.id.related_click);
        Button btnRelatedimg = (Button) findViewById(R.id.add_related_img);
        btnRelated.setOnClickListener(this);
        btnRelatedimg.setOnClickListener(this);

        LinearLayout btnIM = (LinearLayout) findViewById(R.id.im_click);
        Button btnIMmg = (Button) findViewById(R.id.add_im_img);
        btnIM.setOnClickListener(this);
        btnIMmg.setOnClickListener(this);

        View addField = findViewById(R.id.add_field);
        addField.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        scrollView.post(() -> {
            scrollView.fullScroll(ScrollView.FOCUS_UP);

            fNameText.setFocusable(true);
            fNameText.requestFocus();

            if (contactDto.getProfilepic() != null) {
                /**Loading Image*/
                contactDto.setProfilepic(contactDto.getProfilepic());
                ImageCustomManager.getImageCustomManagerInstance()
                        .displayProfilePictureFromBitmap(AddContactActivity.this, contactDto.getProfilepic(), avatar);
            }

            Util.checkAddFieldisHidden(contactDto.getPrefix(),
                    prefixNameText, AddFieldActivity.prefisHidden);
            Util.checkAddFieldisHidden(contactDto.getPhonetic_fName(),
                    phonetic_fNameText, AddFieldActivity.pfirstisHidden);
            Util.checkAddFieldisHidden(contactDto.getMiddleName(),
                    middleNameText, AddFieldActivity.middleisHidden);
            Util.checkAddFieldisHidden(contactDto.getPhonetic_middleName(),
                    phonetic_midNameText, AddFieldActivity.pmiddleisHidden);
            Util.checkAddFieldisHidden(contactDto.getPhonetic_lName(),
                    phonetic_lNameText, AddFieldActivity.plastisHidden);
            Util.checkAddFieldisHidden(contactDto.getSuffix(), sufNameText,
                    AddFieldActivity.suffixisHidden);
            Util.checkAddFieldisHidden(contactDto.getNickname(),
                    nickNameText, AddFieldActivity.nickisHidden);
            Util.checkAddFieldisHidden(contactDto.getJobTitle(),
                    jobTitleNameText, AddFieldActivity.jobTitleisHidden);
            Util.checkAddFieldisHidden(contactDto.getJobDep(),
                    jobDepNameText, AddFieldActivity.jobDepisHidden);

        });


        if (contactDto.getRingtone() != null) {
            localUri = Uri.parse(contactDto.getRingtone());
            Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(), localUri);
            String name = ringtone.getTitle(getApplicationContext());
            ringtoneName.setText(name);
            RingtoneActivity.ringtoneDto.setRintoneName(name);
            RingtoneActivity.ringtoneDto.setIndexSeleced(0);
        } else {
            localUri = Uri.parse(Settings.System.DEFAULT_NOTIFICATION_URI.getPath());
            Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(), localUri);
            String name = ringtone.getTitle(getApplicationContext());
            ringtoneName.setText(getResources().getString(R.string.string_default));
            RingtoneActivity.ringtoneDto.setRintoneName(name);
            RingtoneActivity.ringtoneDto.setIndexSeleced(0);
        }

        super.onResume();
    }

    private void addPhoneNumbers() {
        if (AddHelperField.checkerForPhoneNumbers.size() > 0) {
            if (AddHelperField.checkerForPhoneNumbers
                    .get(AddHelperField.checkerForPhoneNumbers.size() - 1)
                    .getPhoneNumber() != null) {
                if (!AddHelperField.checkerForPhoneNumbers
                        .get(AddHelperField.checkerForPhoneNumbers.size() - 1)
                        .getPhoneNumber().isEmpty()) {
                    helperField.addPhoneFunction(contactDto);
                } else {
                    Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please check numbers", Toast.LENGTH_SHORT).show();
            }

        } else {
            helperField.addPhoneFunction(contactDto);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // Phone Numbers

            case R.id.phone_click:
                addPhoneNumbers();
                break;
            case R.id.add_phone_img:
                addPhoneNumbers();
                break;

            // Email
            case R.id.email_click:
                helperField.addEmailFunction(contactDto);
                break;
            case R.id.add_email_img:
                helperField.addEmailFunction(contactDto);
                break;

            // Ringtone
            case R.id.ringtone_click:
                Intent intent = new Intent(AddContactActivity.this,
                        RingtoneActivity.class);
                RingtoneActivity.oldContactDto = contactDto;
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up,
                        R.anim.slide_in_up_exit);
                break;

            // URL
            case R.id.url_click:
                helperField.addURLFunction(contactDto);
                break;
            case R.id.add_url_img:
                helperField.addURLFunction(contactDto);
                break;

            // Address
            case R.id.address_click:
                helperField.addAddressFunction(contactDto);
                break;
            case R.id.add_address_img:
                helperField.addAddressFunction(contactDto);
                break;

            // Bday
            case R.id.birthday_click:
                if (!AddHelperField.hasBirthday)
                    helperField.addBdayFunction(contactDto);
                break;
            case R.id.add_birthday_img:
                if (!AddHelperField.hasBirthday)
                    helperField.addBdayFunction(contactDto);
                break;

            // Date
            case R.id.date_click:
                helperField.addDateFunction(contactDto);
                break;
            case R.id.add_date_img:
                helperField.addDateFunction(contactDto);
                break;

            // Related
            case R.id.related_click:
                helperField.addREFunction(contactDto);
                break;
            case R.id.add_related_img:
                helperField.addREFunction(contactDto);
                break;

            // IM
            case R.id.im_click:
                helperField.addIMFunction(contactDto);
                break;
            case R.id.add_im_img:
                helperField.addIMFunction(contactDto);
                break;

            // avatar
            case R.id.add_photo_image:
                Util.adddialogProfilePic(this, contactDto, avatar, mFileTemp);
                break;

            case R.id.add_field:
                NewUtil.IntetCustomTrans(this, AddFieldActivity.class);
                break;

            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        finish();
        AddContactActivity.this.overridePendingTransition(0, R.anim.slide_out_down);
        AddFieldActivity.setFalseAllFields();
    }

    /**
     * AsyncTask Saving contact
     */
    class addContactAsync extends AsyncTask<String, String, String> {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        private Activity activity;

        public addContactAsync(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {

            String messages = getResources().getString(
                    R.string.string_diag_savingcontact);
            String messages1 = getResources().getString(
                    R.string.string_diag_savingcontact);

            progressDialog = ProgressDialog.show(activity, messages, messages1 + " ... ");
            progressDialog.setCancelable(true);

            success = true;
            returnmessage = "";

            contactDto = quickContactHelper.removeNullValues(contactDto);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            contactDto = addHelper.addFunction(ops, contactDto);
            contactDto.setRingtone(localUri.toString());

            try {
                getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

                Cursor c = getContentResolver().query(Contacts.CONTENT_URI, null, null, null, null);
                c.moveToLast();
                int contactId = c.getInt(c.getColumnIndex(Contacts._ID));
                contactDto.setContactID(contactId + "");
                c.close();

                Uri contactUri = ContactsContract.Contacts.CONTENT_URI;
                ContentValues values = new ContentValues();
                values.put(ContactsContract.Contacts.CUSTOM_RINGTONE, contactDto.getRingtone());
                getContentResolver().update(contactUri, values, ContactsContract.Contacts._ID + " = ?",
                        new String[]{String.valueOf(contactDto.getContactID())});

                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imm.hideSoftInputFromWindow(fNameText.getWindowToken(), 0);
                    }
                });

            } catch (Exception e) {
                Log.e(TAG, "addContactAsync doInBackground " + e);
                returnmessage = e.getMessage();
                success = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (success) {
                onBackPressed();
                ContactDetailActivity.contactDto = contactDto;
                NewUtil.IntetCustomTrans(AddContactActivity.this, ContactDetailActivity.class);
                Toast.makeText(AddContactActivity.this, "Contact Added", Toast.LENGTH_SHORT).show();

                /**Usable Modifications*/
                AddHelperField.checkerForPhoneNumbersIndex = -1;
                AddHelperField.checkerForPhoneNumbers.clear();

                if (LogsDetail.removeinfoActiviy) {
                    try {
                        Var.logActivity.finish();
                    } catch (Exception e) {
                        Log.e(TAG, "addContactAsync onPostExecute " + e);
                    }

                    DialerFragment.fromKeypad = false;
                    LogsDetail.removeinfoActiviy = false;
                }

                if (keypadCheck) {
                    DialerFragment.fromKeypad = true;
                    keypadCheck = false;
                }
            }

            progressDialog.dismiss();
            progressDialog = null;
            super.onPostExecute(result);
        }
    }

    private void showDialogContactAdd() {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.layout_dialog_contact_add);

        dialog.findViewById(R.id.layout_dialog_bg).setOnClickListener(v -> dialog.dismiss());
        dialog.findViewById(R.id.btn_contact_save_phone).setOnClickListener(v -> {
            dialog.dismiss();
            doSaveContact(SAVE_CONTACT_PHONE).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Boolean>() {
                @Override
                public void onCompleted() {
                    Log.i(TAG, "doSaveContact Completed");
                    progressDialog.dismiss();
                    onBackPressed();
                    ContactDetailActivity.contactDto = contactDto;
                    NewUtil.IntetCustomTrans(AddContactActivity.this, ContactDetailActivity.class);
                    Toast.makeText(AddContactActivity.this, "Contact Added", Toast.LENGTH_SHORT).show();

                    if (LogsDetail.removeinfoActiviy) {
                        try {
                            Var.logActivity.finish();
                        } catch (Exception e) {
                            Log.e(TAG, "doSaveContact Var.logActivity.finish() " + e);
                        }

                        DialerFragment.fromKeypad = false;
                        LogsDetail.removeinfoActiviy = false;
                    }

                    if (keypadCheck) {
                        DialerFragment.fromKeypad = true;
                        keypadCheck = false;
                    }
                }

                @Override
                public void onError(Throwable e) {
                    progressDialog.dismiss();
                }

                @Override
                public void onNext(Boolean aBoolean) {
                    Log.i(TAG, "doSaveContact onNext");

                }
            });
        });
        dialog.findViewById(R.id.btn_contact_save_sim).setOnClickListener(v -> {
            dialog.dismiss();
            doSaveContact(SAVE_CONTACT_SIM).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Boolean>() {
                @Override
                public void onCompleted() {
                    Log.i(TAG, "doSaveContact Completed");
                    progressDialog.dismiss();
                    onBackPressed();
                    ContactDetailActivity.contactDto = contactDto;
                    NewUtil.IntetCustomTrans(AddContactActivity.this, ContactDetailActivity.class);
                    Toast.makeText(AddContactActivity.this, "Contact Added", Toast.LENGTH_SHORT).show();

                    if (LogsDetail.removeinfoActiviy) {
                        try {
                            Var.logActivity.finish();
                        } catch (Exception e) {
                            Log.e(TAG, "doSaveContact Var.logActivity.finish() " + e);
                        }

                        DialerFragment.fromKeypad = false;
                        LogsDetail.removeinfoActiviy = false;
                    }

                    if (keypadCheck) {
                        DialerFragment.fromKeypad = true;
                        keypadCheck = false;
                    }
                }

                @Override
                public void onError(Throwable e) {
                    progressDialog.dismiss();
                }

                @Override
                public void onNext(Boolean aBoolean) {
                    Log.i(TAG, "doSaveContact onNext");

                }
            });
        });

        dialog.show();
    }

    private static int SAVE_CONTACT_PHONE = 1, SAVE_CONTACT_SIM = 2;

    /**
     * Do save contact
     *
     * @param accountType int
     * @return Observable
     */
    rx.Observable<Boolean> doSaveContact(int accountType) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        contactDto = quickContactHelper.removeNullValues(contactDto);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.show();
        return Observable.create((Observable.OnSubscribe<Boolean>) subscriber -> {
            contactDto = addHelper.addFunction(ops, contactDto);
            contactDto.setRingtone(localUri.toString());

            try {
                getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

                Cursor c = getContentResolver().query(Contacts.CONTENT_URI, null, null, null, null);
                c.moveToLast();
                int contactId = c.getInt(c.getColumnIndex(Contacts._ID));
                contactDto.setContactID(contactId + "");
                c.close();

                // Save to phone as default
                Uri contactUri = Contacts.CONTENT_URI;
                if (accountType == SAVE_CONTACT_PHONE) contactUri = Contacts.CONTENT_URI;
                else if (accountType == SAVE_CONTACT_SIM)
                    contactUri = Uri.parse("content://icc/adn");

                ContentValues values = new ContentValues();
                values.put(Contacts.CUSTOM_RINGTONE, contactDto.getRingtone());
                if (accountType == SAVE_CONTACT_SIM)
                    values.put(ContactsContract.RawContacts.ACCOUNT_TYPE, "com.android.contacts.sim");
                getContentResolver().update(contactUri, values, Contacts._ID + " = ?",
                        new String[]{String.valueOf(contactDto.getContactID())});

                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                runOnUiThread(() -> imm.hideSoftInputFromWindow(fNameText.getWindowToken(), 0));

                subscriber.onNext(true);
                subscriber.onCompleted();
            } catch (Exception e) {
                progressDialog.dismiss();
                Log.e(TAG, "doSaveContact doInBackground " + e);
                returnmessage = e.getMessage();
            }
        });
    }

    /**
     * A container class used to repreresent all known information about an account.
     */
    private class AccountData {
        private String mName;
        private String mType;
        private CharSequence mTypeLabel;
        private Drawable mIcon;

        /**
         * @param name        The name of the account. This is usually the user's email address or
         *                    username.
         * @param description The description for this account. This will be dictated by the
         *                    type of account returned, and can be obtained from the system AccountManager.
         */
        public AccountData(String name, AuthenticatorDescription description) {
            mName = name;
            if (description != null) {
                mType = description.type;

                // The type string is stored in a resource, so we need to convert it into something
                // human readable.
                String packageName = description.packageName;
                PackageManager pm = getPackageManager();

                if (description.labelId != 0) {
                    mTypeLabel = pm.getText(packageName, description.labelId, null);
                    if (mTypeLabel == null) {
                        throw new IllegalArgumentException("LabelID provided, but label not found");
                    }
                } else {
                    mTypeLabel = "";
                }

                if (description.iconId != 0) {
                    mIcon = pm.getDrawable(packageName, description.iconId, null);
                    if (mIcon == null) {
                        throw new IllegalArgumentException("IconID provided, but drawable not " +
                                "found");
                    }
                } else {
                    mIcon = getResources().getDrawable(android.R.drawable.sym_def_app_icon);
                }
            }
        }

        public String getName() {
            return mName;
        }

        public String getType() {
            return mType;
        }

        public CharSequence getTypeLabel() {
            return mTypeLabel;
        }

        public Drawable getIcon() {
            return mIcon;
        }

        public String toString() {
            return mName;
        }
    }

    /**
     * Custom adapter used to display account icons and descriptions in the account spinner.
     */
    private class AccountAdapter extends ArrayAdapter<AccountData> {
        public AccountAdapter(Context context, ArrayList<AccountData> accountData) {
            super(context, android.R.layout.simple_spinner_item, accountData);
            setDropDownViewResource(R.layout.account_entry);
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            // Inflate a view template
            if (convertView == null) {
                LayoutInflater layoutInflater = getLayoutInflater();
                convertView = layoutInflater.inflate(R.layout.account_entry, parent, false);
            }
            TextView firstAccountLine = (TextView) convertView.findViewById(R.id.firstAccountLine);
            TextView secondAccountLine = (TextView) convertView.findViewById(R.id.secondAccountLine);
            ImageView accountIcon = (ImageView) convertView.findViewById(R.id.accountIcon);

            // Populate template
            AccountData data = getItem(position);
            firstAccountLine.setText(data.getName());
            secondAccountLine.setText(data.getTypeLabel());
            Drawable icon = data.getIcon();
            if (icon == null) {
                icon = getResources().getDrawable(android.R.drawable.ic_menu_search);
            }
            accountIcon.setImageDrawable(icon);
            return convertView;
        }
    }
}
