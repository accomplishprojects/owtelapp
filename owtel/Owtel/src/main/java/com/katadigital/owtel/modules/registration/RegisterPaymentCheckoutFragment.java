package com.katadigital.owtel.modules.registration;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.models.ListAreaCode;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.SharedPreferencesManager;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.security.PaymentHelper;
import com.katadigital.owtel.security.SecuredString;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.ui.adapter.AddressAdapter;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Omar Matthew Reyes on 7/13/16.
 * Payment checkout page
 */
public class RegisterPaymentCheckoutFragment extends Fragment {
    private static final String TAG = "PaymentCheckoutFragment";

    @Bind(R.id.tv_register_payment_total_amount)
    TextView mTextViewTotalAmount;
    @Bind(R.id.sv_payment_checkout)
    ScrollView mScrollView;
    @Bind(R.id.et_card_number)
    EditText etCardNumber;
    @Bind(R.id.et_card_holder)
    EditText etCardHolder;
    @Bind(R.id.layout_cc_month)
    LinearLayout layoutCcMonth;
    @Bind(R.id.spinner_cc_month)
    Spinner spinnerCardExpireMonth;
    @Bind(R.id.layout_cc_year)
    LinearLayout layoutCcYear;
    @Bind(R.id.spinner_cc_year)
    Spinner spinnerCardExpireYear;
    @Bind(R.id.et_cvv)
    EditText etCardCVV;
    @Bind(R.id.et_street)
    EditText etAddressStreet;
    @Bind(R.id.et_city)
    EditText etAddressCity;
    @Bind(R.id.et_state)
    EditText etAddressState;
    @Bind(R.id.et_zip_code)
    EditText etAddressZipCode;
    @Bind(R.id.layout_country)
    LinearLayout layoutCountry;
    @Bind(R.id.spinner_country)
    Spinner spinnerAddressCountry;
    @Bind(R.id.btn_register_payment_checkout_continue)
    Button btnPaymentCheckout;

    FragmentHolderActivity fragmentHolderActivity;
    private SubscriptionList subscription;
    private IddList idd;
    private ArrayList<String> listYear = new ArrayList<>();
    private ListAreaCode listAreaCode = new ListAreaCode();
    private static List<AddressList> countryList = new ArrayList<>();
//            , stateList = new ArrayList<>(), cityList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_register_payment_checkout, container, false);
        ButterKnife.bind(this, view);

        SharedPreferencesManager.sharedManagerInit(container.getContext());

        fragmentHolderActivity = (FragmentHolderActivity) getActivity();
        fragmentHolderActivity.mProgressDialog.dismiss();

        // Fill spinner credit card expiry month
        spinnerCardExpireMonth.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.register_payment_card_months)));

        // Fill spinner credit card expiry year
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        listYear.add(getString(R.string.register_payment_card_year));
        for (int i = 0; i < 10; i++) {
            listYear.add(Integer.toString(currentYear + i));
        }
        ArrayAdapter<String> spinnerCcYear = new ArrayAdapter<>(getActivity(),
                R.layout.simple_list_item, listYear);
        spinnerCardExpireYear.setAdapter(spinnerCcYear);
        return view;
    }

    @OnClick(R.id.btn_register_payment_checkout_continue)
    public void onClickPaymentCheckout() {
        fragmentHolderActivity.hideSoftInput(btnPaymentCheckout);
        if (validateFieldValues()) doRegistration(getArguments());
        else
            Toast.makeText(getActivity(), getString(R.string.error_fields), Toast.LENGTH_SHORT).show();
    }

    /**
     * Generate default adapter for Spinner
     *
     * @param stringList String[]
     * @return ArrayAdapter<String>
     */
    private ArrayAdapter<String> generateDefaultAdapter(String[] stringList) {
        return new ArrayAdapter<>(getActivity(), R.layout.simple_list_item, stringList);
    }

    /**
     * Check if all fields are valid
     *
     * @return boolean
     */
    private boolean validateFieldValues() {
        return isCreditCardValid(etCardNumber) &&
                isCreditCardHolderValid(etCardHolder) &&
                isCreditCardExpiryValid(spinnerCardExpireMonth, spinnerCardExpireYear) &&
                isCreditCardCvvValid(etCardCVV) &&
                isCreditCardAddressValid(etAddressStreet, etAddressCity, etAddressState,
                        spinnerAddressCountry, etAddressZipCode);
    }

    private boolean isCreditCardValid(EditText etCardNumber) {
        String cardNum = etCardNumber.getText().toString().trim();
        return !(cardNum.equals("") || cardNum.trim().isEmpty());
    }

    private boolean isCreditCardHolderValid(EditText creditCardHolder) {
        String cardHolder = creditCardHolder.getText().toString().trim();
        return !(cardHolder.equals("") || cardHolder.trim().isEmpty());
    }

    /**
     * Check if credit card expiry date is valid
     *
     * @param creditCardExpiryMonth Spinner
     * @param creditCardExpiryYear  Spinner
     * @return boolean
     */
    private boolean isCreditCardExpiryValid(Spinner creditCardExpiryMonth, Spinner creditCardExpiryYear) {
        return creditCardExpiryMonth.getSelectedItemPosition() > 0 &&
                creditCardExpiryYear.getSelectedItemPosition() > 0;
    }

    /**
     * Check if credit card cvv is valid
     *
     * @param creditCardCvv String
     * @return boolean
     */
    private boolean isCreditCardCvvValid(EditText creditCardCvv) {
        return creditCardCvv.getText().toString().trim().length() >= 3;
    }

    /**
     * Check if credit card billing address is valid
     *
     * @param etAddressStreet       EditText
     * @param etCity                EditText
     * @param etState               EditText
     * @param spinnerAddressCountry Spinner
     * @return boolean
     */
    private boolean isCreditCardAddressValid(EditText etAddressStreet, EditText etCity,
                                             EditText etState, Spinner spinnerAddressCountry,
                                             EditText etAddressZipCode) {
        return etAddressStreet.getText().toString().trim().length() > 0 &&
                etCity.getText().toString().trim().length() > 0 &&
                etState.getText().toString().trim().length() > 0 &&
                spinnerAddressCountry.getSelectedItemPosition() > 0 &&
                etAddressZipCode.getText().toString().trim().length() > 0;
    }

    /**
     * Do user registration
     *
     * @param bundle Bundle
     */
    private void doRegistration(Bundle bundle) {
        fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
        fragmentHolderActivity.mProgressDialog.show();
        StringRequest mStringRequestRegisterUser = new StringRequest(Request.Method.POST, API.URL_TEST + API.PROCESS_USER,
                response -> {
                    if(GlobalValues.JC_debug)
                    {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Delete entry")
                                .setMessage("process_user.php log no.1 "+response)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
//                        Toast.makeText(getActivity(), "doRgistration " +response, Toast.LENGTH_SHORT).show();
                    }
                    Log.i(TAG, "Register User response 2" + response);
                    try {
                        JSONObject jsonObjectMain = new JSONObject(response);
                        String jsonObjectStatus = jsonObjectMain.getString("status");
                        if (jsonObjectStatus.equals("Success")) {
                            if (!getArguments().getBoolean(Constants.BUNDLE_REGISTER_PROMO_CODE_USER)) {
                                // Premium User
                                Bundle args = new Bundle();
                                args.putString(Constants.BUNDLE_REGISTER_EMAIL, bundle.getString(Constants.BUNDLE_REGISTER_EMAIL, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_PASSWORD, bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_AREA_CODE, bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_NUMBER, bundle.getString(Constants.BUNDLE_REGISTER_NUMBER, "empty"));
                                RegisterEndFragment registerEndFragment = new RegisterEndFragment();
                                registerEndFragment.setArguments(args);
                                fragmentHolderActivity.changeFragment(registerEndFragment, false);
                            } else {
                                // Promo Code User
                                new ApiHelper(getActivity()).doLoginRequest(fragmentHolderActivity,
                                        bundle.getString(Constants.BUNDLE_REGISTER_EMAIL), bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD));
                            }
                        } else {
                            fragmentHolderActivity.mProgressDialog.dismiss();
                            String jsonObjectMessage = jsonObjectMain.getString("message");
                            fragmentHolderActivity.showAlertDialog(getActivity(),
                                    jsonObjectStatus, jsonObjectMessage,
                                    getResources().getString(R.string.string_ok));
                        }
                    } catch (Exception e) {
                        if (GlobalValues.DEBUG) Log.e(TAG, "Volley " + e);
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        Toast.makeText(fragmentHolderActivity, getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                    }
                }, error -> {
            if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
            fragmentHolderActivity.mProgressDialog.dismiss();
            DialogBuilder.displayVolleyError(getActivity(), error);
        }) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                // Default value for json params
                String month = "0", year = "0", addressCityId, addressStateId, addressCountryId = "";
                if (spinnerCardExpireMonth.getSelectedItemPosition() > 0 && spinnerCardExpireYear.getSelectedItemPosition() > 0) {
                    month = getResources().getStringArray(R.array.register_payment_card_months)[spinnerCardExpireMonth.getSelectedItemPosition()];
                    year = listYear.get(spinnerCardExpireYear.getSelectedItemPosition());
                }
                addressCityId = etAddressCity.getText().toString();
                addressStateId = etAddressState.getText().toString();
                if (countryList.size() > 1)
                    addressCountryId = countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId();
                String jsonObject = new PaymentHelper(OwtelAppController.getInstance().getApplicationContext())
                        .getSecuredJsonRegisterUser(bundle.getString(Constants.BUNDLE_REGISTER_EMAIL, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "") + "-" + bundle.getString(Constants.BUNDLE_REGISTER_NUMBER, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_LOCK_ID, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_PROMO_CODE, ""),
                                subscription.getSubscriptionId(),
                                subscription.getSubscriptionValue(),
                                idd.getIddId(),
                                idd.getIddReloadValue(),
                                (subscription.getSubscriptionValue() + idd.getIddReloadValue()),
                                etCardNumber.getText().toString().trim(),
                                etCardHolder.getText().toString().trim(),
                                etCardCVV.getText().toString().trim(),
                                month,
                                year,
                                etAddressStreet.getText().toString(),
                                addressCityId,
                                addressStateId,
                                addressCountryId,
                                etAddressZipCode.getText().toString());
                params.put(API.ENCRYPTED_OBJECT, jsonObject);
                Log.i(TAG, API.ENCRYPTED_OBJECT + " encrypted " + jsonObject);
                Log.i(TAG, API.ENCRYPTED_OBJECT + " decrypted " + SecuredString.decryptData(jsonObject));
                return params;
            }
        };
        mStringRequestRegisterUser.setTag(Constants.REQUEST_TAG_PROCESS_USER);
        OwtelAppController.getInstance().addToRequestQueue(mStringRequestRegisterUser);
    }

    public void prepareCheckout(SubscriptionList subscription, IddList idd, List<AddressList> countryList) {
        this.subscription = subscription;
        this.idd = idd;
        this.countryList = countryList;
        mTextViewTotalAmount.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                subscription.getSubscriptionValue() + idd.getIddReloadValue()));


        spinnerAddressCountry.setAdapter(new AddressAdapter(countryList, GlobalValues.GET_COUNTRY));
        spinnerAddressCountry.post(new Runnable() {
            @Override
            public void run() {
                spinnerAddressCountry.setSelection(getIndex(spinnerAddressCountry,"USA"));
            }
        });
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinnerAddressCountry);

            // Set popupWindow height to 500px
            popupWindow.setHeight(500);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
        new ApiHelper(getActivity()).generateCityState(etAddressZipCode,
                etAddressCity, etAddressState, spinnerAddressCountry, countryList,
                fragmentHolderActivity.apiService);
    }
    //private method of your class
    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().toLowerCase().contains(myString.toLowerCase())){
                index = i;
                break;
            }
        }
        return index;
    }

}
