package com.katadigital.owtel.modules.main.contacts;


import android.content.Context;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.AutoCompleteTextView;

import java.util.HashMap;

public class CustomAutoCompleteTextView extends AutoCompleteTextView {

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
    }

    public CustomAutoCompleteTextView(Context context) {
        super(context);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
    }

        @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        /** Each item in the autocompetetextview suggestion list is a hashmap object */
        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
        return hm.get("phoneNumber");
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final Layout layout = getLayout();
        if (layout != null) {
            final int lineCount = layout.getLineCount();
            if (lineCount > 0) {
                final int ellipsisCount = layout.getEllipsisCount(lineCount - 1);
                if (ellipsisCount > 0) {

                    final float textSize = getTextSize();

                    // textSize is already expressed in pixels
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, (textSize - 1));

                    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                }
            }
        }
    }

}

//public class CustomAutoCompleteTextView
//import java.util.HashMap;
//
//import android.content.Context;
//import android.util.AttributeSet;
//import android.widget.AutoCompleteTextView;
//
///**
// * Customizing AutoCompleteTextView to return Country Name
// * corresponding to the selected item
// */
//public class CustomAutoCompleteTextView extends AutoCompleteTextView {
//
//    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    /**
//     * Returns the country name corresponding to the selected item
//     */
//    @Override
//    protected CharSequence convertSelectionToString(Object selectedItem) {
//        /** Each item in the autocompetetextview suggestion list is a hashmap object */
//        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
//        return hm.get("phoneNumber");
//    }
//}
