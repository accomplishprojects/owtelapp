package com.katadigital.owtel.modules.main.messaging.adapters;//package app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.daoDb.Message;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.tools.MyTools;
import com.katadigital.owtel.utils.StringFormatter;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class ConversationListAdapter extends RecyclerView.Adapter<ViewHolder> {

//    private SparseBooleanArray mCheckedItems = new SparseBooleanArray();

    private static final String TAG = "ConversationAdapter";
    Context context;

    List<Message> messageList;

    public ConversationListAdapter(Context context, List<Message> messageList) {
        this.context = context;
        this.messageList = messageList;
    }

    public void updateData(List<Message> messageList) {
        this.messageList = messageList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_item_conversation_list, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Message message = messageList.get(position);
        viewHolder.convo_item_contact_name.setText(message.getDisplay_name());
        viewHolder.convo_item_message.setText(message.getValue());
        viewHolder.convo_item_date.setText(new StringFormatter().getFormattedDateTime(message.getCreated()));
        if (MyTools.intToBool(Integer.parseInt(message.getIs_message_read()))){
            viewHolder.convo_item_is_new_message.setVisibility(View.INVISIBLE);
        }else {
            viewHolder.convo_item_is_new_message.setVisibility(View.VISIBLE);
        }
        Log.d(TAG, "onBindViewHolder: " + message.getDisplay_name());
        Log.d(TAG, "onBindViewHolder: " + message.getIs_message_read());
//        else {
//            mCheckedItems.put(position, false);
//        }
//        if(mCheckedItems.get(position))
//        {
//        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public Message getMessage(int position) {
        return messageList.get(position);
    }
}

class ViewHolder extends RecyclerView.ViewHolder {
    public TextView convo_item_contact_name;
    public TextView convo_item_message;
    public TextView convo_item_date;
    public ImageView convo_item_is_new_message;

    public ViewHolder(View itemView) {
        super(itemView);
        convo_item_contact_name = (TextView) itemView.findViewById(R.id.convo_item_contact_name);
        convo_item_message = (TextView) itemView.findViewById(R.id.convo_item_message);
        convo_item_date = (TextView) itemView.findViewById(R.id.convo_item_date);
        convo_item_is_new_message = (ImageView) itemView.findViewById(R.id.new_message_indicator);
    }

}


//
//
//package com.katadigital.owtel.modules.main.messaging.adapters;//package app.adapter;
//
//        import android.content.Context;
//        import android.support.v7.util.DiffUtil;
//        import android.support.v7.widget.RecyclerView;
//        import android.util.Log;
//        import android.util.SparseBooleanArray;
//        import android.view.LayoutInflater;
//        import android.view.View;
//        import android.view.ViewGroup;
//        import android.widget.ImageView;
//        import android.widget.TextView;
//
//        import com.kata.phone.R;
//        import com.katadigital.owtel.daoDb.Message;
//        import com.katadigital.owtel.diffCallback.MessageDiffCallback;
//        import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
//        import com.katadigital.owtel.tools.MyTools;
//        import com.katadigital.owtel.utils.StringFormatter;
//
//        import java.util.List;
//
//        import rx.Subscriber;
//        import rx.android.schedulers.AndroidSchedulers;
//        import rx.schedulers.Schedulers;
//
//
//public class ConversationListAdapter extends RecyclerView.Adapter<ViewHolder> {
//
////    private SparseBooleanArray mCheckedItems = new SparseBooleanArray();
//
//    private static final String TAG = "ConversationAdapter";
//    Context context;
//
//    List<Message> messageList;
//
//    public ConversationListAdapter(Context context, List<Message> messageList) {
//        this.context = context;
//        this.messageList = messageList;
//    }
//
//    public void updateData(List<Message> messageList) {
//        this.messageList = messageList;
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
//        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.recycler_view_item_conversation_list, viewGroup, false);
//        ViewHolder viewHolder = new ViewHolder(v);
//        return viewHolder;
//
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder viewHolder, int position) {
//        Message message = messageList.get(position);
//        viewHolder.convo_item_contact_name.setText(message.getDisplay_name());
//        viewHolder.convo_item_message.setText(message.getValue());
//        viewHolder.convo_item_date.setText(new StringFormatter().getFormattedDateTime(message.getCreated()));
//        if (MyTools.intToBool(Integer.parseInt(message.getIs_message_read()))){
//            viewHolder.convo_item_is_new_message.setVisibility(View.INVISIBLE);
//        }else {
//            viewHolder.convo_item_is_new_message.setVisibility(View.VISIBLE);
//        }
//        Log.d(TAG, "onBindViewHolder: " + message.getDisplay_name());
//        Log.d(TAG, "onBindViewHolder: " + message.getIs_message_read());
////        else {
////            mCheckedItems.put(position, false);
////        }
////        if(mCheckedItems.get(position))
////        {
////        }
//    }
//
//
//    public void swapItems(List<Message> messages) {
//        final MessageDiffCallback diffCallback = new MessageDiffCallback(this.messageList, messages);
//        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
//
//        this.messageList.clear();
//        this.messageList.addAll(messages);
//        diffResult.dispatchUpdatesTo(this);
//    }
//
//
//    @Override
//    public int getItemCount() {
//        return messageList.size();
//    }
//
//    public Message getMessage(int position) {
//        return messageList.get(position);
//    }
//}
//
//class ViewHolder extends RecyclerView.ViewHolder {
//    public TextView convo_item_contact_name;
//    public TextView convo_item_message;
//    public TextView convo_item_date;
//    public ImageView convo_item_is_new_message;
//
//    public ViewHolder(View itemView) {
//        super(itemView);
//        convo_item_contact_name = (TextView) itemView.findViewById(R.id.convo_item_contact_name);
//        convo_item_message = (TextView) itemView.findViewById(R.id.convo_item_message);
//        convo_item_date = (TextView) itemView.findViewById(R.id.convo_item_date);
//        convo_item_is_new_message = (ImageView) itemView.findViewById(R.id.new_message_indicator);
//    }
//
//}




