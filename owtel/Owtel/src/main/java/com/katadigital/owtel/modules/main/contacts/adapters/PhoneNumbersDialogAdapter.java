package com.katadigital.owtel.modules.main.contacts.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

public class PhoneNumbersDialogAdapter extends BaseAdapter {
    private Activity context;
    private ArrayList<PhoneNumberDto> phoneNumbers;

    public PhoneNumbersDialogAdapter(Activity context,
                                     ArrayList<PhoneNumberDto> phoneNumbers) {

        super();
        this.context = context;

        this.phoneNumbers = new ArrayList<PhoneNumberDto>();
        this.phoneNumbers.addAll(phoneNumbers);
    }

    @Override
    public int getCount() {
        return phoneNumbers.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.numberlist_dialog_item,
                viewGroup, false);
        TextView detailNumberType = (TextView) rowView
                .findViewById(R.id.detail_number_type2);
        TextView detailNumber = (TextView) rowView
                .findViewById(R.id.detail_number2);

        detailNumberType.setTypeface(NewUtil.getFontRoman(context));
        detailNumber.setTypeface(NewUtil.getFontRoman(context));

        detailNumberType.setTextSize(NewUtil.gettxtSize());
        detailNumber.setTextSize(NewUtil.gettxtSize());

        if (phoneNumbers.get(i) != null) {
            PhoneNumberDto phoneNumberDto = phoneNumbers.get(i);
            detailNumberType.setText(phoneNumberDto.getNumberType());
            detailNumber.setText(phoneNumberDto.getNumber());
        }

        return rowView;
    }
}
