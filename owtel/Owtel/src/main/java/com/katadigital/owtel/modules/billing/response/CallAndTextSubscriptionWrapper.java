package com.katadigital.owtel.modules.billing.response;

/**
 * Created by MIS on 4/30/2016.
 */
public class CallAndTextSubscriptionWrapper {

    private String email_address;

    private String subscription_id;
    private double subscription_price;
    private String card_number;
    private String card_holder;
    private String cc_cvv;
    private String cc_month;
    private String cc_year;
    private String street;
    private String city_id;
    private String state_id;
    private String country_id;
    private String zip_code;
    private String payment_type;

    private String idd_top_up_id;
    private double idd_top_up_price;

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public double getSubscription_price() {
        return subscription_price;
    }

    public void setSubscription_price(double subscription_price) {
        this.subscription_price = subscription_price;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_holder() {
        return card_holder;
    }

    public void setCard_holder(String card_holder) {
        this.card_holder = card_holder;
    }

    public String getCc_cvv() {
        return cc_cvv;
    }

    public void setCc_cvv(String cc_cvv) {
        this.cc_cvv = cc_cvv;
    }

    public String getCc_month() {
        return cc_month;
    }

    public void setCc_month(String cc_month) {
        this.cc_month = cc_month;
    }

    public String getCc_year() {
        return cc_year;
    }

    public void setCc_year(String cc_year) {
        this.cc_year = cc_year;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getIdd_top_up_id() {
        return idd_top_up_id;
    }

    public void setIdd_top_up_id(String idd_top_up_id) {
        this.idd_top_up_id = idd_top_up_id;
    }

    public double getIdd_top_up_price() {
        return idd_top_up_price;
    }

    public void setIdd_top_up_price(double idd_top_up_price) {
        this.idd_top_up_price = idd_top_up_price;
    }
}
