package com.katadigital.owtel.modules.registration;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.security.PaymentHelper;
import com.katadigital.owtel.security.SecuredString;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.ui.adapter.AddressAdapter;
import com.katadigital.ui.adapter.IddAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Omar Matthew Reyes on 4/12/16.
 * Payment Page
 */
public class RegisterPaymentFragment extends Fragment implements PaymentInteractor {
    private final static String TAG = "RegisterPaymentFragment";

    @Bind(R.id.tv_register_payment_subscription_type)
    TextView textViewSubscriptionType;
    @Bind(R.id.tv_register_payment_subscription_amount)
    TextView textViewSubscriptionPrice;
    @Bind(R.id.spinner_register_payment_idd_amount)
    Spinner spinnerIDDPrices;
    @Bind(R.id.tv_register_payment_total_amount)
    TextView textViewPaymentTotal;
    @Bind(R.id.et_card_number)
    EditText etCardNumber;
    @Bind(R.id.et_card_holder)
    EditText etCardHolder;
    @Bind(R.id.layout_cc_month)
    LinearLayout layoutCcMonth;
    @Bind(R.id.spinner_cc_month)
    Spinner spinnerCardExpireMonth;
    @Bind(R.id.layout_cc_year)
    LinearLayout layoutCcYear;
    @Bind(R.id.spinner_cc_year)
    Spinner spinnerCardExpireYear;
    @Bind(R.id.et_cvv)
    EditText etCardCVV;
    @Bind(R.id.et_street)
    EditText etAddressStreet;
    @Bind(R.id.et_city)
    EditText etAddressCity;
    @Bind(R.id.et_state)
    EditText etAddressState;
    @Bind(R.id.et_zip_code)
    EditText etAddressZipCode;
    @Bind(R.id.layout_country)
    LinearLayout layoutCountry;
    @Bind(R.id.spinner_country)
    Spinner spinnerAddressCountry;

    FragmentHolderActivity fragmentHolderActivity;
    private List<SubscriptionList> listSubscription = new ArrayList<>();
    private List<IddList> listIdd = new ArrayList<>();
    private ArrayList<String> listYear = new ArrayList<>();
    private boolean isPromoUser = false;
    private List<AddressList> countryList = new ArrayList<>(), stateList = new ArrayList<>(), cityList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_register_payment, container, false);
        ButterKnife.bind(this, view);

        fragmentHolderActivity = (FragmentHolderActivity) getActivity();
        fragmentHolderActivity.mProgressDialog.dismiss();

        Bundle bundlePrev = getArguments();
        isPromoUser = getArguments().getString(Constants.BUNDLE_REGISTER_PROMO_CODE, "").trim().length() > 0;
//        listSubscription = Parcels.unwrap(getArguments().getParcelable(Constants.PARCEL_LIST_SUBSCRIPTION));
//        listIdd = Parcels.unwrap(getArguments().getParcelable(Constants.PARCEL_LIST_IDD));

//        textViewSubscriptionType.setText(listSubscription.get(0).getSubscriptionName());
//        textViewSubscriptionPrice.setText(listSubscription.get(0).getSubscriptionPrice());
//        spinnerIDDPrices.setAdapter(new IddAdapter(listIdd));

        if (listSubscription.size() > 0 && listIdd.size() > 0)
            textViewPaymentTotal.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                    (listSubscription.get(0).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue())));

//        getSubscriptionPrices();    // Get Subscription prices
//        getIddList();   // Get IDD prices
        if (GlobalValues.DEBUG) {
            Log.i(TAG, "BUNDLE RP Email: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL, "empty_email"));
            Log.i(TAG, "BUNDLE RP Password: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD, "empty_pass"));
            Log.i(TAG, "BUNDLE RP Area Code: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "empty_area_code"));
            Log.i(TAG, "BUNDLE RP Number: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_NUMBER, "empty_us_number"));
            Log.i(TAG, "BUNDLE RP Promo Code: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_PROMO_CODE, "empty_promo_code"));
            Log.i(TAG, "BUNDLE RP Promo Code User: " + isPromoUser);
            Log.i(TAG, "BUNDLE RP Lock ID: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_LOCK_ID, "empty_lock_id"));
        }

        // Fill spinner credit card expiry month
        spinnerCardExpireMonth.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.register_payment_card_months)));

        // Fill spinner credit card expiry year
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        listYear.add(getString(R.string.register_payment_card_year));
        for (int i = 0; i < 10; i++) {
            listYear.add(Integer.toString(currentYear + i));
        }
        ArrayAdapter<String> spinnerCcYear = new ArrayAdapter<>(getActivity(),
                R.layout.simple_list_item, listYear);
        spinnerCardExpireYear.setAdapter(spinnerCcYear);

//        spinnerAddressCity.setEnabled(false);
//        spinnerAddressCity.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.spinner_city_default)));
//        spinnerAddressState.setEnabled(false);
//        spinnerAddressState.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.spinner_state_default)));

        getAddressList("", "", GlobalValues.GET_COUNTRY);

        return view;
    }

    @OnItemSelected(R.id.spinner_register_payment_idd_amount)
    public void onItemSelectIdd(int position) {
        Log.i(TAG, "Spinner IDD position " + position);
        Log.i(TAG, "Spinner IDD id: " + listIdd.get(position).getIddId() +
                " price " + listIdd.get(position).getIddReloadPrice() +
                " value " + listIdd.get(position).getIddReloadValue());
        if (listSubscription.size() > 0 && listIdd.size() > 0)
            textViewPaymentTotal.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                    (listSubscription.get(0).getSubscriptionValue() + listIdd.get(position).getIddReloadValue())));
    }

//    @OnItemSelected(R.id.spinner_country)
//    public void onItemSelectCountry(int position) {
//        Log.i(TAG, "Spinner Country position " + position);
//        if (position > 0) {
//            getAddressList(countryList.get(position).getCountryId(), "", GlobalValues.GET_STATE);
//            spinnerAddressState.setEnabled(true);
//        } else {
//            spinnerAddressState.setEnabled(false);
//            spinnerAddressState.setSelection(0);
//            spinnerAddressCity.setEnabled(false);
//            spinnerAddressCity.setSelection(0);
//        }
//    }
//
//    @OnItemSelected(R.id.spinner_state)
//    public void onItemSelectState(int position) {
//        if (position > 0) {
//            getAddressList(countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId(),
//                    stateList.get(position).getStateId(), GlobalValues.GET_CITY);
//            spinnerAddressCity.setEnabled(true);
//        } else {
//            spinnerAddressCity.setEnabled(false);
//            spinnerAddressCity.setSelection(0);
//        }
//    }
//
//    @OnItemSelected(R.id.spinner_city)
//    public void onItemSelectCity(int position) {
//        if (position > 0) Log.i(TAG, "Selected City: " + cityList.get(position).getCity());
//    }

    @OnClick(R.id.btn_register_payment_continue)
    public void onClickContinue() {
//        String month = "0", year = "0", addressCityId = "", addressStateId = "", addressCountryId = "";
//        if(spinnerCardExpireMonth.getSelectedItemPosition() > 0 && spinnerCardExpireYear.getSelectedItemPosition() > 0){
//            month = getResources().getStringArray(R.array.register_payment_card_months)[spinnerCardExpireMonth.getSelectedItemPosition()];
//            year = listYear.get(spinnerCardExpireYear.getSelectedItemPosition());
//        }
//        if(cityList.size() > 1) addressCityId = cityList.get(spinnerAddressCity.getSelectedItemPosition()).getCityId();
//        if(stateList.size() > 1) addressStateId = stateList.get(spinnerAddressState.getSelectedItemPosition()).getStateId();
//        if(countryList.size() > 1) addressCityId = countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId();
//        Bundle bundle = getArguments();
//        String jsonObject = new PaymentHelper(OwtelAppController.getInstance().getApplicationContext())
//                .getSecuredJsonRegisterUser(bundle.getString(Constants.BUNDLE_REGISTER_EMAIL, ""),
//                        bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD, ""),
//                        bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "") + "-" + bundle.getString(Constants.BUNDLE_REGISTER_NUMBER, ""),
//                        bundle.getString(Constants.BUNDLE_REGISTER_LOCK_ID, ""),
//                        bundle.getString(Constants.BUNDLE_REGISTER_PROMO_CODE, ""),
//                        listSubscription.get(0).getSubscriptionId(),
//                        listSubscription.get(0).getSubscriptionValue(),
//                        listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddId(),
//                        listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue(),
//                        (listSubscription.get(0).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue()),
//                        etCardNumber.getText().toString().trim(),
//                        etCardHolder.getText().toString().trim(),
//                        etCardCVV.getText().toString().trim(),
//                        month,
//                        year,
//                        etAddressStreet.getText().toString(),
//                        addressCityId,
//                        addressStateId,
//                        addressCountryId,
//                        etAddressZipCode.getText().toString());
//        Log.i(TAG, "json encrypted " + jsonObject);
//        Log.i(TAG, "json decrypted " + SecuredString.decryptData(jsonObject));
        if (validateFieldValues()) {
            doRegistration(getArguments());
        } else if (isPromoUser && spinnerIDDPrices.getSelectedItemPosition() == 0) {
            doRegistration(getArguments());
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_fields), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Check if all fields are valid
     *
     * @return boolean
     */
    private boolean validateFieldValues() {
        return isCreditCardValid(etCardNumber) &&
                isCreditCardHolderValid(etCardHolder) &&
                isCreditCardExpiryValid(spinnerCardExpireMonth, spinnerCardExpireYear) &&
                isCreditCardCvvValid(etCardCVV) &&
                isCreditCardAddressValid(etAddressStreet, etAddressCity, etAddressState,
                        spinnerAddressCountry, etAddressZipCode);
    }

    private boolean isCreditCardValid(EditText etCardNumber) {
        String cardNum = etCardNumber.getText().toString().trim();
        return !(cardNum.equals("") || cardNum.trim().isEmpty());
    }

    private boolean isCreditCardHolderValid(EditText creditCardHolder) {
        String cardHolder = creditCardHolder.getText().toString().trim();
        return !(cardHolder.equals("") || cardHolder.trim().isEmpty());
    }

    /**
     * Check if credit card expiry date is valid
     *
     * @param creditCardExpiryMonth Spinner
     * @param creditCardExpiryYear  Spinner
     * @return boolean
     */
    private boolean isCreditCardExpiryValid(Spinner creditCardExpiryMonth, Spinner creditCardExpiryYear) {
        return creditCardExpiryMonth.getSelectedItemPosition() > 0 &&
                creditCardExpiryYear.getSelectedItemPosition() > 0;
    }

    /**
     * Check if credit card cvv is valid
     *
     * @param creditCardCvv String
     * @return boolean
     */
    private boolean isCreditCardCvvValid(EditText creditCardCvv) {
        return creditCardCvv.getText().toString().trim().length() >= 3;
    }

    /**
     * Check if credit card billing address is valid
     *
     * @param etAddressStreet       EditText
     * @param etCity                EditText
     * @param etState               EditText
     * @param spinnerAddressCountry Spinner
     * @return boolean
     */
    private boolean isCreditCardAddressValid(EditText etAddressStreet, EditText etCity,
                                             EditText etState, Spinner spinnerAddressCountry,
                                             EditText etAddressZipCode) {
        return etAddressStreet.getText().toString().trim().length() > 0 &&
                etCity.getText().toString().trim().length() > 0 &&
                etState.getText().toString().trim().length() > 0 &&
                spinnerAddressCountry.getSelectedItemPosition() > 0 &&
                etAddressZipCode.getText().toString().trim().length() > 0;
    }

    /**
     * Do user registration
     *
     * @param bundle Bundle
     */
    private void doRegistration(Bundle bundle) {
        fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
        fragmentHolderActivity.mProgressDialog.show();
        StringRequest mStringRequestRegisterUser = new StringRequest(Request.Method.POST, API.URL_TEST + API.PROCESS_USER,
                response -> {
                    if (GlobalValues.JC_debug) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Delete entry")
                                .setMessage("process_user.php log no.2 " + response)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                    Log.i(TAG, "Register User response 3" + response);
                    try {
                        JSONObject jsonObjectMain = new JSONObject(response);
                        String jsonObjectStatus = jsonObjectMain.getString("status");
                        if (jsonObjectStatus.equals("Success")) {
                            if (!isPromoUser) {
                                // Premium User
                                Bundle args = new Bundle();
                                args.putString(Constants.BUNDLE_REGISTER_EMAIL, bundle.getString(Constants.BUNDLE_REGISTER_EMAIL, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_PASSWORD, bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_AREA_CODE, bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_NUMBER, bundle.getString(Constants.BUNDLE_REGISTER_NUMBER, "empty"));
                                RegisterEndFragment registerEndFragment = new RegisterEndFragment();
                                registerEndFragment.setArguments(args);
                                fragmentHolderActivity.changeFragment(registerEndFragment, false);
                            } else {
                                // Promo Code User
                                new ApiHelper(getActivity()).doLoginRequest(fragmentHolderActivity,
                                        bundle.getString(Constants.BUNDLE_REGISTER_EMAIL), bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD));
                            }
                        } else {
                            fragmentHolderActivity.mProgressDialog.dismiss();
                            String jsonObjectMessage = jsonObjectMain.getString("message");
                            fragmentHolderActivity.showAlertDialog(getActivity(),
                                    jsonObjectStatus, jsonObjectMessage,
                                    getResources().getString(R.string.string_ok));
                        }
                    } catch (Exception e) {
                        if (GlobalValues.DEBUG) Log.e(TAG, "Volley " + e);
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        Toast.makeText(fragmentHolderActivity, getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                    }
                }, error -> {
            if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
            fragmentHolderActivity.mProgressDialog.dismiss();
            DialogBuilder.displayVolleyError(getActivity(), error);
        }) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                // Default value for json params
                String month = "0", year = "0", addressCityId = "", addressStateId = "", addressCountryId = "";
                if (spinnerCardExpireMonth.getSelectedItemPosition() > 0 && spinnerCardExpireYear.getSelectedItemPosition() > 0) {
                    month = getResources().getStringArray(R.array.register_payment_card_months)[spinnerCardExpireMonth.getSelectedItemPosition()];
                    year = listYear.get(spinnerCardExpireYear.getSelectedItemPosition());
                }
                addressCityId = etAddressCity.getText().toString();
                addressStateId = etAddressState.getText().toString();
//                if(cityList.size() > 1) addressCityId = cityList.get(spinnerAddressCity.getSelectedItemPosition()).getCityId();
//                if(stateList.size() > 1) addressStateId = stateList.get(spinnerAddressState.getSelectedItemPosition()).getStateId();
                if (countryList.size() > 1)
                    addressCountryId = countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId();
                String jsonObject = new PaymentHelper(OwtelAppController.getInstance().getApplicationContext())
                        .getSecuredJsonRegisterUser(bundle.getString(Constants.BUNDLE_REGISTER_EMAIL, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "") + "-" + bundle.getString(Constants.BUNDLE_REGISTER_NUMBER, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_LOCK_ID, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_PROMO_CODE, ""),
                                listSubscription.get(0).getSubscriptionId(),
                                listSubscription.get(0).getSubscriptionValue(),
                                listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddId(),
                                listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue(),
                                (listSubscription.get(0).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue()),
                                etCardNumber.getText().toString().trim(),
                                etCardHolder.getText().toString().trim(),
                                etCardCVV.getText().toString().trim(),
                                month,
                                year,
                                etAddressStreet.getText().toString(),
                                addressCityId,
                                addressStateId,
                                addressCountryId,
                                etAddressZipCode.getText().toString());
                params.put(API.ENCRYPTED_OBJECT, jsonObject);
                Log.i(TAG, API.ENCRYPTED_OBJECT + " encrypted " + jsonObject);
                Log.i(TAG, API.ENCRYPTED_OBJECT + " decrypted " + SecuredString.decryptData(jsonObject));
                return params;
            }
        };
        mStringRequestRegisterUser.setTag(Constants.REQUEST_TAG_PROCESS_USER);
        OwtelAppController.getInstance().addToRequestQueue(mStringRequestRegisterUser);
    }

    /**
     * Generate default adapter for Spinner
     *
     * @param stringList String[]
     * @return ArrayAdapter<String>
     */
    private ArrayAdapter<String> generateDefaultAdapter(String[] stringList) {
        return new ArrayAdapter<>(getActivity(), R.layout.simple_list_item, stringList);
    }

    /**
     * Get Address List
     *
     * @param countryId String
     * @param stateId   String
     * @param type      int
     */
    public void getAddressList(String countryId, String stateId, int type) {
        fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
        fragmentHolderActivity.mProgressDialog.show();
        switch (type) {
            case GlobalValues.GET_COUNTRY:
                countryList = new ArrayList<>();
                break;
            case GlobalValues.GET_STATE:
                stateList = new ArrayList<>();
                break;
            case GlobalValues.GET_CITY:
                cityList = new ArrayList<>();
                break;
        }
        StringRequest mStringRequestAddressList = new StringRequest(Request.Method.POST, API.URL_TEST + API.ADDRESS_LIST,
                response -> {
                    Log.i(TAG, "Address List response " + response);
                    try {
                        doGetAddressList(response, type)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Subscriber<List<AddressList>>() {
                                    @Override
                                    public void onCompleted() {
                                        fragmentHolderActivity.mProgressDialog.dismiss();
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onNext(List<AddressList> addresses) {
                                        for (AddressList address : addresses) {
                                            Log.i(TAG, "Country " + address.getCountry() +
                                                    " State " + address.getState() +
                                                    " City " + address.getCity());
                                        }
                                        switch (type) {
                                            case GlobalValues.GET_COUNTRY:
                                                spinnerAddressCountry.setAdapter(new AddressAdapter(addresses, type));
                                                spinnerAddressCountry.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        spinnerAddressCountry.setSelection(getIndex(spinnerAddressCountry, "USA"));
                                                    }
                                                });
                                                try {
                                                    Field popup = Spinner.class.getDeclaredField("mPopup");
                                                    popup.setAccessible(true);

                                                    // Get private mPopup member variable and try cast to ListPopupWindow
                                                    android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinnerAddressCountry);

                                                    // Set popupWindow height to 500px
                                                    popupWindow.setHeight(500);
                                                } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
                                                    // silently fail...
                                                }
                                                break;
                                            case GlobalValues.GET_STATE:
//                                                spinnerAddressState.setAdapter(new AddressAdapter(addresses, type));
                                                break;
                                            case GlobalValues.GET_CITY:
//                                                spinnerAddressCity.setAdapter(new AddressAdapter(addresses, type));
                                                break;
                                        }
                                    }
                                });
                    } catch (Exception e) {
                        if (GlobalValues.DEBUG) Log.e(TAG, "Volley " + e);
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        Toast.makeText(fragmentHolderActivity, getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                    }
                }, error -> {
            if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
            fragmentHolderActivity.mProgressDialog.dismiss();
            DialogBuilder.displayVolleyError(getActivity(), error);
        }) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put(API.REGISTER_COUNTRY_ID, countryId);
                params.put(API.REGISTER_STATE_ID, stateId);
                return params;
            }
        };
        mStringRequestAddressList.setTag(Constants.REQUEST_TAG_GET_ADDRESS_LIST);
        OwtelAppController.getInstance().addToRequestQueue(mStringRequestAddressList);
    }

    private Observable<List<AddressList>> doGetAddressList(String response, int type) {
        return Observable.create(new Observable.OnSubscribe<List<AddressList>>() {
            @Override
            public void call(Subscriber<? super List<AddressList>> subscriber) {
                try {
                    JSONObject jsonObjectMain = new JSONObject(response);
                    String jsonObjectStatus = jsonObjectMain.getString("status");
                    if (jsonObjectStatus.equals("Success")) {
                        JSONArray jsonObjectAddressList = jsonObjectMain.getJSONArray("address_list");
                        AddressList addressDefault = new AddressList();
                        // Set default value at 0
                        switch (type) {
                            case GlobalValues.GET_COUNTRY:
                                addressDefault.setCountry(getString(R.string.country));
                                addressDefault.setCountryId("");
                                countryList.add(addressDefault);
                                break;
                            case GlobalValues.GET_STATE:
                                addressDefault.setState(getString(R.string.state));
                                addressDefault.setStateId("");
                                stateList.add(addressDefault);
                                break;
                            case GlobalValues.GET_CITY:
                                addressDefault.setCity(getString(R.string.city));
                                addressDefault.setCityId("");
                                cityList.add(addressDefault);
                                break;
                        }
                        for (int i = 0; i < jsonObjectAddressList.length(); i++) {
                            AddressList address = new AddressList();
                            String jsonObjectCountryId, jsonObjectStateId, jsonObjectCityId,
                                    jsonObjectCountryName, jsonObjectStateName, jsonObjectCityName;
                            try {
                                switch (type) {
                                    case GlobalValues.GET_COUNTRY:
                                        jsonObjectCountryName = jsonObjectAddressList.getJSONObject(i).getString("country_name");
                                        jsonObjectCountryId = jsonObjectAddressList.getJSONObject(i).getString("country_id");
                                        address.setCountry(jsonObjectCountryName);
                                        address.setCountryId(jsonObjectCountryId);
                                        countryList.add(address);
                                        break;
                                    case GlobalValues.GET_STATE:
                                        jsonObjectStateName = jsonObjectAddressList.getJSONObject(i).getString("state_name");
                                        jsonObjectStateId = jsonObjectAddressList.getJSONObject(i).getString("state_id");
                                        address.setState(jsonObjectStateName);
                                        address.setStateId(jsonObjectStateId);
                                        stateList.add(address);
                                        break;
                                    case GlobalValues.GET_CITY:
                                        jsonObjectCityName = jsonObjectAddressList.getJSONObject(i).getString("city_name");
                                        jsonObjectCityId = jsonObjectAddressList.getJSONObject(i).getString("city_id");
                                        address.setCity(jsonObjectCityName);
                                        address.setCityId(jsonObjectCityId);
                                        cityList.add(address);
                                        break;
                                }
                            } catch (JSONException e) {
                                Log.e(TAG, "doGetAddressList " + e);
                            }
                        }
                    } else {
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        String jsonObjectMessage = jsonObjectMain.getString("message");
                        fragmentHolderActivity.showAlertDialog(getActivity(),
                                jsonObjectStatus, jsonObjectMessage,
                                getResources().getString(R.string.string_ok));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "doGetAddressList " + e);
                }

                switch (type) {
                    case GlobalValues.GET_COUNTRY:
                        subscriber.onNext(countryList);
                        break;
                    case GlobalValues.GET_STATE:
                        subscriber.onNext(stateList);
                        break;
                    case GlobalValues.GET_CITY:
                        subscriber.onNext(cityList);
                        break;
                }
                subscriber.onCompleted();
            }
        });
    }

    /**
     * Set Subscription
     *
     * @param listSubscription List<SubscriptionList>
     */
    public void setSubscription(List<SubscriptionList> listSubscription) {
        this.listSubscription = listSubscription;
        textViewSubscriptionType.setText(listSubscription.get(0).getSubscriptionName());
        textViewSubscriptionPrice.setText(listSubscription.get(0).getSubscriptionPrice());
    }

    /**
     * Set IDD
     *
     * @param listIdd List<IddList>
     */
    public void setIdd(List<IddList> listIdd) {
        this.listIdd = listIdd;
        spinnerIDDPrices.setAdapter(new IddAdapter(listIdd));
    }

    @Override
    public boolean validEtCardNumber(boolean valid) {
        if (valid)
            etCardNumber.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else etCardNumber.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validEtCardHolder(boolean valid) {
        if (valid)
            etCardHolder.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else etCardHolder.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validSpinnerMonth(boolean valid) {
        if (valid)
            layoutCcMonth.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else layoutCcMonth.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validSpinnerYear(boolean valid) {
        if (valid)
            layoutCcYear.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else layoutCcYear.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validEtCvv(boolean valid) {
        if (valid) etCardCVV.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else etCardCVV.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validEtStreet(boolean valid) {
        if (valid)
            etAddressStreet.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else
            etAddressStreet.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validEtCity(boolean valid) {
        if (valid)
            etAddressCity.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else etAddressCity.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validEtState(boolean valid) {
        if (valid)
            etAddressState.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else etAddressState.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validEtZipCode(boolean valid) {
        if (valid)
            etAddressZipCode.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else
            etAddressZipCode.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    @Override
    public boolean validSpinnerCountry(boolean valid) {
        if (valid)
            layoutCountry.setBackground(getResources().getDrawable(R.drawable.et_drawable_grey));
        else layoutCountry.setBackground(getResources().getDrawable(R.drawable.et_drawable_error));
        return valid;
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().toLowerCase().contains(myString.toLowerCase())) {
                index = i;
                break;
            }
        }
        return index;
    }
}
