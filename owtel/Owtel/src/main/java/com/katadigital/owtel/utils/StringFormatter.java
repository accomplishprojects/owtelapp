package com.katadigital.owtel.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.models.VerySimpleContact;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Omar Matthew Reyes on 4/5/16.
 * Class containing methods mainly used for formatting Strings
 */
public class StringFormatter {
    private static final String countryCode = "+1";
    private static final String TAG = "StringFormatter";

    /**
     * Get US Number format
     * +1 2XX-XXX-XXXX
     *
     * @param areaCode String
     * @param usNumber String
     * @return String
     */
    public static String formatUsNumber(String areaCode, String usNumber) {
        String usNumberPart1 = "", usNumberPart2 = "";
//        if (usNumber != null) {
//            usNumberPart1 = usNumber.substring(0, 3);
//            usNumberPart2 = usNumber.substring(3, usNumber.length()-1);
//        }
        String formatCheker = countryCode + " " + areaCode + "-" + usNumber;
        formatCheker.replaceAll("\\-\\-", "-");
        return formatCheker;
    }

    /**
     * Get US Number format
     * +1 2XX-XXX-XXXX
     *
     * @param usNumber String
     * @return String
     */
    public static String formatUsNumber(String usNumber) {
        if (usNumber.trim().length() > 0) {
            String usNumberPart1, usNumberPart2, usNumberPart3;
            usNumberPart1 = usNumber.substring(0, 3);
            usNumberPart2 = usNumber.substring(3, 6);
            usNumberPart3 = usNumber.substring(6, 10);
            return countryCode + " " + usNumberPart1 + "-" + usNumberPart2 + "-" + usNumberPart3;
        } else return "";
    }

    /**
     * Format price to [Currency]##.##
     *
     * @param currency String
     * @param price    double
     * @return String
     */
    public static String formatCurrency(String currency, double price) {
        return currency + new DecimalFormat("##0.00").format(price);
    }

    /**
     * Get Contact Display Name
     *
     * @param contactNumber String
     * @return String
     */
    public static rx.Observable<String> doGetContactName(String contactNumber) {
        ContentResolver resolver = OwtelAppController.getInstance().getContentResolver();
        return rx.Observable.create(subscriber -> {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactNumber));
            Cursor cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
            if (cursor == null) {
                if (GlobalValues.DEBUG) Log.e(TAG, "doGetContactName cursor null");
            }
            String contactName = null;
            assert cursor != null;
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }

            if (!cursor.isClosed()) {
                cursor.close();
            }
            //send number instead if null
            if (contactName == null)
                subscriber.onNext(contactNumber);
            else
                subscriber.onNext(contactName);
            subscriber.onCompleted();
        });
    }

    public String getContactNameViaMainThread(String contactNumber) {

        VerySimpleContact verySimpleContact=new VerySimpleContact();
        String contactName = null;
        try {
            ContentResolver resolver = OwtelAppController.getInstance().getContentResolver();
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactNumber));
//            Log.e(TAG, "getContactDetailsViaMainThread: " + contactNumber);
            Cursor cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

            if (cursor == null) {
                if (GlobalValues.DEBUG) Log.e(TAG, "doGetContactName cursor null");
            }

            assert cursor != null;
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }

            if (!cursor.isClosed()) {
                cursor.close();
            }
//        if (contactNumber.equals("18008804188"))
//            return "Owtel SMS";
            //send number instead if null
        } catch (Exception e) {
            Log.e(TAG, "getContactDetailsViaMainThread: " + e.getMessage());
        }
        if (contactName == null)
            return contactNumber;
        else
            return contactName;

    }

    public VerySimpleContact getContactDetailsViaMainThread(String contactNumber) {

        VerySimpleContact verySimpleContact=new VerySimpleContact();
        String contactName = null;
        try {
            ContentResolver resolver = OwtelAppController.getInstance().getContentResolver();
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactNumber));
//            Log.e(TAG, "getContactDetailsViaMainThread: " + contactNumber);
            Cursor cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

            if (cursor == null) {
                if (GlobalValues.DEBUG) Log.e(TAG, "doGetContactName cursor null");
            }

            assert cursor != null;
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }

            if (!cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "getContactDetailsViaMainThread: " + e.getMessage());
        }
            verySimpleContact.setNumber(contactNumber);
            verySimpleContact.setName(contactName);


        return verySimpleContact;
    }


    private SimpleDateFormat sDateFormat, sDateFormatTime, sDateFormatDayOfWeek;

    public StringFormatter() {
        this.sDateFormatDayOfWeek = new SimpleDateFormat("EEEE", Locale.getDefault());
        this.sDateFormat = new SimpleDateFormat("M/dd/yy", Locale.getDefault());
        this.sDateFormatTime = new SimpleDateFormat("hh:mm a", Locale.getDefault());
    }

    /**
     * Convert API UTC Time String to Unix Time long
     *
     * @param utcDateTime String
     * @return long
     */
    public long getUtcTimeToUnixTime(String utcDateTime) {
        return formatStrUtcToDate(utcDateTime).getTime();
    }

    /**
     * Format date time from API response
     *
     * @param utcDateTime String
     * @return String
     */
    public String getFormattedDateTime(String utcDateTime) {
        return formatDateTime(formatStrUtcToDate(utcDateTime));
    }

    /**
     * Parse date time format from API
     * yyyy/mm/dd hh:mm:ss
     *
     * @param utcDateTime String
     * @return Date
     */
    public Date formatStrUtcToDate(String utcDateTime) {
        String[] strDateTime = utcDateTime.split(" ");
        String[] strDate = strDateTime[0].split("-");
        String[] strTime = strDateTime[1].split(":");

        TimeZone timeZone = TimeZone.getTimeZone("GMT");   // Set timezone to GMT(server time)
        Calendar cal = Calendar.getInstance(timeZone);
        cal.set(Calendar.YEAR, Integer.parseInt(strDate[0]));
        cal.set(Calendar.MONTH, Integer.parseInt(strDate[1]) - 1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(strDate[2]));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strTime[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(strTime[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(strTime[2]));
        return cal.getTime();
    }

    public String formatDateTime(Date dateTime) {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("M/dd/yy", Locale.getDefault());
        String formattedDate = sDateFormat.format(dateTime);
        String formattedTime = sDateFormatTime.format(dateTime);
        String curDate = sDateFormat.format(Calendar.getInstance().getTime());
        if (curDate.equals(formattedDate)) {
            if (formattedTime.startsWith("0"))
                return formattedTime.substring(1);
            else
                return formattedTime;
        } else {
            return getYesterdayToday(dateTime);
        }
    }

    public String getYesterdayToday(Date newDate) {
        String yesterdayToday;
        String formattedTime = sDateFormatTime.format(newDate);
        Calendar newsDate = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        Calendar yesDate = Calendar.getInstance();

        newsDate.setTime(newDate);
        currentDate.setTime(new Date());
        yesDate.setTime(new Date());

        yesDate.add(Calendar.DAY_OF_MONTH, -1);

        String strNDate = newsDate.get(Calendar.MONTH) + 1 + " "
                + newsDate.get(Calendar.DAY_OF_MONTH) + ", "
                + newsDate.get(Calendar.YEAR);
        String strCDate = currentDate.get(Calendar.MONTH) + 1 + " "
                + currentDate.get(Calendar.DAY_OF_MONTH) + ", "
                + currentDate.get(Calendar.YEAR);
        String strYDate = yesDate.get(Calendar.MONTH) + 1 + " "
                + yesDate.get(Calendar.DAY_OF_MONTH) + ", "
                + yesDate.get(Calendar.YEAR);

        if (strNDate.equals(strCDate)) {
            yesterdayToday = OwtelAppController.getInstance().getString(R.string.string_today) + " " + formattedTime;
        } else if (strNDate.equals(strYDate)) {
            yesterdayToday = OwtelAppController.getInstance().getString(R.string.string_yesterday) + " " + formattedTime;
        } else {
            Date cdate = new Date();
            Long diff = cdate.getTime() - newDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (days < 6)
                yesterdayToday = sDateFormatDayOfWeek.format(newDate) + " " + formattedTime;
            else yesterdayToday = sDateFormat.format(newDate);
        }
        return yesterdayToday;
    }
}
