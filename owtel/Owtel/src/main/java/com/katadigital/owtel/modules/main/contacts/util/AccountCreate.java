package com.katadigital.owtel.modules.main.contacts.util;

import android.graphics.drawable.Drawable;

public class AccountCreate {

    //http://stackoverflow.com/questions/9907751/android-update-a-contact

    private String name;
    private String name2;
    private String actype;
    private Drawable draw;


    public AccountCreate(String name, String name2, String actype, Drawable draw) {
        this.name = name;
        this.name2 = name2;
        this.actype = actype;
        this.draw = draw;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getDraw() {
        return draw;
    }

    public void setDraw(Drawable draw) {
        this.draw = draw;
    }

    public String getActype() {
        return actype;
    }

    public void setActype(String actype) {
        this.actype = actype;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

}
