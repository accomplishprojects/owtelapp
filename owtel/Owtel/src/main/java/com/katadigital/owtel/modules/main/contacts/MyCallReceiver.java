package com.katadigital.owtel.modules.main.contacts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.katadigital.owtel.helpers.Var;
import com.katadigital.ui.custombadger.ShortcutBadger;

public class MyCallReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = "MyCallReceiver";
    static boolean isRinging = false;
    static boolean isReceived = false;
    static String callerPhoneNumber;
    public static int missedCount = 0;

    Context context;

    @Override
    public void onReceive(Context mContext, Intent intent) {

        Var.addLike = false;
        this.context = mContext;

        // Get current phone state
        String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if (state == null)
            return;

        // phone is ringing
        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            isRinging = true;
            // get caller's number
            Bundle bundle = intent.getExtras();
            callerPhoneNumber = bundle.getString("incoming_number");
        }

        // phone is received
        if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            isReceived = true;
        }

        // phone is idle
        if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            // detect missed call
            if (isRinging && !isReceived) {

                // Toast.makeText(mContext,
                // "Got a missed call from : " + callerPhoneNumber,
                // Toast.LENGTH_LONG).show();
//				addShortcutIcon();
                missedCount = missedCount + 1;
                setBadge(mContext, missedCount);
                System.out.println("BERTAX MISSED CALL COUNT NEW " + missedCount);
            }
        }
    }

    public static void setBadge(Context mContext, int num) {
        if (mContext == null) {
            Log.e(TAG, "mContext is NULL");
            return;

        }
        try {

            Log.i(TAG, "Set ICON Badge " + 3);
            Log.i(TAG, "Set ICON Badge1 " + num);
            Log.i(TAG, "Set ICON Badge2 " + mContext.getClass().getSimpleName());

            ShortcutBadger.setBadge(mContext, 3);
            ShortcutBadger.with(mContext).count(3); // 2016_01_12 Removed app icon badger
        } catch (Exception e) {
            Log.e(TAG, "Set Badge Exception " + e);
        }
    }
}
