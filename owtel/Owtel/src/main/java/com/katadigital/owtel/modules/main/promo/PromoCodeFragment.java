package com.katadigital.owtel.modules.main.promo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.kata.phone.R;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.Message;
import com.katadigital.owtel.models.PromoCodes;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.owtel.tools.MyTools;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.modules.main.contacts.entities.PromoCodeDao;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.ui.recyclerview.PromoCodeRecyclerViewAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Omar Matthew Reyes on 4/14/16.
 * Displays Promo Codes list for Premium users
 */
public class PromoCodeFragment extends Fragment {
    public final static String TAG = "PromoCodeFragment";
    private MainActivity mainActivity;
    private FragmentHolderActivity fragmentHolderActivity;
    private SharedPreferences preferencesUser;

    RecyclerView recyclerViewPromoCode;
    SwipeRefreshLayout swipeRefreshLayoutPromoCode;
    Bundle bundlePrev;
    boolean fromRegistration = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ButterKnife.bind(getActivity());
        View view = inflater.inflate(R.layout.fragment_promo_code, container, false);

        bundlePrev = getArguments();
        if (bundlePrev != null)
            fromRegistration = bundlePrev.getBoolean(Constants.BUNDLE_PROMO_CODE_FROM_REGISTER, false);

        // Instantiate MainActivity
        if (fromRegistration) fragmentHolderActivity = (FragmentHolderActivity) getActivity();
        else mainActivity = (MainActivity) getActivity();
        preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();

        swipeRefreshLayoutPromoCode = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayoutPromoCode.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        recyclerViewPromoCode = (RecyclerView) view.findViewById(R.id.rv_promo_code);
        recyclerViewPromoCode.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewPromoCode.setLayoutManager(mLinearLayoutManager);

        swipeRefreshLayoutPromoCode.setOnRefreshListener(() -> getUserPromoCodes());

        // Fragment popBackStack()
        ImageButton mButtonBack = (ImageButton) view.findViewById(R.id.btn_settings_back);
        TextView mTextViewShareSkip = (TextView) view.findViewById(R.id.tv_share_skip);
        if (fromRegistration) {
            fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.progress_promo_codes));
            fragmentHolderActivity.mProgressDialog.show();
            mButtonBack.setVisibility(View.INVISIBLE);
            mTextViewShareSkip.setVisibility(View.VISIBLE);
            final String email = bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL, "");
            final String pass = bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD, "");
            mTextViewShareSkip.setOnClickListener(v -> new ApiHelper(getActivity()).doLoginRequest(fragmentHolderActivity,
                    email, pass));
        } else {
            mainActivity.mProgressDialog.setMessage(getResources().getString(R.string.progress_promo_codes));
            mainActivity.mProgressDialog.show();
            mButtonBack.setOnClickListener(v -> mainActivity.popFragment());
            mButtonBack.setVisibility(View.VISIBLE);
            mTextViewShareSkip.setVisibility(View.INVISIBLE);
        }

        getUserPromoCodes();

        return view;
    }

    private void getUserPromoCodes() {

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiService.SERVICE_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        ApiService service = retrofit.create(ApiService.class);

        String email;
        if (fromRegistration)
            email = bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL, "");
        else
            email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty");


        service.getPromoCodes(email).enqueue(new Callback<PromoCodes>() {
            @Override
            public void onResponse(Response<PromoCodes> response, Retrofit retrofit) {
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    PromoCodeRecyclerViewAdapter mAdapter = new PromoCodeRecyclerViewAdapter(response.body().getPromoCodeList(), fromRegistration, getActivity());
                    recyclerViewPromoCode.setAdapter(mAdapter);
                } else {
                    if (fromRegistration) fragmentHolderActivity.mProgressDialog.dismiss();
                    else mainActivity.mProgressDialog.dismiss();
                    swipeRefreshLayoutPromoCode.setRefreshing(false);
                    if (fromRegistration)
                        fragmentHolderActivity.showAlertDialog(getActivity(),
                                response.body().getStatus(), response.body().getMessage(),
                                getResources().getString(R.string.string_ok));
                    else DialogBuilder.showAlertDialog(getActivity(),
                            response.body().getStatus(), response.body().getMessage(),
                            getResources().getString(R.string.string_ok));
                }

                if (fromRegistration) fragmentHolderActivity.mProgressDialog.dismiss();
                else mainActivity.mProgressDialog.dismiss();

                swipeRefreshLayoutPromoCode.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {
                if (GlobalValues.DEBUG)
                    Log.e(TAG, "That didn't work! Error: " + t.getLocalizedMessage());
                if (fromRegistration) fragmentHolderActivity.mProgressDialog.dismiss();
                else mainActivity.mProgressDialog.dismiss();
                swipeRefreshLayoutPromoCode.setRefreshing(false);
//            DialogBuilder.displayVolleyError(getActivity(), t.getLocalizedMessage());
            }
        });

    }


//        if (jsonObjectStatus.equals("Success")) {
//            JSONArray jsonObjectNumbersAvailable = jsonObjectMain.getJSONArray("promo_code_list");
//            for (int i = 0; i < jsonObjectNumbersAvailable.length(); i++) {
//                PromoCodeDao promoCode = new PromoCodeDao();
//                String jsonObjectPromoCodeId = jsonObjectNumbersAvailable.getJSONObject(i).getString("promo_code_id");
//                String jsonObjectPromoCode = jsonObjectNumbersAvailable.getJSONObject(i).getString("promo_code");
//                String jsonObjectPromoCodeUser = jsonObjectNumbersAvailable.getJSONObject(i).getString("used_by");
//                String jsonObjectPromoCodeUserNumber = jsonObjectNumbersAvailable.getJSONObject(i).getString("owtel_number");
//                String jsonObjectPromoCodeUserEmail = jsonObjectNumbersAvailable.getJSONObject(i).getString("email_address");
//                promoCode.setUserPromoCodeId(jsonObjectPromoCodeId);
//                promoCode.setUserPromoCode(jsonObjectPromoCode);
//                // FIXME: 11/16/16  JC FIX
//                if (jsonObjectPromoCodeUser != null)
//                    promoCode.setUserPromoCodeUser(jsonObjectPromoCodeUser);
//                else
//                    promoCode.setUserPromoCodeUser("0");
//                promoCode.setUserPromoCodeUserNumber(jsonObjectPromoCodeUserNumber);
//                promoCode.setUserPromoCodeUserEmail(jsonObjectPromoCodeUserEmail);
//                promoCodesList.add(promoCode);
//            }
//        } else {
//            if (fromRegistration) fragmentHolderActivity.mProgressDialog.dismiss();
//            else mainActivity.mProgressDialog.dismiss();
//            swipeRefreshLayoutPromoCode.setRefreshing(false);
//            String jsonObjectMessage = jsonObjectMain.getString("message");
//            if (fromRegistration)
//                fragmentHolderActivity.showAlertDialog(getActivity(),
//                        jsonObjectStatus, jsonObjectMessage,
//                        getResources().getString(R.string.string_ok));
//            else DialogBuilder.showAlertDialog(getActivity(),
//                    jsonObjectStatus, jsonObjectMessage,
//                    getResources().getString(R.string.string_ok));
//        }


//        StringRequest mStringRequestPromoCodes = new StringRequest(Request.Method.POST, API.URL_TEST + API.PROMO_CODES, response ->
//                doGetPromoCodesList(response)
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<PromoCodeDao>>() {
//                    @Override
//                    public void onCompleted() {
//                        if (fromRegistration) fragmentHolderActivity.mProgressDialog.dismiss();
//                        else mainActivity.mProgressDialog.dismiss();
//
//                        swipeRefreshLayoutPromoCode.setRefreshing(false);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onNext(List<PromoCodeDao> promoCodesList) {
//                        PromoCodeRecyclerViewAdapter mAdapter = new PromoCodeRecyclerViewAdapter(promoCodesList, fromRegistration, getActivity());
//                        recyclerViewPromoCode.setAdapter(mAdapter);
//                    }
//                })
//                , error -> {
//            if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
//            if (fromRegistration) fragmentHolderActivity.mProgressDialog.dismiss();
//            else mainActivity.mProgressDialog.dismiss();
//            swipeRefreshLayoutPromoCode.setRefreshing(false);
//            DialogBuilder.displayVolleyError(getActivity(), error);
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                final Map<String, String> params = new HashMap<>();
//                if (fromRegistration)
//                    params.put(API.FORGOT_PASSWORD_EMAIL, bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL, ""));
//                else
//                    params.put(API.FORGOT_PASSWORD_EMAIL, preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty"));
//                return params;
//            }
//        };
//        mStringRequestPromoCodes.setRetryPolicy(new DefaultRetryPolicy(
//                GlobalValues.REQUEST_TIMEOUT,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mStringRequestPromoCodes.setTag(Constants.REQUEST_TAG_FORGOT_PASS);
//        OwtelAppController.getInstance().addToRequestQueue(mStringRequestPromoCodes);


//    private Observable<List<PromoCodeDao>> doGetPromoCodesList(String response) {
//        Log.i(TAG, "doGetPromoCodesList response " + response);
//        return rx.Observable.create(new rx.Observable.OnSubscribe<List<PromoCodeDao>>() {
//            @Override
//            public void call(Subscriber<? super List<PromoCodeDao>> subscriber) {
//                List<PromoCodeDao> promoCodesList = new ArrayList<>();
//                try {
//                    JSONObject jsonObjectMain = new JSONObject(response);
//                    String jsonObjectStatus = jsonObjectMain.getString("status");
//                    if (jsonObjectStatus.equals("Success")) {
//                        JSONArray jsonObjectNumbersAvailable = jsonObjectMain.getJSONArray("promo_code_list");
//                        for (int i = 0; i < jsonObjectNumbersAvailable.length(); i++) {
//                            PromoCodeDao promoCode = new PromoCodeDao();
//                            String jsonObjectPromoCodeId = jsonObjectNumbersAvailable.getJSONObject(i).getString("promo_code_id");
//                            String jsonObjectPromoCode = jsonObjectNumbersAvailable.getJSONObject(i).getString("promo_code");
//                            String jsonObjectPromoCodeUser = jsonObjectNumbersAvailable.getJSONObject(i).getString("used_by");
//                            String jsonObjectPromoCodeUserNumber = jsonObjectNumbersAvailable.getJSONObject(i).getString("owtel_number");
//                            String jsonObjectPromoCodeUserEmail = jsonObjectNumbersAvailable.getJSONObject(i).getString("email_address");
//                            promoCode.setUserPromoCodeId(jsonObjectPromoCodeId);
//                            promoCode.setUserPromoCode(jsonObjectPromoCode);
//                            // FIXME: 11/16/16  JC FIX
//                            if (jsonObjectPromoCodeUser != null)
//                                promoCode.setUserPromoCodeUser(jsonObjectPromoCodeUser);
//                            else
//                                promoCode.setUserPromoCodeUser("0");
//                            promoCode.setUserPromoCodeUserNumber(jsonObjectPromoCodeUserNumber);
//                            promoCode.setUserPromoCodeUserEmail(jsonObjectPromoCodeUserEmail);
//                            promoCodesList.add(promoCode);
//                        }
//                    } else {
//                        if (fromRegistration) fragmentHolderActivity.mProgressDialog.dismiss();
//                        else mainActivity.mProgressDialog.dismiss();
//                        swipeRefreshLayoutPromoCode.setRefreshing(false);
//                        String jsonObjectMessage = jsonObjectMain.getString("message");
//                        if (fromRegistration)
//                            fragmentHolderActivity.showAlertDialog(getActivity(),
//                                    jsonObjectStatus, jsonObjectMessage,
//                                    getResources().getString(R.string.string_ok));
//                        else DialogBuilder.showAlertDialog(getActivity(),
//                                jsonObjectStatus, jsonObjectMessage,
//                                getResources().getString(R.string.string_ok));
//                    }
//                } catch (Exception e) {
//                    Log.e(TAG, "doGetPromoCodesList " + e);
//                }
//
//                subscriber.onNext(promoCodesList);
//                subscriber.onCompleted();
//            }
//        });
//    }
}

