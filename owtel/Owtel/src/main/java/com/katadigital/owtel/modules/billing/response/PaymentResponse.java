package com.katadigital.owtel.modules.billing.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MIS on 4/30/2016.
 */
public class PaymentResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("expiration_date")
    private String expiration_date;
    @SerializedName("subscription_status")
    private String subscription_status;
    @SerializedName("idd_credits")
    private double iddCredits;
    private String balance;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getSubscription_status() {
        return subscription_status;
    }

    public void setSubscription_status(String subscription_status) {
        this.subscription_status = subscription_status;
    }

    public double getIddCredits() {
        return iddCredits;
    }

    public void setIddCredits(double iddCredits) {
        this.iddCredits = iddCredits;
    }
}
