package com.katadigital.owtel.dagger2;

import android.database.sqlite.SQLiteDatabase;

import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.DaoMaster;
import com.katadigital.owtel.daoDb.DaoSession;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.dao.query.QueryBuilder;

@Module
public class StorageModule {

    private final OwtelAppController app;

    private final String DATABASE_NAME = "owtelline-db";

    public StorageModule(OwtelAppController app){
        this.app = app;
    }

    @Singleton
    @Provides
    DaoMaster.DevOpenHelper provideDevOpenHelper(){
        return new DaoMaster.DevOpenHelper(app, DATABASE_NAME ,null);
    }

    @Singleton
    @Provides
    SQLiteDatabase provideSQLiteDatabase(DaoMaster.DevOpenHelper helper){
        return helper.getWritableDatabase();
    }

    @Singleton
    @Provides
    DaoMaster provideDaoMaster(SQLiteDatabase db){
        return new DaoMaster(db);
    }

    @Singleton
    @Provides
    DaoSession provideDaoSession(DaoMaster daoMaster){
        QueryBuilder.LOG_SQL = GlobalValues.DEBUG;
        QueryBuilder.LOG_VALUES = GlobalValues.DEBUG;
        return daoMaster.newSession();
    }
}
