package com.katadigital.owtel.modules.main.contacts;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Item;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Row;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Section;
import com.katadigital.owtel.modules.main.contacts.adapters.TestSectionedAdapter;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.tools.Logger;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.portsip.utilities.androidcomponent.service.SipSettings;
import com.katadigital.ui.pinnedheader.PinnedHeaderListView;
import com.katadigital.ui.softkeyboard.SoftKeyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class ContactsFragment extends Fragment implements
        LoaderCallbacks<Cursor>, View.OnClickListener {
    private static final String TAG = "ContactsFragment";

    View rootView;
    private SimpleCursorAdapter mAdapter = null;
    public static String mCurrentFilter = null;
//    ArrayList<String> contactsArray;

    public static EditText searchContacts;

    QuickContactHelper quickContactHelper;
    private ProgressDialog progressDialog;
    String searchedName = "";

    // ALPHABET LISTVIEW VARIABLES
    private List<Object[]> alphabet = new ArrayList<>();
    private HashMap<String, Integer> sections = new HashMap<>();
    private int sideIndexHeight;
    private static float sideIndexX;
    private static float sideIndexY;
    private int indexListSize;

    ArrayList<Integer> contactDtoIDList = new ArrayList<>();

    String[] let = {"#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
            "X", "Y", "Z",};

    static InputMethodManager imm;
    int contactId;

    static boolean isMyNumbershown = false;
    static boolean isSearchClick = false;
    public static boolean isKeyboardVisible = false;

    TextView mButtonCancel;
    TextView mTextViewSearch;
    RelativeLayout layoutContactHeader;
    RelativeLayout layoutContactSearchHeader;
    PinnedHeaderListView pinnedHeaderListViewContacts;

    ImageButton mImageButtonContactsAdd, mImageButtonMore;


    @Override
    public View onCreateView(final LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_contacts, container, false);
        setupViews();

        if (getActivity().toString().contains("MainActivity"))
            ((MainActivity) getActivity()).currentTab = 2;


        searchContacts.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    searchContacts.setVisibility(View.VISIBLE);
                    mTextViewSearch.setVisibility(View.GONE);
                    searchContacts.requestFocus();
                    isSearchClick = true;
                }
            }
            return false;
        });

        pinnedHeaderListViewContacts.setOnTouchListener((v, event) -> {
            // imm.hideSoftInputFromWindow(searchContacts.getWindowToken(),
            // 0);
            // isSearchClick = true;
            return false;
        });

        try {
            quickContactHelper = new QuickContactHelper(getActivity());
            getLoaderManager().initLoader(0, null, this);

            mAdapter = new IndexedListAdapter(this.getActivity(),
                    R.layout.list_item_contacts, null, new String[]{
                    ContactsContract.Contacts.DISPLAY_NAME,
                    Contacts._ID}, new int[]{R.id.display_name});
        } catch (Exception e) {
            Log.e(TAG, "invoke IndexListAdapter " + e);
        }

        return rootView;
    }

    private void setupViews() {

//        contactsArray = new ArrayList<>();

        /**Initialize Views*/
        layoutContactHeader = (RelativeLayout) rootView.findViewById(R.id.contacts_actionbar);
        mTextViewSearch = (TextView) rootView.findViewById(R.id.txt_search_contacts);
        mButtonCancel = (TextView) rootView.findViewById(R.id.cancel);
        searchContacts = (EditText) rootView.findViewById(R.id.search_contacts);
        layoutContactSearchHeader = (RelativeLayout) rootView.findViewById(R.id.layout_search);
        pinnedHeaderListViewContacts = (PinnedHeaderListView) rootView.findViewById(R.id.contact_list);
        mImageButtonContactsAdd = (ImageButton) rootView.findViewById(R.id.btn_contacts_add);
        mImageButtonMore = (ImageButton) rootView.findViewById(R.id.btn_contacts_more);

        /**OnClick listeners*/
        mImageButtonContactsAdd.setOnClickListener(this);
        mImageButtonMore.setOnClickListener(this);
        mTextViewSearch.setOnClickListener(this);
        mButtonCancel.setOnClickListener(this);
        layoutContactSearchHeader.setOnClickListener(this);

        /**Setting up fonts*/
        mButtonCancel.setTypeface(NewUtil.getFontRoman(getActivity()));
        mTextViewSearch.setTypeface(NewUtil.getFontRoman(getActivity()));
        searchContacts.setTypeface(NewUtil.getFontRoman(getActivity()));


        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.rootView);
        SoftKeyboard softKeyboard = new SoftKeyboard(layout, imm);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {
            @Override
            public void onSoftKeyboardHide() {
                Log.i(TAG, "Expand SearchBox from SFKC false");
//                getActivity().runOnUiThread(() -> expandSearchEditText(false));
            }

            @Override
            public void onSoftKeyboardShow() {
//                getActivity().runOnUiThread(() -> expandSearchEditText(true));
            }
        });

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        searchContacts.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                ContactsFragment.this.mAdapter.getFilter().filter(cs);
                searchedName = cs + "";
                if (GlobalValues.DEBUG) Log.i(TAG, "Search query: " + cs);

                mCurrentFilter = (!TextUtils.isEmpty(searchedName) ? searchedName : null);
                getLoaderManager().restartLoader(0, null, ContactsFragment.this);

//                new AsyncTaskContactsSearch(getActivity(), cs.toString(), mCurrentFilter, ContactsFragment.this.mAdapter,
//                        getLoaderManager(), ContactsFragment.this).execute();
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        searchContacts.setOnEditorActionListener((v, actionId, event) -> {
            if (event != null
                    && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                InputMethodManager in = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);

                in.hideSoftInputFromWindow(
                        searchContacts.getApplicationWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                return true;
            }
            return false;
        });

        pinnedHeaderListViewContacts.setOnItemClickListener((myAdapter, myView, myItemInt, mylng) -> {
            if (contactDtoIDList.get(myItemInt) > 0) {
                if (progressDialog == null) {
                    if (GlobalValues.DEBUG)
                        Log.i(TAG, "Contact: " + searchContacts.getText().toString() + " pos: " + myItemInt);
                    ContactDetailActivity.searchValue = searchContacts.getText().toString();
                    ContactDetailActivity.postionlv = myItemInt;
                    new Logger().appendLog("Contact: " + searchContacts.getText().toString() + "\npos: " + myItemInt);
                    new loadContactDetails(contactDtoIDList.get(myItemInt)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
//        mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
//            public Cursor runQuery(CharSequence constraint) {
//                return getDirectoryList(constraint);
//            }
//        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!ContactDetailActivity.searchValue.isEmpty()) {
            Log.i(TAG, "Expand SearchBox from onResume true");
            expandSearchEditText(true);
            pinnedHeaderListViewContacts.setSelection(ContactDetailActivity.postionlv - 1);

            searchContacts.setText(ContactDetailActivity.searchValue);
            layoutContactHeader.setVisibility(View.GONE);
            mButtonCancel.setVisibility(View.VISIBLE);
            searchContacts.setVisibility(View.VISIBLE);
            mTextViewSearch.setVisibility(View.GONE);
            // searchContacts.requestFocus();
            int textLength = searchContacts.getText().length();
            searchContacts.setSelection(textLength, textLength);

        } else {
            Log.i(TAG, "Expand SearchBox from onResume false");
            expandSearchEditText(false);
            pinnedHeaderListViewContacts.setSelection(ContactDetailActivity.postionlv - 1);
        }
    }

    @Override
    public void onPause() {
        isSearchClick = false;
        searchContacts.setText("");
//        expandSearchEditText(false);
        super.onPause();
    }

    /**
     * Expand Contacts search EditText if SoftInputKeyboard is shown
     *
     * @param keyboardVisible boolean
     */
    private void expandSearchEditText(boolean keyboardVisible) {
        Log.i(TAG, "Expand SearchBox " + keyboardVisible);
        isKeyboardVisible = keyboardVisible;
        if (keyboardVisible) {
            layoutContactHeader.setVisibility(View.GONE);
            mButtonCancel.setVisibility(View.VISIBLE);
            searchContacts.setVisibility(View.VISIBLE);
            mTextViewSearch.setVisibility(View.GONE);
            searchContacts.requestFocus();
        } else {
            if (isSearchClick) {
                if (searchContacts.getText().toString().trim().length() == 0) {
                    searchContacts.setText("");
                    isSearchClick = false;
                }
            } else {
                if (searchContacts.getText().toString().trim().length() > 0) {
                    isSearchClick = false;
                } else {
                    layoutContactHeader.setVisibility(View.VISIBLE);
                    mButtonCancel.setVisibility(View.GONE);
                    searchContacts.setVisibility(View.GONE);
                    mTextViewSearch.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void openAddContacts() {
        NewUtil.createContactChooseDiag(getActivity());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
        Uri baseUri;
        if (mCurrentFilter != null) {
            baseUri = Uri.withAppendedPath(Contacts.CONTENT_FILTER_URI, Uri.encode(mCurrentFilter));
        } else {
            baseUri = Contacts.CONTENT_URI;
        }

        String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + Contacts.DISPLAY_NAME + " != '' ))";

        String[] projection = new String[]{Contacts._ID,
                Contacts.DISPLAY_NAME, Contacts.CONTACT_STATUS,
                Contacts.CONTACT_PRESENCE, Contacts.PHOTO_ID,
                Contacts.LOOKUP_KEY,};

        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        return new CursorLoader(getActivity(), baseUri, projection, select, null, sortOrder);

    }

    /**
     * Creates the Loader used to load contact data filtered by the given Query
     * String.
     * http://stackoverflow.com/questions/13065983/search-contacts-using-
     * name-as-well-as-company-like-in-default-android-2-3-3-con
     */
    private Loader<Cursor> createLoaderFiltered(String theQueryString) {

        final String[] COLS = new String[]{"contact_id",
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME};

        final String LIKE = " LIKE '%" + theQueryString + "%'";

        final Uri URI = ContactsContract.Data.CONTENT_URI;

        final String PHONE_MIME = Phone.CONTENT_ITEM_TYPE;

        final String WHERE = "(" + Phone.NUMBER + LIKE + ")" + " AND "
                + ContactsContract.Contacts.DISPLAY_NAME + " NOT " + LIKE;

        final String SORT = ContactsContract.Contacts.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";

        return new CursorLoader(getActivity(), URI, COLS, WHERE, null, SORT);

    }

    // http://stackoverflow.com/questions/9496350/pick-a-number-and-name-from-contacts-list-in-android-app
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        if (GlobalValues.DEBUG) Log.i(TAG, "onLoadFinished");
        String getSimNumber;
        AlphabetListAdapter adapter = new AlphabetListAdapter(getActivity());
        alphabet = new ArrayList<>();
        sections = new HashMap<>();

        List<Row> rows = new ArrayList<>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Object[] tmpIndexItem;
        Pattern numberPattern = Pattern.compile("[0-9]");
        Pattern specialPattern = Pattern.compile("[^A-Za-z-0-9]");
        contactDtoIDList = new ArrayList<>();

        TestSectionedAdapter secAdapter;
        ArrayList<String> listDataHeader = new ArrayList<>();
        HashMap<String, List<String>> listDataChild = new HashMap<>();
        List<String> items = new ArrayList<>();

//        TelephonyManager telephoneManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
//        getSimNumber = telephoneManager.getLine1Number();
//        String stringMyNumber = getResources().getString(
//                R.string.string_mynumber);
//
//        if (getSimNumber != null) {
//            if (getSimNumber.length() != 0) {
//                String myPhoneNumber = stringMyNumber + " " + getSimNumber;
//                listDataHeader.add(myPhoneNumber);
//                listDataChild.put(myPhoneNumber, items);
//                contactDtoIDList.add(0);
//            }
//        }

        String myPhoneNumber = getString(R.string.dialer_number_user) + " " + StringFormatter.formatUsNumber(new SipSettings().getSipUsername());
        listDataHeader.add(myPhoneNumber);
        listDataChild.put(myPhoneNumber, items);
        contactDtoIDList.add(0);

        if (data != null)   // Proceed if Cursor data is not null
            while (data.moveToNext()) {
                String contactDisplayName = data.getString(data.getColumnIndex(Contacts.DISPLAY_NAME));
                String firstLetter = "";

                contactId = data.getInt(data.getColumnIndex(Contacts._ID));

                if (contactDisplayName != null)
                    firstLetter = contactDisplayName.substring(0, 1).toUpperCase();

                // Group numbers together in the scroller
                if (numberPattern.matcher(firstLetter).matches()) firstLetter = "#";

                // If we've changed to a new letter, add the previous letter to the alphabet scroller
                if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                    end = rows.size() - 1;
                    tmpIndexItem = new Object[3];
                    tmpIndexItem[0] = previousLetter.toUpperCase();
                    tmpIndexItem[1] = start;
                    tmpIndexItem[2] = end;
                    alphabet.add(tmpIndexItem);
                    start = end + 1;
                }

                // Check if we need to add a header row
                if (!firstLetter.equals(previousLetter)) {
                    rows.add(new Section(firstLetter));
                    sections.put(firstLetter, start);
                    contactDtoIDList.add(0);
                    listDataHeader.add(firstLetter);
                    items = new ArrayList<>();
                }

                // Add the country to the list
                contactDtoIDList.add(contactId);
                items.add(contactDisplayName);
                rows.add(new Item(contactDisplayName));
                if (!firstLetter.equals(previousLetter)) {
                    listDataChild.put(firstLetter, items);
                }
                previousLetter = firstLetter;

                if (previousLetter != null) {
                    // Save the last letter
                    tmpIndexItem = new Object[3];
                    tmpIndexItem[0] = previousLetter.toUpperCase();
                    tmpIndexItem[1] = start;
                    tmpIndexItem[2] = rows.size() - 1;
                    alphabet.add(tmpIndexItem);
                }
            }

        adapter.setRows(rows);
        secAdapter = new TestSectionedAdapter(getActivity(), listDataHeader, listDataChild);
        pinnedHeaderListViewContacts.setAdapter(secAdapter);

        updateList();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_contacts_add:
                openAddContacts();
                break;

            case R.id.btn_contacts_more:
                DialogBuilder.showDialogMore(getActivity());
                break;

            case R.id.txt_search_contacts:
                Log.i(TAG, "Expand SearchBox from mTVS true");
                expandSearchEditText(true);
                searchContacts.setVisibility(View.VISIBLE);
                mTextViewSearch.setVisibility(View.GONE);
                searchContacts.requestFocus();

                imm.showSoftInput(searchContacts,
                        InputMethodManager.SHOW_IMPLICIT);
                break;

            case R.id.cancel:
                Log.i(TAG, "Expand SearchBox from mBC false");
                expandSearchEditText(false);
                imm.hideSoftInputFromWindow(searchContacts.getWindowToken(), 0);
                searchContacts.setText("");
                isSearchClick = false;
                break;

            case R.id.layout_search:
                Log.i(TAG, "Expand SearchBox from LCSH true");
                expandSearchEditText(true);
                searchContacts.setVisibility(View.VISIBLE);
                mTextViewSearch.setVisibility(View.GONE);
                searchContacts.requestFocus();
                imm.showSoftInput(searchContacts,
                        InputMethodManager.SHOW_IMPLICIT);
                break;
        }

    }

    class IndexedListAdapter extends SimpleCursorAdapter implements
            SectionIndexer {

        // AlphabetIndexer alphaIndexer;

        public IndexedListAdapter(Context context, int layout, Cursor c,
                                  String[] from, int[] to) {
            super(context, layout, c, from, to, 0);
        }

        @Override
        public Cursor swapCursor(Cursor c) {
            try {
                return super.swapCursor(c);
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        public int getPositionForSection(int section) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public Object[] getSections() {
            return null;
        }
    }

    class loadContactDetails extends AsyncTask<String, String, String> {
        long contactID;

        public loadContactDetails(long contactID) {
            this.contactID = contactID;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources()
                    .getString(R.string.string_diag_pleasewait), getResources()
                    .getString(R.string.string_diag_loading_details) + "...");
            progressDialog.setCancelable(false);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            quickContactHelper = new QuickContactHelper(getActivity());
            ContactDetailActivity.contactDto = quickContactHelper.getContactDetails(contactID + "");

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();

            Intent intent = new Intent(getActivity(), ContactDetailActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_up_exit);
            progressDialog = null;
        }
    }

    TextView tmpTV;
    LinearLayout sideIndex;

    public void updateList() {
        sideIndex = (LinearLayout) getActivity().findViewById(R.id.sideIndex);
        sideIndex.removeAllViews();
        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }

        int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

        for (int l = 0; l < let.length; l++) {
            boolean isExist = false;
            for (double i = 1; i <= indexListSize; i = i + delta) {
                Object[] tmpIndexItem = alphabet.get((int) i - 1);
                String tmpLetter = tmpIndexItem[0].toString();

                if (let[l].equalsIgnoreCase(tmpLetter)) {
                    showLetters(let[l]);
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                showLetters(let[l]);
            }

        }

        sideIndexHeight = sideIndex.getHeight();

        sideIndex.setOnTouchListener((v, event) -> {
            // now you know coordinates of touch
            sideIndexX = event.getX();
            sideIndexY = event.getY();

            // and can display a proper item it country list
            displayListItem();

            return false;
        });
    }

    public void showLetters(String let) {
        tmpTV = new TextView(getActivity());
        tmpTV.setText(let);
        tmpTV.setGravity(Gravity.CENTER);
        tmpTV.setTextSize(9);
        tmpTV.setTypeface(NewUtil.getFontBold(getActivity()));
        tmpTV.setTextColor(getActivity().getResources().getColor(
                R.color.phone_app_blue));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        tmpTV.setLayoutParams(params);
        sideIndex.addView(tmpTV);
    }

    public void displayListItem() {
        LinearLayout sideIndex = (LinearLayout) getActivity().findViewById(R.id.sideIndex);
        sideIndexHeight = sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition < alphabet.size()) {
            Object[] indexItem = alphabet.get(itemPosition);
            int subitemPosition = sections.get(indexItem[0]);

            // ListView listView = (ListView) findViewById(android.R.id.list);
            pinnedHeaderListViewContacts.setSelection(subitemPosition);
        }
    }
}
