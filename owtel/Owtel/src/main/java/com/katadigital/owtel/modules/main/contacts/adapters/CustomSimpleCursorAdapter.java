package com.katadigital.owtel.modules.main.contacts.adapters;

import android.content.Context;
import android.database.Cursor;
import android.widget.Filterable;
import android.widget.SimpleCursorAdapter;

/**
 * Created by dcnc123 on 2/27/17.
 */

public class CustomSimpleCursorAdapter extends SimpleCursorAdapter implements Filterable {
    public CustomSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
    }

    public CustomSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }



}
