package com.katadigital.owtel.modules.main.contacts;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

public class AddFieldActivity extends AppCompatActivity implements OnClickListener {

    static boolean prefisHidden;
    static boolean pfirstisHidden;
    static boolean middleisHidden;
    static boolean pmiddleisHidden;
    static boolean plastisHidden;
    static boolean suffixisHidden;
    static boolean nickisHidden;
    static boolean jobTitleisHidden;
    static boolean jobDepisHidden;

    LinearLayout layout_prefix;
    LinearLayout layout_pfname;
    LinearLayout layout_middle;
    LinearLayout layout_pmiddle;
    LinearLayout layout_plast;
    LinearLayout layout_suffix;
    LinearLayout layout_nickname;
    LinearLayout layout_jobTitle;
    LinearLayout layout_jobDep;

    ImageView iv_prefix;
    ImageView iv_pName;
    ImageView iv_middle;
    ImageView iv_pMiddle;
    ImageView iv_pLast;
    ImageView iv_suffix;
    ImageView iv_nick;
    ImageView iv_jobTitle;
    ImageView iv_jobDep;

    int SelectedDone = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_field);
        initData();
        initTextType();

        String leftitem = getResources().getString(R.string.cancel_string);
        // CustomActionBar.viewCustomActionBar(this, leftitem, "", "");

        String doneItem = getResources().getString(R.string.done_string);

        View include = findViewById(R.id.contct_include);
        Button mButtonCustomActionBarLeft = (Button) include.findViewById(R.id.btn_custom_action_bar_left);
        mButtonCustomActionBarLeft.setOnClickListener(v -> onBackPressed());

        TextView a_title = (TextView) include.findViewById(R.id.a_title);

        Button mButtonCustomActionBarRight =  (Button) include.findViewById(R.id.btn_custom_action_bar_right);

        mButtonCustomActionBarLeft.setText(leftitem);
        a_title.setText("");
        mButtonCustomActionBarRight.setText(doneItem);

        mButtonCustomActionBarRight.setVisibility(View.VISIBLE);

        mButtonCustomActionBarRight.setOnClickListener(v -> {
            selectedIndext(SelectedDone);
            onBackPressed();
        });

    }

    private void initTextType() {
        textChangeFontandSize(R.id.txt_prefix);
        textChangeFontandSize(R.id.txt_pname);
        textChangeFontandSize(R.id.txt_middle);
        textChangeFontandSize(R.id.txt_pmiddle);
        textChangeFontandSize(R.id.txt_last);
        textChangeFontandSize(R.id.txt_suffix);
        textChangeFontandSize(R.id.txt_nickname);
        textChangeFontandSize(R.id.txt_jobtitle);
        textChangeFontandSize(R.id.txt_jobdep);
    }

    public void textChangeFontandSize(int idTxt) {
        ((TextView) findViewById(idTxt)).setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(idTxt)).setTextSize(NewUtil.gettxtSize());
    }

    private void initData() {
        ActionBar ab = getActionBar();
        ab.setDisplayShowHomeEnabled(false);
        ab.setDisplayHomeAsUpEnabled(true);

        iv_prefix = (ImageView) findViewById(R.id.iv_pref);
        iv_pName = (ImageView) findViewById(R.id.iv_fname);
        iv_middle = (ImageView) findViewById(R.id.iv_middle);
        iv_pMiddle = (ImageView) findViewById(R.id.iv_pMiddle);
        iv_pLast = (ImageView) findViewById(R.id.iv_pLast);
        iv_suffix = (ImageView) findViewById(R.id.iv_suffix);
        iv_nick = (ImageView) findViewById(R.id.iv_nick);
        iv_jobTitle = (ImageView) findViewById(R.id.iv_jobtitle);
        iv_jobDep = (ImageView) findViewById(R.id.iv_jobDep);

        clearImage();

        layout_prefix = (LinearLayout) findViewById(R.id.layout_prefix);

        layout_prefix.setOnClickListener(this);

        layout_pfname = (LinearLayout) findViewById(R.id.layout_pname);
        layout_pfname.setOnClickListener(this);

        layout_middle = (LinearLayout) findViewById(R.id.layout_middle);
        layout_middle.setOnClickListener(this);

        layout_pmiddle = (LinearLayout) findViewById(R.id.layout_pmiddle);
        layout_pmiddle.setOnClickListener(this);

        layout_plast = (LinearLayout) findViewById(R.id.layout_plast);
        layout_plast.setOnClickListener(this);

        layout_suffix = (LinearLayout) findViewById(R.id.layout_suffix);
        layout_suffix.setOnClickListener(this);

        layout_nickname = (LinearLayout) findViewById(R.id.layout_nickname);
        layout_nickname.setOnClickListener(this);

        layout_jobTitle = (LinearLayout) findViewById(R.id.layout_jobTitle);
        layout_jobTitle.setOnClickListener(this);

        layout_jobDep = (LinearLayout) findViewById(R.id.layout_jobDep);
        layout_jobDep.setOnClickListener(this);

        if (!EditContactsActivity.isEditable) {

            textisHidden(layout_prefix, prefisHidden);
            textisHidden(layout_pfname, pfirstisHidden);
            textisHidden(layout_middle, middleisHidden);
            textisHidden(layout_pmiddle, pmiddleisHidden);
            textisHidden(layout_plast, plastisHidden);
            textisHidden(layout_suffix, suffixisHidden);
            textisHidden(layout_nickname, nickisHidden);
            textisHidden(layout_jobTitle, jobTitleisHidden);
            textisHidden(layout_jobDep, jobDepisHidden);

        } else {
            textisHidden(layout_prefix, EditContactsActivity.removePref);
            textisHidden(layout_pfname, EditContactsActivity.removePfname);
            textisHidden(layout_middle, EditContactsActivity.removeMiddle);
            textisHidden(layout_pmiddle, EditContactsActivity.removePmiddle);
            textisHidden(layout_plast, EditContactsActivity.removePLastname);
            textisHidden(layout_suffix, EditContactsActivity.removeSuffix);
            textisHidden(layout_nickname, EditContactsActivity.removeNickname);
            textisHidden(layout_jobTitle, EditContactsActivity.removeJobTitle);
            textisHidden(layout_jobDep, EditContactsActivity.removeJobDep);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public static void setFalseAllFields() {
        // restart static value
        prefisHidden = false;
        pfirstisHidden = false;
        middleisHidden = false;
        pmiddleisHidden = false;
        plastisHidden = false;
        suffixisHidden = false;
        nickisHidden = false;
        jobTitleisHidden = false;
        jobDepisHidden = false;

        EditContactsActivity.isEditable = false;
    }

    public void textisHidden(LinearLayout layout_prefix, boolean isActive) {
        if (isActive) {
            System.out.println("JM FALSE NOT PAKITA");
            layout_prefix.setVisibility(View.GONE);
        } else {
            System.out.println("JM TRUE PAKITA");
            layout_prefix.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.slide_out_down);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.layout_prefix:
                SelectedDone = 0;
                clearImage();
                iv_prefix.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_pname:
                SelectedDone = 1;
                clearImage();
                iv_pName.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_middle:
                SelectedDone = 2;
                clearImage();
                iv_middle.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_pmiddle:
                SelectedDone = 3;
                clearImage();
                iv_pMiddle.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_plast:
                SelectedDone = 4;
                clearImage();
                iv_pLast.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_suffix:
                SelectedDone = 5;
                clearImage();
                iv_suffix.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_nickname:
                SelectedDone = 6;
                clearImage();
                iv_nick.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_jobTitle:
                SelectedDone = 7;
                clearImage();
                iv_jobTitle.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_jobDep:
                SelectedDone = 8;
                clearImage();
                iv_jobDep.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }

        // onBackPressed();

    }

    public void clearImage() {

        iv_prefix.setVisibility(View.INVISIBLE);
        iv_pName.setVisibility(View.INVISIBLE);
        iv_middle.setVisibility(View.INVISIBLE);
        iv_pMiddle.setVisibility(View.INVISIBLE);
        iv_pLast.setVisibility(View.INVISIBLE);
        iv_suffix.setVisibility(View.INVISIBLE);
        iv_nick.setVisibility(View.INVISIBLE);
        iv_jobTitle.setVisibility(View.INVISIBLE);
        iv_jobDep.setVisibility(View.INVISIBLE);

    }

    public void selectedIndext(int index) {
        switch (index) {
            case 0:
                prefisHidden = true;
                break;
            case 1:
                pfirstisHidden = true;
                break;
            case 2:
                middleisHidden = true;
                break;
            case 3:
                pmiddleisHidden = true;
                break;
            case 4:
                plastisHidden = true;
                break;
            case 5:
                suffixisHidden = true;
                break;
            case 6:
                nickisHidden = true;
                break;
            case 7:
                jobTitleisHidden = true;
                break;

            case 8:
                jobDepisHidden = true;
                break;

            default:
                break;
        }
    }

}
