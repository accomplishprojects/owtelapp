package com.katadigital.owtel.modules.main.contacts.util;

/**
 * Created by Omar Matthew Reyes on 11/3/15.
 */
public class GlobalValues {
    public static final long LOGIN_DELAY_TIME = 0;
    public static boolean DEBUG = true;
    public static final int GET_COUNTRY = 0, GET_STATE = 1, GET_CITY = 2;
    public static final int REQUEST_TIMEOUT = 8000;
    public static boolean JC_debug = false;

    public void setDebug(boolean debug){
        DEBUG = debug;
    }

    public boolean getDebug() {
        return DEBUG;
    }
}
