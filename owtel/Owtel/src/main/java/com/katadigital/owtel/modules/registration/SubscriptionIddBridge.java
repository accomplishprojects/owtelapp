package com.katadigital.owtel.modules.registration;

import android.os.Bundle;

import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;

import java.util.List;

/**
 * Created by Omar Matthew Reyes on 7/13/16.
 * Interface for passing List data
 * from RegisterAreaCodeAndNumberFragment to RegisterPaymentFragment
 */
public interface SubscriptionIddBridge {
    /**
     * Interface for RegisterAreaCodeNumberFragment to
     * RegisterPaymentFragment(legacy), RegisterPaymentItemFragment
     * @param subscriptionList List<SubscriptionList>
     * @param iddList List<IddList>
     * @param args Bundle
     */
    void setSubscriptionList(List<SubscriptionList> subscriptionList, List<IddList> iddList, Bundle args);

    /**
     * Interface for RegisterPaymentItemFragment to RegisterPaymentCheckoutFragment
     * @param subscription SubscriptionList
     * @param idd IddList
     * @param args Bundle
     */
    void setSubscriptionIdd(SubscriptionList subscription, IddList idd, Bundle args, List<AddressList> countryList);
}
