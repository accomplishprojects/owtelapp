package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.portsip.model.PhoneNumberModel;

public class EditHelperJMPhone {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    int fieldPhone_ID = 0;

    public static ArrayList<Button> btnPhonespinner = new ArrayList<Button>();
    boolean custom = false;

    /**
     * Usable Modifications
     */
    String TAG = this.getClass().getSimpleName();
    public static ArrayList<PhoneNumberModel> checkerForPhoneNumbers = new ArrayList<>();
    public static int checkerForPhoneNumberIndex = -1;

    public EditHelperJMPhone(EditContactsActivity activity,
                             EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;
        this.fieldPhone_ID = editHelp.fieldPhone_ID;
        this.btnPhonespinner = editHelp.btnPhonespinner;
    }

    public ContactDto createLoadedNumberField(PhoneNumberDto num, final ContactDto contactDto) {
        /**Usable Modifications*/
        PhoneNumberModel phoneNumberModel = new PhoneNumberModel();

        final LinearLayout holderField = (LinearLayout) activity.findViewById(R.id.phone_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel.inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel.findViewById(R.id.delete_Btn);
        final Button spinnerBtn = (Button) lLayoutPanel.findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel.findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel.findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel.findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerBtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setId(fieldPhone_ID);
        spinnerBtn.setId(fieldPhone_ID);
        btnDelete.setId(fieldPhone_ID);
        editTxt.setId(fieldPhone_ID);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerBtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnPhonespinner.add(spinnerBtn);
        editTxt.setFocusableInTouchMode(false);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(R.array.numberspinner);
        boolean inBounds = (spinnerBtn.getId() >= 0) && (spinnerBtn.getId() < myResArray.length);

        int selectedItem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(num.getNumberType())) {
                custom = false;
                selectedItem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(num.getNumberType())) {
                custom = true;
                btnPhonespinner.get(spinnerBtn.getId()).setText(
                        num.getNumberType());
                btnPhonespinner.get(spinnerBtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnPhonespinner.get(spinnerBtn.getId()).setText(
                    myResArray[selectedItem]);
        } else {
            btnPhonespinner.get(spinnerBtn.getId())
                    .setText(num.getNumberType());
        }

        btnPhonespinner.get(spinnerBtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivityCustomLoader(activity,
                                btnPhonespinner, spinnerBtn,
                                R.array.numberspinner, custom, true);

                    }
                });

        btnPhonespinner.get(spinnerBtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnPhonespinner.get(spinnerBtn.getId())
                                .getText().toString();
                        contactDto.phoneNumbers.get(spinnerBtn.getId())
                                .setNumberType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        editTxt.setFocusableInTouchMode(true);
                        break;
                }
                return false;
            }
        });

        editTxt.setInputType(InputType.TYPE_CLASS_PHONE);
        editTxt.setText(num.getNumber());
        phoneNumberModel.setPhoneNumber(num.getNumber());

        editTxt.setHint(activity.getResources().getString(
                R.string.string_hit_phone));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.phoneNumbers.get(editTxt.getId()).setNumber(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);

                }

                /**Usable Modifications*/
                checkerForPhoneNumbers.get(checkerForPhoneNumberIndex)
                        .setPhoneNumber(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.phoneNumbers.get(editTxt.getId()).setNumber("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDto.phoneNumbers.remove(btnDelete.getId());
                contactDto.phoneNumbers.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();

                checkerForPhoneNumbers.remove(checkerForPhoneNumberIndex);
                --checkerForPhoneNumberIndex;
            }
        });

        String txtbtn = btnPhonespinner.get(spinnerBtn.getId()).getText()
                .toString();

        num.setNumber(editTxt.getText().toString());
        num.setNumberType(txtbtn);

        contactDto.addPhoneNumbers(num);
        holderField.addView(lLayoutPanel);
        fieldPhone_ID++;

        /**Usable Modifications*/
        checkerForPhoneNumbers.add(phoneNumberModel);
        ++checkerForPhoneNumberIndex;

        Log.e(TAG, "createLoadedNumberField: " + checkerForPhoneNumbers.size());

        return contactDto;

    }

    public void addPhoneFunction(final ContactDto contactDto) {
        PhoneNumberDto phoneNumberDto = new PhoneNumberDto();
        PhoneNumberModel phoneNumberModel = new PhoneNumberModel();

        final LinearLayout holderfield = (LinearLayout) activity.findViewById(R.id.phone_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel.inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel.findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel.findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel.findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel.findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel.findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setId(fieldPhone_ID);
        spinnerbtn.setId(fieldPhone_ID);
        btnDelete.setId(fieldPhone_ID);
        editTxt.setId(fieldPhone_ID);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnPhonespinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.numberspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnPhonespinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnPhonespinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnPhonespinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnPhonespinner.get(spinnerbtn.getId()).setTag(
                    myResArray.length - 1);
        }

        btnPhonespinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnPhonespinner,
                                spinnerbtn, R.array.numberspinner);
                    }
                });

        btnPhonespinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnPhonespinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.phoneNumbers.get(spinnerbtn.getId())
                                .setNumberType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setInputType(InputType.TYPE_CLASS_PHONE);
        editTxt.setHint(activity.getResources().getString(
                R.string.string_hit_phone));
        editTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                    contactDto.phoneNumbers.get(editTxt.getId()).setNumber(
                            String.valueOf(charSequence));
                }

                /**Usable Modifications*/
                checkerForPhoneNumbers.get(checkerForPhoneNumberIndex)
                        .setPhoneNumber(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.phoneNumbers.get(editTxt.getId()).setNumber("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDto.phoneNumbers.remove(btnDelete.getId());
                contactDto.phoneNumbers.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();

                checkerForPhoneNumbers.remove(checkerForPhoneNumberIndex);
                --checkerForPhoneNumberIndex;
            }
        });

        String txtbtn = btnPhonespinner.get(spinnerbtn.getId()).getText()
                .toString();

        int tag = (Integer) btnPhonespinner.get(spinnerbtn.getId()).getTag();

        phoneNumberDto.setId(spinnerbtn.getId());
        phoneNumberDto.setNumber(editTxt.getText().toString());
        phoneNumberDto.setNumberType(txtbtn);
        contactDto.addPhoneNumbers(phoneNumberDto);
        holderfield.addView(lLayoutPanel);
        fieldPhone_ID++;

        /**Usable Modifications*/
        checkerForPhoneNumbers.add(phoneNumberModel);
        ++checkerForPhoneNumberIndex;
    }
}
