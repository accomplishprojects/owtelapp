package com.katadigital.owtel.helpers;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

import com.kata.phone.R;

public class CustomTouchListener implements OnTouchListener {

    TextView textview;
    Activity activity;

    public CustomTouchListener(TextView a_text, Activity activity) {
        this.textview = a_text;
        this.activity = activity;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                textview.setTextColor(activity.getResources().getColor(
                        R.color.phone_app_blue_light));
                break;
            case MotionEvent.ACTION_UP:
                textview.setTextColor(activity.getResources().getColor(
                        R.color.phone_app_blue));
                break;
            case MotionEvent.ACTION_CANCEL:
                textview.setTextColor(activity.getResources().getColor(
                        R.color.phone_app_blue));
                break;
        }
        return false;
    }

}
