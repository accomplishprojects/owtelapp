
package com.katadigital.owtel.daoDb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AreaCodeList {

    private List<AreaCodeList_> areaCodeList = new ArrayList<AreaCodeList_>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The areaCodeList
     */
    public List<AreaCodeList_> getAreaCodeList() {
        return areaCodeList;
    }

    /**
     * 
     * @param areaCodeList
     *     The area_code_list
     */
    public void setAreaCodeList(List<AreaCodeList_> areaCodeList) {
        this.areaCodeList = areaCodeList;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
