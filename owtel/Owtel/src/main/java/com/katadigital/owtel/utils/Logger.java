package com.katadigital.owtel.utils;


import android.util.Log;

import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

public class Logger {

    public static void e(String tag,String message){
        if(GlobalValues.DEBUG)
            Log.e(tag,message);
    }

    public static void i(String tag,String message){
        if(GlobalValues.DEBUG)
            Log.i(tag,message);
    }
}
