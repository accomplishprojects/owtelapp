package com.katadigital.owtel.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Omar Matthew Reyes on 7/5/16.
 * Radius Session Object for API response using retrofit
 */
public class RadiusSession {
    @SerializedName("status")
    @Expose
    private String apiStatus;

    /**
     * Get radius session
     * @return String
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * Set radius session
     * @param apiStatus RadSessionObj
     */
    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    @SerializedName("logged")
    @Expose
    private String userSession;

    @SerializedName("message")
    @Expose
    private String apiMessage;

    /**
     * Get user radius session status
     * true, false
     * @return String
     */
    public String getUserSession() {
        return userSession;
    }

    /**
     * Set user radius session
     * @param userSession String
     */
    public void setUserSession(String userSession) {
        this.userSession = userSession;
    }

    /**
     * Get API message. Usually errors
     * @return String
     */
    public String getApiMessage() {
        return apiMessage;
    }

    /**
     * Set API message
     * @param apiMessage String
     */
    public void setApiMessage(String apiMessage) {
        this.apiMessage = apiMessage;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
