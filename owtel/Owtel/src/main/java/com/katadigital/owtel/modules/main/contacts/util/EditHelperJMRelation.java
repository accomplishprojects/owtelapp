package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.AddRelatedActivity;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.RelationDto;

public class EditHelperJMRelation {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    int fieldRE_ID = 0;

    ArrayList<Button> btnREspinner = new ArrayList<Button>();
    ArrayList<EditText> edTextrela = new ArrayList<EditText>();

    boolean custom = false;

    public EditHelperJMRelation(EditContactsActivity activity,
                                EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;
        this.fieldRE_ID = editHelp.fieldRE_ID;
        this.btnREspinner = editHelp.btnREspinner;
    }

    public ContactDto createLoadedRelation(RelationDto rela,
                                           final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.related_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_relation, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);
        final Button btn_related = (Button) lLayoutPanel
                .findViewById(R.id.btn_choose);

        delete.setId(fieldRE_ID);
        spinnerbtn.setId(fieldRE_ID);
        btnDelete.setId(fieldRE_ID);
        editTxt.setId(fieldRE_ID);
        btn_related.setId(fieldRE_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnREspinner.add(spinnerbtn);

        btn_related.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AddRelatedActivity.editTxt = editTxt;
                NewUtil.IntetCustomTrans(activity, AddRelatedActivity.class);
            }
        });

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.relationspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        int selecteditem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(rela.getRelationType())) {
                custom = false;
                selecteditem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(rela.getRelationType())) {
                custom = true;
                btnREspinner.get(spinnerbtn.getId()).setText(
                        rela.getRelationType());
                btnREspinner.get(spinnerbtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnREspinner.get(spinnerbtn.getId()).setText(
                    myResArray[selecteditem]);
        } else {
            btnREspinner.get(spinnerbtn.getId())
                    .setText(rela.getRelationType());
        }

        btnREspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivityCustomLoader(activity,
                                btnREspinner, spinnerbtn,
                                R.array.relationspinner, custom, false);

                    }
                });

        btnREspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnREspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.relation.get(spinnerbtn.getId())
                                .setRelationType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setText(rela.getRelationName());
        editTxt.setHint(activity.getResources().getString(
                R.string.string_relation_hint));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.relation.get(editTxt.getId()).setRelationName(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.relation.get(editTxt.getId()).setRelationName("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.relation.remove(btnDelete.getId());
                contactDto.relation.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnREspinner.get(spinnerbtn.getId()).getText()
                .toString();

        rela.setId(spinnerbtn.getId());
        rela.setRelationName(editTxt.getText().toString());
        rela.setRelationType(txtbtn);
        contactDto.addRelation(rela);

        holderfield.addView(lLayoutPanel);
        fieldRE_ID++;

        return contactDto;
    }

    public void addREFunction(final ContactDto contactDto) {

        RelationDto rela = new RelationDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.related_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_relation, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);
        final Button btn_related = (Button) lLayoutPanel
                .findViewById(R.id.btn_choose);

        delete.setId(fieldRE_ID);
        spinnerbtn.setId(fieldRE_ID);
        btnDelete.setId(fieldRE_ID);
        editTxt.setId(fieldRE_ID);
        btn_related.setId(fieldRE_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnREspinner.add(spinnerbtn);

        btn_related.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AddRelatedActivity.editTxt = editTxt;
                NewUtil.IntetCustomTrans(activity, AddRelatedActivity.class);
            }
        });

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.relationspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnREspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnREspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnREspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnREspinner.get(spinnerbtn.getId()).setTag(myResArray.length - 1);
        }

        btnREspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnREspinner,
                                spinnerbtn, R.array.relationspinner);
                    }
                });

        btnREspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnREspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.relation.get(spinnerbtn.getId())
                                .setRelationType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setHint(activity.getResources().getString(
                R.string.string_relation_hint));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.relation.get(editTxt.getId()).setRelationName(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.relation.get(editTxt.getId()).setRelationName("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.relation.remove(btnDelete.getId());
                contactDto.relation.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnREspinner.get(spinnerbtn.getId()).getText()
                .toString();

        rela.setId(spinnerbtn.getId());
        rela.setRelationName("");
        rela.setRelationType(txtbtn);
        contactDto.addRelation(rela);

        holderfield.addView(lLayoutPanel);
        fieldRE_ID++;
    }

}
