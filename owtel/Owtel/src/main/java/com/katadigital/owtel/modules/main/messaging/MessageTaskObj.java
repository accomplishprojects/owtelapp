package com.katadigital.owtel.modules.main.messaging;

/**
 * Created by dcnc123 on 2/24/17.
 */
public class MessageTaskObj {

    private String title;
    private String description;

    public MessageTaskObj(String title, String description) {
        this.title = title;
        this.description = description;

    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
