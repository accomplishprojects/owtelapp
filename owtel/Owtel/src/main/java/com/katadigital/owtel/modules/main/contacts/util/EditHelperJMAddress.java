package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;

public class EditHelperJMAddress {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    int fieldAddress_ID = 0;

    ArrayList<Button> btnAddresspinner = new ArrayList<Button>();
    boolean custom = false;

    public EditHelperJMAddress(EditContactsActivity activity,
                               EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;
        this.fieldAddress_ID = editHelp.fieldAddress_ID;
        this.btnAddresspinner = editHelp.btnAddresspinner;
    }

    public ContactDto createLoadedAddressField(AddressDto addressDto,
                                               final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.address_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_address_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        // final Button clearBtn = (Button) lLayoutPanel
        // .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);

        final EditText edStreet = (EditText) lLayoutPanel
                .findViewById(R.id.edStreet);
        final EditText edNeighborhood = (EditText) lLayoutPanel
                .findViewById(R.id.edNeighborhood);
        final EditText edCity = (EditText) lLayoutPanel
                .findViewById(R.id.edCity);
        final EditText edPostal = (EditText) lLayoutPanel
                .findViewById(R.id.edPostal);
        final EditText edCountry = (EditText) lLayoutPanel
                .findViewById(R.id.edCountry);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        edStreet.setTypeface(font2);
        edNeighborhood.setTypeface(font2);
        edCity.setTypeface(font2);
        edPostal.setTypeface(font2);
        edCountry.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        edStreet.setTextSize(NewUtil.gettxtSize());
        edNeighborhood.setTextSize(NewUtil.gettxtSize());
        edCity.setTextSize(NewUtil.gettxtSize());
        edPostal.setTextSize(NewUtil.gettxtSize());
        edCountry.setTextSize(NewUtil.gettxtSize());

        delete.setId(fieldAddress_ID);
        spinnerbtn.setId(fieldAddress_ID);
        btnDelete.setId(fieldAddress_ID);
        edStreet.setId(fieldAddress_ID);
        edNeighborhood.setId(fieldAddress_ID);
        edCity.setId(fieldAddress_ID);
        edPostal.setId(fieldAddress_ID);
        edCountry.setId(fieldAddress_ID);

        btnAddresspinner.add(spinnerbtn);

        edStreet.setFocusableInTouchMode(false);
        edNeighborhood.setFocusableInTouchMode(false);
        edCity.setFocusableInTouchMode(false);
        edPostal.setFocusableInTouchMode(false);

        edStreet.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        edStreet.setFocusableInTouchMode(true);
                        break;
                }
                return false;
            }
        });
        edNeighborhood.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        edNeighborhood.setFocusableInTouchMode(true);
                        break;
                }
                return false;
            }
        });
        edCity.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        edCity.setFocusableInTouchMode(true);
                        break;
                }
                return false;
            }
        });
        edPostal.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        edPostal.setFocusableInTouchMode(true);
                        break;
                }
                return false;
            }
        });

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        String[] myResArray = activity.getResources().getStringArray(
                R.array.addressspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        int selecteditem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(addressDto.getAddressType())) {
                custom = false;
                selecteditem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(addressDto
                    .getAddressType())) {
                custom = true;
                btnAddresspinner.get(spinnerbtn.getId()).setText(
                        addressDto.getAddressType());
                btnAddresspinner.get(spinnerbtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnAddresspinner.get(spinnerbtn.getId()).setText(
                    myResArray[selecteditem]);
        } else {
            btnAddresspinner.get(spinnerbtn.getId()).setText(
                    addressDto.getAddressType());
        }

        btnAddresspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnAddresspinner,
                                spinnerbtn, R.array.addressspinner);
                    }
                });

        btnAddresspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnAddresspinner
                                .get(spinnerbtn.getId()).getText().toString();
                        contactDto.addresses.get(spinnerbtn.getId())
                                .setAddressType(txtbtn);
                    }
                });

        edCountry.setText(addressDto.getCountryStr());
        edCountry.setKeyListener(null);
        edCountry.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.CountryPicker(activity, edCountry);
                        break;
                }
                return true;
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.addresses.remove(btnDelete.getId());
                contactDto.addresses.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        // Editext For Address Text Watcher

        edStreet.setText(addressDto.getStreetStr().trim());
        edStreet.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edStreet.getId()).setStreetStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edNeighborhood.setText(addressDto.getNeigborhoodStr().trim());
        edNeighborhood.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edNeighborhood.getId())
                        .setNeigborhoodStr(String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edCity.setText(addressDto.getCityStr().trim());
        edCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edCity.getId()).setCityStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edPostal.setText(addressDto.getZipCodeStr().trim());
        edPostal.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edPostal.getId()).setZipCodeStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edCountry.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                contactDto.addresses.get(edStreet.getId()).setCountryStr(
                        String.valueOf(arg0));
            }
        });

        String txtbtn = btnAddresspinner.get(spinnerbtn.getId()).getText()
                .toString();

        holderfield.addView(lLayoutPanel);
        contactDto.addAddresses(addressDto);
        addressDto.setId(fieldAddress_ID);

        addressDto
                .setNeigborhoodStr(edNeighborhood.getText().toString().trim());
        addressDto.setCityStr(edCity.getText().toString().trim());
        addressDto.setZipCodeStr(edPostal.getText().toString().trim());
        addressDto.setCountryStr(edCountry.getText().toString().trim());
        addressDto.setAddressType(txtbtn);

        fieldAddress_ID++;

        return contactDto;
    }

    public void addAddressFunction(final ContactDto contactDto) {

        AddressDto addressDto = new AddressDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.address_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_address_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        // final Button clearBtn = (Button) lLayoutPanel
        // .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);

        final EditText edStreet = (EditText) lLayoutPanel
                .findViewById(R.id.edStreet);
        final EditText edNeighborhood = (EditText) lLayoutPanel
                .findViewById(R.id.edNeighborhood);
        final EditText edCity = (EditText) lLayoutPanel
                .findViewById(R.id.edCity);
        final EditText edPostal = (EditText) lLayoutPanel
                .findViewById(R.id.edPostal);
        final EditText edCountry = (EditText) lLayoutPanel
                .findViewById(R.id.edCountry);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        edStreet.setTypeface(font2);
        edNeighborhood.setTypeface(font2);
        edCity.setTypeface(font2);
        edPostal.setTypeface(font2);
        edCountry.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        edStreet.setTextSize(NewUtil.gettxtSize());
        edNeighborhood.setTextSize(NewUtil.gettxtSize());
        edCity.setTextSize(NewUtil.gettxtSize());
        edPostal.setTextSize(NewUtil.gettxtSize());
        edCountry.setTextSize(NewUtil.gettxtSize());

        delete.setId(fieldAddress_ID);
        spinnerbtn.setId(fieldAddress_ID);
        btnDelete.setId(fieldAddress_ID);
        edStreet.setId(fieldAddress_ID);
        edNeighborhood.setId(fieldAddress_ID);
        edCity.setId(fieldAddress_ID);
        edPostal.setId(fieldAddress_ID);
        edCountry.setId(fieldAddress_ID);

        btnAddresspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        String[] myResArray = activity.getResources().getStringArray(
                R.array.addressspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnAddresspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnAddresspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());
        } else {
            btnAddresspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnAddresspinner.get(spinnerbtn.getId()).setTag(
                    myResArray.length - 1);
        }

        btnAddresspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnAddresspinner,
                                spinnerbtn, R.array.addressspinner);
                    }
                });

        btnAddresspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnAddresspinner
                                .get(spinnerbtn.getId()).getText().toString();
                        contactDto.addresses.get(spinnerbtn.getId())
                                .setAddressType(txtbtn);
                    }
                });

        String locale = activity.getResources().getConfiguration().locale
                .getDisplayCountry();

        edCountry.setKeyListener(null);
        edCountry.setText(locale);
        edCountry.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.CountryPicker(activity, edCountry);
                        break;
                }
                return true;
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.addresses.remove(btnDelete.getId());
                contactDto.addresses.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        // Editext For Address Text Watcher
        edStreet.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edStreet.getId()).setStreetStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edNeighborhood.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edNeighborhood.getId())
                        .setNeigborhoodStr(String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edCity.getId()).setCityStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edPostal.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edPostal.getId()).setZipCodeStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edCountry.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                contactDto.addresses.get(edStreet.getId()).setCountryStr(
                        String.valueOf(arg0));
            }
        });

        String txtbtn = btnAddresspinner.get(spinnerbtn.getId()).getText()
                .toString();

        holderfield.addView(lLayoutPanel);
        contactDto.addAddresses(addressDto);
        addressDto.setId(fieldAddress_ID);

        addressDto
                .setNeigborhoodStr(edNeighborhood.getText().toString().trim());
        addressDto.setCityStr(edCity.getText().toString().trim());
        addressDto.setZipCodeStr(edPostal.getText().toString().trim());
        addressDto.setCountryStr(edCountry.getText().toString().trim());
        addressDto.setAddressType(txtbtn);

        fieldAddress_ID++;

    }

}
