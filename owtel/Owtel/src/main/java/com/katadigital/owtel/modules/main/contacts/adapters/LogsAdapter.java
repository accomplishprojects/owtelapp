package com.katadigital.owtel.modules.main.contacts.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.history.LogsDetail;
import com.katadigital.owtel.modules.main.contacts.entities.CallLogDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.history.LogsFragment;
import com.katadigital.owtel.helpers.AlertDialogHelper;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.contacts.util.DateTimeHelper;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class LogsAdapter extends BaseAdapter {
    private static final String TAG = "LogsAdapter";

    private Activity context;
    boolean editIsOn = false;
    QuickContactHelper quickContactHelper;
    private ProgressDialog progressDialog;
    SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yy", Locale.getDefault());
    SimpleDateFormat sdfDayOfWeek = new SimpleDateFormat("EEEE", Locale.getDefault());
    SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a", Locale.getDefault());

    // callLogsList - filtered raw call logs list
    // callLogsListRaw - raw call logs list
    List<CallLogDto> callLogsList, callLogListRaw;
    Fragment logsFragment;
    AlertDialogHelper alertDialog;
    Calendar c = Calendar.getInstance();
    ArrayList<HashMap<String, String>> callLogListSelPass;

    public LogsAdapter(Activity context, List<CallLogDto> callLogsList,
                       List<CallLogDto> callLogsListRaw,
                       Fragment logsFragment, boolean edit) {
        super();
        this.context = context;
        this.callLogsList = callLogsList;
        this.callLogListRaw = callLogsListRaw;
        quickContactHelper = new QuickContactHelper(context);
        this.logsFragment = logsFragment;
        editIsOn = edit;
        alertDialog = new AlertDialogHelper();
    }

    @Override
    public int getCount() {
        return callLogsList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private String YesterdayOrToday(long timeInMilliseconds) {
        String yot;
        if (DateUtils.isToday(timeInMilliseconds)) {
            yot = "Today";
        } else if (DateUtils.isToday(timeInMilliseconds + 86400000)) {
            yot = "Yesterday";
        } else {
            Date cdate = new Date();
            Long diff = cdate.getTime() - timeInMilliseconds;
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeInMilliseconds);
            if (days < 6) {
                yot = sdfDayOfWeek.format(calendar.getTime());
            } else {
                yot = sdf.format(calendar.getTime());
            }
        }
        return yot;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.logs_custom_list, viewGroup, false);
            holder.deleteBtn = (Button) view.findViewById(R.id.logs_item_delete_btn);
            holder.textViewContactName = (TextView) view.findViewById(R.id.logs_item_text_view);
            holder.dateTextView = (TextView) view.findViewById(R.id.logs_item_date);
            holder.numberType = (TextView) view.findViewById(R.id.call_log_numbertype);
            holder.callStatusIv = (ImageView) view.findViewById(R.id.logs_item_imageview);
            holder.logInfoBtn = (Button) view.findViewById(R.id.btn_logs_item_info);
            holder.editBtn = (Button) view.findViewById(R.id.btn_edit_log);
            holder.listItem = view.findViewById(R.id.logs_list_item);

            holder.textViewContactName.setTypeface(NewUtil.getFontRoman(context));
            holder.dateTextView.setTypeface(NewUtil.getFontRoman(context));
            holder.numberType.setTypeface(NewUtil.getFontRoman(context));
 
            holder.textViewContactName.setTextSize(NewUtil.gettxtSize());
            holder.dateTextView.setTextSize(NewUtil.gettxtSize());
            holder.numberType.setTextSize(NewUtil.gettxtSize());

            holder.dateTextView.setTextColor(context.getResources().getColor(R.color.gray_notes));

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final CallLogDto callLogDto = callLogsList.get(i);

        String numtype = QuickContactHelper.getContactNumType(callLogDto.getNumber());
        if (callLogDto != null) {
            if (!numtype.trim().isEmpty()) {
                int num = Integer.parseInt(numtype);
                if (num != 0) {
                    String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());
                    try {
                        String name = (String) Phone.getTypeLabel(context.getResources(), num, customLabel);
                        holder.numberType.setText(name);
                    } catch (NullPointerException e) {
                        Log.e(TAG, "get Number Type " + e);
                    }
                } else {
//                    String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());
//                    String name = (String) Phone.getTypeLabel(Var.activity.getResources(), 0, customLabel);
//                    holder.numberType.setText(name);
                    try {
                        String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());
                        String name = (String) Phone.getTypeLabel(Var.activity.getResources(), 0, customLabel);
                        holder.numberType.setText(name);
                    } catch (Exception e) {
                        holder.numberType.setText("");
                        Log.e(TAG, "String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());\n" +
                                "                        String name = (String) Phone.getTypeLabel(Var.activity.getResources(), 0, customLabel);\n" +
                                "                        holder.numberType.setText(name);");
                    }
                }
            } else {
//                Log.e(TAG, "numType is EMPTY");
            }

            holder.listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    context.startActivity(QuickContactHelper.initiateDefaultCall(context, callLogDto.getNumber().replaceAll("#", "%23")));
                    QuickContactHelper.initiateSipCall(context, callLogDto.getNumber().replaceAll("#", "%23"));
                }
            });

            if (callLogsList.get(i).getNumber() != null) {
                String contactName = QuickContactHelper.getContactName(callLogDto.getNumber());
                if (callLogDto.getCount() <= 1) {
                    if (contactName.equals("") || contactName.equals(null)) {
                        holder.textViewContactName.setText(callLogDto.getNumber());
                    } else {
                        holder.textViewContactName.setText(contactName);
                    }
                } else {
                    if (contactName.equals("") || contactName.equals(null)) {
                        String contactNameTemp = callLogDto.getNumber() + " (" + callLogDto.getCount() + ")";
                        holder.textViewContactName.setText(contactNameTemp);
                    } else {
                        String contactNameTemp = contactName + " (" + callLogDto.getCount() + ")";
                        holder.textViewContactName.setText(contactNameTemp);
                    }
                }

                if (callLogDto.getCallType().equals("MISSED"))
                    holder.textViewContactName.setTextColor(Color.RED);
                else holder.textViewContactName.setTextColor(Color.BLACK);

                String formattedDate = sdf.format(callLogDto.getCallDate());
                String formattedTime = sdfTime.format(callLogDto.getCallDate());
                String curDate = sdf.format(c.getTime());

                if (curDate.equals(formattedDate)) {
                    holder.dateTextView.setText(formattedTime);
                } else {
                    holder.dateTextView.setText(YesterdayOrToday(callLogDto.getCallDate().getTime()));
                }
                holder.callStatusIv.setVisibility(View.VISIBLE);
                if (callLogDto.getCallType().equalsIgnoreCase("INCOMING")) {
                    // callStatusIv.setImageResource(R.drawable.incoming_call);
                    holder.callStatusIv.setVisibility(View.INVISIBLE);
                } else if (callLogDto.getCallType().equalsIgnoreCase("OUTGOING")) {
                    holder.callStatusIv.setImageResource(R.drawable.outgoing_call);
                } else {
                    // callStatusIv.setImageResource(R.drawable.missedcall);
                    holder.callStatusIv.setVisibility(View.INVISIBLE);
                }
            } else {
                holder.textViewContactName.setText(" ");
            }

            holder.logInfoBtn.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.information));
            holder.logInfoBtn.setOnClickListener(view1 -> new loadContactDetails(callLogDto, callLogsList.get(i).getNumber(), callLogsList.get(i).getCallDate()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR));

            if (editIsOn) {
                holder.logInfoBtn.setVisibility(View.GONE);
                holder.deleteBtn.setVisibility(View.GONE);
                holder.editBtn.setVisibility(View.VISIBLE);

                holder.editBtn.setOnClickListener(view1 -> {
                    holder.deleteBtn.setVisibility(View.VISIBLE);
                    view1.setVisibility(View.GONE);
                });

                holder.deleteBtn.setOnClickListener(v -> {
                    String formattedDate = sdf.format(callLogDto.getCallDate());
                    for (CallLogDto callLog : callLogsList) {
                        String callLogFormattedDate = sdf.format(callLog.getCallDate());
                        if (holder.numberType.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.string_unknown))) {
                            if (callLogDto.getNumber().equals(callLog.getNumber())
                                    && formattedDate.equals(callLogFormattedDate)
                                    && !callLogDto.getCallType().equals("MISSED")) {
                                context.getContentResolver().delete(android.provider.CallLog.Calls.CONTENT_URI, "_ID = " + callLog.getCallLogID(), null);
                            } else {
                                context.getContentResolver().delete(android.provider.CallLog.Calls.CONTENT_URI, "_ID = " + callLogDto.getCallLogID(), null);
                            }
                        } else {
                            if (callLogDto.getNumber().equals(callLog.getNumber())
                                    && callLogDto.getContactName().equals(callLog.getContactName())
                                    && formattedDate.equals(callLogFormattedDate)
                                    && !callLogDto.getCallType().equals("MISSED")) {
                                context.getContentResolver()
                                        .delete(android.provider.CallLog.Calls.CONTENT_URI,
                                                "_ID = " + callLog.getCallLogID(), null);
                            } else {
                                context.getContentResolver()
                                        .delete(android.provider.CallLog.Calls.CONTENT_URI,
                                                "_ID = " + callLogDto.getCallLogID(), null);
                            }
                        }
                    }

                    callLogsList.remove(i);
                    LogsAdapter.this.notifyDataSetChanged();
                    LogsAdapter.this.notifyDataSetInvalidated();

                    if (callLogsList.size() == 0) {
                        ((LogsFragment) logsFragment).refreshLogs();
                    }
                });
            } else holder.editBtn.setVisibility(View.GONE);
        }
        return view;
    }

    class loadContactDetails extends AsyncTask<String, String, String> {
        private CallLogDto callLogDto;
        private String contactId = "";

        private String contactNumber;
        private Date contactDate;

        public loadContactDetails(CallLogDto callLogDto, String contactNumber, Date contactDate) {
            this.callLogDto = callLogDto;
            this.contactNumber = contactNumber;
            this.contactDate = contactDate;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(context,
                    context.getResources().getString(R.string.string_diag_pleasewait),
                    context.getResources().getString(
                            R.string.string_diag_loading_details) + "...");
            progressDialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            ContactDto contactDto;
            contactId = quickContactHelper.getContactInformation(callLogDto.getNumber());
            contactDto = quickContactHelper.getContactDetails(contactId);
            ContactDetailActivity.contactDto = contactDto;

            // Log callLogListRaw contents
            callLogListSelPass = new ArrayList<>();
            for (CallLogDto callLogDetails : callLogListRaw) {
                HashMap<String, String> hashMapContactLog = new HashMap<>();
                if (callLogDetails.getNumber().equals(callLogDto.getNumber()) &&
                        DateTimeHelper.isSameDay(callLogDetails.getCallDate(), contactDate) &&
                        !(callLogDetails.getCallType().equals("MISSED") ^ callLogDto.getCallType().equals("MISSED"))) {
                    if (GlobalValues.DEBUG) {
                        Log.i(TAG, "NUMBER " + callLogDetails.getNumber());
                        Log.i(TAG, "DATE " + sdf.format(callLogDetails.getCallDate()));
                        Log.i(TAG, "TIME " + sdfTime.format(callLogDetails.getCallDate()));
                        Log.i(TAG, "CALL TYPE " + callLogDetails.getCallType());
                        Log.i(TAG, "DURATION " + callLogDetails.getDuration());
                    }

                    hashMapContactLog.put("time", sdfTime.format(callLogDetails.getCallDate()));
                    hashMapContactLog.put("calltype", callLogDetails.getCallType());
                    hashMapContactLog.put("duration", callLogDetails.getDuration());
                    callLogListSelPass.add(hashMapContactLog);
                }
            }

//            if (!contactId.isEmpty()) {
//                contactDto = quickContactHelper.getContactDetails(contactId);
//                ContactDetailActivity.contactDto = contactDto;
//
//                callLogListSelPass = new ArrayList<>();
//                String formattedDate = sdf.format(callLogDto.getCallDate());
//                String contactId = quickContactHelper.getContactInformation(callLogDto.getNumber());
//
//                for (CallLogDto callLog : callLogListRaw) {
//                    HashMap<String, String> map = new HashMap<>();
//                    String callLogFormattedDate = sdf.format(callLog.getCallDate());
//                    String contactId_callLog = quickContactHelper.getContactInformation(callLog.getNumber());
//                    String callLogType = callLog.getCallType();
//                    if (callLogDto.getCallType().equals("MISSED")) {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && callLogDto.getContactName().equals(callLog.getContactName())
//                                && callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                            System.out.println(formattedTime);
//                        }
//                    } else {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && contactId.equals(contactId_callLog)
//                                && !callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                            System.out.println(formattedTime);
//                        }
//                    }
//                }
//            } else {
//                callLogListSelPass = new ArrayList<>();
//                System.out.println(callLogListRaw.size());
//                String formattedDate = sdf.format(callLogDto.getCallDate());
//
//                for (CallLogDto callLog : callLogListRaw) {
//                    HashMap<String, String> map = new HashMap<>();
//                    String callLogFormattedDate = sdf.format(callLog.getCallDate());
//                    String callLogType = callLog.getCallType();
//                    if (callLogDto.getCallType().equals("MISSED")) {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && callLog.getNumberType().equalsIgnoreCase(context.getResources().getString(R.string.string_unknown))
//                                && callLog.getNumber().equals(callLogDto.getNumber())
//                                && callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                        }
//                    } else {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && callLog.getNumber().equals(callLogDto.getNumber())
//                                && !callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                        }
//                    }
//                }
//            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (!contactId.isEmpty()) {
                Intent intent = new Intent(context, ContactDetailActivity.class);
                intent.putExtra("selectednumber", callLogDto.getNumber());
                intent.putExtra("transactiondate", sdf.format(callLogDto.getCallDate()));
                intent.putExtra("logs", callLogListSelPass);
                intent.putExtra("recents", context.getResources().getString(R.string.tab_log));
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_up_exit);
            } else {
                Intent intent = new Intent(context, LogsDetail.class);
                if (!callLogDto.getNumber().equals("")) {
                    intent.putExtra("logs_number", callLogDto.getNumber());
                }
                if (callLogDto.getCallDate() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()); // Set
                    String formattedDate = sdf.format(callLogDto.getCallDate());
                    intent.putExtra("logs_date", formattedDate);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    String formattedTime = sdf2.format(callLogDto.getCallDate());
                    intent.putExtra("logs_time", formattedTime);
                }
                intent.putExtra("transactiondate", sdf.format(callLogDto.getCallDate()));
                intent.putExtra("logs", callLogListSelPass);
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_up_exit);
            }

            super.onPostExecute(result);
        }
    }

    static class ViewHolder {
        View listItem;
        Button deleteBtn, logInfoBtn, editBtn;
        TextView textViewContactName;
        TextView dateTextView;
        TextView numberType;
        ImageView callStatusIv;
    }

}