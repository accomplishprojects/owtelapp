package com.katadigital.owtel.models.collectedobjects;

import android.net.Uri;

/**
 * Created by dcnc123 on 9/21/16.
 */
public class AudioItem {

    public AudioItem(String audioName, Uri audioUri) {
        this.audioName = audioName;
        this.audioUri = audioUri;
    }

    private String audioName;
    private Uri audioUri;

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public Uri getAudioUri() {
        return audioUri;
    }

    public void setAudioUri(Uri audioUri) {
        this.audioUri = audioUri;
    }


}
