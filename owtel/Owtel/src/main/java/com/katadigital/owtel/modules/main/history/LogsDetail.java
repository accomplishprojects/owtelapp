package com.katadigital.owtel.modules.main.history;

import android.content.Intent;
import android.graphics.Color;
import android.net.ParseException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.adapters.CallLogAdapter;
import com.katadigital.owtel.helpers.ListViewHelper;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.contacts.AddNumberToContact;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class LogsDetail extends AppCompatActivity {

    ListView callLogListView;

    CallLogAdapter callLogAdapter;
    SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yy");

    SimpleDateFormat sdfcom = new SimpleDateFormat("MMMM dd, yyyy");// E is day
    // of week

    public static boolean removeinfoActiviy = false;

    public String YesterOrToday(Date newsdate) {
        String yot = "";

        Calendar newsDate = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        Calendar yesDate = Calendar.getInstance();

        newsDate.setTime(newsdate);
        currentDate.setTime(new Date());
        yesDate.setTime(new Date());

        yesDate.add(Calendar.DAY_OF_MONTH, -1);

        String strNDate = newsDate.get(Calendar.MONTH) + 1 + " "
                + newsDate.get(Calendar.DAY_OF_MONTH) + ", "
                + newsDate.get(Calendar.YEAR);
        String strCDate = currentDate.get(Calendar.MONTH) + 1 + " "
                + currentDate.get(Calendar.DAY_OF_MONTH) + ", "
                + currentDate.get(Calendar.YEAR);
        String strYDate = yesDate.get(Calendar.MONTH) + 1 + " "
                + yesDate.get(Calendar.DAY_OF_MONTH) + ", "
                + yesDate.get(Calendar.YEAR);

        if (strNDate.equals(strCDate)) {
            yot = getResources().getString(R.string.string_today);
        } else if (strNDate.equals(strYDate)) {
            yot = getResources().getString(R.string.string_yesterday);
        } else {
            yot = sdfcom.format(newsdate);
        }

        return yot;
    }

//    RelativeLayout cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_logs_detail);

        Var.logActivity = this;
//        cancel = (RelativeLayout) findViewById(R.id.cancel);

//        TextView recent = (TextView) findViewById(R.id.txt_recents);
        Button mButtonCancel = (Button) findViewById(R.id.btn_custom_action_bar_left);
//        cancel.setOnTouchListener(new CustomTouchListener(recent, this));
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        final String number = bundle.getString("logs_number");
        String callDate = bundle.getString("logs_date");
        String callTime = bundle.getString("logs_time");

        callLogListView = (ListView) findViewById(R.id.callLog_lv);
        Date convertedDate = new Date();
        try {
            convertedDate = sdf.parse(bundle.getString("transactiondate"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        ((TextView) findViewById(R.id.logsexisting_calltime))
                .setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.logsexisting_calltime))
                .setText(YesterOrToday(convertedDate));
        ((TextView) findViewById(R.id.logsexisting_calltime))
                .setTypeface(NewUtil.getFontRoman(this));
        ((TextView) findViewById(R.id.logsexisting_calltime))
                .setTextSize(NewUtil.gettxtSize());
        ((TextView) findViewById(R.id.logsexisting_calltime))
                .setTypeface(NewUtil.getFontRoman(this));
        ((TextView) findViewById(R.id.logsexisting_calltime))
                .setTextSize(NewUtil.gettxtSize());

        // Here BERTAX
        ArrayList<HashMap<String, String>> callLogsList = (ArrayList<HashMap<String, String>>) intent
                .getSerializableExtra("logs");
        callLogAdapter = new CallLogAdapter(this, callLogsList);
        callLogListView.setAdapter(callLogAdapter);
        ListViewHelper.setListViewHeightBasedOnChildren(callLogListView);
        // Bertax end here

        TextView numberText = (TextView) findViewById(R.id.logs_number_called);
        numberText.setText(number);
        numberText.setTypeface(NewUtil.getFontRoman(this));
        numberText.setTextSize(NewUtil.gettxtSize());

        TextView dateText = (TextView) findViewById(R.id.logs_date_called);
        dateText.setText(callDate);
        dateText.setTypeface(NewUtil.getFontRoman(this));
        dateText.setTextSize(NewUtil.gettxtSize());

        TextView timeText = (TextView) findViewById(R.id.logs_time_called);
        timeText.setText(callTime);
        dateText.setTypeface(NewUtil.getFontRoman(this));
        dateText.setTextSize(NewUtil.gettxtSize());

        final Button callNumber = (Button) findViewById(R.id.logs_call_number);
        callNumber.setTypeface(NewUtil.getFontRoman(this));
        callNumber.setTextSize(NewUtil.gettxtSize());
        callNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(QuickContactHelper.initiateDefaultCall(
//                        LogsDetail.this, number.replaceAll("#", "%23")));
                QuickContactHelper.initiateSipCall(LogsDetail.this, number.replaceAll("#", "%23"));
            }
        });

        final Button sendMessage = (Button) findViewById(R.id.logs_send_msg);
        sendMessage.setTypeface(NewUtil.getFontRoman(this));
        sendMessage.setTextSize(NewUtil.gettxtSize());
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                // sendIntent.setData(Uri.parse("sms:" + number));
                // startActivity(sendIntent);
                NewUtil.integrationNumberInfo(LogsDetail.this, number);
            }
        });

        final Button createNewContact = (Button) findViewById(R.id.logs_create_new_contact);
        createNewContact.setTypeface(NewUtil.getFontRoman(this));
        createNewContact.setTextSize(NewUtil.gettxtSize());
        createNewContact.setOnClickListener(view -> {
            String text = String.valueOf(number);
            NewUtil.createContactChooseDiag2(LogsDetail.this, text);
        });

        final Button addNumberToContacts = (Button) findViewById(R.id.logs_add_to_contacts);
        addNumberToContacts.setTypeface(NewUtil.getFontRoman(this));
        addNumberToContacts.setTextSize(NewUtil.gettxtSize());
        addNumberToContacts.setOnClickListener(view -> {
            LogsDetail.removeinfoActiviy = true;
            Intent intent1 = new Intent(LogsDetail.this,
                    AddNumberToContact.class);
            intent1.putExtra("number_to_add", number);
            startActivity(intent1);
            overridePendingTransition(R.anim.slide_in_up,
                    R.anim.slide_in_up_exit);
        });

        final Button blockCall = (Button) findViewById(R.id.logs_block_caller);
        blockCall.setTypeface(NewUtil.getFontRoman(this));
        blockCall.setTextSize(NewUtil.gettxtSize());
        blockCall.setOnClickListener(view -> Toast.makeText(getApplicationContext(), "block", Toast.LENGTH_SHORT).show());

        blockCall.setVisibility(View.INVISIBLE);

        sendMessage.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    sendMessage.setTextColor(Color.parseColor("#a9a9a9"));
                    break;
                case MotionEvent.ACTION_UP:
                    sendMessage.setTextColor(Color.parseColor("#015abb"));
                    break;
                case MotionEvent.ACTION_CANCEL:
                    sendMessage.setTextColor(Color.parseColor("#015abb"));
                    break;
            }
            return false;
        });
        callNumber.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    callNumber.setTextColor(Color.parseColor("#a9a9a9"));
                    break;
                case MotionEvent.ACTION_UP:
                    callNumber.setTextColor(Color.parseColor("#015abb"));
                    break;
                case MotionEvent.ACTION_CANCEL:
                    callNumber.setTextColor(Color.parseColor("#015abb"));
                    break;
            }
            return false;
        });

        createNewContact.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    createNewContact.setTextColor(Color.parseColor("#a9a9a9"));
                    break;
                case MotionEvent.ACTION_UP:
                    createNewContact.setTextColor(Color.parseColor("#015abb"));
                    break;
                case MotionEvent.ACTION_CANCEL:
                    createNewContact.setTextColor(Color.parseColor("#015abb"));
                    break;
            }
            return false;
        });

        addNumberToContacts.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    addNumberToContacts.setTextColor(Color
                            .parseColor("#a9a9a9"));
                    break;
                case MotionEvent.ACTION_UP:
                    addNumberToContacts.setTextColor(Color
                            .parseColor("#015abb"));
                    break;
                case MotionEvent.ACTION_CANCEL:
                    addNumberToContacts.setTextColor(Color
                            .parseColor("#015abb"));
                    break;
            }
            return false;
        });

        mButtonCancel.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        menu.clear();

        getMenuInflater().inflate(R.menu.logsdetail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.slide_out_down);
    }
}
