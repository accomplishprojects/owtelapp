package com.katadigital.owtel.modules.soundsettings;


import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.models.collectedobjects.AudioItem;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.ui.recyclerview.NotificationRecyclerViewAdapter;
import com.katadigital.ui.recyclerview.RingtoneRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * Created by Omar Matthew Reyes on 2/18/16.
 * Settings Fragment
 */
public class NotificationSettingFragment extends Fragment {
    public static final String TAG = "NotificationSettingFragment";
    private String TAGS = this.getClass().getSimpleName();
    private RecyclerView recyclerViewSettings;
    private ArrayList<AudioItem> audioLists = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sound_and_vibration, container, false);

        // Instantiate MainActivity
        MainActivity mainActivity = (MainActivity) getActivity();

        TextView textView = (TextView) view.findViewById(R.id.tv_fragment_title);
        textView.setText(R.string.notification_fragment_title);
        recyclerViewSettings = (RecyclerView) view.findViewById(R.id.rv_settings_sound_and_vibration);
        recyclerViewSettings.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewSettings.setLayoutManager(mLinearLayoutManager);

        // Fragment popBackStack()
        ImageButton mButtonBack = (ImageButton) view.findViewById(R.id.btn_settings_back);
        mButtonBack.setOnClickListener(v -> mainActivity.popFragment());
        mButtonBack.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//            RingtoneManager manager = new RingtoneManager(getActivity());
//            manager.setType(RingtoneManager.TYPE_NOTIFICATION);
//            Cursor cursor = manager.getCursor();
//        audioLists.add(new AudioItem("None",null));
//        while (cursor.moveToNext()) {
//                String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
//            Uri toneURI = manager.getRingtoneUri(cursor.getPosition());
//                // Do something with the title and the URI of ringtone
//            audioLists.add(new AudioItem(title,toneURI));
//            }
//        NotificationRecyclerViewAdapter mAdapter = new NotificationRecyclerViewAdapter(audioLists, getActivity());
//        recyclerViewSettings.setAdapter(mAdapter);


        RingtoneManager manager = new RingtoneManager(getActivity());
        manager.setType(RingtoneManager.TYPE_NOTIFICATION);
        Cursor cursor = manager.getCursor();
        audioLists.add(new AudioItem("None", null));
        while (cursor.moveToNext()) {
            String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            Uri toneURI = manager.getRingtoneUri(cursor.getPosition());
            // Do something with the title and the URI of ringtone
            audioLists.add(new AudioItem(title, toneURI));
            Log.e(TAGS, "onResume: " + " Tone: " + title + " Tone URI: " + toneURI);
        }

        NotificationRecyclerViewAdapter mAdapter = new NotificationRecyclerViewAdapter(audioLists, getActivity());
        recyclerViewSettings.setAdapter(mAdapter);
    }
}

