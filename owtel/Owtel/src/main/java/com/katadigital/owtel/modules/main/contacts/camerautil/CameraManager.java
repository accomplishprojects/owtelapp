package com.katadigital.owtel.modules.main.contacts.camerautil;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import de.hdodenhof.circleimageview.CircleImageView;
import eu.janmuller.android.simplecropimage.CropImage;

import static android.R.attr.maxHeight;
import static android.R.attr.maxWidth;

public class CameraManager {
    private final static String TAG = "CameraManager";

    public static void onActivityResultCamera(int requestCode, int resultCode,
                                              Intent data, Activity act, CircleImageView mImageView,
                                              File mFileTemp, ContactDto contactDto) {

        if (resultCode == act.RESULT_OK) {
            switch (requestCode) {
                case CameraUtil.REQUEST_CODE_GALLERY:
                    try {
                        UCrop.of(data.getData(), Uri.fromFile(mFileTemp))
                                .withAspectRatio(16, 9)
                                .withMaxResultSize(maxWidth, maxHeight)
                                .start(act);

                    } catch (Exception e) {
                        Log.e(TAG, "Error while creating temp file", e);
                        Toast.makeText(act, "Creating temp file failed", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case CameraUtil.REQUEST_CODE_TAKE_PICTURE:
                    UCrop.of(Uri.fromFile(mFileTemp), Uri.fromFile(mFileTemp))
                            .withAspectRatio(16, 9)
                            .withMaxResultSize(maxWidth, maxHeight)
                            .start(act);
                    break;

                case CameraUtil.REQUEST_CODE_CROP_IMAGE:
                    Toast.makeText(act, "Cropping failed", Toast.LENGTH_SHORT).show();
                    break;

                case UCrop.REQUEST_CROP:
                    final Uri resultUri = UCrop.getOutput(data);

                    try {
                        Bitmap bm = MediaStore.Images.Media.getBitmap(act.getContentResolver(), resultUri);
                        Log.e(TAG, "onActivityResultCamera: " + resultUri.toString());
                        contactDto.setProfilepic(bm);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case UCrop.RESULT_ERROR:
                    final Throwable cropError = UCrop.getError(data);
                    Toast.makeText(act, cropError.toString(), Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {
            return;
        }
    }

    public static Bitmap scaleWithAspectRatio(Bitmap image, Activity activity) {
        int imaheVerticalAspectRatio, imageHorizontalAspectRatio;
        float bestFitScalingFactor = 0;
        float percesionValue = (float) 0.2;

        // getAspect Ratio of Image
        int imageHeight = (int) (Math.ceil((double) image.getHeight() / 100) * 100);
        int imageWidth = (int) (Math.ceil((double) image.getWidth() / 100) * 100);
        int GCD = BigInteger.valueOf(imageHeight).gcd(BigInteger.valueOf(imageWidth)).intValue();
        imaheVerticalAspectRatio = imageHeight / GCD;
        imageHorizontalAspectRatio = imageWidth / GCD;
        Log.i(TAG, "Image Dimensions(W:H): " + imageWidth + ":" + imageHeight);
        Log.i(TAG, "Image AspectRatio(W:H): " + imageHorizontalAspectRatio + ":" + imaheVerticalAspectRatio);

        // getContainer Dimensions
        int displayWidth = activity.getWindowManager().getDefaultDisplay().getWidth();
        int displayHeight = activity.getWindowManager().getDefaultDisplay().getHeight();
        // I wanted to show the image to fit the entire device, as a best case.
        // So my ccontainer dimensions were displayWidth & displayHeight. For
        // your case, you will need to fetch container dimensions at run time or
        // you can pass static values to these two parameters

        int leftMargin = 0;
        int rightMargin = 0;
        int topMargin = 0;
        int bottomMargin = 0;
        int containerWidth = displayWidth - (leftMargin + rightMargin);
        int containerHeight = displayHeight - (topMargin + bottomMargin);
        Log.i(TAG,
                "Container dimensions(W:H): " + containerWidth + ":"
                        + containerHeight);

        // iterate to get bestFitScaleFactor per constraints
        while ((imageHorizontalAspectRatio * bestFitScalingFactor <= containerWidth)
                && (imaheVerticalAspectRatio * bestFitScalingFactor <= containerHeight)) {
            bestFitScalingFactor += percesionValue;
        }

        // return bestFit bitmap
        int bestFitHeight = (int) (imaheVerticalAspectRatio * bestFitScalingFactor);
        int bestFitWidth = (int) (imageHorizontalAspectRatio * bestFitScalingFactor);
        Log.i(TAG, "bestFitScalingFactor: "
                + bestFitScalingFactor);
        Log.i(TAG,
                "bestFitOutPutDimesions(W:H): " + bestFitWidth + ":"
                        + bestFitHeight);
        image = Bitmap.createScaledBitmap(image, bestFitWidth, bestFitHeight,
                true);

        // Position the bitmap centre of the container
        int leftPadding = (containerWidth - image.getWidth()) / 2;
        int topPadding = (containerHeight - image.getHeight()) / 2;
        Bitmap backDrop = Bitmap.createBitmap(containerWidth, containerHeight,
                Bitmap.Config.RGB_565);
        Canvas can = new Canvas(backDrop);
        can.drawBitmap(image, leftPadding, topPadding, null);

        return image;
    }

}
