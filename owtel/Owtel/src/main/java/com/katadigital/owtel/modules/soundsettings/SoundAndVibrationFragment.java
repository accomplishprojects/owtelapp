package com.katadigital.owtel.modules.soundsettings;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.ui.recyclerview.SoundAndVibrateRecyclerViewAdapter;

/**
 * Created by Omar Matthew Reyes on 2/18/16.
 * Settings Fragment
 */
public class SoundAndVibrationFragment extends Fragment {
    public static final String TAG = "SoundAndVibrationFragment";
    private RecyclerView recyclerViewSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sound_and_vibration, container, false);

        // Instantiate MainActivity
        MainActivity mainActivity = (MainActivity) getActivity();


        recyclerViewSettings = (RecyclerView) view.findViewById(R.id.rv_settings_sound_and_vibration);
        recyclerViewSettings.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewSettings.setLayoutManager(mLinearLayoutManager);

        // specify an adapter (see also next example)
        //        String[] settingsDataSet = getResources().getStringArray(R.array.list_item_settings_sound_and_vibrate);
//        SoundAndVibrateRecyclerViewAdapter mAdapter = new SoundAndVibrateRecyclerViewAdapter(settingsDataSet, getActivity());
//        recyclerViewSettings.setAdapter(mAdapter);

        // Fragment popBackStack()
        ImageButton mButtonBack = (ImageButton) view.findViewById(R.id.btn_settings_back);
        mButtonBack.setOnClickListener(v -> mainActivity.popFragment());
            mButtonBack.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        String[] settingsDataSet = getResources().getStringArray(R.array.list_item_settings_sound_and_vibrate);
        SoundAndVibrateRecyclerViewAdapter mAdapter = new SoundAndVibrateRecyclerViewAdapter(settingsDataSet, getActivity());
        recyclerViewSettings.setAdapter(mAdapter);
    }
}

