package com.katadigital.owtel.modules.main.contacts.util;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;
import android.util.Log;

import com.katadigital.owtel.modules.main.contacts.ContactsFragment;

/**
 * Created by Omar Matthew Reyes on 12/3/15.
 * Async Task to filter Contacts list in background thread
 */
public class AsyncTaskContactsSearch extends AsyncTask<String, String, String> {
    private static final String TAG = "AsyncTaskContactsSearch";
    private Context context;
    private String query, currentFilter;
    private SimpleCursorAdapter cursorAdapter;
    private LoaderManager loaderManager;
    private LoaderManager.LoaderCallbacks<Cursor> loaderCallbacks;

    public AsyncTaskContactsSearch(Context context, String query, String currentFilter,
                                   SimpleCursorAdapter cursorAdapter, LoaderManager loaderManager,
                                   LoaderManager.LoaderCallbacks<Cursor> loaderCallbacks) {
        this.context = context;
        this.query = query;
        this.currentFilter = currentFilter;
        this.cursorAdapter = cursorAdapter;
        this.loaderManager = loaderManager;
        this.loaderCallbacks = loaderCallbacks;
    }

    @Override
    protected String doInBackground(String... params) {
//        cursorAdapter.getFilter().filter(query);
        Log.i(TAG, "doInBackground");
        Log.i(TAG, "Search query: " + query);

        currentFilter = (!TextUtils.isEmpty(query) ? query : null);
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.i(TAG, "onPostExecute");
        ContactsFragment.mCurrentFilter = currentFilter;
        loaderManager.restartLoader(0, null, loaderCallbacks);
    }
}
