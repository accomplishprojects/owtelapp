package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.IMDto;

public class EditHelperJMIM {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    int fieldIM_ID = 0;

    ArrayList<Button> btnIMspinner = new ArrayList<Button>();
    boolean custom = false;

    public EditHelperJMIM(EditContactsActivity activity,
                          EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;
        this.fieldIM_ID = editHelp.fieldIM_ID;
        this.btnIMspinner = editHelp.btnIMspinner;
    }

    public ContactDto createLoadedIMField(IMDto im, final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.im_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setId(fieldIM_ID);
        spinnerbtn.setId(fieldIM_ID);
        btnDelete.setId(fieldIM_ID);
        editTxt.setId(fieldIM_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnIMspinner.add(spinnerbtn);
        editTxt.setFocusableInTouchMode(false);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        editTxt.setFocusableInTouchMode(true);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.imspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        int selecteditem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(im.getImProtocol())) {
                custom = false;
                selecteditem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(im.getImProtocol())) {
                custom = true;
                btnIMspinner.get(spinnerbtn.getId())
                        .setText(im.getImProtocol());
                btnIMspinner.get(spinnerbtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnIMspinner.get(spinnerbtn.getId()).setText(
                    myResArray[selecteditem]);
        } else {
            btnIMspinner.get(spinnerbtn.getId()).setText(im.getImProtocol());
        }

        btnIMspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivityCustomLoader(activity,
                                btnIMspinner, spinnerbtn, R.array.imspinner,
                                custom, true);
                    }
                });

        btnIMspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnIMspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.imField.get(spinnerbtn.getId())
                                .setImProtocol(txtbtn);
                    }
                });

        btnIMspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnIMspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.imField.get(spinnerbtn.getId())
                                .setImProtocol(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        // Get Text
        editTxt.setText(im.getIm());

        editTxt.setHint(activity.getResources().getString(R.string.adding_im));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.imField.get(editTxt.getId()).setIm(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.imField.get(editTxt.getId()).setIm("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.imField.remove(btnDelete.getId());
                contactDto.imField.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnIMspinner.get(spinnerbtn.getId()).getText()
                .toString();

        im.setId(spinnerbtn.getId());
        im.setIm(editTxt.getText().toString());
        im.setImProtocol(txtbtn);
        contactDto.addIM(im);

        holderfield.addView(lLayoutPanel);
        fieldIM_ID++;

        return contactDto;
    }

    public void addIMFunction(final ContactDto contactDto) {

        IMDto im = new IMDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.im_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setId(fieldIM_ID);
        spinnerbtn.setId(fieldIM_ID);
        btnDelete.setId(fieldIM_ID);
        editTxt.setId(fieldIM_ID);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        btnIMspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.imspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnIMspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnIMspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnIMspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnIMspinner.get(spinnerbtn.getId()).setTag(myResArray.length - 1);
        }

        btnIMspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnIMspinner,
                                spinnerbtn, R.array.imspinner);
                    }
                });

        btnIMspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnIMspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.imField.get(spinnerbtn.getId())
                                .setImProtocol(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setHint(activity.getResources().getString(R.string.adding_im));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.imField.get(editTxt.getId()).setIm(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.imField.get(editTxt.getId()).setIm("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.imField.remove(btnDelete.getId());
                contactDto.imField.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnIMspinner.get(spinnerbtn.getId()).getText()
                .toString();

        im.setId(spinnerbtn.getId());
        im.setIm("");
        im.setImProtocol(txtbtn);
        contactDto.addIM(im);

        holderfield.addView(lLayoutPanel);
        fieldIM_ID++;
    }
}
