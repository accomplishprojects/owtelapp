package com.katadigital.owtel.modules.main.messaging;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.Message;
import com.katadigital.owtel.daoDb.MessageDao;
import com.katadigital.owtel.models.MessageList;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.messaging.adapters.ConversationListAdapter;
import com.katadigital.owtel.modules.main.messaging.adapters.RecyclerItemClickListener;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.owtel.utils.Logger;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.portsip.helper.Ring;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Erick Lester Sumugat on 2/23/2016.
 * Updated by Jose Mari Lumanlan
 */
public class ConversationsFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener, ISimpleDialogListener {

    private static final String TAG = "ConversationFragment";
    private static final int DELETE_COVERSATION_THREAD = 56;
    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerView;
    private android.os.Message androidMessage;

    private ProgressBar progressBar;
    private String currentThreadID = "";

    ConversationListAdapter conversationListAdapter;


    CompositeSubscription subscriptions;
    private boolean isOnResume = false;
    private int lastFirstVisiblePosition = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.conversation_list_layout, container, false);
        NotificationHelper.init(getActivity());
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_conversations);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        ImageButton newMessageBtn = (ImageButton) view.findViewById(R.id.btn_message_new);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.getItemAnimator().setChangeDuration(0);


        newMessageBtn.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), ChatActivity.class);
            getActivity().startActivityForResult(i, MainActivity.REQUEST_TOP_UP);
        });

        getCoversationListAPI();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        try {
                            Message message = conversationListAdapter.getMessage(position);
                            Intent i = new Intent(getActivity(), ChatActivity.class);
                            if (new Functions().getNumber().equals(message.getSender())) {
                                i.putExtra("receiver", message.getReceiver());
                            } else {
                                i.putExtra("receiver", message.getSender());
                            }
                            i.putExtra("thread_id", message.getRoomID());
                            i.putExtra("sms_id", message.getMessageID());
                            message.setIs_message_read("1");
                            message.setIs_deleted("0");
                            Log.d(TAG, "onItemClick: " + message.toString());
                            OwtelAppController.getInstance().getDaoSession().getMessageDao().deleteByKey(message.getId());
                            OwtelAppController.getInstance().getDaoSession().getMessageDao().insert(message);
                            getActivity().startActivityForResult(i, MainActivity.REQUEST_TOP_UP);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                        // ...

                        final String myNumber = new Functions().getNumber();
                        Message message = conversationListAdapter.getMessage(position);
                        currentThreadID = message.getRoomID();
                        String CHAT_PEER = "";
                        if (message.getSender().equals(myNumber)) {
                            CHAT_PEER = message.getReceiver();

                        } else {
                            CHAT_PEER = message.getSender();
                        }
                        StringFormatter.doGetContactName(CHAT_PEER).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(String contactName) {
                                SimpleDialogFragment.createBuilder(getActivity(), getActivity().getSupportFragmentManager()).setTitle("Delete Messages").setMessage("Delete conversation with " + contactName + "?").setTargetFragment(ConversationsFragment.this, DELETE_COVERSATION_THREAD).setPositiveButtonText(getString(R.string.ok)).setNegativeButtonText(getString(R.string.cancel)).show();
                            }
                        });
                    }
                }));

        return view;
    }

    @Override
    public void onRefresh() {
        getCoversationListAPI();
        new Handler().postDelayed(() -> {
            swipeRefreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.GONE);
        }, 1800);
    }


    @Override
    public void onStart() {
        super.onStart();
        subscribeToEvents();
//        OwtelAppController.getInstance().startWebSocketService();
    }

    @Override
    public void onStop() {
        super.onStop();
        subscriptions.unsubscribe();
//        OwtelAppController.getInstance().stopWebSocketService();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");
        NotificationHelper.getNotificationHelperInstance().unotifyReceivedMessage();
        NotificationHelper.getNotificationHelperInstance().unotifyOnMessageReceived();
        swipeRefreshLayout.post(() -> {
            //            swipeRefreshLayout.setRefreshing(true);
            //disable as suggested
            swipeRefreshLayout.setRefreshing(false);
            isOnResume = true;
            lastFirstVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            onRefresh();
        });
    }

    private void getCoversationListAPI() {
        Log.d(TAG, "getCoversationListAPI: started");
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiService.SERVICE_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        ApiService service = retrofit.create(ApiService.class);

        service.getMessagesConversationRxJava(new Functions().getNumber())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MessageList>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.toString());
                        setAdapter();
                    }

                    @Override
                    public void onNext(MessageList messageList) {
                        Log.d(TAG, "onNext: " + messageList.toString());
                        String myNumber = new Functions().getNumber();
                        for (com.katadigital.owtel.models.Message messageModel : messageList.getMessage()
                                ) {
                            Message message = new Message(Long.parseLong(messageModel.getSmsId()));
                            message.setRoomID(messageModel.getThreadId());
                            message.setMessageID(messageModel.getSmsId());
                            message.setType("text");
                            message.setValue(messageModel.getSmsContent());
                            message.setReceiver(messageModel.getReceiver());
                            message.setSender(messageModel.getSender());
                            message.setCreated(messageModel.getSmsDate());
                            message.setUserID(myNumber);
                            message.setIs_message_read("0");
                            message.setIs_deleted("0");
                            saveToDB(message);
                        }

                        Log.d(TAG, "onNext: result " + messageList.toString());
                        setAdapter();
                    }
                });
    }

    private void setAdapter() {
        List<Message> messagesFromDB = getListMessageFromDbInModelForm();
        if (conversationListAdapter == null) {
            conversationListAdapter = new ConversationListAdapter(getActivity(), messagesFromDB);
            recyclerView.setAdapter(conversationListAdapter);
            recyclerView.smoothScrollToPosition(0);
        }

        if (isOnResume) {
            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPosition(lastFirstVisiblePosition);
            conversationListAdapter.updateData(messagesFromDB);
            isOnResume = false;
            lastFirstVisiblePosition = 0;
        } else {
            conversationListAdapter.updateData(messagesFromDB);
            recyclerView.smoothScrollToPosition(0);
        }
        swipeRefreshLayout.setRefreshing(false);

        progressBar.setVisibility(View.GONE);
    }

    private void subscribeToEvents() {
        subscriptions = new CompositeSubscription();

        subscriptions.add(MainActivity.rxBus.toObservable().subscribe(event -> {
            if (event instanceof Message) {
                /**Play notification sound*/
                Ring.getInstance(getActivity()).startNotificationTone(false);

                Logger.e(TAG, "Message Event Detected!");
                updateConversationList((Message) event);
            }
        }));


    }

    private void updateConversationList(Message event) {
        saveToDB(event);

        swipeRefreshLayout.post(() -> {
            swipeRefreshLayout.setRefreshing(true);
            onRefresh();
        });
    }


    private static void saveToDB(Message message) {
        // save to db
        MessageDao messageDao = OwtelAppController.getInstance().getDaoSession().getMessageDao();
        if (messageDao.queryBuilder().where(MessageDao.Properties.MessageID.eq(message.getMessageID())).list().isEmpty()) {
            messageDao.insertOrReplace(message);
            Log.d(TAG, "saveToDB: " + message.toString());
        }
    }


    private void deleteConversationThread(String thread_id) {

        List<Message> messages = OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder().where(MessageDao.Properties.RoomID.eq(thread_id)).list();
        for (Message message : messages
                ) {
            message.setIs_deleted("1");
            OwtelAppController.getInstance().getDaoSession().getMessageDao().update(message);
        }
        onRefresh();
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
        Log.d(TAG, "deleteConversationThread: onNegativeButtonClicked thread_id : " + currentThreadID);
        switch (requestCode) {
            case DELETE_COVERSATION_THREAD:
                break;
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
        Log.d(TAG, "deleteConversationThread: onNeutralButtonClicked thread_id : " + currentThreadID);
        switch (requestCode) {
            case DELETE_COVERSATION_THREAD:
                break;
        }
    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        Log.d(TAG, "deleteConversationThread: onPositiveButtonClicked thread_id : " + currentThreadID);

        switch (requestCode) {
            case DELETE_COVERSATION_THREAD:
                deleteConversationThread(currentThreadID);
                break;
        }
    }

    private List<Message> getListMessageFromDbInModelForm() {

        ArrayList<Message> result = new ArrayList<>();

        String query =  "SELECT *, COUNT(*) , MAX("+MessageDao.Properties.Created.columnName+") FROM "+MessageDao.TABLENAME + " GROUP BY "+MessageDao.Properties.RoomID.columnName +" ORDER BY "+MessageDao.Properties.Created.columnName+" DESC";

        Cursor c = OwtelAppController.getInstance().getDaoSession().getMessageDao().getDatabase().rawQuery(query, null);


                try {
            if (c.moveToFirst()) {
                do {
                    String processNumber;
                    Message simpleMessageModel = new Message();
                    simpleMessageModel.setId(Long.valueOf(c.getString(0)));
                    simpleMessageModel.setRoomID(c.getString(1));
                    simpleMessageModel.setMessageID(c.getString(2));
                    simpleMessageModel.setValue(c.getString(3));
                    simpleMessageModel.setType(c.getString(4));
                    simpleMessageModel.setCreated(c.getString(5));
                    simpleMessageModel.setStatus(c.getString(6));
                    simpleMessageModel.setUserID(c.getString(7));
                    simpleMessageModel.setSender(c.getString(8));
                    simpleMessageModel.setReceiver(c.getString(9));
                    simpleMessageModel.setIs_message_read(c.getString(10));

                    if (simpleMessageModel.getSender() != null) {
                        if (simpleMessageModel.getSender().equals(c.getString(7))) {
                            processNumber = simpleMessageModel.getReceiver();
                            String formattedNumber = PhoneNumberUtils.formatNumber(new StringFormatter().getContactNameViaMainThread(processNumber));
                            simpleMessageModel.setDisplay_name(formattedNumber);
                        } else {
                            processNumber = simpleMessageModel.getSender();
                            String formattedNumber = PhoneNumberUtils.formatNumber(new StringFormatter().getContactNameViaMainThread(processNumber));
                            simpleMessageModel.setDisplay_name(formattedNumber);
                        }
                    } else {
                        Toast.makeText(getActivity(), simpleMessageModel.getSender(), Toast.LENGTH_SHORT).show();
                    }
                    result.add(simpleMessageModel);
                    Log.d(TAG, "LOG IS READ: " + simpleMessageModel.toString());
                } while (c.moveToNext());
            }
        } finally {
            c.close();
        }

        return result;





//        String query =  "SELECT * FROM " +MessageDao.TABLENAME + "WHERE (" +MessageDao.Properties.RoomID.columnName + ", " +MessageDao.Properties.Created.columnName + ") IN (SELECT " +MessageDao.Properties.RoomID.columnName + ", max(" +MessageDao.Properties.Created.columnName + ") as " +MessageDao.Properties.Created.columnName + "FROM " +MessageDao.TABLENAME + "GROUP BY " +MessageDao.Properties.RoomID.columnName + ")";

//Original query
//        result = OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder()
//                .where(new WhereCondition.StringCondition(MessageDao.Properties.UserID.columnName + "=" + new Functions().getNumber() + " and " + MessageDao.Properties.Is_deleted.columnName + "=" + 0 + " GROUP BY " + MessageDao.Properties.RoomID.columnName))
//                .orderDesc(MessageDao.Properties.Created).build().list();

//my created query
//        result = OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder()
//                .where(new WhereCondition.StringCondition(query))
//                .orderDesc(MessageDao.Properties.Created).build().list();



//
//        Log.e(TAG, "new result set: " + result);
//        for (Message message : result
//                ) {
//
//            String processNumber;
//            if (message.getSender() != null) {
//                if (message.getSender().equals(message.getUserID())) {
//                    processNumber = message.getReceiver();
//                    String formattedNumber = PhoneNumberUtils.formatNumber(new StringFormatter().getContactDetailsViaMainThread(processNumber));
//                    message.setDisplay_name(formattedNumber);
//                } else {
//                    processNumber = message.getSender();
//                    String formattedNumber = PhoneNumberUtils.formatNumber(new StringFormatter().getContactDetailsViaMainThread(processNumber));
//                    message.setDisplay_name(formattedNumber);
//                }
//            } else {
//                Toast.makeText(getActivity(), message.getSender(), Toast.LENGTH_SHORT).show();
//            }
//            Log.e(TAG, "MyTracker roomID:" + message.getRoomID()+" smsID:" + message.getMessageID()+" value:" + message.getValue()+" created: " + message.getCreated());
//            result2.add(message);
//        }


    }
}


