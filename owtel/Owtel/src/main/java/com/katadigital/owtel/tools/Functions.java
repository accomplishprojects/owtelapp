package com.katadigital.owtel.tools;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.WindowManager;

import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.securepreferences.SecurePreferences;

/**
 * Created by Jose Mari Lumanlan on 4/20/16.
 * Modified: Omar Matthew Reyes
 * Modified: Rae-An Andres
 */
public class Functions {
    SharedPreferences preferencesUser;
    SecurePreferences securePreferences;

    public Functions() {
        SharedPreferenceManager.init(OwtelAppController.getInstance());
        preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");
        String pass = preferencesUser.getString(Constants.PREFS_USER_PASS, "empty_pass");
        String key = email + pass;
        securePreferences = OwtelAppController.getInstance()
                .getUserPinBasedSharedPreferences(key);
    }

    /**
     * Get user US number
     *
     * @return String
     */
    public String getNumber() {
        return SharedPreferenceManager.getSharedPreferenceManagerInstance().getOwtelNumber();
    }


    /**
     * Get user type
     *
     * @return boolean
     */
    public boolean getUserType() {
        return preferencesUser.getBoolean(Constants.PREFS_USER_TYPE, false);
    }

    public void hideKeyboard(Activity c) {
        c.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }
}
