package com.katadigital.owtel.models.collectedobjects;

import com.google.gson.Gson;

/**
 * Created by MIS on 8/2/2016.
 */
public class RegistrationResult {

    private String email;
    private String password;
    private boolean isWebCsUser;
    private String promoCode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isWebCsUser() {
        return isWebCsUser;
    }

    public void setWebCsUser(boolean webCsUser) {
        isWebCsUser = webCsUser;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
