package com.katadigital.owtel.models;

/**
 * Created by dcnc123 on 12/6/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IddCreditsObj {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("email_address")
    @Expose
    private String emailAddress;
    @SerializedName("subscription_id")
    @Expose
    private String subscriptionId;
    @SerializedName("subscription_name")
    @Expose
    private Object subscriptionName;
    @SerializedName("expiration_date")
    @Expose
    private String expirationDate;
    @SerializedName("subscription_status")
    @Expose
    private String subscriptionStatus;
    @SerializedName("idd_credits")
    @Expose
    private String iddCredits;

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @param emailAddress
     * The email_address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     *
     * @return
     * The subscriptionId
     */
    public String getSubscriptionId() {
        return subscriptionId;
    }

    /**
     *
     * @param subscriptionId
     * The subscription_id
     */
    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    /**
     *
     * @return
     * The subscriptionName
     */
    public Object getSubscriptionName() {
        return subscriptionName;
    }

    /**
     *
     * @param subscriptionName
     * The subscription_name
     */
    public void setSubscriptionName(Object subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    /**
     *
     * @return
     * The expirationDate
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     *
     * @param expirationDate
     * The expiration_date
     */
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     *
     * @return
     * The subscriptionStatus
     */
    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    /**
     *
     * @param subscriptionStatus
     * The subscription_status
     */
    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    /**
     *
     * @return
     * The iddCredits
     */
    public String getIddCredits() {
        return iddCredits;
    }

    /**
     *
     * @param iddCredits
     * The idd_credits
     */
    public void setIddCredits(String iddCredits) {
        this.iddCredits = iddCredits;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
