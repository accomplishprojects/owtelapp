package com.katadigital.owtel.modules.billing;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.utils.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jayhar Vallejos on 4/30/2016.
 */
public class PaymentSuccessFragment extends Fragment {
    private final static String TAG = "PaymentSuccessFragment";
    private MainActivity mainActivity;

    @Bind(R.id.payment_message)
    TextView tvPaymentMessage;
    @Bind(R.id.btn_continue)
    Button btnContinue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_success, container, false);
        ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        tvPaymentMessage.setText(getArguments().getString(Constants.BUNDLE_PAYMENT_MESSAGE));
        return view;
    }

    @OnClick(R.id.btn_continue)
    public void onClickContinue() {
        mainActivity.popFragment(true);
        mainActivity.popFragment();
    }
}

