package com.katadigital.owtel.modules.main.messaging.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.daoDb.Message;
import com.katadigital.owtel.utils.StringFormatter;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ChatAdapter";
    Context context;

    List<Message> messages;

    String selfId;

    private final int CHAT_SELF = 1;

    private final int CHAT_PEER = 2;


    public ChatAdapter(Context context, List<Message> messages, String selfId) {
        this.context = context;
        this.messages = messages;
        this.selfId = selfId;
    }

    public void updateData(List<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case CHAT_SELF:
                View item = inflater.inflate(R.layout.chat_item_self, viewGroup, false);
                SelfItemHolder itemHolder = new SelfItemHolder(item);
                holder = itemHolder;
                break;
            case CHAT_PEER:
                View itemMul = inflater.inflate(R.layout.chat_item_peer, viewGroup, false);
                PeerItemHolder itemHolderMul = new PeerItemHolder(itemMul);
                holder = itemHolderMul;
                break;
            default:
                break;
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case CHAT_SELF:
                configureSelfItem((SelfItemHolder) viewHolder, position);
                break;
            case CHAT_PEER:
                configurePeerItem((PeerItemHolder) viewHolder, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (messages.get(position).getRoute()!=null && messages.get(position).getRoute().equalsIgnoreCase("self")) {
//            return CHAT_SELF;
//        } else if (selfId.equals(messages.get(position).getUserID())) {
//            return CHAT_SELF;
//        } else {
//            return CHAT_PEER;
//        }
        if (messages.get(position).getRoute()!=null && messages.get(position).getRoute().equalsIgnoreCase("self")) {
            return CHAT_SELF;
        }
        if (selfId.equals(messages.get(position).getReceiver())) {
            return CHAT_PEER;
        } else {
            return CHAT_SELF;
        }


    }

    public Message getMessage(int position) {
        return messages.get(position);
    }

    private void configureSelfItem(SelfItemHolder itemHolder, int position) {
        Message message = messages.get(position);
        itemHolder.tvMessage.setText(message.getValue());
        itemHolder.tvMessageDate.setText(new StringFormatter().getFormattedDateTime(message.getCreated()));
        if (message.getStatus() != null && message.getStatus().equals("failed")) {
            itemHolder.tvimg_not_sent_dependency.setVisibility(View.VISIBLE);
            itemHolder.img_not_sent.setVisibility(View.VISIBLE);
        }else
        {
            itemHolder.tvimg_not_sent_dependency.setVisibility(View.GONE);
            itemHolder.img_not_sent.setVisibility(View.GONE);
        }
    }

    private void configurePeerItem(PeerItemHolder itemHolder, int position) {
        Message message = messages.get(position);
        itemHolder.tvMessage.setText(message.getValue());
        itemHolder.tvMessageDate.setText(new StringFormatter().getFormattedDateTime(message.getCreated()));


    }


    class PeerItemHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        TextView tvMessage;
        TextView tvMessageDate;

        public PeerItemHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_peer_message);
            tvMessageDate = (TextView) itemView.findViewById(R.id.tv_peer_message_date);
            itemView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {

            menu.setHeaderTitle("Message Options");
            menu.add(0, v.getId(), 0, "Delete");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "Copy");
            menu.add(0, v.getId(), 0, "View Details");

        }
    }

    class SelfItemHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        TextView tvMessage;
        TextView tvMessageDate;
        TextView tvimg_not_sent_dependency;
        ImageView img_not_sent;

        public SelfItemHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_self_message);
            tvMessageDate = (TextView) itemView.findViewById(R.id.tv_self_message_date);
            tvimg_not_sent_dependency = (TextView) itemView.findViewById(R.id.img_not_sent_dependency);
            img_not_sent = (ImageView) itemView.findViewById(R.id.img_not_sent);
            itemView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {

            menu.setHeaderTitle("Message Options");
            menu.add(0, v.getId(), 0, "Delete");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "Copy");
            menu.add(0, v.getId(), 0, "View Details");

        }
    }

}

