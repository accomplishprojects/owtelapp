package com.katadigital.owtel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Omar Matthew Reyes on 7/22/16.
 * Retrofit API Request API Object for City and State generated
 * from Zip Code
 */
public class ZipCodeCityState {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("state_name")
    @Expose
    private String stateName;
    @SerializedName("state_abbr")
    @Expose
    private String stateAbbr;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The countryId
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     * @param countryId The country_id
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     * @return The countryName
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * @param countryName The country_name
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * @return The stateId
     */
    public String getStateId() {
        return stateId;
    }

    /**
     * @param stateId The state_id
     */
    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    /**
     * @return The stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * @param stateName The state_name
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * @return The stateAbbr
     */
    public String getStateAbbr() {
        return stateAbbr;
    }

    /**
     * @param stateAbbr The state_abbr
     */
    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    /**
     * @return The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     * @param cityId The city_id
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * @return The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @param cityName The city_name
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
