package com.katadigital.owtel.models;

/**
 * Created by dcnc123 on 11/28/16.
 */


import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginObj {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("rad_session_id")
    @Expose
    private String radSessionId;
    @SerializedName("sip_server")
    @Expose
    private String sipServer;
    @SerializedName("sip_port")
    @Expose
    private String sipPort;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("sip_number")
    @Expose
    private String sipNumber;
    @SerializedName("sip_username")
    @Expose
    private String sipUsername;
    @SerializedName("sip_password")
    @Expose
    private String sipPassword;
    @SerializedName("promo_code")
    @Expose
    private String promoCode;
    @SerializedName("expiration_date")
    @Expose
    private String expirationDate;
    @SerializedName("subscription_status")
    @Expose
    private String subscriptionStatus;
    @SerializedName("idd_credits")
    @Expose
    private String iddCredits;
    @SerializedName("message")
    @Expose
    private String message;
    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The radSessionId
     */
    public String getRadSessionId() {
        return radSessionId;
    }

    /**
     *
     * @param radSessionId
     * The rad_session_id
     */
    public void setRadSessionId(String radSessionId) {
        this.radSessionId = radSessionId;
    }

    /**
     *
     * @return
     * The sipServer
     */
    public String getSipServer() {
        return sipServer;
    }

    /**
     *
     * @param sipServer
     * The sip_server
     */
    public void setSipServer(String sipServer) {
        this.sipServer = sipServer;
    }

    /**
     *
     * @return
     * The sipPort
     */
    public String getSipPort() {
        return sipPort;
    }

    /**
     *
     * @param sipPort
     * The sip_port
     */
    public void setSipPort(String sipPort) {
        this.sipPort = sipPort;
    }

    /**
     *
     * @return
     * The region
     */
    public String getRegion() {
        return region;
    }

    /**
     *
     * @param region
     * The region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     *
     * @return
     * The sipNumber
     */
    public String getSipNumber() {
        return sipNumber;
    }

    /**
     *
     * @param sipNumber
     * The sip_number
     */
    public void setSipNumber(String sipNumber) {
        this.sipNumber = sipNumber;
    }

    /**
     *
     * @return
     * The sipUsername
     */
    public String getSipUsername() {
        return sipUsername;
    }

    /**
     *
     * @param sipUsername
     * The sip_username
     */
    public void setSipUsername(String sipUsername) {
        this.sipUsername = sipUsername;
    }

    /**
     *
     * @return
     * The sipPassword
     */
    public String getSipPassword() {
        return sipPassword;
    }

    /**
     *
     * @param sipPassword
     * The sip_password
     */
    public void setSipPassword(String sipPassword) {
        this.sipPassword = sipPassword;
    }

    /**
     *
     * @return
     * The promoCode
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     *
     * @param promoCode
     * The promo_code
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    /**
     *
     * @return
     * The expirationDate
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     *
     * @param expirationDate
     * The expiration_date
     */
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     *
     * @return
     * The subscriptionStatus
     */
    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    /**
     *
     * @param subscriptionStatus
     * The subscription_status
     */
    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    /**
     *
     * @return
     * The iddCredits
     */
    public String getIddCredits() {
        return iddCredits;
    }

    /**
     *
     * @param iddCredits
     * The idd_credits
     */
    public void setIddCredits(String iddCredits) {
        this.iddCredits = iddCredits;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

