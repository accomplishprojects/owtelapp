package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.AddContactActivity;
import com.katadigital.owtel.modules.main.contacts.AddRelatedActivity;
import com.katadigital.owtel.modules.main.contacts.entities.AddressDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.DateDto;
import com.katadigital.owtel.modules.main.contacts.entities.EmailDto;
import com.katadigital.owtel.modules.main.contacts.entities.IMDto;
import com.katadigital.owtel.modules.main.contacts.entities.PhoneNumberDto;
import com.katadigital.owtel.modules.main.contacts.entities.RelationDto;
import com.katadigital.owtel.modules.main.contacts.entities.WebsiteDto;
import com.katadigital.portsip.model.PhoneNumberModel;

// Jeff Spinner Field Custom

public class AddHelperField {

    AddContactActivity activity;

    public static ArrayList<Button> btnPhonespinner = new ArrayList<Button>();
    public static ArrayList<Button> btnEmailspinner = new ArrayList<Button>();
    public static ArrayList<Button> btnURLspinner = new ArrayList<Button>();
    public static ArrayList<Button> btnAddresspinner = new ArrayList<Button>();
    public static ArrayList<Button> btnDatespinner = new ArrayList<Button>();
    public static ArrayList<Button> btnREspinner = new ArrayList<Button>();
    public static ArrayList<Button> btnIMspinner = new ArrayList<Button>();

    public static boolean hasBirthday = false;

    int fieldPhone_ID = 0;
    int fieldEmail_ID = 0;
    int fieldURL_ID = 0;
    int fieldAddress_ID = 0;
    int fieldDate_ID = 0;
    int fieldRE_ID = 0;
    int fieldIM_ID = 0;

    Typeface font1;
    Typeface font2;

    boolean custom = false;
    String TAG = this.getClass().getSimpleName();

    /**
     * Usable Modifications
     */
    public static ArrayList<PhoneNumberModel> checkerForPhoneNumbers = new ArrayList<>();
    public static int checkerForPhoneNumbersIndex = -1;

    public AddHelperField(AddContactActivity activity) {
        this.activity = activity;
        this.font1 = NewUtil.getFontBold(activity);
        this.font2 = NewUtil.getFontRoman(activity);
    }

    public static void clearAllButtonList() {
        btnPhonespinner.clear();
        btnEmailspinner.clear();
        btnURLspinner.clear();
        btnAddresspinner.clear();
        hasBirthday = false;
        btnDatespinner.clear();
        btnREspinner.clear();
        btnIMspinner.clear();
    }

    public void addPhoneFunction(final ContactDto contactDto) {

        /**Usable Modifications*/
        ++AddHelperField.checkerForPhoneNumbersIndex;
        PhoneNumberModel phoneNumberModel = new PhoneNumberModel();

        PhoneNumberDto phoneNumberDto = new PhoneNumberDto();

        final LinearLayout holderField = (LinearLayout) activity.findViewById(R.id.phone_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel.inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel.findViewById(R.id.delete_Btn);
        final Button spinnerBtn = (Button) lLayoutPanel.findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel.findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel.findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel.findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerBtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerBtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        delete.setId(fieldPhone_ID);
        spinnerBtn.setId(fieldPhone_ID);
        btnDelete.setId(fieldPhone_ID);
        editTxt.setId(fieldPhone_ID);

        btnPhonespinner.add(spinnerBtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(R.array.numberspinner);
        boolean inBounds = (spinnerBtn.getId() >= 0) && (spinnerBtn.getId() < myResArray.length);
        if (inBounds) {
            btnPhonespinner.get(spinnerBtn.getId()).setText(myResArray[spinnerBtn.getId()]);
            btnPhonespinner.get(spinnerBtn.getId()).setTag(spinnerBtn.getId());
        } else {
            btnPhonespinner.get(spinnerBtn.getId()).setText(myResArray[myResArray.length - 1]);
            btnPhonespinner.get(spinnerBtn.getId()).setTag(myResArray.length - 1);
        }

        btnPhonespinner.get(spinnerBtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnPhonespinner, spinnerBtn, R.array.numberspinner);
                    }
                });

        btnPhonespinner.get(spinnerBtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnPhonespinner.get(spinnerBtn.getId())
                                .getText().toString();
                        contactDto.phoneNumbers.get(spinnerBtn.getId())
                                .setNumberType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setInputType(InputType.TYPE_CLASS_PHONE);
        editTxt.setHint(activity.getResources().getString(
                R.string.string_hit_phone));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.phoneNumbers.get(editTxt.getId()).setNumber(String.valueOf(charSequence));
                Log.e(TAG, "onTextChanged: " + String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }

                checkerForPhoneNumbers.get(checkerForPhoneNumbersIndex)
                        .setPhoneNumber(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.phoneNumbers.get(editTxt.getId()).setNumber("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDto.phoneNumbers.remove(btnDelete.getId());
                contactDto.phoneNumbers.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();

                /**Usable Modification*/
                checkerForPhoneNumbers.remove(checkerForPhoneNumbersIndex);
                --checkerForPhoneNumbersIndex;
            }
        });

        String txtbtn = btnPhonespinner.get(spinnerBtn.getId()).getText()
                .toString();

        int tag = (Integer) btnPhonespinner.get(spinnerBtn.getId()).getTag();

        phoneNumberDto.setId(spinnerBtn.getId());
        phoneNumberDto.setNumber("");
        phoneNumberDto.setNumberType(txtbtn);
        contactDto.addPhoneNumbers(phoneNumberDto);
        holderField.addView(lLayoutPanel);
        fieldPhone_ID++;

        /**Usable Modifications*/
        checkerForPhoneNumbers.add(phoneNumberModel);

    }

    public void addEmailFunction(final ContactDto contactDto) {

        EmailDto mail = new EmailDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.email_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel.findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel.findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel.findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel.findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel.findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        delete.setId(fieldEmail_ID);
        spinnerbtn.setId(fieldEmail_ID);
        btnDelete.setId(fieldEmail_ID);
        editTxt.setId(fieldEmail_ID);

        editTxt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        btnEmailspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.emailspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnEmailspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnEmailspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnEmailspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnEmailspinner.get(spinnerbtn.getId()).setTag(
                    myResArray.length - 1);
        }

        btnEmailspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnEmailspinner,
                                spinnerbtn, R.array.emailspinner);
                    }
                });

        btnEmailspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnEmailspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.emails.get(spinnerbtn.getId()).setEmailType(
                                txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setHint(activity.getResources().getString(
                R.string.string_hit_email));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.emails.get(editTxt.getId()).setEmail(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.emails.get(editTxt.getId()).setEmail("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDto.emails.remove(btnDelete.getId());
                contactDto.emails.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnEmailspinner.get(spinnerbtn.getId()).getText()
                .toString();

        int tag = (Integer) btnEmailspinner.get(spinnerbtn.getId()).getTag();

        mail.setId(spinnerbtn.getId());
        mail.setEmail("");
        mail.setEmailType(txtbtn);
        contactDto.addEmails(mail);

        holderfield.addView(lLayoutPanel);
        fieldEmail_ID++;
    }

    public void addRingtoneFunction(final ContactDto contactDto) {

    }

    public void addURLFunction(final ContactDto contactDto) {
        WebsiteDto web = new WebsiteDto();

        final LinearLayout holderfield = (LinearLayout) activity.findViewById(R.id.url_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel.inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel.findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel.findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel.findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel.findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel.findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        delete.setId(fieldURL_ID);
        spinnerbtn.setId(fieldURL_ID);
        btnDelete.setId(fieldURL_ID);
        editTxt.setId(fieldURL_ID);

        editTxt.setInputType(InputType.TYPE_TEXT_VARIATION_URI);

        btnURLspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        String[] myResArray = activity.getResources().getStringArray(
                R.array.websitespinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        if (inBounds) {
            btnURLspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnURLspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnURLspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnURLspinner.get(spinnerbtn.getId()).setTag(myResArray.length - 1);
        }

        btnURLspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnURLspinner,
                                spinnerbtn, R.array.websitespinner);
                    }
                });

        btnURLspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnURLspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.websites.get(spinnerbtn.getId())
                                .setWebsiteType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setHint(activity.getResources().getString(
                R.string.string_website));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.websites.get(editTxt.getId()).setWebsite(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.websites.get(editTxt.getId()).setWebsite("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.websites.remove(btnDelete.getId());
                contactDto.websites.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnURLspinner.get(spinnerbtn.getId()).getText()
                .toString();

        int tag = (Integer) btnURLspinner.get(spinnerbtn.getId()).getTag();

        web.setId(spinnerbtn.getId());
        web.setWebsite("");
        web.setWebsiteType(txtbtn);
        contactDto.addWebsite(web);

        holderfield.addView(lLayoutPanel);
        fieldURL_ID++;

    }

    public void addAddressFunction(final ContactDto contactDto) {

        AddressDto addressDto = new AddressDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.address_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_address_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        // final Button clearBtn = (Button) lLayoutPanel
        // .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);

        final EditText edStreet = (EditText) lLayoutPanel
                .findViewById(R.id.edStreet);
        final EditText edNeighborhood = (EditText) lLayoutPanel
                .findViewById(R.id.edNeighborhood);
        final EditText edCity = (EditText) lLayoutPanel
                .findViewById(R.id.edCity);
        final EditText edPostal = (EditText) lLayoutPanel
                .findViewById(R.id.edPostal);
        final EditText edCountry = (EditText) lLayoutPanel
                .findViewById(R.id.edCountry);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        edStreet.setTypeface(font2);
        edNeighborhood.setTypeface(font2);
        edCity.setTypeface(font2);
        edPostal.setTypeface(font2);
        edCountry.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        edStreet.setTextSize(NewUtil.gettxtSize());
        edNeighborhood.setTextSize(NewUtil.gettxtSize());
        edCity.setTextSize(NewUtil.gettxtSize());
        edPostal.setTextSize(NewUtil.gettxtSize());
        edCountry.setTextSize(NewUtil.gettxtSize());

        delete.setId(fieldAddress_ID);
        spinnerbtn.setId(fieldAddress_ID);
        btnDelete.setId(fieldAddress_ID);
        edStreet.setId(fieldAddress_ID);
        edNeighborhood.setId(fieldAddress_ID);
        edCity.setId(fieldAddress_ID);
        edPostal.setId(fieldAddress_ID);
        edCountry.setId(fieldAddress_ID);

        btnAddresspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        String[] myResArray = activity.getResources().getStringArray(
                R.array.addressspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnAddresspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnAddresspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());
        } else {
            btnAddresspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnAddresspinner.get(spinnerbtn.getId()).setTag(
                    myResArray.length - 1);
        }

        btnAddresspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnAddresspinner,
                                spinnerbtn, R.array.addressspinner);
                    }
                });

        btnAddresspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnAddresspinner
                                .get(spinnerbtn.getId()).getText().toString();
                        contactDto.addresses.get(spinnerbtn.getId())
                                .setAddressType(txtbtn);
                    }
                });

        String locale = activity.getResources().getConfiguration().locale
                .getDisplayCountry();

        edCountry.setKeyListener(null);
        edCountry.setText(locale);
        edCountry.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.CountryPicker(activity, edCountry);
                        break;
                }
                return true;
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.addresses.remove(btnDelete.getId());
                contactDto.addresses.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        // Editext For Address Text Watcher
        edStreet.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edStreet.getId()).setStreetStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edNeighborhood.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edNeighborhood.getId())
                        .setNeigborhoodStr(String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edCity.getId()).setCityStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edPostal.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.addresses.get(edPostal.getId()).setZipCodeStr(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    btnDelete.setVisibility(View.GONE);
                    delete.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        edCountry.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                contactDto.addresses.get(edStreet.getId()).setCountryStr(
                        String.valueOf(arg0));
            }
        });

        String txtbtn = btnAddresspinner.get(spinnerbtn.getId()).getText()
                .toString();

        holderfield.addView(lLayoutPanel);
        contactDto.addAddresses(addressDto);
        addressDto.setId(fieldAddress_ID);

        addressDto
                .setNeigborhoodStr(edNeighborhood.getText().toString().trim());
        addressDto.setCityStr(edCity.getText().toString().trim());
        addressDto.setZipCodeStr(edPostal.getText().toString().trim());
        addressDto.setCountryStr(edCountry.getText().toString().trim());
        addressDto.setAddressType(txtbtn);

        fieldAddress_ID++;

    }

    public void addBdayFunction(final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.birthday_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        spinnerbtn.setClickable(false);
        spinnerbtn.setText(activity.getResources().getString(
                R.string.string_birthdayType));

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        editTxt.setKeyListener(null);
        editTxt.setGravity(Gravity.CENTER);
        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.DatePicker(activity, editTxt);
                        contactDto.setBirthday(editTxt.getText().toString().trim());
                        btnDelete.setVisibility(View.GONE);
                        delete.setVisibility(View.VISIBLE);
                        break;

                    default:
                        break;
                }
                return false;
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                hasBirthday = false;
                contactDto.setBirthday("");
                lLayoutPanel.removeAllViews();
            }
        });

        editTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i,
                                      int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                contactDto.setBirthday(editTxt.getText().toString().trim());
            }
        });

        hasBirthday = true;
        holderfield.addView(lLayoutPanel);

    }

    public void addDateFunction(final ContactDto contactDto) {

        DateDto date = new DateDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.date_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setId(fieldDate_ID);
        spinnerbtn.setId(fieldDate_ID);
        btnDelete.setId(fieldDate_ID);
        editTxt.setId(fieldDate_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnDatespinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.eventspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnDatespinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnDatespinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnDatespinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnDatespinner.get(spinnerbtn.getId())
                    .setTag(myResArray.length - 1);
        }

        btnDatespinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnDatespinner,
                                spinnerbtn, R.array.eventspinner);
                    }
                });

        btnDatespinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnDatespinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.dateEvent.get(spinnerbtn.getId())
                                .setDatesType(txtbtn);
                    }
                });

        editTxt.setKeyListener(null);
        editTxt.setGravity(Gravity.CENTER);
        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.DatePicker(activity, editTxt);
                        btnDelete.setVisibility(View.GONE);
                        delete.setVisibility(View.VISIBLE);
                        System.out.println("JEFF");
                        break;

                    default:
                        break;
                }
                return false;
            }
        });

        editTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i,
                                      int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                contactDto.dateEvent.get(editTxt.getId()).setDates(
                        String.valueOf(editable));
            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.dateEvent.get(editTxt.getId()).setDates("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.dateEvent.remove(btnDelete.getId());
                contactDto.dateEvent.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnDatespinner.get(spinnerbtn.getId()).getText()
                .toString();

        date.setId(spinnerbtn.getId());
        date.setDates("");
        date.setDatesType(txtbtn);
        contactDto.addDateEvent(date);
        holderfield.addView(lLayoutPanel);
        fieldDate_ID++;

    }

    public void addREFunction(final ContactDto contactDto) {

        RelationDto rela = new RelationDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.related_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_relation, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);
        final Button btn_related = (Button) lLayoutPanel
                .findViewById(R.id.btn_choose);

        delete.setId(fieldRE_ID);
        spinnerbtn.setId(fieldRE_ID);
        btnDelete.setId(fieldRE_ID);
        editTxt.setId(fieldRE_ID);
        btn_related.setId(fieldRE_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnREspinner.add(spinnerbtn);

        btn_related.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AddRelatedActivity.editTxt = editTxt;
                NewUtil.IntetCustomTrans(activity, AddRelatedActivity.class);
            }
        });

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.relationspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnREspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnREspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnREspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnREspinner.get(spinnerbtn.getId()).setTag(myResArray.length - 1);
        }

        btnREspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnREspinner,
                                spinnerbtn, R.array.relationspinner);
                    }
                });

        btnREspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnREspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.relation.get(spinnerbtn.getId())
                                .setRelationType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setHint(activity.getResources().getString(R.string.string_relation_hint));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.relation.get(editTxt.getId()).setRelationName(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.relation.get(editTxt.getId()).setRelationName("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.relation.remove(btnDelete.getId());
                contactDto.relation.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnREspinner.get(spinnerbtn.getId()).getText()
                .toString();

        rela.setId(spinnerbtn.getId());
        rela.setRelationName("");
        rela.setRelationType(txtbtn);
        contactDto.addRelation(rela);

        holderfield.addView(lLayoutPanel);
        fieldRE_ID++;
    }

    public void addIMFunction(final ContactDto contactDto) {

        IMDto im = new IMDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.im_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setId(fieldIM_ID);
        spinnerbtn.setId(fieldIM_ID);
        btnDelete.setId(fieldIM_ID);
        editTxt.setId(fieldIM_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnIMspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.imspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnIMspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnIMspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnIMspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnIMspinner.get(spinnerbtn.getId()).setTag(myResArray.length - 1);
        }

        btnIMspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnIMspinner,
                                spinnerbtn, R.array.imspinner);
                    }
                });

        btnIMspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnIMspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.imField.get(spinnerbtn.getId())
                                .setImProtocol(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setHint(activity.getResources().getString(R.string.adding_im));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.imField.get(editTxt.getId()).setIm(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.imField.get(editTxt.getId()).setIm("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.imField.remove(btnDelete.getId());
                contactDto.imField.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnIMspinner.get(spinnerbtn.getId()).getText()
                .toString();

        im.setId(spinnerbtn.getId());
        im.setIm("");
        im.setImProtocol(txtbtn);
        contactDto.addIM(im);

        holderfield.addView(lLayoutPanel);
        fieldIM_ID++;
    }

    public ContactDto createLoadedNumberField(PhoneNumberDto num, final ContactDto contactDto) {

        /**Usable Modifications*/
        ++AddHelperField.checkerForPhoneNumbersIndex;
        PhoneNumberModel phoneNumberModel = new PhoneNumberModel();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.phone_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setId(fieldPhone_ID);
        spinnerbtn.setId(fieldPhone_ID);
        btnDelete.setId(fieldPhone_ID);
        editTxt.setId(fieldPhone_ID);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnPhonespinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.numberspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        int selecteditem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(num.getNumberType())) {
                custom = false;
                selecteditem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(num.getNumberType())) {
                custom = true;
                btnPhonespinner.get(spinnerbtn.getId()).setText(
                        num.getNumberType());
                btnPhonespinner.get(spinnerbtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnPhonespinner.get(spinnerbtn.getId()).setText(
                    myResArray[selecteditem]);
        }

        btnPhonespinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivityCustomLoader(activity,
                                btnPhonespinner, spinnerbtn,
                                R.array.emailspinner, custom, true);

                    }
                });

        btnPhonespinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnPhonespinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.phoneNumbers.get(spinnerbtn.getId())
                                .setNumberType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setInputType(InputType.TYPE_CLASS_PHONE);
        editTxt.setText(num.getNumber());
        phoneNumberModel.setPhoneNumber(num.getNumber());
        editTxt.setHint(activity.getResources().getString(
                R.string.string_hit_phone));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.phoneNumbers.get(editTxt.getId()).setNumber(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }

                checkerForPhoneNumbers.get(checkerForPhoneNumbersIndex)
                        .setPhoneNumber(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.phoneNumbers.get(editTxt.getId()).setNumber("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDto.phoneNumbers.remove(btnDelete.getId());
                contactDto.phoneNumbers.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();

                /**Usable Modification*/
                checkerForPhoneNumbers.remove(checkerForPhoneNumbersIndex);
                --checkerForPhoneNumbersIndex;
            }
        });

        String txtbtn = btnPhonespinner.get(spinnerbtn.getId()).getText()
                .toString();

        num.setNumber(editTxt.getText().toString());
        num.setNumberType(txtbtn);

        contactDto.addPhoneNumbers(num);
        holderfield.addView(lLayoutPanel);
        fieldPhone_ID++;

        /**Usable Modifications*/
        checkerForPhoneNumbers.add(phoneNumberModel);

        return contactDto;

    }
}
