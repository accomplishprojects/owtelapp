package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.WebsiteDto;

public class EditHelperJMURL {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    int fieldURL_ID = 0;

    ArrayList<Button> btnURLspinner = new ArrayList<Button>();
    boolean custom = false;

    public EditHelperJMURL(EditContactsActivity activity,
                           EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;
        this.fieldURL_ID = editHelp.fieldURL_ID;
        this.btnURLspinner = editHelp.btnURLspinner;
    }

    public ContactDto createLoadedURLField(WebsiteDto web,
                                           final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.url_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        editTxt.setInputType(InputType.TYPE_CLASS_TEXT);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setId(fieldURL_ID);
        spinnerbtn.setId(fieldURL_ID);
        btnDelete.setId(fieldURL_ID);
        editTxt.setId(fieldURL_ID);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnURLspinner.add(spinnerbtn);
        editTxt.setFocusableInTouchMode(false);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        editTxt.setFocusableInTouchMode(true);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.websitespinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        int selecteditem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(web.getWebsiteType())) {
                custom = false;
                selecteditem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(web.getWebsiteType())) {
                custom = true;
                btnURLspinner.get(spinnerbtn.getId()).setText(
                        web.getWebsiteType());
                btnURLspinner.get(spinnerbtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnURLspinner.get(spinnerbtn.getId()).setText(
                    myResArray[selecteditem]);
        }

        btnURLspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivityCustomLoader(activity,
                                btnURLspinner, spinnerbtn,
                                R.array.websitespinner, custom, false);
                    }
                });

        btnURLspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnURLspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.websites.get(spinnerbtn.getId())
                                .setWebsiteType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        editTxt.setFocusableInTouchMode(true);
                        break;
                }
                return false;
            }
        });

        editTxt.setText(web.getWebsite());
        editTxt.setHint(activity.getResources().getString(
                R.string.string_website));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.websites.get(editTxt.getId()).setWebsite(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.websites.get(editTxt.getId()).setWebsite("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.websites.remove(btnDelete.getId());
                contactDto.websites.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnURLspinner.get(spinnerbtn.getId()).getText()
                .toString();

        web.setId(spinnerbtn.getId());
        web.setWebsite(editTxt.getText().toString());
        web.setWebsiteType(txtbtn);
        contactDto.addWebsite(web);

        holderfield.addView(lLayoutPanel);
        fieldURL_ID++;
        return contactDto;
    }

    public void addURLFunction(final ContactDto contactDto) {
        WebsiteDto web = new WebsiteDto();

        final LinearLayout holderfield = (LinearLayout) activity.findViewById(R.id.url_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel.inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel.findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel.findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel.findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel.findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel.findViewById(R.id.txtField);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setId(fieldURL_ID);
        spinnerbtn.setId(fieldURL_ID);
        btnDelete.setId(fieldURL_ID);
        editTxt.setId(fieldURL_ID);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnURLspinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        String[] myResArray = activity.getResources().getStringArray(
                R.array.websitespinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        if (inBounds) {
            btnURLspinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnURLspinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnURLspinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnURLspinner.get(spinnerbtn.getId()).setTag(myResArray.length - 1);
        }

        btnURLspinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnURLspinner,
                                spinnerbtn, R.array.websitespinner);
                    }
                });

        btnURLspinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnURLspinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.websites.get(spinnerbtn.getId())
                                .setWebsiteType(txtbtn);
                    }
                });

        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.VISIBLE);
                        btnDelete.setVisibility(View.GONE);
                        break;
                }
                return false;
            }
        });

        editTxt.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        editTxt.setHint(activity.getResources().getString(
                R.string.string_website));
        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                contactDto.websites.get(editTxt.getId()).setWebsite(
                        String.valueOf(charSequence));
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.websites.get(editTxt.getId()).setWebsite("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.websites.remove(btnDelete.getId());
                contactDto.websites.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnURLspinner.get(spinnerbtn.getId()).getText()
                .toString();

        web.setId(spinnerbtn.getId());
        web.setWebsite(editTxt.getText().toString());
        web.setWebsiteType(txtbtn);
        contactDto.addWebsite(web);

        holderfield.addView(lLayoutPanel);
        fieldURL_ID++;

    }

}
