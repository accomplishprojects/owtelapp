package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.entities.DateDto;

public class EditHelperJMDate {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    int fieldDate_ID = 0;

    ArrayList<Button> btnDatespinner = new ArrayList<Button>();
    boolean custom = false;

    public EditHelperJMDate(EditContactsActivity activity,
                            EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;
        this.fieldDate_ID = editHelp.fieldDate_ID;
        this.btnDatespinner = editHelp.btnDatespinner;
    }

    public ContactDto createLoadedDateField(DateDto date,
                                            final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.date_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setId(fieldDate_ID);
        spinnerbtn.setId(fieldDate_ID);
        btnDelete.setId(fieldDate_ID);
        editTxt.setId(fieldDate_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnDatespinner.add(spinnerbtn);
        editTxt.setFocusableInTouchMode(false);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        editTxt.setFocusableInTouchMode(true);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.eventspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);

        int selecteditem = 0;
        for (int x = 0; x < myResArray.length; x++) {
            if (myResArray[x].equalsIgnoreCase(date.getDatesType())) {
                custom = false;
                selecteditem = x;
                break;
            } else if (!myResArray[x].equalsIgnoreCase(date.getDatesType())) {
                custom = true;
                btnDatespinner.get(spinnerbtn.getId()).setText(
                        date.getDatesType());
                btnDatespinner.get(spinnerbtn.getId()).setTag("Custom");
                break;
            }
        }

        if (inBounds && !custom) {
            btnDatespinner.get(spinnerbtn.getId()).setText(
                    myResArray[selecteditem]);
        } else {
            btnDatespinner.get(spinnerbtn.getId()).setText(date.getDatesType());
        }

        btnDatespinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivityCustomLoader(activity,
                                btnDatespinner, spinnerbtn,
                                R.array.eventspinner, custom, false);
                    }
                });

        btnDatespinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnDatespinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.dateEvent.get(spinnerbtn.getId())
                                .setDatesType(txtbtn);
                    }
                });

        // Get Text
        editTxt.setText(date.getDates());

        editTxt.setKeyListener(null);
        editTxt.setGravity(Gravity.CENTER);
        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.DatePicker(activity, editTxt);
                        btnDelete.setVisibility(View.GONE);
                        delete.setVisibility(View.VISIBLE);
                        System.out.println("JEFF");
                        break;

                    default:
                        break;
                }
                return false;
            }
        });

        editTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i,
                                      int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                contactDto.dateEvent.get(editTxt.getId()).setDates(
                        String.valueOf(editable));
            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.dateEvent.get(editTxt.getId()).setDates("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.dateEvent.remove(btnDelete.getId());
                contactDto.dateEvent.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnDatespinner.get(spinnerbtn.getId()).getText()
                .toString();

        date.setId(spinnerbtn.getId());
        date.setDates(editTxt.getText().toString());
        date.setDatesType(txtbtn);
        contactDto.addDateEvent(date);
        holderfield.addView(lLayoutPanel);
        fieldDate_ID++;

        return contactDto;
    }

    public void addDateFunction(final ContactDto contactDto) {

        DateDto date = new DateDto();

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.date_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        delete.setId(fieldDate_ID);
        spinnerbtn.setId(fieldDate_ID);
        btnDelete.setId(fieldDate_ID);
        editTxt.setId(fieldDate_ID);

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnDatespinner.add(spinnerbtn);

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        final String[] myResArray = activity.getResources().getStringArray(
                R.array.eventspinner);
        boolean inBounds = (spinnerbtn.getId() >= 0)
                && (spinnerbtn.getId() < myResArray.length);
        if (inBounds) {
            btnDatespinner.get(spinnerbtn.getId()).setText(
                    myResArray[spinnerbtn.getId()]);
            btnDatespinner.get(spinnerbtn.getId()).setTag(spinnerbtn.getId());

        } else {
            btnDatespinner.get(spinnerbtn.getId()).setText(
                    myResArray[myResArray.length - 1]);
            btnDatespinner.get(spinnerbtn.getId())
                    .setTag(myResArray.length - 1);
        }

        btnDatespinner.get(spinnerbtn.getId()).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NewUtil.SpinnerActivity(activity, btnDatespinner,
                                spinnerbtn, R.array.eventspinner);
                    }
                });

        btnDatespinner.get(spinnerbtn.getId()).addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String txtbtn = btnDatespinner.get(spinnerbtn.getId())
                                .getText().toString();
                        contactDto.dateEvent.get(spinnerbtn.getId())
                                .setDatesType(txtbtn);
                    }
                });

        editTxt.setKeyListener(null);
        editTxt.setGravity(Gravity.CENTER);
        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.DatePicker(activity, editTxt);
                        btnDelete.setVisibility(View.GONE);
                        delete.setVisibility(View.VISIBLE);
                        System.out.println("JEFF");
                        break;

                    default:
                        break;
                }
                return false;
            }
        });

        editTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i,
                                      int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                contactDto.dateEvent.get(editTxt.getId()).setDates(
                        String.valueOf(editable));
            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editTxt.setText("");
                contactDto.dateEvent.get(editTxt.getId()).setDates("");
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contactDto.dateEvent.remove(btnDelete.getId());
                contactDto.dateEvent.add(btnDelete.getId(), null);
                lLayoutPanel.removeAllViews();
            }
        });

        String txtbtn = btnDatespinner.get(spinnerbtn.getId()).getText()
                .toString();

        date.setId(spinnerbtn.getId());
        date.setDates(editTxt.getText().toString());
        date.setDatesType(txtbtn);
        contactDto.addDateEvent(date);
        holderfield.addView(lLayoutPanel);
        fieldDate_ID++;

    }

}
