package com.katadigital.owtel.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Omar Matthew Reyes on 7/18/16.
 * Retrofit API Request API Object for Area Code List
 */
public class ListAreaCode {
    @SerializedName("area_code_list")
    @Expose
    private List<AreaCode> areaCodeList = new ArrayList<>();

    public List<AreaCode> getAreaCodeList() {
        return areaCodeList;
    }

    public void setAreaCodeList(List<AreaCode> areaCodeList) {
        this.areaCodeList = areaCodeList;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
