package com.katadigital.owtel.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.models.ListAreaCode;
import com.katadigital.owtel.models.collectedobjects.RegistrationResult;

import java.util.List;

/**
 * Created by MIS on 8/3/2016.
 */
public class SharedPreferencesManager {
    public static SharedPreferencesManager mSharedPreferencesManager;
    public static SharedPreferences mSharedPreferences;

    public static void sharedManagerInit(Context context){
        if(mSharedPreferencesManager == null){
            mSharedPreferencesManager = new SharedPreferencesManager();
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    public static SharedPreferencesManager getInstance(){
        if(mSharedPreferencesManager == null){
            throw new IllegalStateException("Call sharedManagerInit inside the onCreate()");
        }
        return mSharedPreferencesManager;
    }

    public void putRegistrationResultToShared(RegistrationResult result){
        mSharedPreferences.edit().putString(Constants.REGISTRATION_RESULT,result.toString()).apply();
    }

    public RegistrationResult getRegistrationResultFromShared(){
        String registrationInfo = mSharedPreferences.getString(Constants.REGISTRATION_RESULT,null);
        if (registrationInfo != null){
            return new Gson().fromJson(registrationInfo,RegistrationResult.class);
        }
        
        return null;
    }

    public void putPromoCodeToShared(String promoCode){
        mSharedPreferences.edit().putString(Constants.PROMO_CODE,promoCode).apply();
    }

    public String getPromoCodeFromShared(){
        return mSharedPreferences.getString(Constants.PROMO_CODE,null);
    }

    public void removeFromShared(String name){
        mSharedPreferences.edit().remove(name).apply();
    }

   public void putListAreaCodeToShared(ListAreaCode areaCode){
       mSharedPreferences.edit().putString(Constants.ADDRESS_LIST,areaCode.toString()).apply();
   }

   public ListAreaCode getListAreaCodeFromShared(){
       String areaList = mSharedPreferences.getString(Constants.ADDRESS_LIST,null);
       if(areaList != null){
           return new Gson().fromJson(areaList,ListAreaCode.class);
       }
       return null;
   }


    public void putAddressListToShared(List<AddressList> addressLists){
        mSharedPreferences.edit().putString(Constants.ADDRESS_LIST,addressLists.toString()).apply();
    }

    public AddressList getAddressListFromShared(){
        String addressList = mSharedPreferences.getString(Constants.ADDRESS_LIST,null);
        if(addressList != null){
            return new Gson().fromJson(addressList, AddressList.class);
        }
        return null;
    }


}
