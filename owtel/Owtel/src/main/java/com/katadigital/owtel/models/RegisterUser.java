package com.katadigital.owtel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Omar Matthew Reyes on 7/18/16.
 * Retrofit API Request API Object for User Registration
 */
public class RegisterUser {
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("webcs_user")
    private boolean isWebCsUser;

    @SerializedName("promo_code")
    private String promoCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isWebCsUser() {
        return isWebCsUser;
    }

    public void setWebCsUser(boolean webCsUser) {
        isWebCsUser = webCsUser;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
}
