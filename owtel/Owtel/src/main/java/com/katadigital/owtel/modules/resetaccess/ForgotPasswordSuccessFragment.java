package com.katadigital.owtel.modules.resetaccess;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kata.phone.R;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity;
import com.katadigital.owtel.modules.login.LoginActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Omar Matthew Reyes on 4/28/16.
 * Fragment containing success message for forgot password request
 */
public class ForgotPasswordSuccessFragment extends Fragment {
    FragmentHolderActivity fragmentHolderActivity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_forgot_password_success, container, false);
        ButterKnife.bind(this, view);
        fragmentHolderActivity = (FragmentHolderActivity) getActivity();
        fragmentHolderActivity.mProgressDialog.dismiss();
        return view;
    }

    @OnClick(R.id.btn_continue)
    void onContinueClicked(){
        fragmentHolderActivity.startActivity(new Intent(fragmentHolderActivity, LoginActivity.class));
    }
}
