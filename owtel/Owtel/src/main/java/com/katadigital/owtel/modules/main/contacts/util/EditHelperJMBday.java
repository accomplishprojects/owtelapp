package com.katadigital.owtel.modules.main.contacts.util;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.EditContactsActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;

public class EditHelperJMBday {

    EditContactsActivity activity;
    EditHelperField editHelp;

    Typeface font1;
    Typeface font2;

    public EditHelperJMBday(EditContactsActivity activity,
                            EditHelperField editHelp) {
        this.activity = activity;
        this.editHelp = editHelp;
        this.font1 = editHelp.font1;
        this.font2 = editHelp.font2;

    }

    public ContactDto createLoadedBirthdayField(String birthday,
                                                final ContactDto contactDto) {
        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.birthday_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        spinnerbtn.setClickable(false);
        spinnerbtn.setText(activity.getResources().getString(
                R.string.string_birthdayType));

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        editTxt.setText(birthday.trim());
        editTxt.setKeyListener(null);
        editTxt.setGravity(Gravity.CENTER);
        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.DatePicker(activity, editTxt);
                        contactDto.setBirthday(editTxt.getText().toString().trim());
                        break;

                    default:
                        break;
                }
                return false;
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                EditHelperField.hasBirthday = false;
                contactDto.setBirthday("");
                lLayoutPanel.removeAllViews();
            }
        });

        editTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i,
                                      int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                contactDto.setBirthday(editTxt.getText().toString().trim());
            }
        });

        EditHelperField.hasBirthday = true;
        holderfield.addView(lLayoutPanel);
        return contactDto;
    }

    public void addBdayFunction(final ContactDto contactDto) {

        final LinearLayout holderfield = (LinearLayout) activity
                .findViewById(R.id.birthday_holderfield);
        final LayoutInflater lyInflaterForPanel = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                .inflate(R.layout.txtfield_custom, null);

        // layout
        final Button delete = (Button) lLayoutPanel
                .findViewById(R.id.delete_Btn);
        final Button spinnerbtn = (Button) lLayoutPanel
                .findViewById(R.id.spinnerBtn);
        final Button clearBtn = (Button) lLayoutPanel
                .findViewById(R.id.btnclear_text);
        final Button btnDelete = (Button) lLayoutPanel
                .findViewById(R.id.btn_delete);
        final EditText editTxt = (EditText) lLayoutPanel
                .findViewById(R.id.txtField);

        spinnerbtn.setClickable(false);
        spinnerbtn.setText(activity.getResources().getString(
                R.string.string_birthdayType));

        delete.setTypeface(font2);
        spinnerbtn.setTypeface(font2);
        btnDelete.setTypeface(font2);
        editTxt.setTypeface(font2);

        delete.setTextSize(NewUtil.gettxtSize());
        spinnerbtn.setTextSize(NewUtil.gettxtSize());
        btnDelete.setTextSize(NewUtil.gettxtSize());
        editTxt.setTextSize(NewUtil.gettxtSize());

        btnDelete.setVisibility(View.GONE);
        delete.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        delete.setVisibility(View.GONE);
                        btnDelete.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });

        editTxt.setKeyListener(null);
        editTxt.setGravity(Gravity.CENTER);
        editTxt.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        NewUtil.DatePicker(activity, editTxt);
                        contactDto.setBirthday(editTxt.getText().toString().trim());
                        btnDelete.setVisibility(View.GONE);
                        delete.setVisibility(View.VISIBLE);
                        break;

                    default:
                        break;
                }
                return false;
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                EditHelperField.hasBirthday = false;
                contactDto.setBirthday("");
                lLayoutPanel.removeAllViews();
            }
        });

        editTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {
            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i,
                                      int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                contactDto.setBirthday(editTxt.getText().toString().trim());
            }
        });

        EditHelperField.hasBirthday = true;
        holderfield.addView(lLayoutPanel);

    }

}
