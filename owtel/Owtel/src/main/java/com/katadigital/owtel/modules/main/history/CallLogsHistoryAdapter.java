package com.katadigital.owtel.modules.main.history;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kata.phone.R;
import com.katadigital.owtel.helpers.AlertDialogHelper;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.contacts.entities.CallLogDto;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.util.DateTimeHelper;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class CallLogsHistoryAdapter extends RecyclerView.Adapter<CallLogsHistoryAdapter.ViewHolder> {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * Variables
     */
    private Activity context;
    boolean editIsOn = false;
    QuickContactHelper quickContactHelper;
    private ProgressDialog progressDialog;
    SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yy", Locale.getDefault());
    SimpleDateFormat sdfDayOfWeek = new SimpleDateFormat("EEEE", Locale.getDefault());
    SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a", Locale.getDefault());

    // callLogsList - filtered raw call logs list
    // callLogsListRaw - raw call logs list
    List<CallLogDto> callLogsList, callLogListRaw;
    Fragment logsFragment;
    AlertDialogHelper alertDialog;
    Calendar c = Calendar.getInstance();
    ArrayList<HashMap<String, String>> callLogListSelPass;

    int callCounts = 0;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView logInfoBtn, editBtn;
        private View listItem;
        private Button deleteBtn;
        private TextView textViewContactName, dateTextView, numberType;
        private ImageView callStatusIv;

        public ViewHolder(View view) {
            super(view);

            /**Setting up views*/
            deleteBtn = (Button) view.findViewById(R.id.logs_item_delete_btn);
            textViewContactName = (TextView) view.findViewById(R.id.logs_item_text_view);
            dateTextView = (TextView) view.findViewById(R.id.logs_item_date);
            numberType = (TextView) view.findViewById(R.id.call_log_numbertype);
            callStatusIv = (ImageView) view.findViewById(R.id.logs_item_imageview);
            logInfoBtn = (ImageView) view.findViewById(R.id.btn_logs_item_info);
            editBtn = (ImageView) view.findViewById(R.id.btn_edit_log);
            listItem = view.findViewById(R.id.logs_list_item);

            textViewContactName.setTypeface(NewUtil.getFontRoman(context));
            dateTextView.setTypeface(NewUtil.getFontRoman(context));
            numberType.setTypeface(NewUtil.getFontRoman(context));

            textViewContactName.setTextSize(NewUtil.gettxtSize());
            numberType.setTextSize(NewUtil.gettxtSize());
            dateTextView.setTextSize(NewUtil.gettxtSize());

            dateTextView.setTextColor(context.getResources().getColor(R.color.gray_notes));

            /**Display default icon for delete button*/
            Glide.with(context)
                    .load("")
                    .dontAnimate()
                    .placeholder(R.drawable.delete_item_img)
                    .override(20, 20)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(editBtn);

            /**Display default icon for information button*/
            Glide.with(context)
                    .load("")
                    .dontAnimate()
                    .placeholder(R.drawable.information)
                    .override(10, 10)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(logInfoBtn);

            textViewContactName.setTextColor(Color.BLACK);
            callStatusIv.setVisibility(View.VISIBLE);
        }
    }

    public CallLogsHistoryAdapter(Activity context, List<CallLogDto> callLogsList,
                                  List<CallLogDto> callLogsListRaw,
                                  Fragment logsFragment, boolean edit) {
        this.context = context;
        this.callLogsList = callLogsList;
        this.callLogListRaw = callLogsListRaw;
        quickContactHelper = new QuickContactHelper(context);
        this.logsFragment = logsFragment;
        editIsOn = edit;
        alertDialog = new AlertDialogHelper();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View views = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.logs_list_item, parent, false);
        return new ViewHolder(views);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        final CallLogDto callLogDto = callLogsList.get(i);
//        Log.e(TAG, "onBindViewHolder: -1: " + callLogDto.getCallType());
//        Log.e(TAG, "onBindViewHolder: -2: " + callLogDto.getNumber());

        String numtype = QuickContactHelper.getContactNumType(callLogDto.getNumber());

        if (editIsOn) {
            holder.logInfoBtn.setVisibility(View.GONE);
            holder.deleteBtn.setVisibility(View.GONE);
            holder.editBtn.setVisibility(View.VISIBLE);
        } else {
            holder.editBtn.setVisibility(View.GONE);
        }

        /**Assigning Number Type*/
        if (callLogDto != null) {
            if (!numtype.trim().isEmpty()) {
                int num = Integer.parseInt(numtype);
                if (num != 0) {
                    String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());
//                    Log.e(TAG, "onBindViewHolder: 1: " + customLabel);
                    try {
                        String name = (String) ContactsContract.CommonDataKinds.Phone.getTypeLabel(context.getResources(), num, customLabel);
                        holder.numberType.setText(name);
//                        Log.e(TAG, "onBindViewHolder: 2: " + name);

                    } catch (NullPointerException e) {
                        Log.e(TAG, "get Number Type " + e);
                    }
                } else {
//                    String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());
//                    String name = (String) Phone.getTypeLabel(Var.activity.getResources(), 0, customLabel);
//                    holder.numberType.setText(name);
                    try {
                        String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());
                        String name = (String) ContactsContract.CommonDataKinds.Phone.getTypeLabel(Var.activity.getResources(), 0, customLabel);
                        holder.numberType.setText(name);
//                        Log.e(TAG, "onBindViewHolder: 3: " + customLabel);
//                        Log.e(TAG, "onBindViewHolder: 4: " + name);
                    } catch (Exception e) {
                        holder.numberType.setText("");
                        Log.e(TAG, "String customLabel = QuickContactHelper.getContactNumType2(callLogDto.getNumber());\n" +
                                "                        String name = (String) Phone.getTypeLabel(Var.activity.getResources(), 0, customLabel);\n" +
                                "                        holder.numberType.setText(name);");
                    }
                }
            } else {
                Log.e(TAG, "numType is EMPTY");
            }

            if (callLogsList.get(i).getNumber() != null) {
                String contactName = QuickContactHelper.getContactName(callLogDto.getNumber());
//                Log.e(TAG, "onBindViewHolder: 5: " + contactName);

                if (callLogDto.getCount() <= 1) {
                    if (contactName.equals("") || contactName.equals(null)) {
                        holder.textViewContactName.setText(callLogDto.getNumber());
                    } else {
                        holder.textViewContactName.setText(contactName);
                    }
//                    Log.e(TAG, "onBindViewHolder: " + holder.textViewContactName.getText().toString());
                } else {
                    if (contactName.equals("") || contactName.equals(null)) {
                        String contactNameTemp = callLogDto.getNumber() + " (" + callLogDto.getCount() + ")";
                        holder.textViewContactName.setText(contactNameTemp);
                    } else {
                        String contactNameTemp = contactName + " (" + callLogDto.getCount() + ")";
                        holder.textViewContactName.setText(contactNameTemp);
                    }
//                    Log.e(TAG, "onBindViewHolder: " + holder.textViewContactName.getText().toString());
                }

                String formattedDate = sdf.format(callLogDto.getCallDate());
                String formattedTime = sdfTime.format(callLogDto.getCallDate());
                String curDate = sdf.format(c.getTime());

                if (curDate.equals(formattedDate)) {
                    holder.dateTextView.setText(formattedTime);
                } else {
                    holder.dateTextView.setText(YesterdayOrToday(callLogDto.getCallDate().getTime()));
                }

                switch (callLogDto.getCallType()) {
                    case "INCOMING":
                        Glide.with(context)
                                .load("")
                                .dontAnimate()
                                .placeholder(R.drawable.ic_incoming_call)
                                .override(5, 5)
                                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                .into(holder.callStatusIv);
                        break;

                    case "OUTGOING":
                        Glide.with(context)
                                .load("")
                                .dontAnimate()
                                .placeholder(R.drawable.ic_outgoing_call)
                                .override(5, 5)
                                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                .into(holder.callStatusIv);
                        break;

                    case "MISSED":
                        Glide.with(context)
                                .load("")
                                .dontAnimate()
                                .placeholder(R.drawable.ic_missed_call)
                                .override(5, 5)
                                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                .into(holder.callStatusIv);
                        holder.textViewContactName.setTextColor(Color.RED);
                        break;
                }

            } else {
                holder.textViewContactName.setText(" ");
            }

            /**Initiating Call*/
            holder.listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    QuickContactHelper.initiateSipCall(context, callLogDto.getNumber().replaceAll("#", "%23"));
                    Log.e(TAG, "onClick: ");
                }
            });

            /**Initiating Info Button*/
            holder.logInfoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new CallLogsHistoryAdapter.loadContactDetails(callLogDto, callLogsList.get(i).getNumber(), callLogsList.get(i).getCallDate())
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });


            /**Initiate Edit Button*/
            holder.editBtn.setOnClickListener(view1 -> {
                holder.deleteBtn.setVisibility(View.VISIBLE);
                holder.editBtn.setVisibility(View.GONE);
            });

            /**Initiate Delete Button*/
            holder.deleteBtn.setOnClickListener(v -> {
                String formattedDate = sdf.format(callLogDto.getCallDate());
                for (CallLogDto callLog : callLogsList) {
                    String callLogFormattedDate = sdf.format(callLog.getCallDate());
                    if (holder.numberType.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.string_unknown))) {
                        if (callLogDto.getNumber().equals(callLog.getNumber())
                                && formattedDate.equals(callLogFormattedDate)
                                && !callLogDto.getCallType().equals("MISSED")) {
                            context.getContentResolver().delete(android.provider.CallLog.Calls.CONTENT_URI, "_ID = " + callLog.getCallLogID(), null);
                        } else {
                            context.getContentResolver().delete(android.provider.CallLog.Calls.CONTENT_URI, "_ID = " + callLogDto.getCallLogID(), null);
                        }
                    } else {
                        if (callLogDto.getNumber().equals(callLog.getNumber())
                                && callLogDto.getContactName().equals(callLog.getContactName())
                                && formattedDate.equals(callLogFormattedDate)
                                && !callLogDto.getCallType().equals("MISSED")) {
                            context.getContentResolver()
                                    .delete(android.provider.CallLog.Calls.CONTENT_URI,
                                            "_ID = " + callLog.getCallLogID(), null);
                        } else {
                            context.getContentResolver()
                                    .delete(android.provider.CallLog.Calls.CONTENT_URI,
                                            "_ID = " + callLogDto.getCallLogID(), null);
                        }
                    }
                }

                callLogsList.remove(i);
                this.notifyDataSetChanged();

                if (callLogsList.size() == 0) {
                    ((LogsFragment) logsFragment).refreshLogs();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return callLogsList.size();
    }

    private String YesterdayOrToday(long timeInMilliseconds) {
        String yot;
        if (DateUtils.isToday(timeInMilliseconds)) {
            yot = "Today";
        } else if (DateUtils.isToday(timeInMilliseconds + 86400000)) {
            yot = "Yesterday";
        } else {
            Date cdate = new Date();
            Long diff = cdate.getTime() - timeInMilliseconds;
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeInMilliseconds);
            if (days < 6) {
                yot = sdfDayOfWeek.format(calendar.getTime());
            } else {
                yot = sdf.format(calendar.getTime());
            }
        }
        return yot;
    }


    public class loadContactDetails extends AsyncTask<String, String, String> {
        private CallLogDto callLogDto;
        private String contactId = "";

        private String contactNumber;
        private Date contactDate;

        public loadContactDetails(CallLogDto callLogDto, String contactNumber, Date contactDate) {
            this.callLogDto = callLogDto;
            this.contactNumber = contactNumber;
            this.contactDate = contactDate;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(context,
                    context.getResources().getString(R.string.string_diag_pleasewait),
                    context.getResources().getString(
                            R.string.string_diag_loading_details) + "...");
            progressDialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            ContactDto contactDto;
            contactId = quickContactHelper.getContactInformation(callLogDto.getNumber());
            contactDto = quickContactHelper.getContactDetails(contactId);
            ContactDetailActivity.contactDto = contactDto;

            // Log callLogListRaw contents
            callLogListSelPass = new ArrayList<>();
            for (CallLogDto callLogDetails : callLogListRaw) {

                Log.e(TAG, "doInBackground: name " + callLogDetails.getContactName());
                Log.e(TAG, "doInBackground: number " + callLogDetails.getNumber());
                Log.e(TAG, "doInBackground: calltype " + callLogDetails.getCallType());
                Log.e(TAG, "doInBackground: numbertype " + callLogDetails.getNumberType());
                Log.e(TAG, "doInBackground: date " + callLogDetails.getCallDate());
                Log.e(TAG, "doInBackground: count " + callLogDetails.getCount());

                HashMap<String, String> hashMapContactLog = new HashMap<>();
                if (callLogDetails.getNumber().equals(callLogDto.getNumber()) &&
                        DateTimeHelper.isSameDay(callLogDetails.getCallDate(), contactDate) && LogsFragment.allCallsIsActive) {

                    if (callLogDetails.getNumber().equals(callLogDto.getNumber()) &&
                            DateTimeHelper.isSameDay(callLogDetails.getCallDate(), contactDate)) {

                        if (GlobalValues.DEBUG) {
                            Log.e(TAG, "NUMBER " + callLogDetails.getNumber());
                            Log.e(TAG, "DATE " + sdf.format(callLogDetails.getCallDate()));
                            Log.e(TAG, "TIME " + sdfTime.format(callLogDetails.getCallDate()));
                            Log.e(TAG, "CALL TYPE " + callLogDetails.getCallType());
                            Log.e(TAG, "DURATION " + callLogDetails.getDuration());
                        }

                        hashMapContactLog.put("time", sdfTime.format(callLogDetails.getCallDate()));
                        hashMapContactLog.put("calltype", callLogDetails.getCallType());
                        hashMapContactLog.put("duration", callLogDetails.getDuration());
                        callLogListSelPass.add(hashMapContactLog);
                    }

                } else {
                    if (callLogDetails.getNumber().equals(callLogDto.getNumber()) &&
                            DateTimeHelper.isSameDay(callLogDetails.getCallDate(), contactDate) &&
                            !(callLogDetails.getCallType().equals("MISSED") ^ callLogDto.getCallType().equals("MISSED"))) {

                        if (GlobalValues.DEBUG) {
                            Log.e(TAG, "NUMBER " + callLogDetails.getNumber());
                            Log.e(TAG, "DATE " + sdf.format(callLogDetails.getCallDate()));
                            Log.e(TAG, "TIME " + sdfTime.format(callLogDetails.getCallDate()));
                            Log.e(TAG, "CALL TYPE " + callLogDetails.getCallType());
                            Log.e(TAG, "DURATION " + callLogDetails.getDuration());
                        }

                        hashMapContactLog.put("time", sdfTime.format(callLogDetails.getCallDate()));
                        hashMapContactLog.put("calltype", callLogDetails.getCallType());
                        hashMapContactLog.put("duration", callLogDetails.getDuration());
                        callLogListSelPass.add(hashMapContactLog);
                    }
                }
            }

//            if (!contactId.isEmpty()) {
//                contactDto = quickContactHelper.getContactDetails(contactId);
//                ContactDetailActivity.contactDto = contactDto;
//
//                callLogListSelPass = new ArrayList<>();
//                String formattedDate = sdf.format(callLogDto.getCallDate());
//                String contactId = quickContactHelper.getContactInformation(callLogDto.getNumber());
//
//                for (CallLogDto callLog : callLogListRaw) {
//                    HashMap<String, String> map = new HashMap<>();
//                    String callLogFormattedDate = sdf.format(callLog.getCallDate());
//                    String contactId_callLog = quickContactHelper.getContactInformation(callLog.getNumber());
//                    String callLogType = callLog.getCallType();
//                    if (callLogDto.getCallType().equals("MISSED")) {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && callLogDto.getContactName().equals(callLog.getContactName())
//                                && callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                            System.out.println(formattedTime);
//                        }
//                    } else {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && contactId.equals(contactId_callLog)
//                                && !callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                            System.out.println(formattedTime);
//                        }
//                    }
//                }
//            } else {
//                callLogListSelPass = new ArrayList<>();
//                System.out.println(callLogListRaw.size());
//                String formattedDate = sdf.format(callLogDto.getCallDate());
//
//                for (CallLogDto callLog : callLogListRaw) {
//                    HashMap<String, String> map = new HashMap<>();
//                    String callLogFormattedDate = sdf.format(callLog.getCallDate());
//                    String callLogType = callLog.getCallType();
//                    if (callLogDto.getCallType().equals("MISSED")) {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && callLog.getNumberType().equalsIgnoreCase(context.getResources().getString(R.string.string_unknown))
//                                && callLog.getNumber().equals(callLogDto.getNumber())
//                                && callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                        }
//                    } else {
//                        if (formattedDate.equals(callLogFormattedDate)
//                                && callLog.getNumber().equals(callLogDto.getNumber())
//                                && !callLog.getCallType().equals("MISSED")) {
//                            String formattedTime = sdfTime.format(callLog.getCallDate());
//                            map.put("time", formattedTime);
//                            map.put("calltype", callLog.getCallType());
//                            map.put("duration", callLog.getDuration());
//                            callLogListSelPass.add(map);
//                        }
//                    }
//                }
//            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (!contactId.isEmpty()) {
                Intent intent = new Intent(context, ContactDetailActivity.class);
                intent.putExtra("selectednumber", callLogDto.getNumber());
                intent.putExtra("transactiondate", sdf.format(callLogDto.getCallDate()));
                intent.putExtra("logs", callLogListSelPass);
                intent.putExtra("recents", context.getResources().getString(R.string.tab_log));
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_up_exit);
            } else {
                Intent intent = new Intent(context, LogsDetail.class);
                if (!callLogDto.getNumber().equals("")) {
                    intent.putExtra("logs_number", callLogDto.getNumber());
                }

                if (callLogDto.getCallDate() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()); // Set
                    String formattedDate = sdf.format(callLogDto.getCallDate());
                    intent.putExtra("logs_date", formattedDate);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    String formattedTime = sdf2.format(callLogDto.getCallDate());
                    intent.putExtra("logs_time", formattedTime);
                }

                intent.putExtra("transactiondate", sdf.format(callLogDto.getCallDate()));
                intent.putExtra("logs", callLogListSelPass);
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_up_exit);
            }

            super.onPostExecute(result);
        }
    }

}
