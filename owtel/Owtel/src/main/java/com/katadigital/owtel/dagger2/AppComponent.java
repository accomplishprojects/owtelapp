package com.katadigital.owtel.dagger2;

import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.modules.main.messaging.service.WebSocketService;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity;
import com.katadigital.owtel.modules.login.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkingModule.class,StorageModule.class, ApplicationModule.class})
public interface AppComponent {
    void inject(LoginActivity loginActivity);
    void inject(FragmentHolderActivity fragmentHolderActivity);
    void inject(WebSocketService webSocketService);
    void inject(ChatActivity chatActivity);
    void inject(MainActivity mainActivity);
}
