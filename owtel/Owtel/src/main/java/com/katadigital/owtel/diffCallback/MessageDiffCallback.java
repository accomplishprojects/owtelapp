//package com.katadigital.owtel.diffCallback;
//
//import android.support.annotation.Nullable;
//import android.support.v7.util.DiffUtil;
//
//import com.katadigital.owtel.daoDb.Message;
//
//import java.util.List;
//
///**
// * Created by dcnc123 on 1/12/17.
// */
//
//public class MessageDiffCallback extends DiffUtil.Callback {
//
//
//    private final List<Message> oldList;
//    private final List<Message> newList;
//
//
//    public MessageDiffCallback(List<Message> oldList, List<Message> newList) {
//        this.oldList = oldList;
//        this.newList = newList;
//    }
//
//
//    @Override
//    public int getOldListSize() {
//        return oldList.size();
//    }
//
//    @Override
//    public int getNewListSize() {
//        return newList.size();
//    }
//
//    @Override
//    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
//        return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
//    }
//
//    @Override
//    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
//        final Message oldItem = oldList.get(oldItemPosition);
//        final Message newItem = newList.get(newItemPosition);
//
//        return oldItem.getDisplay_name().equals(newItem.getDisplay_name());    }
//
//    @Nullable
//    @Override
//    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
//        // Implement method if you're going to use ItemAnimator
//        return super.getChangePayload(oldItemPosition, newItemPosition);
//    }
//
//
//}
