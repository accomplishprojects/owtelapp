package com.katadigital.owtel.utils;

import android.content.SharedPreferences;

/**
 * Created by Omar Matthew Reyes on 5/25/16.
 */
public class SharedPreferenceHelper {

    /**
     * Save double in SharedPreferences
     * Convert double to bits and store it as long
     * @param edit SharedPreferences.Editor
     * @param key String
     * @param value double
     * @return SharedPreferences.Editor
     */
    public static SharedPreferences.Editor putDouble(final SharedPreferences.Editor edit, final String key, final double value) {
        return edit.putLong(key, Double.doubleToRawLongBits(value));
    }

    /**
     * Get double from Preferences
     * @param prefs SharedPreferences
     * @param key String
     * @param defaultValue double
     * @return double
     */
    public static double getDouble(final SharedPreferences prefs, final String key, final double defaultValue) {
        if (!prefs.contains(key)) return defaultValue;
        return Double.longBitsToDouble(prefs.getLong(key, 0));
//        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(defaultValue)));
    }
}
