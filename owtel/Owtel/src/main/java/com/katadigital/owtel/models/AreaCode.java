package com.katadigital.owtel.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Omar Matthew Reyes on 7/18/16.
 * Retrofit API Request API Object for Area Code List
 * AreaCode List Object
 */
public class AreaCode {
    @SerializedName("area_code")
    private String areaCode;

    @SerializedName("state_name")
    private String state;

    @SerializedName("city_name")
    private String city;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
