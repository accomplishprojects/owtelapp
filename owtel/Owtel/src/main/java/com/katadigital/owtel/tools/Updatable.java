package com.katadigital.owtel.tools;

/**
 * Created by dcnc123 on 11/16/16.
 */

public interface Updatable {

    public void update();

}
