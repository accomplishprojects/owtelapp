package com.katadigital.owtel.modules.main.contacts.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Omar Matthew Reyes on 11/3/15.
 * <p/>
 * Utils for anything related to date and time.
 */
public class DateTimeHelper {
    /**
     * Check if both date params is within same day.
     *
     * @param date1 Date
     * @param date2 Date
     * @return boolean
     */
    public static boolean isSameDay(Date date1, Date date2) {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date1).equals(fmt.format(date2));
//        Calendar calendar1, calendar2;
//        calendar1 = Calendar.getInstance();
//        calendar2 = Calendar.getInstance();
//        calendar1.setTime(date1);
//        calendar2.setTime(date2);
//
//        return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
//                calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);
    }
}
