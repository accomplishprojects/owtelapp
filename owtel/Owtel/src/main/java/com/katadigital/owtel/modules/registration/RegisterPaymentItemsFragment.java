package com.katadigital.owtel.modules.registration;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.security.PaymentHelper;
import com.katadigital.owtel.security.SecuredString;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.ui.adapter.IddAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Omar Matthew Reyes on 7/13/16.
 * Payment Page v2
 */
public class RegisterPaymentItemsFragment extends Fragment {
    private static final String TAG = "PaymentItemsFragment";

    @Bind(R.id.tv_register_payment_subscription_type)
    TextView textViewSubscriptionType;
    @Bind(R.id.tv_register_payment_subscription_amount)
    TextView textViewSubscriptionPrice;
    @Bind(R.id.spinner_register_payment_idd_amount)
    Spinner spinnerIDDPrices;
    @Bind(R.id.tv_register_payment_total_amount)
    TextView textViewPaymentTotal;

    FragmentHolderActivity fragmentHolderActivity;
    private Bundle bundlePrev;
    private boolean isPromoUser = false;
    private List<SubscriptionList> listSubscription = new ArrayList<>();
    private List<IddList> listIdd = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_register_payment_items, container, false);
        ButterKnife.bind(this, view);

        fragmentHolderActivity = (FragmentHolderActivity) getActivity();
        fragmentHolderActivity.mProgressDialog.dismiss();

        bundlePrev = getArguments();
        isPromoUser = getArguments().getString(Constants.BUNDLE_REGISTER_PROMO_CODE, "").trim().length() > 0;

        if (listSubscription.size() > 0 && listIdd.size() > 0)
            textViewPaymentTotal.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                    (listSubscription.get(0).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue())));

        if (GlobalValues.DEBUG) {
            Log.i(TAG, "BUNDLE RP Email: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL, "empty_email"));
            Log.i(TAG, "BUNDLE RP Password: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD, "empty_pass"));
            Log.i(TAG, "BUNDLE RP Area Code: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "empty_area_code"));
            Log.i(TAG, "BUNDLE RP Number: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_NUMBER, "empty_us_number"));
            Log.i(TAG, "BUNDLE RP Promo Code: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_PROMO_CODE, "empty_promo_code"));
            Log.i(TAG, "BUNDLE RP Promo Code User: " + isPromoUser);
            Log.i(TAG, "BUNDLE RP Lock ID: " + bundlePrev.getString(Constants.BUNDLE_REGISTER_LOCK_ID, "empty_lock_id"));
        }

        return view;
    }

    @OnItemSelected(R.id.spinner_register_payment_idd_amount)
    public void onItemSelectIdd(int position) {
        Log.i(TAG, "Spinner IDD position " + position);
        Log.i(TAG, "Spinner IDD id: " + listIdd.get(position).getIddId() +
                " price " + listIdd.get(position).getIddReloadPrice() +
                " value " + listIdd.get(position).getIddReloadValue());
        if (listSubscription.size() > 0 && listIdd.size() > 0)
            textViewPaymentTotal.setText(StringFormatter.formatCurrency(Constants.CURRENCY,
                    (listSubscription.get(0).getSubscriptionValue() + listIdd.get(position).getIddReloadValue())));
    }

    @OnClick(R.id.btn_register_payment_continue)
    public void onClickContinue() {
        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_REGISTER_EMAIL, bundlePrev.getString(Constants.BUNDLE_REGISTER_EMAIL, "empty"));
        args.putString(Constants.BUNDLE_REGISTER_PASSWORD, bundlePrev.getString(Constants.BUNDLE_REGISTER_PASSWORD, "empty"));
        args.putString(Constants.BUNDLE_REGISTER_AREA_CODE, bundlePrev.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "empty"));
        args.putString(Constants.BUNDLE_REGISTER_NUMBER, bundlePrev.getString(Constants.BUNDLE_REGISTER_NUMBER, "empty"));
        args.putBoolean(Constants.BUNDLE_REGISTER_PROMO_CODE_USER, isPromoUser);

        if (isPromoUser && spinnerIDDPrices.getSelectedItemPosition() == 0) {
            // Proceed to register if user uses a promo code and 0.00 IDD credits to buy
            doRegistration(getArguments());
        } else {
            // TODO proceed checkout
            fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
            fragmentHolderActivity.mProgressDialog.show();
            ApiHelper.doGetAddressList(fragmentHolderActivity.mProgressDialog, fragmentHolderActivity, GlobalValues.GET_COUNTRY, "", "")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<AddressList>>() {
                        @Override
                        public void onCompleted() {
                            fragmentHolderActivity.mProgressDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(List<AddressList> countryList) {
                            ((SubscriptionIddBridge)getActivity())
                                    .setSubscriptionIdd(listSubscription.get(0),
                                            listIdd.get(spinnerIDDPrices.getSelectedItemPosition()),
                                            args, countryList);
                        }
                    });
        }
    }

    /**
     * Set Subscription
     * @param listSubscription List<SubscriptionList>
     */
    public void setSubscription(List<SubscriptionList> listSubscription){
        this.listSubscription = listSubscription;
        textViewSubscriptionType.setText(listSubscription.get(0).getSubscriptionName());
        textViewSubscriptionPrice.setText(listSubscription.get(0).getSubscriptionPrice());
    }

    /**
     * Set IDD
     * @param listIdd List<IddList>
     */
    public void setIdd(List<IddList> listIdd){
        this.listIdd = listIdd;
        spinnerIDDPrices.setAdapter(new IddAdapter(listIdd));
    }

    /**
     * Do user registration
     *
     * @param bundle Bundle
     */
    private void doRegistration(Bundle bundle) {
        fragmentHolderActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
        fragmentHolderActivity.mProgressDialog.show();
        StringRequest mStringRequestRegisterUser = new StringRequest(Request.Method.POST, API.URL_TEST + API.PROCESS_USER,
                response -> {
                    if(GlobalValues.JC_debug)
                    {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Delete entry")
                                .setMessage("process_user.php log no.2 "+response)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();                    }
                    Log.i(TAG, "Register User response 1" + response);
                    try {
                        JSONObject jsonObjectMain = new JSONObject(response);
                        String jsonObjectStatus = jsonObjectMain.getString("status");
                        if (jsonObjectStatus.equals("Success")) {
                            if(!isPromoUser){
                                // Premium User
                                Bundle args = new Bundle();
                                args.putString(Constants.BUNDLE_REGISTER_EMAIL, bundle.getString(Constants.BUNDLE_REGISTER_EMAIL, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_PASSWORD, bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_AREA_CODE, bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "empty"));
                                args.putString(Constants.BUNDLE_REGISTER_NUMBER, bundle.getString(Constants.BUNDLE_REGISTER_NUMBER, "empty"));
                                RegisterEndFragment registerEndFragment = new RegisterEndFragment();
                                registerEndFragment.setArguments(args);
                                fragmentHolderActivity.changeFragment(registerEndFragment, false);
                            } else {
                                // Promo Code User
                                new ApiHelper(getActivity()).doLoginRequest(fragmentHolderActivity,
                                        bundle.getString(Constants.BUNDLE_REGISTER_EMAIL), bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD));
                            }
                        } else {
                            fragmentHolderActivity.mProgressDialog.dismiss();
                            String jsonObjectMessage = jsonObjectMain.getString("message");
                            fragmentHolderActivity.showAlertDialog(getActivity(),
                                    jsonObjectStatus, jsonObjectMessage,
                                    getResources().getString(R.string.string_ok));
                        }
                    } catch (Exception e) {
                        if (GlobalValues.DEBUG) Log.e(TAG, "Volley " + e);
                        fragmentHolderActivity.mProgressDialog.dismiss();
                        Toast.makeText(fragmentHolderActivity, getString(R.string.request_failed), Toast.LENGTH_LONG).show();
                    }
                }, error -> {
            if (GlobalValues.DEBUG) Log.e(TAG, "That didn't work! Error: " + error);
            fragmentHolderActivity.mProgressDialog.dismiss();
            DialogBuilder.displayVolleyError(getActivity(), error);
        }) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                String jsonObject = new PaymentHelper(OwtelAppController.getInstance().getApplicationContext())
                        .getSecuredJsonRegisterUser(bundle.getString(Constants.BUNDLE_REGISTER_EMAIL, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_PASSWORD, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_AREA_CODE, "") + "-" + bundle.getString(Constants.BUNDLE_REGISTER_NUMBER, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_LOCK_ID, ""),
                                bundle.getString(Constants.BUNDLE_REGISTER_PROMO_CODE, ""),
                                listSubscription.get(0).getSubscriptionId(),
                                listSubscription.get(0).getSubscriptionValue(),
                                listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddId(),
                                listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue(),
                                (listSubscription.get(0).getSubscriptionValue() + listIdd.get(spinnerIDDPrices.getSelectedItemPosition()).getIddReloadValue()),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "");
                params.put(API.ENCRYPTED_OBJECT, jsonObject);
                if(GlobalValues.DEBUG) {
                    Log.i(TAG, API.ENCRYPTED_OBJECT + " encrypted " + jsonObject);
                    Log.i(TAG, API.ENCRYPTED_OBJECT + " decrypted " + SecuredString.decryptData(jsonObject));
                }
                return params;
            }
        };
        mStringRequestRegisterUser.setTag(Constants.REQUEST_TAG_PROCESS_USER);
        OwtelAppController.getInstance().addToRequestQueue(mStringRequestRegisterUser);
    }
}
