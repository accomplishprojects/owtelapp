package com.katadigital.owtel.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

    public static final String TIMEZONE_UTC = "UTC";

    public static Date parseDateStringToDate(String dateFormat,String dateString, boolean utc) throws  ParseException{
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        if(utc){
            TimeZone utcTimeZone = TimeZone.getTimeZone(TIMEZONE_UTC);
            formatter.setTimeZone(utcTimeZone);
        }
        return  formatter.parse(dateString);
    }

    public static String changeDateStringFormat(String newDateFormat,String oldDateFormat,String value) throws ParseException {
        TimeZone utc = TimeZone.getTimeZone(TIMEZONE_UTC);
        SimpleDateFormat oldFormat = new SimpleDateFormat(oldDateFormat);
        SimpleDateFormat newFormat = new SimpleDateFormat(newDateFormat);
        oldFormat.setTimeZone(utc);
        newFormat.setTimeZone(TimeZone.getDefault());
        Date date = oldFormat.parse(value);
        return newFormat.format(date);
    }

//    public static String getCurrentUtcDate(){
//        SimpleDateFormat formatter = new SimpleDateFormat(Constants.API_DATE_FORMAT);
//        TimeZone utcTimeZone = TimeZone.getTimeZone(TIMEZONE_UTC);
//        formatter.setTimeZone(utcTimeZone);
//        return formatter.format(new Date());
//    }

}
