package com.katadigital.owtel.modules.main;

import android.content.Context;

/**
 * Created by Omar Matthew Reyes on 7/5/16.
 * Interface for various Tasks
 */
public interface MainActivityBridge {
    void finishActivity();
    void initBlocker();
    void switchTab(int tab);
    void setBadgeZero(Context context, boolean isActive);
}
