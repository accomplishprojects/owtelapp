package com.katadigital.owtel.modules.main.contacts.util;

import android.app.Activity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.kata.phone.R;

public class CustomTextField {

    // include
    public static EditText includeInit(int idData, Activity activity) {
        View include = activity.findViewById(idData);
        final EditText editText = (EditText) include
                .findViewById(R.id.txtField);
        final Button clearBtn = (Button) include
                .findViewById(R.id.btnclear_text);
        editText.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        clearBtn.setTypeface(NewUtil.getFontRoman(activity));
        editText.setTypeface(NewUtil.getFontRoman(activity));

        clearBtn.setTextSize(NewUtil.gettxtSize());
        editText.setTextSize(NewUtil.gettxtSize());

        clearBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearBtn.setVisibility(View.GONE);
                editText.setText("");
            }
        });
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i,
                                          int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2,
                                      int i3) {
                if (charSequence.length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        return editText;
    }

}
