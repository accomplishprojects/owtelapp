package com.katadigital.owtel.modules.main.messaging;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.avast.android.dialogs.iface.ISimpleDialogListener;
import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.DaoSession;
import com.katadigital.owtel.daoDb.Message;
import com.katadigital.owtel.daoDb.MessageDao;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.models.MessageList;
import com.katadigital.owtel.models.VerySimpleContact;
import com.katadigital.owtel.modules.login.LoginActivity;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.AddToNewRecipient;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.messaging.adapters.ChatAdapter;
import com.katadigital.owtel.modules.main.messaging.adapters.RecyclerItemClickListener;
import com.katadigital.owtel.modules.main.messaging.service.WebSocket;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.owtel.tools.MyTools;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.Logger;
import com.katadigital.owtel.utils.ProgressDialogHelper;
import com.katadigital.owtel.utils.RxBus;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.portsip.helper.Ring;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.katadigital.ui.Style;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.tavendo.autobahn.WebSocketConnection;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ChatActivity extends AppCompatActivity implements ISimpleDialogListener {

//    private static final int ID_SINGLE_CHOICE_DIALOG = R.id.btn_single_choice_dialog;


//    private LovelySaveStateHandler saveStateHandler;

    public static final String TAG = "ChatActivity";
    private static final int REQUEST_CONTACT_NAME = 9129;
    private static final int DELETE_COVERSATION_ITEM = 31;
    private int longClickPosition = 0;
    public static boolean visible;

    @Bind(R.id.tool_bar)
    protected Toolbar toolbar;

//    @Bind(R.id.btn_detail)
//    protected Button btnDetail;

    @Bind(R.id.rv_chat)
    protected RecyclerView rvChat;

    @Bind(R.id.et_message)
    protected EditText etMessage;

    @Bind(R.id.recipientViewInner)
    protected RelativeLayout recipientViewInner;

    @Bind(R.id.btn_send)
    protected ImageButton btnSend;

    @Bind(R.id.imgViewAddRecipient)
    protected ImageView imgViewAddRecipient;

    @Bind(R.id.toRecipientNumber)
    protected EditText toRecipientNumber;


    @Inject
    ApiService apiService;

    @Inject
    DaoSession daoSession;

    @Inject
    RxBus rxBus;


    List<Message> messageList = new ArrayList<>();

    ChatAdapter chatAdapter;

    WebSocketConnection webSocketConnection;

    CompositeSubscription subscriptions;

    //    //test value
//    public static final String SELF_ID = "2132135364";
//    public static final String ROOM_ID = "11012011";
//    private String owtelNumber = "";
    private String threadID = "";
    private String smsID = "";
    private String receiver = "";
    private String currentSMSID = "";
    private boolean isSharedMessage = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        saveStateHandler = new LovelySaveStateHandler();
        setContentView(R.layout.chat_activity_layout);
        NotificationHelper.init(this);
        SharedPreferenceManager.init(this);
        Style.statusBarColor(this, R.color.colorPrimaryToolbar);
        ButterKnife.bind(this);
        OwtelAppController.getComponent(this).inject(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.messages_new));
        getSupportActionBar().setSubtitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rvChat.setLayoutManager(new LinearLayoutManager(this));

        webSocketConnection = WebSocket.getWebSocketConnection();
        setAdapter(messageList);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String sharedMessage = getIntent().getStringExtra("SharedMessage");
            if (sharedMessage == null) {
                loadMyMessages(bundle);
            } else {
                etMessage.setText(sharedMessage);
                isSharedMessage = true;
            }
        }

        rvChat.addOnItemTouchListener(
                new RecyclerItemClickListener(this, rvChat, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Message message = chatAdapter.getMessage(position);
                        if (message.getStatus() != null && message.getStatus().equals("failed")) {
                            sendMessageAPI(receiver, message.getValue(), message.getId(), "");
                        }
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                        longClickPosition = position;
//                        openContextMenu(view);
//                        showSingleChoiceDialog(position);
                    }
                }));

    }


    private void loadMyMessages(Bundle bundle) {
/** Load bundle data if available **/
        Message messageItem = null;

        receiver = bundle.getString("receiver");


        /** Method for Existing Thread **/

        if (bundle != null && bundle.getString("thread_id") != null) {
            threadID = bundle.getString("thread_id");
            smsID = bundle.getString("sms_id");
            loadMessagefromDB(threadID, smsID);
        } else {


            /** Method for Existing Thread But No ThreadID **/
            messageItem = OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder().where(MessageDao.Properties.Sender.eq(receiver)).orderDesc(MessageDao.Properties.Created).limit(1).unique();
            if (messageItem == null)
                messageItem = OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder().where(MessageDao.Properties.Receiver.eq(receiver)).orderDesc(MessageDao.Properties.Created).limit(1).unique();
            if (messageItem != null) {
                threadID = (messageItem.getRoomID() != null) ? messageItem.getRoomID() : threadID;
                smsID = (messageItem.getMessageID() != null) ? messageItem.getMessageID() : smsID;
                loadMessagefromDB(threadID, smsID);
            } else

            /** Method for Existing Thread But No ThreadID and No Message Located **/ {
                recipientViewInner.setVisibility(View.GONE);
                getSupportActionBar().setTitle(receiver);
            }
        }
    }


    private void loadMessagefromDB(String threadID, String smsID) {
        getMessageIdsFromDb(threadID, receiver).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(message -> {
                    ChatActivity.this.messageList = message;
                    setAdapter(message);
                }, error -> {
                    messageList = new ArrayList<>();
                    setAdapter(messageList);
                    error.printStackTrace();
                });
        recipientViewInner.setVisibility(View.GONE);
        getMessageAPI(new Functions().getNumber(), threadID, smsID, "old");
    }


    private void copyToClipoard(int position) {
        Message message = chatAdapter.getMessage(position);
        ClipboardManager _clipboard = (ClipboardManager) ChatActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
        _clipboard.setText(message.getValue());
        Toast.makeText(ChatActivity.this, "Message copied to clipboard...", Toast.LENGTH_SHORT).show();

    }

    private void deleteMessageItem(int position) {
        Message message = chatAdapter.getMessage(position);
        message.setIs_deleted("1");
        OwtelAppController.getInstance().getDaoSession().getMessageDao().update(message);
        //just run
        loadMessagefromDB(threadID, smsID);
    }

//    private void viewMessageItemDetails(int position) {
//        Message message = chatAdapter.getMessage(position);
//
////        new LovelyInfoDialog(this)
////                .setTopColorRes(R.color.colorPrimary)
////                .setIcon(R.drawable.ic_info_outline_white_36dp)
////                //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
////                .setNotShowAgainOptionEnabled(0)
////                .setTitle(R.string.app_name)
////                .setMessage(messageList.toString())
////                .show();
//
//    }


//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v,
////                                    ContextMenu.ContextMenuInfo menuInfo) {
////
////        menu.setHeaderTitle("Select The Action");
////        menu.add(0, v.getId(), 0, "Call");//groupId, itemId, order, title
////        menu.add(0, v.getId(), 0, "SMS");
////
////    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "Delete") {
            deleteMessageItem(longClickPosition);
        } else if (item.getTitle() == "Copy") {
            copyToClipoard(longClickPosition);
        } else if (item.getTitle() == "View Details") {
            Message message = chatAdapter.getMessage(longClickPosition);
            String string = message.getCreated();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date date = null;
            try {
                date = format.parse(string);
                String s = new SimpleDateFormat("MMMM dd, yyyy HH:mm a").format(date);
                SimpleDialogFragment.createBuilder(this, getSupportFragmentManager()).setTitle("Message Options").setMessage("Type: " + message.getType() + "\nTo: " + message.getReceiver() + "\nSent: " + s).setTargetFragment(new Fragment(), 1).setPositiveButtonText(getString(R.string.ok)).show();
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else {
            return false;
        }
        return true;
    }


//    private void showSingleChoiceDialog(int positionMessageItem) {
//        ArrayAdapter<MessageTaskObj> adapter = new MessageTaskAdapter(this, loadMessageTaskOptions());
//        new LovelyChoiceDialog(this)
//                .setTopColorRes(R.color.colorPrimary)
////                .setTitle(R.string.app_name)
//                .setIcon(R.drawable.ic_info_outline_white_36dp)
//                .setItems(adapter, new LovelyChoiceDialog.OnItemSelectedListener<MessageTaskObj>() {
//                    @Override
//                    public void onItemSelected(int position, MessageTaskObj item) {
//                        switch (position) {
//                            case 0:
//                                deleteMessageItem(positionMessageItem);
//                                break;
//                            case 1:
//                                copyToClipoard(positionMessageItem);
//                                break;
//                            case 3:
//                                viewMessageItemDetails(positionMessageItem);
//                                break;
//                            default:
//                                Toast.makeText(ChatActivity.this,
//                                        item.getTitle(),
//                                        Toast.LENGTH_SHORT)
//                                        .show();
//                                break;
//                        }
//
//                    }
//                })
//                .show();
//    }


//    private List<MessageTaskObj> loadMessageTaskOptions() {
//        List<MessageTaskObj> result = new ArrayList<>();
//        String[] raw = getResources().getStringArray(R.array.message_task);
//        for (String op : raw) {
//            String[] info = op.split("%");
//            result.add(new MessageTaskObj(info[0], info[1]));
//        }
//        return result;
//    }


    @Override
    protected void onStart() {
        super.onStart();
        subscribeToEvents();
    }

    @Override
    protected void onStop() {
        super.onStop();
        subscriptions.unsubscribe();
//        OwtelAppController.getInstance().stopWebSocketService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CONTACT_NAME) {
            if (resultCode == this.RESULT_OK) {
                String result = data.getStringExtra("result");
                receiver = result;
                toRecipientNumber.setText(result);
            }
            if (resultCode == this.RESULT_CANCELED) {
            }
        }
    }

    private Observable<List<Message>> getMessageIdsFromDb(String roomId, String receiver) {
        return Observable.create(subscriber -> {
            Log.e(TAG, "getMessageIdsFromDb: receiver " + receiver);

            if (receiver != null) {
                if (receiver.equals("18008804188")) {
                    //delete duplicate entries
                    List<Message> messageList = OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder().where(MessageDao.Properties.RoomID.eq(roomId), MessageDao.Properties.Is_deleted.eq("0")).orderAsc(MessageDao.Properties.Created).list();
                    Message messageOriginalItem = null;
                    for (Message message : messageList
                            ) {
                        // Delete Duplicate Intro Message
                        if (message.getValue().equalsIgnoreCase(getString(R.string.welcome_message_owtel_sms))) {
                            messageOriginalItem = message;
                            OwtelAppController.getInstance().getDaoSession().getMessageDao().delete(message);
                        }
                    }
                    if (messageOriginalItem != null)
                        OwtelAppController.getInstance().getDaoSession().getMessageDao().insert(messageOriginalItem);
                }
                subscriber.onNext(OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder().where(MessageDao.Properties.RoomID.eq(roomId), MessageDao.Properties.Is_deleted.eq("0")).orderAsc(MessageDao.Properties.Created).list());
            } else {
                subscriber.onNext(OwtelAppController.getInstance().getDaoSession().getMessageDao().queryBuilder().where(MessageDao.Properties.RoomID.eq(roomId), MessageDao.Properties.Is_deleted.eq("0")).orderAsc(MessageDao.Properties.Created).list());
            }
            subscriber.onCompleted();
        });
    }

//    private Observable<List<Message>> getMessagesFromDB(String roomID) {
//        return getMessageIdsFromDb(roomID).flatMap(messageList -> Observable.from(messageList))
//                .map(messageMap -> {
//                    Message messageList = messageDao.queryBuilder().where(MessageDao.Properties.MessageID.eq(messageMap.getMessageID())).unique();
//                    messageList.setUser(userDao.queryBuilder().where(UserDao.Properties.UserID.eq(messageList.getUserID())).unique());
////                    messageMap.setMessage(messageList);
//                    return messageList;
//                }).toList();
//
//    }

    private void setAdapter(List<Message> message) {
        if (chatAdapter == null) {
            chatAdapter = new ChatAdapter(this, message, new Functions().getNumber());
            rvChat.setAdapter(chatAdapter);
            rvChat.getLayoutManager().scrollToPosition(message.size() - 1);
            return;
        }
        chatAdapter.updateData(message);
        rvChat.getLayoutManager().scrollToPosition(message.size() - 1);
    }

    @OnClick(R.id.btn_send)
    protected void onBtnSendClick() {
        if (!etMessage.getText().toString().trim().isEmpty()) {
            // Check if app should allow user to send messageList
            if (new ApiHelper(this).proceedSendNumber()) {
//                if (webSocketConnection.isConnected()) {
                if (receiver == null || receiver.equals(""))
                    receiver = toRecipientNumber.getText().toString();
                if (receiver.equalsIgnoreCase(new Functions().getNumber())) {
                    Log.e(TAG, "onBtnSendClick: " + "Self");
                    sendMessageAPI(receiver, etMessage.getText().toString().trim(), null, "self");
                } else {
                    Log.e(TAG, "onBtnSendClick: " + "None");
                    sendMessageAPI(receiver, etMessage.getText().toString().trim(), null, "");
                }
            } else {
                Toast.makeText(this, getString(R.string.error_credits_insufficient), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.btn_call)
    protected void onBtnCallClicked() {
        /**Initiating call to PortSip*/
        if (recipientViewInner.getVisibility() == View.GONE)
            QuickContactHelper.initiateSipCall(this, receiver);
        else
            Toast.makeText(ChatActivity.this, "Unable to make calls on an empty number.", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.imgViewAddRecipient)
    protected void onBtnAddRecipientClick() {
        if (toRecipientNumber.getText().toString().equals("")) {
            Intent intent = new Intent(this, AddToNewRecipient.class);
            startActivityForResult(intent, REQUEST_CONTACT_NAME);
            this.overridePendingTransition(R.anim.slide_in_up,
                    R.anim.slide_in_up_exit);
        } else {
            Toast.makeText(ChatActivity.this, "Recipients number already added.", Toast.LENGTH_SHORT).show();
        }
    }

    private void subscribeToEvents() {
        subscriptions = new CompositeSubscription();

        subscriptions.add(rxBus.toObservable().subscribe(event -> {
            if (event instanceof Message) {
                Logger.e(TAG, "Message Event Detected!");
                doMessageEvent((Message) event, false);
            }
        }));


    }


    private void doMessageEvent(Message message, boolean isSender) {

        if (threadID != null && threadID.equals(message.getRoomID())) {
            threadID = message.getRoomID();
            smsID = message.getMessageID();
            recipientViewInner.setVisibility(View.GONE);
            message.setIs_message_read("1");
            message.setIs_deleted("0");
            OwtelAppController.getInstance().getDaoSession().getMessageDao().insertOrReplace(message);
            this.messageList.add(message);
            chatAdapter.notifyItemInserted(this.messageList.size() + 1);
            rvChat.getLayoutManager().scrollToPosition(this.messageList.size() - 1);
        } else if (recipientViewInner.getVisibility() == View.VISIBLE) {
            message.setIs_message_read("0");
//            makeNotifications(message);
            NotificationHelper.getNotificationHelperInstance().notifyReceivedMessage(this, message);
        } else {
            threadID = message.getRoomID();
            smsID = message.getMessageID();
            message.setIs_message_read("1");
            message.setIs_deleted("0");
            OwtelAppController.getInstance().getDaoSession().getMessageDao().insertOrReplace(message);

            getMessageIdsFromDb(threadID, receiver).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(messageUniversal -> {
                        ChatActivity.this.messageList = messageUniversal;
                        setAdapter(messageUniversal);
                    }, error -> {
                        messageList = new ArrayList<>();
                        setAdapter(messageList);
                        error.printStackTrace();
                    });
            recipientViewInner.setVisibility(View.GONE);
            getMessageAPI(new Functions().getNumber(), threadID, smsID, "Old");
        }

        /**This block of code is for
         * checking if the message is send or received
         * by the user (then it will play a sound)
         * */
        if (isSender) {
            Ring.getInstance(this).startNotificationTone(isSender);
        } else {
            /**Play notification sound*/
            Ring.getInstance(this).startNotificationTone(isSender);
        }
    }

    private void makeNotifications(Message message) {
        NotificationManager notificationManager = (NotificationManager) OwtelAppController.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        Bitmap icon = BitmapFactory.decodeResource(OwtelAppController.getInstance().getResources(),
                R.mipmap.ic_launcher);

        Intent chatActivityIntent = new Intent(OwtelAppController.getInstance(), ChatActivity.class);
        chatActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_CHAT_OWTEL_NUMBER, message.getSender());
        args.putString(Constants.BUNDLE_CHAT_THREAD_ID, message.getRoomID());
        args.putString(Constants.BUNDLE_CHAT_SMS_ID, message.getMessageID());
        args.putString(Constants.BUNDLE_CHAT_RECEIVER, message.getReceiver());
        chatActivityIntent.putExtras(args);

        /**Play notification sound*/
//        Ring.getInstance(getApplicationContext()).startNotificationTone();

        /**Show notification*/
        Notification.Builder mBuilder = new Notification.Builder(getApplicationContext());
        mBuilder.setContentText(message.getValue())
                .setContentTitle("Owtel message")
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(OwtelAppController.getInstance(), 0,
                        chatActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(R.drawable.messageimage)
                .setLargeIcon(icon);
        notificationManager.notify(0, mBuilder.build());
        OwtelAppController.getInstance().getDaoSession().getMessageDao().insertOrReplace(message);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        switch (intent.getAction()) {
            case Intent.ACTION_SEND:
                String sharedMessage = intent.getStringExtra("SharedMessage");
                if (!sharedMessage.isEmpty()) {
                    etMessage.setText(sharedMessage);
                }
                break;

            default:
//                Intent i = new Intent(ChatActivity.this, ChatActivity.class);
//                i.putExtra("receiver", intent.getExtras().getString(Constants.BUNDLE_CHAT_RECEIVER));
//                i.putExtra("thread_id", intent.getExtras().getString(Constants.BUNDLE_CHAT_THREAD_ID));
//                i.putExtra("sms_id", intent.getExtras().getString(Constants.BUNDLE_CHAT_SMS_ID));
//                finish();
//                startActivity(i);
                break;
        }
    }

    private void getMessageAPI(String owtelNumber, String threadID, String smsID, String scroll) {

        setActivityTitle(receiver);
        Log.d(TAG, "getMessageAPI: started");
        Log.d(TAG, "getMessageAPI: " + owtelNumber);
        Log.d(TAG, "getMessageAPI: " + threadID);
        Log.d(TAG, "getMessageAPI: " + smsID);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiService.SERVICE_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        ApiService service = retrofit.create(ApiService.class);

        service.getMessagesRxJava(owtelNumber, threadID, smsID, scroll)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MessageList>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.toString());
                    }

                    @Override
                    public void onNext(MessageList messageList) {
                        for (com.katadigital.owtel.models.Message messageModel : messageList.getMessage()) {
                            Message message = new Message(Long.parseLong(messageModel.getSmsId()));
                            message.setRoomID(messageModel.getThreadId());
                            message.setMessageID(messageModel.getSmsId());
                            message.setType("text");
                            message.setValue(messageModel.getSmsContent());
                            message.setReceiver(messageModel.getReceiver());
                            message.setSender(messageModel.getSender());
                            message.setCreated(messageModel.getSmsDate());
                            message.setUserID(new Functions().getNumber());
                            message.setIs_message_read("1");
                            message.setIs_deleted("0");

                            saveToDB(message);
                        }
                        getMessageIdsFromDb(threadID, receiver).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(message -> {
                                    ChatActivity.this.messageList = message;
                                    setAdapter(message);
                                }, error -> {
                                    ChatActivity.this.messageList = new ArrayList<>();
                                    setAdapter(ChatActivity.this.messageList);
                                    error.printStackTrace();
                                });
                    }
                });

    }

    private void setActivityTitle(String receiver) {
        VerySimpleContact verySimpleContact = new StringFormatter().getContactDetailsViaMainThread(receiver);
        if (verySimpleContact.getName() != null) {
            getSupportActionBar().setTitle(verySimpleContact.getName());
            String formattedNumber = PhoneNumberUtils.formatNumber(verySimpleContact.getNumber());
            getSupportActionBar().setSubtitle(formattedNumber);
        } else {
            getSupportActionBar().setTitle(verySimpleContact.getNumber());
            getSupportActionBar().setSubtitle(null);
        }
    }


    private void sendMessageAPI(String receiver, String messageToSend, Long key, String route) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiService.SERVICE_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        ApiService service = retrofit.create(ApiService.class);
        ProgressDialogHelper.showDialog(ChatActivity.this);
        service.sendMessage(new Functions().getNumber(), receiver, messageToSend, route)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<com.katadigital.owtel.models.Message>() {
                    @Override
                    public void onCompleted() {
                        if (key != null) {
                            OwtelAppController.getInstance().getDaoSession().getMessageDao().deleteByKey(key);
                            getMessageIdsFromDb(threadID, receiver).subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(message -> {
                                        ChatActivity.this.messageList = message;
                                        setAdapter(message);
                                    }, error -> {
                                        messageList = new ArrayList<>();
                                        setAdapter(messageList);
                                        error.printStackTrace();
                                    });
                            recipientViewInner.setVisibility(View.GONE);
                            getMessageAPI(new Functions().getNumber(), threadID, smsID, "Old");
                        }
                        etMessage.setText("");
                        ProgressDialogHelper.hideDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "sendMessage " + e.toString());
                        // Do Failed Message Update
                        Long generatedID = MyTools.generateRandom(10);
                        Calendar mCalendar = new GregorianCalendar();
                        TimeZone mTimeZone = mCalendar.getTimeZone();
                        int mGMTOffset = mTimeZone.getRawOffset();
                        Date d1 = new Date(System.currentTimeMillis() - TimeUnit.MILLISECONDS.convert(mGMTOffset, TimeUnit.MILLISECONDS));
                        String s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d1);
                        Log.d(TAG, "sendMessage onNext: b " + s);
                        Message messageFailed = new Message(generatedID);
                        Log.e(TAG, "onCompleted: checker 3 " + generatedID + " " + messageToSend);
                        messageFailed.setRoomID(threadID);
                        messageFailed.setMessageID(generatedID.toString());
                        messageFailed.setType("text");
                        messageFailed.setValue(messageToSend);
                        if (receiver != null && !receiver.equals(""))
                            messageFailed.setReceiver(receiver);

                        messageFailed.setSender(new Functions().getNumber());


                        messageFailed.setCreated(s);
                        messageFailed.setUserID(new Functions().getNumber());
                        messageFailed.setIs_message_read("1");
                        messageFailed.setIs_deleted("0");
                        messageFailed.setStatus("failed");
                        setActivityTitle(receiver);
                        if (e.getLocalizedMessage().contains("Network is unreachable")) {
                            DialogBuilder.showAlertDialog(ChatActivity.this,
                                    getString(R.string.error_title), getString(R.string.message_sending_failed),
                                    getResources().getString(R.string.string_ok));
                        } else {
                            DialogBuilder.showAlertDialog(ChatActivity.this,
                                    getString(R.string.error_title), e.getLocalizedMessage(),
                                    getResources().getString(R.string.string_ok));
                        }
                        doMessageEvent(messageFailed, true);
                        if (key != null) {
                            OwtelAppController.getInstance().getDaoSession().getMessageDao().deleteByKey(key);
                            // load local messages from DB initially

                            getMessageIdsFromDb(threadID, receiver).subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(messageUniversal -> {
                                        ChatActivity.this.messageList = messageUniversal;
                                        setAdapter(messageUniversal);
                                    }, error -> {
                                        messageList = new ArrayList<>();
                                        setAdapter(messageList);
                                        error.printStackTrace();
                                    });
                            recipientViewInner.setVisibility(View.GONE);
                            getMessageAPI(new Functions().getNumber(), threadID, smsID, "Old");

                        }
                        etMessage.setText("");
                        ProgressDialogHelper.hideDialog();
                        Log.e(TAG, "MyTracker roomID:" + messageFailed.getRoomID() + " smsID:" + messageFailed.getMessageID() + " value:" + messageFailed.getValue() + " created:" + messageFailed.getCreated());

                    }

                    @Override
                    public void onNext(com.katadigital.owtel.models.Message messageModel) {

                        if (messageModel.getStatus().equalsIgnoreCase("failed")) {
//                            if (messageModel.getMessage().equalsIgnoreCase("Insufficient IDD Credits")) {//Do Auto Top-up dialog
//                                SimpleDialogFragment.createBuilder(ChatActivity.this, ChatActivity.this.getSupportFragmentManager()).setTitle(getString(R.string.error_credits_insufficient)).setMessage("Would you like to do the top-up now ?").setPositiveButtonText(getString(R.string.ok)).setNegativeButtonText(getString(R.string.cancel)).setRequestCode(MainActivity.REQUEST_TOP_UP).show();
//                            } else
                            //failed messageList from API
                            // Do Failed Message Update
                            Long generatedID = MyTools.generateRandom(10);
                            Calendar mCalendar = new GregorianCalendar();
                            TimeZone mTimeZone = mCalendar.getTimeZone();
                            int mGMTOffset = mTimeZone.getRawOffset();
                            Date d1 = new Date(System.currentTimeMillis() - TimeUnit.MILLISECONDS.convert(mGMTOffset, TimeUnit.MILLISECONDS));
                            String s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d1);

//                            String s = new SimpleDateFormat("MM-dd-yyyy HH:mm").format(d1);

                            Message messageFailed = new Message(generatedID);
                            messageFailed.setRoomID(threadID);
                            messageFailed.setMessageID(generatedID.toString());
                            messageFailed.setType("text");
                            messageFailed.setValue(messageToSend);
                            if (receiver != null && !receiver.equals(""))
                                messageFailed.setReceiver(receiver);
                            messageFailed.setSender(new Functions().getNumber());
                            messageFailed.setCreated(s);
                            messageFailed.setUserID(new Functions().getNumber());
                            messageFailed.setIs_message_read("1");
                            messageFailed.setIs_deleted("0");
                            messageFailed.setStatus("failed");
                            String formattedNumber = PhoneNumberUtils.formatNumber(new StringFormatter().getContactNameViaMainThread(receiver));
                            getSupportActionBar().setTitle(formattedNumber);
                            doMessageEvent(messageFailed, true);
                            setActivityTitle(receiver);
                            Log.e(TAG, "MyTracker 1 roomID:" + messageFailed.getRoomID() + " smsID:" + messageFailed.getMessageID() + " value:" + messageFailed.getValue() + " created:" + messageFailed.getCreated());
                        } else {
                            //If Success
                            etMessage.setText("");
                            Message message = new Message(Long.parseLong(messageModel.getSmsId()));
                            message.setRoomID(messageModel.getThreadId());
                            message.setMessageID(messageModel.getSmsId());
                            message.setType("text");
                            message.setValue(messageModel.getSmsContent());
                            message.setReceiver(messageModel.getReceiver());
                            message.setSender(messageModel.getSender());
                            message.setCreated(messageModel.getSmsDate());
                            message.setRoute(messageModel.getRoute());
                            message.setUserID(new Functions().getNumber());
                            message.setIs_message_read("1");
                            message.setIs_deleted("0");
                            setActivityTitle(receiver);
                            recipientViewInner.setVisibility(View.GONE);
                            doMessageEvent(message, true);
                            Log.e(TAG, "MyTracker 2 roomID:" + message.getRoomID() + " smsID: " + message.getMessageID() + " value:" + message.getValue() + " created:" + message.getCreated());
                        }

                    }
                });

    }


    private static void saveToDB(Message message) {
        // save to db
        MessageDao messageDao = OwtelAppController.getInstance().getDaoSession().getMessageDao();
        if (messageDao.queryBuilder().where(MessageDao.Properties.MessageID.eq(message.getMessageID())).list().isEmpty()) {
            messageDao.insert(message);
            Log.d(TAG, "saveToDB: " + message.toString());
        } else {
            Log.d(TAG, "saveToDB: " + message.getMessageID() + " already exist");
            Log.d(TAG, "saveToDB: " + messageDao.queryBuilder().where(MessageDao.Properties.MessageID.eq(message.getMessageID())).list().get(0).toString() + " already exist");

        }
    }


    @Override
    public void onNegativeButtonClicked(int requestCode) {
        switch (requestCode) {
            case DELETE_COVERSATION_ITEM:
                break;
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
        switch (requestCode) {
            case DELETE_COVERSATION_ITEM:
                break;
        }
    }

    @Override
    public void onPositiveButtonClicked(int requestCode) {
        switch (requestCode) {
            case MainActivity.REQUEST_TOP_UP:
                // FIXME: 3/22/17 redirect to topup
                Log.d(TAG, "onPositiveButtonClicked: REQUEST_TOP_UP");
                setResult(MainActivity.REQUEST_TOP_UP);
                finish();
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        new Functions().hideKeyboard(ChatActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new Functions().hideKeyboard(ChatActivity.this);
    }

    @Override
    public void onBackPressed() {
        if (isSharedMessage) {
            startActivity(new Intent(this, LoginActivity.class).setAction(""));
            this.finish();
        } else {
            super.onBackPressed();
        }
    }
}




