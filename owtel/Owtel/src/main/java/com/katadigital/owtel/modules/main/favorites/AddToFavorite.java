package com.katadigital.owtel.modules.main.favorites;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AlphabetIndexer;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Item;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Row;
import com.katadigital.owtel.modules.main.contacts.adapters.AlphabetListAdapter.Section;
import com.katadigital.owtel.modules.main.contacts.adapters.TestSectionedAdapter;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Util;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.ui.pinnedheader.PinnedHeaderListView;

public class AddToFavorite extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    SimpleCursorAdapter mAdapter = null;
    private String mCurrentFilter = null;
    ArrayList<String> contactsArray;
    PinnedHeaderListView contact_listview;
    EditText searchContacts;

    String TAG = this.getClass().getSimpleName();

    // ALPHABET LISTVIEW VARIABLES
    private AlphabetListAdapter adapter;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private int favorites_sideIndexHeight;
    private static float favorites_sideIndexX;
    private static float favorites_sideIndexY;
    private int indexListSize;
    ArrayList<Integer> contactDtoIDList = new ArrayList<Integer>();

    private static final String[] CONTACTS_SUMMARY_PROJECTION = new String[]{
            Contacts._ID, Contacts.DISPLAY_NAME, Contacts.LOOKUP_KEY};
    private String[] PHOTO_BITMAP_PROJECTION = new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO};
    QuickContactHelper quickContactHelper;
    private ProgressDialog progressDialog;
    String searchedName = "";

    String[] let = {"#", "A", "B", "C", "D", "E", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "N", "V", "W",
            "X", "Y", "Z"};

    TextView txt_center_dummy, cancel_search;
    RelativeLayout header, sl;

    InputMethodManager imm;

    Button mButtonCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_favorite);
        contactsArray = new ArrayList<>();

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        header = (RelativeLayout) findViewById(R.id.header);
        cancel_search = (TextView) findViewById(R.id.cancel);
        sl = (RelativeLayout) findViewById(R.id.layout_search);
        mButtonCancel = (Button) findViewById(R.id.cancel_addFav);

        NewUtil.setRomanFont(this, R.id.txt_choose);
        NewUtil.setBoldFont(this, R.id.txt_title_add_fav);

        mButtonCancel.setTypeface(NewUtil.getFontRoman(this));
        cancel_search.setTypeface(NewUtil.getFontRoman(this));

        mButtonCancel.setTextSize(NewUtil.gettxtSize());
        cancel_search.setTextSize(NewUtil.gettxtSize());

        invalidateOptionsMenu();

        quickContactHelper = new QuickContactHelper(this);
        getLoaderManager().initLoader(0, null, AddToFavorite.this);

        mAdapter = new IndexedListAdapter(this, R.layout.list_item_contacts,
                null, new String[]{ContactsContract.Contacts.DISPLAY_NAME,
                Contacts._ID}, new int[]{R.id.display_name});

        initComponents();

        searchContacts.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                AddToFavorite.this.mAdapter.getFilter().filter(cs);
                searchedName = cs + "";

                mCurrentFilter = (!TextUtils.isEmpty(searchedName) ? searchedName : null);
                getLoaderManager().restartLoader(0, null, AddToFavorite.this);
            }

            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        contact_listview.setOnItemClickListener((myAdapter, myView, myItemInt, mylng) -> {
            if (contactDtoIDList.get(myItemInt) > 0) {
                new loadContactDetails(contactDtoIDList.get(myItemInt))
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        txt_center_dummy.setOnClickListener(v -> {
            searchContacts.setVisibility(View.VISIBLE);
            txt_center_dummy.setVisibility(View.GONE);
            header.setVisibility(View.GONE);
            cancel_search.setVisibility(View.VISIBLE);
            searchContacts.requestFocus();

            imm.showSoftInput(searchContacts, InputMethodManager.SHOW_IMPLICIT);
        });

        sl.setOnClickListener(v -> {
            searchContacts.setVisibility(View.VISIBLE);
            header.setVisibility(View.GONE);
            txt_center_dummy.setVisibility(View.GONE);
            cancel_search.setVisibility(View.VISIBLE);
            searchContacts.requestFocus();
            imm.showSoftInput(searchContacts, InputMethodManager.SHOW_IMPLICIT);
        });

        cancel_search.setOnClickListener(v -> {
            imm.hideSoftInputFromWindow(searchContacts.getWindowToken(), 0);
            searchContacts.setText("");
            header.setVisibility(View.VISIBLE);
            cancel_search.setVisibility(View.GONE);
            txt_center_dummy.setVisibility(View.VISIBLE);
            searchContacts.setVisibility(View.GONE);
            Keyboard();
        });

        mButtonCancel.setOnClickListener(v -> {
            imm.hideSoftInputFromWindow(searchContacts.getWindowToken(), 0);
            onBackPressed();
        });
    }

    private void Keyboard() {
//        final View activityRootView = findViewById(R.id.rootView);
//        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
//            Rect r = new Rect();
//            activityRootView.getWindowVisibleDisplayFrame(r);
//            int heightDiff = activityRootView.getRootView()
//                    .getHeight() - (r.bottom - r.top);
//
//            if (heightDiff > 100) {
//                header.setVisibility(View.GONE);
//                cancel_search.setVisibility(View.VISIBLE);
//                searchContacts.setVisibility(View.VISIBLE);
//                txt_center_dummy.setVisibility(View.GONE);
//                searchContacts.requestFocus();
//
//            } else {
//                header.setVisibility(View.VISIBLE);
//                cancel_search.setVisibility(View.GONE);
//                searchContacts.setVisibility(View.GONE);
//                txt_center_dummy.setVisibility(View.VISIBLE);
//            }
//        });
    }

    class IndexedListAdapter extends SimpleCursorAdapter implements SectionIndexer {

        AlphabetIndexer alphaIndexer;

        public IndexedListAdapter(Context context, int layout, Cursor c,
                                  String[] from, int[] to) {
            super(context, layout, c, from, to, 0);
        }

        @Override
        public Cursor swapCursor(Cursor c) {

            return super.swapCursor(c);
        }

        @Override
        public int getPositionForSection(int section) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public Object[] getSections() {
            return null;
        }
    }

    public void initComponents() {
        Keyboard();
        searchContacts = (EditText) findViewById(R.id.search_favorite_contacts);
        contact_listview = (PinnedHeaderListView) findViewById(R.id.add_to_favorlist);
        txt_center_dummy = (TextView) findViewById(R.id.txt_search_favorite_contacts);
    }

    class loadContactDetails extends AsyncTask<String, String, String> {
        long contactID;
        ContactDto contactDto;

        public loadContactDetails(long contactID) {
            this.contactID = contactID;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            progressDialog = ProgressDialog.show(AddToFavorite.this,
                    "Please wait...", "Adding contact to favorites...");
            progressDialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            quickContactHelper = new QuickContactHelper(AddToFavorite.this);
            contactDto = new ContactDto();
            contactDto = quickContactHelper.getContactDetails(contactID + "");

            if (contactDto.getPhoneNumbers().size() != 0) {
                ContentValues values = new ContentValues();
                String[] fv = new String[]{contactDto.getContactID()};
                values.put(ContactsContract.Contacts.STARRED, 1);
                getContentResolver().update(
                        ContactsContract.Contacts.CONTENT_URI, values,
                        ContactsContract.Contacts._ID + "= ?", fv);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            if (contactDto.getPhoneNumbers().size() != 0) {
                FavoritesFragment.reloadFavorites = true;
                Util.syncContacts(AddToFavorite.this);
                onBackPressed();
            } else {
                Toast.makeText(AddToFavorite.this,
                        "Cannot add to favorites no contact numbers available",
                        Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_to_favorite, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        adapter = new AlphabetListAdapter(this);
        alphabet = new ArrayList<>();
        sections = new HashMap<>();

        List<Row> rows = new ArrayList<>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Object[] tmpIndexItem;
        Pattern numberPattern = Pattern.compile("[0-9]");
        Pattern specialPattern = Pattern.compile("[^A-Za-z-0-9]");
        contactDtoIDList = new ArrayList<>();

        TestSectionedAdapter secAdapter;
        ArrayList<String> listDataHeader = new ArrayList<>();
        HashMap<String, List<String>> listDataChild = new HashMap<>();
        List<String> items = new ArrayList<>();

        while (data.moveToNext()) {
            String contactDisplayName = data.getString(data
                    .getColumnIndex(Contacts.DISPLAY_NAME));

            String firstLetter = "";

            int contactid = data.getInt(data
                    .getColumnIndex(Contacts._ID));

            if (contactDisplayName != null) {
                firstLetter = contactDisplayName.substring(0, 1).toUpperCase();
            }
            // Group numbers together in the scroller
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }

            if (specialPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }
            // If we've changed to a new letter, add the previous letter to the
            // alphabet scroller
            if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                end = rows.size() - 1;
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = end;
                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equals(previousLetter)) {
                rows.add(new Section(firstLetter));
                sections.put(firstLetter, start);
                contactDtoIDList.add(0);
                listDataHeader.add(firstLetter);
                items = new ArrayList<String>();
                // contactWithNumItems = new ArrayList<Integer>();
            }

            // Add the country to the list
            contactDtoIDList.add(contactid);
            items.add(contactDisplayName);
            // boolean isNum = quickContactHelper
            // .getContactPNumber(contactid + "");
            // if (isNum)
            // contactWithNumItems.add(1);
            // else
            // contactWithNumItems.add(0);
            rows.add(new Item(contactDisplayName));

            if (!firstLetter.equals(previousLetter)) {
                listDataChild.put(firstLetter, items);
                // contactWithNum.put(firstLetter, contactWithNumItems);
            }
            previousLetter = firstLetter;

        }

        if (previousLetter != null) {
            // Save the last letter
            tmpIndexItem = new Object[3];
            tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
            tmpIndexItem[1] = start;
            tmpIndexItem[2] = rows.size() - 1;
            alphabet.add(tmpIndexItem);
        }

        adapter.setRows(rows);
        secAdapter = new TestSectionedAdapter(listDataHeader, listDataChild);
        contact_listview.setAdapter(secAdapter);

        updateList();
    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public android.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri baseUri;
        if (mCurrentFilter != null) {
            baseUri = Uri.withAppendedPath(Contacts.CONTENT_FILTER_URI,
                    Uri.encode(mCurrentFilter));
        } else {
            baseUri = Contacts.CONTENT_URI;
        }

        String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + Contacts.DISPLAY_NAME + " != '' ))";

        String[] projection = new String[]{Contacts._ID,
                Contacts.DISPLAY_NAME, Contacts.CONTACT_STATUS,
                Contacts.CONTACT_PRESENCE, Contacts.PHOTO_ID,
                Contacts.LOOKUP_KEY,};

        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        CursorLoader cursorLoader = new CursorLoader(this, baseUri, projection,
                select, null, sortOrder);

        return cursorLoader;

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, R.anim.slide_out_down);
    }

    TextView tmpTV;
    LinearLayout favorites_sideIndex;

    public void updateList() {
        favorites_sideIndex = (LinearLayout) findViewById(R.id.favorites_sideIndex);
        favorites_sideIndex.removeAllViews();
        indexListSize = alphabet.size();
        if (indexListSize < 1) {
            return;
        }

        int indexMaxSize = (int) Math
                .floor(favorites_sideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

        for (int l = 0; l < let.length; l++) {
            boolean isExist = false;
            for (double i = 1; i <= indexListSize; i = i + delta) {
                Object[] tmpIndexItem = alphabet.get((int) i - 1);
                String tmpLetter = tmpIndexItem[0].toString();

                if (let[l].equalsIgnoreCase(tmpLetter)) {
                    showLetters(let[l]);
                    isExist = true;
                    break;
                }

            }
            if (!isExist) showLetters(let[l]);
        }

        favorites_sideIndexHeight = favorites_sideIndex.getHeight();
        favorites_sideIndex.setOnTouchListener((v, event) -> {
            // now you know coordinates of touch
            favorites_sideIndexX = event.getX();
            favorites_sideIndexY = event.getY();

            // and can display a proper item it country list
            displayListItem();

            return false;
        });
    }

    public void showLetters(String let) {
        tmpTV = new TextView(AddToFavorite.this);
        tmpTV.setText(let);
        tmpTV.setGravity(Gravity.CENTER);
        tmpTV.setTextSize(10);
        tmpTV.setTextColor(AddToFavorite.this.getResources().getColor(
                R.color.phone_app_blue));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        tmpTV.setLayoutParams(params);
        favorites_sideIndex.addView(tmpTV);
    }

    public void displayListItem() {
        LinearLayout favorites_sideIndex = (LinearLayout) findViewById(R.id.favorites_sideIndex);
        favorites_sideIndexHeight = favorites_sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) favorites_sideIndexHeight
                / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (favorites_sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition < alphabet.size()) {
            Object[] indexItem = alphabet.get(itemPosition);
            int subitemPosition = sections.get(indexItem[0]);

            // ListView listView = (ListView) findViewById(android.R.id.list);
            contact_listview.setSelection(subitemPosition);
        }
    }

}
