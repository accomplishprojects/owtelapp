package com.katadigital.owtel.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by dcnc123 on 2/27/17.
 */
public class ProgressDialogHelper {

    private static ProgressDialog pd;

    public static void showDialog(Context context) {
        pd = new ProgressDialog(context);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        pd.show();
    }
    public static void hideDialog() {
        pd.dismiss();
    }
}
