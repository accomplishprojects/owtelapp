package com.katadigital.owtel.modules.main.contacts.entities;

/**
 * Created by Omar Matthew Reyes on 4/25/16.
 * DAO for Promo Code User List
 */
public class PromoCodeDao {
    private String userPromoCodeId;
    private String userPromoCode;
    private String userPromoCodeUser;
    private String userPromoCodeUserNumber;
    private String userPromoCodeUserEmail;

    public String getUserPromoCodeId() {
        return userPromoCodeId;
    }

    public void setUserPromoCodeId(String userPromoCodeId) {
        this.userPromoCodeId = userPromoCodeId;
    }

    public String getUserPromoCode() {
        return userPromoCode;
    }

    public void setUserPromoCode(String userPromoCode) {
        this.userPromoCode = userPromoCode;
    }

    public String getUserPromoCodeUser() {
        return userPromoCodeUser;
    }

    public void setUserPromoCodeUser(String userPromoCodeUser) {
        this.userPromoCodeUser = userPromoCodeUser;
    }

    public String getUserPromoCodeUserNumber() {
        return userPromoCodeUserNumber;
    }

    public void setUserPromoCodeUserNumber(String userPromoCodeUserNumber) {
        this.userPromoCodeUserNumber = userPromoCodeUserNumber;
    }

    public String getUserPromoCodeUserEmail() {
        return userPromoCodeUserEmail;
    }

    public void setUserPromoCodeUserEmail(String userPromoCodeUserEmail) {
        this.userPromoCodeUserEmail = userPromoCodeUserEmail;
    }
}
