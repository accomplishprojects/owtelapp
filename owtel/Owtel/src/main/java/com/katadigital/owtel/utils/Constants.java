package com.katadigital.owtel.utils;


public class Constants {

    public static final String API_URL = "";

    public static final String REQUEST_TAG_REGISTER = "REGISTER";
    public static final String REQUEST_TAG_ASSIGN_NUMBER = "ASSIGN_NUMBER";
    public static final String REQUEST_TAG_SAVE_USER = "SAVE_USER";
    public static final String REQUEST_TAG_GET_AREA_CODES = "GET_AREA_CODES";
    public static final String REQUEST_TAG_GET_US_NUMBERS = "GET_US_NUMBERS";
    public static final String REQUEST_TAG_GET_SUBSCRIPTION_LIST = "GET_SUBSCRIPTION_LIST";
    public static final String REQUEST_TAG_GET_IDD_LIST = "GET_IDD_LIST";
    public static final String REQUEST_TAG_GET_ADDRESS_LIST = "GET_ADDRESS_LIST";
    public static final String REQUEST_TAG_SUBMIT_PAYMENT = "SUBMIT_PAYMENT";
    public static final String REQUEST_TAG_PROCESS_USER = "PROCESS_USER";
    public static final String REQUEST_TAG_FORGOT_PASS = "FORGOT_PASS";

    public static final String BUNDLE_REGISTER_EMAIL = "BUNDLE_REGISTER_EMAIL";
    public static final String BUNDLE_REGISTER_PASSWORD = "BUNDLE_REGISTER_PASSWORD";
    public static final String BUNDLE_REGISTER_WEBCS_USER = "BUNDLE_REGISTER_WEBCS_USER";
    public static final String BUNDLE_REGISTER_AREA_CODE = "BUNDLE_REGISTER_AREA_CODE";
    public static final String BUNDLE_REGISTER_NUMBER = "BUNDLE_REGISTER_NUMBER";
    public static final String BUNDLE_REGISTER_PROMO_CODE = "BUNDLE_REGISTER_PROMO_CODE";
    public static final String BUNDLE_REGISTER_PROMO_CODE_USER = "BUNDLE_REGISTER_PROMO_CODE_USER";
    public static final String BUNDLE_REGISTER_LOCK_ID = "BUNDLE_REGISTER_LOCK_ID";
    //    public static final String BUNDLE_LOGIN = "BUNDLE_LOGIN";
    public static final String BUNDLE_PROMO_CODE_FROM_REGISTER = "BUNDLE_PROMO_CODE_FROM_REGISTER"; // Bundle boolean if to display back button on Promo Code page or not
    public static final String BUNDLE_PROMO_CODE_USER_EMAIL = "BUNDLE_PROMO_CODE_USER_EMAIL";
    public static final String BUNDLE_PAYMENT_MESSAGE = "BUNDLE_PAYMENT_MESSAGE";
    public static final String BUNDLE_HIDE_BTN_BACK = "BUNDLE_HIDE_BTN_BACK";

    public static final String BUNDLE_CHAT_OWTEL_NUMBER = "owtel_number";
    public static final String BUNDLE_CHAT_THREAD_ID = "thread_id";
    public static final String BUNDLE_CHAT_SMS_ID = "sms_id";
    public static final String BUNDLE_CHAT_RECEIVER = "receiver";

    public static final String PARCEL_LIST_AREA_CODES = "PARCEL_LIST_AREA_CODES";
    public static final String PARCEL_LIST_SUBSCRIPTION = "PARCEL_LIST_SUBSCRIPTION";
    public static final String PARCEL_LIST_IDD = "PARCEL_LIST_IDD";
    public static final String PARCEL_LIST_COUNTRY = "PARCEL_LIST_COUNTRY";

    public static final String PREFS_USER_EMAIL = "PREFS_USER_EMAIL";
    public static final String PREFS_USER_PASS = "PREFS_USER_PASS";
    public static final String PREFS_USER_NUMBER = "PREFS_USER_NUMBER";
    public static final String PREFS_USER_TYPE = "PREFS_USER_PROMO_USER";
    public static final String PREFS_USER_SUBSCRIPTION_EXP = "PREFS_USER_SUBSCRIPTION_EXP";
    public static final String PREFS_USER_IDD_CREDITS = "PREFS_USER_IDD_CREDITS";
    public static final String PREFS_SIP_PASS = "PREFS_SIP_PASS";
    public static final String PREFS_SIP_SERVER = "PREFS_SIP_SERVER";
    public static final String PREFS_SIP_PORT = "PREFS_SIP_PORT";
    public static final String PREFS_SIP_PROTOCOL = "PREFS_SIP_PROTOCOL";
    public static final String PREFS_RAD_ID = "PREFS_RAD_ID";
    public static final String PREFS_PROMO_CODE = "";
    public static final String PREFS_NOTIFICATION = "PREFS_NOTIFICATION";
    public static final String PREFS_VIBRATE = "PREFS_VIBRATE";
    public static final String PREFS_VIBRATE_RING = "PREFS_VIBRATE_RING";
    public static final String PREFS_VIBRATE_NOTIFICATION = "PREFS_VIBRATE_NOTIFICATION";
    public static final String PREFS_CONVERSATION_TONE = "PREFS_CONVERSATION_TONE";

    public static String REGEX_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * Password expression. Password must be between 6 and 50 digits long and include at least one numeric digit.
     **/
    public static String REGEX_PASSWORD = "^(?=.*\\d).{6,50}$";

    public static final String DISPLAY_DATE_FORMAT = "MM/yy";

    public static final String API_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String URL_APP_ANDROID = "https://play.google.com/store/apps/details?id=com.kata.owtel";
    public static final String URL_APP_IOS = "https://www.google.com";

    public static final String CURRENCY = "$";

    public static final String REGISTRATION_RESULT = "Registration Result";
    public static final String PROMO_CODE = "Promo Code";
    public static final String ADDRESS_LIST = "Address List";
    public static String USER_CREDENTIALS = "User Credentials";

    /**
     * Notification Tags
     */
    public static final int ZOIPER_NOTIFICATION_CODE = 0001;

    /**
     * Zoiper Broadcast Receiver Action
     */
    public static final String ZOIPER_INITIALIZATION = "zoiper.initialization";
    public static final String ZOIPER_USER_REGISTERED = "zioper.onuserregistered";
    public static final String ZOIPER_USER_REGISTRATION_FAILURE = "zoiper.onregistrationfailure";
    public static final String ZOIPER_USER_REGISTRATION_RETRYING = "zoiper.onregistrationretrying";
    public static final String ZOIPER_USER_UNREGISTERED = "zoiper.onuserunregistered";
    public static final String ZOIPER_ACTIVATION_COMPLETED = "zoiper.onactivitioncompleted";
    public static final String ZOIPER_CALL_CREATE = "zoiper.oncallcreate";
    public static final String ZOIPER_CALL_CREATED = "zoiper.oncallcreated";
    public static final String ZOIPER_CALL_ACCEPTED = "zoiper.oncallaccepted";
    public static final String ZOIPER_CALL_RINGING = "zoiper.oncallringing";
    public static final String ZOIPER_CALL_REJECTED = "zoiper.oncallrejected";
    public static final String ZOIPER_CALL_HOLD = "zoiper.oncallhold";
    public static final String ZOIPER_CALL_UNHOLD = "zoiper.oncallunhold";
    public static final String ZOIPER_CALL_HANGUP = "zoiper.oncallhangup";
    public static final String ZOIPER_CALL_FAILURE = "zoiper.oncallfailure";

    public static final String ZOIPER_CALL_HangupFromCallScreenZoiper = "zoiper.oncallhangupcallscreenzoiper";
    public static final String FINISH_CALL_SCREEN_MESSAGE = "FinishCallScreen";
    public static final String START_TIMER_CALL_SCREEN_MESSAGE = "StartTimerCallScreen";

    /**
     * PortSip Constants
     */
    public static final String PORTSIP_LICENSE_KEY = "1PB01MjQ0MzRBQTFDMERDNjdFQzk0MzQ2NjUxMTExNEUyNkA5NzA4QUMzMTU0RjcxMUM1NUQ0NUQ2NEM1NjZBMjA3N0BBRDRDNzZGNTc2NDUxNjhFOUFDRUI1NEU3QUUxMTFCQkA0MTU0OTg0RDkyREEyNjVGODc5NkQ0NERBMThBM0I4MQ";
    public static final String PORTSIP_REGISTER_ON_RECEIVER = "com.katadigital.portsip.utilities.receiver.register.portsip";
    public static final String PORTSIP_UNREGISTER_ON_RECEIVER = "com.katadigital.portsip.utilities.receiver.unregister.portsip";
    public static final String PORTSIP_RESET_ON_RECEIVER = "com.katadigital.portsip.utilities.receiver.reset.portsip";

    public static final String PORTSIP_REGISTER_ON_SERVICE = "com.katadigital.portsip.utilities.androidcomponent.service.registerportsip";
    public static final String PORTSIP_UNREGISTER_ON_SERVICE = "com.katadigital.portsip.utilities.androidcomponent.service.unregisterportsip";
    public static final String PORTSIP_RESET_ON_SERVICE = "com.katadigital.portsip.utilities.androidcomponent.service.resetportsip";

    public static final String PORTSIP_CALL_CONNECTED_ON_SERVICE = "com.katadigital.portsip.utilities.androidcomponent.service.portsip.oninviteconnected";
    public static final String PORTSIP_CALL_CLOSE_ON_SERVICE = "com.katadigital.portsip.utilities.androidcomponent.service.portsip.oninviteclose";

    public static final int PORTSIP_SDK_STATUS_CODE = 2017;
    public static final int PORTSIP_CALL_STATUS_CODE = 2018;
    public static final int WEB_SOCKET_RECEIVED_MESSAGE = 2019;
    public static final int MESSAGE_MAKE_NOTIFICATION = 2020;

    /**
     * Notification Constants
     */
    public static final String NOTIFY_PORTSIP_ANSWER_CALL_TAG = "com.katadigital.portsip.utilities.notifications.portsip.answercall";
    public static final String NOTIFY_PORTSIP_REJECT_CALL_TAG = "com.katadigital.portsip.utilities.notifications.portsip.rejectcall";
    public static String PREFS_RAD_NAME = "RADPREFIDFIX";

    /**
     * PREFERENCES
     */
    public static final String SHAREDPREFERENCES_FILENAME = "OwtelPreferences";
    public static final String SHAREDPREFERENCES_PORTSIP_STATUS = "PortSipStatus";
    public static final String SHAREDPREFERENCES_CACHE_USERNAME = "Username";
    public static final String SHAREDPREFERENCES_CACHE_PASSWORD = "UserPassword";
    public static final String SHAREDPREFERENCES_USER_REMEMBER_STATUS = "UserRememberStatus";
    public static final String SHAREDPREFERENCES_RAD_ID = "RadId";
    public static final String SHAREDPREFERENCES_OWTEL_NUMBER = "OwtelNumber";
    public static final String SHAREDPREFERENCES_SELECTED_RINGTONE_NAME = "RingtoneName";
    public static final String SHAREDPREFERENCES_SELECTED_RINGTONE_URI = "RingtoneUri";
    public static final String SHAREDPREFERENCES_RINGTONE_VIBRATION = "RingtoneVibration";
    public static final String SHAREDPREFERENCES_SELECTED_NOTIF_NAME = "NotifToneName";
    public static final String SHAREDPREFERENCES_SELECTED_NOTIF_URI = "NotifToneUri";
    public static final String SHAREDPREFERENCES_NOTIF_VIBRATION = "NotifVibration";
    public static final String SHAREDPREFERENCES_CONVO_TONE_STATUS = "ConvoToneVibration";
    public static final String SHAREDPREFERENCES_SMS_BADGE_COUNT = "SMSBadgeCount";
    public static final String SHAREDPREFERENCES_MISSED_CALL_BADGE_COUNT = "MissedCallBadgeCount";

    /**
     * Network Constants
     */
    public static final int NETWORK_TYPE_WIFI = 1;
    public static final int NETWORK_TYPE_MOBILE = 2;
    public static final int NETWORK_TYPE_NOT_CONNECTED = 0;

    /**
     * ActivityForResult RequestCode
     */
    public static final int REQUEST_CODE_MESSAGE_APPS = 3001;

}
