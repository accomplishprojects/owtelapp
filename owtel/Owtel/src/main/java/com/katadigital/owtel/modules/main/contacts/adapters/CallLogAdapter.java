package com.katadigital.owtel.modules.main.contacts.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

public class CallLogAdapter extends BaseAdapter {

    ArrayList<HashMap<String, String>> callLogList;
    Activity context;

    public CallLogAdapter(Activity con,
                          ArrayList<HashMap<String, String>> callLogList) {
        this.callLogList = callLogList;
        context = con;
    }

    @Override
    public int getCount() {
        return callLogList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.calllog_listitem, parent, false);

        TextView time = (TextView) rowView.findViewById(R.id.time);
        TextView type = (TextView) rowView.findViewById(R.id.type);
        TextView dur = (TextView) rowView.findViewById(R.id.duration);

        time.setTypeface(NewUtil.getFontRoman(context));
        type.setTypeface(NewUtil.getFontRoman(context));
        dur.setTypeface(NewUtil.getFontRoman(context));

        time.setTextSize(NewUtil.gettxtSize());
        type.setTextSize(NewUtil.gettxtSize());
        dur.setTextSize(NewUtil.gettxtSize());

        HashMap<String, String> item;
        item = callLogList.get(position);

        time.setText(item.get("time"));

        if (item.get("calltype").equals("OUTGOING")) {
            if (item.get("duration").equals("0")) {
                type.setText(context.getResources().getString(R.string.string_cancled));
                dur.setText(item.get(""));
            } else {
                type.setText(context.getResources().getString(
                        R.string.string_outgoingcall));
                type.setTextColor(Color.parseColor("#669900"));
                dur.setText(durationConverter(Integer.parseInt(item.get("duration"))));
            }

        } else if (item.get("calltype").equals("INCOMING")) {
            type.setText(context.getResources().getString(
                    R.string.string_incomingcall));
            type.setTextColor(Color.parseColor("#0099cc"));
            dur.setText(durationConverter(Integer.parseInt(item.get("duration"))));
        } else {
            type.setText(context.getResources().getString(R.string.missed_string));
            type.setTextColor(Color.parseColor("#cc0000"));
            dur.setText(item.get(""));
        }

        return rowView;
    }

    public String durationConverter(int time) {
        int retTime = 0;
        String ret = "";
        if (time <= 59) {
            retTime = time;// mins
            if (retTime > 1) {
                ret = Integer.toString(retTime) + " seconds";
            } else {
                ret = Integer.toString(retTime) + " second";
            }
        } else if (time > 59) {
            retTime = time / 60;// mins
            if (retTime > 1) {
                ret = Integer.toString(retTime) + " minutes";
            } else {
                ret = Integer.toString(retTime) + " minute";
            }

        } else if (time > 3599) {
            retTime = (time / 60) / 60;// hours
            if (retTime > 1) {
                ret = Integer.toString(retTime) + " hours";
            } else {
                ret = Integer.toString(retTime) + " hour";
            }
        }

        return ret;
    }

}
