package com.katadigital.owtel.modules.main.contacts.callsmsblocker.notificationCenter;

import android.view.Gravity;
import android.widget.Toast;

import com.katadigital.owtel.OwtelAppController;

public class CallBlockerToastNotification {

    public static void showDefaultShortNotification(String message) {
        Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_SHORT).show();
    }

    public static void showDefaultLongNotification(String message) {
        Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    public static void showTopShortNotification(String message) {
        Toast t = Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.TOP, 0, 0);
        t.show();
    }

    public static void showRightShortNotification(String message) {
        Toast t = Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.RIGHT, 0, 0);
        t.show();
    }

    public static void showLeftShortNotification(String message) {
        Toast t = Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.LEFT, 0, 0);
        t.show();
    }

    public static void showTopLongNotification(String message) {
        Toast t = Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_LONG);
        t.setGravity(Gravity.TOP, 0, 0);
        t.show();
    }

    public static void showRightLongNotification(String message) {
        Toast t = Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_LONG);
        t.setGravity(Gravity.RIGHT, 0, 0);
        t.show();
    }

    public static void showLeftLongNotification(String message) {
        Toast t = Toast.makeText(OwtelAppController.getInstance(), message, Toast.LENGTH_LONG);
        t.setGravity(Gravity.LEFT, 0, 0);
        t.show();
    }
}
