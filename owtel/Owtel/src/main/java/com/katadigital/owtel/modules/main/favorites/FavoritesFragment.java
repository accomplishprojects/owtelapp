package com.katadigital.owtel.modules.main.favorites;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Nickname;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.helpers.QuickContactHelper;
import com.katadigital.owtel.helpers.Var;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.ContactDetailActivity;
import com.katadigital.owtel.modules.main.contacts.ContactsFragment;
import com.katadigital.owtel.modules.main.contacts.adapters.FavoritesListAdapter;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.owtel.modules.main.history.LogsFragment;

import java.util.ArrayList;

public class FavoritesFragment extends Fragment {
    private static final String TAG = "FavoritesFragment";

    View rootView;

    static ArrayList<ContactDto> favoriteList = new ArrayList<>();
    ContactDto contactDto;
    static Button editBtn;
    QuickContactHelper quickContactHelper;
    static RecyclerView listView;
    static Fragment currentFragment;
    public static boolean isEditing = false;
    private ImageButton mButtonFavoriteAdd;
    public static boolean reloadFavorites = false;

    TextView noFavorites;

    @Override
    public void onStart() {
        super.onStart();
        if (GlobalValues.DEBUG) Log.i(TAG, "onStart");
    }

    @Override
    public void onResume() {
        if (GlobalValues.DEBUG) Log.i(TAG, "onResume");
        currentFragment = this;
        try {
            ContactDetailActivity.searchValue = "";
            ContactsFragment.searchContacts.setText("");
        } catch (Exception e) {
            Log.e(TAG, "searchContacts " + e);
        }

        if (reloadFavorites) {
            getFavoriteContacts(isEditing);
            reloadFavorites = false;
        }
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favorites, container, false);
        ((MainActivity) getActivity()).currentTab = 0;

        LogsFragment.isEditing = false;

        /**Setting up recycler view*/
        listView = (RecyclerView) rootView.findViewById(R.id.favor_list);
        listView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());

        mButtonFavoriteAdd = (ImageButton) rootView.findViewById(R.id.btn_favorite_add);

        initActionbar();
        noFavorites = (TextView) rootView.findViewById(R.id.no_fav);
        noFavorites.setText(getResources().getString(R.string.no_favour));
        isEditing = false;

        Var.addLike = true;
        Var.fragmentActivity = getActivity();

        try {
            getFavoriteContacts(isEditing);
        } catch (Exception e) {
            Log.e(TAG, "getFavoriteContacts " + e);
        }

        return rootView;
    }

    public void initActionbar() {
        editBtn = (Button) rootView.findViewById(R.id.fave_edit_btn);

        mButtonFavoriteAdd.setOnClickListener(new View.OnClickListener() {
                                                  @Override
                                                  public void onClick(View view) {
                                                      openAddFavorites();
                                                  }
                                              }
        );

        editBtn.setOnClickListener(view -> {
            if (editBtn.getText().equals(
                    getActivity().getResources().getString(
                            R.string.edit_string))) {
                isEditing = true;
                FavoritesListAdapter adapter = new FavoritesListAdapter(
                        getActivity(), favoriteList, currentFragment,
                        isEditing);
                listView.setAdapter(adapter);
                editBtn.setText(getActivity().getResources().getString(
                        R.string.done_string));
                mButtonFavoriteAdd.setVisibility(View.GONE);
            } else {
                isEditing = false;
                FavoritesListAdapter adapter = new FavoritesListAdapter(
                        getActivity(), favoriteList, currentFragment,
                        isEditing);
                listView.setAdapter(adapter);
                editBtn.setText(getActivity().getResources().getString(
                        R.string.edit_string));
                mButtonFavoriteAdd.setVisibility(View.VISIBLE);
            }
        });
    }

    public void openAddFavorites() {
        if (listView.getAdapter().getItemCount() == 0) {
            isEditing = false;
            Log.e(TAG, "openAddFavorites: " + "Clicked Add Likes Button");
            FavoritesListAdapter adapter = new FavoritesListAdapter(getActivity(), favoriteList, currentFragment, isEditing);
            listView.setAdapter(adapter);
            editBtn.setText(getActivity().getResources().getString(
                    R.string.edit_string));
        }

        Intent intent = new Intent(getActivity(), AddToFavorite.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_up,
                R.anim.slide_in_up_exit);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void getFavoriteContacts(Boolean editing) {
        new loadAllFavorites(editing).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Observable get list of Contact favorites
     *
     * @param isEditing boolean
     * @return Observable
     */
    rx.Observable<ArrayList<ContactDto>> doGetFavorites(boolean isEditing) {
        return rx.Observable.create((rx.Observable.OnSubscribe<ArrayList<ContactDto>>) subscriber -> {
            quickContactHelper = new QuickContactHelper(getActivity());

            String[] projection = new String[]{Contacts._ID,
                    Contacts.DISPLAY_NAME,
                    Contacts.STARRED};
            String sortOrder = Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
            Cursor cursor = getActivity().getContentResolver().query(
                    Contacts.CONTENT_URI, projection,
                    "starred=?", new String[]{"1"}, sortOrder);

            int id = cursor.getColumnIndex(Contacts._ID);

            ArrayList<ContactDto> listContactFavorite = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    contactDto = new ContactDto();
                    String contactDisplayName = cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME));
                    String nameWhere = Data.CONTACT_ID + " = ? AND " + Data.MIMETYPE + " = ?";
                    String[] nameWhereParams = new String[]{
                            String.valueOf(cursor.getString(id)),
                            StructuredName.CONTENT_ITEM_TYPE};
                    Cursor nameCur = getActivity().getContentResolver().query(
                            Data.CONTENT_URI, null, nameWhere, nameWhereParams,
                            null);
                    String fName = null;
                    String lLastName = null;
                    String nickName = null;
                    int thumbnailId = 0;
                    if (nameCur.moveToNext()) {
                        thumbnailId = nameCur.getInt(nameCur.getColumnIndex(Contacts.PHOTO_ID));
                        fName = nameCur.getString(nameCur
                                .getColumnIndex(StructuredName.GIVEN_NAME));
                        lLastName = nameCur.getString(nameCur
                                .getColumnIndex(StructuredName.FAMILY_NAME));
                        nickName = nameCur.getString(nameCur
                                .getColumnIndex(Nickname.NAME));
                    }
                    nameCur.close();
                    Bitmap photoData = QuickContactHelper.fetchThumbnail(thumbnailId);
                    if (photoData != null) {
                        contactDto.setProfilepic(photoData);
                    }
                    contactDto.setDisplayName(contactDisplayName);
                    contactDto.setFirstName(fName);
                    contactDto.setLastName(lLastName);
                    contactDto.setNickname(nickName);
                    contactDto.setContactID(cursor.getString(id));
                    listContactFavorite.add(contactDto);
                } while (cursor.moveToNext());
            }
            cursor.close();
            subscriber.onNext(listContactFavorite);
            subscriber.onCompleted();
        });
    }

    class loadAllFavorites extends AsyncTask<String, String, String> {
        Boolean editing;

        @Override
        protected void onPreExecute() {
            favoriteList = new ArrayList<>();
            super.onPreExecute();
        }

        public loadAllFavorites(Boolean editing) {
            this.editing = editing;
        }

        @Override
        protected String doInBackground(String... params) {
            quickContactHelper = new QuickContactHelper(getActivity());

            String[] projection = new String[]{ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.STARRED};
            String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
            Cursor cursor = getActivity().getContentResolver().query(
                    ContactsContract.Contacts.CONTENT_URI, projection,
                    "starred=?", new String[]{"1"}, sortOrder);

            ArrayList<String> contactIDList = new ArrayList<>();
            if (cursor == null) return null;
            if (cursor.moveToFirst()) {
                int id = cursor.getColumnIndex(ContactsContract.Contacts._ID);
                do {
                    contactDto = new ContactDto();
                    // contactDto.setDisplayName(getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));

                    String contactDisplayName = cursor.getString(cursor
                            .getColumnIndex(Contacts.DISPLAY_NAME));

                    String nameWhere = Data.CONTACT_ID + " = ? AND "
                            + Data.MIMETYPE + " = ?";
                    String[] nameWhereParams = new String[]{
                            String.valueOf(cursor.getString(id)),
                            StructuredName.CONTENT_ITEM_TYPE};
                    Cursor nameCur = getActivity().getContentResolver().query(
                            Data.CONTENT_URI, null, nameWhere, nameWhereParams,
                            null);
                    String fName = null;
                    String lLastName = null;
                    String nickName = null;
                    int thumbnailId = 0;
                    if (nameCur.moveToNext()) {
                        thumbnailId = nameCur
                                .getInt(nameCur
                                        .getColumnIndex(ContactsContract.Contacts.PHOTO_ID));

                        fName = nameCur.getString(nameCur
                                .getColumnIndex(StructuredName.GIVEN_NAME));
                        lLastName = nameCur.getString(nameCur
                                .getColumnIndex(StructuredName.FAMILY_NAME));
                        nickName = nameCur.getString(nameCur
                                .getColumnIndex(Nickname.NAME));
                    }
                    nameCur.close();
                    Bitmap photoData = QuickContactHelper.fetchThumbnail(thumbnailId);
                    if (photoData != null) {
                        contactDto.setProfilepic(photoData);
                    }
                    contactDto.setDisplayName(contactDisplayName);
                    contactDto.setFirstName(fName);
                    contactDto.setLastName(lLastName);
                    contactDto.setNickname(nickName);
                    contactDto.setContactID(cursor.getString(id));
                    favoriteList.add(contactDto);

                    contactIDList.add(cursor.getString(id));
                } while (cursor.moveToNext());
            }
            cursor.close();
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // progressDialog.dismiss();
            FavoritesListAdapter adapter = new FavoritesListAdapter(getActivity(),
                    favoriteList, currentFragment, editing);

            String strEditBtn = editing ? getString(R.string.done_string) : getString(R.string.edit_string);
            editBtn.setText(strEditBtn);

            listView.setAdapter(adapter);
            if (listView.getAdapter().getItemCount() > 0) {
                editBtn.setVisibility(View.VISIBLE);
                noFavorites.setVisibility(View.GONE);
            } else {
                adapter = new FavoritesListAdapter(getActivity(), favoriteList,
                        currentFragment, false);
                listView.setAdapter(adapter);
                editBtn.setVisibility(View.INVISIBLE);
                noFavorites.setVisibility(View.VISIBLE);
                mButtonFavoriteAdd.setVisibility(View.VISIBLE);
            }
            super.onPostExecute(result);
        }
    }
}
