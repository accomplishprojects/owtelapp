package com.katadigital.owtel.helpers;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.contacts.entities.ContactDto;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;

public class CustomSheetKata {

    public static void deleteSheet(final Activity activity,
                                   final ContactDto oldContactDto) {
        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();

        View customView = inflater.inflate(R.layout.dialog_profile_pic, null);

        // Build the dialog
        final Dialog selectModelDialog = new Dialog(activity,
                R.style.DialogSlideAnim);
        selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectModelDialog.setContentView(customView);
        selectModelDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                .getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        selectModelDialog.setTitle("Actions");
        selectModelDialog.show();

        TextView textBtn1, textBtn2, textBtn3, textBtn4, textBtn5;
        textBtn1 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_take_photo);
        textBtn2 = (TextView) customView
                .findViewById(R.id.keypad_dialog_choose_photo);
        textBtn3 = (TextView) customView
                .findViewById(R.id.keypad_dialog_edit_photo);
        textBtn4 = (TextView) customView
                .findViewById(R.id.keypad_dialog_delete_photo);
        textBtn5 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_cancel);

        textBtn1.setText(activity.getResources().getString(
                R.string.string_take_photo));
        textBtn2.setText(activity.getResources().getString(
                R.string.string_chooese_photo));
        textBtn3.setText(activity.getResources().getString(
                R.string.string_edit_photo));
        textBtn4.setText(activity.getResources().getString(
                R.string.string_delete_cntct));
        textBtn5.setText(activity.getResources().getString(
                R.string.cancel_string));

        textBtn1.setVisibility(View.GONE);
        textBtn2.setVisibility(View.GONE);
        textBtn3.setVisibility(View.GONE);
        textBtn4.setTextColor(Color.RED);

        if (textBtn1.getVisibility() == View.GONE && textBtn2.getVisibility() == View.GONE && textBtn3.getVisibility() == View.GONE) {
            // Its visible
            textBtn4.setBackgroundResource(R.drawable.roundedbtn_allsides_selector);
        } else {
            textBtn4.setBackgroundResource(R.drawable.roundedbtn_top_selector);
        }


        textBtn1.setTypeface(NewUtil.getFontBold(activity));
        textBtn2.setTypeface(NewUtil.getFontBold(activity));
        textBtn3.setTypeface(NewUtil.getFontBold(activity));
        textBtn4.setTypeface(NewUtil.getFontBold(activity));
        textBtn5.setTypeface(NewUtil.getFontBold(activity));

        textBtn1.setTextSize(NewUtil.gettxtSize());
        textBtn2.setTextSize(NewUtil.gettxtSize());
        textBtn3.setTextSize(NewUtil.gettxtSize());
        textBtn4.setTextSize(NewUtil.gettxtSize());
        textBtn5.setTextSize(NewUtil.gettxtSize());

        textBtn4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                ArrayList<android.content.ContentProviderOperation> ops = new ArrayList<android.content.ContentProviderOperation>();

                String[] args = new String[]{oldContactDto.getContactID()};
                ops.add(ContentProviderOperation
                        .newDelete(RawContacts.CONTENT_URI)
                        .withSelection(RawContacts.CONTACT_ID + "=?", args)
                        .build());
                try {
                    activity.getContentResolver().applyBatch(
                            ContactsContract.AUTHORITY, ops);
                } catch (RemoteException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (OperationApplicationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Intent intent = new Intent(activity, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                // activity.finish();

                selectModelDialog.dismiss();
            }
        });

        textBtn5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectModelDialog.dismiss();
            }
        });

    }

    public static void blockSheet(final Activity activity,
                                  final ContactDto oldContactDto) {
        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();

        View customView = inflater.inflate(R.layout.dialog_profile_pic, null);

        // Build the dialog
        final Dialog selectModelDialog = new Dialog(activity,
                R.style.DialogSlideAnim);
        selectModelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectModelDialog.setContentView(customView);
        selectModelDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = selectModelDialog.getWindow()
                .getAttributes();

        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        selectModelDialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        selectModelDialog.setTitle("Actions");
        selectModelDialog.show();

        TextView textBtn1, textBtn2, textBtn3, textBtn4, textBtn5;
        textBtn1 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_take_photo);
        textBtn2 = (TextView) customView
                .findViewById(R.id.keypad_dialog_choose_photo);
        textBtn3 = (TextView) customView
                .findViewById(R.id.keypad_dialog_edit_photo);
        textBtn4 = (TextView) customView
                .findViewById(R.id.keypad_dialog_delete_photo);
        textBtn5 = (TextView) customView
                .findViewById(R.id.keypad_dialog_btn_cancel);

        textBtn1.setText(activity.getResources().getString(
                R.string.string_take_photo));
        textBtn2.setText(activity.getResources().getString(
                R.string.string_chooese_photo));
        textBtn3.setText(activity.getResources().getString(
                R.string.string_edit_photo));
        textBtn4.setText(activity.getResources().getString(
                R.string.string_delete_cntct));
        textBtn5.setText(activity.getResources().getString(
                R.string.cancel_string));

        textBtn1.setVisibility(View.GONE);
        textBtn2.setVisibility(View.GONE);
        textBtn3.setVisibility(View.GONE);
        textBtn4.setTextColor(Color.RED);

        textBtn5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectModelDialog.dismiss();
            }
        });

    }

}
