package com.katadigital.owtel.models;

/**
 * Created by dcnc123 on 11/28/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressList {

    @SerializedName("country_name")
    @Expose
    private String countryName;

    @SerializedName("country_id")
    @Expose
    private String countryID;

    @SerializedName("city_id")
    @Expose
    private String cityId;

    @SerializedName("city_name")
    @Expose
    private String cityName;

    @SerializedName("state_id")
    @Expose
    private String stateId;

    @SerializedName("state_name")
    @Expose
    private String stateName;

    @SerializedName("area_code")
    @Expose
    private Object areaCode;

    /**
     *
     * @return
     * The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     *
     * @param cityId
     * The city_id
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     *
     * @return
     * The stateId
     */
    public String getStateId() {
        return stateId;
    }

    /**
     *
     * @param stateId
     * The state_id
     */
    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    /**
     *
     * @return
     * The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     *
     * @param cityName
     * The city_name
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     *
     * @return
     * The areaCode
     */
    public Object getAreaCode() {
        return areaCode;
    }

    /**
     *
     * @param areaCode
     * The area_code
     */
    public void setAreaCode(Object areaCode) {
        this.areaCode = areaCode;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
