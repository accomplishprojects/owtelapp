package com.katadigital.owtel.modules.main.contacts.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.kata.phone.R;

public class AllFieldActivity extends Activity implements OnClickListener {

    public static int number;
    public static ArrayList<Button> spinnerbtn = new ArrayList<Button>();
    public static int spinnerArrayID;
    public static ArrayList<Integer> typeID;
    public static boolean custom;

    int ID_CUSTOM = 3333;

    LinearLayout holderfield;
    LayoutInflater lyInflaterForPanel_custom;
    LinearLayout lLayoutPanel_custom;

    int index;
    boolean isActive = false;
    boolean isDone = false;
    public static boolean noCustom = false;
    EditText editText;
    ImageView ivCheck;

    String leftitem;
    String rightItem;

    ArrayList<ImageView> arrayIv = new ArrayList<ImageView>();
    int getIndexCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allfield_layout);
        initActionBar();
        initData();
    }

    private void initActionBar() {
        leftitem = getResources().getString(R.string.cancel_string);
        rightItem = getResources().getString(R.string.done_string);
        CustomActionBar.viewCustomActionBar(this, leftitem, "", "");
    }

    private void initData() {

        arrayIv.clear();

        holderfield = (LinearLayout) findViewById(R.id.layout_holder_field);
        lyInflaterForPanel_custom = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lLayoutPanel_custom = (LinearLayout) lyInflaterForPanel_custom.inflate(
                R.layout.allfield_item, null);

        String[] myResArray = getResources().getStringArray(spinnerArrayID);

        for (index = 0; index < myResArray.length; index++) {

            LayoutInflater lyInflaterForPanel = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout lLayoutPanel = (LinearLayout) lyInflaterForPanel
                    .inflate(R.layout.allfield_item, null);

            LinearLayout layout = (LinearLayout) lLayoutPanel
                    .findViewById(R.id.layout_fied_click);
            TextView text = (TextView) lLayoutPanel
                    .findViewById(R.id.txt_field);
            ivCheck = (ImageView) lLayoutPanel.findViewById(R.id.iv_check);
            layout.setId(index);
            text.setText(myResArray[index]);
            ivCheck.setVisibility(View.INVISIBLE);
            ivCheck.setId(index);
            arrayIv.add(ivCheck);

            String nameBtn = spinnerbtn.get(number).getText().toString();

            if (nameBtn.equalsIgnoreCase(myResArray[index])) {
                ivCheck.setVisibility(View.VISIBLE);
                getIndexCheck = index;
            }

            text.setTypeface(NewUtil.getFontRoman(this));
            text.setTextSize(NewUtil.gettxtSize());
            layout.setOnClickListener(this);

            holderfield.addView(lLayoutPanel);
        }
        if (noCustom) {
            LinearLayout layout_custom = (LinearLayout) lLayoutPanel_custom
                    .findViewById(R.id.layout_fied_click);
            TextView text_custom = (TextView) lLayoutPanel_custom
                    .findViewById(R.id.txt_field);
            ivCheck = (ImageView) layout_custom.findViewById(R.id.iv_check);
            ivCheck.setVisibility(View.INVISIBLE);

            text_custom.setText(getResources().getString(R.string.add_custom));

            text_custom.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
            text_custom.setTypeface(NewUtil.getFontRoman(this));
            text_custom.setTextSize(NewUtil.gettxtSize());

            layout_custom.setOnClickListener(this);
            layout_custom.setId(ID_CUSTOM);
            holderfield.addView(lLayoutPanel_custom);
        }

    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(0, R.anim.slide_out_down);
        // super.onBackPressed();
    }

    @Override
    public void onClick(final View v) {

        arrayIv.get(getIndexCheck).setVisibility(View.INVISIBLE);

        if (v.getId() == ID_CUSTOM) {

            if (!isActive) {
                LayoutInflater lyInflaterForPanel_editext = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout lLayoutPanel_edit = (LinearLayout) lyInflaterForPanel_editext
                        .inflate(R.layout.text_field_layout, null);

                editText = (EditText) lLayoutPanel_edit
                        .findViewById(R.id.txtField);

                LayoutParams lparams = new LayoutParams(
                        LayoutParams.FILL_PARENT, 100);
                editText.setLayoutParams(lparams);
                editText.setBackgroundColor(Color.WHITE);

                editText.setHint(getResources().getString(R.string.add_custom));

                editText.setTypeface(NewUtil.getFontRoman(this));
                editText.setTextSize(NewUtil.gettxtSize());
                editText.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_CAP_WORDS);

                editText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence charSequence,
                                                  int i, int i2, int i3) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i,
                                              int i2, int i3) {

                        if (charSequence.length() > 5) {

                            View view = CustomActionBar.viewCustomActionBar(
                                    AllFieldActivity.this, leftitem, "",
                                    rightItem);
                            view.setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View id) {
                                    spinnerbtn.get(number).setText(
                                            editText.getText().toString()
                                                    .trim());
                                    onBackPressed();
                                }
                            });

                        } else {

                            arrayIv.get(getIndexCheck).setVisibility(
                                    View.INVISIBLE);
                            CustomActionBar.viewCustomActionBar(
                                    AllFieldActivity.this, leftitem, "", "");
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {

                    }
                });

                holderfield.addView(lLayoutPanel_edit);
                isActive = true;

            }

        } else {

            getIndexCheck = v.getId();
            arrayIv.get(v.getId()).setVisibility(View.VISIBLE);

            String rightItem = getResources().getString(R.string.done_string);
            String leftitem = getResources().getString(R.string.cancel_string);

            View view = CustomActionBar.viewCustomActionBar(this, leftitem, "",
                    rightItem);
            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View id) {
                    String[] myResArray = getResources().getStringArray(
                            spinnerArrayID);

                    System.out.println("JM vvv " + v.getId());

                    spinnerbtn.get(number).setText(myResArray[v.getId()]);
                    spinnerbtn.get(number).setTag(v.getId());
                    onBackPressed();
                }
            });

        }

    }

}
