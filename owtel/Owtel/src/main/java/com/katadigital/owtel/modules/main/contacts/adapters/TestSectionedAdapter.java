package com.katadigital.owtel.modules.main.contacts.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.util.NewUtil;
import com.katadigital.ui.pinnedheader.SectionedBaseAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestSectionedAdapter extends SectionedBaseAdapter {

    Activity activity;
    ArrayList<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, List<Integer>> listDataWithNumber = new HashMap<String, List<Integer>>();

    public TestSectionedAdapter(ArrayList<String> header,
                                HashMap<String, List<String>> child) {
        listDataHeader = header;
        listDataChild = child;
    }

    public TestSectionedAdapter(ArrayList<String> header,
                                HashMap<String, List<String>> child,
                                HashMap<String, List<Integer>> withNum) {
        listDataHeader = header;
        listDataChild = child;
        listDataWithNumber = withNum;
    }

    public TestSectionedAdapter(Activity activity) {
        this.activity = activity;
    }

    public
    TestSectionedAdapter(Activity activity, ArrayList<String> header,
                                HashMap<String, List<String>> child) {
        this.activity = activity;
        listDataHeader = header;
        listDataChild = child;
    }

    @Override
    public Object getItem(int section, int position) {
        return position;
    }

    @Override
    public long getItemId(int section, int position) {
        return position;
    }

    @Override
    public int getSectionCount() {
        return listDataHeader.size();
        // return 7;
    }

    @Override
    public int getCountForSection(int section) {
        return listDataChild.get(listDataHeader.get(section)).size();
        // return 15;
    }

    @Override
    public View getItemView(int section, int position, View convertView,
                            ViewGroup parent) {
        LinearLayout layout = null;
        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // layout = (LinearLayout) inflator.inflate(R.layout.list_item,
            // null);
            layout = (LinearLayout) inflator.inflate(R.layout.row_item, parent,
                    false);
        } else {
            layout = (LinearLayout) convertView;
        }

        String items = "";
        // items =
        // this._dataChild.get(this._listDataHeader.get(groupPosition)).get(childPosition);

        items = listDataChild.get(listDataHeader.get(section)).get(position);

        if (items == null) {
            items = activity.getResources().getString(R.string.string_noname);
        }

        // ((TextView) layout.findViewById(R.id.textItem)).setText("Section " +
        // sections + " Item " + position);
        TextView cont = (TextView) layout.findViewById(R.id.fake_textview);
        cont.setText(items);

//        cont.setTypeface(NewUtil.getFontRoman(activity));
        cont.setTextSize(NewUtil.gettxtSize());

        if (listDataWithNumber.size() != 0) {
            if (listDataWithNumber.get(listDataHeader.get(section)).get(
                    position) == 1) {
                cont.setTextColor(Color.BLACK);
            } else
                cont.setTextColor(Color.GRAY);
        }
        return layout;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView,
                                     ViewGroup parent) {
        LinearLayout layout = null;
        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // layout = (LinearLayout) inflator.inflate(R.layout.header_item,
            // null);
            layout = (LinearLayout) inflator.inflate(R.layout.row_section,
                    parent, false);


        } else {
            layout = (LinearLayout) convertView;
        }

        ((TextView) layout.findViewById(R.id.fake_textview)).setText(listDataHeader.get(section));
//        ((TextView) layout.findViewById(R.id.fake_textview)).setTypeface(NewUtil.getFontBold(activity));
        ((TextView) layout.findViewById(R.id.fake_textview)).setTextSize(NewUtil.gettxtSize());
        ((TextView) layout.findViewById(R.id.fake_textview)).setText(listDataHeader.get(section));

        return layout;
    }

}
