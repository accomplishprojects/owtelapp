package com.katadigital.owtel.security;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.modules.billing.response.CallAndTextSubscriptionWrapper;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Omar Matthew Reyes on 4/18/16.
 * Class for payment and billing
 * Handles sensitive data
 * Call new instance for every usage
 */
public class PaymentHelper {
    private final String TAG = "PaymentHelper";
    private Context context;
    private Gson gson;

    /**
     * Call PaymentHelper instance
     *
     * @param context Context
     */
    public PaymentHelper(Context context) {
        this.context = context;
        gson = new Gson();
    }

    /**
     * Get encrypted JSON Object for process user
     *
     * @param email                   String
     * @param pass                    String
     * @param number                  String
     * @param lockId                  String
     * @param promoCode               String
     * @param subscriptionId          String
     * @param subscriptionPrice       double
     * @param iddId                   String
     * @param iddPrice                double
     * @param totalPrice              double
     * @param cardNumber              String
     * @param cardHolder              String
     * @param cardCvv                 String
     * @param cardExpiryMonth         String
     * @param cardExpiryYear          String
     * @param billingAddressStreet    String
     * @param billingAddressCityId    String
     * @param billingAddressStateId   String
     * @param billingAddressCountryId String
     * @param billingAddressZipCode   String
     * @return String
     */
    public String getSecuredJsonRegisterUser(String email, String pass, String number, String lockId,
                                             String promoCode, String subscriptionId, double subscriptionPrice,
                                             String iddId, double iddPrice, double totalPrice,
                                             String cardNumber, String cardHolder, String cardCvv,
                                             String cardExpiryMonth, String cardExpiryYear,
                                             String billingAddressStreet, String billingAddressCityId,
                                             String billingAddressStateId, String billingAddressCountryId,
                                             String billingAddressZipCode) {
        String jsonObject = "{\"" +
                API.REGISTER_EMAIL + "\":\"" + email + "\",\"" +
                API.REGISTER_PASSWORD + "\":\"" + pass + "\",\"" +
                API.REGISTER_NUMBER + "\":\"" + number + "\",\"" +
                API.REGISTER_LOCK_ID + "\":\"" + lockId + "\",\"" +
                API.REGISTER_PROMO_CODE + "\":\"" + promoCode + "\",\"" +
                API.PAYMENT_SUBSCRIPTION_ID + "\":\"" + subscriptionId + "\",\"" +
                API.PAYMENT_SUBSCRIPTION_PRICE + "\":\"" + subscriptionPrice + "\",\"" +
                API.PAYMENT_IDD_ID + "\":\"" + iddId + "\",\"" +
                API.PAYMENT_IDD_PRICE + "\":\"" + iddPrice + "\",\"" +
                API.PAYMENT_TOTAL_PRICE + "\":\"" + totalPrice + "\",\"" +
                API.PAYMENT_CARD_NUMBER + "\":\"" + cardNumber + "\",\"" +
                API.PAYMENT_CARD_HOLDER + "\":\"" + cardHolder + "\",\"" +
                API.PAYMENT_CARD_CVV + "\":\"" + cardCvv + "\",\"" +
                API.PAYMENT_CARD_EXP_MONTH + "\":\"" + cardExpiryMonth + "\",\"" +
                API.PAYMENT_CARD_EXP_YEAR + "\":\"" + cardExpiryYear + "\",\"" +
                API.PAYMENT_BILL_ADD_STREET + "\":\"" + billingAddressStreet + "\",\"" +
                API.PAYMENT_BILL_ADD_CITY_ID + "\":\"" + billingAddressCityId + "\",\"" +
                API.PAYMENT_BILL_ADD_STATE_ID + "\":\"" + billingAddressStateId + "\",\"" +
                API.PAYMENT_BILL_ADD_COUNTRY_ID + "\":\"" + billingAddressCountryId + "\",\"" +
                API.PAYMENT_BILL_ADD_ZIP_CODE + "\":\"" + billingAddressZipCode + "\"}";
        return SecuredString.encryptData(jsonObject);
    }

    /**
     * Get encrypted JSON Object for promo code payment
     *
     * @param userEmail                  String
     * @param subscriptionId          String
     * @param subscriptionPrice       double
     * @param iddId                   String
     * @param iddPrice                double
     * @param totalPrice              double
     * @param cardNumber              String
     * @param cardHolder              String
     * @param cardCvv                 String
     * @param cardExpiryMonth         String
     * @param cardExpiryYear          String
     * @param billingAddressStreet    String
     * @param billingAddressCityId    String
     * @param billingAddressStateId   String
     * @param billingAddressCountryId String
     * @param billingAddressZipCode   String
     * @return String
     */
    public String getSecuredJsonPromoCodePayment(String userEmail, String subscriptionId, double subscriptionPrice,
                                             String iddId, double iddPrice, double totalPrice,
                                             String cardNumber, String cardHolder, String cardCvv,
                                             String cardExpiryMonth, String cardExpiryYear,
                                             String billingAddressStreet, String billingAddressCityId,
                                             String billingAddressStateId, String billingAddressCountryId,
                                             String billingAddressZipCode) {
        String jsonObject = "{\"" +
                API.REGISTER_EMAIL + "\":\"" + userEmail + "\",\"" +
                API.PAYMENT_SUBSCRIPTION_ID + "\":\"" + subscriptionId + "\",\"" +
                API.PAYMENT_SUBSCRIPTION_PRICE + "\":\"" + subscriptionPrice + "\",\"" +
                API.PAYMENT_IDD_ID + "\":\"" + iddId + "\",\"" +
                API.PAYMENT_IDD_PRICE + "\":\"" + iddPrice + "\",\"" +
//                API.PAYMENT_TOTAL_PRICE + "\":\"" + totalPrice + "\",\"" +
                API.PAYMENT_CARD_NUMBER + "\":\"" + cardNumber + "\",\"" +
                API.PAYMENT_CARD_HOLDER + "\":\"" + cardHolder + "\",\"" +
                API.PAYMENT_CARD_CVV + "\":\"" + cardCvv + "\",\"" +
                API.PAYMENT_CARD_EXP_MONTH + "\":\"" + cardExpiryMonth + "\",\"" +
                API.PAYMENT_CARD_EXP_YEAR + "\":\"" + cardExpiryYear + "\",\"" +
                API.PAYMENT_BILL_ADD_STREET + "\":\"" + billingAddressStreet + "\",\"" +
                API.PAYMENT_BILL_ADD_CITY_ID + "\":\"" + billingAddressCityId + "\",\"" +
                API.PAYMENT_BILL_ADD_STATE_ID + "\":\"" + billingAddressStateId + "\",\"" +
                API.PAYMENT_BILL_ADD_COUNTRY_ID + "\":\"" + billingAddressCountryId + "\",\"" +
                API.PAYMENT_BILL_ADD_ZIP_CODE + "\":\"" + billingAddressZipCode + "\",\"" +
                API.PAYMENT_TYPE + "\":\"" + "subscription" + "\"}";
        return SecuredString.encryptData(jsonObject);
    }

    /**
     * Get encrypted JSON Object for IDD process
     *
     * @param email                   String
     * @param iddId                   String
     * @param iddPrice                double
     * @param cardNumber              String
     * @param cardHolder              String
     * @param cardCvv                 String
     * @param cardExpiryMonth         String
     * @param cardExpiryYear          String
     * @param billingAddressStreet    String
     * @param billingAddressCityId    String
     * @param billingAddressStateId   String
     * @param billingAddressCountryId String
     * @param billingAddressZipCode   String
     * @return String
     */
    public String getSecuredJsonProcessIdd(String email, String iddId, double iddPrice,
                                           String cardNumber, String cardHolder, String cardCvv,
                                           String cardExpiryMonth, String cardExpiryYear,
                                           String billingAddressStreet, String billingAddressCityId,
                                           String billingAddressStateId, String billingAddressCountryId,
                                           String billingAddressZipCode) {
        String jsonObject = "{\"" +
                API.REGISTER_EMAIL + "\":\"" + email + "\",\"" +
                API.PAYMENT_IDD_ID + "\":\"" + iddId + "\",\"" +
                API.PAYMENT_IDD_PRICE + "\":\"" + iddPrice + "\",\"" +
                API.PAYMENT_CARD_NUMBER + "\":\"" + cardNumber + "\",\"" +
                API.PAYMENT_CARD_HOLDER + "\":\"" + cardHolder + "\",\"" +
                API.PAYMENT_CARD_CVV + "\":\"" + cardCvv + "\",\"" +
                API.PAYMENT_CARD_EXP_MONTH + "\":\"" + cardExpiryMonth + "\",\"" +
                API.PAYMENT_CARD_EXP_YEAR + "\":\"" + cardExpiryYear + "\",\"" +
                API.PAYMENT_BILL_ADD_STREET + "\":\"" + billingAddressStreet + "\",\"" +
                API.PAYMENT_BILL_ADD_CITY_ID + "\":\"" + billingAddressCityId + "\",\"" +
                API.PAYMENT_BILL_ADD_STATE_ID + "\":\"" + billingAddressStateId + "\",\"" +
                API.PAYMENT_BILL_ADD_COUNTRY_ID + "\":\"" + billingAddressCountryId + "\",\"" +
                API.PAYMENT_BILL_ADD_ZIP_CODE + "\":\"" + billingAddressZipCode + "\"}\"";
        return SecuredString.encryptData(jsonObject);
    }

    /**
     * Get encrypted JSON Object for Subscription process
     *
     * @param email                   String
     * @param subscriptionId          String
     * @param subscriptionPrice       double
     * @param cardNumber              String
     * @param cardHolder              String
     * @param cardCvv                 String
     * @param cardExpiryMonth         String
     * @param cardExpiryYear          String
     * @param billingAddressStreet    String
     * @param billingAddressCityId    String
     * @param billingAddressStateId   String
     * @param billingAddressCountryId String
     * @param billingAddressZipCode   String
     * @return String
     */
    public String getSecuredJsonProcessSubscription(String email, String subscriptionId, double subscriptionPrice,
                                                    String cardNumber, String cardHolder, String cardCvv,
                                                    String cardExpiryMonth, String cardExpiryYear,
                                                    String billingAddressStreet, String billingAddressCityId,
                                                    String billingAddressStateId, String billingAddressCountryId,
                                                    String billingAddressZipCode) {

        CallAndTextSubscriptionWrapper callAndTextSubscriptionWrapper = new CallAndTextSubscriptionWrapper();
        callAndTextSubscriptionWrapper.setEmail_address(email);
        callAndTextSubscriptionWrapper.setSubscription_id(subscriptionId);
        callAndTextSubscriptionWrapper.setSubscription_price(subscriptionPrice);
        callAndTextSubscriptionWrapper.setCard_number(cardNumber);
        callAndTextSubscriptionWrapper.setCard_holder(cardHolder);
        callAndTextSubscriptionWrapper.setCc_cvv(cardCvv);
        callAndTextSubscriptionWrapper.setCc_month(cardExpiryMonth);
        callAndTextSubscriptionWrapper.setCc_year(cardExpiryYear);
        callAndTextSubscriptionWrapper.setStreet(billingAddressStreet);
        callAndTextSubscriptionWrapper.setCity_id(billingAddressCityId);
        callAndTextSubscriptionWrapper.setState_id(billingAddressStateId);
        callAndTextSubscriptionWrapper.setCountry_id(billingAddressCountryId);
        callAndTextSubscriptionWrapper.setZip_code(billingAddressZipCode);
//        callAndTextSubscriptionWrapper.setPayment_type(paymentType);
        String jsonObject = gson.toJson(callAndTextSubscriptionWrapper);
        String jsonEncrypt = SecuredString.encryptData(jsonObject);
        if(GlobalValues.DEBUG) {
            Log.e("Json String", jsonObject);
            Log.e("Json Encrypt", jsonEncrypt);
        }
        return jsonEncrypt;
    }

    public String getSecuredJsonProcessTopUp(String email, String topUpIddId, double topUpIddPrice,
                                                    String cardNumber, String cardHolder, String cardCvv,
                                                    String cardExpiryMonth, String cardExpiryYear,
                                                    String billingAddressStreet, String billingAddressCityId,
                                                    String billingAddressStateId, String billingAddressCountryId,
                                                    String billingAddressZipCode) {

        CallAndTextSubscriptionWrapper callAndTextSubscriptionWrapper = new CallAndTextSubscriptionWrapper();
        callAndTextSubscriptionWrapper.setEmail_address(email);
        callAndTextSubscriptionWrapper.setIdd_top_up_id(topUpIddId);
        callAndTextSubscriptionWrapper.setIdd_top_up_price(topUpIddPrice);
        callAndTextSubscriptionWrapper.setCard_number(cardNumber);
        callAndTextSubscriptionWrapper.setCard_holder(cardHolder);
        callAndTextSubscriptionWrapper.setCc_cvv(cardCvv);
        callAndTextSubscriptionWrapper.setCc_month(cardExpiryMonth);
        callAndTextSubscriptionWrapper.setCc_year(cardExpiryYear);
        callAndTextSubscriptionWrapper.setStreet(billingAddressStreet);
        callAndTextSubscriptionWrapper.setCity_id(billingAddressCityId);
        callAndTextSubscriptionWrapper.setState_id(billingAddressStateId);
        callAndTextSubscriptionWrapper.setCountry_id(billingAddressCountryId);
        callAndTextSubscriptionWrapper.setZip_code(billingAddressZipCode);
//        callAndTextSubscriptionWrapper.setPayment_type(paymentType);
        String jsonObject = gson.toJson(callAndTextSubscriptionWrapper);
        String jsonEncrypt = SecuredString.encryptData(jsonObject);
        if(GlobalValues.DEBUG) {
            Log.e("Json String", jsonObject);
            Log.e("Json Encrypt", jsonEncrypt);
        }
        return jsonEncrypt;
    }

    /**
     * Generate secured billing details to be sent to API
     *
     * @param cardNumber      String
     * @param cardCvv         String
     * @param cardExpiryMonth String
     * @param cardExpiryYear  String
     * @param totalPrice      double
     * @return String
     */
    public String getSecuredBillingDetails(String cardNumber, String cardHolder, String cardCvv,
                                           String cardExpiryMonth, String cardExpiryYear,
                                           String totalPrice) {
        String bill = cardNumber + ":" + cardCvv + ":" + cardHolder + ":" +
                cardExpiryMonth + "/" + cardExpiryYear + ":" +
                totalPrice;
        return SecuredString.encryptData(bill);
    }

    /**
     * Generate secured key for encryption
     *
     * @return String
     */
    public String getGateKeeper() {
        final String key = "0wt3Ll!ne";
        return generateMD5Hash(generateMD5Hash(key));
    }

    /**
     * Generate MD5 hash from String source
     *
     * @param source String
     * @return String
     */
    @Nullable
    public String generateMD5Hash(String source) {
        final String algorithm = "MD5";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.update(source.getBytes());
            return new BigInteger(1, messageDigest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            if (GlobalValues.DEBUG) Log.e(TAG, "generateMD5Hash " + e);
            return null;
        }
    }
}
