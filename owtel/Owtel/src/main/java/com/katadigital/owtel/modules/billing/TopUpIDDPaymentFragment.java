package com.katadigital.owtel.modules.billing;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.security.SecuredString;
import com.katadigital.owtel.utils.API;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.SharedPreferenceHelper;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.modules.billing.response.PaymentResponse;
import com.katadigital.owtel.security.PaymentHelper;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.portsip.helper.Network;
import com.katadigital.portsip.utilities.network.NetworkHelper;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.katadigital.ui.adapter.AddressAdapter;
import com.katadigital.ui.adapter.IddAdapter;

import org.parceler.Parcels;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit.GsonConverterFactory;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jayhar Vallejos on 5/2/2016.
 * IDD top-up page from Settings
 */
public class TopUpIDDPaymentFragment extends Fragment {
    private final static String TAG = "TopUpIDDPaymentFragment";
    private String CLASS_TAG = this.getClass().getSimpleName();
    private MainActivity mainActivity;

    @Bind(R.id.idd_credit_tv)
    TextView tvIDDCredit;
    @Bind(R.id.top_up_amount_idd)
    Spinner spinnerTopUpAmount;
    @Bind(R.id.et_card_number)
    EditText etCardNumber;
    @Bind(R.id.et_card_holder)
    EditText etCardHolder;
    @Bind(R.id.spinner_cc_month)
    Spinner spinnerCardExpireMonth;
    @Bind(R.id.spinner_cc_year)
    Spinner spinnerCardExpireYear;
    @Bind(R.id.et_cvv)
    EditText etCardCVV;
    @Bind(R.id.et_street)
    EditText etAddressStreet;
    @Bind(R.id.et_city)
    EditText etCity;
    @Bind(R.id.et_state)
    EditText etState;
    @Bind(R.id.et_zip_code)
    EditText etAddressZipCode;
    @Bind(R.id.spinner_country)
    Spinner spinnerAddressCountry;
    @Bind(R.id.btn_back)
    ImageButton btnBack;
    @Bind(R.id.btn_register_payment_continue)
    Button btnSubmitPayment;

    private ArrayList<String> listYear = new ArrayList<>();
    private SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
    private List<AddressList> countryList = new ArrayList<>();
    //    , stateList = new ArrayList<>(), cityList = new ArrayList<>();
    private ArrayList<IddList> listIdd = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_idd_topup, container, false);
        ButterKnife.bind(this, view);
        SharedPreferenceManager.init(getActivity());
        NetworkHelper.init();

        mainActivity = (MainActivity) getActivity();
//        tvIDDCredit.setText(StringFormatter.formatCurrency(Constants.CURRENCY, SharedPreferenceHelper.getDouble(preferencesUser, Constants.PREFS_USER_IDD_CREDITS, 0.0)));
        tvIDDCredit.setText(Constants.CURRENCY + SharedPreferenceManager.getSharedPreferenceManagerInstance().getIDDCredits());
        // Fill spinner credit card expiry month
        spinnerCardExpireMonth.setAdapter(generateDefaultAdapter(getResources().getStringArray(R.array.register_payment_card_months)));
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        listYear.add(getString(R.string.register_payment_card_year));
        for (int i = 0; i < 10; i++) {
            listYear.add(Integer.toString(currentYear + i));
        }
        ArrayAdapter<String> spinnerCcYear = new ArrayAdapter<>(getActivity(),
                R.layout.simple_list_item, listYear);
        spinnerCardExpireYear.setAdapter(spinnerCcYear);
        countryList = Parcels.unwrap(getArguments().getParcelable("countries"));
        listIdd = Parcels.unwrap(getArguments().getParcelable("top_up"));
        spinnerTopUpAmount.setAdapter(new IddAdapter(listIdd));
        spinnerAddressCountry.setAdapter(new AddressAdapter(countryList, GlobalValues.GET_COUNTRY));
        spinnerAddressCountry.post(new Runnable() {
            @Override
            public void run() {
                spinnerAddressCountry.setSelection(getIndex(spinnerAddressCountry, "USA"));
            }
        });
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinnerAddressCountry);

            // Set popupWindow height to 500px
            popupWindow.setHeight(500);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
        /**HAYANG*/
        new ApiHelper(getActivity()).generateCityState(etAddressZipCode,
                etCity, etState, spinnerAddressCountry, countryList,
                mainActivity.apiService);
        return view;
    }

//    @OnItemSelected(R.id.spinner_country)
//    public void onItemSelectCountry(int position) {
//        if(GlobalValues.DEBUG)Log.i(TAG, "Spinner Country position " + position);
//        if (position > 0) {
//            mainActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
//            mainActivity.mProgressDialog.show();
//            ApiHelper.doGetAddressList(mainActivity.mProgressDialog, mainActivity, GlobalValues.GET_STATE, "", countryList.get(position).getCountryId()).subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Subscriber<List<AddressList>>() {
//                        @Override
//                        public void onCompleted() {
//                            spinnerAddressState.setEnabled(true);
//                            mainActivity.mProgressDialog.dismiss();
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//
//                        @Override
//                        public void onNext(List<AddressList> addresses) {
//                            stateList = new ArrayList<>(addresses);
//                            Collections.copy(stateList, addresses);
//                            spinnerAddressState.setAdapter(new AddressAdapter(addresses, GlobalValues.GET_STATE));
//                        }
//                    });
//        } else {
//            spinnerAddressState.setEnabled(false);
//            spinnerAddressState.setSelection(0);
//            spinnerAddressCity.setEnabled(false);
//            spinnerAddressCity.setSelection(0);
//        }
//    }

//    @OnItemSelected(R.id.spinner_state)
//    public void onItemSelectState(int position) {
//        if (position > 0) {
//            mainActivity.mProgressDialog.setMessage(getResources().getString(R.string.processing));
//            mainActivity.mProgressDialog.show();
//            ApiHelper.doGetAddressList(mainActivity.mProgressDialog, mainActivity, GlobalValues.GET_CITY, stateList.get(position).getStateId(), countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Subscriber<List<AddressList>>() {
//                        @Override
//                        public void onCompleted() {
//                            spinnerAddressCity.setEnabled(true);
//                            mainActivity.mProgressDialog.dismiss();
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//
//                        }
//
//                        @Override
//                        public void onNext(List<AddressList> addresses) {
//                            cityList = new ArrayList<>(addresses);
//                            spinnerAddressCity.setAdapter(new AddressAdapter(addresses, GlobalValues.GET_CITY));
//                        }
//                    });
//        } else {
//            spinnerAddressCity.setEnabled(false);
//            spinnerAddressCity.setSelection(0);
//        }
//    }

//    @OnItemSelected(R.id.spinner_city)
//    public void onItemSelectCity(int position) {
//        if (position > 0) Log.i(TAG, "Selected City: " + cityList.get(position).getCity());
//    }

    @OnClick(R.id.btn_register_payment_continue)
    public void onClickContinue() {
        mainActivity.hideSoftInput(btnSubmitPayment);
        if (validateFieldValues()) {
            PaymentHelper paymentHelper = new PaymentHelper(mainActivity);
            String jsonEncrypt = paymentHelper.getSecuredJsonProcessTopUp(preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty"),
                    listIdd.get(spinnerTopUpAmount.getSelectedItemPosition()).getIddId(),
                    Double.parseDouble(listIdd.get(spinnerTopUpAmount.getSelectedItemPosition()).getIddReloadPrice().replace('$', ' ').trim()),
                    etCardNumber.getText().toString(), etCardHolder.getText().toString(), etCardCVV.getText().toString(),
                    spinnerCardExpireMonth.getSelectedItem().toString(), spinnerCardExpireYear.getSelectedItem().toString(), etAddressStreet.getText().toString(),
                    etCity.getText().toString(), etState.getText().toString(), countryList.get(spinnerAddressCountry.getSelectedItemPosition()).getCountryId(), etAddressZipCode.getText().toString());
            submitTopUpPayment(jsonEncrypt);
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_fields), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_back)
    public void back() {
        mainActivity.hideSoftInput(btnBack);
        mainActivity.popFragment();
    }

    /**
     * Check if all fields are valid
     *
     * @return boolean
     */
    private boolean validateFieldValues() {
        return isCreditCardValid(etCardNumber) &&
                isCreditCardHolderValid(etCardHolder) &&
                isCreditCardExpiryValid(spinnerCardExpireMonth, spinnerCardExpireYear) &&
                isCreditCardCvvValid(etCardCVV) &&
                isCreditCardAddressValid(etAddressStreet, etCity, etState,
                        spinnerAddressCountry, etAddressZipCode) &&
                isTopUpNotZero(spinnerTopUpAmount);
    }

    private boolean isTopUpNotZero(Spinner spinnerTopUpAmount) {
        return !(spinnerTopUpAmount.getSelectedItemPosition() == 0);
    }

    private boolean isCreditCardValid(EditText etCardNumber) {
        String cardNum = etCardNumber.getText().toString().trim();
        return !(cardNum.equals("") || cardNum.trim().isEmpty());
    }

    private boolean isCreditCardHolderValid(EditText creditCardHolder) {
        String cardHolder = creditCardHolder.getText().toString().trim();
        return !(cardHolder.equals("") || cardHolder.trim().isEmpty());
    }

    /**
     * Check if credit card expiry date is valid
     *
     * @param creditCardExpiryMonth Spinner
     * @param creditCardExpiryYear  Spinner
     * @return boolean
     */
    private boolean isCreditCardExpiryValid(Spinner creditCardExpiryMonth, Spinner creditCardExpiryYear) {
        return creditCardExpiryMonth.getSelectedItemPosition() > 0 &&
                creditCardExpiryYear.getSelectedItemPosition() > 0;
    }

    /**
     * Check if credit card cvv is valid
     *
     * @param creditCardCvv String
     * @return boolean
     */
    private boolean isCreditCardCvvValid(EditText creditCardCvv) {
        return creditCardCvv.getText().toString().trim().length() >= 3;
    }

    /**
     * Check if credit card billing address is valid
     *
     * @param etAddressStreet       EditText
     * @param etCity                EditText
     * @param etState               EditText
     * @param spinnerAddressCountry Spinner
     * @return boolean
     */
    private boolean isCreditCardAddressValid(EditText etAddressStreet, EditText etCity,
                                             EditText etState, Spinner spinnerAddressCountry,
                                             EditText etAddressZipCode) {
        return etAddressStreet.getText().toString().trim().length() > 0 &&
                etCity.getText().toString().trim().length() > 0 &&
                etState.getText().toString().trim().length() > 0 &&
                spinnerAddressCountry.getSelectedItemPosition() > 0 &&
                etAddressZipCode.getText().toString().trim().length() > 0;
    }

    /**
     * Generate default adapter for Spinner
     *
     * @param stringList String[]
     * @return ArrayAdapter<String>
     */
    private ArrayAdapter<String> generateDefaultAdapter(String[] stringList) {
        return new ArrayAdapter<>(getActivity(), R.layout.simple_list_item, stringList);
    }

    public void submitTopUpPayment(String jsonEncrypt) {
        ProgressDialog mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(getString(R.string.processing));
        mProgressDialog.show();

        Log.i(TAG, API.ENCRYPTED_OBJECT + " idd ");
        Log.i(TAG, API.ENCRYPTED_OBJECT + " encrypted " + jsonEncrypt);
        Log.i(TAG, API.ENCRYPTED_OBJECT + " decrypted " + SecuredString.decryptData(jsonEncrypt));

        retrofit.Retrofit retrofit = new retrofit.Retrofit.Builder()
                .baseUrl(API.URL_TEST)
                .client(NetworkHelper.getNetworkHelperInstance().getHttpClientForTimeOut())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Observable<PaymentResponse> paymentResponseObservable = service.submitSubscription(jsonEncrypt, "idd");
        paymentResponseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Subscriber<PaymentResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.e(TAG, "onCompleted: ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        DialogBuilder.showAlertDialog(getActivity(),
                                "Request Timeout", "Please try again later.",
                                OwtelAppController.getInstance().getString(R.string.string_ok));
                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Log.e(CLASS_TAG + " onError: ", e.toString());
                    }

                    @Override
                    public void onNext(PaymentResponse paymentResponse) {
                        Log.e(CLASS_TAG, "onNext: ");
                        if (paymentResponse.getStatus().equalsIgnoreCase("Success")) {
                            Bundle arguments = new Bundle();
                            arguments.putString(Constants.BUNDLE_PAYMENT_MESSAGE, getString(R.string.payment_successful_idd_credits) + "\n" +
                                    StringFormatter.formatCurrency(Constants.CURRENCY, paymentResponse.getIddCredits()));
                            PaymentSuccessFragment paymentSuccessFragment = new PaymentSuccessFragment();
                            paymentSuccessFragment.setArguments(arguments);
                            mainActivity.changeFragment(paymentSuccessFragment, false);
                            if (mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                        } else {
                            DialogBuilder.showAlertDialog(getActivity(),
                                    paymentResponse.getStatus(), paymentResponse.getMessage(),
                                    OwtelAppController.getInstance().getString(R.string.string_ok));

                            Log.e(TAG, "onNext: " + paymentResponse.getMessage());
                            if (mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                        }
                    }
                });
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().toLowerCase().contains(myString.toLowerCase())) {
                index = i;
                break;
            }
        }
        return index;
    }
}
