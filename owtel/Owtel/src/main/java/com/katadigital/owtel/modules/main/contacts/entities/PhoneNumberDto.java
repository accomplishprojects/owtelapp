package com.katadigital.owtel.modules.main.contacts.entities;

public class PhoneNumberDto {

    int id;
    String numberType;
    String number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    public String getNumber() {
        return number.replaceAll(" ", "");
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
