package com.katadigital.owtel.models;

/**
 * Created by dcnc123 on 11/16/16.
 */

import javax.annotation.Generated;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoCodeObj {

    @SerializedName("promo_code_id")
    @Expose
    private String promoCodeId;
    @SerializedName("promo_code")
    @Expose
    private String promoCode;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("used_by")
    @Expose
    private String usedBy;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_used")
    @Expose
    private String dateUsed;
    @SerializedName("owtel_number")
    @Expose
    private String owtelNumber;
    @SerializedName("email_address")
    @Expose
    private String emailAddress;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("middlename")
    @Expose
    private String middlename;
    @SerializedName("lastname")
    @Expose
    private String lastname;

    /**
     *
     * @return
     * The promoCodeId
     */
    public String getPromoCodeId() {
        return promoCodeId;
    }

    /**
     *
     * @param promoCodeId
     * The promo_code_id
     */
    public void setPromoCodeId(String promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    /**
     *
     * @return
     * The promoCode
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     *
     * @param promoCode
     * The promo_code
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The usedBy
     */
    public String getUsedBy() {
        return usedBy;
    }

    /**
     *
     * @param usedBy
     * The used_by
     */
    public void setUsedBy(String usedBy) {
        this.usedBy = usedBy;
    }

    /**
     *
     * @return
     * The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     *
     * @param dateCreated
     * The date_created
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     *
     * @return
     * The dateUsed
     */
    public String getDateUsed() {
        return dateUsed;
    }

    /**
     *
     * @param dateUsed
     * The date_used
     */
    public void setDateUsed(String dateUsed) {
        this.dateUsed = dateUsed;
    }

    /**
     *
     * @return
     * The owtelNumber
     */
    public String getOwtelNumber() {
        return owtelNumber;
    }

    /**
     *
     * @param owtelNumber
     * The owtel_number
     */
    public void setOwtelNumber(String owtelNumber) {
        this.owtelNumber = owtelNumber;
    }

    /**
     *
     * @return
     * The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @param emailAddress
     * The email_address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     *
     * @return
     * The firstname
     */
    public Object getFirstname() {
        return firstname;
    }

    /**
     *
     * @param firstname
     * The firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     *
     * @return
     * The middlename
     */
    public Object getMiddlename() {
        return middlename;
    }

    /**
     *
     * @param middlename
     * The middlename
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     *
     * @return
     * The lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     *
     * @param lastname
     * The lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this).toString();
    }
}
