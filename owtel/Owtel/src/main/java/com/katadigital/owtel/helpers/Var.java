package com.katadigital.owtel.helpers;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import com.katadigital.owtel.modules.main.history.LogsFragment;
import com.katadigital.owtel.modules.main.history.LogsDetail;

public class Var {

    // keep track of camera capture intent
    public static int CAMERA_CAPTURE = 1;
    //keep track of cropping intent
    public static int PIC_CROP = 2;

    public static boolean addLike = false;
    public static boolean misscaltost = false;


    // PH
    private static String locValue = "+63";

    public static Activity activity;
    public static LogsFragment logsFragment;
    public static LogsDetail logActivity;

    public static FragmentActivity fragmentActivity;

    public static String localPhone = "Local Phone Account";
    public static String namePhone = "Phone";

    public static int numVar = 0;
    public static FragmentActivity logFragActivity;

    public static String getLocValue() {
        return locValue;
    }

    public static CharSequence setLocValue(String locValue) {
        return Var.locValue = (String) locValue.subSequence(3, locValue.length());
    }

}
