
package com.katadigital.owtel.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("sms_id")
    @Expose
    private String smsId;
    @SerializedName("thread_id")
    @Expose
    private String threadId;
    @SerializedName("sender")
    @Expose
    private String sender;
    @SerializedName("receiver")
    @Expose
    private String receiver;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sms_content")
    @Expose
    private String smsContent;
    @SerializedName("sms_date")
    @Expose
    private String smsDate;
    @SerializedName("route")
    @Expose
    private String route;
    /**
     * 
     * @return
     *     The smsId
     */
    public String getSmsId() {
        return smsId;
    }

    /**
     * 
     * @param smsId
     *     The sms_id
     */
    public void setSmsId(String smsId) {
        this.smsId = smsId;
    }

    /**
     * 
     * @return
     *     The threadId
     */
    public String getThreadId() {
        return threadId;
    }

    /**
     * 
     * @param threadId
     *     The thread_id
     */
    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    /**
     * 
     * @return
     *     The sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * 
     * @param sender
     *     The sender
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * 
     * @return
     *     The receiver
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * 
     * @param receiver
     *     The receiver
     */
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The smsContent
     */
    public String getSmsContent() {
        return smsContent;
    }

    /**
     * 
     * @param smsContent
     *     The sms_content
     */
    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    /**
     * 
     * @return
     *     The smsDate
     */
    public String getSmsDate() {
        return smsDate;
    }

    /**
     * 
     * @param smsDate
     *     The sms_date
     */
    public void setSmsDate(String smsDate) {
        this.smsDate = smsDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
}
