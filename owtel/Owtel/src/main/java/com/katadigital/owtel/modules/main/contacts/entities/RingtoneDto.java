package com.katadigital.owtel.modules.main.contacts.entities;

public class RingtoneDto {

    private String rintoneName;
    private int indexSeleced;

    public String getRintoneName() {
        return rintoneName;
    }

    public void setRintoneName(String rintoneName) {
        this.rintoneName = rintoneName;
    }

    public int getIndexSeleced() {
        return indexSeleced;
    }

    public void setIndexSeleced(int indexSeleced) {
        this.indexSeleced = indexSeleced;
    }

}
