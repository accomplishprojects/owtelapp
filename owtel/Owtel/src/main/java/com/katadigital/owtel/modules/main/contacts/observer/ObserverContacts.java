package com.katadigital.owtel.modules.main.contacts.observer;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.kata.phone.R;

import java.io.IOException;
import java.io.InputStream;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Omar Matthew Reyes on 3/16/16.
 */
public class ObserverContacts {
    private final String TAG = "ObserverContacts";
    private Context context;
    private String contactNumber;

    public ObserverContacts(Context context, String contactNumber) {
        this.context = context;
        this.contactNumber = contactNumber;
    }

    public Observable<Bitmap> getBitmapContactPicture(){
        Log.i(TAG, "getBitmapContactPicture()");
        return Observable.create(new Observable.OnSubscribe<Bitmap>() {
            @Override
            public void call(Subscriber<? super Bitmap> subscriber) {
                ContentResolver contentResolver = context.getContentResolver();
                String contactId = null;
                Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactNumber));

                String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

                Cursor cursor =
                        contentResolver.query(
                                uri,
                                projection,
                                null,
                                null,
                                null);

                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                    }
                    cursor.close();
                }

                Bitmap photo = BitmapFactory.decodeResource(context.getResources(), R.drawable.picture_unknown);

                try {
//                    Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId));
//                    Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);
//                    try {
//                        AssetFileDescriptor fd = context.getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
//                        photo = BitmapFactory.decodeStream(fd.createInputStream());
//                    } catch (IOException e) {
//                        Log.e(TAG, "getBitmapContactPicture() retrieveContactPhoto " + e);
//                    }
                    InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));

                    if (inputStream != null) {
                        photo = BitmapFactory.decodeStream(inputStream);
                    }

                    assert inputStream != null;
                    inputStream.close();

                } catch (IOException e) {
                    Log.e(TAG, "getBitmapContactPicture() retrieveContactPhoto " + e);
                }

                subscriber.onNext(photo);
                subscriber.onCompleted();
            }
        });
    }
}
