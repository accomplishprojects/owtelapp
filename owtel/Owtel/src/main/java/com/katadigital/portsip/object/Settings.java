package com.katadigital.portsip.object;

/**
 * Created by dcnc123 on 2/27/17.
 */

public class Settings {

    // Name of the keys in the Preferences XML file
    public static final String PREF_USERNAME = "username";
    public static final String PREF_PASSWORD = "password";
    public static final String PREF_SERVER = "server";
    public static final String PREF_DOMAIN = "domain";
    public static final String PREF_FROMUSER = "fromuser";
    public static final String PREF_PORT = "port";
    public static final String PREF_PROTOCOL = "protocol";
    public static final String PREF_WLAN = "wlan";
    public static final String PREF_3G = "3g";
    public static final String PREF_EDGE = "edge";
    public static final String PREF_VPN = "vpn";
    public static final String PREF_PREF = "pref";
    public static final String PREF_AUTO_ON = "auto_on";
    public static final String PREF_AUTO_ONDEMAND = "auto_on_demand";
    public static final String PREF_AUTO_HEADSET = "auto_headset";
    public static final String PREF_MWI_ENABLED = "MWI_enabled";
    public static final String PREF_REGISTRATION = "registration";
    public static final String PREF_NOTIFY = "notify";
    public static final String PREF_NODATA = "nodata";
    public static final String PREF_SIPRINGTONE = "sipringtone";
    public static final String PREF_SEARCH = "search";
    public static final String PREF_EXCLUDEPAT = "excludepat";
    public static final String PREF_EARGAIN = "eargain";
    public static final String PREF_MICGAIN = "micgain";
    public static final String PREF_HEARGAIN = "heargain";
    public static final String PREF_CODECS = "codecs_new";
    public static final String	DEFAULT_USERNAME = "";
    public static final String	DEFAULT_PASSWORD = "";
    public static final String	DEFAULT_SERVER = "pbxes.org";
    public static final String	DEFAULT_DOMAIN = "";
    public static final String	DEFAULT_FROMUSER = "";
    public static final String	DEFAULT_PORT = "5060";
    public static final String	DEFAULT_PROTOCOL = "tcp";
    public static final boolean	DEFAULT_WLAN = true;
    public static final boolean	DEFAULT_3G = false;
    public static final boolean	DEFAULT_EDGE = false;
    public static final String	DEFAULT_CODECS = null;

}
