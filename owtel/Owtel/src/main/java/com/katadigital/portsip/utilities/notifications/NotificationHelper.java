package com.katadigital.portsip.utilities.notifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.widget.RemoteViews;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.Message;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.helper.Ring;
import com.katadigital.portsip.views.PortSipCallScreenActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationHelper {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    private static NotificationHelper sNotificationHelper;
    private static NotificationCompat.Builder mNotificationBuilder;
    private static NotificationManager mNotificationManager;
    private Notification mNotification;

    public static void init(Context context) {
        if (sNotificationHelper == null) {
            sNotificationHelper = new NotificationHelper();
            mNotificationBuilder = new NotificationCompat.Builder(context);
            mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
        }
    }

    public static NotificationHelper getNotificationHelperInstance() {
        if (sNotificationHelper != null) {
            return sNotificationHelper;
        }
        throw new IllegalStateException("Call NotificationHelper init()");
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void notifyPortSipStatus(Context context, String title, String message, int icon) {
        /**This code sets notification circle icon color to orange as active*/
        if (message.contains("Online")) {
            mNotificationBuilder.setColor(ContextCompat.getColor(context, R.color.colorTextOrange));
        } else {
            mNotificationBuilder.setColor(ContextCompat.getColor(context, R.color.Silver));
        }

        mNotification = mNotificationBuilder
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(icon)
                .setOngoing(true)
                .build();
        mNotificationManager.notify(Constants.PORTSIP_SDK_STATUS_CODE, mNotification);
    }

    public void unnotifyPortSipStatus() {
        mNotificationManager.cancel(Constants.PORTSIP_SDK_STATUS_CODE);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void notifyPortSipOnIncomingCallStatus(Context context, String title, String message, int icon) {

        /**Setting up custom notification views*/
        RemoteViews views = new RemoteViews(context.getPackageName(),
                R.layout.notification_small_portsip_oncall);
        views.setImageViewResource(R.id.image, R.drawable.ic_owtel_icon_notification);

        /**Setting up notification listeners*/
        Intent intentToPortScreenCallScreen = new Intent(context, PortSipCallScreenActivity.class);
        intentToPortScreenCallScreen.setAction("");
        PendingIntent pendingIntentToPortSipCallScreen = PendingIntent
                .getActivity(context, 0, intentToPortScreenCallScreen, 0);

        Intent answerCall = new Intent(context, PortSipCallScreenActivity.class);
        answerCall.setAction(Constants.NOTIFY_PORTSIP_ANSWER_CALL_TAG);
        PendingIntent pendingIntentAnswerCall = PendingIntent.getActivity(context, 0, answerCall, 0);

        Intent rejectCall = new Intent(context, PortSipCallScreenActivity.class);
        rejectCall.setAction(Constants.NOTIFY_PORTSIP_REJECT_CALL_TAG);
        PendingIntent pendingIntentRejectCall = PendingIntent.getActivity(context, 0, rejectCall, 0);

        mNotification = new NotificationCompat.Builder(context)
                .setContent(views)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(message)
                .setOngoing(true)
                .setContentIntent(pendingIntentToPortSipCallScreen)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(0)
                .setColor(context.getResources().getColor(R.color.colorTextOrange))
                .addAction(R.drawable.ic_answer_call, "Answer", pendingIntentAnswerCall)
                .addAction(R.drawable.ic_reject_call, "Reject", pendingIntentRejectCall)
                .build();

        mNotificationManager.notify(Constants.PORTSIP_CALL_STATUS_CODE, mNotification);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void notifyPortSipOnCallConnectedStatus(Context context, String title, String message, int icon) {

        /**Setting up custom notification views*/
        RemoteViews views = new RemoteViews(context.getPackageName(),
                R.layout.notification_small_portsip_oncall);
        views.setImageViewResource(R.id.image, R.drawable.ic_owtel_icon_notification);

        /**Setting up notification listeners*/
        Intent intentToPortScreenCallScreen = new Intent(context, PortSipCallScreenActivity.class);
        intentToPortScreenCallScreen.setAction("");
        PendingIntent pendingIntentToPortSipCallScreen = PendingIntent
                .getActivity(context, 0, intentToPortScreenCallScreen, 0);

        Intent rejectCall = new Intent(context, PortSipCallScreenActivity.class);
        rejectCall.setAction(Constants.NOTIFY_PORTSIP_REJECT_CALL_TAG);
        PendingIntent pendingIntentRejectCall = PendingIntent.getActivity(context, 0, rejectCall, 0);

        mNotification = new NotificationCompat.Builder(context)
                .setContent(views)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(message)
                .setOngoing(true)
                .setContentIntent(pendingIntentToPortSipCallScreen)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(0)
                .setColor(context.getColor(R.color.colorTextOrange))
                .addAction(R.drawable.ic_reject_call, "End Call", pendingIntentRejectCall)
                .addAction(0, "", null)
                .build();

        mNotificationManager.notify(Constants.PORTSIP_CALL_STATUS_CODE, mNotification);
    }

    public void notifyOnMessageReceived(Context context, JSONObject jsonResponse) {
        try {
            Bitmap icon = BitmapFactory.decodeResource(OwtelAppController.getInstance().getResources(),
                    R.mipmap.ic_launcher);

            Intent chatActivity = new Intent(OwtelAppController.getInstance(), ChatActivity.class);
            Bundle args = new Bundle();
            args.putString(Constants.BUNDLE_CHAT_OWTEL_NUMBER, jsonResponse.getString("sender"));
            args.putString(Constants.BUNDLE_CHAT_THREAD_ID, jsonResponse.getString("thread_id"));
            args.putString(Constants.BUNDLE_CHAT_SMS_ID, jsonResponse.getString("sms_id"));
            args.putString(Constants.BUNDLE_CHAT_RECEIVER, jsonResponse.getString("receiver"));
            chatActivity.putExtras(args);

            /**Show notification*/
            mNotification = mNotificationBuilder
                    .setContentText(jsonResponse.getString("sms_content"))
                    .setContentTitle("Owtel message")
                    .setAutoCancel(true)
                    .setContentIntent(PendingIntent.getActivity(OwtelAppController.getInstance(), 0,
                            chatActivity, PendingIntent.FLAG_UPDATE_CURRENT))
                    .setSmallIcon(R.drawable.messageimage)
                    .setLargeIcon(icon)
                    .build();

            /**Play notification sound*/
            Ring.getInstance(context).startNotificationTone(false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mNotificationManager.notify(Constants.WEB_SOCKET_RECEIVED_MESSAGE, mNotification);
    }

    public void unotifyOnMessageReceived() {
        mNotificationManager.cancel(Constants.WEB_SOCKET_RECEIVED_MESSAGE);
    }

    public void notifyReceivedMessage(Context context, Message message) {
        Bitmap icon = BitmapFactory.decodeResource(OwtelAppController.getInstance().getResources(),
                R.mipmap.ic_launcher);

        Intent chatActivityIntent = new Intent(OwtelAppController.getInstance(), ChatActivity.class);
        chatActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_CHAT_OWTEL_NUMBER, message.getSender());
        args.putString(Constants.BUNDLE_CHAT_THREAD_ID, message.getRoomID());
        args.putString(Constants.BUNDLE_CHAT_SMS_ID, message.getMessageID());
        args.putString(Constants.BUNDLE_CHAT_RECEIVER, message.getReceiver());
        chatActivityIntent.putExtras(args);

        /**Play notification sound*/
        Ring.getInstance(context).startNotificationTone(false);

        /**Show notification*/
        mNotification = mNotificationBuilder.setContentText(message.getValue())
                .setContentTitle("Owtel message")
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(OwtelAppController.getInstance(), 0,
                        chatActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(R.drawable.messageimage)
                .setLargeIcon(icon)
                .build();
        mNotificationManager.notify(Constants.MESSAGE_MAKE_NOTIFICATION, mNotification);
        OwtelAppController.getInstance().getDaoSession().getMessageDao().insertOrReplace(message);
    }

    public void unotifyReceivedMessage() {
        mNotificationManager.cancel(Constants.MESSAGE_MAKE_NOTIFICATION);
    }


    public void unnotifyPortSipCallStatus() {
        mNotificationManager.cancel(Constants.PORTSIP_CALL_STATUS_CODE);
    }
}
