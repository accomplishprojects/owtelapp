package com.katadigital.portsip.utilities.androidcomponent.interfaces;

public interface PortSipContract {

    interface PortSipContractListener {
        void onInviteCallConnected();

        void onInviteCallEnded();

        void onBluetoothStatusChange(boolean status);

        void onUpdateBadgeCount();
    }

    interface PortSipContractCallback {
        void onPortSipSendDMTFCallback(char number);
    }
}
