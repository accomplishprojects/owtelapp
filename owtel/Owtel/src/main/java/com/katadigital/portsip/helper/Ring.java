package com.katadigital.portsip.helper;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;

import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;

public class Ring {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    private static final int TONE_RELATIVE_VOLUME = 100;
    private ToneGenerator mRingbackPlayer;
    private ToneGenerator mDTMFPlayer;
    protected Ringtone mRingtonePlayer, mNotificationTonePlayer, mPlayTonePlayer;
    int ringRef = 0;
    private Context mContext;

    private static Ring single = null;
    private static Vibrator mVibrator;
    private long ringtonePattern[] = {2000, 2000, 2000, 2000, 2000, 2000};
    private long notifPattern[] = {200, 200, 200};

    public static Ring getInstance(Context context) {
        if (single == null) {
            single = new Ring(context);
            mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        }
        return single;
    }

    private Ring(Context context) {
        mContext = context;
        SharedPreferenceManager.init(context);
    }

    public boolean stop() {
        stopRingBackTone();
        stopRingTone();
        stopNotificationTone();
        stopDTMF();
        if (mVibrator != null) {
            mVibrator.cancel();
        }

        return true;
    }

//    public void startDTMF(int number) {
//        if (mDTMFPlayer == null) {
//            try {
//                mDTMFPlayer = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, TONE_RELATIVE_VOLUME);
//            } catch (RuntimeException e) {
//                mDTMFPlayer = null;
//            }
//        }
//
//        if (mDTMFPlayer != null) {
//            synchronized (mDTMFPlayer) {
//                switch (number) {
//                    case 0:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_0);
//                        break;
//                    case 1:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_1);
//                        break;
//                    case 2:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_2);
//                        break;
//                    case 3:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_3);
//                        break;
//                    case 4:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_4);
//                        break;
//                    case 5:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_5);
//                        break;
//                    case 6:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_6);
//                        break;
//                    case 7:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_7);
//                        break;
//                    case 8:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_8);
//                        break;
//                    case 9:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_9);
//                        break;
//                    case 10:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_S);
//                        break;
//                    case 11:
//                        mDTMFPlayer.startTone(ToneGenerator.TONE_DTMF_P);
//                        break;
//                }
//            }
//        }
//    }

    public void stopDTMF() {
        if (mDTMFPlayer != null) {
            synchronized (mDTMFPlayer) {
                mDTMFPlayer.stopTone();
                mDTMFPlayer = null;
            }
        }
    }

//    public void playShowcaseTone(Uri toneUri) {
//        Log.d(TAG, "playShowcaseTone: "+ toneUri);
//        if (mPlayTonePlayer != null && mPlayTonePlayer.isPlaying()) {
//            stopShowcaseTone();
//        }
//
//        if (mPlayTonePlayer == null && mContext != null) {
//            try {
//                if (toneUri != null) {
//                    mPlayTonePlayer = RingtoneManager.getRingtone(mContext, toneUri);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                return;
//            }
//        }
//
//        if (mPlayTonePlayer != null) {
//            synchronized (mPlayTonePlayer) {
//                ringRef++;
//                mPlayTonePlayer.play();
//            }
//        }
//    }

    public void stopShowcaseTone() {
        if (mPlayTonePlayer != null) {
            synchronized (mPlayTonePlayer) {

                if (--ringRef <= 0) {
                    /**Stop vibration*/
                    mVibrator.cancel();

                    mPlayTonePlayer.stop();
                    mPlayTonePlayer = null;
                }
            }
        }
    }

    public void startRingTone() {
        String ringtoneUri = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .getSelectedRingtoneURI();
        if (mRingtonePlayer != null && mRingtonePlayer.isPlaying()) {
            ringRef++;
            return;
        }

        if (mRingtonePlayer == null && mContext != null) {
            try {
                if (ringtoneUri != null) {
                    mRingtonePlayer = RingtoneManager.getRingtone(mContext, Uri.parse(ringtoneUri));
                } else {
                    mRingtonePlayer = RingtoneManager.getRingtone(mContext, android.provider.Settings.System.DEFAULT_RINGTONE_URI);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        if (mRingtonePlayer != null) {
            synchronized (mRingtonePlayer) {
                ringRef++;
                mRingtonePlayer.play();

                boolean vibrationStatus = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getRingtoneVibrationStatus();
                Log.e(TAG, "startRingTone: " + vibrationStatus);
                if (vibrationStatus) {
                    /**Start vibration*/
                    mVibrator.vibrate(5000);
                    mVibrator.vibrate(ringtonePattern, 1);
                }
            }
        }
    }

    public void startNotificationTone(boolean isSender) {
        String notificationUri = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .getSelectedNotificationURI();

        Uri conversationToneUri = Uri.parse("android.resource://" + mContext.getPackageName()
                + "/raw/audio_conversation_tone");

        if (mNotificationTonePlayer != null && mNotificationTonePlayer.isPlaying()) {
            ringRef++;
            return;
        } else {
            stopNotificationTone();
        }

        if (mNotificationTonePlayer == null && mContext != null) {
            try {
                if (isSender) {
                    /**If conversation tone is enabled in settings*/
                    if (SharedPreferenceManager.getSharedPreferenceManagerInstance().getConvoToneStatus()) {
                        mNotificationTonePlayer = RingtoneManager.getRingtone(mContext, conversationToneUri);
                        Log.e(TAG, "startNotificationTone1: "+conversationToneUri );
                    }
                } else {
                    if (notificationUri != null) {
                        mNotificationTonePlayer = RingtoneManager.getRingtone(mContext, Uri.parse(notificationUri));
                        Log.e(TAG, "startNotificationTone2: "+notificationUri );

                    } else {
                        mNotificationTonePlayer = RingtoneManager.getRingtone(mContext, Settings.System.DEFAULT_NOTIFICATION_URI);
                        Log.e(TAG, "startNotificationTone3: "+Settings.System.DEFAULT_NOTIFICATION_URI );

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        if (mNotificationTonePlayer != null) {
            synchronized (mNotificationTonePlayer) {
                ringRef++;
                mNotificationTonePlayer.play();

                boolean vibrationStatus = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getNotificationVibrationStatus();
                Log.e(TAG, "startRingTone: " + vibrationStatus);
                if (vibrationStatus) {

                    /**Start vibration*/
                    mVibrator.vibrate(400);
                    mVibrator.vibrate(notifPattern, 2);
                }
            }
        }
    }

    public void stopRingTone() {
        if (mRingtonePlayer != null) {
            synchronized (mRingtonePlayer) {

                if (--ringRef <= 0) {
                    /**Stop vibration*/
                    mVibrator.cancel();

                    mRingtonePlayer.stop();
                    mRingtonePlayer = null;
                }
            }
        }
    }

    public void stopNotificationTone() {
        if (mNotificationTonePlayer != null) {
            synchronized (mNotificationTonePlayer) {

                if (--ringRef <= 0) {
                    /**Stop vibration*/
                    mVibrator.cancel();

                    mNotificationTonePlayer.stop();
                    mNotificationTonePlayer = null;
                }
            }
        }
    }

    public void startRingBackTone(boolean isSpeakerOn) {
        stopRingBackTone();
        if (mRingbackPlayer == null) {
            try {
                if (isSpeakerOn) {
                    mRingbackPlayer = new ToneGenerator(AudioManager.STREAM_RING, TONE_RELATIVE_VOLUME);
                } else {
                    mRingbackPlayer = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, TONE_RELATIVE_VOLUME);
                }
            } catch (RuntimeException e) {
                mRingbackPlayer = null;
            }
        }

        if (mRingbackPlayer != null) {
            synchronized (mRingbackPlayer) {
                mRingbackPlayer.startTone(ToneGenerator.TONE_SUP_RINGTONE);
            }
        }
    }

    public void stopRingBackTone() {
        if (mRingbackPlayer != null) {
            synchronized (mRingbackPlayer) {
                mRingbackPlayer.stopTone();
                mRingbackPlayer = null;
            }
        }
    }

}
