package com.katadigital.portsip.views;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.modules.login.LoginActivity;
import com.katadigital.owtel.modules.main.contacts.CustomAutoCompleteTextView;
import com.katadigital.owtel.utils.CallBlockHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.helper.Line;
import com.katadigital.portsip.helper.Ring;
import com.katadigital.portsip.helper.Session;
import com.katadigital.portsip.utilities.androidcomponent.interfaces.PortSipContract;
import com.katadigital.portsip.utilities.androidcomponent.service.PortSipService;
import com.katadigital.portsip.utilities.logs.CallLogsManager;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;
import com.katadigital.portsip.utilities.ui.image.ImageCustomManager;
import com.katadigital.ui.recyclerview.DialerRecyclerViewAdapter;
import com.portsip.PortSipEnumDefine;
import com.portsip.PortSipSdk;

import de.hdodenhof.circleimageview.CircleImageView;

@TargetApi(Build.VERSION_CODES.CUPCAKE)
public class PortSipCallScreenActivity extends AppCompatActivity implements
        View.OnClickListener, View.OnLongClickListener,
        PortSipContract.PortSipContractListener, PortSipContract.PortSipContractCallback,
        SensorEventListener {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * Variables
     */
    private String strNumberToCall, strCallerDisplayName;
    private boolean isIncomingCall = false, isChronometerIsRunning = false, isCallRejected = false;
    private BluetoothAdapter bluetoothAdapter;

    /**
     * Views variables
     */
    ImageButton mButtonEndCall, mButtonAnswerCall, mButtonRejectCall, mButtonDialer, mButtonDialerCollapse;
    private CircleImageView imgCallerPhoto;
    private ToggleButton mToggleButtonLoudSpeaker, mToggleButtonMute, mToggleButtonBluetooth;
    private Chronometer mChronometerDuration;
    private RelativeLayout mLayoutIncoming, mLayoutCallDetails;
    private LinearLayout mLayoutDialer, mLayoutOngoing;
    TextView mTextViewCallIncoming, mTextViewCallName, mTextViewCallLabel, mTextViewCallPhoneNumber, txtCallerNumber;
    private RecyclerView recyclerViewDialer;
    private CustomAutoCompleteTextView etCallDialer;
    Spinner spline;
    ArrayAdapter<Session> spinnerAdapter;
    private static final int SENSOR_SENSITIVITY = 1;

    /***/
    SensorManager mSensorManager;

    Sensor mProximity;
    PowerManager mPowerManager;
    PowerManager.WakeLock mWakeLock;

    /**
     * PortSipSDK variables
     */
    private PortSipSdk mPortSipSdk;
    private OwtelAppController app;
    Line[] lines = null;
    int _CurrentlyLine = 0;
    private String wakeLockData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**This method will display the activity when the device is on lock screen*/
        showActivityOnLockScreen();
        setContentView(R.layout.layout_call_screen_zoiper);

        /**Initialize helper classes*/
        ImageCustomManager.init();
        NotificationHelper.init(this);
        CallLogsManager.init();

        /**Initialize PortSipSDK*/
        setupPortSipSdk();

        /**Getting intent extras from DialerFragment and OwtelAppController*/
        strNumberToCall = getIntent().getStringExtra("NumberToCall");
        strCallerDisplayName = getIntent().getStringExtra("callerDisplayName");
        Line curSession = OwtelAppController.getInstance().curSession;
        isIncomingCall = getIntent().getBooleanExtra("isIncomingCall", false);

        /**Initializing Views*/
        setupViews();
        setupRecyclerViewDialer();

        if (!isIncomingCall && strNumberToCall != null) {
            /**Initialize PortSipCall to other device*/
            makePortSipCall();
            showIncomingCallLayout();
            ImageCustomManager.getImageCustomManagerInstance()
                    .displayProfilePictureFromUri(this, strNumberToCall, imgCallerPhoto);

        } else if (isIncomingCall && curSession != null) {
            /**Display incoming call screen layout*/
            showIncomingCallLayout();
            ImageCustomManager.getImageCustomManagerInstance()
                    .displayProfilePictureFromUri(this, strCallerDisplayName, imgCallerPhoto);

            Line currentLine = app.findSessionByIndex(_CurrentlyLine);
            Log.e(TAG, "onCreate: " + "CurrentLine: " + currentLine);
            Log.e(TAG, "onCreate: " + "Session ID: " + curSession.getSessionId() +
                    " Current Line: " + currentLine);
        }

        /**Auto Reject Blocked Caller*/
        boolean isCallerBlocked = CallBlockHelper.getInstance(OwtelAppController.getInstance())
                .checkIfContactIsBlocked(strCallerDisplayName);
        if (isCallerBlocked) {
            mButtonAnswerCall.setClickable(false);
            mButtonRejectCall.setClickable(false);
            Toast.makeText(app, "Caller has been blocked: " + strCallerDisplayName, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> {
                mButtonRejectCall.performClick();
            }, 1500);
        }
    }

    private void makePortSipCall() {
        if (strNumberToCall == null || strNumberToCall.length() <= 0) {
            showTips("The phone number is empty.");
            return;
        }

        Session currentLine = app.findSessionByIndex(_CurrentlyLine);
        if (currentLine.getSessionState()
                || currentLine.getRecvCallState()) {
            showTips("Current line is busy now, please switch a line.");
            return;
        }

        /**Ensure that we have been added one audio codec at least*/
        if (mPortSipSdk.isAudioCodecEmpty()) {
            showTips("Audio Codec Empty,add audio codec at first");
            return;
        }

        /**Usually for 3PCC need to make call without SDP*/
        //remove special characters
        long sessionId = mPortSipSdk.call(strNumberToCall.replaceAll("[^\\w+*]", ""),
                true, true);
        if (sessionId <= 0) {
            showTips("Call failure");
            return;
        }

        currentLine.setSessionId(sessionId);
        currentLine.setSessionState(true);
        currentLine.setVideoState(true);
        app.setCurrentLine(lines[_CurrentlyLine]);
        showTips(lines[_CurrentlyLine].getLineName() + ": Calling...");

//        myApp.updateSessionVideo();
        Log.e(TAG, "makePortSipCall: " + lines[_CurrentlyLine].getLineName()
                + ": Calling..." + lines[_CurrentlyLine]);

    }


    private void setupPortSipSdk() {
        app = ((OwtelAppController) getApplicationContext());
        mPortSipSdk = app.getPortSipSdk();
        mPortSipSdk.setLoudspeakerStatus(false);
        lines = app.getLines();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void setupViews() {
        /**Proximity*/
        mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        /**Bluetooth*/
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        /**Buttons*/
        mButtonDialer = (ImageButton) findViewById(R.id.btn_call_keypad);
        mButtonDialerCollapse = (ImageButton) findViewById(R.id.btn_dialer_collapse);
        mToggleButtonLoudSpeaker = (ToggleButton) findViewById(R.id.btn_call_loud_speaker);
        mToggleButtonMute = (ToggleButton) findViewById(R.id.btn_call_mute);
        mToggleButtonBluetooth = (ToggleButton) findViewById(R.id.btn_call_bluetooth);
        mButtonAnswerCall = (ImageButton) findViewById(R.id.btn_call_answer);
        mButtonRejectCall = (ImageButton) findViewById(R.id.btn_call_reject);
        mButtonEndCall = (ImageButton) findViewById(R.id.btn_call_end);

        /**Button Listeners*/
        mButtonDialer.setOnClickListener(this);
        mButtonDialerCollapse.setOnClickListener(this);
        mToggleButtonLoudSpeaker.setOnClickListener(this);
        mToggleButtonMute.setOnClickListener(this);
        mToggleButtonBluetooth.setOnClickListener(this);
        mButtonAnswerCall.setOnClickListener(this);
        mButtonRejectCall.setOnClickListener(this);
        mButtonEndCall.setOnClickListener(this);
        mToggleButtonBluetooth.setOnLongClickListener(this);

        /**Chronometer*/
        mChronometerDuration = (Chronometer) findViewById(R.id.chronometer_duration);

        /**Layouts*/
        mLayoutCallDetails = (RelativeLayout) findViewById(R.id.layout_call_details);
        mLayoutDialer = (LinearLayout) findViewById(R.id.layout_call_dialer);
        mLayoutIncoming = (RelativeLayout) findViewById(R.id.layout_call_incoming);
        mLayoutOngoing = (LinearLayout) findViewById(R.id.layout_call_ongoing);
        recyclerViewDialer = (RecyclerView) findViewById(R.id.rv_dialer);

        /**TextViews*/
        mTextViewCallName = (TextView) findViewById(R.id.tv_call_name);
        mTextViewCallLabel = (TextView) findViewById(R.id.tv_call_label);
        mTextViewCallPhoneNumber = (TextView) findViewById(R.id.tv_call_phone_number);
        mTextViewCallIncoming = (TextView) findViewById(R.id.tv_call_incoming);
        txtCallerNumber = (TextView) findViewById(R.id.txtCallerNumber);

        /**EditText*/
        etCallDialer = (CustomAutoCompleteTextView) findViewById(R.id.et_dialer_display);

        /**Circle ImageView*/
        imgCallerPhoto = (CircleImageView) findViewById(R.id.img_view_contact_picture);

        /**Spinner*/
        spline = (Spinner) findViewById(R.id.sp_lines);
        spinnerAdapter = new ArrayAdapter<>(this,
                R.layout.viewspinneritem, lines);
        spline.setAdapter(spinnerAdapter);
        spline.setOnItemSelectedListener(new MyItemSelectListener());
    }

    private void setupRecyclerViewDialer() {
        DialerRecyclerViewAdapter mAdapter = new DialerRecyclerViewAdapter(this, etCallDialer);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerViewDialer.setLayoutManager(gridLayoutManager);
        recyclerViewDialer.setHasFixedSize(true);
        recyclerViewDialer.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_call_keypad:
                showCallDialer(true);
                break;

            case R.id.btn_dialer_collapse:
                showCallDialer(false);
                break;

            case R.id.btn_call_loud_speaker:
                if (mToggleButtonLoudSpeaker.isChecked()) {
                    mPortSipSdk.setLoudspeakerStatus(mToggleButtonLoudSpeaker.isChecked());
                    if (!isChronometerIsRunning) {
                        Ring.getInstance(this).startRingBackTone(true);
                    }
                } else {
                    mPortSipSdk.setLoudspeakerStatus(mToggleButtonLoudSpeaker.isChecked());
                    if (!isChronometerIsRunning) {
                        Ring.getInstance(this).startRingBackTone(false);
                    }
                }
                break;

            case R.id.btn_call_mute: {
                Session currentLine = app.findSessionByIndex(_CurrentlyLine);
                if (mToggleButtonMute.isChecked()) {
                    mPortSipSdk.muteSession(currentLine.getSessionId(),
                            false, true, true, true);
                } else {
                    mPortSipSdk.muteSession(currentLine.getSessionId(),
                            false, false, false, false);
                }
            }
            break;

            case R.id.btn_call_bluetooth:
                if (!mToggleButtonBluetooth.isChecked() && bluetoothAdapter.isEnabled())
                    bluetoothAdapter.disable();
                else
                    bluetoothAdapter.enable();
                break;

            case R.id.btn_call_answer: {
                callIsAnswered();
            }
            break;

            case R.id.btn_call_reject: {
                isCallRejected = true;
                callRejected();
            }
            break;

            case R.id.btn_call_end: {
                Session currentLine = app.findSessionByIndex(_CurrentlyLine);

                Ring.getInstance(this).stop();
                if (currentLine.getRecvCallState()) {
                    mPortSipSdk.rejectCall(currentLine.getSessionId(), 486);
                    currentLine.reset();
                    showTips(lines[_CurrentlyLine].getLineName()
                            + ": Rejected call");
                    Log.e(TAG, "onClick: " + "Reject");
                    return;
                }

                if (currentLine.getSessionState()) {
                    mPortSipSdk.hangUp(currentLine.getSessionId());
                    currentLine.reset();
                    showTips(lines[_CurrentlyLine].getLineName() + ": Hang up");
                    Log.e(TAG, "onClick: " + "Hang up");
                    onInviteCallEnded();
                    finish();
                }
                finish();
            }
            break;
        }

    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.btn_call_bluetooth:
                startActivity(new Intent().setAction(Settings.ACTION_BLUETOOTH_SETTINGS));
                break;
        }
        return true;
    }

    public void showTips(String text) {
        Log.e(TAG, "showTips: " + text);
        spinnerAdapter.notifyDataSetChanged();
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void showActivityOnLockScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void showCallDialer(boolean show) {
        if (show) {
            mLayoutDialer.setVisibility(View.VISIBLE);
            mLayoutCallDetails.setVisibility(View.GONE);
        } else if (!show) {
            mLayoutDialer.setVisibility(View.GONE);
            mLayoutCallDetails.setVisibility(View.VISIBLE);
        }
    }

    private void showIncomingCallLayout() {
        if (isIncomingCall) {
            mLayoutIncoming.setVisibility(View.VISIBLE);
            mLayoutOngoing.setVisibility(View.GONE);
            mButtonEndCall.setVisibility(View.INVISIBLE);
            mChronometerDuration.setVisibility(View.GONE);
            mTextViewCallIncoming.setVisibility(View.VISIBLE);
            mTextViewCallIncoming.setText("Incoming Call...");
            txtCallerNumber.setVisibility(View.VISIBLE);

            String strContactName = CallLogsManager.getCallLogsManagerInstance()
                    .getContactName(this, strCallerDisplayName);
            Log.e(TAG, "onClick: " + strContactName);

            if (strContactName != null) {
                txtCallerNumber.setText(strContactName);
            } else {
                txtCallerNumber.setText(strCallerDisplayName);
            }
        } else {
            mTextViewCallIncoming.setVisibility(View.VISIBLE);
            mTextViewCallIncoming.setText("Dialing...");
            txtCallerNumber.setVisibility(View.VISIBLE);

            String strContactName = CallLogsManager.getCallLogsManagerInstance()
                    .getContactName(this, strNumberToCall);
            Log.e(TAG, "onClick: " + strContactName);

            if (strContactName != null) {
                txtCallerNumber.setText(strContactName);
            } else {
                txtCallerNumber.setText(strNumberToCall);
            }
        }
    }

    private void callIsAnswered() {
        Ring.getInstance(this).stop();
        mLayoutIncoming.setVisibility(View.GONE);
        mLayoutOngoing.setVisibility(View.VISIBLE);
        mButtonEndCall.setVisibility(View.VISIBLE);
        mTextViewCallIncoming.setVisibility(View.GONE);
        mChronometerDuration.setVisibility(View.VISIBLE);

        /**Answering call in PortSip*/
        Line currentLine = app.findSessionByIndex(_CurrentlyLine);
        if (!currentLine.getRecvCallState()) {
            Log.e(TAG, "onClick: " + "No incoming call on current line, please switch a line.");
            return;
        }

        currentLine.setRecvCallState(false);
        app.answerSessionCall(currentLine, false);
    }

    private void callRejected() {
        Ring.getInstance(this).stopRingTone();
        Session currentLine = app.findSessionByIndex(_CurrentlyLine);
        if (currentLine.getRecvCallState()) {
            mPortSipSdk.rejectCall(currentLine.getSessionId(), 486);
            currentLine.reset();
            showTips(lines[_CurrentlyLine].getLineName() + ": Rejected call");
            Log.e(TAG, "onClick: " + "Reject");
            finish();
        }

        if (currentLine.getSessionState()) {
            mPortSipSdk.hangUp(currentLine.getSessionId());
            currentLine.reset();
            showTips(lines[_CurrentlyLine].getLineName() + ": Hang up");
            Log.e(TAG, "onClick: " + "Hang up");
            onInviteCallEnded();
            finish();
        }

        /**Set call as missed call if the incoming call is rejected*/
        startService(new Intent(this, PortSipService.class)
                .setAction(Constants.PORTSIP_CALL_CLOSE_ON_SERVICE)
                .putExtra("isIncomingCall", isIncomingCall)
                .putExtra("NumberToCall", strNumberToCall)
                .putExtra("callerDisplayName", strCallerDisplayName)
                .putExtra("isChronometerIsRunning", isChronometerIsRunning));
    }

    @Override
    public void onPortSipSendDMTFCallback(char number) {
        Session currentLine = app.findSessionByIndex(_CurrentlyLine);
        if (app.isOnline()
                && currentLine.getSessionState()) {
            if (number == '*') {
                mPortSipSdk.sendDtmf(currentLine.getSessionId(),
                        PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 10,
                        160, true);
                return;
            }
            if (number == '#') {
                mPortSipSdk.sendDtmf(currentLine.getSessionId(),
                        PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, 11,
                        160, true);
                return;
            }
            int sum = Integer.valueOf(number);// 0~9
            Log.e(TAG, "onPortSipSendDMTFCallback: Sum: " + sum +
                    " Number: " + number);
            mPortSipSdk.sendDtmf(currentLine.getSessionId(),
                    PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, sum,
                    160, true);
        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            Log.e(TAG, "onSensorChanged: " + event.values[0]);
            if (event.values[0] > -SENSOR_SENSITIVITY && event.values[0] < SENSOR_SENSITIVITY) {
                if (wakeLockData.equalsIgnoreCase("FAR") || wakeLockData.equals("")) {
                    Log.e(TAG, "onSensorChanged: Proximity is: " + "NEAR");
                    mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                            | PowerManager.ACQUIRE_CAUSES_WAKEUP, "tag");
                    mWakeLock.acquire();
                    wakeLockData = "NEAR";
//                    setScreenBacklight((float) -1);
                    getWindow().setFlags(0,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
            } else {
                if (wakeLockData.equalsIgnoreCase("NEAR") || wakeLockData.equals("")) {
                    Log.e(TAG, "onSensorChanged: Proximity is: " + "FAR");
                    mWakeLock = mPowerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "tag");
                    mWakeLock.acquire();
                    wakeLockData = "FAR";
//                    setScreenBacklight((float) 0.1);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
                /**Hide dialer if visible*/
//                showCallDialer(false);

            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    class MyItemSelectListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            int index = arg2;
            Log.e(TAG, "onItemSelected: " + index);
            if (_CurrentlyLine == (index + Line.LINE_BASE)) {
                return;
            }

            if (!app.isOnline()) {
                showTips("Not Registered, please register at first.");
                return;
            }

//            if (cbConference.isChecked()) {
//                _CurrentlyLine = arg2 + Line.LINE_BASE;
//                return;
//            }

            /**To switch the line, must hold currently line first*/
            Line currentLine = app.findSessionByIndex(_CurrentlyLine);
            if (currentLine.getSessionState() && !currentLine.getHoldState()) {
                mPortSipSdk.hold(currentLine.getSessionId());
                currentLine.setHoldState(true);

                showTips(lines[_CurrentlyLine].getLineName() + ": Hold");
            }

            _CurrentlyLine = arg2 + Line.LINE_BASE;
            currentLine = app.findSessionByIndex(_CurrentlyLine); /**update current line*/
            app.setCurrentLine(currentLine);

            /**If target line was in hold state, then un-hold it*/
            if (currentLine.getSessionState()
                    && currentLine.getHoldState()) {
                mPortSipSdk.unHold(currentLine.getSessionId());
                currentLine.setHoldState(false);

                showTips(lines[_CurrentlyLine].getLineName()
                        + ": UnHold - call established");
            }
            spinnerAdapter.notifyDataSetChanged();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");
        app.setActivity(this);

        NotificationHelper.getNotificationHelperInstance().unnotifyPortSipCallStatus();

        /**This code will prevent incoming layout to be displayed when user calling without opening the app*/
        Session currentLine = app.findSessionByIndex(_CurrentlyLine);
        if (!currentLine.getRecvCallState() && mLayoutIncoming.getVisibility() == View.VISIBLE) {
            Log.e(TAG, "onResume: " + "No available calls");
            startActivity(new Intent(this, LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause: ");
        app.setActivity(this);

        /**Notify when user clicks home and menu buttons while creating or receiving a call*/
        if (isChronometerIsRunning) {
            if (isIncomingCall) {
                NotificationHelper.getNotificationHelperInstance()
                        .notifyPortSipOnCallConnectedStatus(this, "Owtel", strCallerDisplayName
                                + " is connected", R.drawable.ic_owtel_icon_notification);
            } else {
                NotificationHelper.getNotificationHelperInstance()
                        .notifyPortSipOnCallConnectedStatus(this, "Owtel", strNumberToCall
                                + " is connected", R.drawable.ic_owtel_icon_notification);
            }
        } else {
            if (isIncomingCall && mLayoutIncoming.getVisibility() == View.VISIBLE && !isCallRejected) {
                NotificationHelper.getNotificationHelperInstance()
                        .notifyPortSipOnIncomingCallStatus(this, "Owtel", strCallerDisplayName,
                                R.drawable.ic_owtel_icon_notification);
                Log.e(TAG, "onPause: " + "if");
            } else {
                if (!isCallRejected) {
                    NotificationHelper.getNotificationHelperInstance()
                            .notifyPortSipOnCallConnectedStatus(this, "Owtel", strNumberToCall,
                                    R.drawable.ic_owtel_icon_notification);
                }

                Log.e(TAG, "onPause: " + "else");
            }
            isCallRejected = false;
        }
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: " + isCallRejected);

        if (isCallRejected) {
            startActivity(new Intent(this, LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        Ring.getInstance(this).stopRingTone();
        NotificationHelper.getNotificationHelperInstance()
                .unnotifyPortSipCallStatus();

        /**Releasing wake lock
         * added wakelock null checker*/
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
            mWakeLock = null;
        }

    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: ");
    }

    @Override
    public void onInviteCallConnected() {
        mChronometerDuration.setVisibility(View.VISIBLE);
        mTextViewCallIncoming.setVisibility(View.GONE);
        mChronometerDuration.setBase(SystemClock.elapsedRealtime());
        Log.e(TAG, "onInviteCallConnected: " + mChronometerDuration.getText().toString());
        mChronometerDuration.start();
        isChronometerIsRunning = true;
        app.setCallMissed(false);
        startService(new Intent(this, PortSipService.class)
                .setAction(Constants.PORTSIP_CALL_CONNECTED_ON_SERVICE));
    }

    @Override
    public void onInviteCallEnded() {
        Log.e(TAG, "onInviteCallEnded: " + mChronometerDuration.getText().toString() + " getBase(): " + mChronometerDuration.getBase());
        mChronometerDuration.stop();
        finish();
        startService(new Intent(this, PortSipService.class)
                .setAction(Constants.PORTSIP_CALL_CLOSE_ON_SERVICE)
                .putExtra("isIncomingCall", isIncomingCall)
                .putExtra("NumberToCall", strNumberToCall)
                .putExtra("callerDisplayName", strCallerDisplayName)
                .putExtra("isChronometerIsRunning", isChronometerIsRunning));
        isChronometerIsRunning = false;
        isCallRejected = true;
    }

    @Override
    public void onBluetoothStatusChange(boolean isBluetoothEnabled) {
        if (isBluetoothEnabled) {
            mToggleButtonBluetooth.setChecked(true);
            bluetoothAdapter.enable();
        } else {
            mToggleButtonBluetooth.setChecked(false);
            bluetoothAdapter.disable();
        }
    }

    @Override
    public void onUpdateBadgeCount() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e(TAG, "onNewIntent: " + intent.getAction());

        switch (intent.getAction()) {
            case Constants.NOTIFY_PORTSIP_ANSWER_CALL_TAG:
                callIsAnswered();
                break;

            case Constants.NOTIFY_PORTSIP_REJECT_CALL_TAG:
                isCallRejected = true;
                callRejected();
                break;

            default:

                break;
        }
    }

    void setScreenBacklight(float a) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = a;
        getWindow().setAttributes(lp);
    }
}
