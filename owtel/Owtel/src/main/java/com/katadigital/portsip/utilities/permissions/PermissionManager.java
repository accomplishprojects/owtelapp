package com.katadigital.portsip.utilities.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionManager {

    private static PermissionManager sPermissionManager;
    private List<String> listPermissionsNeeded;

    public static void init() {
        if (sPermissionManager == null) {
            sPermissionManager = new PermissionManager();
        }
    }

    public static PermissionManager getPermissionManagerInstance() {
        if (sPermissionManager != null) {
            return sPermissionManager;
        }

        throw new IllegalStateException("Call PermissionManager init()");
    }

    public boolean checkPermissions(Activity activity) {
        boolean isPermissionsGranted = false;
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.WRITE_CALL_LOG,
                Manifest.permission.DISABLE_KEYGUARD,
                Manifest.permission.READ_SMS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.VIBRATE,
                Manifest.permission.WAKE_LOCK,
                Manifest.permission.READ_PHONE_STATE
        };

        listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);

            }
        }
        if (listPermissionsNeeded.size() == 0) {
            isPermissionsGranted = true;
        } else {
            requestPermissions(activity);
        }

        return isPermissionsGranted;
    }

    public void requestPermissions(Activity activity) {
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
        }
    }
}
