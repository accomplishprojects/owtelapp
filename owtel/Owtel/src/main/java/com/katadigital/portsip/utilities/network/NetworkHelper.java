package com.katadigital.portsip.utilities.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.katadigital.owtel.utils.Constants;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

public class NetworkHelper {

    private static NetworkHelper sNetworkHelper;
    private static OkHttpClient httpClient;

    public static void init() {
        if (sNetworkHelper == null) {
            sNetworkHelper = new NetworkHelper();
            httpClient = new OkHttpClient();
        }
    }

    public static NetworkHelper getNetworkHelperInstance() {
        if (sNetworkHelper != null) {
            return sNetworkHelper;
        }
        throw new IllegalStateException("Call NetworkHelper init()");
    }

    public OkHttpClient getHttpClientForTimeOut() {
        httpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        httpClient.setReadTimeout(60, TimeUnit.SECONDS);
        return httpClient;
    }

    public int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return Constants.NETWORK_TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return Constants.NETWORK_TYPE_MOBILE;
        }
        return Constants.NETWORK_TYPE_NOT_CONNECTED;
    }

    public String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == Constants.NETWORK_TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == Constants.NETWORK_TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == Constants.NETWORK_TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }

}
