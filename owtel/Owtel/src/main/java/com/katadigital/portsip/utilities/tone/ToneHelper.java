package com.katadigital.portsip.utilities.tone;


import android.content.Context;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.katadigital.owtel.models.collectedobjects.AudioItem;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;

import java.util.ArrayList;


/**
 * This class is used for setting up the default notification
 * and ring tone when the application is installed at first time
 */

public class ToneHelper {
    private String TAG = this.getClass().getSimpleName();
    private ArrayList<AudioItem> notificationToneList = new ArrayList<>();
    private ArrayList<AudioItem> ringtoneList = new ArrayList<>();
    private Context mContext;

    public ToneHelper(Context context) {
        this.mContext = context;
        SharedPreferenceManager.init(mContext);
    }

    public void setDefaultRingAndNotificationTone() {
        /**For setting default notification tone*/
        RingtoneManager notificationManager = new RingtoneManager(mContext);
        notificationManager.setType(RingtoneManager.TYPE_NOTIFICATION);
        Cursor cursor = notificationManager.getCursor();
        notificationToneList.add(new AudioItem("None", null));
        while (cursor.moveToNext()) {
            String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            Uri toneURI = notificationManager.getRingtoneUri(cursor.getPosition());
            // Do something with the title and the URI of ringtone
            notificationToneList.add(new AudioItem(title, toneURI));
            Log.e(TAG, "onResume: " + " Tone: " + title + " Tone URI: " + toneURI);
        }
        cursor.close();

        SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .putSelectedNotificationName(notificationToneList.get(1).getAudioName());
        SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .putSelectedNotifiactionURI(notificationToneList.get(1).getAudioUri().toString());

        /**For setting default ring tone*/
        RingtoneManager ringtoneManager = new RingtoneManager(mContext);
        ringtoneManager.setType(RingtoneManager.TYPE_RINGTONE);
        cursor = ringtoneManager.getCursor();
        ringtoneList.add(new AudioItem("None", null));
        while (cursor.moveToNext()) {
            String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            Uri toneURI = ringtoneManager.getRingtoneUri(cursor.getPosition());
            // Do something with the title and the URI of ringtone
            ringtoneList.add(new AudioItem(title, toneURI));
            Log.e(TAG, "onResume: " + " Tone: " + title + " Tone URI: " + toneURI);
        }

        SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .putSelectedRingtoneName(ringtoneList.get(1).getAudioName());
        SharedPreferenceManager.getSharedPreferenceManagerInstance()
                .putSelectedRingtoneURI(ringtoneList.get(1).getAudioUri().toString());

    }

}
