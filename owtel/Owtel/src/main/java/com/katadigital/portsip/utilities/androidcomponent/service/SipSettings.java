package com.katadigital.portsip.utilities.androidcomponent.service;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.portsip.object.Settings;

/**
 * Created by dcnc123 on 2/27/17.
 */
public class SipSettings {
    private static final String TAG = "SipSettings";
    //    private Context context;
    private SharedPreferences sharedPreferences;

    public SipSettings() {
//        this.context = context;
        try {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OwtelAppController.getInstance());
        } catch (NullPointerException e){
            Log.e(TAG, "Constructor " + e);
        }
    }

    /**
     * Get SIP username. If preference is empty; return default.
     *
     * @return String
     */
    public String getSipUsername() {
        return sharedPreferences.getString(Settings.PREF_USERNAME, Settings.DEFAULT_USERNAME);
    }

    /**
     * Get SIP password. If preference is empty; return default.
     *
     * @return String
     */
    public String getSipPassword() {
        return sharedPreferences.getString(Settings.PREF_PASSWORD, Settings.DEFAULT_PASSWORD);
    }

    /**
     * Get SIP server. If preference is empty; return default.
     *
     * @return String
     */
    public String getSipServer() {
        return sharedPreferences.getString(Settings.PREF_SERVER, Settings.DEFAULT_SERVER);
    }

    /**
     * Get SIP port. If preference is empty; return default.
     *
     * @return String
     */
    public String getSipPort() {
        return sharedPreferences.getString(Settings.PREF_PORT, Settings.DEFAULT_PORT);
    }

    /**
     * Get SIP protocol. If preference is empty; return default.
     *
     * @return String
     */
    public String getSipProtocol() {
        return sharedPreferences.getString(Settings.PREF_PROTOCOL, Settings.DEFAULT_PROTOCOL);
    }

    /**
     * Get SIP codecs. If preference is empty; return default.
     *
     * @return String
     */
    public String getSipCodec() {
        return sharedPreferences.getString(Settings.PREF_CODECS, Settings.DEFAULT_CODECS);
    }

    /**
     * Set SIP username
     *
     * @param username String
     */
    public void setSipUsername(String username) {
        if(username != null) sharedPreferences.edit().putString(Settings.PREF_USERNAME, username).apply();
    }

    /**
     * Set SIP password
     *
     * @param password String
     */
    public void setSipPassword(String password) {
        sharedPreferences.edit().putString(Settings.PREF_PASSWORD, password).apply();
    }

    /**
     * Set SIP server
     *
     * @param server String
     */
    public void setSipServer(String server) {
        sharedPreferences.edit().putString(Settings.PREF_SERVER, server).apply();
    }

    /**
     * Set SIP port
     *
     * @param port String
     */
    public void setSipPort(String port) {
        sharedPreferences.edit().putString(Settings.PREF_PORT, port).apply();
    }

    /**
     * Set SIP protocol
     *
     * @param protocol String
     */
    public void setSipProtocol(String protocol) {
        sharedPreferences.edit().putString(Settings.PREF_PROTOCOL, protocol).apply();
    }

    /**
     * Set SIP codec
     * @param codec String
     */
    public void setSipCodec(String codec){
        sharedPreferences.edit().putString(Settings.PREF_CODECS, codec).apply();
    }

    /**
     * Set WLAN preference
     * @param enableWlan boolean
     */
    public void setEnableWlan(boolean enableWlan){
        sharedPreferences.edit().putBoolean(Settings.PREF_WLAN, enableWlan).apply();
    }

    /**
     * Set 3G preference
     * @param enable3g boolean
     */
    public void setEnable3g(boolean enable3g){
        sharedPreferences.edit().putBoolean(Settings.PREF_3G, enable3g).apply();
    }

    /**
     * Set Edge preference
     * @param enableEdge boolean
     */
    public void setEnableEdge(boolean enableEdge){
        sharedPreferences.edit().putBoolean(Settings.PREF_EDGE, enableEdge).apply();
    }

    /**
     * Get WLAN preference
     * @return boolean
     */
    public boolean getPrefWlan(){
        return sharedPreferences.getBoolean(Settings.PREF_WLAN, Settings.DEFAULT_WLAN);
    }

    /**
     * Get 3G preference
     * @return boolean
     */
    public boolean getPref3g(){
        return sharedPreferences.getBoolean(Settings.PREF_3G, Settings.DEFAULT_3G);
    }

    /**
     * Get Edge preference
     * @return boolean
     */
    public boolean getPrefEdge(){
        return sharedPreferences.getBoolean(Settings.PREF_EDGE, Settings.DEFAULT_EDGE);
    }

    /**
     * Set earpiece gain
     * @param gain double
     */
    public void setEarpieceGain(double gain){
        sharedPreferences.edit().putString(Settings.PREF_EARGAIN, String.valueOf(gain)).apply();
    }

    /**
     * Set headset gain
     * @param gain double
     */
    public void setHeadsetGain(double gain){
        sharedPreferences.edit().putString(Settings.PREF_HEARGAIN, String.valueOf(gain)).apply();
    }

    /**
     * Set mic gain
     * @param gain double
     */
    public void setMicGain(double gain){
        sharedPreferences.edit().putString(Settings.PREF_MICGAIN, String.valueOf(gain)).apply();
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}