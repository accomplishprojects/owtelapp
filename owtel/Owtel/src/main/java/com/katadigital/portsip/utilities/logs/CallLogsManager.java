package com.katadigital.portsip.utilities.logs;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CallLogsManager {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();


    private static CallLogsManager sCallLogsManager;

    public static void init() {

        if (sCallLogsManager == null) {
            sCallLogsManager = new CallLogsManager();
        }
    }

    public static CallLogsManager getCallLogsManagerInstance() {
        if (sCallLogsManager != null) {
            return sCallLogsManager;
        }

        throw new IllegalStateException("Call CallLogsManager init() method");
    }

    public void saveCallLog(Context context, String number, int callDuration, int callType) {
        ContentValues values = new ContentValues();
        values.put(CallLog.Calls.NUMBER, number);
        values.put(CallLog.Calls.DATE, System.currentTimeMillis());
        values.put(CallLog.Calls.DURATION, callDuration);
        values.put(CallLog.Calls.TYPE, callType);
        values.put(CallLog.Calls.NEW, 1);
        values.put(CallLog.Calls.CACHED_NAME, "");
        values.put(CallLog.Calls.CACHED_NUMBER_TYPE, 0);
        values.put(CallLog.Calls.CACHED_NUMBER_LABEL, "");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "saveCallLog: " + "Requires permissions");
            return;
        }
        context.getContentResolver().insert(CallLog.Calls.CONTENT_URI, values);
    }

    public String getContactsPhoto(Context context, String number) {
        String strContactPhotoURI;
        Cursor phonesCursor = null;
        try {
            Uri phoneUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
            phonesCursor = context.getContentResolver()
                    .query(phoneUri, new String[]{ContactsContract.PhoneLookup.PHOTO_URI}, null, null, null);
        } catch (NullPointerException e) {
            Log.e(TAG, "getContactsPhoto: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "getContactsPhoto: " + e.getMessage());
        }
        if (phonesCursor != null && phonesCursor.moveToFirst()) {
            strContactPhotoURI = phonesCursor.getString(0);
        } else {
            return "";
        }

        return strContactPhotoURI;
    }

    public InputStream openDisplayPhoto(Context context, long contactId) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);
        try {
            AssetFileDescriptor fd = context.getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
            return fd.createInputStream();
        } catch (IOException e) {
            return null;
        }
    }

    public String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }

}
