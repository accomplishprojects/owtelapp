package com.katadigital.portsip.utilities.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.utilities.device.telephony.TelephonyManagerHelper;
import com.scottyab.aescrypt.AESCrypt;

import net.frakbot.glowpadbackport.util.Const;

import java.security.GeneralSecurityException;


public class SharedPreferenceManager {

    /**
     * Flags
     */
    private String TAG = this.getClass().getSimpleName();
    private static SharedPreferenceManager sSharedPreferenceManager;
    private static SharedPreferences mSharedPreferences;

    public static void init(Context context) {
        if (sSharedPreferenceManager == null) {
            TelephonyManagerHelper.init(context);
            sSharedPreferenceManager = new SharedPreferenceManager();
            mSharedPreferences = context.getSharedPreferences(Constants.SHAREDPREFERENCES_FILENAME,
                    Context.MODE_PRIVATE);
        }
    }

    public static SharedPreferenceManager getSharedPreferenceManagerInstance() {
        if (sSharedPreferenceManager != null) {
            return sSharedPreferenceManager;
        }
        throw new IllegalStateException("Call SharedPreferenceManger init()");
    }

    public void putPortSipStatus(boolean portSipStatus) {
        mSharedPreferences.edit().putBoolean(Constants.SHAREDPREFERENCES_PORTSIP_STATUS, portSipStatus)
                .apply();
    }

    public boolean getPortSipStatus() {
        return mSharedPreferences.getBoolean(Constants.SHAREDPREFERENCES_PORTSIP_STATUS, false);
    }

    public void putUsername(String username) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_CACHE_USERNAME, username)
                .apply();
    }

    public String getUsername() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_CACHE_USERNAME, null);
    }

    public void putPassword(String password) {
        try {
            String encryptedPassword = AESCrypt.encrypt(TelephonyManagerHelper
                    .getTelephonyManagerInstance().getUID(), password);
            Log.e(TAG, "putPassword: UID: " + TelephonyManagerHelper.getTelephonyManagerInstance().getUID()
                    + " password: " + password);
            Log.e(TAG, "putPassword: EncryptedPassword: " + encryptedPassword);
            mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_CACHE_PASSWORD, encryptedPassword)
                    .apply();
        } catch (GeneralSecurityException e) {
            Log.e(TAG, "putPassword: " + e.getMessage());
        }
    }

    public String getPassword() {
        String encryptedPassword = mSharedPreferences.getString(Constants.SHAREDPREFERENCES_CACHE_PASSWORD, null);
        Log.e(TAG, "getPassword: EncryptedPassword: " + encryptedPassword);
        String decryptedPassword = "";
        try {
            if (encryptedPassword != null) {
                decryptedPassword = AESCrypt.decrypt(TelephonyManagerHelper
                        .getTelephonyManagerInstance().getUID(), encryptedPassword);
                Log.e(TAG, "getPassword: DecryptedPassword: " + decryptedPassword);
            }
        } catch (GeneralSecurityException e) {
            Log.e(TAG, "getPassword: " + e.getMessage());
        }
        return decryptedPassword;
    }

    public void putUserRememberStatus(boolean status) {
        mSharedPreferences.edit().putBoolean(Constants.SHAREDPREFERENCES_USER_REMEMBER_STATUS, status)
                .apply();
    }

    public boolean getUserRememberStatus() {
        return mSharedPreferences.getBoolean(Constants.SHAREDPREFERENCES_USER_REMEMBER_STATUS, true);
    }

    public void putRadId(String radId) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_RAD_ID, radId)
                .apply();
    }

    public String getRadId() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_RAD_ID, null);
    }

    public void putOwtelNumber(String owtelNumber) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_OWTEL_NUMBER, owtelNumber)
                .apply();
    }

    public String getOwtelNumber() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_OWTEL_NUMBER, null);
    }

    /**
     * Settings Details
     */
    public void putIDDCredits(String IddCredits) {
        mSharedPreferences.edit().putString(Constants.PREFS_USER_IDD_CREDITS, IddCredits).apply();
    }

    public String getIDDCredits() {
        return mSharedPreferences.getString(Constants.PREFS_USER_IDD_CREDITS, "");
    }

    /**
     * Ringtone name
     */
    public void putSelectedRingtoneName(String ringtoneName) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_SELECTED_RINGTONE_NAME, ringtoneName)
                .apply();
    }

    public String getSelectedRingtoneName() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_SELECTED_RINGTONE_NAME, null);
    }

    /**
     * Ringtone URI
     */
    public void putSelectedRingtoneURI(String ringtoneUri) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_SELECTED_RINGTONE_URI, ringtoneUri)
                .apply();
    }

    public String getSelectedRingtoneURI() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_SELECTED_RINGTONE_URI, null);
    }

    /**
     * Ringtone Vibration
     */
    public void putRingtoneVibrationStatus(boolean status) {
        mSharedPreferences.edit().putBoolean(Constants.SHAREDPREFERENCES_RINGTONE_VIBRATION, status)
                .apply();
    }

    public boolean getRingtoneVibrationStatus() {
        return mSharedPreferences.getBoolean(Constants.SHAREDPREFERENCES_RINGTONE_VIBRATION, true);
    }

    /**
     * Notification name
     */
    public void putSelectedNotificationName(String notificationName) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_SELECTED_NOTIF_NAME, notificationName)
                .apply();
    }

    public String getSelectedNotificationName() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_SELECTED_NOTIF_NAME, null);
    }


    public void putSelectedNotifiactionURI(String notifUri) {
        mSharedPreferences.edit().putString(Constants.SHAREDPREFERENCES_SELECTED_NOTIF_URI, notifUri)
                .apply();
    }

    public String getSelectedNotificationURI() {
        return mSharedPreferences.getString(Constants.SHAREDPREFERENCES_SELECTED_NOTIF_URI, null);
    }

    /**
     * Ringtone Vibration
     */
    public void putNotificationVibrationStatus(boolean status) {
        mSharedPreferences.edit().putBoolean(Constants.SHAREDPREFERENCES_NOTIF_VIBRATION, status)
                .apply();
    }

    public boolean getNotificationVibrationStatus() {
        return mSharedPreferences.getBoolean(Constants.SHAREDPREFERENCES_NOTIF_VIBRATION, true);
    }


    /**
     * Conversation Tone Status
     */
    public void putConvoToneStatus(boolean status) {
        mSharedPreferences.edit().putBoolean(Constants.SHAREDPREFERENCES_CONVO_TONE_STATUS, status)
                .apply();
    }

    public boolean getConvoToneStatus() {
        return mSharedPreferences.getBoolean(Constants.SHAREDPREFERENCES_CONVO_TONE_STATUS, true);
    }

    /**
     * Badge Count
     */
    public void putSMSBadgeCount(int count) {
        mSharedPreferences.edit().putInt(Constants.SHAREDPREFERENCES_SMS_BADGE_COUNT, count)
                .apply();
    }

    public int getSMSBadgeCount() {
        return mSharedPreferences.getInt(Constants.SHAREDPREFERENCES_SMS_BADGE_COUNT, 0);
    }

    public void putMissedCallBadgeCount(int count) {
        mSharedPreferences.edit().putInt(Constants.SHAREDPREFERENCES_MISSED_CALL_BADGE_COUNT, count)
                .apply();
    }

    public int getMissedCallBadgeCount() {
        return mSharedPreferences.getInt(Constants.SHAREDPREFERENCES_MISSED_CALL_BADGE_COUNT, 0);
    }

    public void clearAllDataFromShared() {
        mSharedPreferences.edit().clear().apply();
    }

}
