package com.katadigital.portsip.utilities.androidcomponent.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.CallLog;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Chronometer;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.helper.Line;
import com.katadigital.portsip.helper.SettingConfig;
import com.katadigital.portsip.helper.UserInfo;
import com.katadigital.portsip.utilities.androidcomponent.interfaces.PortSipContract;
import com.katadigital.portsip.utilities.converter.DataConverters;
import com.katadigital.portsip.utilities.logs.CallLogsManager;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;
import com.portsip.PortSipEnumDefine;
import com.portsip.PortSipErrorcode;
import com.portsip.PortSipSdk;
import com.securepreferences.SecurePreferences;

import java.util.Random;

public class PortSipService extends Service {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * PortSipSdk Variables
     */
    OwtelAppController app;
    PortSipSdk mPortSipSdk;
    String statusString;
    String LogPath = null;

    /**
     * Views Variables
     */
    Chronometer mChronometer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: " + intent.getAction());
        /**Initialize Chronometer*/
        SharedPreferenceManager.init(getApplicationContext());
        initializePortSipSdk(getApplicationContext());

        switch (intent.getAction()) {
            case Constants.PORTSIP_REGISTER_ON_SERVICE:
                if (!app.isOnline()) {
                    Log.e(TAG, "onStart: " + "Registering...");
                    online(getApplicationContext(), getSipSettings());
                } else {
                    Log.e(TAG, "onStart: " + "Registered");
                }
                break;

            case Constants.PORTSIP_UNREGISTER_ON_SERVICE:
                unregisteringFromPortSipSDK();
                break;

            case Constants.PORTSIP_RESET_ON_SERVICE:
                online(getApplicationContext(), getSipSettings());
                break;

            case Constants.PORTSIP_CALL_CONNECTED_ON_SERVICE:
                mChronometer.setBase(SystemClock.elapsedRealtime());
                mChronometer.start();
                app.setChronometerStatus(true);
                Log.e(TAG, "onStartCommand: Call Connected" + mChronometer.getText());
                break;

            case Constants.PORTSIP_CALL_CLOSE_ON_SERVICE:
                String time = DataConverters.convertMillisToMinutesAndSeconds(
                        SystemClock.elapsedRealtime() - mChronometer.getBase());

                /**Get intent from PortSipCallScreenActivity*/
                boolean isIncomingCall = intent.getBooleanExtra("isIncomingCall", false);
                boolean isChronometerIsRunning = intent.getBooleanExtra("isChronometerIsRunning", false);
                String strCallerDisplayName = intent.getStringExtra("callerDisplayName");
                String strNumberToCall = intent.getStringExtra("NumberToCall");

                if (isIncomingCall) {
                    if (app.isCallMissed()) {
                        CallLogsManager.getCallLogsManagerInstance().saveCallLog(
                                this, strCallerDisplayName,
                                DataConverters.convertTimeToSeconds(time),
                                CallLog.Calls.MISSED_TYPE);
                        Log.e(TAG, "onStartCommand: Missed Call: " + time);

                        /**Badge Count*/
                        int missedCallCount = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                                .getMissedCallBadgeCount();
                        SharedPreferenceManager.getSharedPreferenceManagerInstance()
                                .putMissedCallBadgeCount(++missedCallCount);
                    } else {
                        CallLogsManager.getCallLogsManagerInstance().saveCallLog(
                                this, strCallerDisplayName,
                                DataConverters.convertTimeToSeconds(time),
                                CallLog.Calls.INCOMING_TYPE);
                        app.setCallMissed(true);
                        Log.e(TAG, "onStartCommand: Incoming Call: " + time);
                    }
                } else {
                    /**Resetting chronometer if outgoing call is canceled and not answer by the callee*/
                    if (!isChronometerIsRunning) {
                        mChronometer.setBase(SystemClock.elapsedRealtime());
                        time = mChronometer.getText().toString();
                        Log.e(TAG, "onStartCommand: Canceled: " + time);
                    }

                    CallLogsManager.getCallLogsManagerInstance().saveCallLog(
                            this, strNumberToCall,
                            DataConverters.convertTimeToSeconds(time),
                            CallLog.Calls.OUTGOING_TYPE);

                    Log.e(TAG, "onStartCommand:  Call Close: " + time);
                }
                mChronometer.stop();
                app.setChronometerStatus(false);
                break;
        }

        return START_NOT_STICKY;
    }

    private void initializePortSipSdk(Context context) {
        app = (OwtelAppController) context.getApplicationContext();
        mPortSipSdk = app.getPortSipSdk();

        SetTransType(context, PortSipEnumDefine.ENUM_SRTPPOLICY_NONE);
        SetSRTPType(context, PortSipEnumDefine.ENUM_SRTPPOLICY_NONE);

        mChronometer = app.getChronometer();
    }

    /**
     * This is the method for Un-registering the user into PortSipSDK
     */
    private void unregisteringFromPortSipSDK() {
        Line[] mLines = app.getLines();
        for (int i = Line.LINE_BASE; i < Line.MAX_LINES; ++i) {
            if (mLines[i].getRecvCallState()) {
                mPortSipSdk.rejectCall(mLines[i].getSessionId(), 486);
            } else if (mLines[i].getSessionState()) {
                mPortSipSdk.hangUp(mLines[i].getSessionId());
            }
            mLines[i].reset();
        }
        app.setLoginState(false);
        mPortSipSdk.unRegisterServer();
        mPortSipSdk.DeleteCallManager();
        NotificationHelper.getNotificationHelperInstance()
                .unnotifyPortSipStatus();

    }

    private int online(Context context, SipSettings sipSettings) {

        Log.e(TAG, "Updated USERNAME: " + sipSettings.getSipUsername());
        Log.e(TAG, "Updated PASSWORD: " + sipSettings.getSipPassword());
        Log.e(TAG, "Updated SERVER: " + sipSettings.getSipServer());
        Log.e(TAG, "Updated PORT: " + sipSettings.getSipPort());
        Log.e(TAG, "Updated PROTOCOL: " + sipSettings.getSipProtocol());

        int result = setUserInfo(context, sipSettings);

        if (result == PortSipErrorcode.ECoreErrorNone) {
            result = mPortSipSdk.registerServer(90, 3);
            if (result != PortSipErrorcode.ECoreErrorNone) {
                statusString = "Register server failed";
                Log.e(TAG, "online: " + statusString);
                app.setLoginState(false);
            } else {
                Log.e(TAG, "online: " + "PortSip Result: " + result);
            }
        } else {
            Log.e(TAG, "online: " + "PortSipSdk Not Registered");
            app.setLoginState(false);
        }

        return result;
    }

    private SipSettings getSipSettings() {
        SipSettings sipSettings = new SipSettings();
        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        String email = preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty_email");
        String pass = preferencesUser.getString(Constants.PREFS_USER_PASS, "empty_pass");
        boolean userType = preferencesUser.getBoolean(Constants.PREFS_USER_TYPE, false);
        String key = email + pass;

        SecurePreferences securePreferences = OwtelAppController.getInstance().getUserPinBasedSharedPreferences(key);
//        String sipNumber = SharedPreferenceManager.getSharedPreferenceManagerInstance().getOwtelNumber();
        String sipNumber = securePreferences.getString(Constants.PREFS_USER_NUMBER, "empty_number");
        String sipPass = securePreferences.getString(Constants.PREFS_SIP_PASS, "empty_sip_pass");
        String sipServer = securePreferences.getString(Constants.PREFS_SIP_SERVER, "empty_sip_server");
        String sipPort = securePreferences.getString(Constants.PREFS_SIP_PORT, "empty_sip_port");
        String radId = securePreferences.getString(Constants.PREFS_RAD_ID, "empty_rad_id");

        SharedPreferences prefs = getSharedPreferences(Constants.PREFS_RAD_NAME, MODE_PRIVATE);
        radId = prefs.getString(Constants.PREFS_RAD_ID, null);

        sipSettings.setSipUsername(sipNumber);
        sipSettings.setSipPassword(sipPass);
        sipSettings.setSipServer(sipServer);
        sipSettings.setSipPort(sipPort);
//        sipSettings.setMicGain(0.1);
//        sipSettings.setEarpieceGain(1.0);
//        sipSettings.setHeadsetGain(1.0);
        return sipSettings;
    }

    int setUserInfo(Context context, SipSettings sipSettings) {
        Environment.getExternalStorageDirectory();
        LogPath = Environment.getExternalStorageDirectory().getAbsolutePath() + '/';

//		String localIP = myApplication.getLocalIP(false);// ipv4
        String localIP = "0.0.0.0";
        int localPort = new Random().nextInt(4940) + 5060;
        UserInfo info = saveUserInfo(context, sipSettings);

        if (info.isAvailable()) {
            mPortSipSdk.CreateCallManager(context.getApplicationContext());

            int result = mPortSipSdk.initialize(info.getTransType(),
                    PortSipEnumDefine.ENUM_LOG_LEVEL_NONE, LogPath,
                    Line.MAX_LINES, "PortSIP VoIP SDK for Android",
                    0, 0);

            if (result != PortSipErrorcode.ECoreErrorNone) {
                statusString = "Initializing PortSip Sdk Failed";
                Log.e(TAG, "setUserInfo: " + statusString);
                return result;
            }

            mPortSipSdk.setSrtpPolicy(info.getSrtpType());

            int nSetKeyRet = mPortSipSdk.setLicenseKey(Constants.PORTSIP_LICENSE_KEY);
            if (nSetKeyRet == PortSipErrorcode.ECoreTrialVersionLicenseKey) {
                Log.e(TAG, "setUserInfo: " + context.getString(R.string.trial_version_tips));

            } else if (nSetKeyRet == PortSipErrorcode.ECoreWrongLicenseKey) {
                Log.e(TAG, "setUserInfo: " + context.getString(R.string.wrong_lisence_tips));
                return -1;
            }

            Log.e(TAG, "setUserInfo: " + "Username: " + info.getUserName() +
                    " DisplayName: " + info.getUserDisplayName() +
                    " AuthName: " + info.getAuthName() +
                    " Password: " + info.getUserPassword() +
                    " localIP: " + localIP +
                    " localPort: " + localPort +
                    " UserDomain: " + info.getUserdomain() +
                    " SipServer: " + info.getSipServer() +
                    " SipPort: " + info.getSipPort() +
                    " StunServer: " + info.getStunServer() +
                    " StunPort: " + info.getStunPort());

            result = mPortSipSdk.setUser(info.getUserName(), info.getUserDisplayName(), info.getAuthName(),
                    info.getUserPassword(), localIP, localPort, info.getUserdomain(), info.getSipServer(),
                    info.getSipPort(), info.getStunServer(), info.getStunPort(), null, 5060);

            if (result != PortSipErrorcode.ECoreErrorNone) {
                statusString = "Set user resource failed";
                Log.e(TAG, "setUserInfo: " + statusString);
                return result;
            }
        } else {
            return -1;
        }

        SettingConfig.setAVArguments(context, mPortSipSdk);
        return PortSipErrorcode.ECoreErrorNone;
    }

    private UserInfo saveUserInfo(Context context, SipSettings sipSettings) {

        int port;
        UserInfo userInfo = new UserInfo();

        String item = sipSettings.getSipUsername();
        userInfo.setUserName(item);
        item = sipSettings.getSipPassword();
        userInfo.setUserPwd(item);
        item = sipSettings.getSipServer();
        userInfo.setSipServer(item);
        item = sipSettings.getSipPort();
        try {
            port = Integer.parseInt(item);
        } catch (NumberFormatException e) {
            port = 5060;
        }
        userInfo.setSipPort(port);
//        not used for now
//        item = ((EditText) view.findViewById(R.id.etuserdomain)).getText().toString();
//        userInfo.setUserDomain(item);
//        item = ((EditText) view.findViewById(R.id.etauthName)).getText().toString();
//        userInfo.setAuthName(item);
//        item = ((EditText) view.findViewById(R.id.etdisplayname)).getText().toString();
//        userInfo.setUserDisplayName(item);
//        item = ((EditText) view.findViewById(R.id.etStunServer)).getText().toString();
//        userInfo.setStunServer(item);
//        item = ((EditText) view.findViewById(R.id.etStunPort)).getText().toString();
        try {
            port = Integer.parseInt(item);
        } catch (NumberFormatException e) {
            port = 5060;
        }

        userInfo.setStunPort(port);
        userInfo.setTranType(SettingConfig.getPortsipTranstype(context));
        userInfo.setSrtpType(SettingConfig.getSrtpType(context));

        SettingConfig.setUserInfo(context, userInfo);
        return userInfo;
    }

    private void SetSRTPType(Context context, int index) {

        int SrtType = PortSipEnumDefine.ENUM_SRTPPOLICY_NONE;

        switch (index) {
            case 0:
                SrtType = PortSipEnumDefine.ENUM_SRTPPOLICY_NONE;
                break;
            case 1:
                SrtType = PortSipEnumDefine.ENUM_SRTPPOLICY_FORCE;
                break;
            case 2:
                SrtType = PortSipEnumDefine.ENUM_SRTPPOLICY_PREFER;
                break;

        }

        SettingConfig.setSrtpType(context, SrtType, mPortSipSdk);
    }

    private void SetTransType(Context context, int index) {

        int transType = PortSipEnumDefine.ENUM_TRANSPORT_UDP;

        switch (index) {
            case 0:
                transType = PortSipEnumDefine.ENUM_TRANSPORT_UDP;
                break;

            case 1:
                transType = PortSipEnumDefine.ENUM_TRANSPORT_TLS;
                break;
            case 2:
                transType = PortSipEnumDefine.ENUM_TRANSPORT_TCP;
                break;

            case 3:
                transType = PortSipEnumDefine.ENUM_TRANSPORT_PERS;
                break;
        }

        SettingConfig.setTransType(context, transType);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.e(TAG, "onTaskRemoved: ");
        NotificationHelper.init(getApplicationContext());
        NotificationHelper.getNotificationHelperInstance().unnotifyPortSipStatus();
        getApplicationContext().sendBroadcast(new Intent(Constants.PORTSIP_REGISTER_ON_RECEIVER));
    }
}
