package com.katadigital.portsip.utilities.androidcomponent.receiver;

import android.app.Notification;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.modules.login.LoginActivity;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.utilities.androidcomponent.interfaces.PortSipContract;
import com.katadigital.portsip.utilities.androidcomponent.service.PortSipService;
import com.katadigital.portsip.utilities.device.bluetooth.BluetoothManager;
import com.katadigital.portsip.utilities.network.NetworkHelper;
import com.katadigital.portsip.utilities.notifications.NotificationHelper;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;

public class PortSipReceiver extends BroadcastReceiver {

    /**
     * Flags
     */
    String TAG = this.getClass().getSimpleName();

    /**
     * Variables
     */
    OwtelAppController app;
    PortSipContract.PortSipContractListener mPortSipContractListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive: " + intent.getAction());
        NetworkHelper.init();
        initializePortSipSdk(context);

        switch (intent.getAction()) {
            case Intent.ACTION_BOOT_COMPLETED:
                Log.e(TAG, "onReceive: ");
                context.startService(new Intent(context, PortSipService.class)
                        .setAction(Constants.PORTSIP_REGISTER_ON_SERVICE));
                break;

            case Constants.PORTSIP_REGISTER_ON_RECEIVER:
                context.startService(new Intent(context, PortSipService.class)
                        .setAction(Constants.PORTSIP_REGISTER_ON_SERVICE));
                break;

            case Constants.PORTSIP_UNREGISTER_ON_RECEIVER:
                context.startService(new Intent(context, PortSipService.class)
                        .setAction(Constants.PORTSIP_UNREGISTER_ON_SERVICE));
                break;

            case Constants.PORTSIP_RESET_ON_RECEIVER:
                context.startService(new Intent(context, PortSipService.class)
                        .setAction(Constants.PORTSIP_RESET_ON_SERVICE));
                break;

            case ConnectivityManager.CONNECTIVITY_ACTION:
                int networkStatus = NetworkHelper.getNetworkHelperInstance().getConnectivityStatus(context);
                Log.e(TAG, "onReceive: NetworkStatus: " + networkStatus);
                String networkType = (networkStatus == 0) ? "Connection not Available" : "Connected to Wifi/Mobile data";
                Log.e(TAG, "onReceive: " + networkType);

                switch (networkStatus) {
                    case Constants.NETWORK_TYPE_NOT_CONNECTED:
                        NotificationHelper.getNotificationHelperInstance()
                                .notifyPortSipStatus(context, "Owtel",
                                        "Offline (" + SharedPreferenceManager.getSharedPreferenceManagerInstance()
                                                .getOwtelNumber() + ")", R.drawable.ic_owtel_icon_notification);
                        break;
                    default:
                        NotificationHelper.getNotificationHelperInstance()
                                .notifyPortSipStatus(context, "Owtel",
                                        "Online (" + SharedPreferenceManager.getSharedPreferenceManagerInstance()
                                                .getOwtelNumber() + ")", R.drawable.ic_owtel_icon_notification);
                        break;
                }
                break;

            case "android.bluetooth.adapter.action.STATE_CHANGED":
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Toast.makeText(context, "Bluetooth Off", Toast.LENGTH_SHORT).show();
                        if (app.getActivity() != null) {
                            mPortSipContractListener.onBluetoothStatusChange(false);
                        }
                        break;

                    case BluetoothAdapter.STATE_ON:
                        Toast.makeText(context, "Bluetooth On", Toast.LENGTH_SHORT).show();
                        if (app.getActivity() != null) {
                            mPortSipContractListener.onBluetoothStatusChange(true);
                        }
                        break;
                }
        }
    }

    private void initializePortSipSdk(Context context) {
        app = (OwtelAppController) context.getApplicationContext();
        if (app.getActivity() != null) {
            mPortSipContractListener = (PortSipContract.PortSipContractListener) app.getActivity();
        }
    }
}
