package com.katadigital.portsip.utilities.converter;

import android.database.Cursor;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataConverters {


    public static int convertTimeToSeconds(String time) {
        String[] h1 = time.split(":");
//        int hour = Integer.parseInt(h1[0]);
        int minute = Integer.parseInt(h1[0]);
        int second = Integer.parseInt(h1[1]);
        int temp = second + (60 * minute) /*+ (3600 * hour)*/;
        return temp;
    }

    public static String convertMillisToMinutesAndSeconds(long millisecond) {
        return new SimpleDateFormat("mm:ss").format(new Date(millisecond));
    }

    public static byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
}
