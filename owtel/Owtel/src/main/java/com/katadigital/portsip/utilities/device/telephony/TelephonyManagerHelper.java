package com.katadigital.portsip.utilities.device.telephony;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class TelephonyManagerHelper {

    private static TelephonyManagerHelper sTelephonyManagerHelper;
    private static TelephonyManager mTelephonyManager;
    private static String strDeviceUID;

    public static void init(Context context) {
        if (sTelephonyManagerHelper == null) {
            sTelephonyManagerHelper = new TelephonyManagerHelper();
            mTelephonyManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            strDeviceUID = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }
    }

    public static TelephonyManagerHelper getTelephonyManagerInstance() {
        if (sTelephonyManagerHelper != null) {
            return sTelephonyManagerHelper;
        }
        throw new IllegalStateException("Call TelephonyManager init()");
    }

    public String getImei() {
        return mTelephonyManager.getDeviceId();
    }

    public String getUID() {
        return strDeviceUID;
    }

}
