package com.katadigital.portsip.utilities.device.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;

public class BluetoothManager {

    public static boolean isBluetoothHeadsetConnected() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()
                && mBluetoothAdapter.getProfileConnectionState(BluetoothHeadset.HEADSET)
                == BluetoothHeadset.STATE_CONNECTED;
    }
}
