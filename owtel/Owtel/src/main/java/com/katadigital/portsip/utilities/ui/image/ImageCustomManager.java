package com.katadigital.portsip.utilities.ui.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.kata.phone.R;
import com.katadigital.portsip.utilities.logs.CallLogsManager;

import java.io.ByteArrayOutputStream;

public class ImageCustomManager {

    /**
     * Flags
     */
    private String TAG = this.getClass().getSimpleName();

    private static ImageCustomManager sImageCustomManager;

    public static void init() {
        if (sImageCustomManager == null) {
            sImageCustomManager = new ImageCustomManager();
            CallLogsManager.init();
        }
    }

    public static ImageCustomManager getImageCustomManagerInstance() {
        if (sImageCustomManager != null) {
            return sImageCustomManager;
        }
        throw new IllegalStateException("Call ImageCustomManager init() method");
    }

    public void displayProfilePictureFromBitmap(Context context, Bitmap bitmap, ImageView view) {
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            Glide.with(context)
                    .load(stream.toByteArray())
                    .asBitmap()
                    .placeholder(R.drawable.img_call_user_default)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(view);
        } else {
            Glide.with(context)
                    .load("")
                    .placeholder(R.drawable.img_call_user_default)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(view);
        }
    }

    public void displayProfilePictureFromUri(Context context, String number, ImageView view) {
        try {
            Glide.with(context)
                    .load(Uri.parse(CallLogsManager.getCallLogsManagerInstance()
                            .getContactsPhoto(context, number)))
                    .placeholder(R.drawable.img_call_user_default)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(view);
        } catch (Exception e) {
            Log.e(TAG, "displayProfilePictureFromUri: " + e.getMessage());
            Glide.with(context)
                    .load("")
                    .placeholder(R.drawable.img_call_user_default)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(view);
        }
    }
}
