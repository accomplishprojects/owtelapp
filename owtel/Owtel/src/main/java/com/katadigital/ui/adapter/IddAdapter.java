package com.katadigital.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.IddList;

import java.util.List;

/**
 * Created by Omar Matthew Reyes on 5/17/16.
 * Spinner Adapter for IDD top-up price list
 */

public class IddAdapter extends BaseAdapter {

    String TAG = this.getClass().getSimpleName();
    private List<IddList> iddList;

    public IddAdapter(List<IddList> iddList) {
        this.iddList = iddList;
    }

    @Override
    public int getCount() {
        return iddList.size();
    }

    @Override
    public Object getItem(int position) {
        return iddList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) OwtelAppController.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_payment, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvIddPrice = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.tvIddPrice.setText(iddList.get(position).getIddReloadPrice());

        return convertView;
    }

    private class ViewHolder {
        public TextView tvIddPrice;
    }
}