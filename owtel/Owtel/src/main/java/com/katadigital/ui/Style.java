package com.katadigital.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;

/**
 * Created by Omar Matthew Reyes on 5/19/16.
 * Custom styles
 */
public class Style {
    /**
     * Set status bar color
     *
     * @param color int
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void statusBarColor(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(color));
        }
    }
}
