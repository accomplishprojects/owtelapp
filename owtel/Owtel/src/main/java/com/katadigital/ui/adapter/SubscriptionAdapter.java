package com.katadigital.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.SubscriptionList;

import java.util.List;

/**
 * Created by Omar Matthew Reyes on 5/17/16.
 * Spinner Adapter for IDD top-up price list
 */

public class SubscriptionAdapter extends BaseAdapter {
    private List<SubscriptionList> subscriptionList;

    public SubscriptionAdapter(List<SubscriptionList> subscriptionList) {
        this.subscriptionList = subscriptionList;
    }

    @Override
    public int getCount() {
        return subscriptionList.size();
    }

    @Override
    public Object getItem(int position) {
        return subscriptionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) OwtelAppController.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_payment, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvIddPrice = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();


        viewHolder.tvIddPrice.setText(subscriptionList.get(position).getSubscriptionPrice());
        return convertView;
    }

    private class ViewHolder {
        public TextView tvIddPrice;
    }
}
