package com.katadigital.ui.slidingtab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.settings.SettingsFragment;

import java.util.ArrayList;


/**
 * Created by Omar Matthew Reyes on 6/11/15.
 * ViewPager for MainActivity for displaying main Fragments
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private static int[] mTabIcons = new int[]{
            R.drawable.ic_tab_selector_favorites,
            R.drawable.ic_tab_selector_logs,
            R.drawable.ic_tab_selector_contacts,
            R.drawable.ic_tab_selector_dialer,
            R.drawable.ic_tab_selector_messages
    };

    public ArrayList<Fragment> fr_list = new ArrayList<Fragment>();


//    // Build a Constructor and assign the passed Values to appropriate values in the class
//    public ViewPagerAdapter(FragmentManager mFragmentManager) {
//        super(mFragmentManager);
//    }

    public ViewPagerAdapter(FragmentManager supportFragmentManager, ArrayList<Fragment> fr_list) {
        super(supportFragmentManager);
        this.fr_list=fr_list;

    }

    //This method return the fragment for the every position in the View Pager
//    @Override
//    public Fragment getItem(int position) {
//        Fragment fragment = null;
//        Bundle args = new Bundle();
//        args.putBoolean(Constants.BUNDLE_HIDE_BTN_BACK, true);
//        SettingsFragment settingsFragment = new SettingsFragment();
//        settingsFragment.setArguments(args);
//        switch (position) {
//            case 0:
//                fragment = settingsFragment;
//                break;
//            case 1:
//                fragment = new FavoritesFragment();
//                break;
//            case 2:
//                fragment = new LogsFragment();
//                break;
//            case 3:
//                fragment = new ContactsFragment();
//                break;
//            case 4:
//                fragment = new DialerFragment();
//                break;
//            case 5:
//                fragment = new ConversationsFragment();
//                break;
//            case 6:
//                fragment = settingsFragment;
//                break;
//        }
//
//        return fragment;
//    }

    @Override
    public Fragment getItem(int position)
    {
        return fr_list.get(position);
    }


    /**
     * This method return the titles for the Tabs in the Tab Strip
     *
     * @param position active tab
     * @return null we will not display any text on the tab
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    /**
     * Get number of tabs
     *
     * @return int tab count
     */
//    @Override
//    public int getCount() {
//        return 7;
//    }
    public int getCount() {

        return fr_list.size();

    }
    /**
     * Get icon drawable id for the tabs
     *
     * @param position active tab
     * @return int icon drawable id
     */
    public static int getDrawableId(int position) {
        return mTabIcons[position];
    }

    @Override
    public int getItemPosition(Object object) {
        SettingsFragment f = (SettingsFragment ) object;
        if (f != null) {
            f.onResume();
        }
        return super.getItemPosition(object);
    }
}
