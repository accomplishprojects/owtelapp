package com.katadigital.ui.recyclerview;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;

import android.provider.Settings.System;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.modules.soundsettings.NotificationSettingFragment;
import com.katadigital.owtel.modules.soundsettings.RingtoneSettingFragment;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.portsip.object.Settings;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;

/**
 * Created by Omar Matthew Reyes on 2/19/16.
 * <p>
 * RecyclerView adapter for SettingsFragment list
 */
public class SoundAndVibrateRecyclerViewAdapter extends RecyclerView.Adapter<SoundAndVibrateRecyclerViewAdapter.ViewHolder> {
    //    private ArrayList<AudioItem> audioLists = new ArrayList<>();
    private String[] mDataset;
    private Context context;
    private MainActivity mainActivity;
    private boolean userType;
    private static final int
            SETTINGS_PHONE_NOTIFICATION = 0,
            SETTINGS_PHONE_RINGTONE = 1,
            SETTINGS_VIBRATE_WHILE_RINGING = 2,
            SETTINGS_VIBRATE_ON_NOTIFICATION = 3,
            SETTINGS_NOTIFICATION_SOUND = 4,
            SETTINGS_CONVO_TONE_SOUND = 5;

    private static final String TAG = "SettingsRecycler";
    private MainActivityBridge mMainActivityBridge;
    private int devClick = 0;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView mTextViewSettingsItem, mTextViewSettingsItemDetail;
        protected ImageView mImageViewArrow;
        protected ToggleButton toggleButton;
        protected Context context;

        public ViewHolder(View v, Context context) {
            super(v);
            mTextViewSettingsItem = (TextView) v.findViewById(R.id.tv_settings_item);
            mTextViewSettingsItemDetail = (TextView) v.findViewById(R.id.tv_settings_item_detail);
            mImageViewArrow = (ImageView) v.findViewById(R.id.img_settings_clickable_indicator);
            toggleButton = (ToggleButton) v.findViewById(R.id.toggle_sound_vibrate_settings);
            this.context = context;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "RecyclerView clicked at " + getAdapterPosition());
            switch (getAdapterPosition()) {
                case SETTINGS_PHONE_NOTIFICATION:
                    mainActivity.changeFragment(new NotificationSettingFragment(), SoundAndVibrateRecyclerViewAdapter.TAG);
                    break;
                case SETTINGS_PHONE_RINGTONE:
                    mainActivity.changeFragment(new RingtoneSettingFragment(), SoundAndVibrateRecyclerViewAdapter.TAG);
                    break;
//                case SETTINGS_VIBRATE_WHILE_RINGING:
//                    break;
//                case SETTINGS_VIBRATE_ON_NOTIFICATION:
//                    break;
            }
        }
    }

    public SoundAndVibrateRecyclerViewAdapter(String[] myDataset, Context context) {
        this.mDataset = myDataset;
        this.context = context;
        this.userType = new Functions().getUserType();
        this.mainActivity = (MainActivity) context;
        this.mMainActivityBridge = (MainActivityBridge) context;
        SharedPreferenceManager.init(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // create a new view
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_item_settings_sound_and_vibrate, viewGroup, false);
        return new ViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        holder.mTextViewSettingsItem.setText(mDataset[position]);
        switch (position) {
            case SETTINGS_PHONE_NOTIFICATION:
                String uriString = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREFS_NOTIFICATION,
                        RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString());
                Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(uriString));
                String title = ringtone.getTitle(context);

                String notifName = SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getSelectedNotificationName();
                Log.e(TAG, "onBindViewHolder: " + notifName);
                if (notifName != null) {
                    holder.mTextViewSettingsItemDetail.setText(notifName);
                } else {
                    holder.mTextViewSettingsItemDetail.setText("None");
                }

                String titleNotif = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREFS_NOTIFICATION,
                        null);
                if (titleNotif != null && titleNotif.equals("")) {
                    holder.mTextViewSettingsItemDetail.setText("None");
                }
                ringtone.stop();
                holder.mTextViewSettingsItemDetail.setTypeface(null, Typeface.BOLD);
                holder.mImageViewArrow.setVisibility(View.VISIBLE);
                holder.toggleButton.setVisibility(View.GONE);
                break;
            case SETTINGS_PHONE_RINGTONE:

                String uriString1 = PreferenceManager.getDefaultSharedPreferences(context)
                        .getString(Settings.PREF_SIPRINGTONE,
                                System.DEFAULT_RINGTONE_URI.toString());
//                String a = !TextUtils.isEmpty(uriString) ? Uri.parse(uriString) : null;

                Ringtone ringtone1 = RingtoneManager.getRingtone(context, Uri.parse(uriString1));
                String title1 = ringtone1.getTitle(context);
                String ringToneName = SharedPreferenceManager.getSharedPreferenceManagerInstance().getSelectedRingtoneName();
                if (ringToneName != null) {
                    holder.mTextViewSettingsItemDetail.setText(ringToneName);
                } else {
                    holder.mTextViewSettingsItemDetail.setText("None");
                }

                String titleRingTone = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.PREFS_NOTIFICATION, null);

                if (titleRingTone != null && titleRingTone.equals("")) {
                    holder.mTextViewSettingsItemDetail.setText("None");
                }
                ringtone1.stop();
                holder.mTextViewSettingsItemDetail.setTypeface(null, Typeface.BOLD);
                holder.mImageViewArrow.setVisibility(View.VISIBLE);
                holder.toggleButton.setVisibility(View.GONE);
                break;
            case SETTINGS_VIBRATE_WHILE_RINGING:
                holder.toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putRingtoneVibrationStatus(isChecked);
//                    Toast.makeText(context, "" + isChecked, Toast.LENGTH_SHORT).show();
                });

                holder.toggleButton.setChecked(SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getRingtoneVibrationStatus());

                holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                holder.toggleButton.setVisibility(View.VISIBLE);

                break;
            case SETTINGS_VIBRATE_ON_NOTIFICATION:
                holder.toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putNotificationVibrationStatus(isChecked);
                });
                holder.toggleButton.setChecked(SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getNotificationVibrationStatus());

                holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                holder.toggleButton.setVisibility(View.VISIBLE);
                break;
            case SETTINGS_NOTIFICATION_SOUND:
                holder.toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putConvoToneStatus(isChecked);
                });
                holder.toggleButton.setChecked(SharedPreferenceManager.getSharedPreferenceManagerInstance()
                        .getConvoToneStatus());
                holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                holder.mImageViewArrow.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

//    /**
//     * Interface for Stop running services
//     */
//    public interface ServiceKiller {
//        void stopServices(Context context);
//    }
//
//    /**
//     * Get Address Country List
//     *
//     * @param paymentType String
//     */
//    public void getAddressCountryListRequestTopup(String paymentType) {
//        Bundle bundle = new Bundle();
//        mainActivity.mProgressDialog.setMessage(mainActivity.getResources().getString(R.string.processing));
//        mainActivity.mProgressDialog.show();
//        ApiHelper.doGetAddressList(mainActivity.mProgressDialog, mainActivity, GlobalValues.GET_COUNTRY, "", "").subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<List<AddressList>>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onNext(List<AddressList> addresses) {
//                        Parcelable listAddressParcel = Parcels.wrap(addresses);
//                        bundle.putParcelable("countries", listAddressParcel);
//                        if (paymentType.equalsIgnoreCase(SUBSCRIPTION)) {
//                            ApiHelper.doGetSubscriptionList(context, mainActivity.mProgressDialog, false).subscribeOn(Schedulers.io())
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .subscribe(new Subscriber<List<SubscriptionList>>() {
//                                        @Override
//                                        public void onCompleted() {
//                                            mainActivity.mProgressDialog.dismiss();
//                                            CallAndTextSubscriptionPaymentFragment callAndTextSubscriptionPaymentFragment = new CallAndTextSubscriptionPaymentFragment();
//                                            callAndTextSubscriptionPaymentFragment.setArguments(bundle);
//                                            mainActivity.changeFragment(callAndTextSubscriptionPaymentFragment, false);
//                                        }
//
//                                        @Override
//                                        public void onError(Throwable e) {
//
//                                        }
//
//                                        @Override
//                                        public void onNext(List<SubscriptionList> subscriptionList) {
//                                            Parcelable listSubscriptionParcel = Parcels.wrap(subscriptionList);
//                                            bundle.putParcelable(SUBSCRIPTION, listSubscriptionParcel);
//                                        }
//                                    });
//                        } else {
//                            ApiHelper.doGetIddList(context, mainActivity.mProgressDialog).subscribeOn(Schedulers.io())
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .subscribe(new Subscriber<List<IddList>>() {
//                                        @Override
//                                        public void onCompleted() {
//                                            mainActivity.mProgressDialog.dismiss();
//                                            TopUpIDDPaymentFragment topUpIDDPaymentFragment = new TopUpIDDPaymentFragment();
//                                            topUpIDDPaymentFragment.setArguments(bundle);
//                                            mainActivity.changeFragment(topUpIDDPaymentFragment, false);
//                                        }
//
//                                        @Override
//                                        public void onError(Throwable e) {
//
//                                        }
//
//                                        @Override
//                                        public void onNext(List<IddList> subscriptionList) {
//                                            Parcelable listIddParcel = Parcels.wrap(subscriptionList);
//                                            bundle.putParcelable(TOP_UP, listIddParcel);
//                                        }
//                                    });
//                        }
//
//                    }
//                });
//    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
}
