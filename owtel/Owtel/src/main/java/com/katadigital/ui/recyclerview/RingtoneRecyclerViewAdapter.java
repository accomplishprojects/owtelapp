package com.katadigital.ui.recyclerview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.kata.phone.R;
import com.katadigital.owtel.models.collectedobjects.AudioItem;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.modules.services.RingtonePlayingService;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.portsip.object.Settings;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;

import java.util.ArrayList;

/**
 * Created by Omar Matthew Reyes on 2/19/16.
 * <p>
 * RecyclerView adapter for SettingsFragment list
 */
public class RingtoneRecyclerViewAdapter extends RecyclerView.Adapter<RingtoneRecyclerViewAdapter.ViewHolder> {
    private ArrayList<AudioItem> audioLists = new ArrayList<>();
    private int selected_position = 99999;
    private Context context;
    private MainActivity mainActivity;
    private boolean userType;
    private static final int
            SETTINGS_PHONE_NOTIFICATION = 0,
            SETTINGS_PHONE_RINGTONE = 1,
            SETTINGS_VIBRATE_WHILE_RINGING = 2,
            SETTINGS_VIBRATE_ON_MUTE = 3;

    private static final String TAG = "SettingsRecycler";
    private MainActivityBridge mMainActivityBridge;
    private int devClick = 0;
    private int selectedPos = 0;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTextViewSettingsItem, mTextViewSettingsItemDetail;
        private ImageView mImageViewArrow;
        private ToggleButton toggleButton;
        protected Context context;

        public ViewHolder(View v, Context context) {
            super(v);
            mTextViewSettingsItem = (TextView) v.findViewById(R.id.tv_settings_item);
            mTextViewSettingsItemDetail = (TextView) v.findViewById(R.id.tv_settings_item_detail);
            mImageViewArrow = (ImageView) v.findViewById(R.id.img_settings_clickable_indicator);
            toggleButton = (ToggleButton) v.findViewById(R.id.toggle_sound_vibrate_settings);
            this.context = context;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
            if (audioLists.get(getAdapterPosition()).getAudioUri() != null)
                edit.putString(Settings.PREF_SIPRINGTONE, audioLists.get(getAdapterPosition()).getAudioUri().toString());
            else
                edit.putString(Settings.PREF_SIPRINGTONE, "");
            edit.commit();
            MainActivity mainActivity = (MainActivity) context;
            mainActivity.popFragment();
        }
    }

    public RingtoneRecyclerViewAdapter(ArrayList<AudioItem> audioLists, Context context) {
        this.audioLists = audioLists;
        this.context = context;
        this.userType = new Functions().getUserType();
        this.mainActivity = (MainActivity) context;
        this.mMainActivityBridge = (MainActivityBridge) context;
        SharedPreferenceManager.init(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // create a new view
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_item_ringtone, viewGroup, false);
        return new ViewHolder(v, context);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int pos) {
        holder.mTextViewSettingsItem.setText(audioLists.get(pos).getAudioName());
        holder.mImageViewArrow.setVisibility(View.VISIBLE);
        holder.toggleButton.setVisibility(View.GONE);


        final AudioItem item = audioLists.get(pos);

        if (selected_position == pos) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.material_brown_alpha_50));
            holder.itemView.setSelected(true);
            holder.mTextViewSettingsItem.setTextColor(Color.WHITE);

        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            holder.mTextViewSettingsItem.setTextColor(Color.GRAY);
            holder.itemView.setSelected(false);
        }

        holder.itemView.setOnClickListener(v -> {

            notifyItemChanged(selected_position);
            selected_position = pos;
            notifyItemChanged(selected_position);
            Intent stopIntent = new Intent(context, RingtonePlayingService.class);
            context.stopService(stopIntent);

            if (!v.isSelected()) {
                Intent startIntent = new Intent(context, RingtonePlayingService.class);
                if (audioLists.get(pos).getAudioUri() != null) {
                    startIntent.putExtra("ringtoneUri", audioLists.get(pos).getAudioUri().toString());
                    context.startService(startIntent);
                } else {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSelectedNotificationName("None");
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSelectedNotifiactionURI("");
                    Toast.makeText(context, "No tone selected.", Toast.LENGTH_SHORT).show();
                }

            } else {
//                stop and save ringtone
                if (audioLists.get(pos).getAudioUri() != null) {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSelectedRingtoneName(item.getAudioName());
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSelectedRingtoneURI(item.getAudioUri().toString());
                } else {
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSelectedRingtoneName("None");
                    SharedPreferenceManager.getSharedPreferenceManagerInstance()
                            .putSelectedRingtoneURI("");
                    Toast.makeText(context, "No tone selected.", Toast.LENGTH_SHORT).show();
                }
                mainActivity.popFragment();
            }
        });


//        Intent stopIntent = new Intent(context, RingtonePlayingService.class);
//        context.stopService(stopIntent);
//
//        if (selected_position == pos) {
//
//            SharedPreferenceManager.getSharedPreferenceManagerInstance()
//                    .putSelectedNotificationName(audioLists.get(pos).getAudioName());
//            SharedPreferenceManager.getSharedPreferenceManagerInstance()
//                    .putSelectedNotifiactionURI(audioLists.get(pos).getAudioUri().toString());
//            mainActivity.popFragment();
//        } else {
//            selected_position = pos;
//            Intent startIntent = new Intent(context, RingtonePlayingService.class);
//            startIntent.putExtra("ringtoneUri", audioLists.get(pos).getAudioUri().toString());
//            context.startService(startIntent);
//        }


    }

    @Override
    public int getItemCount() {
        return audioLists.size();
    }

}
