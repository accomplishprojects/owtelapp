package com.katadigital.ui.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.models.PromoCodeObj;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.modules.main.contacts.entities.PromoCodeDao;
import com.katadigital.owtel.modules.main.promo.PromoCodeFragment;
import com.katadigital.owtel.modules.billing.PromoUserPaymentFragment;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

import org.parceler.Parcels;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Omar Matthew Reyes on 4/21/16.
 * RecyclerView Adapter for Promo Code list
 */
public class PromoCodeRecyclerViewAdapter extends RecyclerView.Adapter<PromoCodeRecyclerViewAdapter.ViewHolder> {
    private List<PromoCodeObj> promoCodeList;
    private Context context;
    private MainActivity mainActivity;
    private boolean fromRegistration = false;
    private static final String TAG = "PromoCodeRVAdapter";

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView mTextViewPromoCodeItem, mTextViewPromoCode, mTextViewPromoCodeUserNumber;
        protected Button mButtonPromoCodeShare;
        protected Context context;

        public ViewHolder(View v, Context context) {
            super(v);
            mTextViewPromoCodeItem = (TextView) v.findViewById(R.id.tv_promo_code_item);
            mTextViewPromoCode = (TextView) v.findViewById(R.id.tv_promo_code_number);
            mTextViewPromoCodeUserNumber = (TextView) v.findViewById(R.id.tv_promo_code_user_number);
            mButtonPromoCodeShare = (Button) v.findViewById(R.id.btn_promo_code_share);
            this.context = context;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "RecyclerView clicked at " + getAdapterPosition());

            if (!promoCodeList.get(getAdapterPosition()).getPromoCode().equals("0") && !fromRegistration) {
                Bundle args = new Bundle();
                String promoCodeUserEmail = promoCodeList.get(getAdapterPosition()).getEmailAddress();
                args.putString(Constants.BUNDLE_PROMO_CODE_USER_EMAIL, promoCodeUserEmail);
                mainActivity.mProgressDialog.setMessage(context.getResources().getString(R.string.processing));
                mainActivity.mProgressDialog.show();
                ApiHelper.doGetSubscriptionList(context, mainActivity.mProgressDialog, false).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<List<SubscriptionList>>() {
                            @Override
                            public void onCompleted() {
                                ApiHelper.doGetIddList(context, mainActivity.mProgressDialog).subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Subscriber<List<IddList>>() {
                                            @Override
                                            public void onCompleted() {
                                                ApiHelper.doGetAddressList(mainActivity.mProgressDialog, mainActivity, GlobalValues.GET_COUNTRY, "", "").subscribeOn(Schedulers.io())
                                                        .observeOn(AndroidSchedulers.mainThread())
                                                        .subscribe(new Subscriber<List<AddressList>>() {
                                                            @Override
                                                            public void onCompleted() {
                                                                mainActivity.mProgressDialog.dismiss();
                                                                PromoUserPaymentFragment promoUserPaymentFragment = new PromoUserPaymentFragment();
                                                                promoUserPaymentFragment.setArguments(args);
                                                                mainActivity.changeFragment(promoUserPaymentFragment, PromoCodeFragment.TAG);
                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {

                                                            }

                                                            @Override
                                                            public void onNext(List<AddressList> addresses) {
                                                                Parcelable listCountryParcel = Parcels.wrap(addresses);
                                                                args.putParcelable(Constants.PARCEL_LIST_COUNTRY, listCountryParcel);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onNext(List<IddList> iddList) {
                                                Parcelable listIddParcel = Parcels.wrap(iddList);
                                                args.putParcelable(Constants.PARCEL_LIST_IDD, listIddParcel);
                                            }
                                        });
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(List<SubscriptionList> subscriptionList) {
                                Parcelable listSubscriptionParcel = Parcels.wrap(subscriptionList);
                                args.putParcelable(Constants.PARCEL_LIST_SUBSCRIPTION, listSubscriptionParcel);
                            }
                        });
            }
        }
    }

    public PromoCodeRecyclerViewAdapter(List<PromoCodeObj> promoCodeDaoList, boolean fromRegistration, Context context) {
        this.promoCodeList = promoCodeDaoList;
        this.context = context;
        this.fromRegistration = fromRegistration;

        if (!fromRegistration) this.mainActivity = (MainActivity) context;

        for (PromoCodeObj promoCode : promoCodeDaoList) {
            Log.i(TAG, "constructor doGetPromoCodesList Promo Code " + promoCode.getPromoCode());
            Log.i(TAG, "constructor doGetPromoCodesList Promo User " + promoCode.getOwtelNumber());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_item_promo_code, viewGroup, false);
        return new ViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String itemNum = context.getString(R.string.txt_settings_promo_code) + " " + (position + 1);
        holder.mTextViewPromoCodeItem.setText(itemNum);
        PromoCodeObj promoCodeItem = promoCodeList.get(position);
        Log.d(TAG, "onBindViewHolder: " + promoCodeItem.toString());
        holder.mTextViewPromoCode.setText(promoCodeItem.getPromoCode());
        if (promoCodeItem.getUsedBy() == null || promoCodeItem.getUsedBy().trim().equals("0")) {
            holder.mTextViewPromoCodeUserNumber.setVisibility(View.GONE);
            holder.mButtonPromoCodeShare.setVisibility(View.VISIBLE);
            holder.mButtonPromoCodeShare.setOnClickListener(v -> {
                String shareMessage = context.getString(R.string.share_promo_code_1) + " " +
                        promoCodeItem.getPromoCode() + "\n Please use the link below to download the app \n www.owtel.com/app_download.php" + "?device=android";
//                        context.getString(R.string.share_promo_code_2) + " " +
//                        Constants.URL_APP_ANDROID +
//                        context.getString(R.string.share_promo_code_3) + " " +
//                        Constants.URL_APP_IOS;

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                sendIntent.setType("text/plain");
//                context.startActivity(sendIntent);
                ((Activity) context).startActivityForResult(sendIntent, Constants.REQUEST_CODE_MESSAGE_APPS);
            });
        } else {
            holder.mButtonPromoCodeShare.setVisibility(View.GONE);
            holder.mTextViewPromoCodeUserNumber.setVisibility(View.VISIBLE);
            Log.e(TAG, "onBindViewHolder: " + promoCodeItem.getOwtelNumber());
            holder.mTextViewPromoCodeUserNumber.setText(promoCodeItem.getOwtelNumber());
//            holder.mTextViewPromoCodeUserNumber.setText(StringFormatter.formatUsNumber(promoCodeItem.getUserPromoCodeUserNumber()));
        }
    }

    @Override
    public int getItemCount() {
        return promoCodeList.size();
    }
}