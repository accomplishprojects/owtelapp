package com.katadigital.ui.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kata.phone.R;

/**
 * Created by omar on 4/8/16.
 */
public class MessagesRecyclerViewAdapter extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.ViewHolder> {
    private String[] mDataset;
    private Context context;
    private static final String TAG = "MessagesRecycler";

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView mTextViewSettingsItem, mTextViewSettingsItemDetail;
        protected Context context;

        public ViewHolder(View v, Context context) {
            super(v);
            mTextViewSettingsItem = (TextView) v.findViewById(R.id.tv_settings_item);
            mTextViewSettingsItemDetail = (TextView) v.findViewById(R.id.tv_settings_item_detail);
            this.context = context;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "RecyclerView clicked at " + getAdapterPosition());

        }
    }

    public MessagesRecyclerViewAdapter(String[] myDataset, Context context) {
        this.mDataset = myDataset;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // create a new view
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_item_settings, viewGroup, false);
        return new ViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {        }

    @Override
    public int getItemCount() {
        return 0;
    }
}