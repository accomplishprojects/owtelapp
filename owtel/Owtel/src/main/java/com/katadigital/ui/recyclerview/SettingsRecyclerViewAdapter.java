package com.katadigital.ui.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.daoDb.IddList;
import com.katadigital.owtel.daoDb.SubscriptionList;
import com.katadigital.owtel.modules.main.email.EmailFragment;
import com.katadigital.owtel.modules.soundsettings.SoundAndVibrationFragment;
import com.katadigital.owtel.utils.ApiHelper;
import com.katadigital.owtel.utils.Constants;
import com.katadigital.owtel.utils.SharedPreferenceHelper;
import com.katadigital.owtel.utils.StringFormatter;
import com.katadigital.owtel.modules.billing.CallAndTextSubscriptionPaymentFragment;
import com.katadigital.owtel.modules.main.promo.PromoCodeFragment;
import com.katadigital.owtel.modules.main.settings.SettingsFragment;
import com.katadigital.owtel.modules.billing.TopUpIDDPaymentFragment;
import com.katadigital.owtel.modules.main.MainActivityBridge;
import com.katadigital.owtel.tools.Functions;
import com.katadigital.owtel.modules.main.contacts.util.DialogBuilder;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;
import com.katadigital.portsip.object.Settings;
import com.katadigital.portsip.utilities.androidcomponent.service.SipSettings;
import com.katadigital.portsip.utilities.preferences.SharedPreferenceManager;

import org.parceler.Parcels;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Omar Matthew Reyes on 2/19/16.
 * <p>
 * RecyclerView adapter for SettingsFragment list
 */
public class SettingsRecyclerViewAdapter extends RecyclerView.Adapter<SettingsRecyclerViewAdapter.ViewHolder> {
    private String[] mDataset;
    private Context context;
    private MainActivity mainActivity;
    private final String SUBSCRIPTION = "subscription", TOP_UP = "top_up";
    private boolean userType;
    private static final int
            SETTINGS_EMAIL = 0,
            SETTINGS_SUBSCRIPTION = 1,
            SETTINGS_IDD_CREDIT = 2,
            SETTINGS_NUMBER = 3,
            SETTINGS_PROMO_CODE = 4,
            SETTINGS_SOUND_AND_VIBRATION = 5,
            SETTINGS_ABOUT = 6,
            SETTINGS_LOGOUT = 7;

    private static final String TAG = "SettingsRecycler";
    private MainActivityBridge mMainActivityBridge;
    private int devClick = 0;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView mTextViewSettingsItem, mTextViewSettingsItemDetail;
        protected ImageView mImageViewArrow;
        protected Context context;

        public ViewHolder(View v, Context context) {
            super(v);
            mTextViewSettingsItem = (TextView) v.findViewById(R.id.tv_settings_item);
            mTextViewSettingsItemDetail = (TextView) v.findViewById(R.id.tv_settings_item_detail);
            mImageViewArrow = (ImageView) v.findViewById(R.id.img_settings_clickable_indicator);
            this.context = context;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "RecyclerView clicked at " + getAdapterPosition());
            int devClickNeed = 9;
            switch (getAdapterPosition()) {
                case SETTINGS_SUBSCRIPTION:
                    // Subscription
                    //getSubscriptionPrices();
                    if (userType) getAddressCountryList(SUBSCRIPTION);
                    break;

                case SETTINGS_EMAIL:
                    mainActivity.changeFragment(new EmailFragment(), SettingsFragment.TAG);
                    break;
                case SETTINGS_IDD_CREDIT:
                    // IDD Credit
                    getAddressCountryList(TOP_UP);
                    break;
                case SETTINGS_NUMBER:
                    if (GlobalValues.DEBUG) {
                        devClick++;
                        if (devClick == devClickNeed) {
                            context.startActivity(new Intent(context, Settings.class));
                            devClick = 0;
                        }
                    }
                    break;
                case SETTINGS_PROMO_CODE:
                    if (userType)
                        mainActivity.changeFragment(new PromoCodeFragment(), SettingsFragment.TAG);
                    break;
                case SETTINGS_SOUND_AND_VIBRATION:
                    mainActivity.changeFragment(new SoundAndVibrationFragment(), SettingsFragment.TAG);
                    break;
                case SETTINGS_LOGOUT:
                    DialogBuilder.showAlertDialog(context,
                            context.getString(R.string.logout), context.getString(R.string.logout_dialog_notice),
                            context.getString(R.string.string_ok), true, context1 -> new ApiHelper(context).doLogout());
                    break;
            }
        }
    }

    public SettingsRecyclerViewAdapter(String[] myDataset, Context context) {
        SharedPreferenceManager.init(context);
        this.mDataset = myDataset;
        this.context = context;
        this.userType = new Functions().getUserType();
        this.mainActivity = (MainActivity) context;

        this.mMainActivityBridge = (MainActivityBridge) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // create a new view
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_item_settings, viewGroup, false);
        return new ViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SharedPreferences preferencesUser = OwtelAppController.getInstance().getDefaultSharedPreferences();
        holder.mTextViewSettingsItem.setText(mDataset[position]);
        switch (position) {
            case SETTINGS_EMAIL:
                holder.mTextViewSettingsItemDetail.setText(preferencesUser.getString(Constants.PREFS_USER_EMAIL, "empty"));
                holder.mImageViewArrow.setVisibility(View.VISIBLE);
                break;
            case SETTINGS_SUBSCRIPTION:
                String expiryDate = context.getString(R.string.txt_settings_subscription_expire) + " " +
                        preferencesUser.getString(Constants.PREFS_USER_SUBSCRIPTION_EXP, "empty").split(" ")[0].replace("-", "/");
                holder.mTextViewSettingsItemDetail.setText(expiryDate);
                if (userType) holder.mImageViewArrow.setVisibility(View.VISIBLE);
                else holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                break;
            case SETTINGS_IDD_CREDIT:
                holder.mImageViewArrow.setVisibility(View.VISIBLE);
//                holder.mTextViewSettingsItemDetail.setText(StringFormatter.formatCurrency(Constants.CURRENCY, SharedPreferenceHelper.getDouble(preferencesUser, Constants.PREFS_USER_IDD_CREDITS, 0.0)));
                holder.mTextViewSettingsItemDetail.setText(Constants.CURRENCY + SharedPreferenceManager.getSharedPreferenceManagerInstance().getIDDCredits());

                break;
            case SETTINGS_NUMBER:
                holder.mTextViewSettingsItemDetail.setText(StringFormatter.formatUsNumber(new SipSettings().getSipUsername()));
                holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                break;
            case SETTINGS_PROMO_CODE:
                if (userType) holder.mImageViewArrow.setVisibility(View.VISIBLE);
                else {
                    holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                    holder.mTextViewSettingsItemDetail.setText(preferencesUser.getString(Constants.PREFS_PROMO_CODE, "empty"));
                }
                break;
            case SETTINGS_SOUND_AND_VIBRATION:
                    holder.mImageViewArrow.setVisibility(View.VISIBLE);
                break;
            case SETTINGS_ABOUT:
                holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                try {
                    String version = context.getString(R.string.txt_settings_version) + " " +
                            context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                    holder.mTextViewSettingsItemDetail.setText(version);
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e(TAG, "About: Version " + e);
                }
                break;
            case SETTINGS_LOGOUT:
                holder.mImageViewArrow.setVisibility(View.INVISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    /**
     * Interface for Stop running services
     */
    public interface ServiceKiller {
        void stopServices(Context context);
    }

    /**
     * Get Address Country List
     *
     * @param paymentType String
     */
    public void getAddressCountryList(String paymentType) {
        Bundle bundle = new Bundle();
        mainActivity.mProgressDialog.setMessage(mainActivity.getResources().getString(R.string.processing));
        mainActivity.mProgressDialog.show();
        ApiHelper.doGetAddressList(mainActivity.mProgressDialog, mainActivity, GlobalValues.GET_COUNTRY, "", "").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<AddressList>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<AddressList> addresses) {
                        Parcelable listAddressParcel = Parcels.wrap(addresses);
                        bundle.putParcelable("countries", listAddressParcel);
                        if (paymentType.equalsIgnoreCase(SUBSCRIPTION)) {
                            ApiHelper.doGetSubscriptionList(context, mainActivity.mProgressDialog, false).subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Subscriber<List<SubscriptionList>>() {
                                        @Override
                                        public void onCompleted() {
                                            mainActivity.mProgressDialog.dismiss();
                                            CallAndTextSubscriptionPaymentFragment callAndTextSubscriptionPaymentFragment = new CallAndTextSubscriptionPaymentFragment();
                                            callAndTextSubscriptionPaymentFragment.setArguments(bundle);
                                            mainActivity.changeFragment(callAndTextSubscriptionPaymentFragment, false);
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onNext(List<SubscriptionList> subscriptionList) {
                                            Parcelable listSubscriptionParcel = Parcels.wrap(subscriptionList);
                                            bundle.putParcelable(SUBSCRIPTION, listSubscriptionParcel);
                                        }
                                    });
                        } else {
                            ApiHelper.doGetIddList(context, mainActivity.mProgressDialog).subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Subscriber<List<IddList>>() {
                                        @Override
                                        public void onCompleted() {
                                            mainActivity.mProgressDialog.dismiss();
                                            TopUpIDDPaymentFragment topUpIDDPaymentFragment = new TopUpIDDPaymentFragment();
                                            topUpIDDPaymentFragment.setArguments(bundle);
                                            mainActivity.changeFragment(topUpIDDPaymentFragment, false);
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onNext(List<IddList> subscriptionList) {
                                            Parcelable listIddParcel = Parcels.wrap(subscriptionList);
                                            bundle.putParcelable(TOP_UP, listIddParcel);
                                        }
                                    });
                        }

                    }
                });
    }
}
