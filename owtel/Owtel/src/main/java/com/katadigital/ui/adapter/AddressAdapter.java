package com.katadigital.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kata.phone.R;
import com.katadigital.owtel.OwtelAppController;
import com.katadigital.owtel.daoDb.AddressList;
import com.katadigital.owtel.modules.main.contacts.util.GlobalValues;

import java.util.List;

/**
 * Created by Omar Matthew Reyes on 5/17/16.
 * Spinner Adapter for Address list
 */


public class AddressAdapter extends BaseAdapter {
    private final String TAG = "AddressAdapter";
    private List<AddressList> addressLists;
    private int type;

    public AddressAdapter(List<AddressList> areaCodeList, int type) {
        this.addressLists = areaCodeList;
        this.type = type;
    }

    @Override
    public int getCount() {
        return addressLists.size();
    }

    @Override
    public Object getItem(int position) {
        return addressLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) OwtelAppController.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.simple_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvAddress = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        switch (type) {
            case GlobalValues.GET_COUNTRY:
                Log.i(TAG, "AddressAdapter country " + addressLists.get(position).getCountry());
                viewHolder.tvAddress.setText(addressLists.get(position).getCountry());
                break;
            case GlobalValues.GET_STATE:
                Log.i(TAG, "AddressAdapter state " + addressLists.get(position).getState());
                viewHolder.tvAddress.setText(addressLists.get(position).getState());
                break;
            case GlobalValues.GET_CITY:
                Log.i(TAG, "AddressAdapter city " + addressLists.get(position).getCity());
                viewHolder.tvAddress.setText(addressLists.get(position).getCity());
                break;
        }
        return convertView;
    }

    private class ViewHolder {
        public TextView tvAddress;
    }
}