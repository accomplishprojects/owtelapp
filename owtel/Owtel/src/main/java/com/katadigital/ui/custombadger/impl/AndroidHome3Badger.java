package com.katadigital.ui.custombadger.impl;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.katadigital.ui.custombadger.ShortcutBadgeException;
import com.katadigital.ui.custombadger.ShortcutBadger;
import com.katadigital.ui.custombadger.util.ImageUtil;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA. User: leolin Date: 2013/11/14 Time: 下午7:15 To
 * change this template use File | Settings | File Templates.
 */
public class AndroidHome3Badger extends ShortcutBadger {
    private static final String CONTENT_URI = "content://com.android.launcher3.settings/favorites?notify=true";
    private static final String TAG = "AndroidHome3Badger";

    public AndroidHome3Badger(Context context) {
        super(context);
    }

    @Override
    protected void executeBadge(int badgeCount) throws ShortcutBadgeException {
        byte[] bytes = ImageUtil.drawBadgeOnAppIcon(mContext, badgeCount);
        String appName = mContext.getResources().getText(mContext.getResources().getIdentifier("app_name",
                "string", getContextPackageName())).toString();

        Uri mUri = Uri.parse(CONTENT_URI);
        ContentResolver contentResolver = mContext.getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put("iconType", 1);
        contentValues.put("itemType", 1);
        contentValues.put("icon", bytes);
        contentResolver.update(mUri, contentValues, "title=?", new String[]{appName});
    }

    @Override
    protected List<String> getSupportLaunchers() {
        return Arrays.asList("com.android.launcher3");
    }
}
