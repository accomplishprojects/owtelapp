package com.katadigital.ui.recyclerview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kata.phone.R;
import com.katadigital.owtel.modules.main.contacts.CustomAutoCompleteTextView;
import com.katadigital.portsip.utilities.androidcomponent.interfaces.PortSipContract;

/**
 * Created by Omar Matthew Reyes on 3/28/16.
 * <p>
 * RecyclerView Adapter for Dialer
 * TODO FIX DIAL TONE
 */
public class DialerRecyclerViewAdapter extends RecyclerView.Adapter<DialerRecyclerViewAdapter.ViewHolder> {
    private static Rect rect;

    private final static String TAG = "DialerRVAdapter";
    private final String[] dialerMainLegend = {
            "1", "2", "3",
            "4", "5", "6",
            "7", "8", "9",
            "*", "0", "#"};
    private final String[] dialerSubLegend = {
            "", "ABC", "DEF",
            "GHI", "JKL", "MNO",
            "PQRS", "TUV", "WYXZ",
            "", "+", ""};

    private static final int STREAM = AudioManager.STREAM_DTMF;

    public static ToneGenerator dtmfGenerator = new ToneGenerator(STREAM, ToneGenerator.MAX_VOLUME);

    private static final Integer[] dialerTone = {ToneGenerator.TONE_DTMF_1, ToneGenerator.TONE_DTMF_2, ToneGenerator.TONE_DTMF_3,
            ToneGenerator.TONE_DTMF_4, ToneGenerator.TONE_DTMF_5, ToneGenerator.TONE_DTMF_6, ToneGenerator.TONE_DTMF_7,
            ToneGenerator.TONE_DTMF_8, ToneGenerator.TONE_DTMF_9, ToneGenerator.TONE_DTMF_0, ToneGenerator.TONE_DTMF_P,
            ToneGenerator.TONE_DTMF_S};

    private Context context;
    private CustomAutoCompleteTextView autoCompleteTextView;

    public DialerRecyclerViewAdapter(Context context, CustomAutoCompleteTextView autoCompleteTextView) {
        this.context = context;
        this.autoCompleteTextView = autoCompleteTextView;

    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {
        TextView mTextViewDialerMain, mTextViewDialerSub, txtSpecialKeys;
        RelativeLayout mLayout;
        protected Context context;
        CustomAutoCompleteTextView autoCompleteTextView;
        private float x1, x2;
        static final int MIN_DISTANCE = 150;

        /**
         * PortSip Variables
         */
        public PortSipContract.PortSipContractCallback portSipContractCallback;

        public ViewHolder(View v, Context context, CustomAutoCompleteTextView autoCompleteTextView) {
            super(v);
            portSipContractCallback = (PortSipContract.PortSipContractCallback) context;

            /**Initializing Views*/
            this.autoCompleteTextView = autoCompleteTextView;
            mTextViewDialerMain = (TextView) v.findViewById(R.id.tv_btn_dialer_main);
            mTextViewDialerSub = (TextView) v.findViewById(R.id.tv_btn_dialer_sub);
            txtSpecialKeys = (TextView) v.findViewById(R.id.txtSpecialKeys);

            mLayout = (RelativeLayout) v.findViewById(R.id.rv_cell_dialer);
//            mLayout.post(() -> {
//                Log.i(TAG, "RecyclerView height(px) " + mLayout.getHeight());
//                float dp = convertPixelsToDp(mLayout.getHeight(), context);
//                Log.i(TAG, "RecyclerView height(dp) " + dp);
//            });

            this.context = context;
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
            v.setOnTouchListener(this);
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    x1 = event.getX();
                    Log.e(TAG, "onTouch: " + event.getX() + " / " + event.getY());
                    dtmfGenerator.startTone(dialerTone[getAdapterPosition()]);
                    break;
                case MotionEvent.ACTION_UP:
                    x2 = event.getX();
                    float deltaX = x2 - x1;
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
//                        Toast.makeText(this, "left2right swipe", Toast.LENGTH_SHORT).show ();
                        dtmfGenerator.stopTone();
                    } else {
                        // consider as something else - a screen tap for example
                        dtmfGenerator.stopTone();

                    }
                    break;
                case MotionEvent.ACTION_CANCEL:
                    dtmfGenerator.stopTone();
                    break;
            }


            return false;
        }

        @Override
        public void onClick(View v) {
            String strSelectedNumber = mTextViewDialerMain.getText().toString();
            char number = strSelectedNumber.charAt(0);
            autoCompleteTextView.setText(autoCompleteTextView.getText().append(number));
            Log.e(TAG, "onClick: " +
                    " Editext: " + autoCompleteTextView.getText().toString() +
                    " Number: " + number);

            portSipContractCallback.onPortSipSendDMTFCallback(number);

        }

        @Override
        public boolean onLongClick(View v) {
            switch (getAdapterPosition()) {
                case 9:
                    break;
                case 10:
                    if (autoCompleteTextView.getText().toString().length() > 1) {
                        String input = autoCompleteTextView.getText().toString()
                                .substring(0, autoCompleteTextView.getText().toString().length() - 1) + "+";
                        autoCompleteTextView.setText(input);
                    } else {
                        autoCompleteTextView.setText("+");
                    }
                    break;
                case 11:
                    break;
            }
            return true;
        }

//        @Override
//        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
//            return false;
//        }
//
//        @Override
//        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//
//        }
//
//        @Override
//        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item_dialer, parent, false);
        return new ViewHolder(v, context, autoCompleteTextView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String strMainKey = dialerMainLegend[position];
        String strSubKey = dialerSubLegend[position];

        if (strMainKey.equals("*") || strMainKey.equals("#")) {
            holder.txtSpecialKeys.setVisibility(View.VISIBLE);
            holder.mTextViewDialerMain.setVisibility(View.GONE);
            holder.mTextViewDialerSub.setVisibility(View.GONE);
            if (strMainKey.equals("*")) {
                holder.txtSpecialKeys.setTextSize(36);
            } else {
                holder.txtSpecialKeys.setPadding(0, 10, 0, 0);
            }
            holder.txtSpecialKeys.setText(strMainKey);
        }
        holder.mTextViewDialerMain.setText(strMainKey);
        holder.mTextViewDialerSub.setText(strSubKey);
    }

    @Override
    public int getItemCount() {
        return dialerMainLegend.length;
    }

//
//    private Observable getLayoutHeight(View view){
//        return Observable.create(subscriber -> {
//            view.getHeight();
//            Log.i(TAG, "RecyclerView height " + view.getHeight());
//            subscriber.onNext(view);
//            subscriber.onCompleted();
//        });
//    }


    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

}
