package com.katadigital.owtel.dagger2;

import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkingModule_ProvideHttpLoggingInterceptorFactory implements Factory<HttpLoggingInterceptor> {
  private final NetworkingModule module;

  public NetworkingModule_ProvideHttpLoggingInterceptorFactory(NetworkingModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public HttpLoggingInterceptor get() {  
    HttpLoggingInterceptor provided = module.provideHttpLoggingInterceptor();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<HttpLoggingInterceptor> create(NetworkingModule module) {  
    return new NetworkingModule_ProvideHttpLoggingInterceptorFactory(module);
  }
}

