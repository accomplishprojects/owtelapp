package com.katadigital.owtel.dagger2;

import com.katadigital.owtel.daoDb.DaoMaster;
import com.katadigital.owtel.daoDb.DaoSession;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class StorageModule_ProvideDaoSessionFactory implements Factory<DaoSession> {
  private final StorageModule module;
  private final Provider<DaoMaster> daoMasterProvider;

  public StorageModule_ProvideDaoSessionFactory(StorageModule module, Provider<DaoMaster> daoMasterProvider) {  
    assert module != null;
    this.module = module;
    assert daoMasterProvider != null;
    this.daoMasterProvider = daoMasterProvider;
  }

  @Override
  public DaoSession get() {  
    DaoSession provided = module.provideDaoSession(daoMasterProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<DaoSession> create(StorageModule module, Provider<DaoMaster> daoMasterProvider) {  
    return new StorageModule_ProvideDaoSessionFactory(module, daoMasterProvider);
  }
}

