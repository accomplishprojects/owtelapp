// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RegisterEndFragment$$ViewBinder<T extends com.katadigital.owtel.modules.registration.RegisterEndFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690126, "field 'tvUserEmail'");
    target.tvUserEmail = finder.castView(view, 2131690126, "field 'tvUserEmail'");
    view = finder.findRequiredView(source, 2131690127, "field 'tvUserNumber'");
    target.tvUserNumber = finder.castView(view, 2131690127, "field 'tvUserNumber'");
    view = finder.findRequiredView(source, 2131690128, "field 'btnLineShare'");
    target.btnLineShare = finder.castView(view, 2131690128, "field 'btnLineShare'");
  }

  @Override public void unbind(T target) {
    target.tvUserEmail = null;
    target.tvUserNumber = null;
    target.btnLineShare = null;
  }
}
