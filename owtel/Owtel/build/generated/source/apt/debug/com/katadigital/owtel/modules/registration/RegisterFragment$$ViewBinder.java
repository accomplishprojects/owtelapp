// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RegisterFragment$$ViewBinder<T extends com.katadigital.owtel.modules.registration.RegisterFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690121, "field 'btnRegister' and method 'onContinueClicked'");
    target.btnRegister = finder.castView(view, 2131690121, "field 'btnRegister'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onContinueClicked();
        }
      });
    view = finder.findRequiredView(source, 2131690119, "field 'tilPassword'");
    target.tilPassword = finder.castView(view, 2131690119, "field 'tilPassword'");
    view = finder.findRequiredView(source, 2131690118, "field 'tilEmail'");
    target.tilEmail = finder.castView(view, 2131690118, "field 'tilEmail'");
    view = finder.findRequiredView(source, 2131690063, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131690063, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131690058, "field 'etPassword'");
    target.etPassword = finder.castView(view, 2131690058, "field 'etPassword'");
    view = finder.findRequiredView(source, 2131690060, "field 'checkBoxPasswordShow'");
    target.checkBoxPasswordShow = finder.castView(view, 2131690060, "field 'checkBoxPasswordShow'");
    view = finder.findRequiredView(source, 2131690120, "field 'etPromoCode'");
    target.etPromoCode = finder.castView(view, 2131690120, "field 'etPromoCode'");
  }

  @Override public void unbind(T target) {
    target.btnRegister = null;
    target.tilPassword = null;
    target.tilEmail = null;
    target.etEmail = null;
    target.etPassword = null;
    target.checkBoxPasswordShow = null;
    target.etPromoCode = null;
  }
}
