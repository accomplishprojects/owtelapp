package com.katadigital.owtel.modules.main.messaging.service;

import android.app.Service;
import com.katadigital.owtel.utils.RxBus;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class WebSocketService_MembersInjector implements MembersInjector<WebSocketService> {
  private final MembersInjector<Service> supertypeInjector;
  private final Provider<RxBus> rxBusProvider;

  public WebSocketService_MembersInjector(MembersInjector<Service> supertypeInjector, Provider<RxBus> rxBusProvider) {  
    assert supertypeInjector != null;
    this.supertypeInjector = supertypeInjector;
    assert rxBusProvider != null;
    this.rxBusProvider = rxBusProvider;
  }

  @Override
  public void injectMembers(WebSocketService instance) {  
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    supertypeInjector.injectMembers(instance);
    instance.rxBus = rxBusProvider.get();
  }

  public static MembersInjector<WebSocketService> create(MembersInjector<Service> supertypeInjector, Provider<RxBus> rxBusProvider) {  
      return new WebSocketService_MembersInjector(supertypeInjector, rxBusProvider);
  }
}

