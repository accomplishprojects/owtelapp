package com.katadigital.owtel.dagger2;

import android.database.sqlite.SQLiteDatabase;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.DaoMaster;
import com.katadigital.owtel.daoDb.DaoMaster.DevOpenHelper;
import com.katadigital.owtel.daoDb.DaoSession;
import com.katadigital.owtel.modules.login.LoginActivity;
import com.katadigital.owtel.modules.login.LoginActivity_MembersInjector;
import com.katadigital.owtel.modules.main.MainActivity;
import com.katadigital.owtel.modules.main.MainActivity_MembersInjector;
import com.katadigital.owtel.modules.main.messaging.ChatActivity;
import com.katadigital.owtel.modules.main.messaging.ChatActivity_MembersInjector;
import com.katadigital.owtel.modules.main.messaging.service.WebSocketService;
import com.katadigital.owtel.modules.main.messaging.service.WebSocketService_MembersInjector;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity;
import com.katadigital.owtel.modules.registration.FragmentHolderActivity_MembersInjector;
import com.katadigital.owtel.utils.RxBus;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import dagger.MembersInjector;
import dagger.internal.MembersInjectors;
import dagger.internal.ScopedProvider;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerAppComponent implements AppComponent {
  private Provider<HttpLoggingInterceptor> provideHttpLoggingInterceptorProvider;
  private Provider<OkHttpClient> provideOkHttpClientProvider;
  private Provider<ApiService> provideApiServiceProvider;
  private Provider<DevOpenHelper> provideDevOpenHelperProvider;
  private Provider<SQLiteDatabase> provideSQLiteDatabaseProvider;
  private Provider<DaoMaster> provideDaoMasterProvider;
  private Provider<DaoSession> provideDaoSessionProvider;
  private MembersInjector<LoginActivity> loginActivityMembersInjector;
  private MembersInjector<FragmentHolderActivity> fragmentHolderActivityMembersInjector;
  private Provider<RxBus> provideRxBusProvider;
  private MembersInjector<WebSocketService> webSocketServiceMembersInjector;
  private MembersInjector<ChatActivity> chatActivityMembersInjector;
  private MembersInjector<MainActivity> mainActivityMembersInjector;

  private DaggerAppComponent(Builder builder) {  
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {  
    return new Builder();
  }

  private void initialize(final Builder builder) {  
    this.provideHttpLoggingInterceptorProvider = ScopedProvider.create(NetworkingModule_ProvideHttpLoggingInterceptorFactory.create(builder.networkingModule));
    this.provideOkHttpClientProvider = ScopedProvider.create(NetworkingModule_ProvideOkHttpClientFactory.create(builder.networkingModule, provideHttpLoggingInterceptorProvider));
    this.provideApiServiceProvider = ScopedProvider.create(NetworkingModule_ProvideApiServiceFactory.create(builder.networkingModule, provideOkHttpClientProvider));
    this.provideDevOpenHelperProvider = ScopedProvider.create(StorageModule_ProvideDevOpenHelperFactory.create(builder.storageModule));
    this.provideSQLiteDatabaseProvider = ScopedProvider.create(StorageModule_ProvideSQLiteDatabaseFactory.create(builder.storageModule, provideDevOpenHelperProvider));
    this.provideDaoMasterProvider = ScopedProvider.create(StorageModule_ProvideDaoMasterFactory.create(builder.storageModule, provideSQLiteDatabaseProvider));
    this.provideDaoSessionProvider = ScopedProvider.create(StorageModule_ProvideDaoSessionFactory.create(builder.storageModule, provideDaoMasterProvider));
    this.loginActivityMembersInjector = LoginActivity_MembersInjector.create((MembersInjector) MembersInjectors.noOp(), provideApiServiceProvider, provideDaoSessionProvider);
    this.fragmentHolderActivityMembersInjector = FragmentHolderActivity_MembersInjector.create((MembersInjector) MembersInjectors.noOp(), provideApiServiceProvider, provideDaoSessionProvider);
    this.provideRxBusProvider = ScopedProvider.create(ApplicationModule_ProvideRxBusFactory.create(builder.applicationModule));
    this.webSocketServiceMembersInjector = WebSocketService_MembersInjector.create((MembersInjector) MembersInjectors.noOp(), provideRxBusProvider);
    this.chatActivityMembersInjector = ChatActivity_MembersInjector.create((MembersInjector) MembersInjectors.noOp(), provideApiServiceProvider, provideDaoSessionProvider, provideRxBusProvider);
    this.mainActivityMembersInjector = MainActivity_MembersInjector.create((MembersInjector) MembersInjectors.noOp(), provideRxBusProvider, provideApiServiceProvider);
  }

  @Override
  public void inject(LoginActivity loginActivity) {  
    loginActivityMembersInjector.injectMembers(loginActivity);
  }

  @Override
  public void inject(FragmentHolderActivity fragmentHolderActivity) {  
    fragmentHolderActivityMembersInjector.injectMembers(fragmentHolderActivity);
  }

  @Override
  public void inject(WebSocketService webSocketService) {  
    webSocketServiceMembersInjector.injectMembers(webSocketService);
  }

  @Override
  public void inject(ChatActivity chatActivity) {  
    chatActivityMembersInjector.injectMembers(chatActivity);
  }

  @Override
  public void inject(MainActivity mainActivity) {  
    mainActivityMembersInjector.injectMembers(mainActivity);
  }

  public static final class Builder {
    private NetworkingModule networkingModule;
    private StorageModule storageModule;
    private ApplicationModule applicationModule;
  
    private Builder() {  
    }
  
    public AppComponent build() {  
      if (networkingModule == null) {
        throw new IllegalStateException("networkingModule must be set");
      }
      if (storageModule == null) {
        throw new IllegalStateException("storageModule must be set");
      }
      if (applicationModule == null) {
        throw new IllegalStateException("applicationModule must be set");
      }
      return new DaggerAppComponent(this);
    }
  
    public Builder networkingModule(NetworkingModule networkingModule) {  
      if (networkingModule == null) {
        throw new NullPointerException("networkingModule");
      }
      this.networkingModule = networkingModule;
      return this;
    }
  
    public Builder storageModule(StorageModule storageModule) {  
      if (storageModule == null) {
        throw new NullPointerException("storageModule");
      }
      this.storageModule = storageModule;
      return this;
    }
  
    public Builder applicationModule(ApplicationModule applicationModule) {  
      if (applicationModule == null) {
        throw new NullPointerException("applicationModule");
      }
      this.applicationModule = applicationModule;
      return this;
    }
  }
}

