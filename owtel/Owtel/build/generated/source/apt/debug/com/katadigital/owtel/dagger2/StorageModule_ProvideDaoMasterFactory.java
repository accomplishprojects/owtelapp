package com.katadigital.owtel.dagger2;

import android.database.sqlite.SQLiteDatabase;
import com.katadigital.owtel.daoDb.DaoMaster;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class StorageModule_ProvideDaoMasterFactory implements Factory<DaoMaster> {
  private final StorageModule module;
  private final Provider<SQLiteDatabase> dbProvider;

  public StorageModule_ProvideDaoMasterFactory(StorageModule module, Provider<SQLiteDatabase> dbProvider) {  
    assert module != null;
    this.module = module;
    assert dbProvider != null;
    this.dbProvider = dbProvider;
  }

  @Override
  public DaoMaster get() {  
    DaoMaster provided = module.provideDaoMaster(dbProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<DaoMaster> create(StorageModule module, Provider<SQLiteDatabase> dbProvider) {  
    return new StorageModule_ProvideDaoMasterFactory(module, dbProvider);
  }
}

