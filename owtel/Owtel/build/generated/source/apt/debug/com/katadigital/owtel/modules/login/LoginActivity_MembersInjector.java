package com.katadigital.owtel.modules.login;

import android.support.v7.app.AppCompatActivity;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.DaoSession;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class LoginActivity_MembersInjector implements MembersInjector<LoginActivity> {
  private final MembersInjector<AppCompatActivity> supertypeInjector;
  private final Provider<ApiService> apiServiceProvider;
  private final Provider<DaoSession> daoSessionProvider;

  public LoginActivity_MembersInjector(MembersInjector<AppCompatActivity> supertypeInjector, Provider<ApiService> apiServiceProvider, Provider<DaoSession> daoSessionProvider) {  
    assert supertypeInjector != null;
    this.supertypeInjector = supertypeInjector;
    assert apiServiceProvider != null;
    this.apiServiceProvider = apiServiceProvider;
    assert daoSessionProvider != null;
    this.daoSessionProvider = daoSessionProvider;
  }

  @Override
  public void injectMembers(LoginActivity instance) {  
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    supertypeInjector.injectMembers(instance);
    instance.apiService = apiServiceProvider.get();
    instance.daoSession = daoSessionProvider.get();
  }

  public static MembersInjector<LoginActivity> create(MembersInjector<AppCompatActivity> supertypeInjector, Provider<ApiService> apiServiceProvider, Provider<DaoSession> daoSessionProvider) {  
      return new LoginActivity_MembersInjector(supertypeInjector, apiServiceProvider, daoSessionProvider);
  }
}

