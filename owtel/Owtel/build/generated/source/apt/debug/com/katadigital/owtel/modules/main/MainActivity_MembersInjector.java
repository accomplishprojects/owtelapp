package com.katadigital.owtel.modules.main;

import android.support.v7.app.AppCompatActivity;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.utils.RxBus;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class MainActivity_MembersInjector implements MembersInjector<MainActivity> {
  private final MembersInjector<AppCompatActivity> supertypeInjector;
  private final Provider<RxBus> rxBusProvider;
  private final Provider<ApiService> apiServiceProvider;

  public MainActivity_MembersInjector(MembersInjector<AppCompatActivity> supertypeInjector, Provider<RxBus> rxBusProvider, Provider<ApiService> apiServiceProvider) {  
    assert supertypeInjector != null;
    this.supertypeInjector = supertypeInjector;
    assert rxBusProvider != null;
    this.rxBusProvider = rxBusProvider;
    assert apiServiceProvider != null;
    this.apiServiceProvider = apiServiceProvider;
  }

  @Override
  public void injectMembers(MainActivity instance) {  
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    supertypeInjector.injectMembers(instance);
    instance.rxBus = rxBusProvider.get();
    instance.apiService = apiServiceProvider.get();
  }

  public static MembersInjector<MainActivity> create(MembersInjector<AppCompatActivity> supertypeInjector, Provider<RxBus> rxBusProvider, Provider<ApiService> apiServiceProvider) {  
      return new MainActivity_MembersInjector(supertypeInjector, rxBusProvider, apiServiceProvider);
  }
}

