
package com.katadigital.owtel.daoDb;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2017-04-11T15:50+0800")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class IddList$$Parcelable
    implements Parcelable, ParcelWrapper<com.katadigital.owtel.daoDb.IddList>
{

    private com.katadigital.owtel.daoDb.IddList iddList$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static IddList$$Parcelable.Creator$$1 CREATOR = new IddList$$Parcelable.Creator$$1();

    public IddList$$Parcelable(com.katadigital.owtel.daoDb.IddList iddList$$2) {
        iddList$$0 = iddList$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(iddList$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(com.katadigital.owtel.daoDb.IddList iddList$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(iddList$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(iddList$$1));
            parcel$$1 .writeString(iddList$$1 .IddReloadName);
            parcel$$1 .writeString(iddList$$1 .IddId);
            parcel$$1 .writeDouble(iddList$$1 .IddReloadValue);
            parcel$$1 .writeString(iddList$$1 .IddReloadPrice);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.katadigital.owtel.daoDb.IddList getParcel() {
        return iddList$$0;
    }

    public static com.katadigital.owtel.daoDb.IddList read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            com.katadigital.owtel.daoDb.IddList iddList$$3;
            int reservation$$0 = identityMap$$1 .reserve();
            iddList$$3 = new com.katadigital.owtel.daoDb.IddList();
            identityMap$$1 .put(reservation$$0, iddList$$3);
            iddList$$3 .IddReloadName = parcel$$3 .readString();
            iddList$$3 .IddId = parcel$$3 .readString();
            iddList$$3 .IddReloadValue = parcel$$3 .readDouble();
            iddList$$3 .IddReloadPrice = parcel$$3 .readString();
            return iddList$$3;
        }
    }

    public final static class Creator$$1
        implements Creator<IddList$$Parcelable>
    {


        @Override
        public IddList$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new IddList$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public IddList$$Parcelable[] newArray(int size) {
            return new IddList$$Parcelable[size] ;
        }

    }

}
