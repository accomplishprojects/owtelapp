package com.katadigital.owtel.modules.main.messaging;

import android.support.v7.app.AppCompatActivity;
import com.katadigital.owtel.api.ApiService;
import com.katadigital.owtel.daoDb.DaoSession;
import com.katadigital.owtel.utils.RxBus;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class ChatActivity_MembersInjector implements MembersInjector<ChatActivity> {
  private final MembersInjector<AppCompatActivity> supertypeInjector;
  private final Provider<ApiService> apiServiceProvider;
  private final Provider<DaoSession> daoSessionProvider;
  private final Provider<RxBus> rxBusProvider;

  public ChatActivity_MembersInjector(MembersInjector<AppCompatActivity> supertypeInjector, Provider<ApiService> apiServiceProvider, Provider<DaoSession> daoSessionProvider, Provider<RxBus> rxBusProvider) {  
    assert supertypeInjector != null;
    this.supertypeInjector = supertypeInjector;
    assert apiServiceProvider != null;
    this.apiServiceProvider = apiServiceProvider;
    assert daoSessionProvider != null;
    this.daoSessionProvider = daoSessionProvider;
    assert rxBusProvider != null;
    this.rxBusProvider = rxBusProvider;
  }

  @Override
  public void injectMembers(ChatActivity instance) {  
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    supertypeInjector.injectMembers(instance);
    instance.apiService = apiServiceProvider.get();
    instance.daoSession = daoSessionProvider.get();
    instance.rxBus = rxBusProvider.get();
  }

  public static MembersInjector<ChatActivity> create(MembersInjector<AppCompatActivity> supertypeInjector, Provider<ApiService> apiServiceProvider, Provider<DaoSession> daoSessionProvider, Provider<RxBus> rxBusProvider) {  
      return new ChatActivity_MembersInjector(supertypeInjector, apiServiceProvider, daoSessionProvider, rxBusProvider);
  }
}

