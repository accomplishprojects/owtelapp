// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RegisterPaymentFragment$$ViewBinder<T extends com.katadigital.owtel.modules.registration.RegisterPaymentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689970, "field 'textViewSubscriptionType'");
    target.textViewSubscriptionType = finder.castView(view, 2131689970, "field 'textViewSubscriptionType'");
    view = finder.findRequiredView(source, 2131689972, "field 'textViewSubscriptionPrice'");
    target.textViewSubscriptionPrice = finder.castView(view, 2131689972, "field 'textViewSubscriptionPrice'");
    view = finder.findRequiredView(source, 2131689973, "field 'spinnerIDDPrices' and method 'onItemSelectIdd'");
    target.spinnerIDDPrices = finder.castView(view, 2131689973, "field 'spinnerIDDPrices'");
    ((android.widget.AdapterView<?>) view).setOnItemSelectedListener(
      new android.widget.AdapterView.OnItemSelectedListener() {
        @Override public void onItemSelected(
          android.widget.AdapterView<?> p0,
          android.view.View p1,
          int p2,
          long p3
        ) {
          target.onItemSelectIdd(p2);
        }
        @Override public void onNothingSelected(
          android.widget.AdapterView<?> p0
        ) {
          
        }
      });
    view = finder.findRequiredView(source, 2131689974, "field 'textViewPaymentTotal'");
    target.textViewPaymentTotal = finder.castView(view, 2131689974, "field 'textViewPaymentTotal'");
    view = finder.findRequiredView(source, 2131690102, "field 'etCardNumber'");
    target.etCardNumber = finder.castView(view, 2131690102, "field 'etCardNumber'");
    view = finder.findRequiredView(source, 2131690103, "field 'etCardHolder'");
    target.etCardHolder = finder.castView(view, 2131690103, "field 'etCardHolder'");
    view = finder.findRequiredView(source, 2131690105, "field 'layoutCcMonth'");
    target.layoutCcMonth = finder.castView(view, 2131690105, "field 'layoutCcMonth'");
    view = finder.findRequiredView(source, 2131690106, "field 'spinnerCardExpireMonth'");
    target.spinnerCardExpireMonth = finder.castView(view, 2131690106, "field 'spinnerCardExpireMonth'");
    view = finder.findRequiredView(source, 2131690107, "field 'layoutCcYear'");
    target.layoutCcYear = finder.castView(view, 2131690107, "field 'layoutCcYear'");
    view = finder.findRequiredView(source, 2131690108, "field 'spinnerCardExpireYear'");
    target.spinnerCardExpireYear = finder.castView(view, 2131690108, "field 'spinnerCardExpireYear'");
    view = finder.findRequiredView(source, 2131690109, "field 'etCardCVV'");
    target.etCardCVV = finder.castView(view, 2131690109, "field 'etCardCVV'");
    view = finder.findRequiredView(source, 2131690110, "field 'etAddressStreet'");
    target.etAddressStreet = finder.castView(view, 2131690110, "field 'etAddressStreet'");
    view = finder.findRequiredView(source, 2131690111, "field 'etAddressCity'");
    target.etAddressCity = finder.castView(view, 2131690111, "field 'etAddressCity'");
    view = finder.findRequiredView(source, 2131690112, "field 'etAddressState'");
    target.etAddressState = finder.castView(view, 2131690112, "field 'etAddressState'");
    view = finder.findRequiredView(source, 2131690114, "field 'etAddressZipCode'");
    target.etAddressZipCode = finder.castView(view, 2131690114, "field 'etAddressZipCode'");
    view = finder.findRequiredView(source, 2131690115, "field 'layoutCountry'");
    target.layoutCountry = finder.castView(view, 2131690115, "field 'layoutCountry'");
    view = finder.findRequiredView(source, 2131690116, "field 'spinnerAddressCountry'");
    target.spinnerAddressCountry = finder.castView(view, 2131690116, "field 'spinnerAddressCountry'");
    view = finder.findRequiredView(source, 2131689935, "method 'onClickContinue'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickContinue();
        }
      });
  }

  @Override public void unbind(T target) {
    target.textViewSubscriptionType = null;
    target.textViewSubscriptionPrice = null;
    target.spinnerIDDPrices = null;
    target.textViewPaymentTotal = null;
    target.etCardNumber = null;
    target.etCardHolder = null;
    target.layoutCcMonth = null;
    target.spinnerCardExpireMonth = null;
    target.layoutCcYear = null;
    target.spinnerCardExpireYear = null;
    target.etCardCVV = null;
    target.etAddressStreet = null;
    target.etAddressCity = null;
    target.etAddressState = null;
    target.etAddressZipCode = null;
    target.layoutCountry = null;
    target.spinnerAddressCountry = null;
  }
}
