// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.billing;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CallAndTextSubscriptionPaymentFragment$$ViewBinder<T extends com.katadigital.owtel.modules.billing.CallAndTextSubscriptionPaymentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689932, "field 'expirationDate'");
    target.expirationDate = finder.castView(view, 2131689932, "field 'expirationDate'");
    view = finder.findRequiredView(source, 2131689933, "field 'textViewSubscriptionName'");
    target.textViewSubscriptionName = finder.castView(view, 2131689933, "field 'textViewSubscriptionName'");
    view = finder.findRequiredView(source, 2131689934, "field 'textViewSubscriptionPrice'");
    target.textViewSubscriptionPrice = finder.castView(view, 2131689934, "field 'textViewSubscriptionPrice'");
    view = finder.findRequiredView(source, 2131690102, "field 'etCardNumber'");
    target.etCardNumber = finder.castView(view, 2131690102, "field 'etCardNumber'");
    view = finder.findRequiredView(source, 2131690103, "field 'etCardHolder'");
    target.etCardHolder = finder.castView(view, 2131690103, "field 'etCardHolder'");
    view = finder.findRequiredView(source, 2131690106, "field 'spinnerCardExpireMonth'");
    target.spinnerCardExpireMonth = finder.castView(view, 2131690106, "field 'spinnerCardExpireMonth'");
    view = finder.findRequiredView(source, 2131690108, "field 'spinnerCardExpireYear'");
    target.spinnerCardExpireYear = finder.castView(view, 2131690108, "field 'spinnerCardExpireYear'");
    view = finder.findRequiredView(source, 2131690109, "field 'etCardCVV'");
    target.etCardCVV = finder.castView(view, 2131690109, "field 'etCardCVV'");
    view = finder.findRequiredView(source, 2131690110, "field 'etAddressStreet'");
    target.etAddressStreet = finder.castView(view, 2131690110, "field 'etAddressStreet'");
    view = finder.findRequiredView(source, 2131690111, "field 'etCity'");
    target.etCity = finder.castView(view, 2131690111, "field 'etCity'");
    view = finder.findRequiredView(source, 2131690112, "field 'etState'");
    target.etState = finder.castView(view, 2131690112, "field 'etState'");
    view = finder.findRequiredView(source, 2131690114, "field 'etAddressZipCode'");
    target.etAddressZipCode = finder.castView(view, 2131690114, "field 'etAddressZipCode'");
    view = finder.findRequiredView(source, 2131690116, "field 'spinnerAddressCountry'");
    target.spinnerAddressCountry = finder.castView(view, 2131690116, "field 'spinnerAddressCountry'");
    view = finder.findRequiredView(source, 2131689931, "field 'btnBack' and method 'back'");
    target.btnBack = finder.castView(view, 2131689931, "field 'btnBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.back();
        }
      });
    view = finder.findRequiredView(source, 2131689935, "field 'btnSubmitPayment' and method 'onClickContinue'");
    target.btnSubmitPayment = finder.castView(view, 2131689935, "field 'btnSubmitPayment'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickContinue();
        }
      });
  }

  @Override public void unbind(T target) {
    target.expirationDate = null;
    target.textViewSubscriptionName = null;
    target.textViewSubscriptionPrice = null;
    target.etCardNumber = null;
    target.etCardHolder = null;
    target.spinnerCardExpireMonth = null;
    target.spinnerCardExpireYear = null;
    target.etCardCVV = null;
    target.etAddressStreet = null;
    target.etCity = null;
    target.etState = null;
    target.etAddressZipCode = null;
    target.spinnerAddressCountry = null;
    target.btnBack = null;
    target.btnSubmitPayment = null;
  }
}
