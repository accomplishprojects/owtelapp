// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RegisterPaymentItemsFragment$$ViewBinder<T extends com.katadigital.owtel.modules.registration.RegisterPaymentItemsFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689970, "field 'textViewSubscriptionType'");
    target.textViewSubscriptionType = finder.castView(view, 2131689970, "field 'textViewSubscriptionType'");
    view = finder.findRequiredView(source, 2131689972, "field 'textViewSubscriptionPrice'");
    target.textViewSubscriptionPrice = finder.castView(view, 2131689972, "field 'textViewSubscriptionPrice'");
    view = finder.findRequiredView(source, 2131689973, "field 'spinnerIDDPrices' and method 'onItemSelectIdd'");
    target.spinnerIDDPrices = finder.castView(view, 2131689973, "field 'spinnerIDDPrices'");
    ((android.widget.AdapterView<?>) view).setOnItemSelectedListener(
      new android.widget.AdapterView.OnItemSelectedListener() {
        @Override public void onItemSelected(
          android.widget.AdapterView<?> p0,
          android.view.View p1,
          int p2,
          long p3
        ) {
          target.onItemSelectIdd(p2);
        }
        @Override public void onNothingSelected(
          android.widget.AdapterView<?> p0
        ) {
          
        }
      });
    view = finder.findRequiredView(source, 2131689974, "field 'textViewPaymentTotal'");
    target.textViewPaymentTotal = finder.castView(view, 2131689974, "field 'textViewPaymentTotal'");
    view = finder.findRequiredView(source, 2131689935, "method 'onClickContinue'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickContinue();
        }
      });
  }

  @Override public void unbind(T target) {
    target.textViewSubscriptionType = null;
    target.textViewSubscriptionPrice = null;
    target.spinnerIDDPrices = null;
    target.textViewPaymentTotal = null;
  }
}
