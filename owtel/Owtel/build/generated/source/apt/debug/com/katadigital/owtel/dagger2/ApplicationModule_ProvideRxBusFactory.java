package com.katadigital.owtel.dagger2;

import com.katadigital.owtel.utils.RxBus;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class ApplicationModule_ProvideRxBusFactory implements Factory<RxBus> {
  private final ApplicationModule module;

  public ApplicationModule_ProvideRxBusFactory(ApplicationModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public RxBus get() {  
    RxBus provided = module.provideRxBus();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<RxBus> create(ApplicationModule module) {  
    return new ApplicationModule_ProvideRxBusFactory(module);
  }
}

