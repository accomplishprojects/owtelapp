// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RegisterPaymentCheckoutFragment$$ViewBinder<T extends com.katadigital.owtel.modules.registration.RegisterPaymentCheckoutFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689974, "field 'mTextViewTotalAmount'");
    target.mTextViewTotalAmount = finder.castView(view, 2131689974, "field 'mTextViewTotalAmount'");
    view = finder.findRequiredView(source, 2131690129, "field 'mScrollView'");
    target.mScrollView = finder.castView(view, 2131690129, "field 'mScrollView'");
    view = finder.findRequiredView(source, 2131690102, "field 'etCardNumber'");
    target.etCardNumber = finder.castView(view, 2131690102, "field 'etCardNumber'");
    view = finder.findRequiredView(source, 2131690103, "field 'etCardHolder'");
    target.etCardHolder = finder.castView(view, 2131690103, "field 'etCardHolder'");
    view = finder.findRequiredView(source, 2131690105, "field 'layoutCcMonth'");
    target.layoutCcMonth = finder.castView(view, 2131690105, "field 'layoutCcMonth'");
    view = finder.findRequiredView(source, 2131690106, "field 'spinnerCardExpireMonth'");
    target.spinnerCardExpireMonth = finder.castView(view, 2131690106, "field 'spinnerCardExpireMonth'");
    view = finder.findRequiredView(source, 2131690107, "field 'layoutCcYear'");
    target.layoutCcYear = finder.castView(view, 2131690107, "field 'layoutCcYear'");
    view = finder.findRequiredView(source, 2131690108, "field 'spinnerCardExpireYear'");
    target.spinnerCardExpireYear = finder.castView(view, 2131690108, "field 'spinnerCardExpireYear'");
    view = finder.findRequiredView(source, 2131690109, "field 'etCardCVV'");
    target.etCardCVV = finder.castView(view, 2131690109, "field 'etCardCVV'");
    view = finder.findRequiredView(source, 2131690110, "field 'etAddressStreet'");
    target.etAddressStreet = finder.castView(view, 2131690110, "field 'etAddressStreet'");
    view = finder.findRequiredView(source, 2131690111, "field 'etAddressCity'");
    target.etAddressCity = finder.castView(view, 2131690111, "field 'etAddressCity'");
    view = finder.findRequiredView(source, 2131690112, "field 'etAddressState'");
    target.etAddressState = finder.castView(view, 2131690112, "field 'etAddressState'");
    view = finder.findRequiredView(source, 2131690114, "field 'etAddressZipCode'");
    target.etAddressZipCode = finder.castView(view, 2131690114, "field 'etAddressZipCode'");
    view = finder.findRequiredView(source, 2131690115, "field 'layoutCountry'");
    target.layoutCountry = finder.castView(view, 2131690115, "field 'layoutCountry'");
    view = finder.findRequiredView(source, 2131690116, "field 'spinnerAddressCountry'");
    target.spinnerAddressCountry = finder.castView(view, 2131690116, "field 'spinnerAddressCountry'");
    view = finder.findRequiredView(source, 2131690101, "field 'btnPaymentCheckout' and method 'onClickPaymentCheckout'");
    target.btnPaymentCheckout = finder.castView(view, 2131690101, "field 'btnPaymentCheckout'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickPaymentCheckout();
        }
      });
  }

  @Override public void unbind(T target) {
    target.mTextViewTotalAmount = null;
    target.mScrollView = null;
    target.etCardNumber = null;
    target.etCardHolder = null;
    target.layoutCcMonth = null;
    target.spinnerCardExpireMonth = null;
    target.layoutCcYear = null;
    target.spinnerCardExpireYear = null;
    target.etCardCVV = null;
    target.etAddressStreet = null;
    target.etAddressCity = null;
    target.etAddressState = null;
    target.etAddressZipCode = null;
    target.layoutCountry = null;
    target.spinnerAddressCountry = null;
    target.btnPaymentCheckout = null;
  }
}
