
package com.katadigital.owtel.daoDb;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2017-04-11T16:55+0800")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class SubscriptionList$$Parcelable
    implements Parcelable, ParcelWrapper<com.katadigital.owtel.daoDb.SubscriptionList>
{

    private com.katadigital.owtel.daoDb.SubscriptionList subscriptionList$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static SubscriptionList$$Parcelable.Creator$$0 CREATOR = new SubscriptionList$$Parcelable.Creator$$0();

    public SubscriptionList$$Parcelable(com.katadigital.owtel.daoDb.SubscriptionList subscriptionList$$2) {
        subscriptionList$$0 = subscriptionList$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(subscriptionList$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(com.katadigital.owtel.daoDb.SubscriptionList subscriptionList$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(subscriptionList$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(subscriptionList$$1));
            parcel$$1 .writeString(subscriptionList$$1 .subscriptionName);
            parcel$$1 .writeDouble(subscriptionList$$1 .subscriptionValue);
            parcel$$1 .writeString(subscriptionList$$1 .subscriptionId);
            parcel$$1 .writeString(subscriptionList$$1 .subscriptionPrice);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.katadigital.owtel.daoDb.SubscriptionList getParcel() {
        return subscriptionList$$0;
    }

    public static com.katadigital.owtel.daoDb.SubscriptionList read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            com.katadigital.owtel.daoDb.SubscriptionList subscriptionList$$3;
            int reservation$$0 = identityMap$$1 .reserve();
            subscriptionList$$3 = new com.katadigital.owtel.daoDb.SubscriptionList();
            identityMap$$1 .put(reservation$$0, subscriptionList$$3);
            subscriptionList$$3 .subscriptionName = parcel$$3 .readString();
            subscriptionList$$3 .subscriptionValue = parcel$$3 .readDouble();
            subscriptionList$$3 .subscriptionId = parcel$$3 .readString();
            subscriptionList$$3 .subscriptionPrice = parcel$$3 .readString();
            return subscriptionList$$3;
        }
    }

    public final static class Creator$$0
        implements Creator<SubscriptionList$$Parcelable>
    {


        @Override
        public SubscriptionList$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new SubscriptionList$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public SubscriptionList$$Parcelable[] newArray(int size) {
            return new SubscriptionList$$Parcelable[size] ;
        }

    }

}
