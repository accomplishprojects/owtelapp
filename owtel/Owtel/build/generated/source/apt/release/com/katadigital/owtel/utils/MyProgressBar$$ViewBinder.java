// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.utils;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MyProgressBar$$ViewBinder<T extends com.katadigital.owtel.utils.MyProgressBar> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690136, "field 'card'");
    target.card = finder.castView(view, 2131690136, "field 'card'");
    view = finder.findRequiredView(source, 2131690137, "field 'progressBar'");
    target.progressBar = finder.castView(view, 2131690137, "field 'progressBar'");
    view = finder.findRequiredView(source, 2131690117, "field 'tvMessage'");
    target.tvMessage = finder.castView(view, 2131690117, "field 'tvMessage'");
  }

  @Override public void unbind(T target) {
    target.card = null;
    target.progressBar = null;
    target.tvMessage = null;
  }
}
