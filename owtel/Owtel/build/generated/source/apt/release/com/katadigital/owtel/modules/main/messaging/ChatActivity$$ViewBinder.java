// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.main.messaging;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ChatActivity$$ViewBinder<T extends com.katadigital.owtel.modules.main.messaging.ChatActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689679, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131689679, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131689685, "field 'rvChat'");
    target.rvChat = finder.castView(view, 2131689685, "field 'rvChat'");
    view = finder.findRequiredView(source, 2131689688, "field 'etMessage'");
    target.etMessage = finder.castView(view, 2131689688, "field 'etMessage'");
    view = finder.findRequiredView(source, 2131689681, "field 'recipientViewInner'");
    target.recipientViewInner = finder.castView(view, 2131689681, "field 'recipientViewInner'");
    view = finder.findRequiredView(source, 2131689689, "field 'btnSend' and method 'onBtnSendClick'");
    target.btnSend = finder.castView(view, 2131689689, "field 'btnSend'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onBtnSendClick();
        }
      });
    view = finder.findRequiredView(source, 2131689684, "field 'imgViewAddRecipient' and method 'onBtnAddRecipientClick'");
    target.imgViewAddRecipient = finder.castView(view, 2131689684, "field 'imgViewAddRecipient'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onBtnAddRecipientClick();
        }
      });
    view = finder.findRequiredView(source, 2131689683, "field 'toRecipientNumber'");
    target.toRecipientNumber = finder.castView(view, 2131689683, "field 'toRecipientNumber'");
    view = finder.findRequiredView(source, 2131690236, "method 'onBtnCallClicked'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onBtnCallClicked();
        }
      });
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.rvChat = null;
    target.etMessage = null;
    target.recipientViewInner = null;
    target.btnSend = null;
    target.imgViewAddRecipient = null;
    target.toRecipientNumber = null;
  }
}
