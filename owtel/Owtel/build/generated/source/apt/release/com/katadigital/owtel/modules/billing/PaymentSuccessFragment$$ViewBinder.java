// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.billing;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PaymentSuccessFragment$$ViewBinder<T extends com.katadigital.owtel.modules.billing.PaymentSuccessFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689976, "field 'tvPaymentMessage'");
    target.tvPaymentMessage = finder.castView(view, 2131689976, "field 'tvPaymentMessage'");
    view = finder.findRequiredView(source, 2131689977, "field 'btnContinue' and method 'onClickContinue'");
    target.btnContinue = finder.castView(view, 2131689977, "field 'btnContinue'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickContinue();
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvPaymentMessage = null;
    target.btnContinue = null;
  }
}
