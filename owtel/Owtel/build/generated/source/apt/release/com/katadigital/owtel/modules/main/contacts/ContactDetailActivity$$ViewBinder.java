// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.main.contacts;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ContactDetailActivity$$ViewBinder<T extends com.katadigital.owtel.modules.main.contacts.ContactDetailActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689740, "field 'mButtonContactMessage' and method 'onClickBtnContactMessage'");
    target.mButtonContactMessage = finder.castView(view, 2131689740, "field 'mButtonContactMessage'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickBtnContactMessage();
        }
      });
    view = finder.findRequiredView(source, 2131689742, "field 'mButtonContactShare' and method 'onClickBtnContactShare'");
    target.mButtonContactShare = finder.castView(view, 2131689742, "field 'mButtonContactShare'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickBtnContactShare();
        }
      });
    view = finder.findRequiredView(source, 2131689744, "field 'mButtonContactAddFavorite' and method 'onClickBtnContactAddFavorite'");
    target.mButtonContactAddFavorite = finder.castView(view, 2131689744, "field 'mButtonContactAddFavorite'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickBtnContactAddFavorite();
        }
      });
    view = finder.findRequiredView(source, 2131689746, "field 'mButtonContactBlock' and method 'onClickBtnContactBlock'");
    target.mButtonContactBlock = finder.castView(view, 2131689746, "field 'mButtonContactBlock'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickBtnContactBlock();
        }
      });
  }

  @Override public void unbind(T target) {
    target.mButtonContactMessage = null;
    target.mButtonContactShare = null;
    target.mButtonContactAddFavorite = null;
    target.mButtonContactBlock = null;
  }
}
