
package com.katadigital.owtel.daoDb;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2017-04-11T16:55+0800")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class AddressList$$Parcelable
    implements Parcelable, ParcelWrapper<com.katadigital.owtel.daoDb.AddressList>
{

    private com.katadigital.owtel.daoDb.AddressList addressList$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static AddressList$$Parcelable.Creator$$2 CREATOR = new AddressList$$Parcelable.Creator$$2();

    public AddressList$$Parcelable(com.katadigital.owtel.daoDb.AddressList addressList$$2) {
        addressList$$0 = addressList$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(addressList$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(com.katadigital.owtel.daoDb.AddressList addressList$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(addressList$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(addressList$$1));
            parcel$$1 .writeString(addressList$$1 .country);
            parcel$$1 .writeString(addressList$$1 .city);
            parcel$$1 .writeString(addressList$$1 .stateId);
            parcel$$1 .writeString(addressList$$1 .state);
            parcel$$1 .writeString(addressList$$1 .cityId);
            parcel$$1 .writeString(addressList$$1 .countryId);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.katadigital.owtel.daoDb.AddressList getParcel() {
        return addressList$$0;
    }

    public static com.katadigital.owtel.daoDb.AddressList read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            com.katadigital.owtel.daoDb.AddressList addressList$$3;
            int reservation$$0 = identityMap$$1 .reserve();
            addressList$$3 = new com.katadigital.owtel.daoDb.AddressList();
            identityMap$$1 .put(reservation$$0, addressList$$3);
            addressList$$3 .country = parcel$$3 .readString();
            addressList$$3 .city = parcel$$3 .readString();
            addressList$$3 .stateId = parcel$$3 .readString();
            addressList$$3 .state = parcel$$3 .readString();
            addressList$$3 .cityId = parcel$$3 .readString();
            addressList$$3 .countryId = parcel$$3 .readString();
            return addressList$$3;
        }
    }

    public final static class Creator$$2
        implements Creator<AddressList$$Parcelable>
    {


        @Override
        public AddressList$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new AddressList$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public AddressList$$Parcelable[] newArray(int size) {
            return new AddressList$$Parcelable[size] ;
        }

    }

}
