// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.resetaccess;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ForgotPasswordSuccessFragment$$ViewBinder<T extends com.katadigital.owtel.modules.resetaccess.ForgotPasswordSuccessFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689977, "method 'onContinueClicked'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onContinueClicked();
        }
      });
  }

  @Override public void unbind(T target) {
  }
}
