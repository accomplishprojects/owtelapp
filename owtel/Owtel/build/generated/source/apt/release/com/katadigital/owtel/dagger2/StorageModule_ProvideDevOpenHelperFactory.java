package com.katadigital.owtel.dagger2;

import com.katadigital.owtel.daoDb.DaoMaster.DevOpenHelper;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class StorageModule_ProvideDevOpenHelperFactory implements Factory<DevOpenHelper> {
  private final StorageModule module;

  public StorageModule_ProvideDevOpenHelperFactory(StorageModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public DevOpenHelper get() {  
    DevOpenHelper provided = module.provideDevOpenHelper();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<DevOpenHelper> create(StorageModule module) {  
    return new StorageModule_ProvideDevOpenHelperFactory(module);
  }
}

