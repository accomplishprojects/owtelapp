// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.resetaccess;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ForgotPasswordFragment$$ViewBinder<T extends com.katadigital.owtel.modules.resetaccess.ForgotPasswordFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690063, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131690063, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131690064, "field 'tvEmailNotice'");
    target.tvEmailNotice = finder.castView(view, 2131690064, "field 'tvEmailNotice'");
    view = finder.findRequiredView(source, 2131690061, "field 'etCode'");
    target.etCode = finder.castView(view, 2131690061, "field 'etCode'");
    view = finder.findRequiredView(source, 2131690062, "field 'tvCodeNotice'");
    target.tvCodeNotice = finder.castView(view, 2131690062, "field 'tvCodeNotice'");
    view = finder.findRequiredView(source, 2131690058, "field 'etPassword'");
    target.etPassword = finder.castView(view, 2131690058, "field 'etPassword'");
    view = finder.findRequiredView(source, 2131690059, "field 'tvPassNotice'");
    target.tvPassNotice = finder.castView(view, 2131690059, "field 'tvPassNotice'");
    view = finder.findRequiredView(source, 2131690060, "field 'checkBoxPasswordShow'");
    target.checkBoxPasswordShow = finder.castView(view, 2131690060, "field 'checkBoxPasswordShow'");
    view = finder.findRequiredView(source, 2131690054, "field 'btnSubmit' and method 'onSubmit'");
    target.btnSubmit = finder.castView(view, 2131690054, "field 'btnSubmit'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onSubmit();
        }
      });
    view = finder.findRequiredView(source, 2131690051, "field 'layoutForgotPassEmail'");
    target.layoutForgotPassEmail = view;
    view = finder.findRequiredView(source, 2131690052, "field 'layoutForgotPassCode'");
    target.layoutForgotPassCode = view;
    view = finder.findRequiredView(source, 2131690053, "field 'layoutForgotPassChange'");
    target.layoutForgotPassChange = view;
  }

  @Override public void unbind(T target) {
    target.etEmail = null;
    target.tvEmailNotice = null;
    target.etCode = null;
    target.tvCodeNotice = null;
    target.etPassword = null;
    target.tvPassNotice = null;
    target.checkBoxPasswordShow = null;
    target.btnSubmit = null;
    target.layoutForgotPassEmail = null;
    target.layoutForgotPassCode = null;
    target.layoutForgotPassChange = null;
  }
}
