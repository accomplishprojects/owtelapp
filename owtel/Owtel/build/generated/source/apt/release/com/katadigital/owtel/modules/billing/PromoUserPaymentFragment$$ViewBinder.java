// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.billing;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PromoUserPaymentFragment$$ViewBinder<T extends com.katadigital.owtel.modules.billing.PromoUserPaymentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689970, "field 'textViewSubscriptionType'");
    target.textViewSubscriptionType = finder.castView(view, 2131689970, "field 'textViewSubscriptionType'");
    view = finder.findRequiredView(source, 2131689972, "field 'textViewSubscriptionPrice'");
    target.textViewSubscriptionPrice = finder.castView(view, 2131689972, "field 'textViewSubscriptionPrice'");
    view = finder.findRequiredView(source, 2131689971, "field 'spinnerSubscriptionPrices' and method 'onItemSelectSubscription'");
    target.spinnerSubscriptionPrices = finder.castView(view, 2131689971, "field 'spinnerSubscriptionPrices'");
    ((android.widget.AdapterView<?>) view).setOnItemSelectedListener(
      new android.widget.AdapterView.OnItemSelectedListener() {
        @Override public void onItemSelected(
          android.widget.AdapterView<?> p0,
          android.view.View p1,
          int p2,
          long p3
        ) {
          target.onItemSelectSubscription(p2);
        }
        @Override public void onNothingSelected(
          android.widget.AdapterView<?> p0
        ) {
          
        }
      });
    view = finder.findRequiredView(source, 2131689973, "field 'spinnerIDDPrices' and method 'onItemSelectIdd'");
    target.spinnerIDDPrices = finder.castView(view, 2131689973, "field 'spinnerIDDPrices'");
    ((android.widget.AdapterView<?>) view).setOnItemSelectedListener(
      new android.widget.AdapterView.OnItemSelectedListener() {
        @Override public void onItemSelected(
          android.widget.AdapterView<?> p0,
          android.view.View p1,
          int p2,
          long p3
        ) {
          target.onItemSelectIdd(p2);
        }
        @Override public void onNothingSelected(
          android.widget.AdapterView<?> p0
        ) {
          
        }
      });
    view = finder.findRequiredView(source, 2131689974, "field 'textViewPaymentTotal'");
    target.textViewPaymentTotal = finder.castView(view, 2131689974, "field 'textViewPaymentTotal'");
    view = finder.findRequiredView(source, 2131690102, "field 'etCardNumber'");
    target.etCardNumber = finder.castView(view, 2131690102, "field 'etCardNumber'");
    view = finder.findRequiredView(source, 2131690103, "field 'etCardHolder'");
    target.etCardHolder = finder.castView(view, 2131690103, "field 'etCardHolder'");
    view = finder.findRequiredView(source, 2131690106, "field 'spinnerCardExpireMonth'");
    target.spinnerCardExpireMonth = finder.castView(view, 2131690106, "field 'spinnerCardExpireMonth'");
    view = finder.findRequiredView(source, 2131690108, "field 'spinnerCardExpireYear'");
    target.spinnerCardExpireYear = finder.castView(view, 2131690108, "field 'spinnerCardExpireYear'");
    view = finder.findRequiredView(source, 2131690109, "field 'etCardCVV'");
    target.etCardCVV = finder.castView(view, 2131690109, "field 'etCardCVV'");
    view = finder.findRequiredView(source, 2131690110, "field 'etAddressStreet'");
    target.etAddressStreet = finder.castView(view, 2131690110, "field 'etAddressStreet'");
    view = finder.findRequiredView(source, 2131690111, "field 'etCity'");
    target.etCity = finder.castView(view, 2131690111, "field 'etCity'");
    view = finder.findRequiredView(source, 2131690112, "field 'etState'");
    target.etState = finder.castView(view, 2131690112, "field 'etState'");
    view = finder.findRequiredView(source, 2131690114, "field 'etAddressZipCode'");
    target.etAddressZipCode = finder.castView(view, 2131690114, "field 'etAddressZipCode'");
    view = finder.findRequiredView(source, 2131690116, "field 'spinnerAddressCountry'");
    target.spinnerAddressCountry = finder.castView(view, 2131690116, "field 'spinnerAddressCountry'");
    view = finder.findRequiredView(source, 2131689931, "method 'onBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onBack();
        }
      });
    view = finder.findRequiredView(source, 2131689975, "method 'onContinue'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onContinue();
        }
      });
  }

  @Override public void unbind(T target) {
    target.textViewSubscriptionType = null;
    target.textViewSubscriptionPrice = null;
    target.spinnerSubscriptionPrices = null;
    target.spinnerIDDPrices = null;
    target.textViewPaymentTotal = null;
    target.etCardNumber = null;
    target.etCardHolder = null;
    target.spinnerCardExpireMonth = null;
    target.spinnerCardExpireYear = null;
    target.etCardCVV = null;
    target.etAddressStreet = null;
    target.etCity = null;
    target.etState = null;
    target.etAddressZipCode = null;
    target.spinnerAddressCountry = null;
  }
}
