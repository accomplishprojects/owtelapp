package com.katadigital.owtel.dagger2;

import android.database.sqlite.SQLiteDatabase;
import com.katadigital.owtel.daoDb.DaoMaster.DevOpenHelper;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class StorageModule_ProvideSQLiteDatabaseFactory implements Factory<SQLiteDatabase> {
  private final StorageModule module;
  private final Provider<DevOpenHelper> helperProvider;

  public StorageModule_ProvideSQLiteDatabaseFactory(StorageModule module, Provider<DevOpenHelper> helperProvider) {  
    assert module != null;
    this.module = module;
    assert helperProvider != null;
    this.helperProvider = helperProvider;
  }

  @Override
  public SQLiteDatabase get() {  
    SQLiteDatabase provided = module.provideSQLiteDatabase(helperProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<SQLiteDatabase> create(StorageModule module, Provider<DevOpenHelper> helperProvider) {  
    return new StorageModule_ProvideSQLiteDatabaseFactory(module, helperProvider);
  }
}

