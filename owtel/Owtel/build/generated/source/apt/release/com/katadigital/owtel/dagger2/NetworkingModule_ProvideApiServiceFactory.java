package com.katadigital.owtel.dagger2;

import com.katadigital.owtel.api.ApiService;
import com.squareup.okhttp.OkHttpClient;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkingModule_ProvideApiServiceFactory implements Factory<ApiService> {
  private final NetworkingModule module;
  private final Provider<OkHttpClient> httpClientProvider;

  public NetworkingModule_ProvideApiServiceFactory(NetworkingModule module, Provider<OkHttpClient> httpClientProvider) {  
    assert module != null;
    this.module = module;
    assert httpClientProvider != null;
    this.httpClientProvider = httpClientProvider;
  }

  @Override
  public ApiService get() {  
    ApiService provided = module.provideApiService(httpClientProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ApiService> create(NetworkingModule module, Provider<OkHttpClient> httpClientProvider) {  
    return new NetworkingModule_ProvideApiServiceFactory(module, httpClientProvider);
  }
}

