// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RegisterAreaCodeAndNumberFragment$$ViewBinder<T extends com.katadigital.owtel.modules.registration.RegisterAreaCodeAndNumberFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690122, "field 'spinnerAreaCode' and method 'onAreaCodeSelected'");
    target.spinnerAreaCode = finder.castView(view, 2131690122, "field 'spinnerAreaCode'");
    ((android.widget.AdapterView<?>) view).setOnItemSelectedListener(
      new android.widget.AdapterView.OnItemSelectedListener() {
        @Override public void onItemSelected(
          android.widget.AdapterView<?> p0,
          android.view.View p1,
          int p2,
          long p3
        ) {
          target.onAreaCodeSelected(p2);
        }
        @Override public void onNothingSelected(
          android.widget.AdapterView<?> p0
        ) {
          
        }
      });
    view = finder.findRequiredView(source, 2131690123, "field 'radioGroupNumber'");
    target.radioGroupNumber = finder.castView(view, 2131690123, "field 'radioGroupNumber'");
    view = finder.findRequiredView(source, 2131690124, "field 'ibRefreshNum' and method 'refreshNumberList'");
    target.ibRefreshNum = finder.castView(view, 2131690124, "field 'ibRefreshNum'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.refreshNumberList();
        }
      });
    view = finder.findRequiredView(source, 2131690125, "field 'tvRefreshCredits'");
    target.tvRefreshCredits = finder.castView(view, 2131690125, "field 'tvRefreshCredits'");
    view = finder.findRequiredView(source, 2131689977, "field 'btnContinue' and method 'onContinue'");
    target.btnContinue = finder.castView(view, 2131689977, "field 'btnContinue'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onContinue();
        }
      });
  }

  @Override public void unbind(T target) {
    target.spinnerAreaCode = null;
    target.radioGroupNumber = null;
    target.ibRefreshNum = null;
    target.tvRefreshCredits = null;
    target.btnContinue = null;
  }
}
