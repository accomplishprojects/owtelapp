// Generated code from Butter Knife. Do not modify!
package com.katadigital.owtel.modules.login;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LoginActivity$$ViewBinder<T extends com.katadigital.owtel.modules.login.LoginActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131690142, "field 'btnLogin' and method 'loginBtnClicked'");
    target.btnLogin = finder.castView(view, 2131690142, "field 'btnLogin'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.loginBtnClicked();
        }
      });
    view = finder.findRequiredView(source, 2131690063, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131690063, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131690058, "field 'etPassword'");
    target.etPassword = finder.castView(view, 2131690058, "field 'etPassword'");
    view = finder.findRequiredView(source, 2131690121, "field 'btnRegister' and method 'registerBtnClicked'");
    target.btnRegister = finder.castView(view, 2131690121, "field 'btnRegister'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.registerBtnClicked();
        }
      });
    view = finder.findRequiredView(source, 2131690139, "field 'tvAppName'");
    target.tvAppName = finder.castView(view, 2131690139, "field 'tvAppName'");
    view = finder.findRequiredView(source, 2131690064, "field 'tvEmailNotice'");
    target.tvEmailNotice = finder.castView(view, 2131690064, "field 'tvEmailNotice'");
    view = finder.findRequiredView(source, 2131690059, "field 'tvPassNotice'");
    target.tvPassNotice = finder.castView(view, 2131690059, "field 'tvPassNotice'");
    view = finder.findRequiredView(source, 2131690144, "field 'layoutProgress'");
    target.layoutProgress = view;
    view = finder.findRequiredView(source, 2131690143, "method 'onClickForgotPass'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClickForgotPass();
        }
      });
  }

  @Override public void unbind(T target) {
    target.btnLogin = null;
    target.etEmail = null;
    target.etPassword = null;
    target.btnRegister = null;
    target.tvAppName = null;
    target.tvEmailNotice = null;
    target.tvPassNotice = null;
    target.layoutProgress = null;
  }
}
