package com.katadigital.owtel.dagger2;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class NetworkingModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
  private final NetworkingModule module;
  private final Provider<HttpLoggingInterceptor> httpLoggingInterceptorProvider;

  public NetworkingModule_ProvideOkHttpClientFactory(NetworkingModule module, Provider<HttpLoggingInterceptor> httpLoggingInterceptorProvider) {  
    assert module != null;
    this.module = module;
    assert httpLoggingInterceptorProvider != null;
    this.httpLoggingInterceptorProvider = httpLoggingInterceptorProvider;
  }

  @Override
  public OkHttpClient get() {  
    OkHttpClient provided = module.provideOkHttpClient(httpLoggingInterceptorProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<OkHttpClient> create(NetworkingModule module, Provider<HttpLoggingInterceptor> httpLoggingInterceptorProvider) {  
    return new NetworkingModule_ProvideOkHttpClientFactory(module, httpLoggingInterceptorProvider);
  }
}

