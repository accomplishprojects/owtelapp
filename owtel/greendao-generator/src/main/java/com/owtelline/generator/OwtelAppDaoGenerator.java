package com.owtelline.generator;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class OwtelAppDaoGenerator {

    public static final int DATABASE_VERSION = 5;

    public static final String DEFAULT_JAVA_PACKAGE = "com.katadigital.owtel.chat.daoDb";

    public static void main(String[] args) throws Exception{
        Schema schema = new Schema(DATABASE_VERSION,DEFAULT_JAVA_PACKAGE);

        schema.enableKeepSectionsByDefault();
        // create user entity
//        Entity userEntity = schema.addEntity("User");
//        createUserEntity(userEntity);
//
//        // create messages map entity//
//        Entity messageMapEntity = schema.addEntity("MessageMap");//
//        createMessagesMapEntity(messageMapEntity);

        // create messages entity
        Entity messageEntity = schema.addEntity("Message");
        createMessagesEntity(messageEntity);

        // create number entity
//        Entity usNumberEntity = schema.addEntity("USNumber");
//        usNumberEntity.addStringProperty("number").primaryKey();
//
        // create area code entity
        Entity areaCodeEntity = schema.addEntity("AreaCode");
        createAreaCodeEntity(areaCodeEntity);

        DaoGenerator daoGenerator = new DaoGenerator();
        daoGenerator.generateAll(schema,"./Owtel/src/main/java");

    }

//    private static void createUserEntity(Entity userEntity){
//        userEntity.addIdProperty();
//        userEntity.addStringProperty("userID");
//        userEntity.addStringProperty("firstName");
//        userEntity.addStringProperty("lastName");
//        userEntity.addStringProperty("number");
//        userEntity.addStringProperty("email");
//    }
////
//    private static void createMessagesMapEntity(Entity messageMapEntity){//
//        messageMapEntity.addIdProperty();//
//        messageMapEntity.addStringProperty("roomID");//
//        messageMapEntity.addStringProperty("messageID");//
//        messageMapEntity.addDateProperty("messageCreated");//
//    }

    private static void createMessagesEntity(Entity messageEntity){
        messageEntity.addIdProperty();
        messageEntity.addStringProperty("roomID");
        messageEntity.addStringProperty("messageID");
        messageEntity.addStringProperty("value");
        messageEntity.addStringProperty("type");
        messageEntity.addStringProperty("created");
        messageEntity.addStringProperty("status");
        messageEntity.addStringProperty("userID");
        messageEntity.addStringProperty("sender");
        messageEntity.addStringProperty("receiver");
        messageEntity.addStringProperty("is_message_read");
        messageEntity.addStringProperty("display_name");
        messageEntity.addStringProperty("route");
        messageEntity.addStringProperty("is_deleted");

    }

    private static void createAreaCodeEntity(Entity areaCodeEntity){
        areaCodeEntity.addIdProperty();
        areaCodeEntity.addStringProperty("areaCode");
        areaCodeEntity.addStringProperty("location");
    }


    private static void createfistEntity(Entity areaCodeEntity){
        areaCodeEntity.addIdProperty();
        areaCodeEntity.addStringProperty("areaCode");
        areaCodeEntity.addStringProperty("location");
    }
}
