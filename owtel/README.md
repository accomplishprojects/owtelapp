# README #

Owtel App for Android implements the following:

###[SIPdroid](https://github.com/i-p-tel/sipdroid)###

See http://sipdroid.org for more info
	
* Copyright (C) 2009 The Sipdroid Open Source Project (http://sipdroid.org)
* Copyright (C) 2008 Hughes Systique Corporation, USA (http://hsc.com)
* Copyright (C) 2006 The Android Open Source Project (http://android.com)
* Copyright (C) 2005 Luca Veltri - University of Parma - Italy (http://mjsip.org)

###[Autobahn WebSocketConnection](https://github.com/crossbario/autobahn-android)###

See http://autobahn.ws for more info

* [License](http://www.apache.org/licenses/LICENSE-2.0)


### Requirements ###

* Android Studio
* Android SDK
* Android NDK
* Gradle Build 
* Java SDK 1.7 & 1.8 (for retrolambda)
* Android SDK Tools
* Android SDK Build-tools 23.0.2
* Target SDK version: 23
* Minimum SDK version: 17


### Dependencies ###
    compile 'com.android.support:appcompat-v7:23.2.1'
    compile 'com.android.support:design:23.+'
    compile 'com.android.support:recyclerview-v7:23.2.1'
    compile 'com.android.support:cardview-v7:23.2.1'
    compile 'com.android.volley:volley:1.0.0'
    compile 'de.hdodenhof:circleimageview:2.0.0'
    compile project(':CountryPicker')
    compile project(':cropper')
    compile project(':simplecropimagelib')
    compile project(':EmptyLayout')
    compile files('libs/kataupdater-v1.0.jar')
    compile 'io.reactivex:rxjava:1.1.1'
    compile 'io.reactivex:rxandroid:1.1.0'
    compile 'com.jakewharton:butterknife:7.0.1'
    compile 'com.squareup.retrofit2:retrofit:2.0.0-beta4'
    compile 'com.squareup.retrofit:converter-gson:2.0.0-beta2'
    compile 'com.squareup.okhttp:logging-interceptor:2.6.0'
    compile 'com.squareup.retrofit:adapter-rxjava:2.0.0-beta2'
    apt 'com.google.dagger:dagger-compiler:2.0'
    compile 'com.google.dagger:dagger:2.0'
    provided 'javax.annotation:jsr250-api:1.0'
    compile 'de.greenrobot:greendao:2.1.0'
    compile files('libs/autobahn-0.5.0.jar')
    compile project(':greendao-generator')


### Setup ###

* Fork it ( https://bitbucket.org/kataandroidteam/owtel/pull-requests/new )
* Import to Android Studio
* Setup Android SDK path
* Setup gradle
* Build


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines


### Contributors ###

* Rae-an Andres (2016 July - Present)
* Jose Mari Lumanlan (2016 February - Present)
* Omar Matthew Reyes (2016 February - 2016 July)
* John Erick Lester Sumugat (2016 February - 2016 March)